-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 02/12/2015 às 14:06
-- Versão do servidor: 10.0.22-MariaDB
-- Versão do PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ged`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `acao`
--

CREATE TABLE IF NOT EXISTS `acao` (
  `action_id` int(11) NOT NULL,
  `action_name` varchar(45) NOT NULL,
  `action_module_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `acao`
--

INSERT INTO `acao` (`action_id`, `action_name`, `action_module_id`) VALUES
(1, 'index', 1),
(2, 'home', 1),
(3, 'index', 2),
(4, 'cadastro', 2),
(5, 'save', 2),
(6, 'index', 4),
(7, 'cadastro', 4),
(8, 'save', 4),
(9, 'editar', 4),
(10, 'excluir', 4),
(11, 'index', 3),
(12, 'cadastro', 3),
(13, 'editar', 3),
(14, 'save', 3),
(15, 'excluir', 3),
(16, 'index', 5),
(17, 'cadastro', 5),
(18, 'editar', 5),
(19, 'save', 5),
(20, 'excluir', 5),
(21, 'index', 7),
(22, 'cadastro', 7),
(23, 'save', 7),
(24, 'editar', 7),
(25, 'excluir', 7),
(26, 'index', 6),
(27, 'cadastro', 6),
(28, 'save', 6),
(29, 'editar', 6),
(30, 'excluir', 6),
(31, 'index', 9),
(32, 'cadastro', 9),
(33, 'save', 9),
(34, 'excluir', 9),
(35, 'editar', 9),
(36, 'index', 8),
(37, 'cadastro', 8),
(38, 'save', 8),
(39, 'editar', 8),
(40, 'excluir', 8),
(41, 'index', 11),
(42, 'cadastro', 11),
(43, 'save', 11),
(45, 'editar', 11),
(46, 'excluir', 11),
(47, 'index', 10),
(48, 'cadastro', 10),
(49, 'save', 10),
(50, 'editar', 10),
(51, 'excluir', 10),
(52, 'index', 12),
(53, 'cadastro', 12),
(54, 'save', 12),
(55, 'editar', 12),
(56, 'excluir', 12),
(57, 'index', 13),
(58, 'cadastro', 13),
(59, 'save', 13),
(60, 'editar', 13),
(61, 'excluir', 13),
(62, 'index', 14),
(63, 'cadastro', 14),
(64, 'save', 14),
(65, 'editar', 14),
(66, 'excluir', 14),
(67, 'index', 15),
(68, 'cadastro', 15),
(69, 'save', 15),
(70, 'editar', 15),
(71, 'excluir', 15),
(72, 'visualizar', 2),
(73, 'load_action', 14),
(74, 'save_permission', 13),
(78, 'index', 16),
(79, 'cadastro', 16),
(80, 'save', 16),
(81, 'editar', 16),
(82, 'excluir', 16),
(83, 'index', 17),
(84, 'cadastro', 17),
(85, 'save', 17),
(86, 'detalhes', 17),
(87, 'boleto', 17),
(88, 'index', 18),
(89, 'save_empresa', 1),
(90, 'save_empresa_endereco', 1),
(91, 'save_empresa_contatos', 1),
(92, 'visualizar', 16),
(93, 'diretorios', 10),
(94, 'cliente', 17),
(95, 'enviar', 17),
(96, 'usuarios', 5),
(97, 'save_usuarios', 5),
(98, 'index', 19),
(99, 'visualizar', 19),
(100, 'send', 18),
(101, 'index', 20),
(102, 'quota', 20),
(103, 'save_quota', 20),
(104, 'exibicao', 20),
(105, 'save_exibicao', 20),
(106, 'safe_delete', 2),
(107, 'excluir', 2),
(108, 'permissoes', 5),
(109, 'folder_permissions', 5),
(110, 'save_folder_permissions', 5),
(111, 'adicional', 20),
(112, 'save_adicional', 20),
(114, 'switch_theme', 20),
(115, 'set_sidebar', 20),
(116, 'save_label', 18),
(117, 'contrato', 20),
(118, 'save_contrato', 20),
(119, 'ftp', 20),
(120, 'save_ftp', 20),
(122, 'home', 20),
(123, 'save_home', 20),
(124, 'armazenamento', 20),
(125, 'save_armazenamento', 20),
(126, 'excluir_armazenamento', 20),
(127, 'editar', 2),
(128, 'novo_diretorio', 10),
(129, 'save_diretorio', 10),
(130, 'excluir_diretorio', 10),
(131, 'banco', 20),
(132, 'save_banco', 20),
(133, 'executavel', 20),
(134, 'save_executavel', 20),
(135, 'switch_background', 20),
(136, 'confirmar', 2),
(138, 'confirma_exclusao', 12),
(139, 'index', 21),
(140, 'cadastro', 21),
(141, 'editar', 21),
(142, 'save', 21),
(143, 'excluir', 21),
(144, 'espaco', 20),
(145, 'save_storage', 20),
(146, 'quota_edit', 20),
(147, 'quota_user', 20),
(148, 'meu_contrato', 20);

-- --------------------------------------------------------

--
-- Estrutura para tabela `banco`
--

CREATE TABLE IF NOT EXISTS `banco` (
  `bank_id` int(11) NOT NULL,
  `bank_alias` varchar(100) DEFAULT NULL,
  `bank_nome` varchar(30) DEFAULT NULL,
  `bank_codigo` int(11) DEFAULT NULL,
  `bank_conta` varchar(10) DEFAULT NULL,
  `bank_cdigito` int(11) DEFAULT NULL,
  `bank_acodigo` int(11) DEFAULT NULL,
  `bank_adigito` int(11) DEFAULT NULL,
  `bank_carteira` varchar(8) DEFAULT NULL,
  `bank_nosso_ini` varchar(8) DEFAULT NULL,
  `bank_nosso_fim` varchar(8) DEFAULT NULL,
  `bank_nosso_atual` varchar(8) DEFAULT NULL,
  `bank_contrato` varchar(10) DEFAULT NULL,
  `bank_responsavel` varchar(100) DEFAULT NULL,
  `bank_adesc` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `banco`
--

INSERT INTO `banco` (`bank_id`, `bank_alias`, `bank_nome`, `bank_codigo`, `bank_conta`, `bank_cdigito`, `bank_acodigo`, `bank_adigito`, `bank_carteira`, `bank_nosso_ini`, `bank_nosso_fim`, `bank_nosso_atual`, `bank_contrato`, `bank_responsavel`, `bank_adesc`) VALUES
(1, 'Conta de cobrança', 'Itaú', 341, '03344', 4, 7778, NULL, '103', '12345678', '99999999', '12345682', '678765456', 'Maicon Nicodemo', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `client_id` int(11) NOT NULL,
  `client_user_id` int(11) DEFAULT NULL,
  `client_empresa_id` int(11) DEFAULT NULL,
  `client_volume` varchar(60) NOT NULL,
  `client_key` varchar(30) NOT NULL,
  `client_local` tinyint(1) NOT NULL DEFAULT '0',
  `client_plan_id` int(11) NOT NULL,
  `client_data_vencimento` int(2) NOT NULL,
  `server_id` int(11) DEFAULT NULL,
  `quota_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `ftp_host` varchar(255) DEFAULT NULL,
  `ftp_user` varchar(45) DEFAULT NULL,
  `ftp_password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `cliente`
--

INSERT INTO `cliente` (`client_id`, `client_user_id`, `client_empresa_id`, `client_volume`, `client_key`, `client_local`, `client_plan_id`, `client_data_vencimento`, `server_id`, `quota_enabled`, `ftp_host`, `ftp_user`, `ftp_password`) VALUES
(1, NULL, 1, 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', 'EuCWjVbmOGEWeQ99kuQ2WUikxnQCm8', 1, 1, 15, 1, 0, NULL, NULL, NULL),
(2, NULL, 2, 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', 'zrk7Gs03jcRidFNC9Zk3Xnamd8erR3', 1, 1, 15, 1, 1, NULL, NULL, NULL),
(3, NULL, 3, 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', 'eUUA4cxh0QjLqoCDn1gnetcPrzwZEu', 1, 2, 15, 2, 1, NULL, NULL, NULL),
(4, NULL, 4, 'DocmaisDfbJESXZue8zfZ1j3i4tpIKmjUdBV4mT', '3Pt4MzTerhqTIxfoAE2Ah8Js9ho9pB', 1, 1, 15, 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `compania`
--

CREATE TABLE IF NOT EXISTS `compania` (
  `company_id` int(11) NOT NULL,
  `company_razao` varchar(45) DEFAULT NULL,
  `company_fantasia` varchar(45) DEFAULT NULL,
  `company_cnpj` varchar(20) DEFAULT NULL,
  `company_ie` varchar(20) DEFAULT NULL,
  `company_endereco` varchar(45) DEFAULT NULL,
  `company_cep` varchar(9) DEFAULT NULL,
  `company_bairro` varchar(60) DEFAULT NULL,
  `company_cidade` varchar(155) DEFAULT NULL,
  `company_estado` varchar(2) DEFAULT NULL,
  `company_complemento` varchar(45) DEFAULT NULL,
  `company_email` varchar(155) DEFAULT NULL,
  `company_telefone` varchar(16) DEFAULT NULL,
  `company_celular` varchar(16) DEFAULT NULL,
  `modelo_contrato` text,
  `home_title` varchar(80) NOT NULL,
  `home_subtitle` varchar(255) NOT NULL,
  `exe_path` varchar(255) DEFAULT NULL,
  `exe_instructions` text,
  `linux_path` varchar(255) DEFAULT NULL,
  `macos_path` varchar(255) DEFAULT NULL,
  `storage_size` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `compania`
--

INSERT INTO `compania` (`company_id`, `company_razao`, `company_fantasia`, `company_cnpj`, `company_ie`, `company_endereco`, `company_cep`, `company_bairro`, `company_cidade`, `company_estado`, `company_complemento`, `company_email`, `company_telefone`, `company_celular`, `modelo_contrato`, `home_title`, `home_subtitle`, `exe_path`, `exe_instructions`, `linux_path`, `macos_path`, `storage_size`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(00)  0000-0000', '(xx)  00000-0000', '<h2><a name="partes"></a><strong>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</strong></h2>\n\n<p><strong>A. PRE&Acirc;MBULO</strong></p>\n\n<p><strong>A.1. PARTES</strong></p>\n\n<p><strong>A.1.1.CONTRATANTE:</strong></p>\n\n<p><strong>Raz&atilde;o Social CPF/CNPJ: {empresa_cnpj} RG/I.E.: {empresa_ie} Endere&ccedil;o:{endereco_logradouro}</strong>, {endereco_num}&nbsp;<strong>CEP: {endereco_cep} Cidade:{endereco_cidade} Estado:&nbsp;Telefone:</strong>6548&nbsp;<strong>Respons&aacute;vel:</strong>&nbsp;<strong>CPF:</strong>&nbsp;<strong>Endere&ccedil;o de e-mail:</strong>&nbsp;<strong>Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;</strong>{data_adesao}&nbsp;<strong>Espa&ccedil;o de armazenamento.</strong></p>\n\n<p><strong>A.1.2.CONTRATADA:</strong>&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280Cidade/Estado: Mar&iacute;lia - SP&nbsp;Telefone: (14) 3221-9313Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:contato@centraldenotas.com.br&nbsp;Comer.:comercial@centraldenotas.com.br.</p>\n\n<p><strong>1.DO OBJETO</strong></p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p><strong>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO</strong></p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de Arquivo), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p><strong>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</strong></p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.net</p>\n\n<p><strong>4. CONDI&Ccedil;&Otilde;ES DE USO</strong></p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p><strong>5. CONTA E SENHA</strong></p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.</p>\n\n<p><strong>6. BACKUP DOS DADOS</strong></p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.</p>\n\n<p><strong>7. DOS SERVIDORES DE USO COMPARTILHADO</strong></p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p><strong>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</strong></p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p><strong>9. DA RESCIS&Atilde;O CONTRATUAL</strong></p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p><strong>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</strong></p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p><strong>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</strong></p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>Mar&iacute;lia,&nbsp;{data_adesao}.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas commodo sem sed erat posuere, non dictum justo tristique. Nullam dapibus sagittis velit eu pellentesque. Praesent tincidunt laoreet eros consectetur tincidunt. Phasellus lorem arcu, varius ', '', 'Texto do Contrato', NULL, NULL, 1099511627776);

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `conf_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `filemanager_view` int(11) NOT NULL DEFAULT '0',
  `theme` varchar(20) DEFAULT 'blue',
  `sidebar_position` varchar(20) DEFAULT NULL,
  `background` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `configuration`
--

INSERT INTO `configuration` (`conf_id`, `user_id`, `filemanager_view`, `theme`, `sidebar_position`, `background`) VALUES
(1, 12, 0, 'blue', NULL, '0'),
(2, 16, 0, 'blue', NULL, NULL),
(3, 17, 0, 'blue', NULL, '0'),
(4, 18, 0, 'blue', NULL, NULL),
(5, 19, 0, 'blue', NULL, NULL),
(6, 20, 0, 'blue', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `contato_id` int(11) NOT NULL,
  `contato_valor` varchar(45) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `contato_principal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`contato_id`, `contato_valor`, `tipo_id`, `contato_principal`) VALUES
(3, 'vagnerleitte@outlook.com', 2, 1),
(4, '(23) 94829-4820', 1, 1),
(5, 'teste@ts.com', 2, 1),
(6, '(17) 3298-7987', 1, 1),
(7, 'maicon_digitec@hotmail.com', 2, 1),
(8, '(14) 3221-9313', 1, 1),
(9, 'contato@centraldenotas.com.br', 2, 1),
(10, '(14) 3221-9313', 1, 1),
(11, 'mail@centraldenotas.com.br', 2, 1),
(12, '(14) 3221-0223', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato_tipo`
--

CREATE TABLE IF NOT EXISTS `contato_tipo` (
  `tipo_id` int(11) NOT NULL,
  `tipo_desc` varchar(45) NOT NULL,
  `tipo_editabel` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `contato_tipo`
--

INSERT INTO `contato_tipo` (`tipo_id`, `tipo_desc`, `tipo_editabel`) VALUES
(1, 'Telefone', 0),
(2, 'Email', 0),
(3, 'Skype', 0),
(4, 'Website', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `contrato_id` int(11) NOT NULL,
  `contrato_valor` decimal(10,2) NOT NULL,
  `contrato_adesao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `contrato_ativo` tinyint(1) NOT NULL DEFAULT '0',
  `contrato_cliente` int(11) NOT NULL,
  `content` text,
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  `texto` text
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `contrato`
--

INSERT INTO `contrato` (`contrato_id`, `contrato_valor`, `contrato_adesao`, `contrato_ativo`, `contrato_cliente`, `content`, `accepted`, `texto`) VALUES
(1, '10.00', '2015-11-10 01:38:03', 1, 1, NULL, 1, '</a> <strong>A.1. PARTES</strong></p>\n\n<p><strong>A.1.1.?CONTRATANTE:</strong></p>\n\n<p><strong>Razão Social CPF/CNPJ: 00000000000000 RG/I.E.: ISENTO Endereço:Rua Euclides da Cunha</strong>, 83 <strong>CEP: 17602-490 Cidade:Marília Estado: Telefone:</strong>6548 <strong>Responsável:</strong> <strong>CPF:</strong> <strong>Endereço de e-mail:</strong> <strong>Data Solicitação do serviço: </strong>10 de novembro de 2015 <strong>Espaço de armazenamento / quantidade?aproximada de XML.?</strong></p>\n\n<p><strong>A.1.2.CONTRATADA:</strong> CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endereço: Rua Izaltino Candido Pereira, 280?Cidade/Estado: Marília - SP:?Telefone: (14) 3221-9313?Horário de atendimento: Das 08:00 às 18:00 horas de segunda a sexta e das 08:00 às 12:00 horas de sábado, exceto feriados nacionais. Endereços de e-mail Sup. Téc.:?contato@centraldenotas.com.br??Comer.:comercial@centraldenotas.com.br??</p>\n\n<p><strong>1.DO OBJETO</strong></p>\n\n<p>1.1. O presente Contrato tem por objeto a prestação pela CONTRATADA do serviço conforme opção abaixo de armazenamento digital de dados online à CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 – Plano contratado conforme opção descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a operação é que o computador esteja conectado à internet.</p>\n\n<p>1.1.3. Espaço de armazenamento digital e acesso: É o espaço para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados à internet, cujo acesso se dará através de conta personalizada e senha de conhecimento exclusivo do usuário da CONTRATANTE.???</p>\n\n<p>1.1.4 As opções de espaço de armazenamento digital serão liberados mediante o plano contratado, sendo agregado mais espaços conforme a necessidade e, a mudança de plano ocorrerá quando o cliente ultrapassar o espaço contratado (plano).??</p>\n\n<p><strong>2.DA REMUNERAÇÃO E DA PERIODICIDADE DE PAGAMENTO??</strong></p>\n\n<p>2.1. Da remuneração - A CONTRATANTE pagará à CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da adesão ao serviço prestado e o valor do plano com vencimento fixado para todo dia 15 de cada mês, prorrogando para o primeiro dia útil seguinte quando vencimento constar em sábados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contratação (somente para a contratação de Armazenamento de XML), será respeitada a tabela de planos, considerando a quantidade de notas a armazenar.?</p>\n\n<p>2.3. O presente contrato é celebrado pelo prazo indeterminado, desde que não haja renúncia por qualquer das partes.</p>\n\n<p>2.4. O valor da prestação do serviço contratado será reajustado a cada Ano no mês de Maio a contar da data da celebração do contrato inicial, de acordo com a variação do IGPM/FGV.</p>\n\n<p><strong>3. REGISTRO E ALTERAÇÕES CONTRATUAIS</strong></p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto às cláusulas e condições que regem o presente termo de contratação eletrônica, no qual a confirmação de contratação e a concordância dos termos deste contrato são realizadas através dos pagamentos da remuneração básica, bem como para conhecimento público dos termos aqui presentes, este contrato padrão e suas alterações subsequentes serão registrados em Cartório de Títulos e Documentos de Marília - SP. O contrato em vigor e a data do mesmo permanecerão disponíveis para consultas no site www.centraldenotas.com.br.</p>\n\n<p>???????????????????????????????????????????</p>\n\n<p><strong>4. CONDIÇÕES DE USO??</strong></p>\n\n<p>??</p>\n\n<p>4.1. Para utilização do SERVIÇO, a CONTRATANTE é responsável por prover todos os dados e equipamentos e serviços para a conexão de seu computador à internet, sendo a única responsável também por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE é a única responsável pelo conteúdo de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A violação de qualquer legislação devido ao uso dos recursos dos serviços que redundem em ação judicial ou administrativa de qualquer espécie seja civil, criminal, tributária ou de qualquer outra espécie será de ônus exclusivamente da CONTRATANTE, a qual também responderá solidariamente por aquelas em que a CONTRATADA for chamada por infração cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do serviço será suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o serviço para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o serviço para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informações, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVIÇO não poderá ser mantido quando houver qualquer utilização do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do serviço pelos demais usuários do serviço.</p>\n\n<p>4.3.4. A CONTRATANTE não poderá acessar ou tentar acessar qualquer serviço ou conta de outro usuário para os quais não tenha permissão de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE deverá notificar imediatamente a CONTRATADA de qualquer alteração dos dados cadastrais utilizados na solicitação inicial do serviço, incluindo, endereço, números de telefone e endereços de e-mail, etc.?</p>\n\n<p><strong>5. CONTA E SENHA</strong></p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usuário e a senha para acesso aos serviços. A CONTRATANTE é a única responsável pela senha, sendo que a CONTRATADA não tem acesso à senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE também é a única responsável pelas consequências resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhará todos os seus esforços para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a proteção de acesso a sistemas baseadas em usuário e senha não são totalmente impenetráveis. Assim, a CONTRATANTE reconhece que é possível um usuário não autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e segurança, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por números, letras de caixa baixa, letras de caixa alta, caracteres que não sejam alfanuméricos e cujos caracteres não tenham nenhuma relação com os dados do usuário e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos não fiscaliza, controla ou edita qualquer conteúdo armazenado pela CONTRATANTE ou de qualquer dos seus usuários autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou ilícito de material, a CONTRATADA reserva o direito de examinar o conteúdo dos dados armazenados na conta.?</p>\n\n<p><strong>6. BACKUP DOS DADOS</strong></p>\n\n<p>?</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as cópias de segurança para a área de armazenamento, para que a CONTRATANTE, quando necessário, possa proceder a restauração dos dados armazenados. No entanto, para ter um backup confiável, o elemento mais importante neste processo é o usuário da CONTRATANTE. Espera-se do usuário da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de serviço não serão responsáveis pelas paralisações do serviço e nem serão responsáveis por quaisquer danos, reclamações ou custos de qualquer espécie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem responsável por qualquer reclamação de terceiros contra a CONTRATANTE.?</p>\n\n<p><strong>7. DOS SERVIDORES DE USO COMPARTILHADO</strong></p>\n\n<p>7.1. Para a prestação dos serviços, a CONTRATADA fará uso de servidores que serão compartilhados por vários usuários e que não serão de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manutenção dos programas instalados, com a alteração da configuração dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p><strong>8. SUSPENSÃO DOS SERVIÇOS A PEDIDO DE AUTORIDADES</strong></p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicitação formulada por qualquer autoridade pública judicial, de proteção ao consumidor, economia popular, infância e juventude ou de qualquer outro interesse público para acesso ao conteúdo dos dados armazenados por força deste contrato, a CONTRATADA irá fornecer à autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notificação prévia.?</p>\n\n<p><strong>9. DA RESCISÃO CONTRATUAL</strong></p>\n\n<p>9.1. O presente Contrato poderá ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunicação por escrito à outra parte, com antecedência mínima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensação ou indenização às partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obrigação estabelecida neste contrato, sendo que a parte prejudicada deverá primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimplência seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notificação;</p>\n\n<p>9.3. O não pagamento pela CONTRATANTE de qualquer fatura por um período superior a 60 dias depois do respectivo vencimento acarretará na rescisão de pleno direito deste contrato, independente de aviso ou notificação.?</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-á à multa moratória de 2% (dois por cento) e juros de 1% (um por cento) ao mês ou fração do mês, ambos aplicados sobre o valor em atraso corrigidos conforme o índice de correção aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p><strong>10. DA NÃO PRESERVAÇÃO DOS DADOS</strong></p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados serão excluídos automaticamente no momento do cancelamento dos serviços, após o cancelamento, será salvo em outro local de armazenamento e ficará disponível pelo prazo de 5 anos, foco deste serviço, a possibilidade de se recuperar os dados, se sujeitará ao pagamento de uma taxa, e depois disso será excluído em definitivo.</p>\n\n<p><strong>11. DAS DISPOSIÇÕES FINAIS E FORO</strong></p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao domínio objeto deste instrumento havidos entre as partes anteriores à data da solicitação registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, São Paulo, com renúncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais dúvidas ou controvérsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>?</p>\n\n<p>Marília, 10 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p> </p>\n " readonly><p><strong>Contrato de Prestação de Serviços de Armazenamento Digital de Dados Online</strong></p>\n\n<p><strong>A. PREÂMBULO</strong></p>\n\n<p><a name="partes"></a> <strong>A.1. PARTES</strong></p>\n\n<p><strong>A.1.1.?CONTRATANTE:</strong></p>\n\n<p><strong>Razão Social CPF/CNPJ: 00000000000000 RG/I.E.: ISENTO Endereço:Rua Euclides da Cunha</strong>, 83 <strong>CEP: 17602-490 Cidade:Marília Estado: Telefone:</strong>6548 <strong>Responsável:</strong> <strong>CPF:</strong> <strong>Endereço de e-mail:</strong> <strong>Data Solicitação do serviço: </strong>10 de novembro de 2015 <strong>Espaço de armazenamento / quantidade?aproximada de XML.?</strong></p>\n\n<p><strong>A.1.2.CONTRATADA:</strong> CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endereço: Rua Izaltino Candido Pereira, 280?Cidade/Estado: Marília - SP:?Telefone: (14) 3221-9313?Horário de atendimento: Das 08:00 às 18:00 horas de segunda a sexta e das 08:00 às 12:00 horas de sábado, exceto feriados nacionais. Endereços de e-mail Sup. Téc.:?contato@centraldenotas.com.br??Comer.:comercial@centraldenotas.com.br??</p>\n\n<p><strong>1.DO OBJETO</strong></p>\n\n<p>1.1. O presente Contrato tem por objeto a prestação pela CONTRATADA do serviço conforme opção abaixo de armazenamento digital de dados online à CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 – Plano contratado conforme opção descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a operação é que o computador esteja conectado à internet.</p>\n\n<p>1.1.3. Espaço de armazenamento digital e acesso: É o espaço para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados à internet, cujo acesso se dará através de conta personalizada e senha de conhecimento exclusivo do usuário da CONTRATANTE.???</p>\n\n<p>1.1.4 As opções de espaço de armazenamento digital serão liberados mediante o plano contratado, sendo agregado mais espaços conforme a necessidade e, a mudança de plano ocorrerá quando o cliente ultrapassar o espaço contratado (plano).??</p>\n\n<p><strong>2.DA REMUNERAÇÃO E DA PERIODICIDADE DE PAGAMENTO??</strong></p>\n\n<p>2.1. Da remuneração - A CONTRATANTE pagará à CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da adesão ao serviço prestado e o valor do plano com vencimento fixado para todo dia 15 de cada mês, prorrogando para o primeiro dia útil seguinte quando vencimento constar em sábados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contratação (somente para a contratação de Armazenamento de XML), será respeitada a tabela de planos, considerando a quantidade de notas a armazenar.?</p>\n\n<p>2.3. O presente contrato é celebrado pelo prazo indeterminado, desde que não haja renúncia por qualquer das partes.</p>\n\n<p>2.4. O valor da prestação do serviço contratado será reajustado a cada Ano no mês de Maio a contar da data da celebração do contrato inicial, de acordo com a variação do IGPM/FGV.</p>\n\n<p><strong>3. REGISTRO E ALTERAÇÕES CONTRATUAIS</strong></p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto às cláusulas e condições que regem o presente termo de contratação eletrônica, no qual a confirmação de contratação e a concordância dos termos deste contrato são realizadas através dos pagamentos da remuneração básica, bem como para conhecimento público dos termos aqui presentes, este contrato padrão e suas alterações subsequentes serão registrados em Cartório de Títulos e Documentos de Marília - SP. O contrato em vigor e a data do mesmo permanecerão disponíveis para consultas no site www.centraldenotas.com.br.</p>\n\n<p>???????????????????????????????????????????</p>\n\n<p><strong>4. CONDIÇÕES DE USO??</strong></p>\n\n<p>??</p>\n\n<p>4.1. Para utilização do SERVIÇO, a CONTRATANTE é responsável por prover todos os dados e equipamentos e serviços para a conexão de seu computador à internet, sendo a única responsável também por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE é a única responsável pelo conteúdo de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A violação de qualquer legislação devido ao uso dos recursos dos serviços que redundem em ação judicial ou administrativa de qualquer espécie seja civil, criminal, tributária ou de qualquer outra espécie será de ônus exclusivamente da CONTRATANTE, a qual também responderá solidariamente por aquelas em que a CONTRATADA for chamada por infração cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do serviço será suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o serviço para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o serviço para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informações, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVIÇO não poderá ser mantido quando houver qualquer utilização do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do serviço pelos demais usuários do serviço.</p>\n\n<p>4.3.4. A CONTRATANTE não poderá acessar ou tentar acessar qualquer serviço ou conta de outro usuário para os quais não tenha permissão de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE deverá notificar imediatamente a CONTRATADA de qualquer alteração dos dados cadastrais utilizados na solicitação inicial do serviço, incluindo, endereço, números de telefone e endereços de e-mail, etc.?</p>\n\n<p><strong>5. CONTA E SENHA</strong></p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usuário e a senha para acesso aos serviços. A CONTRATANTE é a única responsável pela senha, sendo que a CONTRATADA não tem acesso à senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE também é a única responsável pelas consequências resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhará todos os seus esforços para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a proteção de acesso a sistemas baseadas em usuário e senha não são totalmente impenetráveis. Assim, a CONTRATANTE reconhece que é possível um usuário não autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e segurança, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por números, letras de caixa baixa, letras de caixa alta, caracteres que não sejam alfanuméricos e cujos caracteres não tenham nenhuma relação com os dados do usuário e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos não fiscaliza, controla ou edita qualquer conteúdo armazenado pela CONTRATANTE ou de qualquer dos seus usuários autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou ilícito de material, a CONTRATADA reserva o direito de examinar o conteúdo dos dados armazenados na conta.?</p>\n\n<p><strong>6. BACKUP DOS DADOS</strong></p>\n\n<p>?</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as cópias de segurança para a área de armazenamento, para que a CONTRATANTE, quando necessário, possa proceder a restauração dos dados armazenados. No entanto, para ter um backup confiável, o elemento mais importante neste processo é o usuário da CONTRATANTE. Espera-se do usuário da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de serviço não serão responsáveis pelas paralisações do serviço e nem serão responsáveis por quaisquer danos, reclamações ou custos de qualquer espécie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem responsável por qualquer reclamação de terceiros contra a CONTRATANTE.?</p>\n\n<p><strong>7. DOS SERVIDORES DE USO COMPARTILHADO</strong></p>\n\n<p>7.1. Para a prestação dos serviços, a CONTRATADA fará uso de servidores que serão compartilhados por vários usuários e que não serão de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manutenção dos programas instalados, com a alteração da configuração dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p><strong>8. SUSPENSÃO DOS SERVIÇOS A PEDIDO DE AUTORIDADES</strong></p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicitação formulada por qualquer autoridade pública judicial, de proteção ao consumidor, economia popular, infância e juventude ou de qualquer outro interesse público para acesso ao conteúdo dos dados armazenados por força deste contrato, a CONTRATADA irá fornecer à autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notificação prévia.?</p>\n\n<p><strong>9. DA RESCISÃO CONTRATUAL</strong></p>\n\n<p>9.1. O presente Contrato poderá ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunicação por escrito à outra parte, com antecedência mínima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensação ou indenização às partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obrigação estabelecida neste contrato, sendo que a parte prejudicada deverá primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimplência seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notificação;</p>\n\n<p>9.3. O não pagamento pela CONTRATANTE de qualquer fatura por um período superior a 60 dias depois do respectivo vencimento acarretará na rescisão de pleno direito deste contrato, independente de aviso ou notificação.?</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-á à multa moratória de 2% (dois por cento) e juros de 1% (um por cento) ao mês ou fração do mês, ambos aplicados sobre o valor em atraso corrigidos conforme o índice de correção aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p><strong>10. DA NÃO PRESERVAÇÃO DOS DADOS</strong></p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados serão excluídos automaticamente no momento do cancelamento dos serviços, após o cancelamento, será salvo em outro local de armazenamento e ficará disponível pelo prazo de 5 anos, foco deste serviço, a possibilidade de se recuperar os dados, se sujeitará ao pagamento de uma taxa, e depois disso será excluído em definitivo.</p>\n\n<p><strong>11. DAS DISPOSIÇÕES FINAIS E FORO</strong></p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao domínio objeto deste instrumento havidos entre as partes anteriores à data da solicitação registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, São Paulo, com renúncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais dúvidas ou controvérsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>?</p>\n\n<p>Marília, 10 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p> </p>\n\n\n                        <div class="well">\n                            \n                            <a id="termos" name="plano"></a>\n                            <h3>Plano: BASIC <span class="pull-right">R$ 10.00</span></h3>\n                            \n                        </div>\n\n                        <h3><strong>Serviços Contratados</strong></h3>\n                        <a name="services" id="services"></a>\n                        <ul class="list-group">\n                            \n                            <li class="list-group-item">Gerenciador de Arquivos</li>\n                            \n                            <li class="list-group-item">Armazenamento Externo	</li>\n                            \n                            <li class="list-group-item">1 GB</li>\n                            \n                        </ul>\n                       ');
INSERT INTO `contrato` (`contrato_id`, `contrato_valor`, `contrato_adesao`, `contrato_ativo`, `contrato_cliente`, `content`, `accepted`, `texto`) VALUES
(2, '10.00', '2015-11-10 05:39:12', 1, 2, NULL, 1, '<p>A.1. PARTES</p>\n\n<p>&nbsp;</p>\n\n<p>A.1.1.?CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: 32034333896 RG/I.E.: ISENTO Endere&ccedil;o:Segismundo nunes de oliveiro, 140&nbsp;CEP: 17512-52 Cidade:Marilia Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;10 de novembro de 2015&nbsp;Espa&ccedil;o de armazenamento / quantidade?aproximada de XML.?</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280?Cidade/Estado: Mar&iacute;lia - SP:?Telefone: (14) 3221-9313?Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:?contato@centraldenotas.com.br??Comer.:comercial@centraldenotas.com.br??</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.???</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO??</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de XML), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.?</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.com.br.</p>\n\n<p>???????????????????????????????????????????</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO??</p>\n\n<p>??</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.?</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>?</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.?</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.?</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>?</p>\n\n<p>Mar&iacute;lia,&nbsp;10 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>&quot; readonly&gt;</p>\n\n<p>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</p>\n\n<p>A. PRE&Acirc;MBULO</p>\n\n<p>A.1. PARTES</p>\n\n<p>A.1.1.?CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: 32034333896 RG/I.E.: ISENTO Endere&ccedil;o:Segismundo nunes de oliveiro, 140&nbsp;CEP: 17512-52 Cidade:Marilia Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;10 de novembro de 2015&nbsp;Espa&ccedil;o de armazenamento / quantidade?aproximada de XML.?</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280?Cidade/Estado: Mar&iacute;lia - SP:?Telefone: (14) 3221-9313?Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:?contato@centraldenotas.com.br??Comer.:comercial@centraldenotas.com.br??</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.???</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO??</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de XML), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.?</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.com.br.</p>\n\n<p>???????????????????????????????????????????</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO??</p>\n\n<p>??</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.?</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>?</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.?</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.?</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>?</p>\n\n<p>Mar&iacute;lia,&nbsp;10 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>Plano: BASIC R$ 10.00</p>\n\n<p>Servi&ccedil;os Contratados</p>\n\n<p>&nbsp;</p>\n\n<p>Gerenciador de Arquivos</p>\n\n<p>Armazenamento Externo</p>\n\n<p>1 GB</p>\n');
INSERT INTO `contrato` (`contrato_id`, `contrato_valor`, `contrato_adesao`, `contrato_ativo`, `contrato_cliente`, `content`, `accepted`, `texto`) VALUES
(3, '10.00', '2015-11-10 09:56:43', 1, 3, NULL, 1, '<p>A.1. PARTES</p>\n\n<p>&nbsp;</p>\n\n<p>A.1.1.?CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: 32034555678 RG/I.E.: ISENTO Endere&ccedil;o:teste teste, 345&nbsp;CEP: 17520-230 Cidade:marilia Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;10 de novembro de 2015&nbsp;Espa&ccedil;o de armazenamento / quantidade?aproximada de XML.?</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280?Cidade/Estado: Mar&iacute;lia - SP:?Telefone: (14) 3221-9313?Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:?contato@centraldenotas.com.br??Comer.:comercial@centraldenotas.com.br??</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.???</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO??</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de XML), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.?</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.com.br.</p>\n\n<p>???????????????????????????????????????????</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO??</p>\n\n<p>??</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.?</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>?</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.?</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.?</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>?</p>\n\n<p>Mar&iacute;lia,&nbsp;10 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>&quot; readonly&gt;</p>\n\n<p>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</p>\n\n<p>A. PRE&Acirc;MBULO</p>\n\n<p>A.1. PARTES</p>\n\n<p>A.1.1.?CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: 32034555678 RG/I.E.: ISENTO Endere&ccedil;o:teste teste, 345&nbsp;CEP: 17520-230 Cidade:marilia Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;10 de novembro de 2015&nbsp;Espa&ccedil;o de armazenamento / quantidade?aproximada de XML.?</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280?Cidade/Estado: Mar&iacute;lia - SP:?Telefone: (14) 3221-9313?Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:?contato@centraldenotas.com.br??Comer.:comercial@centraldenotas.com.br??</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.???</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO??</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de XML), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.?</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.com.br.</p>\n\n<p>???????????????????????????????????????????</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO??</p>\n\n<p>??</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.?</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>?</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.?</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.?</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>?</p>\n\n<p>Mar&iacute;lia,&nbsp;10 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>Plano: Avan&ccedil;aado R$ 10.00</p>\n\n<p>Servi&ccedil;os Contratados</p>\n\n<p>&nbsp;</p>\n\n<p>Gerenciador de Arquivos</p>\n\n<p>Armazenamento Externo</p>\n');
INSERT INTO `contrato` (`contrato_id`, `contrato_valor`, `contrato_adesao`, `contrato_ativo`, `contrato_cliente`, `content`, `accepted`, `texto`) VALUES
(4, '10.00', '2015-11-23 12:12:21', 1, 4, NULL, 1, '<p>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</p>\n\n<p>A. PRE&Acirc;MBULO</p>\n\n<p>A.1. PARTES</p>\n\n<p>A.1.1.CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: 05285633192 RG/I.E.: ISENTO Endere&ccedil;o:almeida campos, 145&nbsp;CEP: 17503-220 Cidade:Marilia Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;23 de novembro de 2015&nbsp;Espa&ccedil;o de armazenamento.</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280Cidade/Estado: Mar&iacute;lia - SP&nbsp;Telefone: (14) 3221-9313Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:contato@centraldenotas.com.br&nbsp;Comer.:comercial@centraldenotas.com.br.</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de Arquivo), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.net</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>Mar&iacute;lia,&nbsp;23 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>&quot; readonly&gt;</p>\n\n<p>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</p>\n\n<p>A. PRE&Acirc;MBULO</p>\n\n<p>A.1. PARTES</p>\n\n<p>A.1.1.CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: 05285633192 RG/I.E.: ISENTO Endere&ccedil;o:almeida campos, 145&nbsp;CEP: 17503-220 Cidade:Marilia Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;23 de novembro de 2015&nbsp;Espa&ccedil;o de armazenamento.</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280Cidade/Estado: Mar&iacute;lia - SP&nbsp;Telefone: (14) 3221-9313Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:contato@centraldenotas.com.br&nbsp;Comer.:comercial@centraldenotas.com.br.</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de Arquivo), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.net</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>Mar&iacute;lia,&nbsp;23 de novembro de 2015.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>Plano: BASIC R$ 10.00</p>\n\n<p>Servi&ccedil;os Contratados</p>\n\n<p>&nbsp;</p>\n\n<p>Gerenciador de Arquivos</p>\n\n<p>Armazenamento Externo</p>\n\n<p>1 GB</p>\n');
INSERT INTO `contrato` (`contrato_id`, `contrato_valor`, `contrato_adesao`, `contrato_ativo`, `contrato_cliente`, `content`, `accepted`, `texto`) VALUES
(5, '10.00', '2015-11-23 12:15:51', 1, 0, NULL, 1, '<p>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</p>\n\n<p>A. PRE&Acirc;MBULO</p>\n\n<p>A.1. PARTES</p>\n\n<p>A.1.1.CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: {empresa_cnpj} RG/I.E.: {empresa_ie} Endere&ccedil;o:{endereco_logradouro}, {endereco_num}&nbsp;CEP: {endereco_cep} Cidade:{endereco_cidade} Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;{data_adesao}&nbsp;Espa&ccedil;o de armazenamento.</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280Cidade/Estado: Mar&iacute;lia - SP&nbsp;Telefone: (14) 3221-9313Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:contato@centraldenotas.com.br&nbsp;Comer.:comercial@centraldenotas.com.br.</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de Arquivo), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.net</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>Mar&iacute;lia,&nbsp;{data_adesao}.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>&quot; readonly&gt;</p>\n\n<p>Contrato de Presta&ccedil;&atilde;o de Servi&ccedil;os de Armazenamento Digital de Dados Online</p>\n\n<p>A. PRE&Acirc;MBULO</p>\n\n<p>A.1. PARTES</p>\n\n<p>A.1.1.CONTRATANTE:</p>\n\n<p>Raz&atilde;o Social CPF/CNPJ: {empresa_cnpj} RG/I.E.: {empresa_ie} Endere&ccedil;o:{endereco_logradouro}, {endereco_num}&nbsp;CEP: {endereco_cep} Cidade:{endereco_cidade} Estado:&nbsp;Telefone:6548&nbsp;Respons&aacute;vel:&nbsp;CPF:&nbsp;Endere&ccedil;o de e-mail:&nbsp;Data Solicita&ccedil;&atilde;o do servi&ccedil;o:&nbsp;{data_adesao}&nbsp;Espa&ccedil;o de armazenamento.</p>\n\n<p>A.1.2.CONTRATADA:&nbsp;CENTRAL DE NOTAS ARMAZENAMENTO DIGITAL LTDA - ME.?CNPJ: 14.566.416/0001-86?Endere&ccedil;o: Rua Izaltino Candido Pereira, 280Cidade/Estado: Mar&iacute;lia - SP&nbsp;Telefone: (14) 3221-9313Hor&aacute;rio de atendimento: Das 08:00 &agrave;s 18:00 horas de segunda a sexta e das 08:00 &agrave;s 12:00 horas de s&aacute;bado, exceto feriados nacionais. Endere&ccedil;os de e-mail Sup. T&eacute;c.:contato@centraldenotas.com.br&nbsp;Comer.:comercial@centraldenotas.com.br.</p>\n\n<p>1.DO OBJETO</p>\n\n<p>1.1. O presente Contrato tem por objeto a presta&ccedil;&atilde;o pela CONTRATADA do servi&ccedil;o conforme op&ccedil;&atilde;o abaixo de armazenamento digital de dados online &agrave; CONTRATANTE, para a finalidade de armazenar os arquivos e documentos digitais.</p>\n\n<p>1.1.1 &ndash; Plano contratado conforme op&ccedil;&atilde;o descrita no final do documento.</p>\n\n<p>1.1.2. O requisito para a opera&ccedil;&atilde;o &eacute; que o computador esteja conectado &agrave; internet.</p>\n\n<p>1.1.3. Espa&ccedil;o de armazenamento digital e acesso: &Eacute; o espa&ccedil;o para a guarda dos dados em servidores de backup de uso compartilhado, mantidos no Data Center da CONTRATADA e conectados &agrave; internet, cujo acesso se dar&aacute; atrav&eacute;s de conta personalizada e senha de conhecimento exclusivo do usu&aacute;rio da CONTRATANTE.</p>\n\n<p>1.1.4 As op&ccedil;&otilde;es de espa&ccedil;o de armazenamento digital ser&atilde;o liberados mediante o plano contratado, sendo agregado mais espa&ccedil;os conforme a necessidade e, a mudan&ccedil;a de plano ocorrer&aacute; quando o cliente ultrapassar o espa&ccedil;o contratado (plano).??</p>\n\n<p>2.DA REMUNERA&Ccedil;&Atilde;O E DA PERIODICIDADE DE PAGAMENTO</p>\n\n<p>2.1. Da remunera&ccedil;&atilde;o - A CONTRATANTE pagar&aacute; &agrave; CONTRATADA, de acordo o plano ideal e adequado a necessidade do seu movimento, mediante ao pagamento da ades&atilde;o ao servi&ccedil;o prestado e o valor do plano com vencimento fixado para todo dia 15 de cada m&ecirc;s, prorrogando para o primeiro dia &uacute;til seguinte quando vencimento constar em s&aacute;bados, domingos e feriados.</p>\n\n<p>2.2. Caso houver a necessidade de armazenamento de documentos com data anterior a da contrata&ccedil;&atilde;o (somente para a contrata&ccedil;&atilde;o de Armazenamento de Arquivo), ser&aacute; respeitada a tabela de planos, considerando a quantidade de notas a armazenar.</p>\n\n<p>2.3. O presente contrato &eacute; celebrado pelo prazo indeterminado, desde que n&atilde;o haja ren&uacute;ncia por qualquer das partes.</p>\n\n<p>2.4. O valor da presta&ccedil;&atilde;o do servi&ccedil;o contratado ser&aacute; reajustado a cada Ano no m&ecirc;s de Maio a contar da data da celebra&ccedil;&atilde;o do contrato inicial, de acordo com a varia&ccedil;&atilde;o do IGPM/FGV.</p>\n\n<p>3. REGISTRO E ALTERA&Ccedil;&Otilde;ES CONTRATUAIS</p>\n\n<p>3.1. Para o pleno conhecimento da CONTRATANTE e quanto &agrave;s cl&aacute;usulas e condi&ccedil;&otilde;es que regem o presente termo de contrata&ccedil;&atilde;o eletr&ocirc;nica, no qual a confirma&ccedil;&atilde;o de contrata&ccedil;&atilde;o e a concord&acirc;ncia dos termos deste contrato s&atilde;o realizadas atrav&eacute;s dos pagamentos da remunera&ccedil;&atilde;o b&aacute;sica, bem como para conhecimento p&uacute;blico dos termos aqui presentes, este contrato padr&atilde;o e suas altera&ccedil;&otilde;es subsequentes ser&atilde;o registrados em Cart&oacute;rio de T&iacute;tulos e Documentos de Mar&iacute;lia - SP. O contrato em vigor e a data do mesmo permanecer&atilde;o dispon&iacute;veis para consultas no site www.centraldenotas.net</p>\n\n<p>4. CONDI&Ccedil;&Otilde;ES DE USO</p>\n\n<p>4.1. Para utiliza&ccedil;&atilde;o do SERVI&Ccedil;O, a CONTRATANTE &eacute; respons&aacute;vel por prover todos os dados e equipamentos e servi&ccedil;os para a conex&atilde;o de seu computador &agrave; internet, sendo a &uacute;nica respons&aacute;vel tamb&eacute;m por estas despesas.</p>\n\n<p>4.2. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pelo conte&uacute;do de todos os dados armazenados e/ou restaurados, incluindo as tentativas para armazenar ou recuperar dados da sua conta. A viola&ccedil;&atilde;o de qualquer legisla&ccedil;&atilde;o devido ao uso dos recursos dos servi&ccedil;os que redundem em a&ccedil;&atilde;o judicial ou administrativa de qualquer esp&eacute;cie seja civil, criminal, tribut&aacute;ria ou de qualquer outra esp&eacute;cie ser&aacute; de &ocirc;nus exclusivamente da CONTRATANTE, a qual tamb&eacute;m responder&aacute; solidariamente por aquelas em que a CONTRATADA for chamada por infra&ccedil;&atilde;o cometida pela CONTRATANTE ou a elas der causa, resguardado o direito de regresso;</p>\n\n<p>4.3. O uso do servi&ccedil;o ser&aacute; suspenso caso infrinja qualquer dos itens a seguir:</p>\n\n<p>4.3.1. Utilizar o servi&ccedil;o para qualquer fim ilegal.</p>\n\n<p>4.3.2. Utilizar o servi&ccedil;o para armazenar, recuperar, transmitir ou exibir qualquer arquivo, dados, imagens ou programas que contenham informa&ccedil;&otilde;es, fotos ou dados ilegais de qualquer natureza.</p>\n\n<p>4.3.3. O SERVI&Ccedil;O n&atilde;o poder&aacute; ser mantido quando houver qualquer utiliza&ccedil;&atilde;o do mesmo que venha a prejudicar o funcionamento do servidor ou o uso do servi&ccedil;o pelos demais usu&aacute;rios do servi&ccedil;o.</p>\n\n<p>4.3.4. A CONTRATANTE n&atilde;o poder&aacute; acessar ou tentar acessar qualquer servi&ccedil;o ou conta de outro usu&aacute;rio para os quais n&atilde;o tenha permiss&atilde;o de acesso.</p>\n\n<p>4.3.5 A CONTRATANTE dever&aacute; notificar imediatamente a CONTRATADA de qualquer altera&ccedil;&atilde;o dos dados cadastrais utilizados na solicita&ccedil;&atilde;o inicial do servi&ccedil;o, incluindo, endere&ccedil;o, n&uacute;meros de telefone e endere&ccedil;os de e-mail, etc.?</p>\n\n<p>5. CONTA E SENHA</p>\n\n<p>5.1. No cadastramento inicial, a CONTRATANTE cria o usu&aacute;rio e a senha para acesso aos servi&ccedil;os. A CONTRATANTE &eacute; a &uacute;nica respons&aacute;vel pela senha, sendo que a CONTRATADA n&atilde;o tem acesso &agrave; senha pelo fato da mesma se encontrar criptografada. A CONTRATANTE tamb&eacute;m &eacute; a &uacute;nica respons&aacute;vel pelas consequ&ecirc;ncias resultantes da sua falha em manter a confidencialidade das senhas de acesso.</p>\n\n<p>5.2. A CONTRATADA empenhar&aacute; todos os seus esfor&ccedil;os para restringir o acesso aos dados e arquivos armazenados pela CONTRATANTE, entretanto a prote&ccedil;&atilde;o de acesso a sistemas baseadas em usu&aacute;rio e senha n&atilde;o s&atilde;o totalmente impenetr&aacute;veis. Assim, a CONTRATANTE reconhece que &eacute; poss&iacute;vel um usu&aacute;rio n&atilde;o autorizado, tenha acesso para ver, copiar os dados e arquivos que foram armazenados em sua conta. Para aumentar a privacidade e seguran&ccedil;a, a CONTRATADA recomenda que a CONTRATANTE escolha senhas fortes para acesso e criptografia. Para que uma senha seja considerada forte deve possuir pelo menos 06 caracteres e formada por n&uacute;meros, letras de caixa baixa, letras de caixa alta, caracteres que n&atilde;o sejam alfanum&eacute;ricos e cujos caracteres n&atilde;o tenham nenhuma rela&ccedil;&atilde;o com os dados do usu&aacute;rio e da CONTRATANTE.</p>\n\n<p>5.3. A CONTRATADA em seus procedimentos n&atilde;o fiscaliza, controla ou edita qualquer conte&uacute;do armazenado pela CONTRATANTE ou de qualquer dos seus usu&aacute;rios autorizados. No entanto, se a CONTRATADA suspeitar que uma conta esteja sendo utilizada para armazenar e/ou distribuir qualquer tipo ilegal ou il&iacute;cito de material, a CONTRATADA reserva o direito de examinar o conte&uacute;do dos dados armazenados na conta.</p>\n\n<p>6. BACKUP DOS DADOS</p>\n\n<p>6.1. O PROGRAMA pode ser configurado para enviar regularmente as c&oacute;pias de seguran&ccedil;a para a &aacute;rea de armazenamento, para que a CONTRATANTE, quando necess&aacute;rio, possa proceder a restaura&ccedil;&atilde;o dos dados armazenados. No entanto, para ter um backup confi&aacute;vel, o elemento mais importante neste processo &eacute; o usu&aacute;rio da CONTRATANTE. Espera-se do usu&aacute;rio da CONTRATANTE, que ele verifique periodicamente os registros dos backups para assegurar que os processos ocorreram com sucesso e tomar medidas corretivas se houverem falhas.</p>\n\n<p>6.2. A CONTRATADA, seus fornecedores e seus prestadores de servi&ccedil;o n&atilde;o ser&atilde;o respons&aacute;veis pelas paralisa&ccedil;&otilde;es do servi&ccedil;o e nem ser&atilde;o respons&aacute;veis por quaisquer danos, reclama&ccedil;&otilde;es ou custos de qualquer esp&eacute;cie ou quaisquer danos diretos, indiretos ou incidentais ou qualquer lucro cessante ou perda de receita, nem respons&aacute;vel por qualquer reclama&ccedil;&atilde;o de terceiros contra a CONTRATANTE.</p>\n\n<p>7. DOS SERVIDORES DE USO COMPARTILHADO</p>\n\n<p>7.1. Para a presta&ccedil;&atilde;o dos servi&ccedil;os, a CONTRATADA far&aacute; uso de servidores que ser&atilde;o compartilhados por v&aacute;rios usu&aacute;rios e que n&atilde;o ser&atilde;o de uso exclusivo da CONTRATANTE. Para garantir o funcionamento normal dos servidores, a CONTRATANTE autoriza a CONTRATADA a executar as seguintes atividades:</p>\n\n<p>7.2. Realizar a manuten&ccedil;&atilde;o dos programas instalados, com a altera&ccedil;&atilde;o da configura&ccedil;&atilde;o dos servidores, bem como habilitar ou desabilitar comandos ou recursos que prejudiquem o funcionamento normal dos servidores;</p>\n\n<p>8. SUSPENS&Atilde;O DOS SERVI&Ccedil;OS A PEDIDO DE AUTORIDADES</p>\n\n<p>8.1. A CONTRATANTE declara ter conhecimento de que em caso de ordem judicial ou de solicita&ccedil;&atilde;o formulada por qualquer autoridade p&uacute;blica judicial, de prote&ccedil;&atilde;o ao consumidor, economia popular, inf&acirc;ncia e juventude ou de qualquer outro interesse p&uacute;blico para acesso ao conte&uacute;do dos dados armazenados por for&ccedil;a deste contrato, a CONTRATADA ir&aacute; fornecer &agrave; autoridade solicitante a senha de acesso aos dados armazenados, independentemente de qualquer notifica&ccedil;&atilde;o pr&eacute;via.?</p>\n\n<p>9. DA RESCIS&Atilde;O CONTRATUAL</p>\n\n<p>9.1. O presente Contrato poder&aacute; ser renunciado por qualquer das partes, a qualquer tempo, mediante simples comunica&ccedil;&atilde;o por escrito &agrave; outra parte, com anteced&ecirc;ncia m&iacute;nima de 30 (trinta) dias, a contar da data do recebimento protocolado, sem que caiba qualquer compensa&ccedil;&atilde;o ou indeniza&ccedil;&atilde;o &agrave;s partes, independente da data deste instrumento.</p>\n\n<p>9.2. Quando ocorrer inadimplemento de qualquer obriga&ccedil;&atilde;o estabelecida neste contrato, sendo que a parte prejudicada dever&aacute; primeiro notificar judicialmente ou extrajudicialmente a parte inadimplente, determinando que a inadimpl&ecirc;ncia seja sanada dentro do prazo de 24 (vinte quatro) horas, contadas do recebimento da notifica&ccedil;&atilde;o;</p>\n\n<p>9.3. O n&atilde;o pagamento pela CONTRATANTE de qualquer fatura por um per&iacute;odo superior a 60 dias depois do respectivo vencimento acarretar&aacute; na rescis&atilde;o de pleno direito deste contrato, independente de aviso ou notifica&ccedil;&atilde;o.</p>\n\n<p>9.4. Pela falta de qualquer pagamento, a CONTRATANTE sujeitar-se-&aacute; &agrave; multa morat&oacute;ria de 2% (dois por cento) e juros de 1% (um por cento) ao m&ecirc;s ou fra&ccedil;&atilde;o do m&ecirc;s, ambos aplicados sobre o valor em atraso corrigidos conforme o &iacute;ndice de corre&ccedil;&atilde;o aplicado neste Contrato (IGPM/FGV).?</p>\n\n<p>10. DA N&Atilde;O PRESERVA&Ccedil;&Atilde;O DOS DADOS</p>\n\n<p>10.1. Findando este contrato, seja por qualquer motivo, os dados armazenados ser&atilde;o exclu&iacute;dos automaticamente no momento do cancelamento dos servi&ccedil;os, ap&oacute;s o cancelamento, ser&aacute; salvo em outro local de armazenamento e ficar&aacute; dispon&iacute;vel pelo prazo de 5 anos, foco deste servi&ccedil;o, a possibilidade de se recuperar os dados, se sujeitar&aacute; ao pagamento de uma taxa, e depois disso ser&aacute; exclu&iacute;do em definitivo.</p>\n\n<p>11. DAS DISPOSI&Ccedil;&Otilde;ES FINAIS E FORO</p>\n\n<p>11.1. O presente Contrato revoga e substitui todos os outros Contratos, que eventualmente existam, referentes ao dom&iacute;nio objeto deste instrumento havidos entre as partes anteriores &agrave; data da solicita&ccedil;&atilde;o registrada neste Contrato.</p>\n\n<p>11.2. Fica eleito o Foro de Marilia, S&atilde;o Paulo, com ren&uacute;ncia a qualquer outro, por mais privilegiado que seja, para nele serem dirimidas eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes deste Contrato.</p>\n\n<p>11.3. E, por estarem assim justos e acertados, obrigando-se a cumprir o Contrato por si, administradores ou sucessores aceitam o presente contrato.</p>\n\n<p>Mar&iacute;lia,&nbsp;{data_adesao}.</p>\n\n<p>Central de Notas Armazenamento Digital ME</p>\n\n<p>CNPJ: 14.566.416/0001-86</p>\n\n<p>&nbsp;</p>\n\n<p>Servi&ccedil;os Contratados</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n');

-- --------------------------------------------------------

--
-- Estrutura para tabela `download_link`
--

CREATE TABLE IF NOT EXISTS `download_link` (
  `link_id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `user_key` varchar(255) DEFAULT NULL,
  `generated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires` timestamp NULL DEFAULT NULL,
  `downloads` int(11) NOT NULL DEFAULT '0',
  `key` varchar(255) NOT NULL,
  `downloaded` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `single` tinyint(1) NOT NULL DEFAULT '0',
  `user_agent` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `download_link`
--

INSERT INTO `download_link` (`link_id`, `hash`, `user_key`, `generated`, `expires`, `downloads`, `key`, `downloaded`, `ip_address`, `single`, `user_agent`) VALUES
(1, 'KHAVXboYWKG6ejTDXUoAOMm1PxEead', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '2015-11-10 15:39:06', '2015-11-10 14:39:06', 1, 'Pl32Of4TI2VfRXdLKys9mY7bNDNWuV6yjP8F5DlywmeiKjRmuAKS17eSIkHUEINWAp7tdyHvXcRpsCGlnGO2cv8Z6zWBEhib8d8N', '2015-11-10 01:39:07', '177.129.252.6', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36'),
(2, 'Pfnmhd79dmlkmw96y4EgS7NXJH26ak', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '2015-11-10 15:41:53', '2015-11-10 14:41:53', 0, 'DyA4TrTA8D3ia31ucHXWXXSOWz0AeYKj8EM6XAVO4bvG4CpWmgE7zPYKlqLOgrsyL5HJVmqm3QKV6YUZl4qfbaO0O4d4BADIo24H', NULL, NULL, 1, NULL),
(3, 'emPstv5vzyCeDnt8sfVIiEypVmpTTu', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '2015-11-10 15:42:11', '2015-11-10 14:42:11', 0, 'htfmcGGpMDe8nqJGxBqsNqVX3ODVf2NWLp3KgjJVp0GlRNALNyVZaWxxMfrgOnsOp7ztrKLYy7zgXagz8ux9LcB1yiAX65E4iMQp', NULL, NULL, 1, NULL),
(4, '7d0t6gvUNLgfuuabofTGmz3IkUQR9T', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '2015-11-10 15:50:34', '2015-11-10 14:50:34', 0, '03bxaOx30mT1oVfoKXNygLzCwVHfuRXShB9DHOsL75iB96YyAEtd3iSkxmpw6Ge5UTsvxZZgFpisrGh3NqS3SUOxLHTqcQkOdmYK', NULL, NULL, 1, NULL),
(5, 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-10 22:36:03', '2015-11-10 21:36:03', 0, '4zvXKUTAoMWCZZODPZQ5YjoBTKbdV6Pa6c0hBCuwrNoGHJOGtwaqyYKBo66FXhYOljmRaI3Jp6U2gKh42Xg9zPdxPAdgpCZYwLCX', NULL, NULL, 1, NULL),
(6, '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-10 22:36:29', '2015-11-10 21:36:29', 0, 'aSWUvKIW1iVue25Jc566faTr5BSqzogqUGgIZv8EZeu9G7mgzfPRUPAJRWKd4w8uODWB5LdPJDw4ojxcYUx8ySXeCzBQYTrNiSn9', NULL, NULL, 1, NULL),
(7, '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-11-11 00:21:34', '2015-11-10 23:21:34', 0, 'jpZG4UhOegQiYZkWL1rZ5IaXcdJpGDtId6krrUEze2n4G8hZdlsT3CkelhscNhD25qJ3FL2BhZVxrZVprVsu4m1Ntc99OaM2YZi0', NULL, NULL, 1, NULL),
(8, 'K28Ik4mAHlYBY89X1nYzD0QwxJCiFZ', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-11-11 00:22:09', '2015-11-10 23:22:09', 0, 'uUHkT4PjwyKdxqMvHwyXrigr2CkaZfhnYDJQRyXByPRSAWZiAYhPjdzo2amECy4uqu69AJf853GSWdqYTuqKLZ2f1VFSvShOrX7J', NULL, NULL, 1, NULL),
(9, '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-11 01:29:51', '2015-11-11 00:29:51', 0, 'kh8t3VFtbZ9jaAGQfICranEleCLtzSmjXdXeccQ1WFkZ3OrSsg7rxrvP1NicdK6N3zsyceET6xMWwq5xQ7dEKz4khnPcV8A0z6i7', NULL, NULL, 1, NULL),
(10, 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-17 15:50:50', '2015-11-17 14:50:50', 1, 'TdZJ7v2TGXSLKHJsR8cYbbpniJBfFGvB2GHPM5pkCeiJ9oMX60QfaVeUrtcFlGcSrRkvYhykztZQbRqyQSTnKjqDUEv2xJR5P0UV', '2015-11-17 01:50:50', '179.233.208.222', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(11, 'lwZBegPD17Wv5ZHZyHKhuVHstmMILE', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-17 15:51:01', '2015-11-17 14:51:01', 1, 'OtD8l649dn4qgmbJv3RtDAs36Zoo68SxlapVMrSoJiedTv4aOe49ak1hL5yLFPnL91PMsLsC62EyE5rrRrfS0jcXiimPBtJHE4fH', '2015-11-17 01:51:02', '179.233.208.222', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(12, 'On40mISDaJwDNTlOFMLWH3g5rRWbd7', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-18 22:29:24', '2015-11-18 21:29:24', 0, '9fYJTV5VuUIw4yVzvOZkzKk9lTN18LAoUU1vym5KSpP2p1V3RDfaKjBLsyqotfVj1H0f4bUyZISD87RbnyzqBtYic2sTZIeSfHqJ', NULL, NULL, 1, NULL),
(13, 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-18 22:30:20', '2015-11-18 21:30:20', 1, 'HtlwJYQXS26UVCC1iBcVPAnFCzSg8JPWIttbJjzH1aXzT0NE7HJtRf8ts3Otafxx6Z60b0WfDrRDxBXO5tRqWZjfyFHSgkt3VOzq', '2015-11-18 08:30:22', '179.96.156.98', 1, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(14, 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-18 22:31:59', '2015-11-18 21:31:59', 1, 'JFqcwMNIgDlSNvpXKK7Lq5fQOQDqFtLyLfDixy3v9WfAnVcotAhiBl8WKTue83aG57x4zGj3m2CBRPdnvQ9UO2Au0evCIiGi7REJ', '2015-11-18 08:32:00', '179.96.156.98', 1, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(15, 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-26 18:30:54', '2015-11-26 17:30:54', 1, 'KaaUZ2VMKE1PJz5xNPf9RoOKsGyCo3grRX6FCvanH5CsOQoauGgyST0SGRPC20et8btPv434IeesQ5vURxdyeZxILYbWdNS4Mauh', '2015-11-26 04:30:57', '179.96.156.104', 1, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(16, 'ZZ32euGuaTaZf7twwB2UHt7wnAnZ1F', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-26 18:31:03', '2015-11-26 17:31:03', 1, 'tYkjpXWbt7Wa06QfyLmFsLgoucXMmh6MgZe14qV69VYMNVEcBFCfz6PTkggoee9GGXRHe1Pvpti4kjB3GBSTf6IukbJZumfz3Wm5', '2015-11-26 04:31:06', '179.96.156.104', 1, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(17, 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-26 18:31:12', '2015-11-26 17:31:12', 1, 'wChgEDkaijIOIofstsVdIQENtYdthIHASi2XXNFXMIGwqS3EMRuMf3QRUAh2g8AaVhGZtsuoM4paB218nJhfCbidXXrAHNu3AqoU', '2015-11-26 04:31:14', '179.96.156.104', 1, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(18, '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-28 12:54:45', '2015-11-28 11:54:45', 0, 'onH6UzAa5c5be27TTdFhEMTenSNq4CZg6ap2R5M4JhLVmmLfk2nCfYaC0oACBs9buXyYBLMIirVrVsv6aD1JN44a6f27RQLP5A1f', NULL, NULL, 1, NULL),
(19, 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-28 12:55:07', '2015-11-28 11:55:07', 0, 'gMP4xVuxhEAOqbouYSBRujepP04d6hgTbL9uMeXpBrfhqh2RBRgSRZtjlxfDOaeTHQeeQurN01XqC7y6EDFLryGdjWpZxqVdj3cd', NULL, NULL, 1, NULL),
(20, 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '2015-11-29 23:52:35', '2015-11-29 22:52:35', 1, 'iJaymffAiCAflVvCNQPb7CK40F1oLdDOUZ6fk4UKNjItwWoCtJ6nHuSPDTrPB0cryWVMNvdWfP0jpUSWWQupOLf9snPdaUNgzpSs', '2015-11-29 09:52:39', '191.247.228.34', 1, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36'),
(21, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 19:17:59', '2015-12-01 18:17:59', 1, 'waTD0SH2AD3WB7Z23WolUyxhtQHA6JKZCfT5XAIyK9qALUj8Er66hHdQZtjVWwXIyGXFQSr6j0KdvYSiqt2zISkJjcCXj4gCA5za', '2015-12-01 05:18:01', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(22, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 19:18:59', '2015-12-01 18:18:59', 1, '4k2AebyidE4YxH4DLQJRavPCzvQZ00BCuik6EN5ITmMTVZMhogKGYfquew2TpkJ1l0AaFCUuuUj7mze8Djnxc4IsrBnd00rWcYrV', '2015-12-01 05:19:00', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(23, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 19:19:14', '2015-12-01 18:19:14', 1, 'VGbbGFikP6JSBJf24uXvmatwiJIitsR2mGNbc8X5X6UdUqCkIuXSdIM8Ojsx1Vxw9lR3MlzlYsTS94AczQsTkDByWV5cc0DW5rcj', '2015-12-01 05:19:15', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(24, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 19:38:42', '2015-12-01 18:38:42', 1, 'dRczoQuvVaVwrm9BkXKDKYgvqHsZoBoCPCUImwGaT3UT5pFWpWUFs11nSIRU1z8yzn5kMcsioMmf9HA50HRzR2Fjx8Fn6YWoBwXF', '2015-12-01 05:38:44', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(25, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 19:42:14', '2015-12-01 18:42:14', 1, '7ImwVpAet1CkefdaOXShcM8AhyUGDIlOOf3YfRDXDDaBgJjT3H2pmIwYSX70VeRq3tI0b3WLNAz6rWtUdytP2PIUsIwb22eJ7wzL', '2015-12-01 05:42:16', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(26, '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 20:35:44', '2015-12-01 19:35:44', 0, 'n597HGVUcX6wDjt9CuwVR73WL3y05baMapq27DB9UBWGqX6fRMaRUjU9S9oOI7Z9putrtTYiCN0gyAKTdWHVwKshBajJbpShHJBj', NULL, NULL, 1, NULL),
(27, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 20:35:59', '2015-12-01 19:35:59', 1, 'xSwpJepZvY6KEPPIasG0hbGc8NTr4ATYfiQ5FhzS6m0lQOv08gorH5A6it0PaAvh6qfO9YTnTZ5Ktdzy8ZxTBo43oGqsjUoILm1u', '2015-12-01 06:36:00', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(28, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '2015-12-01 20:42:47', '2015-12-01 19:42:47', 1, 'jO68HtNvacGVgsWQTLMmipn0O2hJ11HycZa1xMuZXNvz5LlhMUY3GgvsXaUQcnWB4GaCBUUpnM4xE3pNU3zAC4vCHynWanGfaU4v', '2015-12-01 06:42:49', '177.129.252.59', 1, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36');

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa`
--

CREATE TABLE IF NOT EXISTS `empresa` (
  `empresa_id` int(11) NOT NULL,
  `empresa_razao` varchar(155) NOT NULL,
  `empresa_fantasia` varchar(155) NOT NULL,
  `empresa_cnpj` varchar(14) NOT NULL,
  `empresa_ie` varchar(10) DEFAULT NULL,
  `empresa_segmento_id` int(11) NOT NULL,
  `empresa_user_id` int(11) DEFAULT NULL,
  `empresa_logo` varchar(255) DEFAULT NULL,
  `empresa_key` varchar(10) NOT NULL,
  `empresa_principal` tinyint(1) NOT NULL DEFAULT '0',
  `empresa_register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `empresa`
--

INSERT INTO `empresa` (`empresa_id`, `empresa_razao`, `empresa_fantasia`, `empresa_cnpj`, `empresa_ie`, `empresa_segmento_id`, `empresa_user_id`, `empresa_logo`, `empresa_key`, `empresa_principal`, `empresa_register_date`) VALUES
(1, 'Teste', 'Testando', '00000000000000', 'ISENTO', 1, 16, NULL, '73q6Xod2', 1, '2015-11-10 15:37:01'),
(2, 'teste', 'teste.com', '32034333896', 'ISENTO', 1, 17, NULL, 'VetsLOtz', 1, '2015-11-10 19:30:22'),
(3, 'Ged teste', 'armazenamento', '32034555678', 'ISENTO', 1, 18, NULL, 'Vwq9HJQR', 1, '2015-11-10 23:52:32'),
(4, 'Danieledoc', 'Docmais', '05285633192', 'ISENTO', 2, 20, NULL, '3ukSpcUY', 1, '2015-11-23 14:10:23');

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa_contato`
--

CREATE TABLE IF NOT EXISTS `empresa_contato` (
  `empresa_contato_id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `contato_id` int(11) NOT NULL,
  `principal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `empresa_contato`
--

INSERT INTO `empresa_contato` (`empresa_contato_id`, `empresa_id`, `contato_id`, `principal`) VALUES
(1, 1, 5, 1),
(2, 1, 6, 1),
(3, 2, 7, 1),
(4, 2, 8, 1),
(5, 3, 9, 1),
(6, 3, 10, 1),
(7, 4, 11, 1),
(8, 4, 12, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa_endereco`
--

CREATE TABLE IF NOT EXISTS `empresa_endereco` (
  `empresa_endereco_id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `endereco_id` int(11) NOT NULL,
  `principal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `empresa_endereco`
--

INSERT INTO `empresa_endereco` (`empresa_endereco_id`, `empresa_id`, `endereco_id`, `principal`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(3, 3, 3, 1),
(4, 4, 4, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa_usuario`
--

CREATE TABLE IF NOT EXISTS `empresa_usuario` (
  `id_empresa_usuario` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `quota` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `empresa_usuario`
--

INSERT INTO `empresa_usuario` (`id_empresa_usuario`, `id_empresa`, `id_usuario`, `quota`) VALUES
(1, 1, 16, NULL),
(2, 2, 17, NULL),
(3, 3, 18, NULL),
(4, 2, 19, NULL),
(5, 4, 20, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `endereco`
--

CREATE TABLE IF NOT EXISTS `endereco` (
  `endereco_id` int(11) NOT NULL,
  `endereco_logradouro` varchar(155) NOT NULL,
  `endereco_num` varchar(10) NOT NULL,
  `endereco_bairro` varchar(155) NOT NULL,
  `endereco_cep` varchar(45) NOT NULL,
  `endereco_estado` varchar(2) NOT NULL,
  `endereco_cidade` varchar(155) NOT NULL,
  `endereco_complemento` varchar(155) DEFAULT NULL,
  `endereco_alias` varchar(45) NOT NULL,
  `endereco_principal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `endereco`
--

INSERT INTO `endereco` (`endereco_id`, `endereco_logradouro`, `endereco_num`, `endereco_bairro`, `endereco_cep`, `endereco_estado`, `endereco_cidade`, `endereco_complemento`, `endereco_alias`, `endereco_principal`) VALUES
(1, 'Rua Euclides da Cunha', '83', '', '17602-490', 'SP', 'Marília', NULL, 'Escritório Principal', 1),
(2, 'Segismundo nunes de oliveiro', '140', '', '17512-52', 'SP', 'Marilia', 'casa', 'Casa', 1),
(3, 'teste teste', '345', '', '17520-230', 'SP', 'marilia', 'fundos', 'Escritório', 1),
(4, 'almeida campos', '145', '', '17503-220', 'SP', 'Marilia', 'Fundos', 'casa', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `espaco_adicional`
--

CREATE TABLE IF NOT EXISTS `espaco_adicional` (
  `id` int(11) NOT NULL,
  `quant_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `espaco_adicional`
--

INSERT INTO `espaco_adicional` (`id`, `quant_id`, `client_id`, `price`) VALUES
(1, 1, 2, '10.00'),
(2, 2, 2, '20.00'),
(3, 2, 2, '20.00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `faturas`
--

CREATE TABLE IF NOT EXISTS `faturas` (
  `fatura_id` int(8) unsigned zerofill NOT NULL,
  `fatura_emissao` datetime NOT NULL,
  `fatura_vencimento` date NOT NULL,
  `client_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `fatura_obs` text,
  `fatura_hash` varchar(255) NOT NULL,
  `nosso_numero` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `faturas`
--

INSERT INTO `faturas` (`fatura_id`, `fatura_emissao`, `fatura_vencimento`, `client_id`, `status_id`, `fatura_obs`, `fatura_hash`, `nosso_numero`) VALUES
(00000001, '2015-11-10 01:38:03', '2015-11-20', 1, 1, 'Fatura automatica gerada no cadastro do cliente.', 'yrAnBS8J7WR25ckaTQRUlhkrITSrb3l2iRvYn6ldkk1S327Vs3J7Z07gs6AMFCE2k7f1ptFaXf5gi2oS86pc43kBj6Pbby2di5TwpD0gCVNcvEP9QWMKW5ykAdpUJek8KNBqqzHD5wxdyG7oyRdygi0XYfKI7gnHzXQvtuw4Yu5bx9RzRkEq9fqJMSnI4JDCje7TSs0L5MOzpZJbfVLUROqGYgM4hGiLRnSeACrE4bXs3ky1R9PVx3TqHDeZqIa', 12345679),
(00000002, '2015-11-10 05:39:13', '2015-11-20', 2, 1, 'Fatura automatica gerada no cadastro do cliente.', 'IVWv1spgobnyqZU9q1L8fE6ssJA9GePV8VxUNFZ8rS00va73zlYXm7Qv4ZKzyYLpFo8Jhfr4jOOp0GaSLY0fSyLynXKrgYV9e26c45BYmGzLHwWlRAC3I9J5dvM8wlsDEiTcFCJxpsTLXXYObN9o07tAOXimGdQ1gnbRoPJSCe5PGp9oRtQp3CHPye7iJtyTFLbtgMno7u0iUbPsxL4SAGeplY2ZZVJe9qBrS4v3ELJ7taLxkjFWZT9bpCzs1ly', 12345680),
(00000003, '2015-11-10 09:56:45', '2015-11-20', 3, 1, 'Fatura automatica gerada no cadastro do cliente.', 'JGmU9pm4mA3vvm0aUUlX1TlCTbCYbpWVwbNLFiBThCuyYKqKOQBlUPUKi9yUfM7SfMY7fTjbBL8TcXxxFkmQDlHQQZskgAsLcbCzME0IvMgknIfDdxYEF1iIbc3L52OSEeqMilWLPG9mvVLQPoOwdu3YrmOQevnpNjASTcuysghXouMNU6Gq4KW9KseqYrATcNBkvWhZB0TOCCuNbHO1g6JxHR4x7ZJ1qdzvzyFq6UeFuTZXNaOyJEgmsFIIVG3', 12345681),
(00000004, '2015-11-23 12:12:21', '2015-12-03', 4, 1, 'Fatura automatica gerada no cadastro do cliente.', 'dEnGGRZldnPBbuD6hvawv7qH14GOPCOVFt5uOaAz6v22Xzcnhlyhu86ABDBD4bilaEABJAxRDD7pVWZ15plLNMiio4RZfbCG8Y4n6EQCafgQguCOGFX3IH0VmA0eXy6Hy0P02u2EMCZ6adjvowy3hr4aStYSMYDsmN6B920H9YDgKg91pxVEjCPLBYf0IWUmDnBqqF7WT0lSFDyb0wqPKU9FFcsVHb6etuG71HLRdtOu68C1xZhJ85RJvUJmBP4', 12345682);

--
-- Gatilhos `faturas`
--
DELIMITER $$
CREATE TRIGGER `update_number` BEFORE INSERT ON `faturas`
 FOR EACH ROW BEGIN
DECLARE nosso, proximo INT(11);
SELECT max(nosso_numero) INTO nosso FROM faturas;

IF(nosso IS NULL)
THEN
	SELECT bank_nosso_atual INTO nosso FROM banco;
END IF;

SET proximo = nosso +1;

SET new.nosso_numero = proximo;

UPDATE banco set bank_nosso_atual = proximo;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fatura_itens`
--

CREATE TABLE IF NOT EXISTS `fatura_itens` (
  `item_id` int(11) NOT NULL,
  `fatura_id` int(11) unsigned zerofill NOT NULL,
  `service_id` int(11) NOT NULL,
  `item_valor` decimal(10,0) NOT NULL,
  `adicional` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `fatura_itens`
--

INSERT INTO `fatura_itens` (`item_id`, `fatura_id`, `service_id`, `item_valor`, `adicional`) VALUES
(1, 00000000001, 1, '0', 0),
(2, 00000000001, 3, '0', 0),
(3, 00000000001, 12, '0', 0),
(4, 00000000002, 1, '0', 1),
(5, 00000000002, 2, '0', 1),
(6, 00000000002, 1, '0', 0),
(7, 00000000002, 3, '0', 0),
(8, 00000000002, 12, '0', 0),
(9, 00000000003, 1, '0', 0),
(10, 00000000003, 3, '0', 0),
(11, 00000000004, 1, '0', 0),
(12, 00000000004, 3, '0', 0),
(13, 00000000004, 12, '0', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fatura_status`
--

CREATE TABLE IF NOT EXISTS `fatura_status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `status_editable` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `fatura_status`
--

INSERT INTO `fatura_status` (`status_id`, `status_name`, `status_editable`) VALUES
(1, 'Gerada', 0),
(2, 'Enviada', 0),
(3, 'Vencida', 0),
(4, 'Cancelada', 0),
(5, 'Atualizada', 0),
(6, 'Em Protesto', 0),
(7, 'Paga', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `file_log`
--

CREATE TABLE IF NOT EXISTS `file_log` (
  `log_id` int(11) NOT NULL,
  `log_action` varchar(45) NOT NULL,
  `user_key` varchar(100) NOT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` text,
  `alert` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `file_log`
--

INSERT INTO `file_log` (`log_id`, `log_action`, `user_key`, `hash`, `datetime`, `text`, `alert`) VALUES
(1, 'list_files', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Home', '2015-11-10 01:38:08', NULL, 0),
(2, 'upload', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'uEbpjESLL46PzTEQXmffuzfitcMpEt', '2015-11-10 01:39:00', NULL, 0),
(3, 'upload', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'KHAVXboYWKG6ejTDXUoAOMm1PxEead', '2015-11-10 01:39:00', NULL, 0),
(4, 'list_files', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Home', '2015-11-10 01:39:03', NULL, 0),
(5, 'generate_download_link', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'KHAVXboYWKG6ejTDXUoAOMm1PxEead', '2015-11-10 01:39:06', NULL, 0),
(6, 'open_or_download', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'KHAVXboYWKG6ejTDXUoAOMm1PxEead', '2015-11-10 01:39:06', NULL, 0),
(7, 'download', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'KHAVXboYWKG6ejTDXUoAOMm1PxEead', '2015-11-10 01:39:07', NULL, 0),
(8, 'list_files', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Home', '2015-11-10 01:39:21', NULL, 0),
(9, 'upload', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Pfnmhd79dmlkmw96y4EgS7NXJH26ak', '2015-11-10 01:40:57', NULL, 0),
(10, 'upload', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'gyCvSp7APg4qq8bXohY74RtKBtUvLQ', '2015-11-10 01:41:01', NULL, 0),
(11, 'upload', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '7d0t6gvUNLgfuuabofTGmz3IkUQR9T', '2015-11-10 01:41:45', NULL, 0),
(12, 'upload', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'emPstv5vzyCeDnt8sfVIiEypVmpTTu', '2015-11-10 01:41:46', NULL, 0),
(13, 'list_files', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Home', '2015-11-10 01:41:47', NULL, 0),
(14, 'list_files', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Home', '2015-11-10 01:41:49', NULL, 0),
(15, 'generate_download_link', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Pfnmhd79dmlkmw96y4EgS7NXJH26ak', '2015-11-10 01:41:53', NULL, 0),
(16, 'open_or_download', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'Pfnmhd79dmlkmw96y4EgS7NXJH26ak', '2015-11-10 01:41:53', NULL, 0),
(17, 'generate_download_link', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'emPstv5vzyCeDnt8sfVIiEypVmpTTu', '2015-11-10 01:42:11', NULL, 0),
(18, 'open_or_download', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'emPstv5vzyCeDnt8sfVIiEypVmpTTu', '2015-11-10 01:42:11', NULL, 0),
(19, 'generate_download_link', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '7d0t6gvUNLgfuuabofTGmz3IkUQR9T', '2015-11-10 01:50:34', NULL, 0),
(20, 'open_or_download', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', '7d0t6gvUNLgfuuabofTGmz3IkUQR9T', '2015-11-10 01:50:34', NULL, 0),
(21, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 05:39:35', NULL, 0),
(22, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 05:39:49', NULL, 0),
(23, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lwZBegPD17Wv5ZHZyHKhuVHstmMILE', '2015-11-10 05:40:15', NULL, 0),
(24, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'k189i9lLDubIiGnQiTJOkJyr1oVuO0', '2015-11-10 05:40:15', NULL, 0),
(25, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'W5mojJE0kIxgjpQRwUEhiULXFQhDQa', '2015-11-10 05:40:15', NULL, 0),
(26, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'z34YFXg63vH1Jql2PbazQREv00DVlv', '2015-11-10 05:40:15', NULL, 0),
(27, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 05:40:23', NULL, 0),
(28, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 05:49:16', NULL, 0),
(29, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 05:49:49', NULL, 0),
(30, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 08:14:49', NULL, 0),
(31, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 08:17:41', NULL, 0),
(32, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 08:28:07', NULL, 0),
(33, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3GVgcp3sQFUzDlvspmCEgJBnVG26Sw', '2015-11-10 08:30:11', NULL, 0),
(34, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'X8RHYPF5MVvpw14OU3FEDr8L0mOaFf', '2015-11-10 08:30:22', NULL, 0),
(35, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '8Jq4aZhvN1uUsGUYJ8DY9yqse06KcB', '2015-11-10 08:30:25', NULL, 0),
(36, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'fgHG9gC8DQURAZnErg1Axp4t6Cu5Jy', '2015-11-10 08:30:34', NULL, 0),
(37, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lYNqRJ2rtDODoBKRnAReGnpSVTwFcC', '2015-11-10 08:30:38', NULL, 0),
(38, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'f6VTnirf5ACgOHPfuxfS0Zq3XTgAVH', '2015-11-10 08:30:41', NULL, 0),
(39, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lceubbOhrMl52jMllNSjROLPXprGmp', '2015-11-10 08:30:48', NULL, 0),
(40, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'YgDGG2GyAHzSwtzB3CSwUwt2GQHwyW', '2015-11-10 08:30:50', NULL, 0),
(41, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'm3tSnL7H5fSKNVoWNWPvM6TLfcHjdy', '2015-11-10 08:30:52', NULL, 0),
(42, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-10 08:31:40', NULL, 0),
(43, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'QTOLGVHhvuyi0C2NdJff3bf6F893ie', '2015-11-10 08:31:40', NULL, 0),
(44, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', '2015-11-10 08:31:42', NULL, 0),
(45, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'qCZ7406ZX47ksRszLr2ff5cI9yIvtS', '2015-11-10 08:34:35', NULL, 0),
(46, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 08:35:23', NULL, 0),
(47, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 08:35:25', NULL, 0),
(48, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', '2015-11-10 08:36:03', NULL, 0),
(49, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', '2015-11-10 08:36:03', NULL, 0),
(50, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-10 08:36:29', NULL, 0),
(51, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-10 08:36:29', NULL, 0),
(52, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:10:25', NULL, 0),
(53, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:10:27', NULL, 0),
(54, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:15:04', NULL, 0),
(55, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:15:12', NULL, 0),
(56, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:16:29', NULL, 0),
(57, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:16:38', NULL, 0),
(58, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '7XjWNt9VilJ0ZmoCRbcRsVPaR9dPMx', '2015-11-10 22:17:21', NULL, 0),
(59, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '4bp6m5iAOmAAnf69WJMDdcExq652JD', '2015-11-10 22:17:24', NULL, 0),
(60, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'PmgXSND3vCrjh1yNnLdhReQLKxvIcQ', '2015-11-10 22:17:24', NULL, 0),
(61, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'JLrGZkcVM2CSll6KTZwIgyZ1V7VoAY', '2015-11-10 22:17:32', NULL, 0),
(62, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-11-10 22:17:47', NULL, 0),
(63, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'K28Ik4mAHlYBY89X1nYzD0QwxJCiFZ', '2015-11-10 22:17:49', NULL, 0),
(64, 'upload', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', '2015-11-10 22:20:39', NULL, 0),
(65, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-11-10 10:20:53', NULL, 0),
(66, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', '2015-11-10 10:21:34', NULL, 0),
(67, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', '2015-11-10 10:21:34', NULL, 0),
(68, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'K28Ik4mAHlYBY89X1nYzD0QwxJCiFZ', '2015-11-10 10:22:09', NULL, 0),
(69, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'K28Ik4mAHlYBY89X1nYzD0QwxJCiFZ', '2015-11-10 10:22:09', NULL, 0),
(70, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 10:53:59', NULL, 0),
(71, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 11:24:35', NULL, 0),
(72, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 11:27:48', NULL, 0),
(73, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 11:28:00', NULL, 0),
(74, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', '2015-11-10 11:28:23', NULL, 0),
(75, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 11:29:44', NULL, 0),
(76, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 11:29:46', NULL, 0),
(77, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-10 11:29:51', NULL, 0),
(78, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-10 11:29:51', NULL, 0),
(79, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-10 11:33:20', NULL, 0),
(80, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-10 11:33:32', NULL, 0),
(81, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 06:19:43', NULL, 0),
(82, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 06:20:03', NULL, 0),
(83, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 10:26:42', NULL, 0),
(84, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 10:26:44', NULL, 0),
(85, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lYNqRJ2rtDODoBKRnAReGnpSVTwFcC', '2015-11-11 11:14:56', NULL, 0),
(86, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '8Jq4aZhvN1uUsGUYJ8DY9yqse06KcB', '2015-11-11 11:15:29', NULL, 0),
(87, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '8Jq4aZhvN1uUsGUYJ8DY9yqse06KcB', '2015-11-11 11:15:31', NULL, 0),
(88, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'ZZ32euGuaTaZf7twwB2UHt7wnAnZ1F', '2015-11-11 11:16:48', NULL, 0),
(89, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'W4EvOZIe2W7MVyJSTlLlQlCM8tY3jd', '2015-11-11 11:16:48', NULL, 0),
(90, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-11 11:16:50', NULL, 0),
(91, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '17DX0mjThc20T6l8QVL5ZhU68CM95v', '2015-11-11 11:16:52', NULL, 0),
(92, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'LzxwMiWGHf38757UReRHw0N4ObZ0d2', '2015-11-11 11:16:54', NULL, 0),
(93, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'E4XmdSHWBS4F5kk8BgFTUzfA8JQb8b', '2015-11-11 11:16:55', NULL, 0),
(94, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 's4VmqRXGLNwlpk7SqEICbmucMC7N1l', '2015-11-11 11:17:05', NULL, 0),
(95, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'dlTEGwOpdMg0UwLdQWL00SLoolgZKA', '2015-11-11 11:17:05', NULL, 0),
(96, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'TtDVQZ1E8RKGMKKwzx5oS1B8YAdpu0', '2015-11-11 11:17:07', NULL, 0),
(97, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'TGLv7BwbUQPf5yNALIKO0eLYKQgSsd', '2015-11-11 11:17:07', NULL, 0),
(98, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'jDrgGCRpI6ZmzVevyGeCCxqCl6DANq', '2015-11-11 11:17:08', NULL, 0),
(99, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Mtsh6fTG2wcz06KOKwwMIvYlAi89gm', '2015-11-11 11:17:09', NULL, 0),
(100, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'MvMk7hoCrbcu9otAB7XRaXQYsz2NXg', '2015-11-11 11:17:16', NULL, 0),
(101, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'r3KEf8ig94joewL7XtH1YxWiQbTyuE', '2015-11-11 11:17:16', NULL, 0),
(102, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'a8ePIAVAwCr4E9k7gYfjUv6Q6ULK5i', '2015-11-11 11:17:18', NULL, 0),
(103, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 11:17:29', NULL, 0),
(104, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', '2015-11-11 11:19:44', NULL, 0),
(105, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 11:21:26', NULL, 0),
(106, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 11:21:27', NULL, 0),
(107, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '10QCEMUtJ0WMQxg3vnRVyHQFeosG3Y', '2015-11-11 11:23:26', NULL, 0),
(108, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'BjMe7qXhMPIvGf28wGoHGH8jXA9vx8', '2015-11-11 11:23:26', NULL, 0),
(109, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'B7CiwMz5hNTWaQsGw6fKsWZtBmAwDS', '2015-11-11 11:23:27', NULL, 0),
(110, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 11:23:46', NULL, 0),
(111, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '10QCEMUtJ0WMQxg3vnRVyHQFeosG3Y', '2015-11-11 11:24:26', NULL, 0),
(112, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '10QCEMUtJ0WMQxg3vnRVyHQFeosG3Y', '2015-11-11 11:24:28', NULL, 0),
(113, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 11:28:24', NULL, 0),
(114, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3GVgcp3sQFUzDlvspmCEgJBnVG26Sw', '2015-11-11 11:31:32', NULL, 0),
(115, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-11 11:32:44', NULL, 0),
(116, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'X8RHYPF5MVvpw14OU3FEDr8L0mOaFf', '2015-11-11 11:33:16', NULL, 0),
(117, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'fgHG9gC8DQURAZnErg1Axp4t6Cu5Jy', '2015-11-11 11:33:44', NULL, 0),
(118, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'z34YFXg63vH1Jql2PbazQREv00DVlv', '2015-11-12 12:01:39', NULL, 0),
(119, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-12 12:02:44', NULL, 0),
(120, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-12 12:02:56', NULL, 0),
(121, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-13 08:49:37', NULL, 0),
(122, 'list_files', '3ds5hj5KTKCgXE0ggnP31kMzb4LDrQX3cyZayGvhgYUmeBX3eQ5Kpl1lwifvOwLWT8PSxyej8nSsFiBWWLePFEYUP39zg0hvwZ8F', 'Home', '2015-11-13 09:29:05', NULL, 0),
(123, 'list_files', '3ds5hj5KTKCgXE0ggnP31kMzb4LDrQX3cyZayGvhgYUmeBX3eQ5Kpl1lwifvOwLWT8PSxyej8nSsFiBWWLePFEYUP39zg0hvwZ8F', 'Home', '2015-11-13 09:29:24', NULL, 0),
(124, 'list_files', '3ds5hj5KTKCgXE0ggnP31kMzb4LDrQX3cyZayGvhgYUmeBX3eQ5Kpl1lwifvOwLWT8PSxyej8nSsFiBWWLePFEYUP39zg0hvwZ8F', 'Home', '2015-11-13 09:34:33', NULL, 0),
(125, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-15 11:38:18', NULL, 0),
(126, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-15 11:38:47', NULL, 0),
(127, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-15 11:39:09', NULL, 0),
(128, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-15 11:39:18', NULL, 0),
(129, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-15 11:42:07', NULL, 0),
(130, 'delete_share', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-15 11:43:08', NULL, 0),
(131, 'share', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-15 11:43:09', NULL, 0),
(132, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 01:50:02', NULL, 0),
(133, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 01:50:36', NULL, 0),
(134, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-17 01:50:50', NULL, 0),
(135, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-17 01:50:50', NULL, 0),
(136, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-17 01:50:50', NULL, 0),
(137, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lwZBegPD17Wv5ZHZyHKhuVHstmMILE', '2015-11-17 01:51:01', NULL, 0),
(138, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lwZBegPD17Wv5ZHZyHKhuVHstmMILE', '2015-11-17 01:51:01', NULL, 0),
(139, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'lwZBegPD17Wv5ZHZyHKhuVHstmMILE', '2015-11-17 01:51:02', NULL, 0),
(140, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 01:51:23', NULL, 0),
(141, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 01:52:39', NULL, 0),
(142, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 02:09:33', NULL, 0),
(143, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 02:09:47', NULL, 0),
(144, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 02:51:36', NULL, 0),
(145, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 02:51:46', NULL, 0),
(146, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 04:33:56', NULL, 0),
(147, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 04:34:32', NULL, 0),
(148, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 04:35:44', NULL, 0),
(149, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 04:37:02', NULL, 0),
(150, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 05:51:07', NULL, 0),
(151, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 05:52:31', NULL, 0),
(152, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 07:07:20', NULL, 0),
(153, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 07:26:12', NULL, 0),
(154, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'W5mojJE0kIxgjpQRwUEhiULXFQhDQa', '2015-11-17 07:26:28', NULL, 0),
(155, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-17 07:29:11', NULL, 0),
(156, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 07:32:29', NULL, 0),
(157, 'new_dir', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'zpusEsG1CNsn4AUeUBTyqHh0ZHcYf4', '2015-11-18 07:32:57', NULL, 0),
(158, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 07:32:57', NULL, 0),
(159, 'new_dir', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'C8khbf2ptV9QBfEwc4k3FOPcz5JqQs', '2015-11-18 07:33:18', NULL, 0),
(160, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 07:33:18', NULL, 0),
(161, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-18 07:36:16', NULL, 0),
(162, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '5KzQQ5M7fjcnRnpHt6IOaYpE9I0CTe', '2015-11-18 07:37:57', NULL, 0),
(163, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'xa4flq9UiJuvpfoIWridqQGKqF7NvL', '2015-11-18 07:45:14', NULL, 0),
(164, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Yxc6fZnd6V66K3W5Cnag6ayA9Ej0rh', '2015-11-18 07:47:12', NULL, 0),
(165, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '62CIDHhZUb18daIG9bbuoASGdl8yXK', '2015-11-18 07:57:32', NULL, 0),
(166, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Wi6L1rkIwyprLFNRTRxkDrw4S1Eku1', '2015-11-18 08:08:21', NULL, 0),
(167, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 07:37:42', NULL, 0),
(168, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'QTOLGVHhvuyi0C2NdJff3bf6F893ie', '2015-11-18 07:43:24', NULL, 0),
(169, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 07:43:32', NULL, 0),
(170, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'pHeXcjBTxVHcfNsaRrlyN3gWGnVWx3', '2015-11-18 08:11:31', NULL, 0),
(171, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '977Kl9kYxiM00jxIrwDDVQWyREz9On', '2015-11-18 08:11:39', NULL, 0),
(172, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:12:17', NULL, 0),
(173, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:12:38', NULL, 0),
(174, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'pHeXcjBTxVHcfNsaRrlyN3gWGnVWx3', '2015-11-18 08:13:07', NULL, 0),
(175, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:13:35', NULL, 0),
(176, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:14:17', NULL, 0),
(177, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '62CIDHhZUb18daIG9bbuoASGdl8yXK', '2015-11-18 08:14:25', NULL, 0),
(178, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '62CIDHhZUb18daIG9bbuoASGdl8yXK', '2015-11-18 08:14:28', NULL, 0),
(179, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Yxc6fZnd6V66K3W5Cnag6ayA9Ej0rh', '2015-11-18 08:16:48', NULL, 0),
(180, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Yxc6fZnd6V66K3W5Cnag6ayA9Ej0rh', '2015-11-18 08:16:52', NULL, 0),
(181, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'pHeXcjBTxVHcfNsaRrlyN3gWGnVWx3', '2015-11-18 08:17:17', NULL, 0),
(182, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:24:22', NULL, 0),
(183, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:24:25', NULL, 0),
(184, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'pHeXcjBTxVHcfNsaRrlyN3gWGnVWx3', '2015-11-18 08:24:34', NULL, 0),
(185, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'rv8ffP6BiP8b35j4uPsKnZvya0V198', '2015-11-18 08:26:12', NULL, 0),
(186, 'upload', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'On40mISDaJwDNTlOFMLWH3g5rRWbd7', '2015-11-18 08:27:01', NULL, 0),
(187, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:27:08', NULL, 0),
(188, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:27:11', NULL, 0),
(189, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'rv8ffP6BiP8b35j4uPsKnZvya0V198', '2015-11-18 08:27:29', NULL, 0),
(190, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:28:06', NULL, 0),
(191, 'delete_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'xa4flq9UiJuvpfoIWridqQGKqF7NvL', '2015-11-18 08:28:30', NULL, 0),
(192, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:28:41', NULL, 0),
(193, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'On40mISDaJwDNTlOFMLWH3g5rRWbd7', '2015-11-18 08:29:24', NULL, 0),
(194, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'On40mISDaJwDNTlOFMLWH3g5rRWbd7', '2015-11-18 08:29:24', NULL, 0),
(195, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-18 08:29:59', NULL, 0),
(196, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-18 08:30:20', NULL, 0),
(197, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-18 08:30:20', NULL, 0),
(198, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-18 08:30:22', NULL, 0),
(199, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-18 08:31:59', NULL, 0),
(200, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-18 08:31:59', NULL, 0),
(201, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-18 08:32:00', NULL, 0),
(202, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-26 04:30:43', NULL, 0),
(203, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-26 04:30:54', NULL, 0),
(204, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-26 04:30:57', NULL, 0),
(205, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'ZZ32euGuaTaZf7twwB2UHt7wnAnZ1F', '2015-11-26 04:31:03', NULL, 0),
(206, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'ZZ32euGuaTaZf7twwB2UHt7wnAnZ1F', '2015-11-26 04:31:06', NULL, 0),
(207, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-26 04:31:12', NULL, 0),
(208, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '2015-11-26 04:31:14', NULL, 0),
(209, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:45:42', NULL, 0),
(210, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '977Kl9kYxiM00jxIrwDDVQWyREz9On', '2015-11-28 10:48:13', NULL, 0),
(211, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:49:09', NULL, 0),
(212, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:53:35', NULL, 0),
(213, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:54:26', NULL, 0),
(214, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:54:28', NULL, 0),
(215, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-28 10:54:45', NULL, 0),
(216, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', '2015-11-28 10:54:45', NULL, 0),
(217, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-28 10:55:07', NULL, 0),
(218, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-28 10:55:07', NULL, 0),
(219, 'view_file', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', '2015-11-28 10:55:35', NULL, 0),
(220, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:56:13', NULL, 0),
(221, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-28 10:56:15', NULL, 0),
(222, 'delete_share', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '5KzQQ5M7fjcnRnpHt6IOaYpE9I0CTe', '2015-11-28 10:57:04', NULL, 0),
(223, 'share', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', '5KzQQ5M7fjcnRnpHt6IOaYpE9I0CTe', '2015-11-28 10:57:05', NULL, 0),
(224, 'list_files', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'Home', '2015-11-29 09:52:23', NULL, 0),
(225, 'generate_download_link', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-29 09:52:35', NULL, 0),
(226, 'open_or_download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-29 09:52:35', NULL, 0),
(227, 'download', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '2015-11-29 09:52:39', NULL, 0),
(228, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 05:17:51', NULL, 0),
(229, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:17:59', NULL, 0),
(230, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:17:59', NULL, 0),
(231, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:18:01', NULL, 0),
(232, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 05:18:45', NULL, 0),
(233, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:18:59', NULL, 0),
(234, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:18:59', NULL, 0),
(235, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:19:00', NULL, 0),
(236, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:19:14', NULL, 0),
(237, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:19:14', NULL, 0),
(238, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:19:15', NULL, 0),
(239, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 05:38:34', NULL, 0),
(240, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:38:42', NULL, 0),
(241, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:38:42', NULL, 0),
(242, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:38:44', NULL, 0),
(243, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:42:14', NULL, 0),
(244, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:42:14', NULL, 0),
(245, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 05:42:16', NULL, 0),
(246, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 06:34:59', NULL, 0),
(247, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 06:35:39', NULL, 0),
(248, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', '2015-12-01 06:35:44', NULL, 0),
(249, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', '2015-12-01 06:35:44', NULL, 0),
(250, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 06:35:59', NULL, 0),
(251, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 06:35:59', NULL, 0),
(252, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 06:36:00', NULL, 0),
(253, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 06:42:34', NULL, 0),
(254, 'generate_download_link', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 06:42:47', NULL, 0),
(255, 'open_or_download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 06:42:47', NULL, 0),
(256, 'download', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', '2015-12-01 06:42:49', NULL, 0),
(257, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 06:43:12', NULL, 0),
(258, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 06:43:21', NULL, 0),
(259, 'list_files', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'Home', '2015-12-01 06:43:33', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `file_object`
--

CREATE TABLE IF NOT EXISTS `file_object` (
  `id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `original_name` varchar(255) NOT NULL,
  `full_path` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `mime` varchar(20) NOT NULL,
  `size` bigint(20) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `owner` varchar(100) NOT NULL,
  `storage_key` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent` int(11) DEFAULT NULL,
  `in_trash` tinyint(1) NOT NULL DEFAULT '0',
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_image` tinyint(1) NOT NULL DEFAULT '0',
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `file_object`
--

INSERT INTO `file_object` (`id`, `hash`, `original_name`, `full_path`, `type`, `mime`, `size`, `extension`, `owner`, `storage_key`, `created`, `parent`, `in_trash`, `is_home`, `is_image`, `description`) VALUES
(1, 'uEbpjESLL46PzTEQXmffuzfitcMpEt', 'yes-i-am-a-Web-designer1.jpg', 'Home/yes-i-am-a-Web-designer1.jpg', 1, '268', 25805, 'jpg', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', '2015-11-10 01:39:00', NULL, 0, 0, 1, NULL),
(2, 'KHAVXboYWKG6ejTDXUoAOMm1PxEead', 'ic_abstract.png', 'Home/ic_abstract.png', 1, '500', 6481, 'png', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', '2015-11-10 01:39:00', NULL, 0, 0, 1, NULL),
(3, 'Pfnmhd79dmlkmw96y4EgS7NXJH26ak', '47CloudComputing.pdf', 'Home/47CloudComputing.pdf', 1, '24', 902435, 'pdf', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', '2015-11-10 01:40:57', NULL, 0, 0, 0, NULL),
(4, 'gyCvSp7APg4qq8bXohY74RtKBtUvLQ', 'laravel-5_0-docs.pdf', 'Home/laravel-5_0-docs.pdf', 1, '24', 1077778, 'pdf', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', '2015-11-10 01:41:01', NULL, 0, 0, 0, NULL),
(5, '7d0t6gvUNLgfuuabofTGmz3IkUQR9T', 'briefing-47.doc', 'Home/briefing-47.doc', 1, '320', 62464, 'doc', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', '2015-11-10 01:41:45', NULL, 0, 0, 0, NULL),
(6, 'emPstv5vzyCeDnt8sfVIiEypVmpTTu', 'Domain-6.docx', 'Home/Domain-6.docx', 1, '352', 132655, 'docx', 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 'TestandoI0M9YjTPMekxN4e67avkCtJ8YLgMkZvz', '2015-11-10 01:41:46', NULL, 0, 0, 0, NULL),
(7, 'lwZBegPD17Wv5ZHZyHKhuVHstmMILE', 'b47558033743530bd261dfba00ae3aa2.jpg', 'Home/b47558033743530bd261dfba00ae3aa2.jpg', 1, '268', 21745, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 05:40:15', NULL, 0, 0, 1, NULL),
(8, 'k189i9lLDubIiGnQiTJOkJyr1oVuO0', '47772_Papel-de-Parede-Botao-de-Volume_1280x720.jpg', 'Home/47772_Papel-de-Parede-Botao-de-Volume_1280x720.jpg', 1, '268', 79477, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 05:40:15', NULL, 0, 0, 1, NULL),
(9, 'W5mojJE0kIxgjpQRwUEhiULXFQhDQa', '114687_papel-de-parede-mulher-e-violoncelo_1280x960.jpg', 'Home/114687_papel-de-parede-mulher-e-violoncelo_1280x960.jpg', 1, '268', 157460, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 05:40:15', NULL, 0, 0, 1, NULL),
(10, 'z34YFXg63vH1Jql2PbazQREv00DVlv', '280863_Papel-de-Parede-Caixa-de-Som-na-Floresta_1280x720.jpg', 'Home/280863_Papel-de-Parede-Caixa-de-Som-na-Floresta_1280x720.jpg', 1, '268', 235202, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 05:40:15', NULL, 1, 0, 1, NULL),
(11, '3GVgcp3sQFUzDlvspmCEgJBnVG26Sw', 'luna-nebo-noch-oblaka-pejzazh-27859.jpg', 'Home/luna-nebo-noch-oblaka-pejzazh-27859.jpg', 1, '268', 25442, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:11', NULL, 1, 0, 1, NULL),
(12, 'X8RHYPF5MVvpw14OU3FEDr8L0mOaFf', 'ba12.jpg', 'Home/ba12.jpg', 1, '268', 274725, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:22', NULL, 1, 0, 1, NULL),
(13, '8Jq4aZhvN1uUsGUYJ8DY9yqse06KcB', 'Nailson_Moura_Emblematico_Sertao_DSC4283.jpg', 'Home/Nailson_Moura_Emblematico_Sertao_DSC4283.jpg', 1, '268', 357283, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:25', NULL, 1, 0, 1, NULL),
(14, 'fgHG9gC8DQURAZnErg1Axp4t6Cu5Jy', 'Judas02.jpg', 'Home/Judas02.jpg', 1, '268', 527665, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:34', NULL, 1, 0, 1, NULL),
(15, 'lYNqRJ2rtDODoBKRnAReGnpSVTwFcC', 'ibitiguaia-res.jpg', 'Home/ibitiguaia-res.jpg', 1, '268', 789709, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:38', NULL, 1, 0, 1, NULL),
(16, 'f6VTnirf5ACgOHPfuxfS0Zq3XTgAVH', 'sertao_sem_fim_f_025.jpg', 'Home/sertao_sem_fim_f_025.jpg', 1, '268', 153973, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:41', NULL, 0, 0, 1, NULL),
(17, 'lceubbOhrMl52jMllNSjROLPXprGmp', 'timthumb.jpg', 'Home/timthumb.jpg', 1, '268', 85895, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:48', NULL, 0, 0, 1, NULL),
(18, 'YgDGG2GyAHzSwtzB3CSwUwt2GQHwyW', '4.jpg', 'Home/4.jpg', 1, '268', 1422830, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:50', NULL, 0, 0, 1, NULL),
(19, 'm3tSnL7H5fSKNVoWNWPvM6TLfcHjdy', 'SSF0035.jpg', 'Home/SSF0035.jpg', 1, '268', 280613, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:30:52', NULL, 0, 0, 1, NULL),
(20, '3fZj71Yrb7O51k72SbyxzXhYYSOjaP', 'Contrato de Prestação de Serviços de Armazenamento Digital de Dados Online.docx', 'Home/Contrato de Prestação de Serviços de Armazenamento Digital de Dados Online.docx', 1, '352', 18846, 'docx', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:31:40', NULL, 0, 0, 0, NULL),
(21, 'QTOLGVHhvuyi0C2NdJff3bf6F893ie', 'clara boia.jpg', 'Home/clara boia.jpg', 1, '268', 29486, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:31:40', NULL, 0, 0, 1, NULL),
(22, 'Rfc5Yc6FspVsPJeIB3kicAmi85lzmD', 'boleto atmosfera musical.pdf', 'Home/boleto atmosfera musical.pdf', 1, '24', 116654, 'pdf', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:31:42', NULL, 1, 0, 0, NULL),
(23, 'qCZ7406ZX47ksRszLr2ff5cI9yIvtS', 'aranao.rar', 'Home/aranao.rar', 1, '525', 15548929, 'rar', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-10 08:34:35', NULL, 0, 0, 0, NULL),
(24, '7XjWNt9VilJ0ZmoCRbcRsVPaR9dPMx', 'corretor-nuvem.jpg', 'Home/corretor-nuvem.jpg', 1, '268', 55915, 'jpg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:17:21', NULL, 0, 0, 1, NULL),
(25, '4bp6m5iAOmAAnf69WJMDdcExq652JD', 'FreeGreatPicture.com-32253-technology-news-1024x846.jpg', 'Home/FreeGreatPicture.com-32253-technology-news-1024x846.jpg', 1, '268', 139952, 'jpg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:17:24', NULL, 0, 0, 1, NULL),
(26, 'PmgXSND3vCrjh1yNnLdhReQLKxvIcQ', 'corelynx_blog_51.jpg', 'Home/corelynx_blog_51.jpg', 1, '268', 198838, 'jpg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:17:24', NULL, 0, 0, 1, NULL),
(27, 'JLrGZkcVM2CSll6KTZwIgyZ1V7VoAY', 'Cloud-Computing-1.jpg', 'Home/Cloud-Computing-1.jpg', 1, '268', 1005269, 'jpg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:17:32', NULL, 0, 0, 1, NULL),
(28, 'UxOHijOZW4Wx9VmJlZUAvimDtG1Cmg', 'banheiro.jpg', 'Home/banheiro.jpg', 1, '268', 72426, 'jpg', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:17:47', NULL, 0, 0, 1, NULL),
(29, 'K28Ik4mAHlYBY89X1nYzD0QwxJCiFZ', 'Portal do Parceiro atmosfera musical - Sistema Hiper.pdf', 'Home/Portal do Parceiro atmosfera musical - Sistema Hiper.pdf', 1, '24', 131212, 'pdf', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:17:49', NULL, 0, 0, 0, NULL),
(30, '9BGjtpJxwAXJbzJcuN84Dhdyc4BORz', 'teste S3.docx', 'Home/teste S3.docx', 1, '352', 12866, 'docx', 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 'armazenamentoLZVkta1nok7WJEDj45VUnrb2Dsq0bQv0', '2015-11-10 22:20:39', NULL, 0, 0, 0, NULL),
(31, 'ZZ32euGuaTaZf7twwB2UHt7wnAnZ1F', '07ee834.jpg', 'Home/07ee834.jpg', 1, '268', 53976, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:16:48', NULL, 0, 0, 1, NULL),
(32, 'W4EvOZIe2W7MVyJSTlLlQlCM8tY3jd', 'maxresdefault.jpg', 'Home/maxresdefault.jpg', 1, '268', 50463, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:16:48', NULL, 0, 0, 1, NULL),
(33, 'KPK4iA7xm6Jau1m2cSMB2Gg1Og3bj2', '7set2014---indio-faz-selfie-durante-a-4-edicao-dos-jogos-tradicionais-indigenas-realizado-no-distrito-de-maruda-municipio-de-marapanim-distante-cerca-de-160-km-de-belem-no-para-indios-d.jpg', 'Home/7set2014---indio-faz-selfie-durante-a-4-edicao-dos-jogos-tradicionais-indigenas-realizado-no-distrito-de-maruda-municipio-de-marapanim-distante-cerca-de-160-km-de-belem-no-para-indios-d.jpg', 1, '268', 120350, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:16:50', NULL, 0, 0, 1, NULL),
(34, '17DX0mjThc20T6l8QVL5ZhU68CM95v', 'harold-golen-gallery-before.jpg', 'Home/harold-golen-gallery-before.jpg', 1, '268', 126245, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:16:52', NULL, 0, 0, 1, NULL),
(35, 'LzxwMiWGHf38757UReRHw0N4ObZ0d2', 'galeria.jpg', 'Home/galeria.jpg', 1, '268', 215867, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:16:54', NULL, 0, 0, 1, NULL),
(36, 'E4XmdSHWBS4F5kk8BgFTUzfA8JQb8b', 'Natal-Dunas-Jenipabu-2.jpg', 'Home/Natal-Dunas-Jenipabu-2.jpg', 1, '268', 235560, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:16:55', NULL, 0, 0, 1, NULL),
(37, 's4VmqRXGLNwlpk7SqEICbmucMC7N1l', 'superbox-full-5.jpg', 'Home/superbox-full-5.jpg', 1, '268', 29440, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:05', NULL, 0, 0, 1, NULL),
(38, 'dlTEGwOpdMg0UwLdQWL00SLoolgZKA', 'superbox-full-1.jpg', 'Home/superbox-full-1.jpg', 1, '268', 71730, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:05', NULL, 0, 0, 1, NULL),
(39, 'TtDVQZ1E8RKGMKKwzx5oS1B8YAdpu0', 'russia_2744410b.jpg', 'Home/russia_2744410b.jpg', 1, '268', 79780, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:07', NULL, 0, 0, 1, NULL),
(40, 'TGLv7BwbUQPf5yNALIKO0eLYKQgSsd', 'pirâmides-egito-gizé.jpg', 'Home/pirâmides-egito-gizé.jpg', 1, '268', 182705, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:07', NULL, 0, 0, 1, NULL),
(41, 'jDrgGCRpI6ZmzVevyGeCCxqCl6DANq', 'superbox-full-7.jpg', 'Home/superbox-full-7.jpg', 1, '268', 45229, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:08', NULL, 0, 0, 1, NULL),
(42, 'Mtsh6fTG2wcz06KOKwwMIvYlAi89gm', 'o4x1zkrsusq.jpg', 'Home/o4x1zkrsusq.jpg', 1, '268', 330951, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:09', NULL, 0, 0, 1, NULL),
(43, 'MvMk7hoCrbcu9otAB7XRaXQYsz2NXg', 'superbox-full-14.jpg', 'Home/superbox-full-14.jpg', 1, '268', 55100, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:16', NULL, 0, 0, 1, NULL),
(44, 'r3KEf8ig94joewL7XtH1YxWiQbTyuE', 'superbox-full-18.jpg', 'Home/superbox-full-18.jpg', 1, '268', 71723, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:16', NULL, 0, 0, 1, NULL),
(45, 'a8ePIAVAwCr4E9k7gYfjUv6Q6ULK5i', 'tesouro-de-fenn.jpg', 'Home/tesouro-de-fenn.jpg', 1, '268', 89491, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:17:18', NULL, 0, 0, 1, NULL),
(46, '10QCEMUtJ0WMQxg3vnRVyHQFeosG3Y', 'doc.docx', 'Home/doc.docx', 1, '352', 0, 'docx', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:23:26', NULL, 1, 0, 0, NULL),
(47, 'BjMe7qXhMPIvGf28wGoHGH8jXA9vx8', 'PPT.pptx', 'Home/PPT.pptx', 1, '346', 0, 'pptx', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:23:26', NULL, 0, 0, 0, NULL),
(48, 'B7CiwMz5hNTWaQsGw6fKsWZtBmAwDS', 'Planilha.xlsx', 'Home/Planilha.xlsx', 1, '350', 6310, 'xlsx', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-11 11:23:27', NULL, 0, 0, 0, NULL),
(49, 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', '00003.jpg', 'Home/00003.jpg', 1, '268', 287132, 'jpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-15 11:39:09', NULL, 0, 0, 1, NULL),
(50, 'zpusEsG1CNsn4AUeUBTyqHh0ZHcYf4', 'doc', 'Home/doc', 2, '687', 0, NULL, 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:32:57', NULL, 0, 0, 0, NULL),
(51, 'C8khbf2ptV9QBfEwc4k3FOPcz5JqQs', 'Financeiro', 'Home/Financeiro', 2, '687', 0, NULL, 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:33:18', NULL, 0, 0, 0, NULL),
(52, 'Hx8wkVDgXIblGNwdjw0Y3KeoLS58nj', 'panamericano.mp3', 'Home/panamericano.mp3', 1, '404', 2237577, 'mp3', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:36:16', NULL, 0, 0, 0, NULL),
(53, '5KzQQ5M7fjcnRnpHt6IOaYpE9I0CTe', 'Apresentacao - Introducao Informatica.ppt', 'Home/Apresentacao - Introducao Informatica.ppt', 1, '346', 1239552, 'ppt', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:37:57', NULL, 0, 0, 0, NULL),
(54, 'xa4flq9UiJuvpfoIWridqQGKqF7NvL', 'Aula 7 Instalando a placa-mãe.mpg', 'Home/Aula 7 Instalando a placa-mãe.mpg', 1, '404', 12894212, 'mpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:45:14', NULL, 1, 0, 0, NULL),
(55, 'Yxc6fZnd6V66K3W5Cnag6ayA9Ej0rh', 'Aula 5 Instalando o processador.mpg', 'Home/Aula 5 Instalando o processador.mpg', 1, '404', 21202948, 'mpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:47:12', NULL, 1, 0, 0, NULL),
(56, '62CIDHhZUb18daIG9bbuoASGdl8yXK', 'Aula 15 Instando CPU LGA 775.mpg', 'Home/Aula 15 Instando CPU LGA 775.mpg', 1, '404', 51924996, 'mpg', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 07:57:32', NULL, 1, 0, 0, NULL),
(57, 'Wi6L1rkIwyprLFNRTRxkDrw4S1Eku1', 'Como funciona o Windows (A Saga completa)-Crédito.avi', 'Home/Como funciona o Windows (A Saga completa)-Crédito.avi', 1, '50', 148893580, 'avi', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 08:08:21', NULL, 0, 0, 0, NULL),
(58, 'pHeXcjBTxVHcfNsaRrlyN3gWGnVWx3', 'Ariana Grande   Problem ft Iggy Azalea..mp4', 'Home/Ariana Grande   Problem ft Iggy Azalea..mp4', 1, '408', 28884962, 'mp4', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 08:11:31', NULL, 1, 0, 0, NULL),
(59, '977Kl9kYxiM00jxIrwDDVQWyREz9On', 'Christina Perri - A Thousand Years [Official Music Video].3gp', 'Home/Christina Perri - A Thousand Years [Official Music Video].3gp', 1, '3', 39734428, '3gp', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 08:11:39', NULL, 0, 0, 0, NULL),
(60, 'rv8ffP6BiP8b35j4uPsKnZvya0V198', 'Husband_vs_Wife.wmv', 'Home/Husband_vs_Wife.wmv', 1, '327', 3341133, 'wmv', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 08:26:12', NULL, 1, 0, 0, NULL),
(61, 'On40mISDaJwDNTlOFMLWH3g5rRWbd7', 'little girl clssc.flv', 'Home/little girl clssc.flv', 1, '163', 7455886, 'flv', 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 'testecomh6ZiPTM6ww2cyxKYwp0v3UZ0bXclHnsI', '2015-11-18 08:27:01', NULL, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `file_type`
--

CREATE TABLE IF NOT EXISTS `file_type` (
  `id_file_type` int(11) NOT NULL,
  `desc_file_type` varchar(200) DEFAULT NULL,
  `mime_file_type` varchar(20) DEFAULT NULL,
  `ext_file_type` varchar(5) DEFAULT NULL,
  `ico_file_type` text
) ENGINE=InnoDB AUTO_INCREMENT=689 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `file_type`
--

INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(1, 'Desconhecido', 'unknown', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAO3klEQVR4Xu2dX4hcVx3Hz7lzdzbNFqqY2fVBsMaCoNLUhxpRqpAXhT5YCoqiDz6EBoQGKsH2IZmdjJFUfKglCGmhDz6o1EKfWhGFPpYUxMYqGrWUPphiMptCakx2d2bOkdn8MTa7mzt/7sz3d8/HV++953u+n9/97Oye3dQ7/kcDNJBsAz7ZnbNxGqABhwAYAhpIuAEEkDB8tk4DCIAZoIGEG0AACcNn6zSAAJgBGki4AQSQMHy2TgMIgBmggYQbQAAJw2frNIAAmAEaSLgBBJAwfLZOAwiAGaCBhBtAAAnDZ+s0gACYARpIuAEEkDB8tk4DCIAZoIGEG0AACcNn6zSAAJgBGki4AQSQMHy2TgMIgBmggYQbQAAJw2frNIAAmAEaSLgBBJAwfLZOAwjg2gwsHYoL4Y7+g967fcHF+6Lr786c/4Bz2RxjMnoDnfbctjPWaK4/2mnXT4y+AneO00DyAlhqre6OMX+8H/vfyly2ME6Z3HtrA7cXwFovutpXV9r5y/Q3/QaSFcA9j8b5ix/sLTsXDvFVvrzBu70AujGEcCnL/QOdVv10eUl48mYNJCmApcNXPtZ1+Yu1zN3HWJTbQBEBXEtwNvTzvRd+6M+Wm4in39xAcgJYaq19utdzv8uy7MOMQvkNDCGAQZjXXZZ/sdPyl8pPxgqDBpISwOArf89lr/LyT2/4hxSAczG81DlTf8i94PvTS5nuSskIYPA9/7t39U7xsX+6wz60AJxzwfkTF9r5wekmTXO1ZATQWO4ed9E9kSbm2e16FAFcTRsPcjxYPrckBDA46gvBn+Gn/eUP1PtXGF0Aoc/xYPm8khDA4nLvmRjjI+XXyQqTE4BzHA+WP0+VF8DgN/x6O9fP8Us+5Q/TZiuM/gngxtM4HiwRXeUF0DjS+7rz8fkSO+TR2zQwAQFwPFjihFVeAIvN3sno4oESO+TR5QuA48GSpiwBAXRfi859tqT+eOxtGpjQJ4CNVTgenPy4VV4Auw6vdnxW2zX56nhikQYmKYCr63E8WKT3otdUXgCN5to6x39Fx2Hy101eABwPTpJSAgLoxkkWxrOGa2DyAuB4cDgC21+NACbZJs+6pYEyBHBtEY4HJzBvCGACJfKIrRsoUQCDRfnrwTGHDwGMWSC3b99AyQLgeHDMAUQAYxbI7TMWAMeDY40gAhirPm6+XQOlfwK4EYDjwdux2Oz/RwCjtMY9hRuYngA4HiwM5aYLEcAorXFP4QamJwCOBwtDQQCjVMU9ozQwTQFcy8fx4BCg+AQwRFlcOnwDMxDAICTHgwVRIYCCRXHZaA3MSAAcDxbEhQAKFsVlozUwMwFwPFgIGAIoVBMXjdrALAVwNTPHg9uxQwCjTjb3FWpg9gLgeBABFBpVLiqjgdkLgONBBFDGZPPMQg0oCOBaUI4HNyHGtwCFxpiLRm1ASACDLXA8+D6QCGDUyea+Qg2ICYDjQQRQaG65aEINyAmA48H/I8sngAkNOo/ZvAFFAVxNyvHgoAUEwJtbagO6AuB4EAGUOvo8fNCArgA4HkQAvKOlN6AsAI4H+Rag9Bcg9QUMCCDp40F+BpD6G1ry/o0IINnjQQRQ8guQ+uPNCCDR40EEkPobWvL+LQkgxeNBBFDyC5D64+0JIK3jQQSQ+hta8v7tCSCt40EEUPILkPrjLQogpeNBBJD6G1ry/g0LIInjQQRQ8guQ+uONC6Dyx4MIIPU3tOT9d7J83rX8+lbLLB5eW4tZVi85xliPD86fuNDOD471ENGbEYAomKrEqnfzXWeP+wtb7afR7J5xzn1Cf7/V/OtBBKA/eaYTRh/vXzla//2WAlju/dTF+F39TVbzeBAB6E+e8YRxf6ddf27LbwGW1+6N0Z12LpOfxRDCpSz3D3Ra9dPGodyIL1/6uEU3mt047jO4f/QGfIg/P3+s/u3tnrDY7J2MLh4YfZWp3lmpf1wUAUx1dhJcLIT3FvL60tstv7rV7u9uxR3/Cb3fOOe+ZKShyvzjogjAyMTZjum/02nnP9tuDx95LN6xvtB/Omb9/Ra+HXAxvNQ5U3/IveD7ltkgAMv07GT/W+ev+aeKvCyNI+uf8T47EF3c50P4qPIRYRWOBxGAnZfIdNLo4vdW2vWnTG+iguERQAWhKm4phHA55u7+d1vzf1HMl2omBJAq+VnsO7q/z9XyL7zT8iuzWJ41b20AATAV020guD/U+vlX/nXcd6a7MKtt1gACYC5m0cA/siw8fK41/+dZLM6a/2sAATANM2kguHClFrPm+XP50+5Z351JCBblvwzEDMy2gdD3b/os/Khem/vFOy1/ebZp0ludTwDpMZfc8cbv2df8r2N0r2TRv+H6+Vs76+7idr9BKLkRY6EQgDFgxNVq4Hb/4IlWWk4B1HmQz1gDCEAcGH8NKA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDRADigIzHQwDiABGAOCDj8RCAOEAEIA7IeDwEIA4QAYgDMh4PAYgDbDTX1p3L5sRjEs9gAz6E9fPH5ucNRr8R2VsOXyT7h5qr5zNXaxS5lmtoYJgGgut3LrR3LA5zj9q1lRfArmb3lHdur1rx5LHfQHTutZX23Ocs76TyAlhs9k5GFw9YhkR2zQa898+eP5qbnq3KC6DR7H3NufgrzREile0G/Dc67fx5y3uovACWDsWF3s71c5nLFiyDIrtWAyGEy1leX+q0/CWtZMOlqbwABnUsLveeiTE+Mlw1XE0D2zQQ/HOdY/l+6x0lIYCl1uruEPwZjgOtj6tK/tB1Pn6yc3THmyqJRs2RhAAG5TSa3Sedc4+PWhT30cD1BqJzP15pz32/Co0kI4C7W3HHe6F3qubcniqAYw+zaSAG98adeb737ZZfnU2Cya6ajAAGtQ2+FeiF7NXM+aXJ1sjTUmhg8Is/mXefr8JH/+u8khLAxg8Ej6zv6Uf32yzzpn+DK4UXTmmPGy9/zL7c+UH9daVc42ZJTgAbEji8+vHgai/6zN07boHcX/0GgnN/ynz/4Sp95U/2E8D1jQ9+JnAp9o76GB7jdKD6L/FoOwzd6LKf7Ph3vvzPp/yV0Z6hfVeSnwBuRtJYXr3H9fMngut/M8uyndq4SDeNBjZ+ycfVfulqvSer+FX/5g6TF8D1MhqteKcL/Qe9d/tCjHui6++uBX9XzLL6NIaONWbTwOBPevtZvOhd7a3M+z/G6F5xWe1l67/hV7RNBFC0Ka6jgQo2gAAqCJUt0UDRBhBA0aa4jgYq2AACqCBUtkQDRRtAAEWb4joaqGADCKCCUNkSDRRtAAEUbYrraKCCDSCACkJlSzRQtAEEULQprqOBCjaAACoIlS3RQNEGEEDRpriOBirYAAKoIFS2RANFG0AARZviOhqoYAMIoIJQ2RINFG0AARRtiutooIINIIAKQmVLNFC0gf8Ct83UTPynJ6QAAAAASUVORK5CYII='),
(2, '3D Crossword Plugin', 'application/vnd.hzn-', '.x3d', ''),
(3, '3GP', 'video/3gpp', '.3gp', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAeEElEQVR4Xu2de5BkVX3Hf79zb/fM7vKI7PTsSlkEFnwjrAqsgibl/iGWVCqGRBQw8QEsGgsUVECdR0/PWrAKAmIirIhaJoAhkkpKLB4lplIWCPhgg8oCyyNVsNmZ7sHgvma67z2/1J19sMtu33Nvn3u6+879zr99zu/x+f3ud073ufdcJvyBAAgUlgAXNnMkDgIgQBAANAEIFJgABKDAxUfqIAABQA+AQIEJQAAKXHykDgIQAPQACBSYAASgwMVH6iAAAUAPgECBCUAAClx8pA4CEAD0AAgUmAAEoMDFR+ogAAFAD4BAgQlAAApcfKQOAhAA9AAIFJgABKDAxUfqIAABQA+AQIEJQAAKXHykDgIQAPQACBSYAASgwMVH6iAAAUAPgECBCUAAClx8pA4CEAD0AAgUmAAEoMDFR+og0DUBqFTlENLhGcy0mkRWah2uYMWHE6kSytA5gXqtFFvDyljzonqtfEPnHjBzIRNwLgDDI7PHEvmXhxSeq5RavJBh9iI3swDMBULeXzZq/l29iA8++5uAMwE4uiqD2yUYp1B/npTy+xtDfqMzC0BLtNbblM/vrlfLj+Y3U0TugoATAVhWnV3R0t6dHtGJLoKGzZcJJBGA3aNf0KG/auYr/AL4gcAeApkLwLLRubcEzPcp4mXA7J5ACgGIgvkNKf/P6lXe5j4yeMgDgUwFIPrPH2j1AC7+7pU+pQAQif5xfWP5A3QHh92LEp76lUBmAhB95/+jDn6BZX93S51aAIhIE98wU/Mv7m6k8NaPBDITgMpY6yoiurwfk1zIMXUiALt4yMXYHlzInZEst0wEINrqE+KN+LU/GfQsR3UuADrE9mCWlcinrYwEIFgvSi7IJ4J8R925ABBhezDftc8iemsBiO7w00FzCjf5ZFGO9DZsBGC3N2wPpse+YGbYC8BY8CEiuX3BEMlZIhkIQJQxtgdzVveswrUWgOHx4CYRWZNVQLCTjkBGAoDtwXTYF8xoewEYaz0kRKcsGCI5SyQzAcD2YM4qn0241gJQGZtrEKml2YQDK2kJZCkAu3xjezBtDfI8PgsBaOKR3t61QPYCgO3B3lWz+54zEICWdD9seNxDIHsBwPZgkboLApDzarsQgN1IsD2Y895IEj4EIAmlPh7jUACirLE92Me1zyI0CEAWFHtow7EAYHuwh7XthmsIQDcoO/ThXACwPeiwer03DQHofQ2sIuiGAGB70KpEfT0ZAtDX5TEH1z0BwPaguRr5GwEByF/N9ou4ewKA7cGct8pBw4cA5Lyq3RQAbA/mvFkOEj4EIOc17YEAYHsw5z2zb/gQgJwXs0cCgO3BnPfNnvAhADkvZM8EANuDOe+cXeFDAHJexl4KALYHc948EID8F7D3AoDtwTx3EVYAea4eEfVeALA9mOcWggDkuXp9IgDYHsxvE0EA8lu7+cj7YQWwD0I8PZizfoIA5Kxgrwy3zwQA24M56ycIQM4K1vcCgO3BXHUUBCBX5Tow2L5bAewNEYeL5qG1IAB5qFJMjP0rANgezENrQQDyUKVcCgC2B/PQWhCAPFQppwKA7cH+by4IQP/XKDbC/v0KsF/Y2B7s0z6DAPRpYZKGlRMBwPZg0oJ2eRwEoMvAs3aXGwHA9mDWpc/EHgQgE4y9M5InAdhFCduDveuWAz1DAPqpGh3Ekj8BwPZgB2V2NgUC4AxtdwznTwCwPdidzkjmBQKQjFPfjsqjAGB7sH/aCQLQP7XoKJIcC0CUL7YHO6p6dpMgANmx7ImlnAsAtgd70jUvO4UA9LgAtu7ryh+gKjfb2RkemZsTpcq2flzO18Q3zNT8i136gO2DE4AA5LwzSsqvbK5yo10albHWRiJ6ff+nie3BXtQIAtAL6hn61CKnzEyWH2krAOPBP5DI32fo0pEpbA86AhtrFgLQC+oZ+hSRCxqT5ZvbfgUYnztBhB4lUta1zjDsg5rSWm9TPr+7Xi0/6toX7O8iYN0UlbGWAGbvCDDLrdMT5XPjIhgeC24Ukgt7F2Uqzy/o0F818xV+IdUsDO6IAASgI2z9NElvHdhaXvb8tbyzXVRHV2Vwuw7uJqI/76fIY2LB9mCXCgUB6BJop25EPl6fLH8vzsdrLpFFzSXh9aLC8/PwdYBE/7i+sfwBuoNDp+wKbhwCsBAaQOjJuue/maocmNKpjDbfyqwuFJLVrPWf9vMWIbYHTdW0/xwCYM+wLyyI0Ocak6Wv90UwCCI3BCAAuSlVfKCa9M6S0MlbJgd+t0BSQhpdIAAB6ALkLrp4ymv5p225kutd9AlXOSYAAchx8dqE/hvF/vumJnh64aWGjLImAAHImmgf2NMhb/JVeObU5MBjfRAOQuhjAhCAPi6OTWha61lPqbFp5V8f97CQjQ/MzT8BCED+axibgSZ+mkmv83aUbp26mrcv8HSRXkoCEICUwPI6XJPerkj9hEXuJ+IN4vnPLiH6v+eqNEfEuJ07r4W1jBsCYAkQ090SMB144tb7wrcOAVj4Nc51hhAAt+WDALjlC+uWBCAAlgAN0yEAbvnCuiUBCIAlQAiAW4Cw7pYABMAtX6wA3PKFdUsCEABLgFgBuAUI624JQADc8sUKwC1fWLckAAGwBIgVgFuAsO6WAATALV+sANzyhXVLAhAAS4BYAbgFCOtuCUAA3PLFCsAtX1i3JAABsASIFYBbgLDulgAEwC1frADc8oV1SwIQAEuAWAG4BQjrbglAANzyxQrALV9YtyQAAbAEiBWAW4BJrGsK64rVz0TTg8zqCdLBpnI48OKibbRt0w08l8QGxoCACwJYAbigOm9T/4HIu1WJ/sHUZOlhnLrjDDQMWxCAAFjAO9hUrfUWZvU19vz19Spvy9g8zIFApgQgAFnh1DoQT11HO/1a46u8NSuzsAMCLglAADKgO3/yrtJnNarlX2dgDiZAoGsEIACWqFnoPwLP/9sXq/xHS1OYDgJdJwABsEHOfHOdvU8leS23jRvMBQFXBCAAnZKNLv4Jbw1+3e8UIOb1AwEIQAdVEKJ/byj/b9L853/V5XJ4qRz+hSh6D4mcyFofI4oOJVKlDkLAlMIQ0C0i+iORepaYHxVNP6U5766sfmiGAKRvpKdC5Z+U9Dv/0Mjs68Xzr6Aw/LBSajC9O8wAgf0JaK13KPJuIy+4qj4xuMmGDwQgDT2tA/L55Hq1/Khp2pFVWRzoYK2QvphIeabx+BwE0hPQLSF13eBWf/z5a3ln+vlEEIB01NbVa6UrTFOi//qKvTuF6U2msfgcBGwJaKLHFIdndrIagAAkpa9ps5r1X2d6w+7QaPPtJPpuVt5QUtMYBwK2BOafNxF1en2y/Js0tiAACWmJyCWNyfJ1ccOj//xE9HNc/AmhYlimBOZFQNNp9bWDTyU1DAFIQIq1fpFny0fF/fdf9nlZohcHjxDRGxOYxBAQcEIg+jqwaKu/KulvAhCABGXQxDfM1PyL44ZWxlrR6uAzCcxhCAg4JcBE10zXSp9P4gQCkICSFjllZrIc/Xc/6N/S6twblKbf4tf+BDAxxD2B+QfT5M2NicEnTc4gAAZCmmRqplZ6ddwdf5Xx5vdJ+O9MsPE5CHSLgGb+3syE/3GTPwiAgRBruW16bfmcdsOiO/xUqbkFN/mYWg2fd5OAJr1TVHm56YY1CICxKvLZeq18fbthw6PBR4TlB0YzGAACXSYgwuc2Jv1b49xCAEwrAKL3TddK97T9/j8S3KKUGJdaXa493IEAkebv1Nf650MALJqBdXjc9NrBp9uZqIy0fkWK3mbhAlNBwBWBR+q10ikQAAu85ZY/9MKVPNP2K8DI3IwodYSFC0wFAScERIeNxtrBCgTAAm9d+QNU5WbbFcDYXBOP9FoAxlSHBHSrXhsoQwAsEJteTFEZa4mF+RRT9VbW/GOt6GfCtGGAS89sjp4TjxGneeNVKR9JdNictFawppUstFqTnKGUOiSF8wyG7h9/qVl6dssAvWSM39KzqT6m+lq6n+e/fI4OF7+1ImRayUyrtcgZitQSa9sJDJjyw4+ABogmgKYGS1Cj+CFCTxLJuoFtpduS3t5p8hndthwubp0jpC5XJMeaxlt9vjv+kle6fXOVd1jZ6mCyqT6m+nbg0jilUpVDJGydo7W63PNkhXGCxQBTfhCAPhWA6NAHVjzSUKUb0pw8lKpX1kipsjy4VGtdzfo+hmgfmom/7DT+BMn2owDsDbsq5UoYXKpZVxWpgQTppB4CAUiNbP8JJoCmBuvQ/ROh0me+WB34fYfzU00bHp87IRTvzsxWA0JPhp7+q27FH5esqT6m+qYC2eHg4dHmiYFWd7pYDZjywwqg/1YAvyq3/NPjdh467LPYacvGZbglwb0e0YlW9jX9uuT7p2+ucsPKTkaT8yAAUarDX5JlWgX3sqITMkp93gwEwJKmCaCpwVK5F9pYDvx3dfvi3xNjJAKBhA90vBIQerLk+af1y8Uf5WWqj6m+qepnOTgSgYDDB7JcCZjywwqgT1YAmvR20nTyzNqBxy37yGr6/NeBkB5K+5tA9JuF+HRyPyz79wWQJwGYXwmMNk8MWR7K6jcBCIDV5WBeQpkaLKn7JCcOJbVlO254tHWZMK1LY0dILm3UytemmdONsab6mC6QbsT4Sh+VsdblRHRVFr5N+WEF0A8rAKGN9Y3+8XQHh6aiD403T1KhWhMKvcfj8ChRKv5GD62botT/MPH9Wun1id5fuEZKS5eHj6f4KvBEXfnHJ9mt2Bu/otWKwqN6fROV6QIxCYipXtwJ/6qUj2iFj2fxVcCUHwSgHwSA+GP1mv/9uFCivftgSfhNJfIxU9PFfq75OwPbvYtM9xQMjTbPZ+ZvJ/Mln6jXyt/tSvzJAko8ynSB2ArAAYEk5T/WvICJ1ydOpM1AU34QgJ4LgH5piSovf67Ks+1C2X3e4H1E9E7bhojmC9HPB7f6740TgXnBGZw/58Bwx6DeWlLl5XE3+czbWhzcq4hOzSL+LG2YLpDMBSAh/+hmIa2bW2zvGDTlBwHosQCwln+eXlv+SFwYS8eD71r/53+lg/l3G/oXxPmtjDd/SMJnxY0xHZgSza2MNL9Hij+a5YWblS3TBeJCAOZjT8J/rPUvRPRBm1xN+UEAeiwARHJ+vVb+TrswovcMMPMvbZrg4HO1kPDb486RHxprXsjEN8b5FpI1jVq57VeF6Ds/C7c9TzH7vNJZNF0gzgSAzPyHR5ufFOZvpcto/9Gm/CAAPRYAYTm5MVFue4EPjwc3icgamyZoN1eYb2xM+J9q+9VjvPkOLfxgnG/NsmpmovxwuzHDI8F6URK70nCRW1KbpgvEnQAQmfgPj7beKUwPJM3lYONM+UEAeiwApvMGKmOt6GTX19o0QczcJ+q10hvaC4AMawmm4nx7LX94y5Vcbzdm6ZeDp5QnxzmK39qs6QJxKQBEFMs/ujFI/GCLTZKm/CAAPRYA03kDwyNzc6atvk4bRJOem6kNtH1j8XEXycBLrwra/jgZ+TXFX+nz8xJMF4hLAciCv6n2pvwgAD0WAFMBXX5uakCqil/RQfR++rZ/pgZzeQFlwaaX8Zv4JxFgEwNTfhCAAgsARTcgTZbavsrsyKoMtXTQdnk/vwKolWJ7CAIQ02AG/tGzGaavYBAAEwHLz/Pe4LHpM/9jfcL/dLsxSX7BzzufnsZv4L8swY+wpvY25YcVQGFXAFpY+K3Tk+UNbQUgwd1opgbDCqAd3UT8jduwEAATAcvP897g7dI3bUFF8yrjzdtI+MP4DcCyiQ4yPSF/441YpshM/YsVQDFXAP+1RPmnx91+/JpLZNHckuYWUuowCIDpMkv9uZH/rtunm1O4FTg122wnmBS035e4+9PQwuStX6y8z8Zd/PP//UebnyDmtnco7rGbdz7djT85/3QPY7XveVN+WAEs8BVAtNWkRD1Liu9nrdfHfeffi2LXYaG/S3IDkqnB+l0gXcffEf+qlJfq8PcpHsdu28Wm/CAAORcAU4E7WQ8NjbU+x0RXJ5lr8m8SANP8JDHEjem1/07ix4EgnVBzNMfUoKYGcxTWXrOm+NL6X1adOz7Q9LAitSjJXJN/Ex/T/CQxLCQBwJFgthXPeL6pQU0NnnE4B5gzxZfG//IvSqVVCh9Ms/Q0+TfxMc1PE//Bxvbaf5r4cShoGlpdGmtqUFODuQ7TFF9S/9HF3ywF96U9Ftzk38THND9p/O3G9dp/0vgrVVke6uDutPxN9k188RsAfgOYJxDd9jsbBPd5ilaammrfz00N1usLsNf+k7CsjDbfKqx+xCTHJBmfZoypPhAACMBeApEIzLXCB9M8vmtqsF5fgL32H9deux/2uZS1rrp64tNUHwgABGA/AstH597cYnoEPwKm+T+bbuzQZXKoGmidq1ld5uK/fpoVGgQAAnAAgaHR1qXMdE2Stjb9h+n1f+De+hc+ukoD24n+hClYQaGsDJlXE+n3JxXYJDWIG2OqDwQg5wJgapAOz6WPzgGIXkxqPInI1GCmC9AUv+lzW/+2803x9fpzU34QgAUuAAekl/Bc+rzcCmxqcJMA2c7v9QVu8m/KDwJQNAFIeC59Xh4GMjU4BCD+wBYIQAEFYD7lJOfS5+BxYAhAfAOb+EAAiioACc6lT/JEmqnBTP+BTUtY0+e2/m3nm+Lr9eem/CAAhRUA87n0eTgSzNTgJgGynd/rC9zk35QfBKDAAmA6lz4Ph4KaGhwCgN8ATCIZ+7ltg1k5dzzZdCx1Ho4Ft62P7XzHJbI2b8oPK4AerwBML9bAi0HsfuTCCgArACsVNSmoqcFMzkvKr2yucqPdOKevBjOcSx89IRiWgum4HBT7y6YmuO0Y168Gs62P7XxTfXv9uSk/rAB6vALQIqfMTJbbvj13eCy4UUgudNJIpnPpR5urNPMv4gVA3jk1UW47xuXLTaO4TA1uEmjb+U7qkqFRU34QgB4LgIhc0Jgs39wujKFq822s+VcZ9sRuUwnOpR9trmHmm+J8s8inpifLbV8h7i7+XVGZGhwCgK8AVteObYMZnbPcXp8onx03rjIS3ExKzjPaSjEg0bn0I83bSfGH4sxqoX+dmSx9sNvx7/FnWx/b+SmQ92SoKT+sAHq8AiDSWwe2lpc9fy3vbBdKdFvu7KHBvUz0roy6yHgu/ZFVWTwXNKeUUofECoDWO5RfXlav8rZ24yJbTR3ck2H8e12ZGhwrAKwArK4Z2wZL5FzkvPpk+Za4sdFF1ArCb9itBJKfS18Za55HxG2/muwbq5B8slErx35VmI9fwutJ5PxETBIOsq2P7fyEYfZsmCk/rAB6vgKYD+CpuvLfRFUOTJ0SfacmURewyHtY62NMJ8l0dC79GiktrYS/T3oykBA/21DeG6jKzcTxa1lNrI8hUiXTnLjPTQ2OFQBWADb9Zf0jU1LnLPSF6clSorP4k9rsdNzwaOsyYVqXar7QF+uTpatSzenCYAgABMCqzWz/wyR1rknv9IVWTU0OPJZ0jotxw+NzJ4QhPaSUGkxjf36lofgd9Wr50TTzXI+FAEAArHqsWwIQBalD3uT73mlxN9ZYJWOYvGxchgMJH0jzXoD9fwvgZ1l5p9arvMVlnGlsQwAgAGn65YCx3RSAyHmo6dGS55/ebRGYfymFCu5Oeyz4K4FposeU8t/bLyIAAYAA5EoA5lcCxE97HJ45PTHw31bBJ5xcqTZXhi31I8+TFQmnxA6LfhRkpc/sh68DEAAIgFVPd3sFsCdYrfWsx2p8esq/ltZzyyqJdpOrUq6EwaWadVWRGsjSx/xvAqQm6sq/JsnuQJa+97UFAYAAWPVWrwRgrxAQP82k13k7SrdOXc3brZLZPblSlUNEt87Vobosq//67eKKVgMk+ir2SrfG3SyURV4HswEBgABY9VavBeBlIdDbFamfCMlPPaYNRKVnDn2RXtp0A8/FJXh0VQZ3NOlw8oIVxLIyFF5Not+vlFpsBSblZK31DmL1E4/kp0S8gUL/mcVleum5Ks0RsaQ0l3g4BAACkLhZDjbQ9Lx+ZWyuaXszi1WAmAwCbQhE74SYXjsQ+9UOdwIa2qfc8odeuJJn2g0bHpmbEaWOQBeCQL8R0BTWZ2qDw3FxQQBMVePwtfWJwU3thlXGWr8korebzOBzEOg2ASZ6eLpWWgUBsCDPRO+brpXuaSsADh7VtQgXU0FgLwHW/O3ptf4aCIBVU8hn67Xy9e1MDI0F5zLJP1m5wGQQcEBAhM9uTPq3QwBs4BoO7DiiKoexbm7p1ttebVLB3OIQiHZdTOc0RDTwG4ChJzTJ1Eyt9Oq4raqlI8EtSsnHi9NeyLTvCSR49RsEIGEVTQd3Do3Pvo5D/h0p5Sc0iWEg4JCAbrGWN06vHXza5AQrABOhXTL5jfpE6TNxQ4fHWlcL0eeSmMMYEHBMYF29VroiiQ8IQBJKpGdKqnzU5irvaDd81+u0g4dJ0fGJTGIQCDggEBJtOEz573iuyrNJzEMAklCKxoh8pj5Z/kbc8Mr47HFa6AFFXiWpWYwDgawIRL9X+UqfOlUdfCapTQhAUlJEL6gd/utND+REj9bqgO5RimPvwEruFiNBwEwguvg9odOnJ8sbzKNfHgEBSEOL6Mp6rfQl05ThkdljNXl3sqITTGPxOQjYEpg/RMYP/zrNf/49PiEAqejrFjOdlOSgjugpvG0STLDoS/CwUCrIGJyYgG4Rq2sOf9Gvmp4KbWcSApAY9t6BT5DyT0r6bHu0GhDPv0KH4TndfgQ3fWqYkQcCmvR2T3u3sh9c1cl//X1zhAB0UnGhO+sb/bPoDg6TTo8O4SAdnsFMq0lkZUjhMZ7mw03n+ie1j3ELk0D0SG+o5CUm7xnFvEGE7ifl3ZX0H5CJCgTARKjN50L8rUbN+7TLwyw6DA3TQCAxAQhAYlQHDpwXgce9i9KsBCzcYSoIZE4AAmCP9N9k1v9o46u81d4ULIBAdwlAALLgLfQkefKhfjgGO4t0YKM4BCAAWdVa60A89XVmfzKrH2iyCg12QKAdAQhA5r2h/1eIv+btKK033TWYuWsYBIGUBCAAKYElHc5avyied7si/YMpLj1MVdZJ52IcCHSLAASgC6Sj01kVef8pJA8yqyeEgk1+c+APgwO01fW5+F1IDy5yTAACkOPiFSF004tZisDAZY4QAJd0YduaAATAGmGsAQiAW76wbkkAAmAJ0DAdAuCWL6xbEoAAWAKEALgFCOtuCUAA3PLFCsAtX1i3JAABsASIFYBbgLDulgAEwC1frADc8oV1SwIQAEuAWAG4BQjrbglAANzyxQrALV9YtyQAAbAEiBWAW4Cw7pYABMAtX6wA3PKFdUsCEABLgFgBuAUI624JQADc8sUKwC1fWLckAAGwBIgVgFuAsO6WAATALV+sANzyhXVLAhAAS4DuVwBzTbz6ym2Rimtdt+q1gXJx83efufUKYGhkts7KG3IfKjwUjYDosNFYO4hXrTssvLUADI+1HhKiUxzGCNMFJcBED0/XSqsKmn5X0rYXgPHgJhFZ05Vo4aRQBFjzt6fX+ugth1W3FoDKaHAWsfzQYYwwXVACInx2Y9K/vaDpdyVtewGoyiE6aE7h1dddqVdhnGjSO3m2vAyvXHNbcmsBiMIbHgnWi5IL3IYK64UiwHxLfcI/r1A59yDZjARg9lgh3khK+T3IAS4XGgGtAyJ5U33t4FMLLbV+yycTAYiSGhpvrWOhy/otQcSTPwLCdHVjovSF/EWev4gzE4DXXCKLZpcEv2BFJ+QPAyLuFwKa6LFFW/1Vz1/LO/slpoUcR2YCsOu3gNljQ1IPKMXDCxkacnNDILrxh4lOxdLfDd+DWc1UACIHlWpzpdb6XkUe7uDqXh1z7ym6+MlXpzeq5V/nPpkcJZC5AMyLwPjscVq8OxXRW3LEAqH2ioCm3xKFZ+I/f/cL4EQAojTmfxM4NJhgrS/B7kD3C5sLj1oH4qnryuyPb67yjlzEvMCCdCYAezhVRmZfS55/hZbwbEVq0QLjh3Q6IBDd5KPYu43C4Cr81+8AYIZTnAvAnliHLpNDaSA8QwmtJiUnah2uYMWH41HiDKvZl6Z0S7S8pJT3DGneoJnupznvLtzh1x/F6poA9Ee6iAIEQGBfAhAA9AMIFJgABKDAxUfqIAABQA+AQIEJQAAKXHykDgIQAPQACBSYAASgwMVH6iAAAUAPgECBCUAAClx8pA4CEAD0AAgUmAAEoMDFR+ogAAFAD4BAgQlAAApcfKQOAhAA9AAIFJgABKDAxUfqIAABQA+AQIEJ/D8Dr8fT+AnFWQAAAABJRU5ErkJggg=='),
(4, '3GP2', 'video/3gpp2', '.3g2', ''),
(5, '3GPP MSEQ File', 'application/vnd.mseq', '.mseq', ''),
(6, '3M Post It Notes', 'application/vnd.3m.p', '.pwn', ''),
(7, '3rd Generation Partnership Project - Pic Large', 'application/vnd.3gpp', '.plb', ''),
(8, '3rd Generation Partnership Project - Pic Small', 'application/vnd.3gpp', '.psb', ''),
(9, '3rd Generation Partnership Project - Pic Var', 'application/vnd.3gpp', '.pvb', ''),
(10, '3rd Generation Partnership Project - Transaction Capabilities Application Part', 'application/vnd.3gpp', '.tcap', ''),
(11, '7-Zip', 'application/x-7z-com', '.7z', ''),
(12, 'AbiWord', 'application/x-abiwor', '.abw', ''),
(13, 'Ace Archive', 'application/x-ace-co', '.ace', ''),
(14, 'Active Content Compression', 'application/vnd.amer', '.acc', ''),
(15, 'ACU Cobol', 'application/vnd.acuc', '.acu', ''),
(16, 'ACU Cobol', 'application/vnd.acuc', '.atc', ''),
(17, 'Adaptive differential pulse-code modulation', 'audio/adpcm', '.adp', ''),
(18, 'Adobe (Macropedia) Authorware - Binary File', 'application/x-author', '.aab', ''),
(19, 'Adobe (Macropedia) Authorware - Map', 'application/x-author', '.aam', ''),
(20, 'Adobe (Macropedia) Authorware - Segment File', 'application/x-author', '.aas', ''),
(21, 'Adobe AIR Application', 'application/vnd.adob', '.air', ''),
(22, 'Adobe Flash', 'application/x-shockw', '.swf', ''),
(23, 'Adobe Flex Project', 'application/vnd.adob', '.fxp', ''),
(24, 'Adobe Portable Document Format', 'application/pdf', '.pdf', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAZqklEQVR4Xu2de4wk1XWHz7lV3TPLgjeG6VmIkMPLxk9YAgYHy4myfwRkFMXCDg6Qh5B5BFu8DAGCZmd6eoYANjZgZBsWjBwRAQ7JRolMBEQhUWTzTMwS7HiB5RGJxTvTMxDY10x31T1RDSwssN23um/d6rpTv/m365577ndOf327b08XE/5AAARKS4BLu3IsHARAgCAANAEIlJgABFDi4mPpIAABoAdAoMQEIIASFx9LBwEIAD0AAiUmAAGUuPhYOghAAOgBECgxAQigxMXH0kEAAkAPgECJCUAAJS4+lg4CEAB6AARKTAACKHHxsXQQgADQAyBQYgIQQImLj6WDAASAHgCBEhOAAEpcfCwdBCAA9AAIlJgABFDi4mPpIAABoAdAoMQEIIASFx9LBwEIAD0AAiUmAAGUuPhYOgjkJoBaXfYlHZ/CTGtJZI3W8WGseBWRqqAM/RNoNipda1gbb13QbFRv7n8GjFzOBJwLYHRs4XCi8IqY4jOVUvssZ5iDWJtZAIuRUPAHc43wvkHkhzmLTcCZAA6py/AOiSYo1peRUmGxMfibnVkAbdFab1chf65Zr270d6XI3AUBJwJYXV84rK2DDQHR0S6SRsx3CKQRwFtXb9FxeML81bwF/EBgN4HMBbB63eKnIuZ/UcSrgdk9gR4EkCTzJKnwt5t13u4+M8zgA4FMBZC88kdaPYwnf36l71EARKJ/3NxU/QLdy3F+WWKmohLITADJe/43dPQotv35lrpnARCRJr55vhFemG+mmK2IBDITQG28fS0RXVHERS7nnPoRwJs85EIcDy7nzki3tkwEkBz1CfEmfNqfDnqWV/UvAB3jeDDLSvgZKyMBROtFyTl+IvA76/4FQITjQb9rn0X21gJIvuGno9YMvuSTRTl6j2EjgLdmw/Fg79iXzQh7AYxHXyaSe5YNEc8WkoEAkhXjeNCzumeVrrUARieiW0Xk3KwSQpzeCGQkABwP9oZ92VxtL4Dx9mNCdPyyIeLZQjITAI4HPat8NulaC6A2vjhHpA7IJh1E6ZVAlgJ4c24cD/ZaA5+vz0IALfxL7+BaIHsB4HhwcNXMf+YMBNCW/NPGjLsJZC8AHA+WqbsgAM+r7UIAbyHB8aDnvZEmfQggDaUCX+NQAMmqcTxY4NpnkRoEkAXFAcZwLAAcDw6wtnlMDQHkQdnhHM4FgONBh9UbfGgIYPA1sMogDwHgeNCqRIUeDAEUujzm5PITAI4HzdXw7woIwL+avSvj/ASA40HPW2Wv6UMAnlc1TwHgeNDzZtlL+hCA5zUdgABwPOh5z+yZPgTgeTEHJAAcD3reN7vThwA8L+TABIDjQc875830IQDPyzhIAeB40PPmgQD8L+DgBYDjQZ+7CDsAn6tHRIMXAI4HfW4hCMDn6hVEADge9LeJIAB/a7eUeRF2AHsgxH8PetZPEIBnBXtvugUTAI4HPesnCMCzghVeADge9KqjIACvyvX+ZAu3A3g7Rfy4qA+tBQH4UKUuORZXADge9KG1IAAfquSlAHA86ENrQQA+VMlTAeB4sPjNBQEUv0ZdMyzuW4B3pY3jwYL2GQRQ0MKkTcsTAeB4MG1Bc74OAsgZeNbTeSMAHA9mXfpM4kEAmWAcXBCfBPAmJRwPDq5b3j8zBFCkavSRi38CwPFgH2V2NgQCcIY2n8D+CQDHg/l0RrpZIIB0nAp7lY8CwPFgcdoJAihOLfrKxGMBJOvF8WBfVc9uEASQHcuBRPJcADgeHEjXvDMpBDDgAthO31ThENW51SnO6NjioihVtZ3H5XhNfPN8I7zQ5RyIvXcCEIDnnVFRYe2VOs91WkZtvL2JiI4s/jJxPDiIGkEAg6Ce4Zxa5Pj5qeoTHQUwEX2XRL6a4ZSOQuF40BHYrmEhgEFQz3BOETlnbqp6e8e3ABOLR4nQRiJlXesM095rKK31dhXy55r16kbXcyH+mwSsm6I23hbAHBwBZrlrdrJ6ZrcMRsejW4TkvMFl2dPMW3QcnjB/NW/paRQu7osABNAXtiIN0tuGtlVXv3wD7+qU1SF1Gd6ho/uJ6HeKlHmXXHA8mFOhIICcQDudRuSs5lT1h93mOPgSWdFaGd8kKj7bh7cDJPrHzU3VL9C9HDtlV/LgEMByaAChZ5tB+Amqc2RaTm1d6xhmdZ6QrGWtf6PIR4Q4HjRV0/5xCMCeYSEiiNClc1OVbxciGSThDQEIwJtSdU9Uk95VEfr01qmhXyyTJWEZORCAAHKAnOMUzwXt8LNbr+FmjnNiKo8JQAAeF69D6k8qDk+emeTZ5bc0rChrAhBA1kQLEE/HvDlU8akzU0NPFyAdpFBgAhBAgYtjk5rWeiFQanxWhTd1+2chmzkw1n8CEID/Ney6Ak38PJO+LthZuWvmet6xzJeL5fVIAALoEZivl2vSOxSpf2aRh4j4KQnCF1cS/d9LdVokYnyd29fCWuYNAVgCxHC3BEw/eOJ29uUfHQJY/jX2eoUQgNvyQQBu+SK6JQEIwBKgYTgE4JYvolsSgAAsAUIAbgEiulsCEIBbvtgBuOWL6JYEIABLgNgBuAWI6G4JQABu+WIH4JYvolsSgAAsAWIH4BYgorslAAG45YsdgFu+iG5JAAKwBIgdgFuAiO6WAATgli92AG75IrolAQjAEiB2AG4BIrpbAhCAW77YAbjli+iWBCAAS4DYAbgFiOhuCUAAbvliB+CWL6JbEoAALAFiB+AWYJromuKmYvVvoukRZvUM6WhzNR56dcV22r75Zl5MEwPXgIALAtgBuKC6FFO/RhTcpUTfOTNVeRy/uuMMNAJbEIAALODtbajWeiuz+iYH4fpmnbdnHB7hQCBTAhBAVji1jiRQN9KusDH3Dd6WVVjEAQGXBCCADOgu/fKu0qfN1as/yyAcQoBAbgQgAEvULPRPURD+yat1fsMyFIaDQO4EIAAb5My3Nzk4P81tuW2mwVgQcEUAAuiXbPLknwzOxaf7/QLEuCIQgAD6qIIQ/eOcCr/Uyyv/B6+QVZVq/Pui6HdJ5GjW+lBRtB+RqvSRAoaUhoBuE9EbROpFYt4omv6VFoP7svqgGQLovZGei1V4XNr3/CNjC0dKEF5JcfxHSqnh3qfDCBB4NwGt9U5Fwd0URNc2J4c32/CBAHqhp3VEIX+6Wa9uNA379brsE+loWkhfSKQC0/V4HAR6J6DbQurG4W3hxMs38K7exxNBAL1Ru67ZqFxpGpK86isONgjTx03X4nEQsCWgiZ5WHJ/az24AAkhLX9MraiH8iOkOuyPrWseS6PtZBSNpQ+M6ELAlsPT/JqJOak5Vn+wlFgSQkpaIXDI3Vb2x2+XJKz8R/QRP/pRQcVmmBJYkoOmzzenh59IGhgBSkGKtX+WF6oe6vfqvvkxW6n2iJ4joYylC4hIQcEIgeTuwYlt4QtrPBCCAFGXQxDfPN8ILu11aG28nu4OLUoTDJSDglAATfWu2UbkszSQQQApKWuT4+alq8uq+178D6osfVZp+jk/7U8DEJe4JLP1jmnxibnL4WdNkEICBkCaZmW9UDur2jb/aROuvSfhPTbDxOAjkRUAz/3B+MjzLNB8EYCDEWu6ena6e0emy5Bt+qtLaii/5mFoNj+dJQJPeJap6oOkLaxCAsSpycbNRvanTZaProj8WljuNYXABCORMQITPnJsK7+o2LQRg2gEQnTzbqDzQ8f3/WHSHUmLcauVce0wHAkSaf9CcDs+GACyagXV8xOz08POdQtTG2v9Fin7TYgoMBQFXBJ5oNirHQwAWeKvtcGTLNTzf8S3A2OK8KLW/xRQYCgJOCIiO5+amh2sQgAXepgqHqM6tjjuA8cUW/qXXAjCGOiSg283GUBUCsEBsujFFbbwtFuHJFN8m9tLYulQPXKRVErYPi5nWMNNaLXKKIrXSOjYR8s8CYpcYrvsLHwIaCmh6groukIv+qtVlX4nbZ2itrggCOcxmDhMfm9gdd12e598LE9f9BQGUUABvL7ku1VocfV2zritSQ7005u5rByGAd+Wvo0s16Qkv808BHAJIAcnlJaYGd10gl2vbHXt0XevoSKsN/ewGTHzyyL+2rnWM1rRBBXxIr/MVIf9uObvuL+wAyrwD2GPto1fJaq2iB1nRUb08iYryBBq5Sg5iFT1Iij7pY/4d3+44/owJAoAA3iaQSCDi+OFedgJFEUCyiEQCFMY/ZZJD00qgSPnvLWfsANJW0tF1pgZxXSBHy+oYNnk7ELM8lvY9tYlP3vknbwdY5FFRquvxVyE+w0gBx3V/YQeAHcD7CNTG21cQ0bUp+tP9MWCaJN5zTW1d+ypiujrN0KIJ7L05QwBpqujwGlODuC6QbXzWuiVK/S8TP6SVXp/q/oV1qe7fjn+Z5q2Aaz795H/EBTL02gfam9J8KOg6f4etmSq0aX3YARR8B2ArgPctT/MPhnYEF5h+MmpkvHUOE683dZmpwQaV/+i61p8L8/cLl78poYwfN9UHAiibAIhIiH4yvC38vW4SSL4spHVrq+kbg6YGy1wAKfMfuVz2k+HWjCK1oluJB5F/xs/xruFM64MASiiApSUv3dswPKfb8mvj7b8loj8s5BMoRf4HjLf/ThF9sZD552QBCMAStAmg7Suc6/idl6+FhI/t9jvyabbRBc//q8L8XQigMwHsAMq6A0i20sy3zE2G53dCMLqu/VvC9HBRn0DG/OvtE0XTT4uav+VrU6rhJkFDACUWABE902xUPtpRAFfJagmjrQV+AnXNv1aXA0lHvypw/qmexDYXQQA29FL8u6u/bwGINOnF+cZQxzsWJ8dpr38wWijqEyg5IpydHur4T0yH1GV4h4663jTT9ASxra9l+1kPN60PO4AS7wB8F0BSuu4NLlwbj3RRBWb97E4RAAJIAWmQDWIqkNNXIKFNzalKx1uZrZ6QUS3RzCD5mMpny892vCm/QT9uWh92ACXeARDz95qT4dc6IVg90fqMFn4EAhj007j/+SGA/tktjTQBtH2Fdh2/8/K1sPAxs1PVpzpdMzLeOo+Jb4EALJtogMNN/YUdQEl3AKYjtARLbaL1IxI+DQIY4DPYcmoIwDFAT3cA/7FShSe9VOeOn/AntzuP9ln6Km3XHw81NZgtH1P5bOe3HW/Kb9CPm9aHHUCpdgBamIL1+6jg4m5P/gTJyLrW2cx8m6mBTQ0GAZgIun3cVB8IYJkLIDnqU6JeJMUPsdbru73nfxtFXaoH6Ph/FMnhpvY0NRgEYCLo9nFTfSCAggvAbXvsPXqWPwjiuwBc8x80HwgAAngXgax/EmzQDW6a3/QKCQEYCJgAuwboOr6pQWzXb4rven17xnfxo6C2fEzrN/EzzW8ab5rf9nFTfrbxTevDDgA7gCUCyT/OxDq6PyA6upemMzXYoBvcNL8p/15Y9HOtKb9+Yu45xrQ+CAACoOSXdIXV3/fyc9q7sZkabNANbprflL/tE9A03pSfabzpcdP6IIASC+Ct//b7OmtdT/sz2u/FZWqwQTe4aX5T/qYnmO3jpvxs45vWBwGUUADJ7+WpofaZmtXl/bzq97LFHHSDm+Y3PUFsn4Cm8ab8TONNj5vWBwEsawEIH1KnoR1Ev8YUHUaxrImZ1xLpz5t+LNPUWHgLkJZQ9+sggGw4OotiMqhtAV3HdwbmrcCDzt92ftvxrvnaxjetDzuAgu8AbAVj20Cm8aYGc52/7fy24018Bv24aX0QAARg1aOmBoMArPBaDzbVBwKAAKyazNRgEIAVXuvBpvpAABCAVZOZGgwCsMJrPdhUHwgAArBqMlODQQBWeK0Hm+oDAUAAVk1majAIwAqv9WBTfSAACMCqyUwNNlgB4GfBTfWBACAAbwVguq8Bbgxi/lFbCAAC8FYAZLivAW4NBgFYNXcy2LSFst3iuo5vDWDAguw6veG+BqO4Oaixf7EDGHCDQwD9Ksp8X4PR8db5Qvy9bjP4zt9Ez7Q+CAACMPVQ18dNDWa7Q+o0eZr7Ghywrn2vYvoSBNCZAAQAAfgoAON9DWp12VdHrRml1D4QAATQd5O7foVzHb/vhaccmG/+Pd3X4FxmvtW0jHzzN2WT/eOm9WEHgB2AVdeZGsz2LUC/9zUY0fGmND924jp/K7gZDDatDwKAAKzazNRgVsH7HFwbb/8lEf1VmuGm/G0FliYHl9eY1gcBQABW/WdqMKvgfQyu1VtrtJZHFamhNMNN+UMABoq+AzI1iesGcR3ftD7bx03528bvZbyP9zXoZX39XGuqD3YA2AH001dvjzE1mFXwHgb7el+DHpbY16Wm+kAAEEBfjbV7kKnBrIKnHOzzfQ1SLrHvy0z1gQAggL6bKxloajCr4KbBdanWdHSpz/c1MC3R9nFTfSAACMCqx0wNZhW8w+DkSz6i22cSqSvSHPV1y8GUv++fcZnWBwFAAFbPUVODWQWnPe5rEEeHEsuaWHgtif686Rt+aec15Q8BGEj6DsjUKE0VDlGdW52uq40vtohUxRQHj4NA3gRY69bs9FDX41DsAAxVqbbDkS3X8Hyny0bHFudFqf3zLi7mAwETAU1xc74xPNrtOgjARJHjDzcnhzd33gG0/5OIjjWFweMgkDcBJnp8tlE5AQKwIM9EJ882Kg90FMBYdDsp+YrFFBgKAk4IsObbZqfDcyEAK7xycbNRvalTiJHx6Ewm+RurKTAYBBwQEOHT56bCeyAAG7gs9zQnq6d3CrF/XT7AurU1q7vt2qSKsSCwm4DWeqcKq6ubdd4OAVj0hSaZmW9UDiJi6RTmgLHoDqXkLItpMBQEsiXAfHtzMjzHFBQfApoIEZEWOX5+qvpEx7cBEwsf4Zh/QUqFKcLhEhBwTEC3WcvHZqeHnzdNBAGYCCWPM32nOVm5qNulo+Pt64Xo0jThcA0IOCZwXbNRuTLNHBBAGkqk5yuq+qFX6ryz0+UHXyIrFldGj5OiT6YKiYtAwAGBmOipD6jwMy/VeSFNeAggDaXkGpGLmlPV73S7vDaxcIQWelhRUEsbFteBQFYEks+rQqVPnKkPv5A2JgSQlhTRFrUzPHLmet7RVQLJL9JE9IBS3PUbWOmnxZUgYCaQPPkDoZNmp6pPma9+5woIoBdaRNc0G5WrTENGxxYO1xRsYEVHma7F4yBgSyDWtLESxl/s5ZV/95wQQE/0dZuZjpudHPpv07DkxpTbJZpk0Zfgn4VMtPB4fwR0m1h9a9WrYX3zzbzYTwwIoHdqz5AKjzN9wWJ32GQ3IEF4pY7jM7L6F9beU8aI5URAk94R6OAuDqNr+3nV35MFBNBPZwhtaG4KT6N7OU47PPkRC9LxKcy0lkTWxBQfGmheJUpV08bAdeUjkPxLb6zkdabgBcX8lAg9RCq4L+0LkIkYBGAi1OFxIf7+XCP4WrdvCPYZGsNAIDcCEIAF6iUJ/DK4oJedgMV0GAoCmROAAOyR/oMshH829w3eZh8KEUAgXwIQQBa8hZ6lQL7crFc3ZhEOMUAgLwIQQFaktY4kUN9mDqey+oAmq9QQBwQ6EYAAMu8N/Ssh/maws7Le9K3BzKdGQBDokQAE0COwtJez1q9KENyjSN85w5XHqc467VhcBwJ5EYAAciCd/DqrouDfheQRZvWMULQ5bA29NjxE216q0yKOEnMoAqbYKwEIAI1RaAKmG3cUOnkPkoMAPChSmVOEANxWHwJwyxfRLQlAAJYADcMhALd8Ed2SAARgCRACcAsQ0d0SgADc8sUOwC1fRLckAAFYAsQOwC1ARHdLAAJwyxc7ALd8Ed2SAARgCRA7ALcAEd0tAQjALV/sANzyRXRLAhCAJUDsANwCRHS3BCAAt3yxA3DLF9EtCUAAlgCxA3ALENHdEoAA3PLFDsAtX0S3JAABWALEDsAtQER3SwACcMsXOwC3fBHdkgAEYAnQ/Q5gsYVbX7ktUnmj63azMYQbpzhsAOsdwMjYQpNVMOIwR4QuKQHR8dzc9DBute6w/tYCGB1vPyZExzvMEaFLSoCJHp9tVE4o6fJzWba9ACaiW0Xk3FyyxSSlIsCab5udDtFbDqtuLYDauug0YvmRwxwRuqQERPj0uanwnpIuP5dl2wugLvvqqDWDW1/nUq/STKJJ7+KF6mrccs1tya0FkKQ3OhatFyXnuE0V0UtFgPmO5mT4lVKteQCLzUgAC4cL8SZSKhzAGjDlciOgdUQkH29ODz+33JZWtPVkIoBkUSMT7etY6PKiLRD5+EdAmK6fm6z8hX+Z+5dxZgI4+BJZsbAyepQVHeUfBmRcFAKa6OkV28ITXr6BdxUlp+WcR2YCePOzgIXDY1IPK8Wjyxka1uaGQPLFHyY6EVt/N3z3FjVTASQT1OqtNVrrBxUF+AZXfnX0fqbkyU+hOmmuXv2Z94vxaAGZC2BJAhMLR2gJNiiiT3nEAqkOioCmnxPFp+KVP/8COBFAsoylzwT2iyZZ60twOpB/Yb2YUetIAnVjlcOJV+q804ucl1mSzgSwm1NtbOHDFIRXaolPV6RWLDN+WE4fBJIv+SgO7qY4uhav+n0AzHCIcwHsznXkctmPhuJTlNBaUnK01vFhrHgV/pU4w2oWMpRui5bXlQpeIM1PaaaHaDG4D9/wK0axchNAMZaLLEAABPYkAAGgH0CgxAQggBIXH0sHAQgAPQACJSYAAZS4+Fg6CEAA6AEQKDEBCKDExcfSQQACQA+AQIkJQAAlLj6WDgIQAHoABEpMAAIocfGxdBCAANADIFBiAhBAiYuPpYMABIAeAIESE4AASlx8LB0EIAD0AAiUmMD/Ay+sOMQfVi9TAAAAAElFTkSuQmCC'),
(25, 'Adobe PostScript Printer Description File Format', 'application/vnd.cups', '.ppd', ''),
(26, 'Adobe Shockwave Player', 'application/x-direct', '.dir', ''),
(27, 'Adobe XML Data Package', 'application/vnd.adob', '.xdp', ''),
(28, 'Adobe XML Forms Data Format', 'application/vnd.adob', '.xfdf', ''),
(29, 'Advanced Audio Coding (AAC)', 'audio/x-aac', '.aac', ''),
(30, 'Ahead AIR Application', 'application/vnd.ahea', '.ahea', ''),
(31, 'AirZip FileSECURE', 'application/vnd.airz', '.azf', ''),
(32, 'AirZip FileSECURE', 'application/vnd.airz', '.azs', ''),
(33, 'Amazon Kindle eBook format', 'application/vnd.amaz', '.azw', ''),
(34, 'AmigaDE', 'application/vnd.amig', '.ami', ''),
(35, 'Andrew Toolkit', 'application/andrew-i', 'N/A', ''),
(36, 'Android Package Archive', 'application/vnd.andr', '.apk', ''),
(37, 'ANSER-WEB Terminal Client - Certificate Issue', 'application/vnd.anse', '.cii', ''),
(38, 'ANSER-WEB Terminal Client - Web Funds Transfer', 'application/vnd.anse', '.fti', ''),
(39, 'Antix Game Player', 'application/vnd.anti', '.atx', ''),
(40, 'Apple Installer Package', 'application/vnd.appl', '.mpkg', ''),
(41, 'Applixware', 'application/applixwa', '.aw', ''),
(42, 'Archipelago Lesson Player', 'application/vnd.hhe.', '.les', ''),
(43, 'Arista Networks Software Image', 'application/vnd.aris', '.swi', ''),
(44, 'Assembler Source File', 'text/x-asm', '.s', ''),
(45, 'Atom Publishing Protocol', 'application/atomcat+', '.atom', ''),
(46, 'Atom Publishing Protocol Service Document', 'application/atomsvc+', '.atom', ''),
(47, 'Atom Syndication Format', 'application/atom+xml', '.atom', ''),
(48, 'Attribute Certificate', 'application/pkix-att', '.ac', ''),
(49, 'Audio Interchange File Format', 'audio/x-aiff', '.aif', ''),
(50, 'Audio Video Interleave (AVI)', 'video/x-msvideo', '.avi', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAafklEQVR4Xu2df5AlVXXHz73d780sC1Ky82bRoozyw98CifwwGpOSPwJKpWKRFAYxiZawlkmxutEIFWdn3rwZC1BUYJOKIhotE8CiilRS4s8K5g8LFfyxRI0LrGiqXMJOz6zB/TXvve57Um+WBXaZ7tvd997+8fo7/757zzn3c8799u0+Pe8Jwh8IgEBjCYjGrhwLBwEQIAgAigAEGkwAAtDg5GPpIAABQA2AQIMJQAAanHwsHQQgAKgBEGgwAQhAg5OPpYMABAA1AAINJgABaHDysXQQgACgBkCgwQQgAA1OPpYOAhAA1AAINJgABKDBycfSQQACgBoAgQYTgAA0OPlYOghAAFADINBgAhCABicfSwcBCABqAAQaTAAC0ODkY+kgAAFADYBAgwlAABqcfCwdBCAAqAEQaDABCECDk4+lg0BhAtDp8omkokuFoIuI+VylotOFFCcTyRbSkJ9A0Gsl5rAzO7gm6LV35PeAmeNMwLkATM+snkHkXxtRdKWU8oRxhlnG2vQC0A+ZvD9e7vn3lhEffFabgDMBeHGXJw9yOEeR+iBJ6VcbQ32j0wvAkJVSB6Qv3hh02zvru1JE7oKAEwHY3F09fai8ezyic1wEDZvPEEgjAE+N3qMi/8KVj4g94AcCRwlYF4DN2/uvCYX4piSxGZjdE8ggAKNgfkTS//2gKw64jwwe6kDAqgCMrvyhkvdj8xeX+owCQMTqy8Gu9lvpbhEVFyU8VZWANQEY3fP/RoXfxbG/2FRnFgAiUiR2rPT8rcVGCm9VJGBNADqzwxuI6NoqLnKcY8ojAEd48Fa0B8e5MtKtzYoAjFp9TGIXnvang25zVH4BUBHagzYzUU9blgQgvI0lX11PBPWOOr8AEKE9WO/c24jeWABGb/ipcLAXL/nYSEd2GyYC8JQ3tAezYx+bGeYCMBu+jYjvGhsiNVuIBQEYrRjtwZrl3Va4xgIwPRd+mpm32AoIdrIRsCQAaA9mwz42o80FYHb4PSa6YGyI1Gwh1gQA7cGaZd5OuMYC0JntLxPJTXbCgZWsBGwKwBHfaA9mzUGdx9sQgAH+pbe8ErAvAGgPlpfN4j1bEIAhFx82PB4lYF8A0B5sUnVBAGqebRcC8BQStAdrXhtpwocApKFU4TEOBWC0arQHK5x7G6FBAGxQLNGGYwFAe7DE3BbhGgJQBGWHPpwLANqDDrNXvmkIQPk5MIqgCAFAe9AoRZWeDAGodHr0wRUnAGgP6rNRvxEQgPrl7JiIixMAtAdrXirrhg8BqHlWixQAtAdrXizrhA8BqHlOSxAAtAdrXjPPDh8CUPNkliQAaA/WvG6Ohg8BqHkiSxMAtAdrXjlHwocA1DyNZQoA2oM1Lx4IQP0TWL4AoD1Y5yrCCaDO2SOi8gUA7cE6lxAEoM7Zq4gAoD1Y3yKCANQ3d2uRV+EE8CyE+O/BmtUTBKBmCTs+3IoJANqDNasnCEDNElZ5AUB7sFYVBQGoVbqeG2zlTgBPh4gvF61DaUEA6pClhBirKwBoD9ahtCAAdchSLQUA7cE6lBYEoA5ZqqkAoD1Y/eKCAFQ/R4kRVvcW4Jiw0R6saJ1BACqamLRh1UQA0B5Mm9CCx0EACgZu211tBADtQdupt2IPAmAFY3lG6iQARyihPVhetTzXMwSgStnIEUv9BADtwRxpdjYFAuAMbTGG6ycAaA8WUxnpvEAA0nGq7Kg6CgDag9UpJwhAdXKRK5IaC8BovWgP5sq6vUkQAHssS7FUcwFAe7CUqnnGKQSg5ASYug+kP0FdMYizMz3T77OUbVM/LucrEjtWev5Wlz5ge30CEICaV0ZL+p3Hu2I5bhmd2eEuInpZ9ZeJ9mAZOYIAlEHdok/FfMHKQvvBWAGYC/+BmP/KoktHptAedAQ20SwEoAzqFn0y89XLC+3bY28B5vpnM9NOImmca4thr2tKKXVA+uKNQbe907Uv2D9CwLgoOrNDBszyCAjBdyzNt69MimB6NvwUE7+nvCgzed6jIv/ClY+IPZlmYXAuAhCAXNiqNEntn9jf3vyrT4rDcVG9uMuTB1X4NSL6gypFnhAL2oMFJQoCUBBop26Y3xUstD+f5OO0bbxhsDG6hWV0VR1uB4jVl4Nd7bfS3SJyyq7hxiEA41AATI8Env8q6opQt5zO9sFvCyHfw8QXCaV+q8otQrQHddk0/xwCYM6wEhaY6QPLC61PVCIYBFEbAhCA2qQqOVBF6nCL6fwnFiZ+OiZLwjIKIAABKABygS4e9Yb+G564XgQF+oSrGhOAANQ4eTGh/0gK/5K982Jp/JaGFdkmAAGwTbQC9lQkdvsyumzvwsSPKxAOQqgwAQhAhZNjEppSatWTcnZJ+rck/bOQiQ/MrT8BCED9c5i4AkXi54LUjd6h1h17bxIHx3y5WF5GAhCAjMDqOlyROihJfkUw30ckHmLP/8VGov/7ZZf6RAKvc9c1sYZxQwAMAWK6WwK6Lzxx6338rUMAxj/HtV4hBMBt+iAAbvnCuiEBCIAhQM10CIBbvrBuSAACYAgQAuAWIKy7JQABcMsXJwC3fGHdkAAEwBAgTgBuAcK6WwIQALd8cQJwyxfWDQlAAAwB4gTgFiCsuyUAAXDLFycAt3xh3ZAABMAQIE4AbgHCulsCEAC3fHECcMsX1g0JQAAMAeIE4BYgrLslAAFwyxcnALd8Yd2QAATAECBOAG4BwrpbAhAAt3xxAnDLF9YNCUAADAHiBOAWYBrriqJACvktVvQdIeTDpMLd7Whi34YDdGD3DtFPYwNjQMAFAZwAXFBds6l+TeTdIVl9ce9C6wF8644z0DBsQAACYABvvalKqSeEkB8Tnn9b0BUHLJuHORCwSgACYAunUiF78mY67PeWPyr22zILOyDgkgAEwALdtW/elery5W77hxbMwQQIFEYAAmCIWjD9e+j5f76vK35jaArTQaBwAhAAE+RC3B4I771pfpbbxA3mgoArAhCAvGRHm3/e24Kn+3kBYl4VCEAAcmSBif5tWfp/muXK//xr+eRWO/ojlvQmYj5HKPUSlnQSkWzlCAFTGkNADYnoN0TyFyTETlb0H9T37rX1oBkCkL2QHo2kf17ae/6pmdWXsedfR1H0Z1LKyezuMAMEjiWglDokybuTvPCGYH5ytwkfCEAWekqF5Ivzg257p27aC7t8QqjCRSa1lUh6uvH4HASyE1BDJnnz5H5/7lefFIezzyeCAGSjdmPQa12nmzK66kvh3cOCXqkbi89BwJSAIvqxFNFleU4DEIC09BU9Llf9l+p+YXdq++C1xOprQnpTaU1jHAiYElj7fxOWFwcL7R9lsQUBSEmLmbctL7RvTho+uvIT0bex+VNCxTCrBNZEQNEbgsXJR9MahgCkICWU2idW2y9Kuvpv/iBvVCeEDxLRK1KYxBAQcEJgdDuwYb9/YdpnAhCAFGlQJHas9PytSUM7s8PR6eB9KcxhCAg4JSCIPr7Ua30wjRMIQApKivmClYX26Oq+7t+mbv/lUtFP8LQ/BUwMcU9g7R/T+FXL85OP6JxBADSEFPHelV7rBUlv/HXmBl8gFn+hg43PQaAoAkqIz6/M++/S+YMAaAgJxXcuLbbfHjds9IafbA2ewEs+ulLD50USUKQOs2yfqnthDQKgzQq/P+i1b4kbNr09fAcL/qLWDAaAQMEEmMWVywv+HUluIQC6EwDRJUu91tdj7/9nws9JydqjVsG5hzsQIFLis8GifxUEwKAYhIrOXFqc/Hmcic7M8Ack6XcMXGAqCLgi8GDQa10AATDA2x76U3uuFyuxtwAz/RWW8hQDF5gKAk4IsIqWlxcnOxAAA7yB9CeoKwaxJ4DZ/gD/0msAGFMdElDDoDfRhgAYINb9MEVndsgG5i1OVfuFEl9Wkr7Fgh6aEK3HHh/9H3mCeK0573L7hUTP6/PwdKHoXMF0kSK+VEp5opXgRHRW0j+pbPpw+Kj0+Ewrvo4zoiKxe+Uj/lnxt2+rZ5H0Envluvybxu26fnTx4yGgJoM6gK4TqC0wpkeI+MaJA607077+qbM5eq05OmH4diZ5rSQ+Qzc++XPeGvTaO+LGbJoNb5XE15j5iJkt6NZgvhX7dmZn+2ArCRHb4RlZ1eXfNG7X9aOLHwJQUwEYfSmEkGJmWbZ2ZPlmokwFu4VbnVPDv1FKdfO/56C+GvQm3hJ/CxW+mYi/kimutIMlvTnotr4WN3xqe/+rQshLkszpNlDaUOLX7/YEqYsfAlBPAXg4kuqyfd2J/zYtwDTzp+f6Z0fs3ZPnNDB6IeUk2T7ll12xup6v07bxhsMbB/vyC8z6K7DlV7eB0vBLGoMTgClBx/N1BeA6gess7wftoX9xUmfCBZLNczw95PAbHtE5me1buBJn9kl2Th66/GeP69gZrutHFz9OAHU6ATDtaof+7xW9+Y8iGolAyNH9mU8CFu7Fs280O88edBsoe1wQAFNmhc7XFYBrBT+6WEXqICk6f2Vx4meFAjjO2drtQETfy3Jkt/E0PvOaLXUfdPnPHNdxE1zXjy5+nABqcgJI841EpsWYdv709uGHWNCNacevjbO0IdP4tCk4ug2UJh48AzClVOJ8XQG4VvC1pTPtCnb5r6a7RaRDMTU3OE9GckvE9CZPRC9iKZNfBFFqwFL+jyBxn5LqtlS/b7iFW5tOjX6W7VbAzpFct/4jYmPe/kvlJ0WbsJD6SAhWV784AdTiBCDeGfT8LySFOurdhxujv5fM70xbvOuOU+KzEwe9a3TvFExtH1wlhPhMel92Hsql8lfgQ0fdBoMApMpYdQeVn2D15EbZPjWujTYi99T3EX6TiH7XBkkm+vbkfv8Pk0RgTXAm174HIdUbg7bacrr1FeXnaBzl10cyEV18OAFU/AQgFP/L0mL7HUlhbpoL/8n4yn+8g7XfPvSvTrx/nRt8iVhcrtuUT39eyJW5wJMGbgGIyj7ipC6+nAN1Cup+/XxV0Gt/Ni780e8QCCG+n3N5CdMUE4vXJn3P/NTs4D2CxKdS+y7k3rzAZw0QAAiAawFgwecvz7djN/j0XPhpZt6SehNmGMhCfGp53n9v3JTNc4PXKRbfSWvS5tP5WJ8FdhtGMZR/gcAtQNr6yzWu7ATrvo+gMzsc/Tdb7H+85Vr0M5MeDnqtl8cLAE8rDvdm8uFwgxYiMMcttuz60LHXxYdnABV/BqD7PoLpmX5f1+rTFUnc54pUf6U3EfuLxmdewxNPPj9c9x3/eJ8Oj+iF3GIcuzLdBnN9QtTlVhcfBKDqAtBrJebIdYHpCii7f4cP6Qp5yAgBOIZA9gLQaVa1Pre/AbKtb9z8u2rTubKry1bZ+TGNDycAnAASCTgpcCdXaocniwRCTvjodnWGz3XxQQAgAMULgJN7dYfPFiAA8QRwC1DuN7q45q+7guTx7+RpvcPuQpJCuuCT4QKvHaqLDycAnACKPwGMPFrcsE4ERbu1jgzQbbA8ApnSdaphuvggABCAcgSALB7ZndxSpNpfEICyFS5dmvKP0imo6/WPr3+LD+2cPFRMVzNl50cXpS4+nABqfgLQFYDrz/MKoK22nS07eTnpNlhePnnjOX6eLj4IAATAqNaMCtzKldviSSIHCd0GM+KTIx4IgAVozzZRdoJ1/i0vN7M5owK3cu9u8VlC5tXjISD+HdjxT4ONswBYeXpvsZuQY//jIaDRFSAP8YLn6Dag6/Xr/BeM4znujNdvsIGtCIghQF1+jPk4jg/PAPAMwKjEzAvc4Ahv5RbCaPk4AZgXgFkCXM8uW+F1/l2vX2ffPP8GD/GsPETUrTD5c11+zPm4jQ8nAJwAjCrMtMDztvHyzjNa7DqTIQCOH4LZTlhWe2UnuBH+c13JDU4OWYsgYXzZ+dEtRRcfTgA1PwGYXoFNC8iK/1z38gbPDnSLzvC5boNZ4ZMhnuOH6uKDAEAAEgnoCshGged6mm/QPTDYT8+ZWgQfk3h18UEAIAClC8BaABk2dC7BMNlFuAWIJ2DjCuAoN1bM6hTU9fqb4z/DkT7XLYOVcsAJ4HgCrjeAm7Slt9qcDbg+k+LWn+GhXq6HhulznmVkcXyyRPXMWF18uAXALUAlbgHStvVIEp8k26fE/Vbiadt4w+GNg31SytivM8+3lcoWyHxRQwDycXt6lg6g6xNQo/ynurITB72Jt8SltTMbvpmIv2KY9tTTy86PLlBdfDgB4ARQiRPAkQeBdGsw33pf7ObePthKYiQA7R1xYzbNhrdK4mt0G8PW57oN5voCoVuHLj4IAASgMgKQ6um+RxzMT+6OFYAPh49Kj8/UbQxbn+s2GATAFumS7JSd4Mb517QDk8qgM7N6Fklv9FuJhf2VnR/dQnXx4QSAE0BlTgBHAkluByYKwNotgrhFtylsfq7bYDgB2KRdgq2yE9w8/8ntwKQSmNre/6oQ8pIiy6Ts/OjWqosPJwCcACp1AtC1A+OCLbr9dzQO3QbDCUAnURX/vOwEN9K/ph24XskU3f6DADxFoGyFc60fjdyAz4Jayvo17cD1cl50+w8CAAFYI+BaAEvZgCULgK4duK4AOGr/6fibXoDKrh88A8AzgEo9A3g6mAztQJftPwiAZoO4VjBThTWdrysA1+tvrv/07cCOw/afjr9pfZVdPzgB4ARQzRMApW8Humz/QQBwAkgUybIVfFz9p20Hum7/QQAgABCAhBpwKkAp2oGu238QAAgABKAsAUjRDnTd/oMAQAAgACUJQJp24CZH7b+0fX48BBz33wWQ/gR1xSAu0Z3Z/oBItkwLAfNBwDYBodRgaXFiIskuugAa6u2hP7XnerESN2x6pr/CUp5iO3mwBwKmBBRFwUpvchoCYEJS80JKZ3b4fSJ6rYkLzAUBFwQE0QNLvdaFEAADuoLokqVe6+uxtwAz4e0k+d0GLjAVBJwQEEp8ZmnR3wIBMMLL7w967dgvmZiaDa8UxP9s5AKTQcABAWZxxfKCfxcEwASu4LuC+fYVcSZO6fLzhBo8IUluMHGDuSBgk4BS6pD025uDrjgAATAgq4j3rvRaLyASHGdm00z4OSn5XQZuMBUE7BIQ4vZg3r9aZxRdAB0hIlLMF6wstB+MGzo1t/pSEYmfkpR+CnMYAgKOCaihUPyKpcXJn+scQQB0hEafp3gjbXp2eBMTfSCNOYwBAccEbgx6revS+IAApKFEaqUl2y96vCsOxQ0f/VNKf2P4AEl6dSqTGAQCDghERA89T/qvi/vptONdQgDSJoH5fcFC+9ak4Z251TMV0/2SvE5asxgHArYIjJ5X+VK9fm938rG0NiEAaUkR7ZGH/JftvUkcTBSB7uBcFdLXpRSJb2Cld4uRIKAnMNr8HtPFSwvth/SjnxkBAchCi+j6oNf6O92U6ZnVMxR59whJZ+vG4nMQMCUQKdrZ8qM/yXLlP+oTApCJvhoKQectzU/8l27ai7s8eYDDecFqG/5ZSEcLn+cjoIYk5MdP3ud3d+8Q/Tw2IADZqT1M0j9P94LFUbOj0wB7/nUqit4upTwhuzvMAIFjCShSBz3l3SH88IY8V/1nW4MA5KkupnuCXf7ldLeI0k7vdPlEUtGlQtBFxHxuRNFLPCVOZinbaW1gXPMIjP6lN5L8pCDvMSnEQ8x0H0nv3rQXIB0xCICOUMznTOIfl3veXye9IZjTNKaBQGEEIAAGqNdE4GfeNVlOAgbuMBUErBOAAJgj/Vde9f9y+aNiv7kpWACBYglAAGzwZnqEPH5b0G3vtGEONkCgKAIQAFuklQrZk58Qwl+w9YDGVmiwAwJxBCAA1mtD/S+T+Jh3qHWb7q1B665hEAQyEoAAZASWdrhQah973l2S1Bf3itYD1BUq7VyMA4GiCEAACiA9+nZWSd5/MvF3hJAPM4W7/cHErycnaP8vu9RHK7GAJMDFugQgACiMShNw/cs8lV58AcFBAAqADBf5CUAA8rNLMxMCkIYSxpRGAALgFj0EwC1fWDckAAEwBKiZDgFwyxfWDQlAAAwBQgDcAoR1twQgAG754gTgli+sGxKAABgCxAnALUBYd0sAAuCWL04AbvnCuiEBCIAhQJwA3AKEdbcEIABu+eIE4JYvrBsSgAAYAsQJwC1AWHdLAALgli9OAG75wrohAQiAIUCcANwChHW3BCAAbvniBOCWL6wbEoAAGALECcAtQFh3SwAC4JavhRNAf4CfvnKbpOZaV8OgN4EfTnFYAMYCMDWzGgjpTTmMEaYbSoBVtLy8OImfWneYf2MBmJ4dfo+JLnAYI0w3lIAgemCp17qwocsvZNnmAjAXfpqZtxQSLZw0ioBQ4jNLiz5qy2HWjQWgsz28nAR/yWGMMN1QAsziiuUF/66GLr+QZZsLQJdPVOFgL376upB8NcaJInVYrLY34yfX3KbcWABG4U3PhLex5KvdhgrrjSIgxOeCef/djVpzCYu1JACrZzCJXSSlX8Ia4HLcCCgVEvErg8XJR8dtaVVbjxUBGC1qam54o2D6UNUWiHjqR4AF3bQ83/rb+kVev4itCcBp23jD6sbwu0LS2fXDgIirQkAR/XjDfv/CX31SHK5KTOMchzUBOPIsYPWMiOT9UorpcYaGtbkhMHrxRxC9Hkd/N3zXs2pVAEYOOt3BuUqpb0jy8AZXcXmsvafR5idfXrzcbf+w9oup0QKsC8CaCMytnqnYu0cSvaZGLBBqWQQU/YQougxX/uIT4EQARstYeyZwUjgvlNqG7kDxia2FR6VC9uTNbeHPPd4Vh2oR85gF6UwAjnLqzKyeRZ5/neLoCklyw5jxw3JyEBi95COFdydF4Q246ucAaHGKcwE4GuvUh/gkmogulUwXkeRzlIpOF1KcjH8ltpjNSppSQ1b8pJTeY6TEQ0rQfdT37sUbftVIVmECUI3lIgoQAIFnE4AAoB5AoMEEIAANTj6WDgIQANQACDSYAASgwcnH0kEAAoAaAIEGE4AANDj5WDoIQABQAyDQYAIQgAYnH0sHAQgAagAEGkwAAtDg5GPpIAABQA2AQIMJQAAanHwsHQQgAKgBEGgwAQhAg5OPpYMABAA1AAINJvD/H/4504KFzgAAAAAASUVORK5CYII='),
(51, 'Audiograph', 'application/vnd.audi', '.aep', ''),
(52, 'AutoCAD DXF', 'image/vnd.dxf', '.dxf', ''),
(53, 'Autodesk Design Web Format (DWF)', 'model/vnd.dwf', '.dwf', ''),
(54, 'BAS Partitur Format', 'text/plain-bas', '.par', ''),
(55, 'Binary CPIO Archive', 'application/x-bcpio', '.bcpi', ''),
(56, 'Binary Data', 'application/octet-st', '.bin', ''),
(57, 'Bitmap Image File', 'image/bmp', '.bmp', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAbxUlEQVR4Xu2da5AcV3XHz73dM7uSbCvYOyvhooiRzRtsEYxFoAhEH2IKVyoUSQy28ygCFiGUDTZEdqh99O6Ksg0GbFwJIAxFipRtQqFUUjhloGJIipKxnYAcXjIW4FQhR7uzawJ67cx035Pq1QNJ3pnT3bfvTPf0f77Oveee8zun/32mb0+3InxAAAQqS0BVNnIEDgIgQBAAFAEIVJgABKDCyUfoIAABQA2AQIUJQAAqnHyEDgIQANQACFSYAASgwslH6CAAAUANgECFCUAAKpx8hA4CEADUAAhUmAAEoMLJR+ggAAFADYBAhQlAACqcfIQOAhAA1AAIVJgABKDCyUfoIAABQA2AQIUJQAAqnHyEDgIQANQACFSYAASgwslH6CAAAUANgECFCUAAKpx8hA4CEADUAAhUmAAEoMLJR+gg0DcBaAR8FpnoCqVoKzFvNibapLRaT6RrSEN2As3ZWs8cNqba1zVn63dlXwEzh5mAcwEYn1i+kMi/KaLoGq312mGGOYjYZAFohUzeHyzO+vcPwj+sWWwCzgTggoBHD3M4TZH5AGntFxtDeb2TBaDDxphD2levawb1PeWNFJ67IOBEADYEy5s6xtvlEV3iwmnY/DWBJAJwfPR+E/lblj6k9oMfCJwgkLsAbJhsvTxU6uua1AZgdk8ghQDEznyXtP87zUAdcu8ZVigDgVwFID7zh0bvxsHfv9SnFAAiNl9p7q2/mb6kov55iZWKSiA3AYh/8//KhN9G29/fVKcWACIypO5amvWv76+nWK2IBHITgMZU51YiuqmIQQ6zT1kE4BgPvh7bg8NcGcliy0UA4q0+JrUXV/uTQc9zVHYBMBG2B/PMRDlt5SQA4U7WfG05EZTb6+wCQITtwXLnPg/vrQUgvsPPhO153OSTRzrS27ARgOOrYXswPfahmWEvAFPhW4n4vqEhUrJAchCAOGJsD5Ys73m5ay0A49Php5l5W14OwU46AjkJALYH02EfmtH2AjDVeZiJLhsaIiULJDcBwPZgyTKfj7vWAtCYai0S6fPycQdW0hLIUwCOrY3twbQ5KPP4PASgjb/0Dq4E8hcAbA8OLpv9XzkHAehw/93GiicI5C8A2B6sUnVBAEqebRcCcBwJtgdLXhtJ3IcAJKFU4DEOBSCOGtuDBc59Hq5BAPKgOEAbjgUA24MDzG0/loYA9IOywzWcCwC2Bx1mb/CmIQCDz4GVB/0QAGwPWqWo0JMhAIVOj+xc/wQA24NyNso3AgJQvpyd5nH/BADbgyUvlVXdhwCUPKv9FABsD5a8WFZxHwJQ8pwOQACwPVjymjnVfQhAyZM5IAHA9mDJ6+aE+xCAkidyYAKA7cGSV84x9yEAJU/jIAUA24MlLx4IQPkTOHgBwPZgmasIHUCZs0dEgxcAbA+WuYQgAGXOXkEEANuD5S0iCEB5c7fieRE6gFMQ4t+DJasnCEDJEnamuwUTAGwPlqyeIAAlS1jhBQDbg6WqKAhAqdL1TGcL1wGcdBEPFy1DaUEAypClHj4WVwCwPViG0oIAlCFLpRQAbA+WobQgAGXIUkkFANuDxS8uCEDxc9TTw+L+BDjNbWwPFrTOIAAFTUxSt0oiANgeTJrQPo+DAPQZeN7LlUYAsD2Yd+pzsQcByAXj4IyUSQCOUcL24OCq5ZkrQwCKlI0MvpRPALA9mCHNzqZAAJyh7Y/h8gkAtgf7UxnJVoEAJONU2FFlFABsDxannCAAxclFJk9KLABxvNgezJT1/CZBAPJjORBLJRcAbA8OpGp+vSgEYMAJsF2+qf0RClS7m53xiVaLta7bruNyviF119Ksf73LNWB7dQIQgJJXRk37jacCtdgtjMZUZy8RvbD4YWJ7cBA5ggAMgnqOaxrmy5bm6o92FYDp8G+J+a9yXNKRKWwPOgLb0ywEYBDUc1yTma9dnKvf3fUnwHTrYmbaQ6Stc52j26uaMsYc0r56XTOo73G9FuwfI2BdFI2pDgPm4AgoxfcszNSv6eXB+FT4KSZ+1+C8TLXyfhP5W5Y+pPanmoXBmQhAADJhK9Ikc3DkYH3Dzz+ujnbz6oKARw+b8AEien2RPO/hC7YH+5QoCECfQDtdhvntzbn653ut8ZwbeE17XXQn6+idZfg5QGy+0txbfzN9SUVO2VXcOARgGAqA6cdNz38pBSqUwmlMtl+hlH4XE29VxvxmkbcIsT0oZdP+ewiAPcNCWGCm9y/O1T5WCGfgRGkIQABKk6rejhoyR2tMrzowN/KDIQkJYfSBAASgD5D7uMQTXsd/7YFbVLOPa2KpEhOAAJQ4eV1c/65W/hvnZ9TC8IWGiPImAAHIm2gB7JlI7fN19Jb5uZHvFcAduFBgAhCAAifHxjVjzLKn9dSC9u/s9WchmzUwt/wEIADlz2HPCAypnygyt3lHavfM364OD3m4CC8lAQhASmBlHW7IHNak/1UxP0ikHmPP/9k6ov97MqAWkcLt3GVNrKXfEABLgJjuloD0wBO3qw+/dQjA8Oe41BFCANymDwLgli+sWxKAAFgCFKZDANzyhXVLAhAAS4AQALcAYd0tAQiAW77oANzyhXVLAhAAS4DoANwChHW3BCAAbvmiA3DLF9YtCUAALAGiA3ALENbdEoAAuOWLDsAtX1i3JAABsASIDsAtQFh3SwAC4JYvOgC3fGHdkgAEwBIgOgC3AGHdLQEIgFu+6ADc8oV1SwIQAEuA6ADcAoR1twQgAG75ogNwyxfWLQlAACwBogNwCzCJdUNRUyv9DTb0kFL6cTLhvno08vSaQ3Ro312qlcQGxoCACwLoAFxQXbFpfkHk3aPZfGF+rvYInrrjDDQMWxCAAFjAW22qMeaAUvojyvN3NgN1KGfzMAcCuRKAAOSF05iQPX0HHfVnFz+sDuZlFnZAwCUBCEAOdFeevKvNlYtB/Ts5mIMJEOgbAQiAJWrF9C+h5//p04H6laUpTAeBvhOAANggV+rupvLeneS13DbLYC4IuCIAAchKNj74Z7xtuLqfFSDmFYEABCBDFpjonxe1/0dpzvzPuonX1+rR77Om3yXmS5Qxz2NNZxPpWgYXMKUyBEyHiH5FpH9GSu1hQ/9GLe/+vC40QwDSF9ITkfYvTfqbf2xi+YXs+TdTFL1Naz2afjnMAIHTCRhjjmjy7iUvvLU5M7rPhg8EIA09Y0Ly1auaQX2PNO38gNeGJtzBZK4n0p40Ht+DQHoCpsOk7xg96E///OPqaPr5RBCAdNRua87WbpamxGd9rbxdrOgl0lh8DwK2BAzR97SK3pKlG4AAJKVv6Cm97L9AesPu2GT7lcTmAaW9saSmMQ4EbAms/N+E9eXNufp309iCACSkxcw3LM7V7+g1PD7zE9G3cPAnhIphuRJYEQFDr23uGH0iqWEIQAJSypin1XL9ub3O/hs+wOvM2vBRInpxApMYAgJOCMQ/B9Yc9LckvSYAAUiQBkPqrqVZ//peQxtTnbg7eG8CcxgCAk4JKKKPLszWPpBkEQhAAkqG+bKluXp8dl/1c17QepE29H1c7U8AE0PcE1j5Yxq/dHFm9MfSYhAAgZAhnl+arT271x1/jen23xOrP5Ng43sQ6BcBo9Tnl2b8t0vrQQAEQsrwvQs76ld3Gxbf4adr7QO4yUcqNXzfTwKGzFHW9Y3SDWsQADEr/L7mbP3ObsPGJ8M/YcVfEM1gAAj0mQCzumZxzr+n17IQAKkDIHrjwmztq11//0+En9OaxVarz7nHciBAZNRnmzv8d0IALIpBmeiihR2jP+lmojHR+S/S9FsWS2AqCLgi8GhztnYZBMACb73jj+2/RS11/Qkw0Vpirc+1WAJTQcAJATbR4uKO0QYEwAJvU/sjFKh21w5gqtXGX3otAGOqQwKm05wdqUMALBBLL6ZoTHXYwjxJ9m1sr8wNuH4+0Tkt7mxShjYrpq2G+Aqt9VnWtolE/xtTnW8S0evzWGsVG//enK29oZdtKT/94L+xRevZ72yKFG1WirYa5is06XWOmJxmVooPFwGFLEgApQKTkizZl+Zn+T6+bTla27maSd+kiS/MYuPEHMn/saDzBmXoGzZrdJsbP1xlMajFAtP1I+VH8t+F342Az+Koc7Ux+ibP400u1kiaHwhABQXgZMjbuNbYGN5ojAmy3seQ5ABy1AWIZ/84ziIKwEn+AdcbUXijUSbQpEdcCIGUHwhAlQXgeOzj062LI/Z2ZekGpAKLl3DRBSQ5+xdeAE7wn2xfEhq9y0U3IOUHAgABWCGwYZrHOxx+zSO6JM2ZSCqwE7Zy7gISnf3LIgCxn+Mf5A1Gh19Tmi5Ow18aK+UHAgABOEkgFoGQo91pOgGpwE4Yz7MLSHr2L5MAnBCBUEW78+wEpPxAACAApxFY+TkQ0cNJrwlIBXaq8Zy6gMRn/7IJwIoITLYviRQ/nNc1ASk/EAAIwDMIjE92trOi26T2Mv5eKrBTbeTRBaQ5+5dRAI77fBMR3ZqEvzRGyg8EoOACIF3FlgpAGdNmrf9HkXrQaLMz0fsLt3HtvI3Rj5L8FJAK7Ez/LLuAVGf/PARgIPwDrp/biX6Ux08BKT8QgCEXgGeEZ9RnRw5710mPjBqbbL9TKfUZSWCkAjtzvk0XkPbsXwQByMx/qn2tIrVT4i99L+UHAlA1ASAiJvrW6EH/93qJQHyzUDi68pyDnncMSgW2Gt6MXUDqs38hBSAh//hmIWPaB2zvGJTyAwGooACshLzybkP/2l7hN6bbXyRWV/YaIxXYanOzdAFZzv5FFYDE/Kc6/0hEfyyd5W3yAwGoqgCQYWL1yl7PkR+bar9LkfqUTYF1m5uyC8h09i+0ACTgPz7Z/ktW6pMQABsClnOlM5ztRSLX9nuFz0p9anHGf3e3MRum2682rB5yIQBpuoCsZ/9iCwCRxH98svPbrGi3TQlL9YUOoLIdwErgjzdnay/qLgA8bjicdyEAxw/OJP8UzHz2L7oASPzjuwPZDw9AAGwIWM6VFLTMHYAh01qaHen6xuKLruORXz4rXHYlAEm6AJuzf9EFIA/+UnlL9YsOoMIdgFSAFLDfMGH8fvquH6nA4oO81192hWsBPc/+km0IgHyjFgSgwgJATHubc7WurzI7P+CxjgmbNgIQH+C9HtrRqwuQzv6S7aILgMQ//m+G9BMMHYBEwPJ76QxX5p8ApNTfNWf893RDNDbdvlSx6vpGpHheEj5JDuRVnhoknv3jB40kWd9SwKye+NSz/AT+SS7CSuUt8UEHUNkOwLBi9YqFufpjXQUgwd1oUoEdF8hEB/OpfiQVjYTrZ/4JYyvw3csrEX9xGxYCIBGw/N62wKTlXdvvtr60BbXSPk+37yVWb8vjDJr0gD6+VmLBsOVnO1/KryV/8UYsaX0pPnQA1ewA/mOd9i9/MlBdr/A/5wZe01rXPkBan5OHABBR4oM6jVhIBS6dwW3nSwdgl+9F/iu3Yq9tz+NW4IyE85rmukBc2z+dg2FF3s612ntfr4N/5ew/2f4LUuqzEsc0/ic8sOPrCl2f9HvmRcM0668Wi+18iU9W/kn/jCWtL8WHDmDIO4B4q0+z/hlp9aAyZmev3/wnURx7WOgPiOj5tgV2xhlY7ALi9dJsG0oFPugOIBP/gOvnmeiHSf6ObZsfCEDBBUBKsIvvx6Y671dEtyexnfYAlLqAXmuutmWYdv0z7UvzkzDIe0xjqoMHguQNNas9qUCkM4y0rmRfmp/39xuC1stCQ49o0muS2Jb8X4VP5lt7V7tpKMP6p4UlzU/CIM8xeCRYnjRzsCUVyDAJwMa/4UanFj2UpvXMwidLF9DthqEs659aFtL8HEoosYn43n88FDQxrv4MlApkWAQgPvjbtfDreT8WvAuf1F1At1uGbfMjze9PlRE1At4YmfCBtPwl/6T4cA0A1wBWCMS3/S6H4dc9TZulokpzBu0mkGm6gF63C0sFLgm0ND8Ni6xjG5PtV7DSX1bEz8tqo9s8KT4IAATgJIFYBFqd6CHt8UVJC1EqsB4HYOIuoNcfhizWXwlRmp+UQ5Zxx/9teaMyJmCte77FN4v9JPFBACAApxHYONl6aUfRow4vAp5cL0kXIP1lWDqAi9gBjG3ns/VI5xqj9HYXZ/00HRoEAALwDAJjk50blaKPJjnrWB6AYhcgPTrMcn3HHQCrCwIaOUz0G4rCTRTx5kiprUTmTUkFNkkOeo2R+EAACi4A0hlMKpBM7wU49hyAHzq4EUhyN/X3UoFL/Gznp3a4zxOk+CAAQy4Azwgv4XsBXNwK7KL2pQKHANR6HuMQgKoJQMLn0jv4M5CL419s4SEAEACrwrM9w0iLu7bfdf1k7wXI7e/AEoes39vys52f1e9+zZPiQwdQwQ7gWMgJ3guQ4PVgUoFJZ2DbA8F2fdv5tv67ni/FBwGorADIz6XP65FgLotcKnBJgGznu4wtD9tSfBCACguA9Fz6nB4K6u6Zeglu5IEA4BqAlZBKCioVmLS4a/u91u/HY8Ft+bjmN0j+Umx5fC/Fhw6gwh2AJAB5vBgEApDHYZzdBgQgO7uVmRJA2wJ3bb9n+MJ7AeJ/CEa1cKGXjYH6n0N+Bu2/ZXmK06X40AFUuAOQ3guwYbK9xSj1bQiAeJwVdgAEwDI1EsDydgAJnks/2d6mlPo0BMCyiAY4XapfdAAV7QASvRdgon0fafVWCMAAj2DLpSEAjgGWtAMQn0t/fsBrW2F7Xmt9FgTAsogGOB0CYAlfAlguAUjxXoCp9juI1N0SPtd8XK8/aP+l+Gy/l+LDT4Ah/wmQ6bn027h2XiP6YZInA0kFZiuQ0gFgu77tfMm/QX8vxQcBKLgADKKAxic721nRbUnWlgoMApCEorsxUn4gABCA0wiMT7cujiJ6WGs9mqQspQKDACSh6G6MlB8IAATgJIEN0zwecrTb9XsB8ix3qcAlAbKdn2csLmxJ8UEAIAArBFZeSqHDB/r1WPC8il0qcAgA/gxkVWu2BSYtLtmX5ufxfSNob446+suex5vS2pP8lw7AtOudOd52fdv5tv67ni/Fhw6gyh1AwPVGFN5olAk06ZEsxSgVGAQgC9X85kj5gQBUUAAaAZ/FpnONifT2LGf9U5FJBQYByO9gzmJJyg8EYMgF4IKAR4+0aT154SZSvDlitZXYvElrvTZLQeXdgtv6IBW4JEC28239dz1fig8CIAmA9kcoUO1uwxpTrTaRrrlOJOyDQFoC8TshFnaM9PxpBwEQqNY7/tj+W9RSt2HjE60l1vrctMnBeBBwTcBQ1FyaHR3vtQ4EQMqCip7fnBnd170D6PwnEb1SMoPvQaDfBBTRIwuztS0QAAvyiuiNC7O1r3YVgInwbtL8DoslMBUEnBBQRn1mYYe/DQJghZff15yt39nNxNhUeI0i/gerJTAZBBwQYFZXLc7590EAbOAqvq85U7+qm4lzAz5HmfaBfr3t1SYUzK0OAWPMEe3XNzQDdQgCYJF3Qzy/NFt7NpHq+nz78ybCz2nNb7dYBlNBIF8CCV79Fi+Ii4AJsBvmy5bm6o92/RkwvfwCFakfkNZ+AnMYAgKOCZiOMvzihR2jP5EWggBIhI7J5CeaM7X39ho6PtW5nYnen8QcxoCAYwK3NWdrNydZAwKQhBKZpZquP/epQB3pNvzY67TDR0jTyxKZxCAQcEAgInrsHO2/+slALScxDwFIQikew/ze5lz9E72GN6aXLzJMuzV5jaRmMQ4E8iIQX6/ytXnNfDD606Q2IQBJSRHt10f8F87frg73FIGgvdmE9FWtVc87sJIvi5EgIBOID36P6fKFufpj8uhfj4AApKFFdEtztvZBacr4xPKFhrxdStPF0lh8DwK2BCJDe2p+9Idpzvwn1oQApKJvOkrRpQszI/8tTYv/hXeIwxnF5gb8WUiihe+zETAdUvqj65/2g313qVYWGxCA9NQeJ+1fKt1gccJs3A2w599soujqvP6Cm95lzBgmAobMYc949yg/vDXLWf9UFhCALJXBtKu517+SvqSipNPjh3CQia5QirYS8+aIoud5Rq1nretJbWBc9QjEf+mNNP9SkfdTrdRjzPQgae/+pCcgiRgEQCLU5Xsm9cnFWe89ve4QzGga00CgbwQgABaoV0TgR951aToBi+UwFQRyJwABsEf6T7zs//nih9VBe1OwAAL9JQAByIM304/J47c2g/qePMzBBgj0iwAEIC/SxoTs6Y8p5c/ldYEmL9dgBwS6EYAA5F4b5n+Z1Ee8I7Wd0l2DuS8NgyCQkgAEICWwpMOVMU+z592nyXxhXtUeoUCZpHMxDgT6RQAC0AfS8dNZNXnfZOKHlNKPM4X7/PbIL0ZH6OCTAbWwldiHJGCJVQlAAFAYhSYgvdii0M6XwDkIQAmSVGUXIQBusw8BcMsX1i0JQAAsAQrTIQBu+cK6JQEIgCVACIBbgLDulgAEwC1fdABu+cK6JQEIgCVAdABuAcK6WwIQALd80QG45QvrlgQgAJYA0QG4BQjrbglAANzyRQfgli+sWxKAAFgCRAfgFiCsuyUAAXDLFx2AW76wbkkAAmAJEB2AW4Cw7pYABMAtX3QAbvnCuiUBCIAlQHQAbgHCulsCEAC3fNEBuOUL65YEIACWAN13AK02Xn3lNknVtW46zdkRvDjFYQFYdwBjE8tNpb0xhz7CdEUJsIkWF3eM4lXrDvNvLQDjU52Hmegyhz7CdEUJKKJHFmZrWyoafl/CtheA6fDTzLytL95ikUoRUEZ9ZmGHj9pymHVrAWhMhleS4i869BGmK0qAWV21OOffV9Hw+xK2vQAEfJYJ2/N49XVf8lWZRQyZo2q5vgGvXHObcmsBiN0bnwh3suZr3boK65UioNTnmjP+OyoV8wCCzUkAli9kUntJa38AMWDJYSNgTEjEL2nuGH1i2EIrWjy5CEAc1Nh05zbFtL1oAcKf8hFgRbcvztT+unyel8/j3ATgOTfwmuV14beVpovLhwEeF4WAIfremoP+lp9/XB0tik/D7EduAnDsWsDyhRHp3Vqr8WGGhtjcEIhv/FFEr0Hr74bvalZzFYB4gUbQ3myM+ZomD3dw9S+PpV8pPvjJ15cvBvXvlD6YEgWQuwCsiMD08kWGvV2a6OUlYgFXB0XA0PeJorfgzN//BDgRgDiMlWsCZ4czypgbsDvQ/8SWYkVjQvb0HXXlTz8VqCOl8HnInHQmACc4NSaWn0+ef7Ph6CpNes2Q8UM4GQjEN/lo5d1LUXgrzvoZAOY4xbkAnPB1bDufTSPRFZppK2m+xJhok9JqPf5KnGM2C2nKdNjwL7X2fkpGPWYUPUgt737c4VeMZPVNAIoRLrwAARA4lQAEAPUAAhUmAAGocPIROghAAFADIFBhAhCACicfoYMABAA1AAIVJgABqHDyEToIQABQAyBQYQIQgAonH6GDAAQANQACFSYAAahw8hE6CEAAUAMgUGECEIAKJx+hgwAEADUAAhUmAAGocPIROghAAFADIFBhAv8Ph6T504zv6vYAAAAASUVORK5CYII='),
(58, 'BitTorrent', 'application/x-bittor', '.torr', ''),
(59, 'Blackberry COD File', 'application/vnd.rim.', '.cod', ''),
(60, 'Blueice Research Multipass', 'application/vnd.blue', '.mpm', ''),
(61, 'BMI Drawing Data Interchange', 'application/vnd.bmi', '.bmi', ''),
(62, 'Bourne Shell Script', 'application/x-sh', '.sh', ''),
(63, 'BTIF', 'image/prs.btif', '.btif', ''),
(64, 'BusinessObjects', 'application/vnd.busi', '.rep', ''),
(65, 'Bzip Archive', 'application/x-bzip', '.bz', ''),
(66, 'Bzip2 Archive', 'application/x-bzip2', '.bz2', ''),
(67, 'C Shell Script', 'application/x-csh', '.csh', ''),
(68, 'C Source File', 'text/x-c', '.c', ''),
(69, 'CambridgeSoft Chem Draw', 'application/vnd.chem', '.cdxm', '');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(70, 'Cascading Style Sheets (CSS)', 'text/css', '.css', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAeiUlEQVR4Xu2df7AkVXXHz7nd0/OW5Ud037xdjaWw4I8ouosKqGiiWCWWJhVDEg2SxFi6ixpBQQJE349589YACqJuVFjRMmUKMSSbSkosxYiWJmvAiKw/4i6LYKqE7HszDwP7g/dmuu9J9YOHC7zu2zO3b8/0m+/8O/eee87nnP727b7dt5nwAwEQGFoCPLSRI3AQAAGCAKAIQGCICUAAhjj5CB0EIACoARAYYgIQgCFOPkIHAQgAagAEhpgABGCIk4/QQQACgBoAgSEmAAEY4uQjdBCAAKAGQGCICUAAhjj5CB0EIACoARAYYgIQgCFOPkIHAQgAagAEhpgABGCIk4/QQQACgBoAgSEmAAEY4uQjdBCAAKAGQGCICUAAhjj5CB0EIACoARAYYgIQgCFOPkIHAQgAagAEhpgABGCIk4/QQaAwAajV5WjS0RuZ6UwS2ax1tJEVH0ekKkhD7wSajUpqDmuT7fObjWB77yOg52om4FwAxsYXTiTyL40oOlcpddRqhtmP2MwCsBgKeb/favg398M/jDnYBJwJwPF1GTkk4RRF+mJSyh9sDOX1ziwAHdFaH1Q+v6pZD+4sb6Tw3AUBJwKwvr6wsaO9nR7RJhdOw+avCWQRgEdb36cj//T5D/N94AcCywRyF4D1E4svDJm/oYjXA7N7Al0IQOzMD0n5v92s80H3nmGEMhDIVQDiM3+o1S4c/MWlvksBIBL9leae4E10E0fFeYmRBpVAbgIQX/M/pMP/xLS/2FR3LQBEpIm3zzf8C4r1FKMNIoHcBKA22bmCiC4dxCBXs0+9CMAjPOQCLA+u5srIFlsuAhAv9QnxHtztzwY9z1a9C4COsDyYZybKaSsnAQh3iJIt5URQbq97FwAiLA+WO/d5eG8tAPETfjpsz+IhnzzS0b0NGwF4dDQsD3aPfdX0sBeAyfAtRHLjqiFSskByEIA4YiwPlizveblrLQBjU+F1IrI1L4dgpzsCOQkAlge7w75qWtsLwGTnNiE6bdUQKVkguQkAlgdLlvl83LUWgNrkYotIrcvHHVjplkCeAvDI2Fge7DYHZW6fhwC08Upv/0ogfwHA8mD/sln8yDkIQEeKdxsjLhPIXwCwPDhM1QUBKHm2XQjAo0iwPFjy2sjiPgQgC6UBbuNQAOKosTw4wLnPwzUIQB4U+2jDsQBgebCPuS1iaAhAEZQdjuFcALA86DB7/TcNAeh/Dqw8KEIAsDxolaKB7gwBGOj0mJ0rTgCwPGjORvlaQADKl7PHeVycAGB5sOSlsqL7EICSZ7VIAcDyYMmLZQX3IQAlz2kfBADLgyWvmSPdhwCUPJl9EgAsD5a8bpbdhwCUPJF9EwAsD5a8ch5xHwJQ8jT2UwCwPFjy4oEAlD+B/RcALA+WuYowAyhz9oio/wKA5cEylxAEoMzZGxABwPJgeYsIAlDe3C15PggzgCMQ4u3BktUTBKBkCXuiuwMmAFgeLFk9QQBKlrCBFwAsD5aqoiAApUrXk50duBnAYy5ic9EylBYEoAxZSvFxcAUAy4NlKC0IQBmyVEoBwPJgGUoLAlCGLJVUALA8OPjFBQEY/Byleji4lwCPcxvLgwNaZxCAAU1MVrdKIgBYHsya0ILbQQAKBp73cKURACwP5p36XOxBAHLB2D8jZRKARyhhebB/1fLkkSEAg5SNHnwpnwBgebCHNDvrAgFwhrYYw+UTACwPFlMZ2UaBAGTjNLCtyigAWB4cnHKCAAxOLnrypMQCEMeL5cGesp5fJwhAfiz7YqnkAoDlwb5Uza8HhQD0OQG2wzeVX6U6t5PsjI0vLopSge04Lvtr4u3zDf8Cl2PA9soEIAAlr4yK8mv317mVFEZtsrOHiJ47+GFiebAfOYIA9IN6jmNqkdPmZ4LvJwrAVPgpEnlPjkM6MoXlQUdgU81CAPpBPccxRWRLaya4PvESYGrxRSJ0J5GyznWObq9oSmt9UPn8qmY9uNP1WLD/CAHroqhNdgQw+0eAWW6Ymw7OTfNgbDK8VkjO65+XXY18n4780+c/zPd11QuNeyIAAegJ2yB10geqB4L1v7yGH07y6vi6jBzS4deI6HcGyfMUX7A8WFCiIAAFgXY6jMjbmzPBF9LGeMaFsqa9NvqEqOidZbgcINFfae4J3kQ3ceSU3ZAbhwCshgIQuqvp+S+gOoemcGoT7VOY1XlCciZr/axBXiLE8qApm/b/QwDsGQ6EBRH6QGum8rGBcAZOlIYABKA0qUp3VJN+uCJ06v6Z6k9XSUgIowACEIACIBc4xD6v45+x/3JuFjgmhioxAQhAiZOX4PoPFfuvn53mudUXGiLKmwAEIG+iA2BPR3y3r6KzZ2eqPx4Ad+DCABOAAAxwcmxc01oveEpNzin/E2kvC9mMgb7lJwABKH8OUyPQxD9n0ld6hys3zF7Fh1Z5uAivSwIQgC6BlbW5Jn1Ikfoqi9xKxLvF8+9dS/R/v6jTIhHjce6yJtbSbwiAJUB0d0vAtOGJ29FXv3UIwOrPcakjhAC4TR8EwC1fWLckAAGwBGjoDgFwyxfWLQlAACwBQgDcAoR1twQgAG75Ygbgli+sWxKAAFgCxAzALUBYd0sAAuCWL2YAbvnCuiUBCIAlQMwA3AKEdbcEIABu+WIG4JYvrFsSgABYAsQMwC1AWHdLAALgli9mAG75wrolAQiAJUDMANwChHW3BCAAbvliBuCWL6xbEoAAWALEDMAtQFh3SwAC4JYvZgBu+cK6JQEIgCVAzADcAsxiXVPUVKy+JZq+x6z2kg7vDqLqA2sO0sG7t/NiFhtoAwIuCGAG4ILqkk39KyLvBiX6i7Mzldux644z0DBsQQACYAFvpa5a6/3M6qPs+TuadT6Ys3mYA4FcCUAA8sKpdSie+jg97DdaH+EDeZmFHRBwSQACkAPdpZ13lX5zqx7ckYM5mACBwghAACxRs9C/hp7/Zw/U+SFLU+gOAoUTgADYIGe+vsneu7N8lttmGPQFAVcEIAC9ko0P/mlvK+7u9woQ/QaBAASghywI0b+0lP9H3Zz5n3KpHFcJot8TRa8hkU2s9Qmi6BgiVenBBXQZGgK6Q0QPEal7iflO0fRNWvRuzutGMwSg+0LaFyn/pVmv+UfHF54rnn8ZRdGfKKVGuh8OPUDg8QS01ocVeV8iL7yiOT1ytw0fCEA39LQOyedTm/XgTlO3p9flqFCH24T0BUTKM7XH/yDQPQHdEVIfHzngT/3yGn64+/5EEIDuqF3ZbFQuM3WJz/qKvZ3C9HxTW/wPArYENNGPFUdn9zIbgABkpa/pfrXgP8f0hd3RifZLSPTXWHmjWU2jHQjYElh630TUWc2Z4Ifd2IIAZKQlIhe2ZoKPpzWPz/xE9O84+DNCRbNcCSyJgKYzmttG9mU1DAHIQIq1foAXgmemnf3XXyxr9VHh94notzKYRBMQcEIgvhxYc8A/Pes9AQhAhjRo4u3zDf+CtKa1yU48O3hfBnNoAgJOCTDR1XONysVZBoEAZKCkRU6bnwnis/uKv3X1xecpTT/B3f4MMNHEPYGlF9PkBa3pkbtMg0EADIQ0yex8o/K0tCf+alPtvyPhPzfBxv8gUBQBzfyF+Wn/7abxIAAGQqzlS3PbgrcmNYuf8FOV9n485GMqNfxfJAFN+mFRwQbTA2sQAGNW5P3NRvCJpGZjE+GfCssXjWbQAAQKJiDC57Zm/BvShoUAmGYARK+fa1S+nnj9Px5+XikxTrUKzj2GAwEizZ9rbvPfCQGwKAbW0Ulz20Z+nmSiNt75ASl6scUQ6AoCrgh8v9monAYBsMAbdPzR+y7n+cRLgPHFeVHqqRZDoCsIOCEgOmq1to3UIAAWeJvKr1Kd24kzgMnFNl7ptQCMrg4J6E6zUQ0gABaITR+mqE12xMJ89q5aP8Qef0Vr+pYQ7a56lXvvj98TTxGnJeN1CZ5OdOyidDayps2s6LVa5I2K1Nrsg9u31FofVB5/RTR9yyPazWHlnv1VejCL/xsW6TjxOxsjps3MdCb8z54PU/3iJqCBpQlgAQKwl0iurB6o3Jj18U5TecSPLUdrOudqrS71PNloam/5/77Y/4qqfOn+Oh+2tLXUvVaXoyXqvBX+m2ma6hcCMKACEG/6wMwfau2pbKebODKnuocWdQlqOvyAJj2lSFV7sJDYJV6HZlHjrVl/O+3geFeb/H+x/1F4kWZdh/8r44UAWJadCaCjGcBerfUfzG+r/szS/Uzda/X2ZtFqJ5OckKmDqZHQXR7ps/fPVH9qaprH/2MT7U2hVjtzm82U3f8joJrqFzOAwZsB/CDo+GelrTzkcdA80UatLhsoDL9Bik62sq/pjorvn3V/nVtWdrrsPPZBWa9VeAsrelGXXR/fvOz+PyF4CIBVNRCZAOY8A9gbdPwzij74lxGNflCeJtzZpTw+vkdseyvKf2XRB/+yr7EIhBztspgJlN3/J6XNVL+YAQzIDECTPkSaTi1q2p8U9mi9/WLRsqvba+r4noX4dOoD9ep/9ygeuXSLLwciltuGzf8keBAAy7IyAcxrBpBlxyHLUDJ3r012PkRE2zJ3ICIhuajVCK7ppo+rtrXJzqVEdEU39svuPwSgm2x30bYQARDa09zjn5zlbv/oVPulKlJbI6HXeBw9U5RKf9BD67Yo9T9MfKtWekem7xfWJVjX6ezt4lJgb1P5J2f5TsK6ifapSqmtOqRXKy96lvkhqnhffPULYf6m0vrauZlgtzF9dQme2ol+1sWlQNn9T0Riql9cAgzCJYDI25szwRfSXInX7sO10d8qkb8wHgBpDTR/rnrIO9/0TMHYZPvdQvzpTGNl8D9euyfpfMp23wRhvnbkIe8ik/+jk+0tTLxjKPxPCRICkKkCkhuZAFpfAmj9UPVQsCGtoB/db/AbRPRyy3CWugvRv48c8F+XNuboJXKMjLRnFak1qWNm8H/p4NfhLXn5TyTfWqsqb/hFnReSfIvH1Lq93/jEY9n9tzyBYQZgCdBWAJjlhrnp4Nw0N2rj7S+Q4rflcfA/ZmPp24b+ltRxJzs7iegP0tr00f9PN6f9vzT4/w9E9Mer2X9TTZhOYBCAPguAiGxpzQTXJ7kRX/OzcOJ+hKYCSP5fCwm/JG0f+dpE+73EvD1tjP75T8SsN81NV3+U5N/YRPtdwvyZ1ey/Kf8QABOhPguAacPRsanwOhHZahnGit3j6+nWtP/uxGn0ROdVxPSdtLGN/o+HO0RJ6kyj99j4U82G/95kAei8XJh2rWb/TewgACZCfRaAivJraQ/O1CY78c6uz7YMI6n73maj8rykP9d9SH5TeeEv08Y2+b/uQ+E+5clJTvyPV09mKonfYYgfDBI/3L+a/TdxhQCYCPVZAEz7DYyNLy6alvp6DVGTXpxvVBO/WPzozceDafZN/tcc7pfAWrfntlUTX2I66XypPviUMPFGYRxX2f035R4CYCLUbwFoVFLvw9jeZLQM39i9nweQ0bkMDcruvylECICJEATAipDX8cf2X87NxPsIbi9hrHyPO5fdfxMACICJEATAipBmOX1+Org9ycjYZHitkJxnNYjDzmX334QGAmAiBAGwIiQkW1uN4LOJM4CJ9inEfIfVIA47l91/ExoIgIkQBMCOEMuNzengnDQjLpcy7Zyn+Itv5fbfsn7xIJAlQNubdCaFtrVvfYAYDMSbfVb9YH3afn/PuFDWLB7d+Soxv9q1P93aL7v/pnhN9QUBgACYasj8v8g7mjPB59MaLonAsdFVJNG7iZR13Zmd6qJF2f1PCRUC0EUdrNTUBND2DO3avmX4Wbvva+73X5Bl88/1E4sv1MrbSiKv1aQ3drtxR1aHumxXdv8TwzXVl7US2x4AXSaq8OYmgLbxu7ZfFDAhurjVqFyd+3jL3zWIOicw0SYldKYo+V0idUyeY5Xd/yQWpvqCAOASIJfjKN4GvCJ0ahE7AT+9Lkd1dOctJHwZMT0njwDK7j8EII8qWMGGSUExA3gctH1exz8j7cGgXNO0VSqj68PzhfU2474F2QYuu/9PitJUv5gBYAaQ7dDI2krTHV7kv74wESCiDROLL4hI7cxlNlB2/5+QJwhA1sJNaGcCiBnAiuD2KaXPnq1Xf2KJP3P3DX8ttcgLv5bTp9rL7v9j3Ez1ixkAZgCZD7JuGi59GozURGu//8ksqwPd2E5quyQClfA/8nh9uuz+LzOCAFhWlgkgZgBGwLl/HDRtxPX1xZNDTbfndE8gHqrU/pvqFzMAzACMR3AeDZY+D058szDd6in6EVHlnmMeoAfv3s6Ledg/0sbYROdiYfponnbL6j8EwLIKTAAxA7AEbOgeb/ohSt1LxP+mJLpudqb6Y+OIW6VS2xDGHyZ1tZOS0YXlBv3231S/mAFgBpC5mAehoRB/5mjlXZS2JXjsZ22y/Q4iTtxstV+xFO0/BMAy0yaAmAFYAu6lu8i3qwcrbzB9SyE8aum7Bmt7GcJpnwL9N9UvZgCYATitdVfGmfi6uYb/rjT7tan2l0n4za58sLFblP8QAJssxZtGOt6zz7V9y/AHu7uSU5r14M4kJ0cn2+cx8bUDG0QB/pvqCzMAzAAG9vgwOWb6rsHYhPm7AKYxXP5fhP8QAMsMmgDiHoAlYJvuQnc1ZyrPTTKR5bsANsNb9y3Af1P9YgaAGYBlHesH0w2o4ywHSOyex3cBiMrufzpdCIBl9ZkAWs8AlF+lOrcTz2L4MIhTAejndwHyETAIgOUhbgfQVgD6ui+94dNao1PydJbwvjRCffWfKPXTZuunZExLOLua/TcVv+kEhkuAPl8CKJGXzc4EtyXOAFzuq8+c+ontWr3zStL03TREpn31R6fCz7BI6nKdqYiT/o8fqmk1/Pck/b9+qv0yLfy91ey/iR0EwESozwIgJO9qNYLrktwYrbdfzJp/YBnGCt21sPApczPB7kTxmWi/R5g/lTa2iJzXmgl2pNjYJMyJS3VWceWwjFZ2/038IAAmQn0WACK6qdmopD6sUhsPrycl77AM5XHdTUtQceN1k51/VER/mDquli83twV/ktbGxdeBcnuQpuz+W9YvLgEsAdreA9CkD/mHg/WzV/GhJFfiLbUXjglvYaJX5iQC31mr/LPSnqev1eVoHbZnlVJHpU6htT7oLwQbTP7n/F2A71YP+Gfl8Shw/JZfmf031QNmACZCfRaAeHjTNDRus7QRZhh90m4moIXJ23GU8t5vepmmm6foRGRLayZIffFmScSOjT5mez+AmXcED3nvTzv4Y16jE+13MnPiJ8uOTHvZ/U8rYQhACQQgivieB5re87LsnBPfEyBRW1jkNaz1CaJUkHqGJr2oRN1Lim9lrXekXfM/ZqcuwaiO9jDJCVnwaeKfzyvv+WnLmct2xibamzSr81jktcT6BCJVSRtj+XVaIb6Vld6R9ujvkf6v09F/K5ITh8F/CECWLPfYxqSgtpcAR7h1WbNRubJHN3PtVpvofJCYPtyNURa6dG6m8pFu+rhqW5vsXEpEV3Rjv+z+J8Vqql/cAxiAS4DYBa31gsf8skxn6G4qu8u2tYn2KZrle91+sWfJf49On5uu/qjLIXNtHs8wIpbbhs1/CECuZfRrYyYFzXEGQPGlgC/eK+b+hlMfXnEUKtXqskF0tCvr1P+JfsSXAl7ondEv/+Nn/0OOdnmebOyFUdn9XylmU/1iBjAgM4BlN0TTj5T2X1f0QRQfPFqFt7CiF/Vy8Cz3iYh2+6F/VtH+x+IV6fBrHtGmYfb/ibFDAGyqoYD9AFZyb2kmoPTZRV0O1OrtzaLVzl7P/E+MoXD/J9qnCKt/gv9PriYIQAkFYPmegGI13Zz1r86yOtBTmHUJajr8gCY91e01s2k8Ha8+kJpqKv+aLKsDJnsr/X/S+VJ98CnhRax13bQa0q39svu/HC8EoNvMP6G9CWCe9wCSZgPK01d4hys3pD1s002Y8UM+ojvnEqlL8zprJo0fzwaU0leyV7mhWeeD3fiZ1Hb0EjlGVTvnalaXwP90oqb6xT2AAbsHkORO/MSgIvVVIfmmx7Q76776x9dl5HCbjiMv3EgsmyPhM0n0G0xP+OVxoB5pQ2t9mFh91SP5JhHvpsi/56iAHvxFnRaJWFYeT/j4OlUPEf0GU7iRItkcMZ9JpN+Q44c/MoVaVv8hAJnSm9yon++LW7qO7kNOwLTfQIwHMwBDkQQdf/S+y3k+qdnY+OK8KPXUIa81hD+ABDRFzfnGyFiaaxAAU+I4enZzeuTupGa1yc5/EdFLTGbwPwgUTYCJbp9rVE6HAFiQZ6LXzzUqX08UAAev6lq4i64g8BgB1vzZuW3+VgiAVVHI+5uN4BNJJkYnw3OZ5O+thkBnEHBAQITPac34N0IAbOCy3NicDs5JMvHUuhzLur2/6LvSNiGh7+onEK9aKD9Yb1p6xT0AQy1oktn5RuVpyUtVROvGw88rJW9f/WWFCEtDgPn65rS/xeQvBMBEKH5TT+S0+Zng+4mXAVMLz+GIf0pK+RnMoQkIOCagO6zlt+a2jfzcNBAEwEQo/p/pk83pyvvSmo5Ndq4Sog9kMYc2IOCYwJXNRuWyLGNAALJQIj1fUcEz76/z4aTm8ZZXi2vD20nRyZlMohEIOCAQv415rPJfZtrybXloCEDWJIi8rzkTfDKteW1q4SQttEuRV8tqFu1AIC8C8f0qX+lXzNZH7slqEwKQlRTRfeqw/1zTCznxq7U6pK8rxalPYGUfFi1BwEwgPvg9obO6fYUcAmBme2SLy5uNygdNXcbGF07U5O203VzDNA7+B4GYQKTpzoof/WE3Z35cAvRUO7rDTC/Nsu9d/BbeQQmnWfSFpp1ve3IFnUCAdIdYXX3cA3797u282AsQzAC6p7aXlP9S0wMWy2bj2YB4/mU6it5a9Cu43YeGHmUgEL8a7mnvBvbDK3o56x8ZIwSgl4wL7Wzu8d9MN3GUtXu8CQfp6I3MdCaJbI4oOsHTfFzeO9lk9QftykEgfqU3UvIgk3ePYt4tQreS8m7OegIyRQkBMBFK+P+RL9N6f5n2hGCPptENBAojAAGwQL0kAj/zzu9mJmAxHLqCQO4EIAD2SP9ZFvy3tT7CB+xNwQIIFEsAApAHb6G7yJO3ZPpuXR7jwQYI5EQAApATSNI6FE99jNmfyesGTV6uwQ4IJBGAAOReG/p/hfij3uHKDtNTg7kPDYMg0CUBCECXwLI2Z60fEM+7UZH+4ixXbqc666x90Q4EiiIAASiAdLw7qyLv20LyPWa1Vyi8229XfzVSpQPp++IX4ByGGGoCEIChTv/gB2/6sMXgRzDYHkIABjs/Q+8dBMBtCUAA3PKFdUsCEABLgIbuEAC3fGHdkgAEwBIgBMAtQFh3SwAC4JYvZgBu+cK6JQEIgCVAzADcAoR1twQgAG75Ygbgli+sWxKAAFgCxAzALUBYd0sAAuCWL2YAbvnCuiUBCIAlQMwA3AKEdbcEIABu+WIG4JYvrFsSgABYAsQMwC1AWHdLAALgli9mAG75wrolAQiAJUDMANwChHW3BCAAbvliBuCWL6xbEoAAWAJ0PwNYbOPTV26TNLzWdafZqAbDG7/7yK1nAKPjC01W3qh7VzHCsBEQHbVa20bwqXWHibcWgLHJzm1CdJpDH2F6SAkw0e1zjcrpQxp+IWHbC8BUeJ2IbC3EWwwyVARY82fntvmoLYdZtxaA2kT4ZmL5skMfYXpICYjwOa0Z/8YhDb+QsO0FoC5H67A9i09fF5KvoRlEk36YF4L1+OSa25RbC0Ds3th4uEOUbHHrKqwPFQHmzzen/XcMVcx9CDYnAVg4UYj3kFJ+H2LAkKuNgNYhkTy/uW1k32oLbdDiyUUA4qBGpzpXstAlgxYg/CkfAWG6qjVd+avyeV4+j3MTgGdcKGsW1ob/yYpeVD4M8HhQCGiiH6854J/+y2v44UHxaTX7kZsAPHIvYOHEiNQupXhsNUNDbG4IxA/+MNErMPV3w3clq7kKQDxArd7erLW+RZGHJ7iKy2PpR4oPfvLVWa16cEfpgylRALkLwJIITC2cpMXbqYheWCIWcLVfBDT9hCg6G2f+4hPgRADiMJbuCRwTTrPWF2J1oPjElmJErUPx1McD9qfur/PhUvi8ypx0JgDLnGrjC88mz79MS3SOIrVmlfFDOD0QiB/yUex9iaLwCpz1ewCYYxfnArDs6+glcgxVozcqoTNJySato42s+Di8SpxjNgfSlO6IlgeV8u4hzbs106206N2MJ/wGI1mFCcBghAsvQAAEjiQAAUA9gMAQE4AADHHyEToIQABQAyAwxAQgAEOcfIQOAhAA1AAIDDEBCMAQJx+hgwAEADUAAkNMAAIwxMlH6CAAAUANgMAQE4AADHHyEToIQABQAyAwxAQgAEOcfIQOAhAA1AAIDDEBCMAQJx+hgwAEADUAAkNM4P8By9168XYbCZcAAAAASUVORK5CYII='),
(71, 'ChemDraw eXchange file', 'chemical/x-cdx', '.cdx', ''),
(72, 'Chemical Markup Language', 'chemical/x-cml', '.cml', ''),
(73, 'Chemical Style Markup Language', 'chemical/x-csml', '.csml', ''),
(74, 'CIM Database', 'application/vnd.cont', '.cdbc', ''),
(75, 'Claymore Data Files', 'application/vnd.clay', '.cla', ''),
(76, 'Clonk Game', 'application/vnd.clon', '.c4g', ''),
(77, 'Close Captioning - Subtitle', 'image/vnd.dvb.subtit', '.sub', ''),
(78, 'Cloud Data Management Interface (CDMI) - Capability', 'application/cdmi-cap', '.cdmi', ''),
(79, 'Cloud Data Management Interface (CDMI) - Contaimer', 'application/cdmi-con', '.cdmi', ''),
(80, 'Cloud Data Management Interface (CDMI) - Domain', 'application/cdmi-dom', '.cdmi', ''),
(81, 'Cloud Data Management Interface (CDMI) - Object', 'application/cdmi-obj', '.cdmi', ''),
(82, 'Cloud Data Management Interface (CDMI) - Queue', 'application/cdmi-que', '.cdmi', ''),
(83, 'ClueTrust CartoMobile - Config', 'application/vnd.clue', '.c11a', ''),
(84, 'ClueTrust CartoMobile - Config Package', 'application/vnd.clue', '.c11a', ''),
(85, 'CMU Image', 'image/x-cmu-raster', '.ras', ''),
(86, 'COLLADA', 'model/vnd.collada+xm', '.dae', ''),
(87, 'Comma-Seperated Values', 'text/csv', '.csv', ''),
(88, 'Compact Pro', 'application/mac-comp', '.cpt', ''),
(89, 'Compiled Wireless Markup Language (WMLC)', 'application/vnd.wap.', '.wmlc', ''),
(90, 'Computer Graphics Metafile', 'image/cgm', '.cgm', ''),
(91, 'CoolTalk', 'x-conference/x-coolt', '.ice', ''),
(92, 'Corel Metafile Exchange (CMX)', 'image/x-cmx', '.cmx', ''),
(93, 'CorelXARA', 'application/vnd.xara', '.xar', ''),
(94, 'CosmoCaller', 'application/vnd.cosm', '.cmc', ''),
(95, 'CPIO Archive', 'application/x-cpio', '.cpio', ''),
(96, 'CrickSoftware - Clicker', 'application/vnd.cric', '.clkx', ''),
(97, 'CrickSoftware - Clicker - Keyboard', 'application/vnd.cric', '.clkk', ''),
(98, 'CrickSoftware - Clicker - Palette', 'application/vnd.cric', '.clkp', ''),
(99, 'CrickSoftware - Clicker - Template', 'application/vnd.cric', '.clkt', ''),
(100, 'CrickSoftware - Clicker - Wordbank', 'application/vnd.cric', '.clkw', ''),
(101, 'Critical Tools - PERT Chart EXPERT', 'application/vnd.crit', '.wbs', ''),
(102, 'CryptoNote', 'application/vnd.rig.', '.cryp', ''),
(103, 'Crystallographic Interchange Format', 'chemical/x-cif', '.cif', ''),
(104, 'CrystalMaker Data Format', 'chemical/x-cmdf', '.cmdf', ''),
(105, 'CU-SeeMe', 'application/cu-seeme', '.cu', ''),
(106, 'CU-Writer', 'application/prs.cww', '.cww', ''),
(107, 'Curl - Applet', 'text/vnd.curl', '.curl', ''),
(108, 'Curl - Detached Applet', 'text/vnd.curl.dcurl', '.dcur', ''),
(109, 'Curl - Manifest File', 'text/vnd.curl.mcurl', '.mcur', ''),
(110, 'Curl - Source Code', 'text/vnd.curl.scurl', '.scur', ''),
(111, 'CURL Applet', 'application/vnd.curl', '.car', ''),
(112, 'CURL Applet', 'application/vnd.curl', '.pcur', ''),
(113, 'CustomMenu', 'application/vnd.yell', '.cmp', ''),
(114, 'Data Structure for the Security Suitability of Cryptographic Algorithms', 'application/dssc+der', '.dssc', ''),
(115, 'Data Structure for the Security Suitability of Cryptographic Algorithms', 'application/dssc+xml', '.xdss', ''),
(116, 'Debian Package', 'application/x-debian', '.deb', ''),
(117, 'DECE Audio', 'audio/vnd.dece.audio', '.uva', ''),
(118, 'DECE Graphic', 'image/vnd.dece.graph', '.uvi', ''),
(119, 'DECE High Definition Video', 'video/vnd.dece.hd', '.uvh', ''),
(120, 'DECE Mobile Video', 'video/vnd.dece.mobil', '.uvm', ''),
(121, 'DECE MP4', 'video/vnd.uvvu.mp4', '.uvu', ''),
(122, 'DECE PD Video', 'video/vnd.dece.pd', '.uvp', ''),
(123, 'DECE SD Video', 'video/vnd.dece.sd', '.uvs', ''),
(124, 'DECE Video', 'video/vnd.dece.video', '.uvv', ''),
(125, 'Device Independent File Format (DVI)', 'application/x-dvi', '.dvi', ''),
(126, 'Digital Siesmograph Networks - SEED Datafiles', 'application/vnd.fdsn', '.seed', ''),
(127, 'Digital Talking Book', 'application/x-dtbook', '.dtb', ''),
(128, 'Digital Talking Book - Resource File', 'application/x-dtbres', '.res', ''),
(129, 'Digital Video Broadcasting', 'application/vnd.dvb.', '.ait', ''),
(130, 'Digital Video Broadcasting', 'application/vnd.dvb.', '.svc', ''),
(131, 'Digital Winds Music', 'audio/vnd.digital-wi', '.eol', ''),
(132, 'DjVu', 'image/vnd.djvu', '.djvu', ''),
(133, 'Document Type Definition', 'application/xml-dtd', '.dtd', ''),
(134, 'Dolby Meridian Lossless Packing', 'application/vnd.dolb', '.mlp', ''),
(135, 'Doom Video Game', 'application/x-doom', '.wad', ''),
(136, 'DPGraph', 'application/vnd.dpgr', '.dpg', ''),
(137, 'DRA Audio', 'audio/vnd.dra', '.dra', ''),
(138, 'DreamFactory', 'application/vnd.drea', '.dfac', ''),
(139, 'DTS Audio', 'audio/vnd.dts', '.dts', ''),
(140, 'DTS High Definition Audio', 'audio/vnd.dts.hd', '.dtsh', ''),
(141, 'DWG Drawing', 'image/vnd.dwg', '.dwg', ''),
(142, 'DynaGeo', 'application/vnd.dyna', '.geo', ''),
(143, 'ECMAScript', 'application/ecmascri', '.es', ''),
(144, 'EcoWin Chart', 'application/vnd.ecow', '.mag', ''),
(145, 'EDMICS 2000', 'image/vnd.fujixerox.', '.mmr', ''),
(146, 'EDMICS 2000', 'image/vnd.fujixerox.', '.rlc', ''),
(147, 'Efficient XML Interchange', 'application/exi', '.exi', ''),
(148, 'EFI Proteus', 'application/vnd.prot', '.mgz', ''),
(149, 'Electronic Publication', 'application/epub+zip', '.epub', ''),
(150, 'Email Message', 'message/rfc822', '.eml', ''),
(151, 'Enliven Viewer', 'application/vnd.enli', '.nml', ''),
(152, 'Express by Infoseek', 'application/vnd.is-x', '.xpr', ''),
(153, 'eXtended Image File Format (XIFF)', 'image/vnd.xiff', '.xif', ''),
(154, 'Extensible Forms Description Language', 'application/vnd.xfdl', '.xfdl', ''),
(155, 'Extensible MultiModal Annotation', 'application/emma+xml', '.emma', ''),
(156, 'EZPix Secure Photo Album', 'application/vnd.ezpi', '.ez2', ''),
(157, 'EZPix Secure Photo Album', 'application/vnd.ezpi', '.ez3', ''),
(158, 'FAST Search & Transfer ASA', 'image/vnd.fst', '.fst', ''),
(159, 'FAST Search & Transfer ASA', 'video/vnd.fvt', '.fvt', ''),
(160, 'FastBid Sheet', 'image/vnd.fastbidshe', '.fbs', ''),
(161, 'FCS Express Layout Link', 'application/vnd.deno', '.fe_l', ''),
(162, 'Flash Video', 'video/x-f4v', '.f4v', ''),
(163, 'Flash Video', 'video/x-flv', '.flv', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAYaElEQVR4Xu2dbYwkxXnHn6rumd3jwKdwO3txhBzMi4ntGEiM7yJbThS+AEZRLBLhYPIiZIOVRJx9MjEo3tudnV0E2Ni8nKLEmCBHRECERJTIvCUKyYcI25CYI9jKwR2YDxxht2fPMfe2O9NdFc0eG+5lZ6q7q56erun/fZ2up576Pc/8prpr5lYQ/oEACFSWgKjsyrFwEAABggDQBCBQYQIQQIWLj6WDAASAHgCBChOAACpcfCwdBCAA9AAIVJgABFDh4mPpIAABoAdAoMIEIIAKFx9LBwEIAD0AAhUmAAFUuPhYOghAAOgBEKgwAQigwsXH0kEAAkAPgECFCUAAFS4+lg4CEAB6AAQqTAACqHDxsXQQgADQAyBQYQIQQIWLj6WDAASAHgCBChOAACpcfCwdBCAA9AAIVJgABFDh4mPpIFCYABpNfTqp5Eoh6FLS+mKlknOEFJuIZA1lyE8gatUG1rAx3bkxatV35Z8BI0eZALsAJqeWzyUKb04ouVZKedoowxzG2swCWIk1Bb/dboWPDyM/zFluAmwCOLupxw/reIYSdRNJGZYbg7/ZmQXQ1UqpQzIUn4ya9d3+rhSZcxBgEcCW5vI5XRU8FhBdxJE0Yr5LII0A3rl6v0rCbUu3iv3gBwJrBJwLYMvOlY/EQvyzJLEFmPkJZBBAL5kXSIa/HjXFIf7MMIMPBJwKoPfJHyv5LN78xZU+owCItPputKf+aXpUJMVliZnKSsCZAHr3/G+r+PvY9hdb6swCICJFYtdSK9xebKaYrYwEnAmgMd29nYhuLuMiRzmnPAI4xkNvx/HgKHdGurU5EUDvqE+T2IOn/emgu7wqvwBUguNBl5XwM5YjAcT3aamv9xOB31nnFwARjgf9rr2L7K0F0PuGn4o7C/iSj4tyZI9hI4B3ZsPxYHbsIzPCXgDT8WeI9CMjQ8SzhTgQQG/FOB70rO6u0rUWwORM/C2t9Q2uEkKcbAQcCQDHg9mwj8zV9gKY7v5AE20dGSKeLcSZAHA86Fnl3aRrLYDG9EqbSG52kw6iZCXgUgDH5sbxYNYa+Hy9CwF08JPe4bWAewHgeHB41Sx+ZgcC6Ori08aMawTcCwDHg1XqLgjA82pzCOAdJDge9Lw30qQPAaShVOJrGAXQWzWOB0tcexepQQAuKA4xBrMAcDw4xNoWMTUEUARlxjnYBYDjQcbqDT80BDD8GlhlUIQAcDxoVaJSD4YASl0ec3LFCQDHg+Zq+HcFBOBfzU7IuDgB4HjQ81ZZN30IwPOqFikAHA963izrpA8BeF7TIQgAx4Oe98zx6UMAnhdzSALA8aDnfbOWPgTgeSGHJgAcD3reOcfShwA8L+MwBYDjQc+bBwLwv4DDFwCOB33uIuwAfK4eEQ1fADge9LmFIACfq1cSAeB40N8mggD8rd1q5mXYARyHEL8e9KyfIADPCnZyuiUTAI4HPesnCMCzgpVeADge9KqjIACvynVqsqXbAfx/ivjPRX1oLQjAhyoNyLG8AsDxoA+tBQH4UCUvBYDjQR9aCwLwoUqeCgDHg+VvLgig/DUamGF5bwFOSBvHgyXtMwigpIVJm5YnAsDxYNqCFnwdBFAwcNfTeSMAHA+6Lr2TeBCAE4zDC+KTAI5RwvHg8Lrl1JkhgDJVI0cu/gkAx4M5ysw2BAJgQ1tMYP8EgOPBYjoj3SwQQDpOpb3KRwHgeLA87QQBlKcWuTLxWAC99eJ4MFfV3Q2CANyxHEokzwWA48GhdM27k0IAQy6A7fSRDMeoKTr94kxOraxoKeu283COVyR2LbXC7ZxzIPb6BCAAzzujJsPGm03R7reMxnR3DxFdUP5l4nhwGDWCAIZB3eGcSuutS3P15/sKYCb+C9L6TxxOyRQKx4NMYAeGhQCGQd3hnFrr69tz9fv73gLMrFyoNe0mkta1dpj2uqGUUodkKD4ZNeu7uedC/GMErJuiMd3VgDk8AkLohxZn69cOymByOv4rTfoLw8sy08z7VRJuW7pV7M80ChfnIgAB5MJWpkHq4NjB+pY37hJH+2V1dlOPH1bxU0T0G2XKfEAuOB4sqFAQQEGgWafR+rporv6dQXOctUNv6GxM7tEy+bwPtwOk1XejPfVP06MiYWVX8eAQwCg0gKZXoiD8MDVFbFpOY2fnV4SQX9CkLxVK/WKZjwhxPGiqpv3rEIA9w1JE0Jq+3J6rfbMUySAJbwhAAN6UanCiitTRmqaPvTU39uMRWRKWUQABCKAAyAVOsTfohp946zYRFTgnpvKYAATgcfH6pP6CFOHlC7NicfSWhhW5JgABuCZagngqEftCmVy1MDf2UgnSQQolJgABlLg4NqkppZYDKacXZXjPoB8L2cyBsf4TgAD8r+HAFSgSrwpSdwRHag8t3CkOj/hysbyMBCCAjMB8vVyROixJPiG0foZIvKiD8Ccbif739SatEAl8ndvXwlrmDQFYAsRwXgKm//CEd/bRjw4BjH6NvV4hBMBbPgiAly+iWxKAACwBGoZDALx8Ed2SAARgCRAC4AWI6LwEIABevtgB8PJFdEsCEIAlQOwAeAEiOi8BCICXL3YAvHwR3ZIABGAJEDsAXoCIzksAAuDlix0AL19EtyQAAVgCxA6AFyCi8xKAAHj5YgfAyxfRLQlAAJYAsQPgBYjovAQgAF6+2AHw8kV0SwIQgCVA7AB4ASI6LwEIgJcvdgC8fBHdkgAEYAkQOwBegGmiK0oiKeS/akXfE0K+TCreV0/GDmw4RIf27RIraWLgGhDgIIAdAAfV1Zjqp0TBQ1KrBxfmas/hf91hA43AFgQgAAt46w1VSr0lhPy6CML7oqY45Dg8woGAUwIQgCucSsU6kHfT0bDV/po46Cos4oAAJwEIwAHd1f95V6qr2836Dx2EQwgQKIwABGCJWmj6xzgI/+BAU7xtGQrDQaBwAhCADXIh7o9E8Mdp/iy3zTQYCwJcBCCAvGR7b/7Z4AY83c8LEOPKQAACyFEFTfQPbRn+bpZP/p+7WW+q1ZPf0pJ+k7S+SCj1fi3pDCJZy5EChlSGgOoS0dtE8ickxG6t6F9oJXjc1YNmCCB7I+1NZHhJ2nv+ianlC3QQ3kJJ8ntSyvHs02EECJxIQCl1RFLwMAXx7dHs+D4bPhBAFnpKxRSKj0XN+m7TsF9o6tNiFc9rUtuJZGC6Hq+DQHYCqqtJ3j1+MJx54y5xNPt4IgggG7U7olbtFtOQ3qe+FMFjWtCHTNfidRCwJaCIXpIiuSrPbgACSEtf0ZtyOfyA6S/sTuzsfJS0ekrIYCJtaFwHArYEVn9vouVl0Vz9hSyxIICUtLTWO9pz9bsHXd775Ceif8ebPyVUXOaUwKoEFH0imh/fmzYwBJCClFDqgFiuv2/Qp/+Wm/RGdVr8PBF9MEVIXAICLAR6twMbDobb0j4TgABSlEGR2LXUCrcPurQx3e3tDr6YIhwuAQFWAoLoG4ut2k1pJoEAUlBSWm9dmqv3Pt3X/be5ufJLUtGP8LQ/BUxcwk9g9Ydp+sPt2fFXTJNBAAZCivTCUqv23kHf+GvMdP6GtPhDE2y8DgJFEVBCfGdpNrzONB8EYCAklH54cb7+2X6X9b7hJ2udt/AlH1Or4fUiCShSR7Ws/7zpC2sQgLEq+ktRq35Pv8smd8a/r4V+0BgGF4BAwQS0Fte258KHBk0LAZh2AESXL7ZqT/e9/5+KH5BSG7daBdce04EAkRJ/Hc2Hn4cALJpBqOS8xfnxV/uFaEx1/5Mk/arFFBgKAlwEno9ata0QgAXeejec2H+bWOp7CzC1sqSlPNNiCgwFARYCWiXt9vx4AwKwwBvJcIyaotN3BzC90sFPei0AYygjAdWNWmN1CMACsekPUzSmu9oiPJni28R2Mda0PlP+pvEucrSKIZLzB/2IZvNX470y0OdZzdFnsErEvqVbw/P7314un08yMJ7lD8rNVB88BDRU1gTQtsFN8TkaL0tM0/pM+ZvGZ8mF51q9PWrVd/WLvXk6vleSvpFlbkH3RrO1vt8ebezsbCch+p5ApcnJVB8IAAIYSMD0BjY1mGl8mibmvUY9GbXGPtX/Fi++gkg/wZKDpCuiZu2pfrEndq48KYS83GZuU30gAAig0gLofWHmDFk/8/WmWF4PxFk79IajGzsHXH/Rq6h5IQAbfRIZ79FtP+FMBbJM33q4aX2m/E3jrRN0EaCAT+JT0yxm52GqD3YA2AFUegewuvgC7sVPhVzMswcIwPITwgTQ9hPOFN8yfevhpvWZ8jeNt07QQYAinsafkmZBpw+m+mAHgB0AdgCru4DijgOLFA4EYPkJYQJo+wnHHd9y+cbhvuf/7gKL2ZIXfcthqg92ACXfAdgKxvgOtrzA1GBlz//d5RfzUG51vgIfOprqAwFAAFYKMDWYLwIo6liuqHnWimqqDwQAAUAAawQK+WQucKeR4hgbAoAAIIA1AoUcBxb4rAECsOrt1cGmLZTtFpc7vj2BwRF8z//41RXydL7A04Y0/YsdAHYAVo4YJQEce0LPdxxYiGBOqqapPhAABAABnECAcYteyC3GieWEAKzaG7cAJnymBrO9RTLN7/51xod0hTxkhACc9gR3g3PHdwpjnWC+53/ykriO6bjimuprqg9uAXALYOqhga+bGsy/HQDXF3UYdxYDKmSqDwQAAUAAJxNguVdnfLYAAVj18FA/4UyGLvsnqO/5r1d8lqf1jKcLgxrYVB/sALADsLKnqcHKLrC+i3f4hmURSsqqmeoDAUAAKVtp/ctMDeatAMjhlp3lliJd2Uz1gQAggHSd1OcqU4P5KwCHD+2GcPy3Vi5TfSCAkgvA6t2ZYrDtG9TUYLbxUyyB5RJXx3au4uRdpKk+EAAEwPqHTXwVwGpbOPnkdriTyGEBCCAHtOOHmADaNrgpvmX6xuHc+dvGNy6A8wIn9+4OnyXkWKupv7ADwA4AO4A+PeDk6b3D04Qc73/jr1khAAgAAhjUAxZvYCcCyfOuP24MdgDMAG23uKYCWaZvHM6dv2184wLYL7DYwju5hbBboKm/sAPADgA7gIE9YPEQz8lDRAjAjoDlaJNBbT/hTPEt0zcO587fNr5xAcwX5D3GyzvO9XJM/YUdQMl3AGV/A5karOz5p3rD5fokt9g5pEoq3UWm+kAAEEC6TupzlanBRkIAue7lLZ4dWFXkxMGm+kAAEIBVu5kabBQEkOtpvsXpgVVBThpsqg8EAAFY9ZupwUZBAKuAMryhcwnDqgr9B5vqAwFAAFatZ2qwkRFAll8H5rplsCpD38Gm+kAAEIBV55kabHQEkOGhXq6HhlZlgAB48OF/BTZxrYoA0h7rkSR9hqyf+XpTLK/H7qwdesPRjZ0DUspxE1sXr5vqgx0AdgBWfWZqsNHZAaT9dSDpqDX2qX5QG9PxFUT6CSvoGQab6gMBQAAZ2unUS00NNlICSHVv3xNAfVc/qJun43sl6RutoGcYbKoPBAABZGinagsg1dP9gHQ0O76vrwC+Gu+VgT7PCnqGwRBABljrXWoCaPsJxx3fcvnG4b7nb1zgyRcYjgMHxWtMLZ9PMngl85wWA0z1wQ4AOwCL9uJ/SGqVHMvgwd/wGyiAnZ3tJMQ9LGn1CQoBWNI2AcQOoDbwQ8SWj2X5GIYPPg4cNOHEzpUnhZCXMyTVN6Spf7EDwA7Aqh9NDTZqAjAdB/aDWfTx31oepvpAABAABJCVgOGLPuuFK/r4DwLIWtSc91C2n3AmQ9vGd4Qh9xaz7Pnn4mM4DlwvZtHHfxBArsqeOoj7Dcod3xGG3ALgnt8Un0NApuPAdQVQ8PEfBGDqjJSvc79BueOnXGbuy0z55w7saCCHAFZTy3AcOIzjPwjAUQOZGty2wbjjO8KAHcApBNIfBzaGcPwHATjqfO43KHd8RxgggFMIpD8OHMbxHwTgqPO536Dc8R1hgABOIpD2OHBYx38QgKPO536Dcsd3hAECWI9AiuPAYR3/QQCOOp/7Dcod3xEGCGA9AimOA4d1/AcBOOp87jcod3xHGCCAdQikOQ7cPKTjPwjAUedHMhyjpuj0C9eYXukQyZqj6RAGBJwREEp1FufHxgYFxFeBDbjr3XBi/21iqd9lk1MrS1rKM51VDYFAwBEBRUm01BqfhABsgBq+8NGY7v4HEX3UZgqMBQEOAoLoucVWbRsEYEFXEF2+2Ko93fcWYCq+n6T+nMUUGAoCLASEEt9enA9vgACs8OovRa163//EYWI6vlaQ/lurKTAYBBgIaC2uac+Fj0AANnCFfiSarV/TL8SZTf0eoTpvSZIbbKbBWBBwSUApdUSG9S1RUxyCACzIKtILS63ae4mE7hdm81T8gJT6OotpMBQE3BIQ4v5oNrzeFBSnACZCRKS03ro0V3++36UTM8sfEIn4MUkZpgiHS0CAmYDqCqU/uDg//qppIgjARKj3eopvfE1Od+/URF9OEw7XgAAzgTuiVu2WNHNAAGkokVqqyfr73myKI/0u7/3oY2Vj/BxJ+uVUIXERCDAQSIhefI8Mf63fnyY7eUoIIG0RtP5iNFe/d9DljZnl85SmZyUFjbRhcR0IuCLQe14VSvXxheb4a2ljQgBpSRHtl0fCCxbuFIcHSqDZuVjF9LSUYuA3sNJPiytBwEyg9+YPNF22OFd/0Xz1u1dAAFloEd0WtWp/bhoyObV8rqLgMSHpQtO1eB0EbAkkinbXwuR3snzyr80JAWSir7pC0CWLs2P/ZRp2dlOPH9LxrNBqB34sZKKF1/MRUF0S8hubDoTNfbvESp4YEEB2ai+TDC8xfcFiLWxvN6CD8BaVJJ+VUp6WfTqMAIETCShShwMVPCTC+PY8n/rHR4MA8nSXpseiPeHV9KhI0g5vNPXppJIrhaBLSeuLE0reHyixSUtZTxsD11WPQO8nvYnUPxMUvCaFeFFreoZk8HjaDyATMQjARKjP65rEX7ZbwZ8O+oZgztAYBgKFEYAALFCvSuC/gxuz7AQspsNQEHBOAAKwR/r3ejn8o/bXxEH7UIgAAsUSgABc8Nb0CgX6M1GzvttFOMQAgaIIQACuSCsV60B+U4hwztUDGlepIQ4I9CMAATjvDfU/msTXgyO1+0zfGnQ+NQKCQEYCEEBGYGkvF0od0EHwiCT14IKoPUdNodKOxXUgUBQBCKAA0r3/nVVS8G+a9PeEkC9riveFnbGfjo/RwdebtIKjxAKKgCnWJQABoDFKTcD0h1NKnbwHyUEAHhSpyilCALzVhwB4+SK6JQEIwBKgYTgEwMsX0S0JQACWACEAXoCIzksAAuDlix0AL19EtyQAAVgCxA6AFyCi8xKAAHj5YgfAyxfRLQlAAJYAsQPgBYjovAQgAF6+2AHw8kV0SwIQgCVA7AB4ASI6LwEIgJcvdgC8fBHdkgAEYAkQOwBegIjOSwAC4OWLHQAvX0S3JAABWALEDoAXIKLzEoAAePliB8DLF9EtCUAAlgD5dwArHfzpK94iVTe66katMfzhFMYGsN4BTEwtR0IGE4w5InRFCWiVtNvz4/hT64z1txbA5HT3B5poK2OOCF1RAoLoucVWbVtFl1/Isu0FMBN/S2t9QyHZYpJKERBKfHtxPkRvMVbdWgCNnfHVJPTfMeaI0BUloLW4pj0XPlLR5ReybHsBNPXpKu4s4E9fF1KvykyiSB0Vy/Ut+JNrvCW3FkAvvcmp+D4t9fW8qSJ6pQgI8UA0G36uUmsewmIdCWD5XE1iD0kZDmENmHLUCCgVE+kPRfPje0dtaWVbjxMB9BY1MdO9Q2j6StkWiHz8I6AF3dmerf2Zf5n7l7EzAZy1Q29Y3hh/X0i60D8MyLgsBBTRSxsOhtveuEscLUtOo5yHMwEcexawfG5C8lkpxeQoQ8PaeAj0vvgjiD6OrT8P3/WiOhVAb4JGs3OxUuqfJAX4BldxdfR+pt6bn0J5WbtZ/6H3i/FoAc4FsCqBmeXzlA4ek0Qf8YgFUh0WAUU/Ikquwid/8QVgEUBvGavPBM6IZ4VSO3A6UHxhvZhRqVgH8u66CGfebIojXuQ8YkmyCWCNU2Nq+XwKwluUTq6RJDeMGD8sJweB3pd8pAgepiS+HZ/6OQA6HMIugLVcJ76iz6Cx5Eqp6VKS+iKlknOEFJvwU2KH1SxlKNXVSv9MyuA1UuJFJegZWgkexzf8ylGswgRQjuUiCxAAgeMJQADoBxCoMAEIoMLFx9JBAAJAD4BAhQlAABUuPpYOAhAAegAEKkwAAqhw8bF0EIAA0AMgUGECEECFi4+lgwAEgB4AgQoTgAAqXHwsHQQgAPQACFSYAARQ4eJj6SAAAaAHQKDCBCCAChcfSwcBCAA9AAIVJvB/LZEHxLmJPQIAAAAASUVORK5CYII='),
(164, 'FlashPix', 'image/vnd.fpx', '.fpx', ''),
(165, 'FlashPix', 'image/vnd.net-fpx', '.npx', ''),
(166, 'FLEXSTOR', 'text/vnd.fmi.flexsto', '.flx', ''),
(167, 'FLI/FLC Animation Format', 'video/x-fli', '.fli', ''),
(168, 'FluxTime Clip', 'application/vnd.flux', '.ftc', ''),
(169, 'Forms Data Format', 'application/vnd.fdf', '.fdf', ''),
(170, 'Fortran Source File', 'text/x-fortran', '.f', ''),
(171, 'FrameMaker Interchange Format', 'application/vnd.mif', '.mif', ''),
(172, 'FrameMaker Normal Format', 'application/vnd.fram', '.fm', ''),
(173, 'FreeHand MX', 'image/x-freehand', '.fh', ''),
(174, 'Friendly Software Corporation', 'application/vnd.fsc.', '.fsc', ''),
(175, 'Frogans Player', 'application/vnd.frog', '.fnc', ''),
(176, 'Frogans Player', 'application/vnd.frog', '.ltf', ''),
(177, 'Fujitsu - Xerox 2D CAD Data', 'application/vnd.fuji', '.ddd', ''),
(178, 'Fujitsu - Xerox DocuWorks', 'application/vnd.fuji', '.xdw', ''),
(179, 'Fujitsu - Xerox DocuWorks Binder', 'application/vnd.fuji', '.xbd', ''),
(180, 'Fujitsu Oasys', 'application/vnd.fuji', '.oas', ''),
(181, 'Fujitsu Oasys', 'application/vnd.fuji', '.oa2', ''),
(182, 'Fujitsu Oasys', 'application/vnd.fuji', '.oa3', ''),
(183, 'Fujitsu Oasys', 'application/vnd.fuji', '.fg5', ''),
(184, 'Fujitsu Oasys', 'application/vnd.fuji', '.bh2', ''),
(185, 'FutureSplash Animator', 'application/x-future', '.spl', ''),
(186, 'FuzzySheet', 'application/vnd.fuzz', '.fzs', ''),
(187, 'G3 Fax Image', 'image/g3fax', '.g3', ''),
(188, 'GameMaker ActiveX', 'application/vnd.gmx', '.gmx', ''),
(189, 'Gen-Trix Studio', 'model/vnd.gtw', '.gtw', ''),
(190, 'Genomatix Tuxedo Framework', 'application/vnd.geno', '.txd', ''),
(191, 'GeoGebra', 'application/vnd.geog', '.ggb', ''),
(192, 'GeoGebra', 'application/vnd.geog', '.ggt', ''),
(193, 'Geometric Description Language (GDL)', 'model/vnd.gdl', '.gdl', ''),
(194, 'GeoMetry Explorer', 'application/vnd.geom', '.gex', ''),
(195, 'GEONExT and JSXGraph', 'application/vnd.geon', '.gxt', ''),
(196, 'GeoplanW', 'application/vnd.geop', '.g2w', ''),
(197, 'GeospacW', 'application/vnd.geos', '.g3w', ''),
(198, 'Ghostscript Font', 'application/x-font-g', '.gsf', ''),
(199, 'Glyph Bitmap Distribution Format', 'application/x-font-b', '.bdf', ''),
(200, 'GNU Tar Files', 'application/x-gtar', '.gtar', ''),
(201, 'GNU Texinfo Document', 'application/x-texinf', '.texi', ''),
(202, 'Gnumeric', 'application/x-gnumer', '.gnum', ''),
(203, 'Google Earth - KML', 'application/vnd.goog', '.kml', ''),
(204, 'Google Earth - Zipped KML', 'application/vnd.goog', '.kmz', ''),
(205, 'GrafEq', 'application/vnd.graf', '.gqf', ''),
(206, 'Graphics Interchange Format', 'image/gif', '.gif', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAYp0lEQVR4Xu2da5BcxXXHT/e9M7uSwKrA7sp2UYSXjZ9AbJAciJOKPhiqqFQckuBgkjjESI6TMlgxMSRedmdn14WwwYCVVMwjhBQpoRQpUkmByzhlnA8pjEUMUrATIcTjA1K0O7NyQK/dmXu7U7NigzCaOXfm3J57e+5/v87t06d/59zf9EzPzijCHwiAQGEJqMKuHAsHARAgCABNAAIFJgABFLj4WDoIQADoARAoMAEIoMDFx9JBAAJAD4BAgQlAAAUuPpYOAhAAegAECkwAAihw8bF0EIAA0AMgUGACEECBi4+lgwAEgB4AgQITgAAKXHwsHQQgAPQACBSYAARQ4OJj6SAAAaAHQKDABCCAAhcfSwcBCAA9AAIFJgABFLj4WDoIQADoARAoMAEIoMDFx9JBAAJAD4BAgQlAAAUuPpYOAn0TwGjFnkQmvlwpWk/WXmBMfJbSajWRLqEMvROoVUsdazg60fhCrVre0vsMGDnIBJwLYGx84Wyi8MaY4qu11isHGWYWa+MFsBhZCn69Xg0fyyI/zJlvAs4EcEbFDh+20STF5gbSOsw3Bn+z4wXQtMaYQzpUH69Vyjv8XSkyd0HAiQDWVBbOaprgkYDofBdJI+abBJII4I2r95o4XDf/VbUX/EBgmUDqAlhz8+KHI6X+VZNaA8zuCXQhgFYyz5IOf7lWUYfcZ4YZfCCQqgBaz/yR0U/i5u9f6bsUAJE1j9Z2lT9JD6u4f1liprwSSE0Ardf8r5voKWz7+1vqrgVARIbUlvlqeF1/M8VseSSQmgBGJ5qbiejGPC5ykHPqRQDHeNjrcDw4yJ2RbG2pCKB11GdJ7cK7/cmgp3lV7wIwMY4H06yEn7FSEkB0j9V2g58I/M66dwEQ4XjQ79qnkb1YAK1P+JmoMYsP+aRRju5jSATwxmw4Huwe+8CMkAtgIvoUkd02MEQ8W0gKAmitGMeDntU9rXTFAhibjO621m5MKyHE6Y5ASgLA8WB32AfmarkAJpo/tERrB4aIZwtJTQA4HvSs8umkKxbA6MRinUifmk46iNItgTQFcGxuHA92WwOfr09DAA38S292LZC+AHA8mF01+z9zCgJo2v6njRmXCaQvABwPFqm7IADPq+1CAG8gwfGg572RJH0IIAmlHF/jUACtVeN4MMe1TyM1CCANihnGcCwAHA9mWNt+TA0B9IOywzmcCwDHgw6rl31oCCD7Gogy6IcAcDwoKlGuB0MAuS4Pn1z/BIDjQb4a/l0BAfhXs7dk3D8B4HjQ81Y5YfoQgOdV7acAcDzoebOcIH0IwPOaZiAAHA963jPHpw8BeF7MjASA40HP+2Y5fQjA80JmJgAcD3reOcfShwA8L2OWAsDxoOfNAwH4X8DsBYDjQZ+7CDsAn6tHRNkLAMeDPrcQBOBz9XIiABwP+ttEEIC/tVvKPA87gOMQ4r8HPesnCMCzgv1sujkTAI4HPesnCMCzguVeADge9KqjIACvyvX2ZHO3A/j/FPHloj60FgTgQ5U65JhfAeB40IfWggB8qJKXAsDxoA+tBQH4UCVPBYDjwfw3FwSQ/xp1zDC/LwHekjaOB3PaZxBATguTNC1PBIDjwaQF7fN1EECfgac9nTcCwPFg2qVPJR4EkArG7IL4JIBjlHA8mF23vH1mCCBP1eghF/8EgOPBHsrsbAgE4AxtfwL7JwAcD/anM5LNAgEk45Tbq3wUAI4H89NOEEB+atFTJh4LoLVeHA/2VPX0BkEA6bHMJJLnAsDxYCZd8+akEEDGBZBOX9PhEFVUo12csfHFRat1WTqPy/GG1Jb5anidyzkQ+8QEIADPO6Okw9F9FVVvt4zRieYuIjo3/8vE8WAWNYIAsqCe4pzG2rXz0+Wn2wpgMvorsvaPU5zSUSgcDzoC2zEsBJAF9RTntNZuqE+X72v7EmBy8TxraQeRFtc6xbRPGMoYc0iH6uO1SnmH67kQ/xgBcVOMTjQtYGZHQCm7dW6qfHWnDMYmom9Zsp/LLsuuZt5r4nDd/FfV3q5G4eKeCEAAPWHL0yBzcOhgec2rd6ij7bI6o2KHD5voO0T0K3nKvEMuOB7sU6EggD6BdjqNtdfUpssPdJrjtE12RWNVfJfV8bU+vBwgax6t7Sp/kh5WsVN2BQ8OAQxCA1jaXQvCD1JFRdxyRm9u/IJS+nOW7HplzM/n+YgQx4NcNeWPQwByhrmIYC19qT5d+kYukkES3hCAALwpVedEDZmjJUsX7Z8e+smALAnL6AMBCKAPkPs4xQtBM7xk/y2q1sc5MZXHBCAAj4vXJvVntQovm51Sc4O3NKwobQIQQNpEcxDPxGpPqOMrZqeHnstBOkghxwQggBwXR5KaMWYh0HpiTod3dfpnIckcGOs/AQjA/xp2XIEh9aIic2twpLR19jZ1eMCXi+V1SQAC6BKYr5cbMoc16W8ra58gUjttEL68iuh/X6nQIpHCx7l9LawwbwhACBDD3RLgvvDE7eyDHx0CGPwae71CCMBt+SAAt3wRXUgAAhACZIZDAG75IrqQAAQgBAgBuAWI6G4JQABu+WIH4JYvogsJQABCgNgBuAWI6G4JQABu+WIH4JYvogsJQABCgNgBuAWI6G4JQABu+WIH4JYvogsJQABCgNgBuAWI6G4JQABu+WIH4JYvogsJQABCgNgBuAWI6G4JQABu+WIH4JYvogsJQABCgNgBuAWI6G4JQABu+WIH4JYvogsJQABCgNgBuAWYJLqhuKaV/r419AOl9PNkoj3leOjAikN0aM8WtZgkBq4BARcEsANwQXUppvkpUbBVW/Pg7HRpO751xxloBBYQgAAE8E401BizXyn9dRWE99Qq6lDK4REOBFIlAAGkhdOYyAb6TjoaVutfUwfTCos4IOCSAASQAt2lb97V5sp6pfxMCuEQAgT6RgACEKJWlv4lCsLfO1BRrwtDYTgI9J0ABCBBrtR9NRV8PsnPckumwVgQcEUAAuiVbOvmnwo24t39XgFiXB4IQAA9VMES/XNdh7/VzTP/z91oV5fK8a9ZTb9K1p6vjDnTajqZSJd6SAFDCkPANInodSL9Mim1wxr6Hi0Gj6X1RjME0H0jvRDr8MKkr/lHxhfOtUF4E8Xx72ith7ufDiNA4K0EjDFHNAUPURBtrk0N75HwgQC6oWdMRKG6qFYp7+CGvbtiV0YmmrFkriPSAXc9HgeB7gmYpiV95/DBcPLVO9TR7scTQQDdUbu1Vi3dxA1pPetrFTxiFX2AuxaPg4CUgCF6Tqv4il52AxBAUvqG9umF8L3cL+yO3Nz4KFnzHaWDkaShcR0ISAks/b+J1ZfWpsvPdhMLAkhIy1q7qT5dvrPT5a1nfiL6d9z8CaHislQJLEnA0CW1meEXkgaGABKQUsYcUAvl0zs9+6+5wa4yK6Oniej9CULiEhBwQqD1cmDFwXBd0vcEIIAEZTCktsxXw+s6XTo60WztDq5PEA6XgIBTAoro9rlq6YYkk0AACSgZa9fOT5dbz+4n/Du1svg+bejHeLc/AUxc4p7A0j+m2Q/Wp4Z3c5NBAAwhQ3Z2vlp6V6dP/I1ONv6OrPp9DjYeB4F+ETBKPTA/FV7DzQcBMISUsQ/NzZQ/3e6y1if8dKmxHx/y4VoNj/eTgCFz1OryO7kPrEEAbFXsF2vV8l3tLhu7Ofpdq+yDbBhcAAJ9JmCturo+HW7tNC0EwO0AiC6bq5Yeb/v6fzy6X2vLbrX6XHtMBwJERv1NbSa8FgIQNIMy8TlzM8MvtgsxOt78EWn6iGAKDAUBVwSerlVLayEAAd5yMxzZe4uab/sSYHxx3mp9imAKDAUBJwSsiev1meFRCECAt6bDIaqoRtsdwMRiA//SKwCMoQ4JmGatOlSGAASIuR+mGJ1oWkH4FIeag8qoR42m71tFO0uN0sv7h+i1TvJKY3Ju/Rw/aQ5Zz+86f2l8jj/eBGQIcwC5BpQWkB1vaTeRvbUUlLbtq6gj7PUpX8Ctn+MnTSfr+V3nL43P8YcAPBVA65xXkfpKXZe2dPPNRNKG+tnxWd+AWc8v5cnlL40PAQgJcgBdF/CE6VvaHQfmNw5Uhv5LuDzxcG79HD9pAlnP7zp/aXyOP3YAvu0ADD1TCsNL91VUXdocaYzP+gbMen4pQy5/aXwIQEiQA+i6gG9J39LuUhBekpebv5Ubt36On7A8mc/vOn9pfI4/dgCe7ABaXwRpQ7ooD9v+45FBALJblOMni04EAQgJcgBdF3A5fUv2T+vV8h3C5aQ+nFs/x0+aUNbzu85fGp/jjx2AHzuA52s6/FCSd/tHJhsX6lhvjDWt1xSfnvcPKXENyt3g3A3iOj43f9aPc+uHALwQgP3DWrX8t51SbX0lWbQq/ktt7R9k3XTdzM81KATQDc23X8vxhQByLwBzsKTL7+z0IZ+lm39l9F1NdLGsXfo/mmtQCEBWE44vBJBzAXBfSLL0Tvx44wHS6jOyVslmNNegEICsLhxfCCDnArBkN9ar5Xvbpdl6za+savt9hbL2cT+aa1AIQFYDji8EkHMBGGXXzU+Vt7dLc2w8usdqu0HWJtmN5hoUApDVhuMLAeRcAEEzHNt/i6q1S/PUr0Qv6MCeI2uT7EZzDQoByGrD8YUAci6AQf8+Aq5BIQAIQEZAONp1g3LpZT0/l5/0cdfrcx1fun7X47n1YweQ9x1AtdSxRtJnSNcNyMXnGlS6PtfxufVl/Ti3fggAAsi0R7kGhQBk5eH4QgAQgKzDhKO5BoUAZIA5vhAABCDrMOForkEhABlgji8EAAHIOkw4mmtQCEAGmOMLAUAAsg4TjuYaFAKQAeb4QgAQgKzDhKO5BoUAZIA5vhDAgAuAawBZe/FfCcbF5/JzLQAuP+nj0vyl83N8IQAIQNRj0gbnGtR1fNHiEwyW5p9gio6XcHwhAAhA1GPSBuca1HV80eITDJbmn2AKCEAKqdN41w3K5S6dnxvPzc89Lm1wLj/X8bn1SR+X5i+dn+OLHQB2AKIekzY416Cu44sWn2CwNP8EU2AHIIWEHUDvBKUNDgH0zj7JSI4vdgDYASTpo7bXQACd8Un5iIpD+F0AKT/2hxVcF5gzODc/N14KiJufi8/l5zo+l5/0cWn+0vk5vtgBDPgOwHUDSRuca9C8x5fydT2e4wsBQACiN5HyfoNyN4A0f9c3sDQ+t34IAAKAAKR3WY7HQwDC4nAAXT+DDPr8rtfnOr6wvZwP59aPHQB2ANgBOL8Ns5sAAhCy5wBiB9C0EsSu+bqOL1l7P8Zy68cOADsA7AD6cSdmNAcEIATPAcQOADsAYYs5Hc71L3YAed8B6HCIKqrRLs3RicUGkS656iKugaQC9D2+K+5pxeX4QgA5F4BW4ZrZKTXXLk3XPw3GNRAEkNat6iYOVz8IIPcCsL84O1V+ql2aY5PR3dbajW7ah/8sOQTginw6cSEAIUcOoPQG4NJT1n5+brr8rXbXjVQaH1FG/YiL0+vjrtfve/xeufZrHMcXO4Cc7wCMpX+cny79dqc0R8ej+0jbz7poKq6BpAL0Pb4L5mnG5PhCAHkXgDFHdFheU6uoQ+1SfXfFrmyY6HFF9EtpNk8rFtdAEEDaxNONx9UPAsi5AFrpWbJ/VK+W7+6UaksCTRvfRdZem2YLcQ0EAaRJO/1YXP0gAC8EoF6u6+B9nY4Dl5fRek+ArN6gjF1PypwpPSLkGggCSP+mTTMiVz8IwAMBLKVo6c9r06XNaTZHGrE4AXANKM1BOj83Xppf1uM5/hCAJwIwZBa1Vh+rVco7sm6q4+fnbiCuAaVrkc7PjZfml/V4jj8E4IkAjm0C1MtKBxfXKmp/1o21PD93A3ENKF2HdH5uvDS/rMdz/CEAjwTQStUQPad1+Im8SIC7gbgGlN4g0vm58dL8sh7P8YcAPBPAmzsBc0UeXg5wNxDXgNIbRDo/N16aX9bjOf4QgIcCOLYTMIua9FRNh7cnOR1w1YjcDcQ1oDQv6fzceGl+WY/n+EMAngpgOe3W+wJkzWYVlLZ2+rCQq0bkbiCuAaV5Sefnxkvzy3o8xx8C8FwAy+kbY46Q0t8OyH6PSO2kOHxpZZlee6VCi0RK9K09nRBxNxDXgNIbRDo/N16aX9bjOf4QACeAjP8fP+sGwvz+ElDGNOZmhoY6rQACYOpbboYje29R8+0uGxtfnLdan+JvmyDzQSVgKK7NV4fHIABJhVX8ntrU8J52IUYnmv9BRB+VTIGxIOCCgCLaPlctrYMABHQV0WVz1dLjbQXg8F9xBWljKAiQMureuZmw45fF4CUA2yj2i7Vq+a52l41MRFcrsn/PhsEFINBnAtaqq+rT4TbsACTgld1Wmypf1S7EKRX7DmUa+zXpFZJpMBYE0iTQOhXivkeiNR92AAx1Q3Z2vlp6V6ejtFPHo/u1ttekWUDEAgERAaXuq02FG7gYEABHqPWpO2vXzk+Xn277MmBy4b0qVj8hrcME4XAJCDgmYJrK2PfPzQy/yE0EAXCEju2TvlmbKl3f6dKxieZtluhLScLhGhBwTODWWrV0U5I5IIAklMjMl3T59H0VdaTd5adtsisWV0XbSdOHEoXERSDggEBMtPMdOvzYKxW1kCQ8BJCEUusaa6+vTZe/2eny0cmFc4ylJzUFo0nD4joQSItA6/2qUJuLZyvDLyWNCQEkJUW0Vx8Jz529TR3uKIFK4wIT0eNaq46fwEo+La4EAZ5A6+YPLF06N13eyV/95hUQQDe0iG6pVUt/wQ0ZG18421DwiNJ0HnctHgcBKYHY0I5SGP9mN8/8y3NCAF3RN02l6MK5qaH/5IadUbHDh2w0pazZJP1mXm4uPF5UAqZJSt+++kBY2bNFLfZCAQLontrzpMMLk/7vfWs3YIPwJhPHn9Zar+x+OowAgbcSMGQOBybYqsJocy/P+sdHgwB66S5Lj9R2hVfSwypOOny0Yk8iE1+uFK0nay+IKT4zMGq11bqcNAauKx6B1r/0xtq+pih4SSu101p6gnTwWNInII4YBMARavO4JfXX9WrwJy6/bKPH1DAMBBITgAASo3r7hUsS+O/gC93sBATTYSgIpE4AApAj/Se7EH6m/jV1UB4KEUCgvwQggDR4W9pNgf1UHr6mO43lIEZxCEAAadXamMgG+htKhdNpvUGTVmqIAwLtCEAAqfeG+R9L6uvBkdI93KcGU58aAUGgSwIQQJfAkl6ujDlgg2CbJvPgrCptp4oyScfiOhDoFwEIoA+kW9/Oqin4N0v2B0rp5y1Fe8LG0E+Hh+ig6+/t78PyMIXHBCAAj4tXhNS5H7YoAgOXa4QAXNJFbDEBCECMsGMACMAtX0QXEoAAhACZ4RCAW76ILiQAAQgBQgBuASK6WwIQgFu+2AG45YvoQgIQgBAgdgBuASK6WwIQgFu+2AG45YvoQgIQgBAgdgBuASK6WwIQgFu+2AG45YvoQgIQgBAgdgBuASK6WwIQgFu+2AG45YvoQgIQgBAgdgBuASK6WwIQgFu+2AG45YvoQgIQgBAgdgBuASK6WwIQgFu+2AG45YvoQgIQgBCg+x3AYgM/feW2SMWNbpq16hB+OMVhA4h3ACPjCzWlgxGHOSJ0QQlYE9frM8P4qXWH9RcLYGyi+UNLtNZhjghdUAKKaPtctbSuoMvvy7LlApiM7rbWbuxLtpikUASUUffOzYToLYdVFwtg9OboSlL2HxzmiNAFJWCtuqo+HW4r6PL7smy5ACr2JBM1ZvHT132pV2EmMWSOqoXyGvzkmtuSiwXQSm9sPLrHarvBbaqIXigCSt1fmwo/W6g1Z7DYlASwcLYltYu0DjNYA6YcNALGRET2A7WZ4RcGbWl5W08qAmgtamSyeauy9OW8LRD5+EfAKrqtPlX6M/8y9y/j1ARw2ia7YmFV9JTSdJ5/GJBxXggYoudWHAzXvXqHOpqXnAY5j9QEcOy9gIWzY9JPaq3GBhka1uaGQOuDP4roYmz93fA9UdRUBdCaYLTSuMAY811NAT7B1b86ej9T6+anUF9ar5Sf8X4xHi0gdQEsSWBy4Rxjg0c00Yc9YoFUsyJg6MdE8RV45u9/AZwIoLWMpfcETo6mlDGbcDrQ/8J6MaMxkQ30nWUVTu6rqCNe5DxgSToTwDKn0fGF91AQ3mRsfJUmvWLA+GE5PRBofchHq+AhiqPNeNbvAWCKQ5wLYDnXkS/bk2kovlxbWk/anm9MfJbSajX+lTjFauYylGlaY1/TOniJjNppFD1Bi8Fj+IRfPorVNwHkY7nIAgRA4HgCEAD6AQQKTAACKHDxsXQQgADQAyBQYAIQQIGLj6WDAASAHgCBAhOAAApcfCwdBCAA9AAIFJgABFDg4mPpIAABoAdAoMAEIIACFx9LBwEIAD0AAgUmAAEUuPhYOghAAOgBECgwAQigwMXH0kEAAkAPgECBCfwfLFVDtRD2qgEAAAAASUVORK5CYII='),
(207, 'Graphviz', 'text/vnd.graphviz', '.gv', ''),
(208, 'Groove - Account', 'application/vnd.groo', '.gac', ''),
(209, 'Groove - Help', 'application/vnd.groo', '.ghf', ''),
(210, 'Groove - Identity Message', 'application/vnd.groo', '.gim', ''),
(211, 'Groove - Injector', 'application/vnd.groo', '.grv', ''),
(212, 'Groove - Tool Message', 'application/vnd.groo', '.gtm', ''),
(213, 'Groove - Tool Template', 'application/vnd.groo', '.tpl', ''),
(214, 'Groove - Vcard', 'application/vnd.groo', '.vcg', ''),
(215, 'H.261', 'video/h261', '.h261', ''),
(216, 'H.263', 'video/h263', '.h263', ''),
(217, 'H.264', 'video/h264', '.h264', ''),
(218, 'Hewlett Packard Instant Delivery', 'application/vnd.hp-h', '.hpid', ''),
(219, 'Hewlett-Packard''s WebPrintSmart', 'application/vnd.hp-h', '.hps', ''),
(220, 'Hierarchical Data Format', 'application/x-hdf', '.hdf', ''),
(221, 'Hit''n''Mix', 'audio/vnd.rip', '.rip', ''),
(222, 'Homebanking Computer Interface (HBCI)', 'application/vnd.hbci', '.hbci', ''),
(223, 'HP Indigo Digital Press - Job Layout Languate', 'application/vnd.hp-j', '.jlt', ''),
(224, 'HP Printer Command Language', 'application/vnd.hp-p', '.pcl', ''),
(225, 'HP-GL/2 and HP RTL', 'application/vnd.hp-h', '.hpgl', ''),
(226, 'HV Script', 'application/vnd.yama', '.hvs', ''),
(227, 'HV Voice Dictionary', 'application/vnd.yama', '.hvd', ''),
(228, 'HV Voice Parameter', 'application/vnd.yama', '.hvp', ''),
(229, 'Hydrostatix Master Suite', 'application/vnd.hydr', '.sfd-', ''),
(230, 'Hyperstudio', 'application/hyperstu', '.stk', ''),
(231, 'Hypertext Application Language', 'application/vnd.hal+', '.hal', ''),
(232, 'HyperText Markup Language (HTML)', 'text/html', '.html', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAXiUlEQVR4Xu2da4wk1XXHz7lV3TPLgojt7dk4Qg5ajIni8HCCdyUsxw4fYit8iGVHIIIjx4mBPGTsDQ5Y0exMT88gvBjxMIoSNrblBAmTIBElsh3bkR/5EGwgNusHyWIWmyTsemdqZgHvq2e6696oZ5fNsmz3qepbVfdW1X++dt17z/mdU7+q7lvTzYQ/EACB2hLg2maOxEEABAgCQBOAQI0JQAA1Lj5SBwEIAD0AAjUmAAHUuPhIHQQgAPQACNSYAARQ4+IjdRCAANADIFBjAhBAjYuP1EEAAkAPgECNCUAANS4+UgcBCAA9AAI1JgAB1Lj4SB0EIAD0AAjUmAAEUOPiI3UQgADQAyBQYwIQQI2Lj9RBAAJAD4BAjQlAADUuPlIHAQgAPQACNSYAAdS4+EgdBCAA9AAI1JgABFDj4iN1EEgsgFbbnE06voqZriRjLtM63sKKzyVSDWAcn0DUaYysQWtm7cNRp3nf+CtgJAgMJyAKYGq6ewFReGtM8XVKqbMAM1sCsgBW+4aC317uhF/MdmXMBgI0/HcBzm+bySOmP0ux/hgpFQJWPgRkAfSM1vqwCvntUbu5O58oMGtdCZzxDmBzu7ulp4NHAqJL6wqmqLyTCOBELPt0HG5buY33FRUb1qk+gVcJYPOO1Yv7zP+qiDdXP333GaYQwCDYJ0mFvx61+bD7yBFBFQi8QgCDK39fq0dx8hdX2pQCIDL6C9Ge5nvoYY6LixIrVZXASQEM3vP/TPe/jdv+YkudWgBEpInvW+mENxUbKVarIoGTAmjN9D5BRLdWMUmfcxpHAMfzMTdhe9DnypYjtnUBDLb6DPEefNpffNHGF4COsT1YfL2qtuIJAfR3GWWur1pyZchnfAEQYXuwDBX2O0YePOGn+2uLeMjHTaFsBHAiYmwPuildJVbl1kz/GiLzUCWyKWESGQhgkDW2B0tYex9C5qnZ/v3GmBt8CKaOMWQkAGwP1rF5MsiZp2Z6jxmirRnMhSnGIJCZALA9OAZ9DOHWzOoykXodULghkKUAjmeA7UE3lSznqgMBrOFfet0VL3sBYHvQXTXLtzK3ZnqmfGFXJ+LsBYDtwep0R/6ZQAD5Mx65Qh4COLEgtgcd17YMy0MAjquUowCwPei4tmVYHgJwXKWcBYDtQcf19X15CMBxhXIXALYHHVfY7+UhAMf1KUIA2B50XGSPl4cAHBenOAFge9Bxqb1cHgJwXJbiBIDtQcel9nJ5CMBxWYoUALYHHRfbw+UhAMdFcSAAbA86rrlPy0MAjqvhSADYHnRcd1+WhwAcV8KZALA96LjyfiwPATiug0sBYHvQcfE9WB4CcFwE9wLA9qDjFnC6PATgFD+RewFge9BxCzhdHgJwit8PAWB70HETOFweAnAIf7C0D3cApyDAl4s67oeil4cAiiZ+2nqeCQDbg477oejlIYCiifsuAGwPOu6IYpeHAIrl/arVvLsDOBkhvlzUcWsUsjwEUAjm4Yv4KwBsDzpujUKWhwAKwVxGAWB70HFrFLI8BFAI5nIKANuDjpujgOUhgAIgj1rC37cAr4ga24OO+ySv5SGAvMgmnLckAsD2YMJ6lu0wCMBxxUojAGwPOu6UfJaHAPLhmnjWMgngeFLYHkxc3BIcCAE4LlL5BIDtQcctk+nyEECmONNPVj4BYHswfZX9HQEBOK5NGQWA7UHHTZPh8hBAhjDHmarEAhiki+3BcYru0RgIwHExSi4AbA867h/b5SEAW4KW4yMVTlCb14ZNMzW9umqUalouk+twTXzfSie8KddFMHkuBCCAXLAmn7Shwtb+Ni8PG9Ga6e0hoouSz+jqSGwPuiJvsy4EYEMvg7HamK0r880nhgpgtv+XZMyfZLBUzlNgezBnwLlMDwHkgjX5pMaY65fnm58e+hZgdvUSY2g3keLks7o5Umt9WIX89qjd3O0mAqyalgAEkJZYxsczmweX5prXjZp2aqb/14bMjRkvndd0+3Qcblu5jffltQDmzY4ABJAdyzFn0ocmDjU3P383Hxs2wfltM3lE979MRO8Yc5Gih2F7sGjiY64HAYwJLtNhxnwwmm9+btSc5203G9Y2xvcaFX+oDG8HyOgvRHua76GHOc6UFSbLlAAEkCnOMScz9KMoCN9Mbe5LM7R2rL2FWd1oyFzJWv+iz1uE2B6Uqun+dQjAfQ3WIzCGbl6eb9zlSTgIoyYEIABPCq1JH2sYeuuB+YmnPAkJYdSAAATgV5GfCXrh2w7czpFfYSGaqhKAAPyr7JOKw3cvzvGSf6EhoqoRgAA8rKiOeW+o4vcuzk/8wMPwEFKFCEAAnhZTa90NlJpZUuG9o/5ZyNPwEVZJCEAAnhdKEz/LpHcGRxsPLt7JRzwPF+GVjAAEUJKCadJHFKkvsTFfJ+LvmSD8yUaiF59r0yoRm5KkgTA9IwABeFaQqoUjfeFJ1fItWz4QQNkqVrJ4IQC/CwYB+F2f0kcHAfhdQgjA7/qUPjoIwO8SQgB+16f00UEAfpcQAvC7PqWPDgLwu4QQgN/1KX10EIDfJYQA/K5P6aODAPwuIQTgd31KHx0E4HcJIQC/61P66CAAv0sIAfhdn9JHBwH4XUIIwO/6lD46CMDvEkIAften9NFBAH6XEALwuz6ljw4C8LuEEIDf9Sl9dBCA3yWEAPyuT+mjgwD8LmEpBKApjhSrbxhN32JWT5Pu723GEwc3HKbDe+/jVb8RIzoQ8JeAxwLQLxAFDyqjH1icbzyOb73xt4kQWXkJeCcArfUBZvVJDsJdUZsPlxctIgcB/wn4IwCt+yZQ99CxsLN8Bx/yHx0iBIHyE/BCAOvffKv01cvt5nfLjxQZgEB5CDgXABv6534Q/t7BNv+sPNgQKQhUg4BbATB/OuLgj5P8LHY1cCMLEPCLgDsBDE7+ueAGfLrvV0MgmnoRcCIAQ/RPyyr8nTRX/lbbnE06voqZriRjLtM63sKKzyVSjXqVDNnWi4DuaTIvMgU/VsS7DdHX1NHgS1n9SpQLATwTq/DypO/5N7e7W4wJb41NfJ0itbFexUe2IPBqAoNfiQp08CBRf+fSwuSzNoyKFYDWfQr5rVG7uVsK+vy2mTxi+rNk9M24yku08Ho9CeieYXX32RzOPtfm7jgMihUA0c6o0/i4FOjgqt/TwSMB0aXSsXgdBOpOwGj6Pgfx+6K5yb1pWRQnAE37VTd8k/TeZWp29ZLY8FcV8ea0yeB4EKgrgfX/lzHqXdF888k0DAoTgDFm+/J8855RwU1Ndy+IST2qFE+lSQLHggAIEK1LQNPbooXJZ5LyKEQArPVB7jbfMOrqf952s+HYOf3HFNHFSYPHcSAAAqcR0PTDRhhu29/mo0nYFCIATXzfSie8aeTVf6Z3pyG6OUnQOAYEQGA4AWa6a2mukehcKkYAxmxdmW8+MSzkTbPdN3HMT5FSIQoLAiBgSWDwj3VkfmV5YfJpaabcBaDJLK50Gq8f9cTf66b7n1XKfFAKFq+DAAgkJKDN30YLzd+Xjs5dAKzN55cWmr87LJDX3GrOVY21A0qpSSlYvA4CIJCMgNa6q3vNn39hJ780akTuAiAyH406zXuHBTG1o/9+w+aBZGnhKBAAgaQEjOHrlufDB50KgInevdRpfGVYELj9T1pOHAcCKQlo/ky0EH7IrQB0/MZRzyu3pnvfIUW/mjI1HA4CICATeCLqNLY6FUCzF27adzuvDH0LML26YpR6rZwLjgABEEhDwOh4eXlhsuVUAJEKJ6jNa8OCaM2sruGffdKUFceCQFICuhd1JppuBdBp8KgAWjM9kzSdMx0n/fCE7fxSbLbrS+Ol9fN+XeInxd+a6X2TiN6RU5z/FnUa77TprwTxW/VnTnknnlbKL/ddACkAqcGkTPOeP+/1pfil9fN+XaqPFP+mdu+drOkbecRpFP3GcrsxEMzQP9v4pfF55JXlnFJ9IABL2hJgqYGk8ZbhWQ/PIv6c7gLEq/8gedv4pfHWgHOeQOovCMCyABJgqYGk8ZbhWQ/PIv487gKSXP0hACKpvyAAy1NEApzFCWQZotXwrOLP+C4g0dUfAoAAxFtAq7ODZMBZnUC2cY47Pqv4s7wLSHr1hwDk/sQdwLhnxolxuAMYvctzKt6M7gISX/0hAAgAdwCWgsvqDmAQRhZ3AWmu/hAABOC9ACzPT/FDHukEznv90+e3vAtIdfWHACAACMDyQStJENJboNPH29wFpL36QwAQAATgmQBOnJTjPB2Y+uoPAUAAEICHAhjnLmCcqz8EAAFAAB4KYIy7gLGu/hAABAABeCqANHcB4179IQAIAALwVAAp7gLGvvpDABAABOCxAJLcBdhc/SEACAACcCyAwUk+6l92hecCRl79pbkhAAgAAnAsgMEJPupLO0bdBUhXf2luCAACgADcC8AkOZHP8K1B4tV/8EUj0oNI0pOQtuOlB6Vcvy7lh38GsqyQBFhqQMvlvX8U+ET+iU7mU1kklYYtf9vxtvXLe7yUHwRgWQEJMARw/Dsfk57QJ8qRWBi2/G3HW7ZP7sOl/CAAyxJIgCGAk1/6mvikTiMLW/624y3bJ/fhUn4QgGUJJMAQwP9/63PCE3vwtmboN/2e/qGhLX/b8Zbtk/twKT8IwLIEEmAI4BVf+y7eBQzKkWbb0Ja/7XjL9sl9uJRf5QWQO2FhAQjglb/7IN0FjMJ5pi1DqcEl/rbjXfeXtL6UHwQgEbR8XWpAy+nLsgtwappjP9p7poeGpAaX+NuOt61f3uOl/CCAnCsgNaDt8lKBfVx/nLuAYQ8M2eZvO962fnmPl/KDAHKugI8nYJYpSw02JP/UdwHDHhkec/2TCGzHZ8kyj7mk/CCAPKifMicEcObffkxzFzDqcWGpwSX+tuNzbh/r6aX8IABrxKMnkBrQdnmpwB6vn/guYNQ/DNnmbzvetn55j5fygwByroDHJ2AmmUsNNir/JHcB0r8M26w/AGA7PhOIOU4i5QcB5Ah/MDUEMPLn38W7AOlrxKUGl/jbjs+5faynl/KrvACkBrAlLAHG+iMFYIvf+gruun7WAIQJpPwgAMsKSIAhAAjAssWshkv9CQFY4XX/HlIqMAQ0WkCu+Vm2nzhcyg8CEBGOPkACjBMQdwCWLWY1XOpPCMAKL+4ApAbzXYCu47dsP3G4lB8EICLEHcAoAlKDQQCWDWY5XKoPBJAzYN9PAMv0rT+Fd72+dILkXT/b/KXxUn4QgETQcpsl7waSCoz18SHgqBaGACAAKwJlF5Dr+K3gJxgs5QcBJIBY5ffAlunjLYAtwJzHQwAefC9+njWWCoy3AHgLgLcAOZ6BOAEbPAqv7wJyXb8cW3N9aik/vAWwrIAE2PcTwDJ9scF8z991/Wz5S+Ol/CAAiSB2AUYSkBoMArBsMMvhUn0ggJwB+34CWKaPOwBbgDmPhwDwIeD6T3Pl9Sc1mO8CdB1/XnV5eV4pP9wBWFZAAuz7CWCZfuXvAGz52I637R+pPyEAywpJgG0LKIWH9e12ISR+Ev+8X7ftHyk/CMCyghJg2wJK4WF9CGBUj0j9AQFIZxh2AWq9C2DZHtbDbS8gEAA+BMSHgCNOQ+kEsT6DLSeAAHAFrvQV2PL8sP4QEgIo+RVSKqCtQaUGxfp278ElvtLrtvyl8dL6eb9u279SfvgMwLKCEmDbAkrhYX07AUn8JP55v27bP1J++QtAhRPU5rVhoFozq2tEqpE3SMwPAnUjwFqvLS1MTIzKO3cBNHvhpn2388qwIKamV1eMUq+tW3GQLwjkTUBTHK10JqecCoA4vjCam9w7/A6g9x9E9Gt5w8D8IFA3Akz0+FKnsc2pAJjo3UudxleGCmC2/xky5g/qVhzkCwJ5E2DmXUtz4Y1OBUBkPhp1mvcOfQuwo/9+w+aBvGFgfhCoHQHD10Tz4T+4FQCbh6K55rXDgnjNreZc1Vg7oJSarF2BkDAI5ERAkz4SHm1uXryTjzgVgCazuNJpvJ6Ihz6R1ppe+xwp/kBOLDAtCNSOABPfv9QJ/0hKPPddgEEA2pitK/PNJ4YFs2m6exErfopIBVLAeB0EQGA0gcH2H5P+pcWFDT+RWBUiAGL6VDTX+MioYKZmencZou1SwHgdBEBAIMC0EM01diThVIwASK80VPMN+9t8dFhQv9A2Z63p/uNM9OYkgeMYEACBMxL4TqTCK0Y9fHfqqIIEQETGfCSab35qVNFa090LtaJ/VxS0UFwQAIGUBDTtD014xU9v4/9OOrI4ARDtU0fDi6RPJVs71t6iDX1ZKR75BFPSBHEcCNSEwL5Y6d882J74zzT5FimAQVy3R53GX0gBTk13L9AUPMKKLpGOxesgAAL0RByH7zt4G/9vWhYFC0D3mOnypbmJ70uBnt82k0d0v02k/wz/LCTRwut1JKC17gZK7Vw6EN5Gu7g3DoOCBbAe4tOkwsujNh9OEvDgboCC8JbYxNcpUhuTjMExIFBtAvoQUfB3fRXc8UKb/8cmVxcCIDL0SLQnvJoe5jhp8Js/Zjbqs+LfMkxXsjGXajJbiMzPKVIj/90x6fw4DgR8JKBJrxLxi0T8rGLezUxfa74U/Mvzd/OxLOJ1I4DBpgDxXy13gj8d9YRgFgliDhAAgeEEnAlgENK6BP4r+HCaOwEUEwRAIDsCTgVwIo1/NN3wA8t38KHs0sJMIAACSQj4IIDBrcCPKDDXRO3m7iRB4xgQAIFsCPghgEEuWvdNoO5iDueT7hBkgwCzgEB9CfgjgJM10D81xJ8MjjZ2SU8N1rdsyBwEsiHgoQCOJ8ZaHzRB8JAi/cAiNx6nNutsUsYsIAACLxPwVgCnlmjw7aaKgm8aMt9iVk8b6u8N1yZemJygQ8+1abBPmuvPX6FdQKCqBEohgKrCr0Ne0g9T1IGBzzlCAD5XpwKxQQB+FxEC8Ls+pY8OAvC7hBCA3/UpfXQQgN8lhAD8rk/po4MA/C4hBOB3fUofHQTgdwkhAL/rU/roIAC/SwgB+F2f0kcHAfhdQgjA7/qUPjoIwO8SQgB+16f00UEAfpcQAvC7PqWPDgLwu4QQgN/1KX10EIDfJYQA/K5P6aODAPwuIQTgd31KHx0E4HcJIQC/61P66CAAv0sIAfhdn9JHBwH4XUJuzayu4ae3/C5SeaPTvagz0Sxv/NWPnDdNdyNWwabqp4oMiyZgdLy8vDCJn3ovGnyK9XhqpveYIdqaYgwOBYFEBJjo8aVOY1uig3GQEwI8Ndu/3xhzg5PVsWilCbDmv1laCNFbHleZWzv6VxObv/c4RoRWUgLG8LXL8+FDJQ2/FmFzq23O1v21RaXUWbXIGEkWQkCTPsbd5mb85FshuMdehAcjp6b7u4wy1489CwaCwOkEmD8bzYV/CDB+EzghgO4FhngPKRX6HS6iKwUBrftE5pejhclnShFvjYNcF8Dgb9NsbycbuqXGLJB6RgQM053Lc40/z2g6TJMjgZMCOG+72dDd2P82K7okx/UwdcUJaKIfbDgUbnv+bj5W8VQrkd5JARz/LKB7QUzqUaV4qhLZIYlCCQwe/GGiK3DrXyh2q8VeIYDBTK322mVa668qCvAElxXaeg0enPwUqnctt5vfrVfm5c72VQJYl8Bs943aBI8ooovLnR6iL4SAph8Sxe/Flb8Q2pkuckYBDFZY/0zgnP4ca70duwOZMq/OZFr3TaDuaXI4u7/NR6uTWH0yGSqAlxG0prsXUhB+XJv4WkVqQ33QINNhBAYP+SgOPk9x/xO46pe7T0QBvJzeplvMOTQRX6UMXUnKXKp1vIUVn4t/JS53A8jR657R5iWlgh+T5u9ppq/TavBFPOEnkyvDEYkFUIZkECMIgEA6AhBAOl44GgQqRQACqFQ5kQwIpCMAAaTjhaNBoFIEIIBKlRPJgEA6AhBAOl44GgQqRQACqFQ5kQwIpCMAAaTjhaNBoFIEIIBKlRPJgEA6AhBAOl44GgQqRQACqFQ5kQwIpCMAAaTjhaNBoFIEIIBKlRPJgEA6AhBAOl44GgQqRQACqFQ5kQwIpCMAAaTjhaNBoFIE/g/MPnDT2ZWM1AAAAABJRU5ErkJggg=='),
(233, 'IBM DB2 Rights Manager', 'application/vnd.ibm.', '.irm', ''),
(234, 'IBM Electronic Media Management System - Secure Container', 'application/vnd.ibm.', '.sc', ''),
(235, 'iCalendar', 'text/calendar', '.ics', ''),
(236, 'ICC profile', 'application/vnd.iccp', '.icc', ''),
(237, 'Icon Image', 'image/x-icon', '.ico', ''),
(238, 'igLoader', 'application/vnd.iglo', '.igl', ''),
(239, 'Image Exchange Format', 'image/ief', '.ief', ''),
(240, 'ImmerVision PURE Players', 'application/vnd.imme', '.ivp', ''),
(241, 'ImmerVision PURE Players', 'application/vnd.imme', '.ivu', ''),
(242, 'IMS Networks', 'application/reginfo+', '.rif', ''),
(243, 'In3D - 3DML', 'text/vnd.in3d.3dml', '.3dml', ''),
(244, 'In3D - 3DML', 'text/vnd.in3d.spot', '.spot', ''),
(245, 'Initial Graphics Exchange Specification (IGES)', 'model/iges', '.igs', ''),
(246, 'Interactive Geometry Software', 'application/vnd.inte', '.i2g', ''),
(247, 'Interactive Geometry Software Cinderella', 'application/vnd.cind', '.cdy', ''),
(248, 'Intercon FormNet', 'application/vnd.inte', '.xpw', ''),
(249, 'International Society for Advancement of Cytometry', 'application/vnd.isac', '.fcs', ''),
(250, 'Internet Protocol Flow Information Export', 'application/ipfix', '.ipfi', ''),
(251, 'Internet Public Key Infrastructure - Certificate', 'application/pkix-cer', '.cer', ''),
(252, 'Internet Public Key Infrastructure - Certificate Management Protocole', 'application/pkixcmp', '.pki', ''),
(253, 'Internet Public Key Infrastructure - Certificate Revocation Lists', 'application/pkix-crl', '.crl', ''),
(254, 'Internet Public Key Infrastructure - Certification Path', 'application/pkix-pki', '.pkip', ''),
(255, 'IOCOM Visimeet', 'application/vnd.inso', '.igm', ''),
(256, 'IP Unplugged Roaming Client', 'application/vnd.ipun', '.rcpr', ''),
(257, 'iRepository / Lucidoc Editor', 'application/vnd.irep', '.irp', ''),
(258, 'J2ME App Descriptor', 'text/vnd.sun.j2me.ap', '.jad', ''),
(259, 'Java Archive', 'application/java-arc', '.jar', ''),
(260, 'Java Bytecode File', 'application/java-vm', '.clas', ''),
(261, 'Java Network Launching Protocol', 'application/x-java-j', '.jnlp', ''),
(262, 'Java Serialized Object', 'application/java-ser', '.ser', '');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(263, 'Java Source File', 'text/x-java-source,j', '.java', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAActElEQVR4Xu2de5AlVX3Hf7/TfXtmdyEoO3cGLcrgAmJFBYw8FGNS7h/K449YaLAUU2oCiyQFumLAxzzu3BkCqwjCJlFWMRoqiCEhlZQCYqFJVUQePsDnAitowuLO3JlF3NfMvd3npHr24T6m+3Tffp3u/s6/c/r8fr/P9/T3dvevH0z4AwEQqC0Brm3lKBwEQIBgAFgEIFBjAjCAGouP0kEABoA1AAI1JgADqLH4KB0EYABYAyBQYwIwgBqLj9JBAAaANQACNSYAA6ix+CgdBGAAWAMgUGMCMIAai4/SQQAGgDUAAjUmAAOosfgoHQRgAFgDIFBjAjCAGouP0kEABoA1AAI1JgADqLH4KB0EYABYAyBQYwIwgBqLj9JBAAaANQACNSYAA6ix+CgdBGAAWAMgUGMCMIAai4/SQSCyATRb6iiS3gXMtJaUOl1Kbw0LPoZINICxfwKddiNUg+Z494pO29nYfwRsCQLBBLQGMDy6cCKRfY1H3sVCiJWAmS4BvQEsuoqsP51r219PNzJmAwEK/i7ACS01uEu5E+TJj5AQNmBlQ0BvAD0lpdwpbH5Tp+U8lk0WmLWuBJY9AhhpLazpSetui+i0uoLJq+4oBrAvl63Ss8+ev5a35pUb4lSfwBEGMDK2+BqX+ZuCeKT65RdfYQwD8JP9IQn7jzst3ll85sigCgQOMQD/l9+V4kHs/PlJG9MAiJT8Wmez8za6i738skSkqhI4YAD+Of9vpfsQDvvzlTq2ARCRJN4437avzDdTRKsigQMG0BzvXU9E11SxSJNr6scA9tajrkR70GRly5HbkgH4rT5FvBlX+/MXrX8DkB7ag/nrVbWI+wzA3aSEurRqxZWhnv4NgAjtwTIobHaO7N/hJ93uDG7yKUaoJAawL2O0B4uRrhJRuTnuvpNI3VmJakpYRAoG4FeN9mAJtTchZR6ecG9VSq0zIZk65pCSAaA9WMfFk0LNPDzee1gRnZXCXJiiDwKpGQDag33QxybcHF+cIxKrgaIYAmkawN4K0B4sRslyRvUNoItHeosTL30DQHuwODXLF5mb4z1VvrSrk3H6BoD2YHVWR/aVwACyZxwaIQsD2BcQ7cGCtS1DeBhAwSplaABoDxasbRnCwwAKViljA0B7sGB9TQ8PAyhYocwNAO3BghU2OzwMoGB98jAAtAcLFtng8DCAgsXJzwDQHixYaiPDwwAKliU/A0B7sGCpjQwPAyhYljwNAO3BgsU2MDwMoGBRCjAAtAcL1tyk8DCAgtUoyADQHixYd1PCwwAKVqIwA0B7sGDlzQgPAyhYhyINAO3BgsU3IDwMoGARijcAtAcLXgKFhocBFIqfqHgDQHuw4CVQaHgYQKH4zTAAtAcLXgQFhocBFAjfD23CEcBBCPBy0YLXQ97hYQB5Ez8snmEGgPZgwesh7/AwgLyJm24AaA8WvCLyDQ8DyJf3EdGMOwI4kCFeLlrw0sglPAwgF8zBQcw1ALQHC14auYSHAeSCuYwGgPZgwUsjl/AwgFwwl9MA0B4seHHkEB4GkAPksBDmngIckjXagwWvk6zCwwCyIhtx3pIYANqDEfUs2zAYQMGKlcYA0B4seKVkEx4GkA3XyLOWyQD2FoX2YGRxSzAQBlCwSOUzALQHC14yqYaHAaSKM/5k5TMAtAfjq2zuFjCAgrUpowGgPVjwokkxPAwgRZj9TFViA/DLRXuwH9EN2gYGULAYJTcAtAcLXj9Jw8MAkhJMuH1H2APU4m7QNMOji4tKCCdhmEw3l8Qb59v2lZkGweSZEIABZII1+qQNYTefa/Fc0BbN8d5mIjol+oxFjUR7sCjySeLCAJLQS2FbqdRZ81POo4EGMOH+PSn1VymEyngKtAczBpzJ9DCATLBGn1QpdenclPOFwFOAicVTlaLHiARHn7WYkVLKncLmN3VazmPFZICocQnAAOISS3k8s7pjdtK5OGza4XH3c4rUZSmHzmq6rdKzz56/lrdmFQDzpkcABpAeyz5nkjsGdjgjz97Ee4ImOKGlBndJ9z4i+pM+g+S9GdqDeRPvMx4MoE9wqW6m1Ps7U86XwuY8fr1a0V3l3ayEd0kZTgdIya91Njtvo7vYS5UVJkuVAAwgVZx9TqboyY5lv4pa7OpmaI51X8ssLlOk1rKUv29yixDtQZ2axf8fBlC8BksZKEVXzU01bjQkHaRREwIwAEOEliT3NBSduW1q4KeGpIQ0akAABmCWyE9ZPfuN267jjllpIZuqEoABmKfsDwXb585M8qx5qSGjqhGAARioqPR4iy28C2emBn5sYHpIqUIEYACGiimlXLCEGJ8V9s1hDwsZmj7SKgkBGIDhQkniXzDJDdbuxh0zN/Auw9NFeiUjAAMoiWCS5C5B4h5W6ltE/Liy7GdWEf3mly1aJGJVkjKQpmEEYACGCVK1dHQvPKlavWWrBwZQNsVKli8MwGzBYABm61P67GAAZksIAzBbn9JnBwMwW0IYgNn6lD47GIDZEsIAzNan9NnBAMyWEAZgtj6lzw4GYLaEMACz9Sl9djAAsyWEAZitT+mzgwGYLSEMwGx9Sp8dDMBsCWEAZutT+uxgAGZLCAMwW5/SZwcDMFtCGIDZ+pQ+OxiA2RLCAMzWp/TZwQDMlhAGYLY+pc8OBmC2hDAAs/UpfXYwALMlLIUBSPI6gsW3laTvMosnSLpbHG9g+4qdtHPLRl40GzGyAwFzCRhsAPJ5IusOoeTtM1ONR/DWG3MXETIrLwHjDEBKuY1ZfIote1OnxTvLixaZg4D5BMwxACldZYnP0B67PfdJ3mE+OmQIAuUnYIQBLL35VsiL5lrOD8qPFBWAQHkIFG4ArOg/Xcv+8+0t/m15sCFTEKgGgWINgPkLHbYuj/JZ7GrgRhUgYBaB4gzA3/knrXW4um/WgkA29SJQiAEoov+YE/Y74vzyN1vqKJLeBcy0lpQ6XUpvDQs+hkg06iUZqq0XAdmTpH7DZD0tiB9TRA+I3dY9aX0lqggDeMoT9hlRz/lHWgtrlLKv8ZR3sSCxql7io1oQOJKA/5UoS1p3ELkbZqcHf5GEUb4GIKVLNp/ZaTmP6ZI+oaUGdyl3gpS8Cr/yOlr4fz0JyJ5icdNRbE/8ssUL/TDI1wCINnTajY/qEvV/9XvSutsiOk03Fv8HgboTUJJ+xJb39s7k4Ja4LPIzAEnPiQX7Fbpzl+GJxVM9xfcL4pG4xWA8CNSVwNLzMkq8tTPl/DAOg9wMQCm1fm7K+UxYcsOjCyd6JB4UgofjFIGxIAACREsmIOmNnenBp6LyyMUAWMrtvOC8LOzX//j1asWeo92HBdFroiaPcSAAAocRkPSThm2f/VyLd0dhk4sBSOKN8237ytBf//HeDYroqihJYwwIgEAwAWa6cXayEWlfyscAlDprfsp5NCjloYmFV7DHPyUhbAgLAiCQkID/YB2pV89NDz6hmylzA5CkZubbjZeE3fG3etT9ohDq/bpk8X8QAIGIBKT6cmfaeZ9udOYGwFJ9ZXbaeXdQIi++Rh0jGt1tQohBXbL4PwiAQDQCUsoF2XOOe34DvxC2ReYGQKQ+1Gk7NwclMTzmvkexuj1aWRgFAiAQlYBSfPHclH1HoQbAROfOthvfCEoCh/9R5cQ4EIhJQPJtnWn7kmINQHonhd2v3BztfZ8E/WHM0jAcBEBAT+DRTrtxVqEG4PTsoa3X8XzgKcDo4rwS4lh9LRgBAiAQh4CS3tzc9GCzUAPoCHuAWtwNSqI5vtjFwz5xZMVYEIhKQPY67QGnWANoNzgsgeZ4T0Utx8Rxug9fpFef3MGSvyYFfVsxPT7AjaefI/ptmLku8Wop56VEv7eoemtY0umsaK0kdYEQ4qhUeLJ3cthDKKs/4T4lLHVSKrEOm0R6vGX+WvvkwB+X0YWTSVhPphPbUP6a4nTrM/MugC6B9HaQdGSOO0vm9Sl6kkhtGNjZ+MqzN/GeuPktN37kI2qVt7L3bkXiGkHqxGRzqis7bWdj0Byrx91bBKkrksUI2Jrpls5k44OBBjDWvZKYAztQkXIynn94Fbr1CQOItAqCB+kA92twUsrdLHh0TjQ2xnlzUqxy1qlG8zj3w1LKVv/3Ych7O+2B84NP8dzziNQ9sfKKOljQeZ1W476g4UNji/cyi3OjTnfwuPLwhwH0o29q22RkAE94Ql64vTXws9QSDZlo7yPY1t39HA1IknuOFs6xQS+kWHrIa1V3e/8Gs3ziGcctDX/d+tCtTxwB6AgmPMfq4wjg+07PfmtY5yRhystuPjKhhnvKvb+vl7Bk+EscXGtmRx7l4x+yIGAAWewtB82pAxzLABRtdlz7j/Le+feX45uAq7wHYx8J5HEufoSOGVx7KCt/GEDGe3kCwFENwH/RI0k6c3564OfFVUO0dDrg0cNxDtnzvRq/j07K3Ycy8w9bL7ofKJwCJNzbdICjGkCUNyYlTDXy5sNjvasV04bIG/gDU94hw2JnYTil5x8ATLc+YQCxVvmRg3WAIxmAos2dzfar6S72dOkMTXTPEJ5Y5yl6s8Xey5QQ4Td6SNlVQvyKib8lhdwU6fuL61Rj9XHez+OdCmRwSB4EI+1TjkrwXx6Wbn3CAHR7XC4XAfl9nbb95bBQfu/eXeX9nVBK+4x3aMqSbxvYZV2hu6dgaKx7CTN/PjqezC7KHZlC6hcdq8AfBhB9raY4Uuew+iMA+cIq4RwX9l53f+eXK91vEtEb0khdEf3P4A77LWEmsGQ4g0vvaYh0x2DGbbkDZacfpxr8g9aFbn3iCCDhHqUDrDMAluqfZ6ed94SlsXrC/cfEv/yHB1j6NqN9aVjc5kT3q6T4osiIUv9lXi5yukcaleK/DC7d+oQBRF7d/R1i6QyASF3SaTu3BaUxNNZ9HTN/L2Gay2wuFSl+Xdh75IfGu5cx8ecix0773HzZwGlfa6gQfxhA5KWa2kCdw+oMQLE6c27SCdzBhyfcW5VS61JL+KCJFPPn5ibty4PmHpnovl4q/m7U2FlcnT8idsrdhirxX04n3frEEUDU1R0wTgdYZwC69yU0x3v+02yBT7wlTP+JTrvxymADUMNSuTOxYqS8gx4cOwuDqRz/w8TSrU8YQKzVfeRgHWCdAejelzA8urioa/X1W4IkuTjfHgh8GetJV6iBF17sxvzoZNqH6AdVl8EpRvX4H7oadOsTBtDv3rNvOx1grQEU/L6EpPkfiS/di3SHzJ/BRcb064+3oLKOr5sfBhBPryNG6wDXzQDSb9PtRZ7VvEn1S7h8KOv4uvlhAAkV1AGumwEs4czgl5oomyOLpPolXD4wAN0OkhRw1tsnXUBJt09aXybxMzhXJ8rm2kIm9ccQJev4uvlxBBBDrOWG6gDrDC7p9gnTz+QXKIur9Vk9bFRF/gevCV19MICEe5AOcB0NYAlpiu3ATAwlpYu4CZdPJgYMA0iqSoztYQBBsFI8ZM/klGJv3kn1i7FUlh2adXzd/DgCSKigDnBtjwDSvGiXyUVFGMDSgZpugSbcPwp32KT567bP2gB08bP+f7/rI622XVrzBHHS6Zc1X938/fLfP6+uPhiATgHN/3WAdQLqtk+YXuLNdfmHBkjllzub9l/UHSQxwIQTJOIf4RQHBpBQIN0OrBNQt33C9BJvrss/NEAq5+4pXktYJtlK84cBJF7/2gl0C0i3A+m21yaQ8QBd/mHhU7l6n2I3Yblcq8zfr1dXH44AEu5AOsC6HUi3fcL0Em+uy18bIMEOnIqBJDyF09aX8YCk/HXrCwaQUEAdYJ2Auu0Tppd4c13++gAJDuFTOYUIz7Dq/HX1wQD0Kzh0hA6wbgfSbZ8wvcSb6/LXB0hwES+Vi4gwgDACMAD9CoYBJGDUbxuv3+3iplp1A9bVV3oD0BWY/Bcs2S+ILn7Z84+0w/X1S57gyCFSUnsHVZ2/rj4YQIzF0s9VZBiAf7sZ3dKZbHwwCHVzrHslMd986P8TXDuIoaluB9HpFyPUskOzjq+bHwaQUEEdYN0CSrp9wvRz+QXs62p+gu5BHCZV56+rDwYQZ7UsM1YHGAawD1qMHbovw+hTx6T69Rn2wGZZx9fNDwNIqKAOMAxgP+AYh/R9nTL0J2RS/fqL+rutso6vmx8GkFBBHWAYwH7AMS7q9XXRsD8hk+rXX1QYwAECuh1EB7jsApY9f50+B3Z/knuOFs6xQd9APH69WrFnVXc7CVJRxgkhAl9nHjUndAEq8Diw2TuQ4ua4K8MWpNn5EyU16ENqj/TLTqrTHjg/sGMw7p5HpO6Js5PXmb9ufZX/FEDYA9TibvCCWewSiUZaC+bQeWSv0x5wguY+oaUGd0l3T50X4CG1Rzq39w3A2RjEbPW4e4sgdUVaeup2kFQNcJmks46vm7/0BiDYHpmZ5NnABfMJ9ylhqZPSWjCHzKPoyc5U45RA82mp40i6v4YB7CUQ6eq+RaozObglLz11OwgMIOGekzVgweoNM5POQ0FpFvlxzeFW7xwl6TswgIMIaNqBYayaowsnk7D8byWm9pf1+tQlmnV83fylPwJgpS6fnXICP2Gd3ee1/Q9gqNd2Ws5jgeYz3r1cEf8DDOBgAuHtwFADWPaOQd0uFv5/3Q6CI4BkfDO/00wq+tf5qcafhS6cCfc2UuovEpZy6NG/5tPa/uDVY727BNM7YAAHEwhvB4axGhpbvJdZnJumjjCA8Z5KE+jhc2UNWEq5W9jOSKfFO4PqeGlLrexK934memNKtf73wA77vGdv4sALfM2WOkq63RkhxEoYwO8I6J7yC2K1v02YVvtvf5ys16duvWUdXzd/6U8BfMCK1Afm2s6tYbBHPqJWyVXeLUmPBBTxZwd3WFeF7fx+HkNj3XXMHJqTP04nUNkPQZfVRNMOXG6bZsrtPxjAXgIVMQB+Zk5YrwxrB+4X3L8mIFhcqpR6M7F8ua5FyFJ2lRDPEPMDLOWm2SnncZ2rU0s5Q9LbzKRerhtbSwPQtAOXY5Z2+w8GUCEDWCpF0cc6U43rdTtcHv9vjvc+RkR/GyVWHQ1A1w5c1gAyaudWnb+uvkocAfgLRpJcFIJfH3ZVPsoOmXRMs9U9XUr1kCAxEGUunUCVPAVYOvb0Tg7r9x/MLov2H44AqnYEsHQQwM+wsM7ptHhblJ0v7THDH1cjLnsPWpZaE3Xu2hqA5nPfhxhABu0/GEAFDWDvkQD9WAj7LXmbQLOljvOke59FdFrUnd8fV18DiN4OzKL9BwOoqAHsvRzgHwnIC/M6HWiOdV+rWPxblIt+h5tDXQ0gajswq/YfDKDCBnDgmgCJyY6wPx2lOxDnV/vA2JZymtK9iqVsKSECHwoKm7uuBrDEJEI7MKv2Hwyg4gawX2D/aICUvJ6txh1hNwvFMQD/Jh8lexcTiWv6+dU/OFatDSBCOzCr9h8MoCYGsF9o/45BYnGPReoBIn6cPPvplQ698MsWLRJxwN2Qik9o0cAuohex576cWJ3uKV5LSp6vu8MvqqHU2QCitANXZ9T+gwHkZQCFPq8fdTfEOBCoHgH/JrbZ6YHQdnTm9wE4PXto63U8H4R3eHRxXglxbPXwoyIQKJaAJK8z3x4cDssicwPQ3fDRHO99j4heVywqRAeB6hFgokdm242zCzUAJjp3tt34RlASzQwe1a2elKgIBOITYOZNs5P2ZYUaAJH6UKftHPbZp9+lNDzmvkexuj1+edgCBEAglIDid3am7H8p1gBY3dmZdN4VlMSLr1HHiEZ3W9rPeWNpgECdCUiSu+zdzsjMDbyrUAOQpGbm242XBLfaiJqj3S+R4PfWWTDUDgJpEmDiW2fb9gd0c2Z/EdC/P1+ps+annEeDkhkaXTiFBf+USFi6hPF/EACBcAJ++49JvnJmesUzOla5GIDu89B+ksPjvRsV0Xpdwvg/CICAhgDTdGeyMRaFUz4GQHK+IZyXPdfi3UFJ7Xtv3yNM9KooiWMMCIDAsgS+3xH2OVGff8nJAPxH9NQHO1POLWGi+S9+kIK+I8hqQlwQAIGYBCQ9Zyv7nF9fy7+KumV+BkC0Vey2T9FdlfQfrZWK7hOCQ+9gilogxoFATQhs9YR8y/bWwM/i1JunAfh5XddpNz6uS3B4dOFESdbdLOhU3Vj8HwRAgB71PPvt26/l/4vLImcDkD1mOmN2cuBHukT3fVizRSQ/rHtzr24u/B8EqkhASrlgCbFhdpt9LW3iXj815mwASyk+QcI+I+qz+f7RAFn21Z7yLhYkVvVTJLYBgWoRkDuIrH9yhfXJ51v8v0lqK8IA/Hd23d3ZbF9Ed7EXNfmlD3us9M5XTGtZqdMk+S/eVC+K+vbdqHEwDgRMIuC/7ZqIf0PEvxDMjzHTA84L1r26D9NEraEYA9j73r7PzrWtvw67QzBqERgHAiDQH4HCDMBPd8kEfm5dEedIoL8ysRUIgMByBAo1gH0J/btasN8790neAYlAAATyJWCCAfiHAk+Spd6Z12u880WMaCBgLgEzDMDnI6WrLHEjsz0VtUNgLlZkBgLlIGCOARzgJX+tiD9l7W5s0t01WA7EyBIEzCVgoAHshcVSbleWdacgefsMNx6hFktzMSIzECgnAWMN4GCc/ttNBVn/pUh9l1k8ocjdYncHnh8coB3h7/UvpyjIGgTyIlAKA8gLBuKkT0D34ZP0I2LGOARgAHFoYWxsAjCA2Mhy3QAGkCvu+gWDAZitOQzAbH1Knx0MwGwJYQBm61P67GAAZksIAzBbn9JnBwMwW0IYgNn6lD47GIDZEsIAzNan9NnBAMyWEAZgtj6lzw4GYLaEMACz9Sl9djAAsyWEAZitT+mzgwGYLSEMwGx9Sp8dDMBsCWEAZutT+uxgAGZLCAMwW5/SZwcDMFtCGIDZ+pQ+OxiA2RLCAMzWp/TZwQDMlpCb44tdfHrLbJHKm53sddoDTnnzr37mPDS60GFhDVW/VFSYNwElvbm56UF86j1v8DHi8fB472FFdFaMbTAUBCIRYKJHZtuNsyMNxqBCCPDwhHurUmpdIdERtNIEWPLnZ6dtrC2DVebmmHsRsfqqwTkitZISUIrfNTdl31nS9GuRNjdb6ijpdmeEECtrUTGKzIWAJLmHF5wRfPItF9x9B2F/y+FRd5MS6tK+Z8GGIHA4AeYvdibtvwQYswnsM4CFExXxZhLCNjtdZFcKAlK6ROoPOtODT5Ui3xonuWQA/t/QRG8DK7q6xixQekoEFNMNc5ONv0lpOkyTIYEDBnD8erViYZX7EAs6NcN4mLriBCTRj1fssM9+9ibeU/FSK1HeAQPYey1g4USPxINC8HAlqkMRuRLwb/xhonNw6J8r9kTBDjEAf6Zmq3u6lPJ+QRbu4EqEtl4b+zs/2eKtcy3nB/WqvNzVHmEASyYwsXCSVNbdgug15S4P2edCQNJPiLwL8cufC+1UgyxrAH6EpWsCR7uTLOV6dAdSZV6dyaR0lSU+47A98VyLd1ensPpUEmgA+xE0RxdOJsv+qFTeuwSJFfVBg0qDCPg3+Qi2vkKeez1+9cu9TrQGsL+8oavV0TTgXSAUrSWhTpPSW8OCj8GjxOVeAPrsZU9J9YIQ1tMk+XHJ9C1atL6OO/z05MowIrIBlKEY5AgCIBCPAAwgHi+MBoFKEYABVEpOFAMC8QjAAOLxwmgQqBQBGECl5EQxIBCPAAwgHi+MBoFKEYABVEpOFAMC8QjAAOLxwmgQqBQBGECl5EQxIBCPAAwgHi+MBoFKEYABVEpOFAMC8QjAAOLxwmgQqBQBGECl5EQxIBCPAAwgHi+MBoFKEYABVEpOFAMC8QjAAOLxwmgQqBSB/wf+uBoA8LPTuQAAAABJRU5ErkJggg=='),
(264, 'JavaScript', 'application/javascri', '.js', ''),
(265, 'JavaScript Object Notation (JSON)', 'application/json', '.json', ''),
(266, 'Joda Archive', 'application/vnd.joos', '.joda', ''),
(267, 'JPEG 2000 Compound Image File Format', 'video/jpm', '.jpm', ''),
(268, 'JPEG Image', 'image/jpeg', '.jpeg', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAbsElEQVR4Xu2df5AlVXXHz7nd783sLkiEebNoWYZfij9hEQSFmJT7h6hUKoYoBkliVHaVWKCIAdTZmTdv1rAoCriJyorGlCnEkJBKSiwhJSYpgwhRd4M/lmUFUsXizrw3GNxfM+9135PqWRYWnPduv3f79o/X3/m37z33nM85/e3TfXv6MeEPBECgtAS4tJEjcBAAAYIAoAhAoMQEIAAlTj5CBwEIAGoABEpMAAJQ4uQjdBCAAKAGQKDEBCAAJU4+QgcBCABqAARKTAACUOLkI3QQgACgBkCgxAQgACVOPkIHAQgAagAESkwAAlDi5CN0EIAAoAZAoMQEIAAlTj5CBwEIAGoABEpMAAJQ4uQjdBCAAKAGQKDEBCAAJU4+QgcBCABqAARKTAACUOLkI3QQgACgBkCgxAQgACVOPkIHgdQEoFaXI0iH5zHTWhJZo3V4Ais+ikhVkIbBCTQblZ45rE22L202qpsHXwEzh5mAcwEYn1g4kci/KqTwIqXUymGGmUVsZgFYDIS8P2g1/Duy8A9r5puAMwE4ri6j+ySYolB/lJTy842huN6ZBaAjWuu9yuc3NOvVrcWNFJ67IOBEAFbXF07oaO92j+hUF07D5jME4gjAU6N36dA/a/6TvAv8QOAQgcQFYPWGxVcHzP+miFcDs3sCfQhA5MyPSfm/26zzXveeYYUiEEhUAKIrf6DVPTj500t9nwJAJPqbze3Vt9FtHKbnJVbKK4HEBCC65/+1Du5F259uqvsWACLSxJvnG/5l6XqK1fJIIDEBqE12NhHRVXkMcph9GkQADvKQy7A9OMyVES+2RAQg2uoT4u142h8PepKjBhcAHWJ7MMlMFNNWQgIQbBEl64qJoNheDy4ARNgeLHbuk/DeWgCiN/x00J7FSz5JpKN/GzYC8NRq2B7sH/vQzLAXgMngnURy69AQKVggCQhAFDG2BwuW96TctRaA8angJhFZn5RDsNMfgYQEANuD/WEfmtH2AjDZ+YEQnTk0RAoWSGICgO3BgmU+GXetBaA2udgiUsck4w6s9EsgSQE4uDa2B/vNQZHHJyEAbfxLb3YlkLwAYHswu2ymv3ICAtCR9N3GiocIJC8A2B4sU3VBAAqebRcC8BQSbA8WvDbiuA8BiEMpx2McCkAUNbYHc5z7JFyDACRBMUMbjgUA24MZ5jaNpSEAaVB2uIZzAcD2oMPsZW8aApB9Dqw8SEMAsD1olaJcT4YA5Do9ZufSEwBsD5qzUbwREIDi5exZHqcnANgeLHipLOs+BKDgWU1TALA9WPBiWcZ9CEDBc5qBAGB7sOA1c7j7EICCJzMjAcD2YMHr5pD7EICCJzIzAcD2YMEr56D7EICCpzFLAcD2YMGLBwJQ/ARmLwDYHixyFaEDKHL2iCh7AcD2YJFLCAJQ5OzlRACwPVjcIoIAFDd3S57noQM4DCH+e7Bg9QQBKFjCnutuzgQA24MFqycIQMESlnsBwPZgoSoKAlCodP2ms7nrAJ52ER8XLUJpQQCKkKUePuZXALA9WITSggAUIUuFFABsDxahtCAARchSQQUA24P5Ly4IQP5z1NPD/N4CPMttbA/mtM4gADlNTFy3CiIA2B6Mm9CUx0EAUgae9HKFEQBsDyad+kTsQQASwZidkSIJwEFK2B7Mrlp+c2UIQJ6yMYAvxRMAbA8OkGZnUyAAztCmY7h4AoDtwXQqI94qEIB4nHI7qogCgO3B/JQTBCA/uRjIkwILQBQvtgcHynpykyAAybHMxFLBBQDbg5lUzTOLQgAyToDt8k3lj1Cd293sjE8sLopSVdt1XM7XxJvnG/5lLteA7eUJQAAKXhkV5dcer3OrWxi1yc52Ijo5/2FiezCLHEEAsqCe4Jpa5Mz5mer9XQVgKvgbEvmLBJd0ZArbg47A9jQLAciCeoJrisi61kz15q63AFOLp4jQViJlnesE3V7WlNZ6r/L5Dc16davrtWD/IAHroqhNdgQwsyPALLfMTVcv6uXB+GTwRSF5f3Ze9rXyLh36Z81/knf1NQuDByIAARgIW54m6T0je6qrH7ueD3Tz6ri6jO7TwbeJ6Pfy5HkPX7A9mFKiIAApgXa6jMh7mjPVr/Za40WXy4r2qvBGUeHFRbgdINHfbG6vvo1u49Apu5IbhwAMQwEI7Wh6/iupzoEpnNqG9mnM6v1Cspa1/u08bxFie9CUTfvjEAB7hrmwIEJXtGYqn82FM3CiMAQgAIVJVW9HNekDFaHX7p4Z+emQhIQwUiAAAUgBcopLPOR1/HN2X8PNFNfEUgUmAAEocPK6uP5jxf6bZ6d5bvhCQ0RJE4AAJE00B/Z0yDt9FZ4/OzPyQA7cgQs5JgAByHFybFzTWi94Sk3OKf/GXv8sZLMG5hafAASg+DnsGYEm/gWTvtbbX7ll9jreN+ThIrw+CUAA+gRW1OGa9D5F6lsscjcRbxPPf2QV0f89WqdFIsbr3EVNrKXfEABLgJjuloDpgyduVx9+6xCA4c9xoSOEALhNHwTALV9YtyQAAbAEaJgOAXDLF9YtCUAALAFCANwChHW3BCAAbvmiA3DLF9YtCUAALAGiA3ALENbdEoAAuOWLDsAtX1i3JAABsASIDsAtQFh3SwAC4JYvOgC3fGHdkgAEwBIgOgC3AGHdLQEIgFu+6ADc8oV1SwIQAEuA6ADcAoR1twQgAG75ogNwyxfWLQlAACwBogNwCxDW3RKAALjliw7ALV9YtyQAAbAEiA7ALcA41jWFTcXqu6Lp+8zqQdLBzmo48sSKvbR352ZejGMDY0DABQF0AC6oLtnUvyLyblGivzY7U7kPX91xBhqGLQhAACzgLTdVa72bWX2aPX9Ls857EzYPcyCQKAEIQFI4tQ7EUzfQAb/R+hTvScos7ICASwIQgAToLn15V+kLWvXqjxIwBxMgkBoBCIAlahb618Dz//SJOv/a0hSmg0DqBCAANsiZb26yd0mcn+W2WQZzQcAVAQjAoGSjk3/aW4+n+4MCxLw8EIAADJAFIfqXlvLf3s+V//lXyVGVavj7ouiNJHIqa328KDqSSFUGcAFTSkNAd4jo10TqEWLeKpq+Q4veHUk9aIYA9F9ID4XKPyPuPf/YxMLJ4vlXUxj+sVJqtP/lMAMEnk1Aa71fkfd18oJNzenRnTZ8IAD90NM6IJ9f26xXt5qmvbAuKwMdbBTSlxEpzzQex0GgfwK6I6RuGN3jTz12PR/ofz4RBKA/atc2G5WrTVOiq75i73ZheoVpLI6DgC0BTfSA4vD8QboBCEBc+poeVwv+S02/sDu2oX06if42K28srmmMAwFbAkv/byLq3OZM9cf92IIAxKQlIpe3Zqo39BoeXfmJ6Hs4+WNCxbBECSyJgKZzmhtHH4prGAIQgxRr/QQvVF/c6+q/+qOySq8M7ieil8cwiSEg4IRAdDuwYo9/VtxnAhCAGGnQxJvnG/5lvYbWJjtRd/ChGOYwBAScEmCiz8w1Kh+NswgEIAYlLXLm/Ew1urov+3dMffFlStNP8LQ/BkwMcU9g6R/T5JWt6dEdpsUgAAZCmmR2vlF5Qa83/mpT7b8j4T8zwcZxEEiLgGb+6vy0/x7TehAAAyHW8vW5jdV3dRsWveGnKu3deMnHVGo4niYBTfqAqOqxphfWIADGrMiHm43qjd2GjW8I/kRYvmY0gwEgkDIBEb6oNePf0mtZCICpAyB681yjcmfX+/+J4CtKibHVSjn3WA4EiDR/ubnRvxgCYFEMrMOT5jaO/qKbidpE54ek6DUWS2AqCLgicH+zUTkTAmCBt9rxx3Zdw/NdbwEmFudFqaMtlsBUEHBCQHTYam0crUEALPA2lT9CdW537QAmF9v4l14LwJjqkIDuNBsjVQiABWLTD1PUJjtiYT7zqbbxmeZbB1iX6rGLdJT4nRNCpjXMtFaLnKdIrbK23ZcBvYc1f1Mr+q4wbau0K4/sHqEne10c+jLfZbBtfZnyg4eAhiyZANomKIkisbFhG59pvo1vXbuuuhwhYeddWqurPE9OcLHG0zaFdhDJtRWvcuvjdd7vdK1ljNvWlyk/EAAIQM8aMBWgqcCcnjB1qdbC4COadV2RGklyrWgfnYk/0VKVzf18+SlJHyJbJv6m9Uz5gQBAAIorAE/lbnxD+9RAq9sT6waEdoSe/sMn6iM/M51gro9DAFwThgAUXgCiFI5/XFZrFdzFik6xKhlNP6r4/rmP17llZSehyRCAhEAOasbUQtkmaFC/kppnG59pflJ+xrETiUDA4T0DdwJCOyqef05eTn7cAsTJuuMxpgKHAFSsbyOTTGF0OxCy/KDfZwLRhzbFp9fmoe0/nIdtfZnq1zp5tg4mmXwXtkwAix6/bXym+S5yYrJZm+xcRUSbTOMOPy4kH2k1qtf3MyeNsbb1ZcoPBADPAJw+A7AtYNa6LUr9LxPfrZXeEuv3F+tSPboT/ryPW4EHm8p/VZyn/WNT7TNUqNaHitYqCl+c95fAIACWMm0CaFvglu5ZT7eNz3Z+3wFo/vLIPu9S0yevxibb65h4Szz78t5mo/q3vcZGn3wLVoV/rUT+PJ7NfIwy5QcdADqAXHcAy6VHiL43usd/Uy8RqNXlCK3bu81vDOo9FVU9ttdLPksn/8rgLkV0dj5O6/heQADis1p2pAkgOoDeDwGd8Vn6bUZ/Xa/01iY7/0BE7+g1xvTBl6Un8RPtr5Lid1uWUibTTfWLDgAdQOE6gIMp00LCp/f6Dv74hvYHhPkLvVIsJOtbjeqXuo2J7vlZuOv3IDM5q/tYFALQB6zlhpoAOrvCWfodd7ptfLbz4/q57K0A8xdb0/4l3WyMb+i8Xpju6bWGZjlrfrp6X1cbE8EWUdKz07CJwfVcU37QAaADKGgHsJS4B5uNysu6nrwfl9XiB7t7pdjr+OO7r+FmtzHHfCJ4SHlykusT1ZV9CIAlWRNAdAAZPQOIbgJIL843Rrr+4vJJl8rIk88PFnqVwLB/78FUv+gA0AEUtgNIRAAa2QmY5bUp1nQIQCxM3QeZAKIDyPAEEtrenKl0/Sm21VMyriWY7dkBQADszpCinwCm6CEAvb94lCkf5s83p/0Pdsvh6qn267Tw9yEA3QngFgC3AAW9BdDCwqfNzVS3dUvh2GT7/Uz8RQgABMB0oe96PNMr3MBex59oG5/t/PiePnukGLYAo9G1qfY3SPgCCAAEYNA6o6wKfGCH+5xoG5/t/D7dPTT8P1cp/9xH69z1Cf/B13fbs6ZXgTPyf8Cw+59mig+3ALgFKNAtgBYmb8tK5X2418kfpXRsQ/tiZu76ht+htJtOkKI/4zLFBwGAAORaAKKtPiXqEVJ8N2u9pdc9/9OprEv1GB3+TJGcaLpmmk4QCICBYNEBoUDstvFMJ5CJr4vj/XwQxOS/qb5N823jM61vsm/yDx0AOgCnHYCpQJM+3u8nwUwniOkENM23jc+0vsm+yT8IAARgaARgkI+Cmk4Q0wlomm86QU3HTeub5pv8gwBAAIZCAGp1OTbUwbc9olNNJ8Xhx00niOkENM3vx5flxprWN9k3+QcBgAAUXgBqG9qnCat/YpLjTSfEc4+bThDTCWia368/zx1vWt9k3+QfBAACUFgBeOq//T7CWtdFqZ6/gtstzaYTxHQCmuabTlDTcdP6pvkm/yAAEIDCCcDYlXKkGulcpFldOchVH7cAzxCAAEAAciwAwsfVaWQf0W8xBSdQKGtC5rVE+q2K1ArT1S/OcdMV0nQFNs2P40OvMab1TfZN/kEAMhYAU4JsC8C2QEzrZ+2/KT7T8az9d72+yT4EAALgtAMwCYjpBHV93HSCuPbf9fom+xAACAAEoEcNQAAMJ4hrQK6vACb7JgW1jd+1fdfxZe2/KT7T8az9d72+yT46AHQA6ADQAZh0svtx2yvg4CunM9OkoLbxu7ZvomS7vu18k3+uj2ftv+v1TfbRAaADQAeADmBwnbW9Ag6+cjozTQpqG79r+yZKvdcXrk0GupeNrP03xWc6nrX/rtc32UcHkHUHoPwRqnO7mxu1ycW2u9+g151mY6TrK7TH1WV0nw4ODLUAZMqfnH9yDgJgugRkLACK/dWz0zzXzQ2nP00ltKM5Uzm5q/jU5VjSwS+HWQAy5U8QAMvT0/10k4La3gIoltfPTlfv7RbJ+FRwk4isdxGp6cu64/XO2aLpv4ZbALLjH3F1XV8m+7gFyLgDYJFL5maqXb9dP7ahfToz/7cLASAlpzXr1a1dxWeyfYkQf36YBcDIv95+DWv+oRP+EABXWJOza1JQ2w5AC/3j/EzlHb08rk0FXyaR9yYXFZHp6h+tdcyGzm2K6e3DLACx+E8EN5OS9yXJ/5At1/Vlso8OIOMOQGu9X/nV1c067+3mygvrsrKtg7uY6JyEivA/Rvb4b3nseu76gK9WlyN00J5VSq0cagGIz/9OJvqdhPg/bcZ0gtpeYEz2IQAZC0C0vJB8oNWo3tTLleiHLvSq8HO2nYAQf2F0j3dFr5M/8mNsQ3s9M/f0KY172KRPuOXsxeEfiXBHwhtJ5OIkfTKdoBCAJGkPYMt1gg4KAD/SUt7Lem0HHnI9eiagWK0TkTcS6+NNW4SsdVuUeoSYv9PPd/XHdLg9zsc20uAzQNr6mtIX/3r7NSRqHWtZG4e/yRHX/Ez20QHkoANYckHoY82ZyiZTwaRxvDbZ+RgR/VWctUwFZnsFi+NDImNyxP/weGz5mfIDAciJACz9Ao7i1/V6Kp9IoRuM1OrtNVrLvYrUSJz1TAVmW8BxfEhiTF74PzcWW36m/EAAciIAh24FWHlnN+u8O4mi7tdGFt/V79dHl+OjW4Es+S8XGwTAZcZj2DYpqG2CnuuCJnpAKf9NaYtAVt/Vj5GCVIdkxb9bkLb1ZapfdAA56gAOuXLwSqTPT+t2IMvv6qd6dsdcLG3+vdyCAMRMmqthJgW1TVA3v5fuSUlNN5X/mTi7AwPFX5dqTQdXZPld/YH8TmFSKvxjxGFbX6b6RQeQww7gcJeiqxGJ3sRe5ZZeLwvFqKWnh0Qv+YjuXESkroqz1dfLtqnAbAu4n7hcjHXBvx8/bfmZ8gMByLkAHHIvemOQWH3LI/kOEW+j0H94ZZWefLROi0Qsy4dx2Hf1w+B4YlkTCq8l0W81veEXt0hNBWZbwHH9cD1uMP72XtnyM+UHAmASgIz/X9y+hGChrASil8DmNo703M6FABiqo9rxx3Zdw/Pdho1PLM6LUkeXtcgQd34JaAqb843R8V4eQgBM+ePwJc3p0Z3dhtUmO9G/6p5uMoPjIJA2ASa6b65ROQsCYEGeid4816jc2VUAHP6rqIXbmAoCxJq/NLfR7/kxGXQAxkKRDzcb1Ru7DRubDC5ikr83msEAEEiZgAhf2Jrxb0UHYAOe5dbmdPXCbiaOrsvzWLd3J/VrtTauYi4IHCIQ5zsT0Vh0AIaa0SSz843KC7pvtREdMxF8RSl5D8oPBHJDgPnm5rS/zuQPBMBEiIi0yJnzM9X7u94GTC28lEP+KSnlxzCHISDgmIDusJaXz20c/YVpIQiAidDBPulzzenKh3oNHZ/sXCdEV8QxhzEg4JjAtc1G5eo4a0AA4lAiPV9R1Rc/Xuf93Ya/6HJZsbgquI8UvSqWSQwCAQcEQqJtz1P+6x6t80Ic8xCAOJSiMSIfas5UP9dreG1q4SQtdI8irxbXLMaBQFIEoudVvtJnz9ZHH45rEwIQlxTRLrXfP3n2Ot7XUwSiL+oEdKdS3PMNrPjLYiQImAlEJ78ndO7cTHWbefQzIyAA/dAiuqbZqHzcNGV8YuFETd7trOgU01gcBwFbAqGmrRU//KN+rvyH1oQA9EVfd5jpjLnpkf8xTYt+WHOvBNMs+nLTl3tNtnAcBJYnoDvE6jNHPeHXd27mxUEoQQD6p/YgKf+MuP+bH3UD4vlX6zB8V1L/gtu/y5gxTAQ06X2e9m5hP9g0yFX/cBYQgEEqQ+j25nb/ArqNw7jTo49wkA7PY6a1JLImpPB4T/NRolTXn+eOaxvjhpdA9C+9oZInmbyHFfM2EbqblHdH3AuQiQwEwESoy/HoF3ZaDe+Dvd4QHNA0poFAagQgABaol0Tg596l/XQCFsthKggkTgACYI/0n2XBf3frU7zH3hQsgEC6BCAASfAW2kGevDOtz3gn4TJsgEBEAAKQVB1oHYinPsvszyT1gCYp12AHBLoRgAAkXhv6l0L8aW9/ZYvprcHEl4ZBEOiTAASgT2Bxh7PWT4jn3apIf22WK/dRnXXcuRgHAmkRgACkQDr6Oqsi79+F5PvM6kGhYKffHvnV6Ajt6f1d/xScwxKlJgABKHX68x+86Yct8h9Bvj2EAOQ7P6X3DgLgtgQgAG75wrolAQiAJUDDdAiAW76wbkkAAmAJEALgFiCsuyUAAXDLFx2AW76wbkkAAmAJEB2AW4Cw7pYABMAtX3QAbvnCuiUBCIAlQHQAbgHCulsCEAC3fNEBuOUL65YEIACWANEBuAUI624JQADc8kUH4JYvrFsSgABYAkQH4BYgrLslAAFwyxcdgFu+sG5JAAJgCRAdgFuAsO6WAATALV90AG75wrolAQiAJUD3HcBiGz995TZJ5bWuO83GCH44xWEBWHcAYxMLTVbemEMfYbqkBESHrdbGUfzUusP8WwvA+GTnB0J0pkMfYbqkBJjovrlG5ayShp9K2PYCMBXcJCLrU/EWi5SKAGv+0txGH7XlMOvWAlDbEFxALN9w6CNMl5SACF/YmvFvLWn4qYRtLwB1OUIH7Vn89HUq+SrNIpr0AV6orsZPrrlNubUARO6NTwRbRMk6t67CeqkIMH+lOe2/r1QxZxBsQgKwcKIQbyel/AxiwJLDRkDrgEhe0dw4+tCwhZa3eBIRgCiosanOtSx0Zd4ChD/FIyBM17WmK39ZPM+L53FiAvCiy2XFwqrgXlZ0SvEwwOO8ENBED6zY45/12PV8IC8+DbMfiQnAwWcBCyeGpO5RiseHGRpic0MgevGHic5G6++G73JWExWAaIFavb1Ga32XIg9vcKWXx8KvFJ385KtzW/XqjwofTIECSFwAlkRgauEkLd7tiujVBWIBV7MioOknROH5uPKnnwAnAhCFsfRM4MhgmrW+HLsD6Se2ECtqHYinbqiyP/V4nfcXwuchc9KZABziVJtYeAl5/tVawgsVqRVDxg/hDEAgeslHsfd1CoNNuOoPADDBKc4F4JCvY1fKkTQSnqeE1pKSU7UOT2DFR+FfiRPMZi5N6Y5oeVIp72HSvE0z3U2L3h14wy8fyUpNAPIRLrwAARA4nAAEAPUAAiUmAAEocfIROghAAFADIFBiAhCAEicfoYMABAA1AAIlJgABKHHyEToIQABQAyBQYgIQgBInH6GDAAQANQACJSYAAShx8hE6CEAAUAMgUGICEIASJx+hgwAEADUAAiUmAAEocfIROghAAFADIFBiAv8PV7TYxMBZFDAAAAAASUVORK5CYII='),
(269, 'JPGVideo', 'video/jpeg', '.jpgv', ''),
(270, 'Kahootz', 'application/vnd.kaho', '.ktz', ''),
(271, 'Karaoke on Chipnuts Chipsets', 'application/vnd.chip', '.mmd', ''),
(272, 'KDE KOffice Office Suite - Karbon', 'application/vnd.kde.', '.karb', ''),
(273, 'KDE KOffice Office Suite - KChart', 'application/vnd.kde.', '.chrt', ''),
(274, 'KDE KOffice Office Suite - Kformula', 'application/vnd.kde.', '.kfo', ''),
(275, 'KDE KOffice Office Suite - Kivio', 'application/vnd.kde.', '.flw', ''),
(276, 'KDE KOffice Office Suite - Kontour', 'application/vnd.kde.', '.kon', ''),
(277, 'KDE KOffice Office Suite - Kpresenter', 'application/vnd.kde.', '.kpr', ''),
(278, 'KDE KOffice Office Suite - Kspread', 'application/vnd.kde.', '.ksp', ''),
(279, 'KDE KOffice Office Suite - Kword', 'application/vnd.kde.', '.kwd', ''),
(280, 'Kenamea App', 'application/vnd.kena', '.htke', ''),
(281, 'Kidspiration', 'application/vnd.kids', '.kia', ''),
(282, 'Kinar Applications', 'application/vnd.kina', '.kne', ''),
(283, 'Kodak Storyshare', 'application/vnd.koda', '.sse', ''),
(284, 'Laser App Enterprise', 'application/vnd.las.', '.lasx', ''),
(285, 'LaTeX', 'application/x-latex', '.late', ''),
(286, 'Life Balance - Desktop Edition', 'application/vnd.llam', '.lbd', ''),
(287, 'Life Balance - Exchange Format', 'application/vnd.llam', '.lbe', ''),
(288, 'Lightspeed Audio Lab', 'application/vnd.jam', '.jam', ''),
(289, 'Lotus 1-2-3', 'application/vnd.lotu', '.123', ''),
(290, 'Lotus Approach', 'application/vnd.lotu', '.apr', ''),
(291, 'Lotus Freelance', 'application/vnd.lotu', '.pre', ''),
(292, 'Lotus Notes', 'application/vnd.lotu', '.nsf', ''),
(293, 'Lotus Organizer', 'application/vnd.lotu', '.org', ''),
(294, 'Lotus Screencam', 'application/vnd.lotu', '.scm', ''),
(295, 'Lotus Wordpro', 'application/vnd.lotu', '.lwp', ''),
(296, 'Lucent Voice', 'audio/vnd.lucent.voi', '.lvp', ''),
(297, 'M3U (Multimedia Playlist)', 'audio/x-mpegurl', '.m3u', ''),
(298, 'M4v', 'video/x-m4v', '.m4v', ''),
(299, 'Macintosh BinHex 4.0', 'application/mac-binh', '.hqx', ''),
(300, 'MacPorts Port System', 'application/vnd.macp', '.port', ''),
(301, 'MapGuide DBXML', 'application/vnd.osge', '.mgp', ''),
(302, 'MARC Formats', 'application/marc', '.mrc', ''),
(303, 'MARC21 XML Schema', 'application/marcxml+', '.mrcx', ''),
(304, 'Material Exchange Format', 'application/mxf', '.mxf', ''),
(305, 'Mathematica Notebook Player', 'application/vnd.wolf', '.nbp', ''),
(306, 'Mathematica Notebooks', 'application/mathemat', '.ma', ''),
(307, 'Mathematical Markup Language', 'application/mathml+x', '.math', ''),
(308, 'Mbox database files', 'application/mbox', '.mbox', ''),
(309, 'MedCalc', 'application/vnd.medc', '.mc1', ''),
(310, 'Media Server Control Markup Language', 'application/mediaser', '.mscm', ''),
(311, 'MediaRemote', 'application/vnd.medi', '.cdke', ''),
(312, 'Medical Waveform Encoding Format', 'application/vnd.mfer', '.mwf', ''),
(313, 'Melody Format for Mobile Platform', 'application/vnd.mfmp', '.mfm', ''),
(314, 'Mesh Data Type', 'model/mesh', '.msh', ''),
(315, 'Metadata Authority Description Schema', 'application/mads+xml', '.mads', ''),
(316, 'Metadata Encoding and Transmission Standard', 'application/mets+xml', '.mets', ''),
(317, 'Metadata Object Description Schema', 'application/mods+xml', '.mods', ''),
(318, 'Metalink', 'application/metalink', '.meta', ''),
(319, 'Micosoft PowerPoint - Macro-Enabled Template File', 'application/vnd.ms-p', '.potm', ''),
(320, 'Micosoft Word - Macro-Enabled Document', 'application/vnd.ms-w', '.docm', ''),
(321, 'Micosoft Word - Macro-Enabled Template', 'application/vnd.ms-w', '.dotm', ''),
(322, 'Micro CADAM Helix D&D', 'application/vnd.mcd', '.mcd', ''),
(323, 'Micrografx', 'application/vnd.micr', '.flo', ''),
(324, 'Micrografx iGrafx Professional', 'application/vnd.micr', '.igx', ''),
(325, 'MICROSEC e-Szign¢', 'application/vnd.eszi', '.es3', ''),
(326, 'Microsoft Access', 'application/x-msacce', '.mdb', ''),
(327, 'Microsoft Advanced Systems Format (ASF)', 'video/x-ms-asf', '.asf', ''),
(328, 'Microsoft Application', 'application/x-msdown', '.exe', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAYdElEQVR4Xu2da4xkxXXHT9W93TPLU4Ht2ThCDuZhwA6PxHg3suVE4QvEKIpFIhxMHkI2WHl48crEoGh2pqdnLMDGBryKEmOCHBEBERJRIgMmUUg+RNiGxCzGlhdYMB9YwsydwTH7mum+tyrq2R32Nd1Vt6vO7Vt9//N1qk6d8zun/vfcW7e7BeEPBECgsgREZSNH4CAAAgQBQBGAQIUJQAAqnHyEDgIQANQACFSYAASgwslH6CAAAUANgECFCUAAKpx8hA4CEADUAAhUmAAEoMLJR+ggAAFADYBAhQlAACqcfIQOAhAA1AAIVJgABKDCyUfoIAABQA2AQIUJQAAqnHyEDgIQANQACFSYAASgwslH6CAAAUANgECFCUAAKpx8hA4CEADUAAhUmAAEoMLJR+ggAAFADYBAhQlAACqcfIQOAoUJQKOpTyGVXS0EXUFaX6ZUdo6Q4nQiWUMaBieQtGp9c9iYan8uadV3DL4CZo4yAXYBmJhcPpcovjWj7Hop5UmjDHMYsZkFYCXVFP3uYit+fBj+Yc1yE2ATgLObeny/TqcpU7eQlHG5MYTrnVkAOloptU/G4mNJs74z3EjhOQcBFgHY1Fw+p6OixyKiSzmchs0jBGwE4PDoPSqLtyx9SewBPxBYI+BdADZtX7k4FeLfJIlNwMxPIIcAdJ15nmT8G0lT7OP3DCuEQMCrAHSv/KmSz2DzF5f6nAJApNW3k131T9CjIivOS6xUVgLeBKB7z/+OSr+Htr/YVOcWACJSJHYsteKtxXqK1cpIwJsANKY6dxDRrWUMcpR9GkQADvHQW3E8OMqVYRebFwHoHvVpErvwtN8Ous9RgwuAynA86DMTYdryJADpfVrqG8NEELbXgwsAEY4Hw869D++dBaD7hp9K2/N4ycdHOvLbcBGAw6vheDA/9pGZ4S4AU+knifQjI0MksEA8CEA3YhwPBpZ3X+46C8DEdPoNrfVNvhyCnXwEPAkAjgfzYR+Z0e4CMNX5vibaPDJEAgvEmwDgeDCwzPtx11kAGlMri0TyTD/uwEpeAj4F4NDaOB7Mm4OQx/sQgDY+0ju8EvAvADgeHF42i1/ZgwB0dPFuY8U1Av4FAMeDVaouCEDg2eYQgMNIcDwYeG3YuA8BsKFU4jGMAtCNGseDJc69D9cgAD4oDtEGswDgeHCIuS1iaQhAEZQZ12AXABwPMmZv+KYhAMPPgZMHRQgAjgedUlTqyRCAUqfH7FxxAoDjQXM2whsBAQgvZ8d4XJwA4Hgw8FJZ130IQOBZLVIAcDwYeLGs4z4EIPCcDkEAcDwYeM0c7T4EIPBkDkkAcDwYeN2suQ8BCDyRQxMAHA8GXjmH3IcABJ7GYQoAjgcDLx4IQPgJHL4A4Hgw5CpCBxBy9oho+AKA48GQSwgCEHL2SiIAOB4Mt4ggAOHmbtXzMnQARyHEpwcDqycIQGAJO97dkgkAjgcDqycIQGAJK70A4HgwqIqCAASVrhOdLV0H8K6L+HLREEoLAhBClvr4WF4BwPFgCKUFAQghS0EKAI4HQygtCEAIWQpUAHA8WP7iggCUP0d9PSzvLcAxbuN4sKR1BgEoaWJs3QpEAHA8aJvQgsdBAAoG7nu5YAQAx4O+U+/FHgTAC8bhGQlJAA5RwvHg8KrlxJUhAGXKxgC+hCcAOB4cIM1sUyAAbGiLMRyeAOB4sJjKsFsFAmDHqbSjQhQAHA+Wp5wgAOXJxUCeBCwA3XhxPDhQ1v1NggD4YzkUS4ELAI4Hh1I1RxaFAAw5Aa7LJzIeo6Zo97IzMbmyoqWsu67DOV+R2LHUirdyrgHb6xOAAAReGTUZN95sisVeYTSmOruI6ILyh4njwWHkCAIwDOoe11Rab16arT/XUwCm078mrf/M45JMpnA8yAS2r1kIwDCoe1xTa33j4mz9/p63ANMrl2hNO4mkc649ur2uKaXUPhmLjyXN+k7utWD/EAHnomhMdTRgDo+AEPqhhZn69f08mJhK/1aT/uzwvMy18h6VxVuWviT25JqFwQMRgAAMhK1Mk9Tesb31TW/cLQ728ursph7fr9LvENFvlsnzPr7geLCgREEACgLNuozWNySz9W/1W+OsbXpD++TsXi2zz4RwO0BafTvZVf8EPSoyVnYVNw4BGIUC0PRyEsUfpKZITeE0trd/VQj5WU36CqHUL5f5iBDHg6Zsuv8fAuDOsBQWtKYvLM7WvlYKZ+BEMAQgAMGkqr+jitTBmqYPvzU79uMRCQlhFEAAAlAA5AKXeCXqxB9963aRFLgmlgqYAAQg4OT1cP15KeKr5mfEwuiFhoh8E4AA+CZaAnsqE7tjmV0zPzv2YgncgQslJgABKHFyXFxTSi1HUk4tyPjefh8WclkDc8MnAAEIP4d9I1AkXhWk7owO1B6av0vsH/FwEV5OAhCAnMBCHa5I7ZcknxBaP00kXtBR/NOTif7v9SatEAm8zh1qYh39hgA4AsR0XgKmLzzhXX30rUMARj/HQUcIAeBNHwSAly+sOxKAADgCNEyHAPDyhXVHAhAAR4AQAF6AsM5LAALAyxcdAC9fWHckAAFwBIgOgBcgrPMSgADw8kUHwMsX1h0JQAAcAaID4AUI67wEIAC8fNEB8PKFdUcCEABHgOgAeAHCOi8BCAAvX3QAvHxh3ZEABMARIDoAXoCwzksAAsDLFx0AL19YdyQAAXAEiA6AFyCs8xKAAPDyRQfAyxfWHQlAABwBogPgBWhjXVGWSCH/Qyv6rhDyJVLp7no29vaGfbRv9w6xYmMDY0CAgwA6AA6qqzbVz4iih6RWD87P1p7Ft+6wgYZhBwIQAAd4601VSr0lhPyKiOL7kqbY59k8zIGAVwIQAF84lUp1JO+hg3Fr8ctiry+zsAMCnAQgAB7orn7zrlTXLjbrP/BgDiZAoDACEABH1ELTv6RR/EdvN8U7jqYwHQQKJwABcEEuxP2JiP7U5me5XZbBXBDgIgABGJRsd/PPRDfh6f6gADGvDAQgAANkQRP986KMfz/Plf8XbtWn1+rZ72hJv0VaXyqUep+WdCqRrA3gAqZUhoDqENE7RPKnJMROrejfaSV63NeDZghA/kJ6JZPx5bb3/Bsnly/QUXwbZdkfSCnH8y+HGSBwLAGl1AFJ0cMUpXckM+O7XfhAAPLQUyqlWHw4adZ3mqb9UlOflKp0TpPaSiQj03j8HwTyE1AdTfKe8b3x9Bt3i4P55xNBAPJRuzNp1W4zTele9aWIHtOCPmAai/+DgCsBRfSiFNk1g3QDEABb+orelMvx+02/sLtxe/tDpNV3hIw22prGOBBwJbD6eRMtr0xm68/nsQUBsKSltd62OFu/p9/w7pWfiP4Lm98SKoZ5JbAqAoo+msyNv2JrGAJgQUoo9bZYrr+339V/0y36ZHVS+hwRXWRhEkNAgIVA93Zgw954i+0zAQiARRoUiR1LrXhrv6GNqU63O7jZwhyGgAArAUH01YVW7RabRSAAFpSU1puXZuvdq/u6f2c2Vy6Uin6Ep/0WMDGEn8DqB9P0Bxdnxl82LQYBMBBSpOeXWrX39HvjrzHd/nvS4o9NsPF/ECiKgBLiW0sz8Q2m9SAABkJC6YcX5uqf6jWs+4afrLXfwks+plLD/4skoEgd1LL+i6YX1iAAxqzozyet+r29hk1sT/9QC/2g0QwGgEDBBLQW1y/Oxg/1WxYCYOoAiK5aaNWe6nn/P5k+IKU2tloF5x7LgQCREn+XzMWfgQA4FINQ2XkLc+Ov9jLRmOz8D0n6NYclMBUEuAg8l7RqmyEADnjrnXjjntvFUs9bgMmVJS3lGQ5LYCoIsBDQKltcnBtvQAAc8CYyHqOmaPfsAKZW2vhIrwNgTGUkoDpJa6wOAXBAbPphisZURzuYJ5N9F9s+5prjEx9PWvGTvdbauH3lSSHkVT58OdGGejJpjX28tzinv02kn+i3dvj8+5M1xYeHgIbKNAE0bxC3BPFsHHurFvH9JJHxJb2+HOXMyZWLJNEPScrYflWLkUqlKqaLl5pju9Yd3dRxI0tfJEEXQgB6E4AAQAD6ErAQACLSW5NWfUfPK/F0uoO0/guLbW09xPR6dmN7eysJ0fP4dm0hk8BbO8Q00I5/78VN8UEAIADOAtD9sNRyXD//naZ4ez1jpzX1GeNp+xVfD0t9rmfaIEz72tosBMAaFc9AU4FwJ4gnKnurtvH5uiLbeeav4zDl184fvlG2/Ht5YIoPHQA6AOcOYNWAp3tyi63k9ZmDaYNY+MM6BALAitds3FQg3Akye8g7Il987k/lzdH4PXUw5dfsD++IfPxP9MUUHzoAdAB+OoB3rfjdoMc6519gTBuEd3ubrUMAzIxYR5gKhDtBrvZZ4axv3GuL/u4STLcY3PkdAv9jljTFhw6g5B1AgALAcizI9ZDRtEHC5H+kqE3xQQAgAN4vUj6P6brO+bZ3dMCmDQIBMJRH6IBM1c9dINz2TfFx/d/vFdvfsd/x8Y4q/7U4TfGhA0AHwKMB/u7ZeZ4pHI7atEFCv8CZ4oMAQAB4BGDVqo+n9pynCmT8MBYEALcAfUXStUBMCu1qn3F3W5p22cA+BKS/m6PO3xQfOgB0AJYbeeBhg7Xw/m4h+jpu2iChC7ApPggABGDgnW0/Mf9DPL8PEXt7atogEADcAuAWwH6nrzsy7zFe3vEu7kEAXOgRUegKaQqfu0C47ZviK+r/+a7o+TuGQeMYdf6m+HALgFuAQfdOvnn29/S6yG8YMm2Q0C9wpvggABCAfBvZabTNU/3u0Vxx3zFo2iAQADwDwDMAp01//OT+x4L9lmpMmb/kM6+rEIC8xI4bH7pCmsLnLhBu+6b4hvD/vseCPf2x/JLPvPGMOn9TfLgFwC1A3j3jYXz/h3zrLWD7JZ95nTNtkNAvcKb4IAAlF4C8BZ13/DAK3HTMd3wMvr9U9Gj7pg2Sl2fe8dz8TfFBACAATj9skrfg18abjgWPtttg+FrxNfumDTJofLbzIAC2pIY0zlQgrgk02ecO29X/gf0zHQseNsz2wyKH7Y86f1N86ADQAQylAziEvf+xYHcE70+LmT8NOLDAWU7kFmAIgGUieg0zAXRNoMm+o/vG6a7+GxfoOwACwM3fVF/oANABDKcDwC3AauVBANwuIeyzTQrqmkCTfe4AXf0f1D88BDxEjpu/qb7QAaADKLwDwDHgkaKDAAx6CSlonklBXRPIbb8gTDmXwYtAOYENPNxUX+gASt4BuArMwJXDNxGvAvOxPcEyBMARtgmg6wbltu8YPsN0fBiIAWpPk6b6QgeADqDAesTHgQuEvboUBMCRuAkgOgBLwKZjvyOf9sMXglgitRlmql90AOgAbOrIeYzp2O/YT/vhK8GcgR82AAFwJGkCiA7ADNh07Hf8p/3yjjd70HsEd35dfPMx1xQfOgB0AD7qzGAj/xU9X8cweAimDeIq8IN75memKT4IAATAT6X1toIfBuEm3Mc+BMARvgmg6xWC275j+B6m46fBPEAc2ISpvtABoAMYuLjME22O/fQT/e24CIjZQ9MGcRV4swe8I0zxQQAgADwVaH/sd6HBgcFuISyjMm0QCIABZOiATHXCXSDc9k3xcf3f70O8/A8RbeMaVf5r8ZviQweADsB2r1iP832M59ve0YGYNkjoFzhTfBAACID1xrYf6P+K7bejOBKJaYNAAHALgF8Gst/53ZE89+z+nikcEw0EIF9yTxgdukKawucuEJN9k3/c/8+fX86n9j5OFY4lNnr888WHW4CS3wJwb3CT/XwC4H+DnuifX4GBAJgqALcAQ70FcEyP83RrAWBq0dcJwOstBgTAsUSsC8RxnWFNNxWIa/wm+8OKe21d2/i4HtKtH7+/h4yjwr9XnZjiwy0AbgH6ErARAM5juvWc87meaYOEIsAQAKZMmQrEZoP0c81knyksa7N28fm7Its65qvjGA3+vamZ4kMHgA7AtQPwek9uKwDk6ZmDaYNY+8M00E6AIQBM+M3fqcadILbALA2b4/P7VN7SrcPD3E8dIAD5iJ8w2lwgjgsMeXoi4zFqinYvNxpTK20iWRuym1geBE4gIJRqL8yNjfVDg1sAQ+HUO/HGPbeLpV7DJiZXlrSUZ6D+QKBsBBRlyVJrfAIC4JIZkZ2fzIzv7t0BdP6biD7ksgTmggAHAUH07EKrtgUC4EBXEF210Ko91VMAJtP7SepPOyyBqSDAQkAo8c2FufgmCIATXv35pFW/t5eJjVPp9YL0PzgtgckgwEBAa3Hd4mz8CATABa7QjyQz9et6mTijqU8Tqv2WJLnBZRnMBQGfBJRSB2Rc35Q0xT4IgANZRXp+qVV7D5Ho+TPaZ06mD0ipb3BYBlNBwC8BIe5PZuIbTUZxCmAiRERK681Ls/Xnet4GTC+/X2TixyRlbGEOQ0CAmYDqCKUvWpgbf9W0EATARKj7f0FfT2ZqN/cbOjHVuUsTfcHGHMaAADOBO5NW7TabNSAANpRILdVk/b1vNsWBXsPP2qY3rJycPkuSfsXKJAaBAAOBjOiF02T86683xbKNeQiADaXuGK1vTmbrX+83vDG9fJ7S9IykqGFrFuNAwBeB7vOqWKqPzDfHX7O1CQGwJUW0Rx6IL5i/S+zvKwLN9mUqpaekFH3fwLJfFiNBwEygu/kjTVcuzNZfMI8+MgICkIcW0e1Jq/ZXpikTk8vnKooeE5IuMY3F/0HAlUCmaGctzn4vz5V/bU0IQC76qiMEXb4wM/ZD07Szm3p8n05nhFbb8GEhEy38fzACqkNCfvX0t+Pm7h1iZRAbEID81F4iGV9uesFizWy3G9BRfJvKsk9JKU/KvxxmgMCxBBSp/ZGKHhJxescgV/2jrUEABqkuTY8lu+Jr6VGR2U5vNPUppLKrhaArSOvLMsreFylxupaybmsD46pHoPuR3kzqnwuKXpNCvKA1PU0yetz2AmQiBgEwEerxf03ibxZb0Z/3e0NwQNOYBgKFEYAAOKBeFYGfRJ/L0wk4LIepIOCdAATAHek/6eX4Txa/LPa6m4IFECiWAATAB29NL1OkP5k06zt9mIMNECiKAATAF2mlUh3JrwkRz/p6QOPLNdgBgV4EIADea0P9rybxlehA7T7TW4Pel4ZBEMhJAAKQE5jt8O6v1+goekSSenBe1J6lplC2czEOBIoiAAEogHT321klRf+pSX9XCPmSpnR33B772fgY7X29SSs4SiwgCVhiXQIQABRGqQmU/Yc7Sg3PwjkIgAUkDBkeAQgAL3sIAC9fWHckAAFwBGiYDgHg5QvrjgQgAI4AIQC8AGGdlwAEgJcvOgBevrDuSAAC4AgQHQAvQFjnJQAB4OWLDoCXL6w7EoAAOAJEB8ALENZ5CUAAePmiA+DlC+uOBCAAjgDRAfAChHVeAhAAXr7oAHj5wrojAQiAI0B0ALwAYZ2XAASAly86AF6+sO5IAALgCBAdAC9AWOclAAHg5YsOgJcvrDsSgAA4AuTvAFba+Okr3iRV17rqJK0x/HAKYwE4dwAbJ5cTIaONjD7CdEUJaJUtLs6N46fWGfPvLAATU53va6LNjD7CdEUJCKJnF1q1LRUNv5Cw3QVgOv2G1vqmQrzFIpUiIJT45sJcjNpizLqzADS2p9eS0P/I6CNMV5SA1uK6xdn4kYqGX0jY7gLQ1KeotD2Pn74uJF+VWUSROiiW65vwk2u8KXcWgK57E5PpfVrqG3ldhfVKERDigWQm/nSlYh5CsJ4EYPlcTWIXSRkPIQYsOWoElEqJ9AeSufFXRi20ssXjRQC6QW2c7twpNH2xbAHCn/AIaEF3Lc7U/jI8z8Pz2JsAnLVNb1g+Of2ekHRJeBjgcVkIKKIXN+yNt7xxtzhYFp9G2Q9vAnDoWcDyuRnJZ6QUE6MMDbHxEOi++COIPoLWn4fvela9CkB3gUazfZlS6l8lRXiDq7g8Br9Sd/NTLK9cbNZ/EHwwAQXgXQBWRWB6+Tylo8ck0cUBsYCrwyKg6EdE2TW48hefABYB6Iax+kzg1HRGKLUNpwPFJzaIFZVKdSTvqYt4+s2mOBCEzyPmJJsArHFqTC6fT1F8m9LZdZLkhhHjh3AGINB9yUeK6GHK0jtw1R8AoMcp7AKw5uvGL+pTaSy7Wmq6gqS+VKnsHCHF6fgoscdsltKU6milfy5l9Bop8YIS9DStRI/jDb9yJKswAShHuPACBEDgaAIQANQDCFSYAASgwslH6CAAAUANgECFCUAAKpx8hA4CEADUAAhUmAAEoMLJR+ggAAFADYBAhQlAACqcfIQOAhAA1AAIVJgABKDCyUfoIAABQA2AQIUJQAAqnHyEDgIQANQACFSYAASgwslH6CAAAUANgECFCfw/+tdqxLg7DDUAAAAASUVORK5CYII='),
(329, 'Microsoft Artgalry', 'application/vnd.ms-a', '.cil', ''),
(330, 'Microsoft Cabinet File', 'application/vnd.ms-c', '.cab', ''),
(331, 'Microsoft Class Server', 'application/vnd.ms-i', '.ims', ''),
(332, 'Microsoft ClickOnce', 'application/x-ms-app', '.appl', ''),
(333, 'Microsoft Clipboard Clip', 'application/x-msclip', '.clp', ''),
(334, 'Microsoft Document Imaging Format', 'image/vnd.ms-modi', '.mdi', ''),
(335, 'Microsoft Embedded OpenType', 'application/vnd.ms-f', '.eot', ''),
(336, 'Microsoft Excel', 'application/vnd.ms-e', '.xls', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAdoElEQVR4Xu2df5AlVXXHz73dr98sy48I82ZXy1JYQFSEhQisSkwM/gGBpGKRBEWSWBTu4o+AggSImR9v3qzhhyjglhFWQpEyBRgrpJISRBPRqiQokCAL/ljYBflDyM68N5hlfzDzXvc9qZ7dhd1lum+/d+/t1z3vO//Oveee8zmnv+92n/4hCH8gAAIDS0AMbOQIHARAgCAAKAIQGGACEIABTj5CBwEIAGoABAaYAARggJOP0EEAAoAaAIEBJgABGODkI3QQgACgBkBggAlAAAY4+QgdBCAAqAEQGGACEIABTj5CBwEIAGoABAaYAARggJOP0EEAAoAaAIEBJgABGODkI3QQgACgBkBggAlAAAY4+QgdBCAAqAEQGGACEIABTj5CBwEIAGoABAaYAARggJOP0EEAAoAaAIEBJgABGODkI3QQyE0AanU+lFR0nhB0FjGfolS0SkhxBJGsIA29E2g2Kqk5rI23L2s2gg29r4CZS5mAcwEYGZ07lsi/JqLoIinlIUsZZj9i0wvAfMjk/WGr4d/fD/+wZrEJOBOAo+s8tIvDCYrUVSSlX2wM5fVOLwAdVkrtlL54f7MePFHeSOG5CwJOBGBFfW5VR3n3eUSrXTgNm68RyCIAe0e/oCJ/zewXxAvgBwL7CFgXgBVj8yeFQvybJLECmN0T6EIAYmd+QtL/7WZd7HTvGVYoAwGrAhD/8odKPoyDP7/UdykARKy+3dwcfIi+JaL8vMRKRSVgTQDic/6XVfhjbPvzTXXXAkBEisSG2YZ/eb6eYrUiErAmALXxzvVEdE0Rg1zKPvUiAHt48OVoDy7lysgWmxUBiFt9TGIzrvZng25zVO8CoCK0B21mopy2LAlAuJElry0ngnJ73bsAEKE9WO7c2/DeWADiO/xU2J7GTT420tG9DRMB2Lsa2oPdY18yM8wFYDz8MBHfu2SIlCwQCwIQR4z2YMnybstdYwEYmQhvZ+Z1thyCne4IWBIAtAe7w75kRpsLwHjnESY6Y8kQKVkg1gQA7cGSZd6Ou8YCUBufbxHJo+y4AyvdErApAHvWRnuw2xyUebwNAWjjkd7+lYB9AUB7sH/ZzH9lCwLQ4fzdxor7CNgXALQHB6m6IAAlz7YLAdiLBO3BktdGFvchAFkoFXiMQwGIo0Z7sMC5t+EaBMAGxT7acCwAaA/2Mbd5LA0ByIOywzWcCwDagw6z13/TEID+58DIgzwEAO1BoxQVejIEoNDp0TuXnwCgPajPRvlGQADKl7MDPM5PANAeLHmpLOo+BKDkWc1TANAeLHmxLOI+BKDkOe2DAKA9WPKa2d99CEDJk9knAUB7sOR1s899CEDJE9k3AUB7sOSVs8d9CEDJ09hPAUB7sOTFAwEofwL7LwBoD5a5irADKHP2iKj/AoD2YJlLCAJQ5uwVRADQHixvEUEAypu7Bc+LsAPYDyGeHixZPUEASpawg90tmACgPViyeoIAlCxhhRcAtAdLVVEQgFKl6/XOFm4H8KqLeLloGUoLAlCGLKX4WFwBQHuwDKUFAShDlkopAGgPlqG0IABlyFJJBQDtweIXFwSg+DlK9bC4pwAHuI32YEHrDAJQ0MRkdaskAoD2YNaE5jwOApAzcNvLlUYA0B60nXor9iAAVjD2z0iZBGAPJbQH+1ctr18ZAlCkbPTgS/kEAO3BHtLsbAoEwBnafAyXTwDQHsynMrKtAgHIxqmwo8ooAGgPFqecIADFyUVPnpRYAOJ40R7sKev2JkEA7LHsi6WSCwDag32pmtcWhQD0OQGmyzelX6W6aCfZGRmdn2cpA9N1XM5XJDbMNvzLXa4B24sTgACUvDIq0q+9WBetpDBq453NRHRC8cNEe7AfOYIA9IO6xTUV8xmzU8FjiQIwEX6VmD9lcUlHptAedAQ21SwEoB/ULa7JzGtbU8EdiacAE/MnM9MTRNI41xbdXtSUUmqn9MX7m/XgCddrwf4eAsZFURvvMGD2j4AQfPfMZHBRmgcj4+FtTHxp/7zsauUXVOSvmf2CeKGrWRjcEwEIQE/YijRJ7ajuCFb86mbxSpJXR9d5aJcKHySi3ymS5ym+oD2YU6IgADmBdroM88XNqeCutDXefAUvay+PbmUZfbwMpwPE6tvNzcGH6FsicspuwI1DAJZCATA90/T8E6kuQl04tbH2qULIS5n4LKHUW4vcIkR7UJdN8/9DAMwZFsICM32uNVX5ciGcgROlIQABKE2q0h1VpF6pMJ2+bar6syUSEsLIgQAEIAfIOS6xxev4Z267TjRzXBNLlZgABKDEyUtw/SdS+OdMT4qZpRcaIrJNAAJgm2gB7KlIbPVldP70VPWpArgDFwpMAAJQ4OSYuKaUmvOkHJ+R/q1pDwuZrIG55ScAASh/DlMjUCSeFaRu8HZX7p6+Sexa4uEivC4JQAC6BFbW4YrULknyAcH8EJHYxJ7/y+VE//d8neaJBG7nLmtiDf2GABgCxHS3BHQvPHG7+tK3DgFY+jkudYQQALfpgwC45QvrhgQgAIYANdMhAG75wrohAQiAIUAIgFuAsO6WAATALV/sANzyhXVDAhAAQ4DYAbgFCOtuCUAA3PLFDsAtX1g3JAABMASIHYBbgLDulgAEwC1f7ADc8oV1QwIQAEOA2AG4BQjrbglAANzyxQ7ALV9YNyQAATAEiB2AW4Cw7pYABMAtX+wA3PKFdUMCEABDgNgBuAUI624JQADc8sUOwC1fWDckAAEwBIgdgFuAWawrippSyB+woh8JIZ8mFW4NoupLy3bSzq0bxHwWGxgDAi4IYAfgguqCTfVrIu9uyeob01OVR/HWHWegYdiAAATAAN5iU5VS24SQXxSev7FZFzstm4c5ELBKAAJgC6dSIXvyFnrFb7RuFDtsmYUdEHBJAAJgge7Cm3eluqBVDx63YA4mQCA3AhAAQ9SC6V9Dz/+zl+riZUNTmA4CuROAAJggF+KOpvA+meWz3CbLYC4IuCIAAeiVbHzwT3rrcHW/V4CYVwQCEIAessBE/9KS/h9388v/hmv4iEoQ/QFL+l1iXi2UOoYlHUYkKz24gCkDQ0B1iOhlIvlLEuIJVvR9mvfut3WhGQLQfSFtiaR/WtZz/uHRuRPY86+lKPqIlHKo++UwAwQOJKCU2i3Ju4e88Prm5NBWEz4QgG7oKRWSL05v1oMndNPeVOdDQhWuZ1KXE0lPNx7/B4HuCagOk7xlaIc/8aubxSvdzyeCAHRH7YZmo3Ktbkr8qy+Fdx8LeqduLP4PAqYEFNFTUkTn97IbgABkpa/oRTnnv033hd3hsfa7idWDQnrDWU1jHAiYElh43oTl2c2p4Cfd2IIAZKTFzFe0poJb0obHv/xE9J84+DNCxTCrBBZEQNGZzfVDW7IahgBkICWUeknMBW9J+/VfcRUvV4eEjxHROzKYxBAQcEIgPh1YtsNfk/WaAAQgQxoUiQ2zDf/ytKG18U68O/hMBnMYAgJOCQiiL800KldlWQQCkIGSYj5jdiqIf90X/TuqPv92qeinuNqfASaGuCew8GAan9iaHHpGtxgEQENIEU/PNipvTLvjrzbR/nti8ec62Pg/CORFQAlx1+ykf7FuPQiAhpBQfM/M+uCjScPiO/xkpb0NN/noSg3/z5OAIvUKy2Cl7oY1CIA2K/zZZiO4NWnYyFj4pyz4G1ozGAACORNgFhe1pvy705aFAOh2AETnzDQq3008/x8N75SStVutnHOP5UCASIm/a673Pw4BMCgGoaLjZtYPPZtkojba+R+S9JsGS2AqCLgi8FizUTkDAmCAN+j4wy9cJ2YTTwFG52dZyiMNlsBUEHBCgFXUaq0fqkEADPA2pV+lumgn7gDG59t4pNcAMKY6JKA6zUY1gAAYINZ9mKI23mED80Qkzm02/O8k2Rgem/+OEPIcszWSZqvvNBvVc5PFLfw9In4gbW33fNxE3q1VpdRO6Ylvs6IfeESbRFh5bluVtqf9OCysUedg5TwdwX5nVSToFCHoLMV8niS5vFsfehmvyw8uAmqo6gCaCwD9oin9k5NeLnLU6Pw7JNGTJKXfSwEkzlEqVD6dNFuvbl50TJ39WhQ+RYLePuACsIWIb6jIyj0v1sVuGzmo1flQjjofVUpe43m8yobNJBu6+oUA9F8AiIgvbzaCDYm/xBPhBmL+C5uForu9uTbWvpyESGx/7vNFV2AWBNJm2JltxX10wXK0Ne1voI0ifiuP/b86B7UovFIJVZckq/YXINLlBwJQAAGIHzaa84PjX66LlxZz5/A6HzkUtrfYuthocz1dgZVSAJie8Uidv22q+jMXB+XBNkfG2qtDJe9zsRvQ5QcCUAABiF2w9YucrWDt7Th0BVY6AVD0eMX3z36xLlrZWNoZNfJ5XqFk+D0h6WQ7FvdY0eUHAlAQASBL5+QZisfqNQddgZVMAJ6uSP+38j749+UsFoFQRA/b3Ano8gMBKIoALPhhflVeLwB2uw66AiuLAMQv2mSfTn+pXv25nqG7EfHpQCT4EVvXBHT5gQAUSgBiZ+weoAeGZ19gdAVWFgFg4itbjeBmd4d2dsu18c41RHR99hnJI3X5gQAUTgActQUdnWLoCqwkAvB0U/rvyvKdh6PG2qdLKdepkD4gveit+pvA4vf6y+dZiO9LpW6bmQo2aQ/sOgdHdqJf2DgV0OUHAlA8AXDSFnR1kVFXYKUQAOaLm1PBXWmlEPfuiTtfNX3vAwtx29DL3pW6V3YNj7fXChIbtWJhWL8QAEOALgrcZptu4aTCYZux9AKg1MvVXcHKtANy4eBX4feI6L2mB+Se+fyD5bJy7vN1MZdkL15TqfY20zsGdfmBABRQABYuB2reQ5j1Rp29BefsRiNdgbkQSDsH4R4rQvDdM5PBRam//qPtu0iKj9lcl4T42+ak/+nUdcc7/0hEf2Kyri4/EICCCoDFtqDVtt/BuHQFVnQBYOa1rangjqQyGJ5onyZYJL4P0uTgFEKtnpmsPplkY2Ss/QkW4msma+jyAwEoqgAs+GXjqr3LroL+RpOiC4Duha8jo+FGlrzW5CBMniu+2mz4ibd4j4x13suCHjZZGwJgQi/DnVTuC9zkALYhIOkAdQXmno9ZgivSr6Xd+HPUX4dbpMfHma2SMJtpc3OqkvgdifjGIPbDbSZr6/KDHUChdwALzvW2hXfU9ltqpwD9fN+DUKo9s76a+BDQcZdxdfsbwsQLhVmEAQKQhVLKGB3AfH7hur933+5FxGRAxeDTe5L7KQC9e519pi4/2AEUfwfQdRvPZdtvqe0AvI4/su060Uwqg9p4J/64xvHZD7lijYQAGOZDBzCfHUC3bcHudwy9YioKn179V4LXzE4GjybNHxkPb2PiS3u13+95uvxgB1CCHcCCi9nP6TnPNwzpCiwvgez1QGPida1G8PXEHcBY+1QS4vFe7fd7ni4/EICyCMAeFcjwDr+4NZffOwZ1BVZ0ASDB9zYngwvTymBkIrydmdf1+2DuZX1dfiAApRKA2Nn0tmBaOLVx/Us+uy0yXYEVXQDil31W/WBF2vv+3nwFL5s/tPMACfGBbvn0e7wuPxCA0glAelswMZyML/nstmB1BVZ0AViIl/mS5lRwZ1rsCyJweHQTcfRJIml83HTLudfxuvwYB1KKBPdKrxA3Ai3mfPpFvsVmdPfsQHZgugIrSX1saW7zT8zy8s8VY/MnKemtI+YPKlKrbL24Izvx7kbq8gMBKN8OQNsWPDgk2y8V3d++rsBKIgDERFe1GpUvdXd4ZRhd5+BNRIfPR51jBNFqyXQWS/59InlYhtnGQ3T5gQCUUAAWLgdqnhbcP6yag9eK77OvK7CyCED8GvAK0+l5vAn4TXU+pKM6HyYW15Kgtxkf5SkGdPmBAJRUALRtwb1xOfuwyF77ugIriwDsDWeL1/HPTLsxyOrBuo4rwyvCy1io9ZLkMqu2M+YHAlBWAVjwO70tGI9w+2mx8j8N+Lr0K3rci/xzchMBIlo5Nn9iRPI+F7sBnUBDACAARj88ugIr2Q5gH4stUqrzp+vVnxrB6WLyyr/iWuSFD9r+1LwuPxCAsgqA7s5AnAJ0cfi9fujCp8FIjrW2+V/J0h0wWmzv5AURqIT/ZfPZAwiAYWZ0APv1C4eLgIaJzT7d+sdB05ZeUZ9/V6joUVvXBHT1ix1ACXcAuqf9Dg4JbcDsR3vSyIXPg5O4nwU95El6kqjy3GEv0fatG8S8ufUDLYyMda5iQV+0YRcCYEhRB7A/OwDcCGSY1tymxy/9YCl/SST+XXJ0+/RU9Snt4uu4UlsZxh8mNX4MWVe/2AGUbweQ+oagxHBwK7D2uMtjAJP42qHSuzLtleCxH7Xx9iVEIvFlpVl9hQBkJZUwTgcw/x0AHgYyTGn/pzP/sLqzcm7atwhWXMXLw0Pa0/guQJ/TVSwBwOPAfS4Ha8sLErfPNPxPpBmsTbS/SSwuMFlUV784BSjLKYCu7ffaFh8vBDE5YvKcK/nUZj14ImnJ4fH2pYLEbSYuQQBM6BXoaUBd2+/Ap/3wSjDDtOcyPf5OYGvS/2TSYvguQC5pSF9Ep6B5XAPQtf0ObvN1O94EcxH4mPjf17lMzzSnKickCgC+C9DX9CwsXowC7/4XvbsdQ++ci8Gnd/+J1Pb02fIIE+tpc/FdAFdkLdotQIHjwyAW83mwqX5+FwAC4DCxtkz3XwDwaTBbuVzMTp+/C/B0s1F5e1J8KyZ4RHE4bRK/rn7RBdDQ1QF0ew0gS9uPH0gPwURA9KXXXz56/3QjdN8FGJ4IvyaYU9t1ujWS/h/fFNRq+J9KFoD2exSLH/VqP8spLASgqAKQve2X+AuyN7TeTiEyVl3ZBYCZL21NBRsTL8SNtVezEImtuoyYFh+GNqARvlwm96vA7V7E6/4iYla4/eKT1T/tOMXfbK4PPpI2zsXXgXAjkDYzxRjQjwK33cazbW//zPSDj83KiJ/y8+eCldM3iV1Jdh18F+A/qjv8s3ErsM1MOrLVnwK3/4ttd0fxGuz+8LGbbGZe25oKUh+8iUVg7vDoy6bXA4QQG4OXvc+mHfxxdMNj7Y8LIRI/WZaVgC4/uAZQvGsAbs7Z7V1TOICYrsDcXiTNehikj1Mknp2V3jupLto6iyNj7dVKyEsF8wdJqGOIZCVtzr7HgZnEQ0KqjWm3/r5qp87BUSr6uSQ+VueP7v+6/EAACicALq/a2+gqHAhMV2C6AnX9/6wCJJiumZmq3Ojanyz2a+Oda4jo+ixjdWN0+YEAFEoA7B+grw/PrsDoCkxXoK7/n1UAlFJznkdrZiarT7r2Kc1+vMOIBD9i64tDuvxAAIoiAI626IuEZ/UUQ1dg/TyY4rWzCkA8Nj4V8ELvzJm/EUY33/Qa88jneUUoooc9j1f1auPgebr8QAAKIgCuLtItHp69i4y6ArNVyL3a6UYA4jUiok1+6J+dtwjU6rwyUuGDHtHqXmNdbJ4uPxCAAgiAyzbdYuHZXE9XYDaLuRdb3QrAgghE4jlfqvNnpoJNvazZ7ZzaWPtUFvKfBPEx3c7VjdflBwJQAAEgsveLrCuIff+3tePQFVhWf1yN60UA9pwOqHlJcqIp/ZuzdAd68f+4y7i6/Q3hlUKpOksZ9GJDN0eXHwhA/wXA6jm5riBe/b+law66Asvsj6OBvQrAPnfi3YCU6gbhVe5u1sVOG24OX82HyWrnIiXk1S5+9ff3UZcfCEDfBcDuVfnuCtS866ArsO78sT/aVABe3TEptZuEfMAj/j6R2ESR/9whAW1/vk7zRIIX95zF0XWq7iL6DUHhKor4lEiIs4jUubY+/KEjpssPBEAnANKvpm0Ba+Pzbd3NILok4f8g4IKA7n0D8ZoQAA35oOMPv3CdmE0aNjI6P8tSHukigbAJAiYEFEXN2cbQSJoNCICOsIiOb04ObU0aVhvv/DcRvVtnBv8HgbwJCKJHZxqVNRAAA/KC6JyZRuW7iQIwGt5Bki8xWAJTQcAJAaHE12fW++sgAEZ4+bPNRnBrkonh8fAiQfwPRktgMgg4IMAsLmxN+fdCAEzgCr63ORlcmGTiyDofLlR7W15XdU1CwdzBIaCU2i39YIWudYlrAJqaUMTTs43KG5NbPURHjYZ3SskXD055IdLCExDijuakv1bnJwRARyi+K4z5jNmp4LHE04CJubeJSPyMpPQzmMMQEHBMQHWE4nfMrB96VrcQBEBHKP6/oK80JyufSRs6Mt65iYk+l8UcxoCAYwI3NBuVa7OsAQHIQonUbEUGb3mxLnYnDV94b9zy8FGS9K5MJjEIBBwQiJ9mPFz673m+LuaymIcAZKEUj2H+THMq+Era8NrE3HGK6WFJXi2rWYwDAVsE4utVvlTvm64PPZfVJgQgKymiF+Ru/4S0t8fGpmr19ikqpO9KKVLvwMq+LEaCgJ5AfPB7TGd3+wgzBEDPdv8R1zUblc/rpoyMzh2ryLtPSDpZNxb/BwFTApGiJyp+9Efd/PLvWxMC0BV91RGCTsvy3rij6zy0k8NJweoKPCzUFWQMzkxAdUjILx3xkl/fukHMZ56230AIQPfUnibpn6a7wWKf2Xg3wJ5/rYqij0opD+l+OcwAgQMJKFK7POXdLfzw+l5+9fe3BgHopbqY7mtu9i+gb4ko6/RanQ8lFZ0nBJ1FzKdEFB3jKXGEqzfBZPUL44pNIH6kN5K8XZD3nBRiEzM9RNK7P+sPkC46CICOUML/93zZ1ft02h2CPZrGNBDIjQAEwAD1ggj8wrusm52AwXKYCgLWCUAAzJH+M8/5H2vdKHaYm4IFEMiXAATABm+mZ8jjD2f67puN9WADBCwRgABYAklKhezJLwvhT9m6QGPLNdgBgSQCEADrtaH+l0l80dtd2ai7a9D60jAIAl0SgAB0CSzr8PjrO+x590pS35gWlUepLlTWuRgHAnkRgADkQDp+O6sk74dM/CMh5NNM4Va/Xf31UJV2pL9XPgfnsMRAE4AADHT6ix+87sMWxY+g2B5CAIqdn4H3DgLgtgQgAG75wrohAQiAIUDNdAiAW76wbkgAAmAIEALgFiCsuyUAAXDLFzsAt3xh3ZAABMAQIHYAbgHCulsCEAC3fLEDcMsX1g0JQAAMAWIH4BYgrLslAAFwyxc7ALd8Yd2QAATAECB2AG4BwrpbAhAAt3yxA3DLF9YNCUAADAFiB+AWIKy7JQABcMsXOwC3fGHdkAAEwBAgdgBuAcK6WwIQALd8sQNwyxfWDQlAAAwBut8BzLfx6Su3SRpc66rTbFSDwY3ffeTGO4Dh0bmmkN6we1exwqARYBW1WuuH8Kl1h4k3FoCR8c4jTHSGQx9hekAJCKJHZxqVNQMafi5hmwvARHg7M6/LxVssMlAEhBJfn1nvo7YcZt1YAGpj4QUk+JsOfYTpASXALC5sTfn3Dmj4uYRtLgB1PlSF7Wl8+jqXfA3MIorUK2IuWIFPrrlNubEAxO6NjIYbWfJat67C+kAREOLO5qR/yUDF3IdgLQnA3LFMYjNJ6fchBiy51AgoFRLxO5vrh7YstdCKFo8VAYiDGp7o3CCYri5agPCnfARY0E2tycpfls/z8nlsTQDefAUvm1se/lhIOrl8GOBxUQgooqeW7fDX/Opm8UpRfFrKflgTgD3XAuaOjUg+LKUYWcrQEJsbAvGNP4Lofdj6u+G7mFWrAhAvUKu3T1FKfU+Shzu48stj6VeKD37y5dmtevB46YMpUQDWBWBBBCbmjlPs3SeJTioRC7jaLwKKfkoUnY9f/vwT4EQA4jAWrgkcFk4Kpa5AdyD/xJZiRaVC9uQtgfAnXqyL3aXweYk56UwA9nGqjc4dT55/reLoQkly2RLjh3B6IBDf5COFdw9F4fX41e8BoMUpzgVgn6/DV/NhVI3Ok0xnkeTVSkWrhBRH4FFii9kspCnVYcXbpfSeIyU2KUEP0bx3P+7wK0aychOAYoQLL0AABPYnAAFAPYDAABOAAAxw8hE6CEAAUAMgMMAEIAADnHyEDgIQANQACAwwAQjAACcfoYMABAA1AAIDTAACMMDJR+ggAAFADYDAABOAAAxw8hE6CEAAUAMgMMAEIAADnHyEDgIQANQACAwwAQjAACcfoYMABAA1AAIDTOD/AZyXTOIpogvBAAAAAElFTkSuQmCC'),
(337, 'Microsoft Excel - Add-In File', 'application/vnd.ms-e', '.xlam', ''),
(338, 'Microsoft Excel - Binary Workbook', 'application/vnd.ms-e', '.xlsb', ''),
(339, 'Microsoft Excel - Macro-Enabled Template File', 'application/vnd.ms-e', '.xltm', ''),
(340, 'Microsoft Excel - Macro-Enabled Workbook', 'application/vnd.ms-e', '.xlsm', ''),
(341, 'Microsoft Html Help File', 'application/vnd.ms-h', '.chm', ''),
(342, 'Microsoft Information Card', 'application/x-mscard', '.crd', ''),
(343, 'Microsoft Learning Resource Module', 'application/vnd.ms-l', '.lrm', ''),
(344, 'Microsoft MediaView', 'application/x-msmedi', '.mvb', ''),
(345, 'Microsoft Money', 'application/x-msmone', '.mny', '');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(346, 'Microsoft Office - OOXML - Presentation', 'application/vnd.open', '.pptx', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wcUAxcFNUjWwgAAEm9JREFUeNrtnWtwXOV5x//vWe2uZBuBLGmbYNJAEm4fjAXh4jRtoTNJgKR0ms7gTCcpk1tdIDMxENqRbUmrtWXkumGImwGnnmRyYdpO+JDOtAHCh7TQdsAXILYpdBgckiY4mJVkmWBZ2iPvefthjzAXa/es9nbOnt/vk717tHv2Oe/zP//nfc55jwQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABtxZ2vE4M60z8ybxZ7r3dojgBBWUzDv2H9EWn3KknSys2FP3Mc3SiZqyRdItm0MQ5HoQYmtiTLHsPeobmNU2Od40QKzkRHo7+ge+U5K5NDczuM43zJvEtvDEegwTjGuadn88knp7cte4JowLvGRyM+tMe3nr3Dc+PpZHLKcRJnSH5ojscz6kh0PN7z18fP7/+WJR7QWAE4b5eVY91z+oYKLzsmMShD4oeh0kt0LX+++KuZZT2bmBeABgnAud+1OvHyifMcp+tV4zgXEN4wSYCWOU7H89P3dKrrzhkCAvUVgJ6hORUOv3FOsrPzJRl1EtoQioCTOL9vuPAfs/ctl/QQAYH6CcD0WKeMST9L8od9SsC5rneo8IC0jmBA/QSgd3huHNsfkYPuOLf1Ds3eSiSgdgFYf0TdgzMrHTmDhDNCB94kdvVsPnktkUAAamP3KiWTiR3M9keuFqA9CPUpAYwSXyKUkVQB2oMIwNI56yu/Ve/Q3Kc5+UdZAmgPIgBL5I37u2WM+RRhjLgI0B5EAGoYPlcRxnaYEqA9iAAszUNeTBjbZDDQHkQAqsbaNGFsowFBexABqNI6EsX2qgVoDyIAEHMVoD2IAEC8JYD2IAIA8RYB2oMIAMR9SoD2IAIA8R4ktAcRAIj5QKE9iABArGsB2oMIAMRcBWgPIgAQbwmgPYgAQLxFgPYgAgBxnxKgPYgAQLwHD+1BBABiPoBoDyIAEOtagPYgAgAxVwHagwgAxFsCaA8iABBvEaA9iABA3KcEaA8iABDvQUV7EAGAmA8s2oMIAMS6FqA9iABAzFWA9iACAPGWANqDCADEWwRoDyIAEPcpAdqDCADEe7DRHkQAIOYDjvYgAgCxrgVoDyIAEHMVoD2IAEC8JYD2IAIA8RYB2oMIADQWa72QTwnQHkQAYOkJdPvhdHmrbY6EfhDSHkQAYGmcfc55F5Z737Pew5EYiLQHEQConoS8K8/etUg77S+OSkVtl41Au432IAIAS8ibRMeNr99mzvzmg+/R1HjnLzxrvxORX0N7EAGA6k6cpvzs2Y0vaWos/WXPFn8WDQmgPYgAQFWs3HTyc4u++eiF6tl0QlNbO6/wPO97USgHaA8iAFDNQUx0/F2596fvWSHd/GtNjaW/4Gr+Iq9Y/I619miYW4R+e/CfaQ823HHVRv/IPDM2IcDzindOjXV+o+KG649Ju1dG5nf1DLlmeizFGEMAoCxWck+dfF+qu/uViY2GeAAlQNykPNnR9ewbs+pYyQw6IAAx1ABj+ld47ouzxXRH/zjGDBCA+ImA43xgeWr+N6emT7yPaAACEFMn0JHu/FXfcOFrRAMQgJjOCRjjfL1v2M2v3Dj7hYWX3/vvlAbwtmFSG3QBokHp+h/7L7Lzj3pex/7ZmYkXZ3eeNxv2/e4fmTcTW5KMMQQA4sjEliQ9TUoAAEAAAAABAAAEAAAQAABAAAAAAQAABAAAEAAAQAAAAAEAAAQAABAAAEAAABAAAIgjHYSg8VjPc2XMw9Y79bhR4nm3WHxh5tVXporfv8glOuVhQRAEIKppb72it7vo2AeOj3UdkqTzrdUvDetbVAPJjwBEK+1l56xXHJoa67pXkrT+yJvvkfyAALRz6nv2vsmx9F1ve3n3KkIDoYVJwPq4/aliYe7CUvJniQcgALHJ/aL304mtqb7UqrMOl17JERRAAGKR/J734OS29MeWbziho1+lvgfmAGKD53kPTo2lb9GNL2lm5woCAghAfEp+76cLya9HL6zqbzM5d7ukOUkflfQxogkB2CPpMUmd+WxqsJ4fzINBqk5+Ozm5NdW/4u43dOLrZ1VKdpPPpqyf9NdKWivJ1iPuEM9zjz929kh6Ip9NDS6MMQSgSfH35uY+aHrOenlyY+XQZXLuGkkHSHpooBgM5LOpg0v9ECYBq6r77b1TO7qDJv8BP/lF8kMDWBhTB/yxhgA02PvPTo2l7w7S58/k3IOSLiNo0CQu88ccAtCws3+xuLH0r1yQ5F/NWR+a7AZWL0UEEIBgp39v6p6unQFtP8kPrRSBqsoB2oCB0t/7VoXEN77lX0O0oMUisMaffD4UpDuAAwhi/625X5//xaLv+4E+QKQgJBwI2hpEACqf/ueObUu/oO9dUM4BbFepLQMQilHrj0kEoOZIyvw4wGbXUvdDyEqBaxGAelAs/ud5T1Y8ua8lUBAy1iIA9aj/HfM/r/yewf5DW5YBCEAlAZj3XqiwyRz2H0JaBswhADVy4uTPj1XY5KNRPDtwZGPB9ZU24DqACpz65ur5Cpssdkvvxnw2tb1e+9E/Whg2xiQkfUjSZ2s9O+Szqbq5lkzOXSvpBkmzkoL+5rrGp5Fkcu7BfDa1JsB290na0ABHWPGGn0zOHZQ0Xu08AA6gcXTW88MmRtNb89nUaD6b+pyfvB9R6bbQlpPPpvbks6lRSTvesm+DFdxGZ4SO5WWZUfdm/4KvcnG4U9KL9XRq1tohSYcCbPunS/kCBCCi+En3ET/Z9obB1i9cfJLPpva8RQwOtUXAjR4quonlZc/CWwqSdEsdv3X/xGh6W7mLejI512Ry7qikqxGA+ArBWklbQrZfC2IwIGmgDUJtE6ni42V/80ha+Wxqv6T76iHInud9McBmV6t0i6pBAOJNTuG9F+GQon+fhJH04cyoe1OAUuCuWksBa/WXjuO8EGDTnbV8DwLQPk7ASnrOFwIb0n2L/s1SRv9a+K0SDSwFrKRnJ0ZT326k9UcA2lQE/Mm4fQhU40qBdLf2NrAUMEU3EeQy3pqs/wK0AVtIJudeJ+m6Mpsk3RmNHt+Rmq9y8cc7JD3V4H2TpJ/4E37ViMBoJudmFeAilQD7aCqdJWtZMLNMKXBFZtS9SUY/Lvf5+WzqrkzO/bSk86s4//9JIlWcabT1RwDCwXUqv8aYTS3XpkzOXajxR4MM6nw2tSeTc/eotnsUKu2bJGX9fdvji86+Svvm1885SeOZnDteo6uoVIvbTM6thwC8e1HXUimQlHRqsT/qz85LsusCOjIr6WB+NPVvAeKXrdX6UwJEpeI8zYg/mFdXmoTyeaKJVnutLwKrA0yQLZQpUT0OgUuBiVxyoRTYFeBYmHw2dXmzrD8CEF0xOFhpEspPtEE1/x6Fg1UIVDscjysCdgVul/R/5T/NXBMwbjvr+SMoAaKHTXfrEUmfCOn+Haxkzf2BPqjglw2HWQZqLQWspEP5keS+Zlp/HEC0zzwfD7jtA63YQf/egLJlgKSuNhLkWkoB418s1VTrjwBEnEpJtjD2WrR7N8RMkK/IjLqfCFgKvPaOlwdaYf0pAaLPDQrJzUAgyeixSqVPf9aVpJsk7X/T+le+y68h1h8HAFDnUiCTc58tXwqklM+mnpb03VZbfwQAoP6lwOUBS4EvSvqjVlp/SoDo8xNCEM1SwBeBx8ta/ztekXoy442y/jiAiBPwEtzrW7R7cRaniqVAJTLbrNSTuVqlVqlBAOBtA0xSLqB9bMVy5XsqiZO/77NxLwUWFffNRpL+oRk7SwkQPX4e8ktp7wjgXgI/uabdS4Ey1r8pt07jAKLFM56n6/z7zVXhLNvM5xUsfM8aBbjxJeA1DLErBTJbClJP5qpmWH8EIBykg9pqa+1IPpu6cjKXOpIfCfRntT6uLF3Ftlv8s91zAe8GvDYGx7ZUCuTcPwxaCvjH9QfN3ElKgBaSz6Y2StpYz8/MbClI1lxda/1fzb4tDPAg997Hwv6/nb8P2O9XJud+Q9LFzdw5HEC7iUrpLPL9JgtZ4FIjk3MPKF4PJvmrQHHZZqU6LJKCA4g5mZx7sNlnkSpcwmVqh3UBA84BqLRSc6Dl2fyZ/0F/JaZrEABYavKvVsieVegn/2pJB2J0OPYtsVuzQU28x4MSIPpJb8Kc/D5ZlRYLiRMbliocauLCqQhAxBNfVh9fWCoshGf+tf7ahCMxOjTWT+Alrczc7JWdEYAoJr1/Vs3kXCujx/z/tzT5+0cL5i37uT2Tc59SaWXiaxSvx6fv85/hWOsZfEMzdpY5gPAn/Xadnh2efUsLreUz6ZlR9xYZfWCRfTMxPWQbAh5XU2FJ8b2ZnLtL0q2NjCUC0NrkHtfpp+iWs5RnGgAmBPu2GHFM/sCz/pmc+6KkmzM597kKInB7Jud+Xg1cPg0BaC2FECdTgcNTvfUPUMJlJV2oAIun9o+6RtIfSHqaOQCA6Fv/t67uY/2LohZlYjRl89nUM5KeaVTJhwAA1G79g87673yHs1uTyblrAqwgdGWjnCAlAEDzrP+ZVvc5EPA5CgNqwIVUOACA5lr/dzmISqWAf23AQZUuprIIAEB0rf87qaYUGKh3KUAJANAa6x+KUgAHANAa6x+KUgABAGiN9Q9FKUAJANBa69/SUgAHANBa69/SUgABAGit9W9pKUAJABAO69+SUgAHABAO69+SUgABAAiH9W9JKUAJABAu67/UUmBQUtXPW8ABAITL+p+pFPjvSqWAlrhoCALQOObqtE2Y97/duSOfTe1tsvU/Uynw0UzOHWjE8apZsfpH5tv6KS8TW5KV7FcUf387rNkX9Dc047eGNp6VygccQDwxMfoNhngiAACAAAAAAgAACAAAAgAACAAAIABQPXsIAUR1bCIAFejeeCJTYZPHiBKElMcQgBrpSCQ/WGGTToXgSb0A78D6YxMBqK1G8i7v+fbi+Z3PpgYV30dhQ3gx/thEAGoLY+L3p79cMb+ZB4DI1f8IQCAjZT8VYKsnKAMgZPb/CQSgHgbAcbp7N81eLD1EGQBtZf8RgOAisF5at+j7l/zISqXFGQHCwEClZcTeHNu1flO7rwdQqgK8U5Nb08lK2/mLOF6GG4AWWv9D/hqBgcABBFFJ43Ss3Dy7vtJ2fuCfYz4AWpT8z1WT/AhANYFKJO4Nsl0+m1qDCECLkn9N1eOa2AV0ATIr+oYLo1WIwCGiBk3i0FKSnzmA6icD5Bbd33VM8tfT29IVN8/k3DUqPbWlHdbgg/Cd9Y2kAf/hIEtztsSxqskAJROpJ6e3paXbjgdVZiNph05fmEFpALUkvfyxtMMfWzU5TRzAUo6CV3x4cqzzj0vXBqyr6m8zOXe7Sks4Xy9pLWMaArBHpRt7OoP29xGAhouA98DkWPorSxEBgLBACbBU5XSc2/uGCw9I64KWAwAIQHtNCTi39Q65D2vXOeofp7QHBCB+AXTMJ/uG3Vfmj828n2gAAhBLJ2BWJbtSv+wbnhsjGoAAxFMGZExic/+IO9O7ae7WN1/+8NOEBkI8amskrl2AilivaD17f9GY3dNj6ecl6VJr9b+G64EAAYiXFljvpIx52Jsv/peRfla09vDx7cteExcFBRlfZmJLkjghABBHKj2eHZgDAAAEAAAQAABAAAAAAQAABAAAEAAAQAAAAAEAQAAIAQACAAAIAAAgAACAAAAAAgAACMCZsZ5HFKEhWMvYCr0AGMfME0ZoBMYwtsLvAKxeIozQGAegw0Qh9HMAZi9hhAZ5gP3EIMQCsOyOGcl6jxBGaNAcwCNdf3OSQDRSYuvxISwMCnVPfkmTLAgahRJA8rziDwgl1FUAvOI/EYUoOID1x9S9clkmnXJeq5OhALy/TlnvvdMzhaO672ziEfYSQJJ6hwv3Osa5i5BC7Wd/+83JsdRXiURESgApq6mt6a9Z6x0hpFDbyd87Wkr+hwhGlBxAz+aCnMJ8n7MsdVQyCUILS0n/+ULxPU5S+WNjXYQjSgIgSZfst/rND49fkF624rC4zwCqTv65iy7987MOPzXAXFIkBUCSLt5r9dqPZvs6UomDxjjnEmIIYPtfPXXKG7j0M515kj+ScwCnefEao0QiMTm5Nb3KenYnD8CFMplfmvDbmj7XMSL528EBnOYhSevUvWnud5IJ3WuU+Kzh+IJKpwRri//oubp7+m87jy6MFWgrAfC583Ut9HJ7Nxc+Yxznk5K9WlYfkrEdxjBV0Ob2XsaY+dKNPWaftd6jU2PpH75zbEAM6LqL67pjPwa4th8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAI/w8cs+8zb+WSYQAAAABJRU5ErkJggg=='),
(347, 'Microsoft Office - OOXML - Presentation (Slide)', 'application/vnd.open', '.sldx', ''),
(348, 'Microsoft Office - OOXML - Presentation (Slideshow)', 'application/vnd.open', '.ppsx', ''),
(349, 'Microsoft Office - OOXML - Presentation Template', 'application/vnd.open', '.potx', ''),
(350, 'Microsoft Office - OOXML - Spreadsheet', 'application/vnd.open', '.xlsx', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAeUklEQVR4Xu2df5AlVXXHz7ndr98syw+BeW9Wy1JcQFSEhQTYKDGxsCoQSFUsTCCIiTHK4o+AgoQ1ZH68eTMEFgmIW0ZYfxQJVStqhVRSCkgCWpUSBSLyQ3RhAUnirjvz3iwu+2vmve57Uj37g91lXp/uN/3jdveZf+/te8/5fs98Xnff7tsI8icKiAKlVQBLm7kkLgqIAiAAkCIQBUqsgACgxOZL6qKAAEBqQBQosQICgBKbL6mLAgIAqQFRoMQKCABKbL6kLgoIAKQGRIESKyAAKLH5krooIACQGhAFSqyAAKDE5kvqooAAQGpAFCixAgKAEpsvqYsCAgCpAVGgxAoIAEpsvqQuCggApAZEgRIrIAAosfmSuiggAJAaEAVKrIAAoMTmS+qigABAakAUKLECAoASmy+piwICAKkBUaDECggASmy+pC4KhAZArUGHg/YuQIRzgOg0rb3lqPAoAFURGftXoNWsBHpQG+1c0Wo6a/ufQY4UBXorwAKgPjx7PIC92gPvUqXUYSJmvArwAJhzCaw/bjft78Y7s4wmCkDv7wIc16CBneSOgaevAaVsESsZBXgAdElrvUPZ+J5Ww3kimShk1LIqsOAZwFBjdnlXW/dYACvKKkxaeYcBwN5YNmnPXjlzPW5KKzaZp/gKvAYAQyNzp7iI/6EAh4qffvYZRgCAH+xPQdm/12rgjuwjlwiKoMBBAPB/+V2tHpZ//vSsjQgAANLfaW1w3g/fRi+9KGWmoiqwHwD+Nf8r2v2xnPana3VkAACABlw707SvTDdSma2ICuwHQG20eyMArC5ikibn1A8A9uRDV8ryoMnO5iO2eQD4S30EuEHu9qdvWv8A0J4sD6bvV9Fm3AsAdx0puqxoyeUhn/4BACDLg3lw2OwY0X/CT7udKXnIJxujFgOAvRHL8mA21hViVqyNuhcD0N2FyCaHScQAAD9rWR7MofcmhIz1MfcOIlplQjBljCEmAMjyYBmLJ4acsT7afYQAzophLBmiDwViA4AsD/ahvhyCtdG5NoA6VqTIRoE4AbAnA1kezMbJfM7qA6Ajr/RmZ178AJDlwezczN/MWBvtUv7CLk7E8QNAlgeLUx3JZyIASF7jwBmSAMDeCWV5MGNv8zC9ACBjlxIEgCwPZuxtHqYXAGTsUsIAkOXBjP01fXoBQMYOJQ4AWR7M2GGzpxcAZOxPGgCQ5cGMTTZ4egFAxuakBwBZHszYaiOnFwBkbEt6AJDlwYytNnJ6AUDGtqQJAFkezNhsA6cXAGRsSgYAkOXBjD03aXoBQMZuZAQAWR7M2HdTphcAZOxEZgCQ5cGMnTdjegFAxj5kCQBZHszYfAOmFwBkbEL2AJDlwYxLINPpBQCZyg+QPQBkeTDjEsh0egFApvKbAQBZHsy4CDKcXgCQofj+1CacARwggWwumnE9pD29ACBtxQ+ZzzAAyPJgxvWQ9vQCgLQVNx0AsjyYcUWkO70AIF29XzObcWcA+yOUzUUzLo1UphcApCJz70nMBYAsD2ZcGqlMLwBIReY8AkCWBzMujVSmFwCkInM+ASDLgxkXRwrTCwBSEDloCnMvAQ6KWpYHM66TpKYXACSlbMhxcwIAWR4M6WfeugkAMnYsNwCQ5cGMKyWZ6QUAyegaetQ8AWBPUrI8GNrcHHQUAGRsUv4AIMuDGZdMrNMLAGKVM/pg+QOALA9Gd9ncIwQAGXuTRwDI8mDGRRPj9AKAGMXsZ6gcA8BPV5YH+zHdoGMEABmbkXMAyPJgxvWz2OkFAItVcJHHt5RdhQZ2eg1TH56bI6WcRU6T6OEacO1M074y0Ulk8EQUEAAkImv4QSvKrm1uYLvXEbXR7gYAOCn8iFn1lOXBrJRfzLwCgMWoF8OxmuismQnnsZ4AGHO/BESfjGGqhIeQ5cGEBU5keAFAIrKGH5SILmtPOF/teQkwNncqETwBoDD8qNn01FrvUDa+p9VwnsgmApk1qgICgKiKxdwfkdZPjzuXBg1bH3VvJ6DLY546qeE2ac9eOXM9bkpqAhk3PgUEAPFp2edIent1uzP0q1txd68BjmvQwE7t3g8Av9/nJGkfJsuDaSve53wCgD6Fi/Uwoo+0Jpw7g8Z841W0pLPUu42U97E8XA4A6e+0Njjvh2+jF6tWMlisCggAYpWzz8EInmtZ9snQQJcboTbSOR1RXU5A56DWbzZ5iVCWBzk3s28XAGTvwXwERPDZ9kTlFkPCkTBKooAAwBCjNejdFYIzt0xUnzEkJAmjBAoIAMwyeaPVtc/ecgO2zApLoimqAgIA85z9qUL7vKlxnDYvNImoaAoIAAx0VHv4vK28C6cmqk8bGJ6EVCAFBACGmqm1nrWUGp1W9m1BLwsZGr6ElRMFBACGG6UBX0DQa6xdlfVTN+NOw8OV8HKmgAAgJ4Zp0DsVqHuR6CEAfJIs+5dLAX7zUgPmAJBykoaEaZgCAgDDDClaONyGJ0XLN2/5CADy5ljO4hUAmG2YAMBsf3IfnQDAbAsFAGb7k/voBABmWygAMNuf3EcnADDbQgGA2f7kPjoBgNkWCgDM9if30QkAzLZQAGC2P7mPTgBgtoUCALP9yX10AgCzLRQAmO1P7qMTAJhtoQDAbH9yH50AwGwLBQBm+5P76AQAZlsoADDbn9xHJwAw20IBgNn+5D46AYDZFgoAzPYn99EJAMy2UABgtj+5j04AYLaFuQCABq+lUH2fNPwIUT0L2n3e8apbl+yAHc+vxTmzJZboRAFzFTAYAPplAGu9In3X1ETlUdn1xtwiksjyq4BxANBab0FUn0fLXtdq4I78SiuRiwLmK2AOALR2yVJfgN12s30TbjdfOolQFMi/AkYAYH7nW6Uvajecx/MvqWQgCuRHgcwBgAT/7lr2n29t4Cv5kU0iFQWKoUC2AED8agutT4T5LHYx5JYsRAGzFMgOAP4//7i1Su7um1UQEk25FMgEAATwb21l/0mUX/5agw4H7V2ACOcA0Wlae8tR4VEAqlIuyyTbcimguxroNwjWiwrwCQJ4UO2y7o3rK1FZAGCjp+wzwl7zDzVmlxPZqz3yLlWglpbLfMlWFHitAv5XoixtrQdw10xPDrywGI3SBYDWLth4ZqvhPMEFfVyDBnaSOwakPyu/8pxa0l5OBXSXUN16ONpjLzVwth8N0gUAwJpWs/I5LlD/V7+rrXssgBVcX2kXBcquAGl4Ci3vA63xgeejapEeADRsVrP2W7lrl/rY3Kke4QMKcChqMtJfFCirAvPvy5A6tzXh/DSKBqkBgIiuak84XwgKrj48e7wH6mGlsB4lCekrCogCAPMQ0HB2a3JgY1g9UgEAar0VZ503Bf36v/EqWrL7CPcRBXBK2OClnyggChyigIafVWx75eYG7gqjTSoA0IBrZ5r2lYG//qPdmwngs2GClj6igCjQWwFEuGV6vBLqfykdABCdNTPhPNYr5MGx2beih8+AUrYYKwqIAotUwH+xDuid7cmBZ7mREgeABpqaaVZeH/TE37HD7teVoo9wwUq7KCAKhFRA0z+1Jp2/5HonDgDU9I3pSeeDvQI5ejUdpSqdLUqpAS5YaRcFRIFwCmitZ3XXWfbyGtwWdETiAACgz7Sazm29gqiPuB8ipLvCpSW9RAFRIKwCRHhpe8JenykAEOC86Wble72CkNP/sHZKP1EgogIav9aatD+WLQC0d0LQ88q14e5PQMFvRUxNuosCogCvwGOtZuWsTAHgdO3BTTfgTM9LgOG5GVLqGD4X6SEKiAJRFCDttduTA7VMAdBSdhUa2OkVRG10riMv+0SxVfqKAmEV0N1Ws+pkC4BmBYMCqI12KWw6C/fD81tN+75eYwyOzN2HqM5b3By9jtb3tZrV83vDzf1DALo3aG7uwxmL1yeZzKOOqrXeoSz8Dmn4vgXwJLqVF7dUYVvQj8P8HA1yls3BUWR3l3sIp/n7QWiiC+J7NTzf9cP5wNVX4qsAXAAxFPgvWso+tdfmIscOz71dATwV+0NGWrvahlNmGtUNC5rQILvmuU8DwttKDoCNALSmoirfCPt4KlfU/uYw5HU/qLVabVm0nOvPtOe6frjcuf+/IgAAAOjKVtNZ2/OXeMxdC0R/zYkVpZ17vLk20rkSEHsuf+6bizMoBkBGSSu2vhr0biQ13J6y18I67MY28IEDNcipee7VGnVDgar2P0d+64fLmauvQgDAf9lo1nZOfKWBWxcS5MgGHTPgdjbGdbMxzvk4g3IJAILnLNAXbpmoPsMVaBzt9ZHOClere/o9G4jTzzD5pDkfV1+FAIAvely/yGEMjPOMgzModwDQ8HjFts/d3MB2OC3j6VW/joa0ch9ABaf2M2Je64fLlauvwgAAYrom5wQFgFivGTmDcgaAZyvK/t20//n3eeZDwEXv4b7OBHJaP1y9cvVVHADMK7H4u/KcoADx3jXmDMoLALTWu8iGM7c2qj/nNUyuh3854CE90t89gfzVD6ckV18FA4AvR7z/oAcLHH+BcAblBQAEdHW76dzKFWQa7bXR7moAuLG/ufJVP1yOXH0VEADxnqLvFzihU0TOoJwA4NmWst8Z5jsPx450zlRKrdIuvFdZ3pv5h8B0F0C9RIgPKq1vn55wnuSK3n924Jiu94u+LgVivsRLun44Lbj6KiIAElkWTOomEWdQLgBA9JHWhHNnUDHOf9iFul8Cwr/gijaonRBvH3jFuvpXt+LuoH6Do53LEHBdf3PFvyyYVP1w+XH1VUgAxL3MEvd4B5rGGWQ8ALR+pbrTWRb0D7nnq07uAwDwLq5gw7XT95eqyvlBe+H7c2rd2dLPE4Nx+x33eOE02tOLq69CAmD+diCzD2HYB3X2yBj/L8I+EzmDTAcAIq2fHncuDfz1H+7cCQo/HKVw2b6I/9gatz8VOO9o91sA8KfsWAt0yEv9cLlx9VVYAMS4LBjrst+hhnEGmQ4AIrqsPeF8tVchDo51zkDCnvtBcgUc1I6oV0yPV5/q1ac+0vk4IX65rzniu+eTaP1wuXH1VVwAzCsTx137JO8K86dopgNAMxu+1ofddaToMq5Q+2vHL7Wads9HvOsj3XcRwsP9jZ2P+uFyKzkAfHkW8w8cB0CCLeIMMh0AFWXXgh78Ofbv3I3KohO4Qu2rnWBDa6Ly9p5nANfRENnulr7G3n+Q2fXD5cbVV8HPAObl6e8ULL5TwECPOINMB0CW+z2g1p3pyWrPl4BOuIKq2452+/po5gGmGV0/AgBOgfn26Dfx4r0J1DtIAUAoAzPuZG79cMJw9VWGMwCIugwTtT9nQlA7Z5DpZwBW165vuQFbvXKsjXafA4ATF6NR1sdGrYeo/ZPMj6uvUgBg/nZOpGXB6MTv10TOINMBoJFWzow7j/a8Dh91byegy/vVx5TjTK0fTh+uvkoDgAjLgpTmDkOcQaYDgIBWtZvOV3qeAYx0TgfEx7lCNb49/D2hVOuH042rr/IAYF6pMHf1/aW59PYY5AwyHQCAdHdr3LkkqBDrY+4dRLSKK1bz282rH04zrr5KBgBfruBlnSBBa6P8Jp+cIYe2cwaZDgB/s8+q7QwF7ffnf/p97vDuvYD43qj6mNffrPrh9OHqq4QACF4W7CloyE0+OUOKBoD5fIg+2ppwvh6U+zwEjvRuBvI+AaACd4qOqmHK/QOXBdOuHy53AcCCCgXf5FvokGjvDnC2vNrOGWT6GcDeTDa2ttgnh9n8c2hk7hStrFVA9D4Nenl/G3eE1zeZnubUD5cfV19lPANglwUPFTXuTUUPHJ8zKCcAAAK4pt2s/ANXkJHbG+S8AeDIOa/7FgRYoQjOIUV/BKCOiDxWTAdwy3xp1g+XEldfpQSALxq3rHOgsLUEthXfNz5nUF4A4G8DXiE4M42dgN/QoMO6unsxEH4OEN7K/RMk0W5K/XC5cfVVWgCwy4J7lU3swyJ7x+cMygsA9l0KWF377KAHg7iCjdS+iiqDQ+4VhHpSgVoS6djFduaWBVOqHy4Nrr7KC4B55YKXdfweyX5aLP9vA76mADU8bnn2ealBAACWjcyd7IG6J/2zgezrRwDAKRDYnr2BHKFzdgawT+2NSukLpxrVny3KnggHL/tbqnmWe3+6n5rPvn44ibj6Ku8ZgCGncJxBOQUAzH8aDNRIe4v9xTCrA1whh2mfh0DF/WEq7x4YUj+cLlx9lRYAptzE4QzKKwAOKMzYPw4aVPRDjbl3uhoeTfqegCn1IwDgFFig3aRlnBIAYM/dFv/z4IDfJYSHLAVPAVRePGIrbHt+Lc71YWHgIfWR7jWE8Pm4x903nkn1w+XI1VdJzwDMeZCDM6gAZwBcjQa2+5t+kFK/BMD/VOTdMTVRfZodcBVVastc/8OkCb2GbE79cFpw9VVGABj1KCdnUNkBcGiBE+CXD1fW1UFbgvvH1EY7HwXAnpuVcv84Ae1G1Q+XB1dfJQSAWS9zcAYJABYocaIfVHdUzg/6FsHQNbTUPawz1c93AYL/qcyqHwEAp8BB7ea9zikAiGTg/s4IeMd00/540NG1sc43gfCi/mZY6Cjz6ofLjauv8pwBcMs2r77tl+qGDpxBcgYQUOKKTm81nCd69Rgc7VyOgLdz/ySh2g2tHy52rr5KAwBu2ebgt/1kSzCusExo978T2B63P9ErlsV/F+DVkU2tH84HAYC/BYjWW2dt58RXGrh1IcEOfdsvan/OhKB2ziA5AwhQj+C51kTlpJ4AiOW7AGbXD1d7XH2V5Awg+i96NOJzNvRu5wwyHwB6W3D26qj+1WFux6XzXQCjt5XntOXqqwwAMPrDDpxBpgNAPgwy93YF8BQoZR98v1m72oZTZhrVDQv+kya0w9Shc3H1VQIAmP1pJ84g0wGQ8XcBnm01K2/r9Ss4NEZ1Te4U9yvJnGec3/8msWFWDejexcUXfDRXXwUHQBwGLAYgvLWcQaYDgPsuwOCY+2UkClyu41VauIf/UFC7aX+yNwA6v6MJf9Tv+Nzr4uE2iU22frjcuPoqLgDCL9v0/AXZK25/lxCcM3vbOYNMBwARXd6ecNb1vhPfWUGIPZfqQsq0cLcklwFzUj+cflx9FRYA8d7Ei34TkTNmXztnkOkAAE3fbE06fxaUbz2BrwMl/SBQXuqHqzOuvgoJgLiX8eIe70DTOINMB4D/lp896yybuhl39irGBL4L8F/V7fa5ST0KHLffcY/H/dNHqa9CAqCfrwFzosb7i/DqbHkHgJ8JEV3WnnACX7zxITB7pHfLYu8HIOI65xXrM0H//H5MgyOdjyFiz0+WBfsd/xlfUvXD1S1XX0UEQDLX7PFdEx7kGWeQ6WcAfjIa8IUZZb0DGtjhCrI+0lmhUV2ORO8D1G8BUJWgY/a9DkyAD6HS64Ie/d0/ToOcY7X3cwV0PBfPAu25qh8uP66+CgiAJO+6xrGqcLBlnEGcwUm3hwUQEqyenqjclHQ8YcavjXZXA8CNYfq+tk++6ofLkauvggEg/n/QpAuEM4gzOOn2sADQWs9aFqycHq8+lXRMgTccRzorPKRH+vviUP7qh9Oaq6/iACChU/SkTxE5gziDk24PC4B9lwKWa509/fe4yIdv+suqfh0Nueg9bFm0PPIIOa0fLk+uvgoDgHRvssR3k4gziDM46fYoAPBj8QCetF373LQhUGvQMk+791sAK/rRJK/1w+XK1VchAJD2Mkuc83EGcQYn3R4VAPMQ8PBFW+kLpyecJ5OOzx+/NtI5nVD9CwK9pZ/54vQzzPxpzsfVVyEAkMSyH2dkXL8YnEFcHEm39wOAPZcDek6BGmsp+9YwqwP95HHCFVTddrR7NWrdIKWcfsbYc0x8Z3RhY4irfrj5uPoqAgCSWbbhlI3pmpEziAsj6fZ+AbAvLv9sQCm9Bq3K+lYDd8QR7+C1dISqdi/VqK7t91f/gDhyXT+cnlx9FQAASS7bcPIu/q4xZxAXQdLtiwXAvvi01rsA1b0W0IMA+CR49ouHObDtpQbMASAtnAfhcQ2o7gR4HYK7HDw6zUM8B0CfH9+HP/JdP5z/XH0lDwBlV4NOAWujcx3uYRAuSWkXBUSB1yrgP0Q1PVmtBmmTOACcrj246Qac6RVEfXhuhpQ6RgwUBUSBeBXQ4LVmmgP1TAEA6J3YGh94vlcQtdHufwPAb8ebuowmCogCCPDodLOyMlMAIMB5083K93oCYMz9GhD9ldglCogC8Srgvzg1PW5fnikAAOgzraZzW89LgBH3Q4R0V7ypy2iigCgAhBe3JuxvZQsApLtb484lvYI4ejUdpSqdLUqpAbFMFBAF4lFAg95p73KGgvZp8GdK/CagBpqaaVZe33upB6A23LkTFH44ntRlFFFAFAizY1IqAPAn0URnzUw4j/WyZXB49iRU+AyAssQ6UUAUWJwC/vIfgn7b1OSSX3IjJX4GMB8Awhdb45VPBwVTH+3eQgBXcQFLuyggCjAKIEy2xisjYXRKBwCgZyrKedPmBu7qFdQbGnRYR7uPIsDJYQKXPqKAKLCgAj9pKfvdYd+/SAkA8xvHfbo14XwxyLTa8OyJWsEPFVg1MVcUEAUiKqBhs032u399Pf5P2CPTAwDAJrXLPom7K+m/2qkJ7lcKA59gCpug9BMFSqLAJk/pP9jaqP48Sr5pAsCP64ZWs3IdF2B9ePZ4DdY9qOBUrq+0iwKiADzmefYHtl6P/xdVi5QBoLuIcEaYfeOOa9DATu02APTV8rJQVFulfxkUmN+HUak101vs62EddvvJOWUAzIf4LCj7jLDvhvtnA2DZ13rkXapALe0nSTlGFCiWAno7gPXPrrJuermB/7uY3LIAAADBPa0N9kXwbfTCBj90DS3Vh3nnE8I5SLRCg7/xI72uv91fw84q/USBbBXwd1YCwN8A4AsK8QlEeNDZZt3HfRglbNTZAMBfFJj/sqv1qaAnBMMmIf1EAVGgPwUyA4Af7jwEfmFdEeVMoL805ShRQBRYSIFMAbA3oH+lWfvD7Ztwu1gkCogC6SpgAgD8U4HnwKKLQ333LV19ZDZRoNAKmAEAX2KtXbLULYj2RNgVgkI7I8mJAikoYA4A9ierf02An7d2VdZxTw2moI9MIQoUWgEDAbBHb//rKWRZdyvQd01h5VFooC60E5KcKJCBAsYC4EAt/N1NFVg/IKAfIapnCdzn7U715YEqbA/eVz4DRWVKUSBHCuQCADnSU0I9RAHuwxQiWLYKCACy1b/wswsAzLZYAGC2P7mPTgBgtoUCALP9yX10AgCzLRQAmO1P7qMTAJhtoQDAbH9yH50AwGwLBQBm+5P76AQAZlsoADDbn9xHJwAw20IBgNn+5D46AYDZFgoAzPYn99EJAMy2UABgtj+5j04AYLaFAgCz/cl9dAIAsy0UAJjtT+6jEwCYbaEAwGx/ch+dAMBsCwUAZvuT++gEAGZbKAAw25/cRycAMNtCrI3OdeTTW2ablN/odLfVrDr5jb/4kePg8GwLlTVY/FQlw7QVIO2125MD8qn3tIWPMB/WR7uPEMBZEY6RrqJAKAUQ4NHpZmVlqM7SKRMFsD7m3kFEqzKZXSYttAKo8SvTk7bUlsEuY23EvQiQvmlwjBJaThUgwkvaE/bdOQ2/FGFjrUGHa7czpZQ6rBQZS5KpKKBB78ZZZ0g++ZaK3H1Pgv6R9WF3HSm6rO9R5EBR4FAFEL/eGrc/KsKYrcBeAMweT4AbQCnb7HAlulwooLULQO9oTQ5szEW8JQ5yHgD+3+BYdw0SXFtiLST1mBQghJvb45W/iWk4GSZBBfYD4I1X0ZLZpe6PUcGpCc4nQxdcAQ3w9JLt9spf3Yq7C55qIdLbD4A99wJmj/dAPawU1guRnSSRqgL+gz8I8G459U9V9kVNdhAA/JFqjc5pWusHFFjyBNeipC3Xwf4/P9jq3HbDebxcmec729cAYB4CY7MnaLLuUQCn5Ds9iT4VBTT8DMC7UH75U1E71kkWBIA/w/w9gSPccdT6KlkdiFXz4gymtUuW+oKD9tjmBu4qTmLlyaQnAPZJUBuePREs+3OavEsUqCXlkUYy7aWA/5CPQusb4Lk3yq9+vuuEBcC+9AavpSOg6l2gCM4BRSu09pajwqPkVeJ8FwAfve6Spm1KWS+Cxic1wkMwZ31XnvDjlctDj9AAyEMyEqMoIApEU0AAEE0v6S0KFEoBAUCh7JRkRIFoCggAouklvUWBQikgACiUnZKMKBBNAQFANL2ktyhQKAUEAIWyU5IRBaIpIACIppf0FgUKpYAAoFB2SjKiQDQFBADR9JLeokChFBAAFMpOSUYUiKaAACCaXtJbFCiUAgKAQtkpyYgC0RQQAETTS3qLAoVSQABQKDslGVEgmgICgGh6SW9RoFAK/D9XUkcPa/TKYgAAAABJRU5ErkJggg=='),
(351, 'Microsoft Office - OOXML - Spreadsheet Teplate', 'application/vnd.open', '.xltx', ''),
(352, 'Microsoft Office - OOXML - Word Document', 'application/vnd.open', '.docx', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAeNElEQVR4Xu2de5QkVX3Hf79b1T2zPERlehaNx8AiipGn8lDQ5EjOAQJ/RMHgUcwxPgBRQREDCvPo6RkCqwjiRoUVDQnnrCSYzUkOguIBc0xEHkF5+NgFBEzYdWd6ZhGW3Z3prrq/nJpdFvbRdau6Xre6v/1v3Xt/39/nd+vbVXXrwYQfCIBA3xLgvs0ciYMACBAMAJMABPqYAAygj4uP1EEABoA5AAJ9TAAG0MfFR+ogAAPAHACBPiYAA+jj4iN1EIABYA6AQB8TgAH0cfGROgjAADAHQKCPCcAA+rj4SB0EYACYAyDQxwRgAH1cfKQOAjAAzAEQ6GMCMIA+Lj5SBwEYAOYACPQxARhAHxcfqYMADABzAAT6mAAMoI+Lj9RBAAaAOQACfUwABtDHxUfqIAADwBwAgT4mAAPo4+IjdRCIbAC1uuxD2j+dmU4ikaO09pex4v2IVAUYuyfQbFRCa1Aba13QbFRXdB8BPUGgMwGjAQyPzB9M5F7qk3+2UmovwEyXgNkAFjwh5y9nG+73042M0UCAOn8X4MC6DG4Wb5x8/XlSygWsbAiYDaAtWusXlMvvatarD2WjAqP2K4E9HgEsrc8va2tntUN0ZL+CySvvKAawXcs67bvHz13B6/LShji9T2A3A1g6unC4x/wjRby099MvPsMYBhCI/QUp90+bdX6heOVQ0AsEdjKA4J/f0+oe7Pz5lTamARCJvq25pvoeupX9/FQiUq8S2GEAwTn/89q7F4f9+ZY6tgEQkSZeMddwL8xXKaL1IoEdBlAba19FRJf2YpI259SNAWzLRy7E8qDNlS2HtkUDCJb6hHgNrvbnX7TuDUD7WB7Mv169FnG7AXgrRck5vZZcGfLp3gCIsDxYhgrbrZGDO/y015rGTT7FFCqJAWxXjOXBYkrXE1G5Nua9n0hu6YlsSphECgYQZI3lwRLW3gbJPDzu3SAi59ogph81pGQAWB7sx8mTQs48PNa+T4iOS2EsDNEFgdQMAMuDXdBHF66NLcwSqf2BohgCaRrAtgywPFhMJcsZNTCAFh7pLa546RsAlgeLq2b5InNtrC3lk907itM3ACwP9s7syD4TGED2jEMjZGEA2wNiebDg2pYhPAyg4CplaABYHiy4tmUIDwMouEoZGwCWBwuur+3hYQAFVyhzA8DyYMEVtjs8DKDg+uRhAFgeLLjIFoeHARRcnPwMAMuDBZfayvAwgILLkp8BYHmw4FJbGR4GUHBZ8jQALA8WXGwLw8MACi5KAQaA5cGCa25TeBhAwdUoyACwPFhw3W0JDwMouBKFGQCWBwuuvB3hYQAF16FIA8DyYMHFtyA8DKDgIhRvAFgeLHgKFBoeBlAofqLiDQDLgwVPgULDwwAKxW+HAWB5sOBJUGB4GECB8IPQNhwBvAwBXi5a8HzIOzwMIG/iu8SzzACwPFjwfMg7PAwgb+K2GwCWBwueEfmGhwHky3u3aNYdAexQiJeLFjw1cgkPA8gFc+cg9hoAlgcLnhq5hIcB5IK5jAaA5cGCp0Yu4WEAuWAupwFgebDgyZFDeBhADpDDQth7CrCTaiwPFjxPsgoPA8iKbMRxS2IAWB6MWM+yNYMBFFyx0hgAlgcLninZhIcBZMM18qhlMoBtSWF5MHJxS9AQBlBwkcpnAFgeLHjKpBoeBpAqzviDlc8AsDwYv8r29oABFFybMhoAlgcLnjQphocBpAizm6FKbABBulge7KboFvWBARRcjJIbAJYHC54/ScPDAJISTNi/qdwBqnOr0zDDIwsLolQ1YZhMu2viFXMN98JMg2DwTAjAADLBGn3QinJr6+s826lHbay9hojeFH3EolpiebAo8kniwgCS0EuhrxY5bm6y+kBHAxj3vk4in0whVMZDYHkwY8CZDA8DyARr9EFF5JzZyeqNHU8BxheOEKGHiBRHH7WYllrrF5TL72rWqw8VowBR4xKAAcQllnJ7Zlk1M1E9O2zY4THveiE5L+XQWQ23Tvvu8XNX8LqsAmDc9AjAANJj2eVIetPApurSZ67lrZ0GOLAug5u19wMi+rMug+TdDcuDeRPvMh4MoEtwqXYT+UhzsnpT2Jivu0iWtPb2rxPlf7wMpwMk+rbmmup76Fb2U2WFwVIlAANIFWeXgwk91nTct1CdPdMItdHW0czqPCE5ibX+Y5uXCLE8aKpm8dthAMXXYFGBCF08O1m5xhI5kNEnBGAAlhRak95aETp2w+TAryyRBBl9QAAGYFeRH3fa7okbruSmXbKgplcJwADsq+wvFLunTk/wjH3SoKjXCMAALKyo9vkJV/lnTE8OPGqhPEjqIQIwAEuLqbWed5Qam1HudWEPC1kqH7JKQgAGYHmhNPFvmfRyZ0tl1fTVvNlyuZBXMgIwgJIUTJPerEjdziJ3E/HD4rhP7U30h6frtEDEUpI0INMyAjAAywrSa3JMLzzptXzLlg8MoGwVK5leGIDdBYMB2F2f0quDAdhdQhiA3fUpvToYgN0lhAHYXZ/Sq4MB2F1CGIDd9Sm9OhiA3SWEAdhdn9KrgwHYXUIYgN31Kb06GIDdJYQB2F2f0quDAdhdQhiA3fUpvToYgN0lhAHYXZ/Sq4MB2F1CGIDd9Sm9OhiA3SWEAdhdn9KrgwHYXUIYgN31Kb06GIDdJYQB2F2f0quDAdhdQhiA3fUpvToYgN0lLIUBaPKbitWPRdPPmNVa0t4TVX9g45IX6IUnVvCC3YihDgTsJWCxAehniZxVSvTN05OV+/HWG3snEZSVl4B1BqC13sCsvsyOu7JZ5xfKixbKQcB+AvYYgNaeOOqrtNVtzH6JN9mPDgpBoPwErDCAxTffKn3WbL368/IjRQYgUB4ChRsAC/2H57h/vbHOz5cHG5SCQG8QKNYAmG9ssnN+lM9i9wZuZAECdhEozgCCnX/CORdX9+2aEFDTXwQKMQAh+vdZ5b4vzj9/rS77kPZPZ6aTSOQorf1lrHg/IlXpr5Ih2/4ioNua5A9MzpOK+CEhukttcW5P6ytRRRjA475yj4l6zr+0Pr9MxL3UF/9sRWrv/io+sgWB3QkEX4lytLOKyFs+MzX42ySM8jUArT1y+dhmvfqQSfSBdRncLN44ib4Y//ImWtjenwR0W1hduw+740/Xeb4bBvkaANHyZqPyBZPQ4F+/rZ3VDtGRprbYDgL9TkA0PcKOf2ZzYvCJuCzyMwBN69W8+0bTucvw+MIRvvCdinhp3GTQHgT6lcDi8zKiTmlOVn8Rh0FuBiAiF81OVr8aJm54ZP5gn9Q9SvFwnCTQFgRAgGjRBDSd2JwafDwqj1wMgLXeyPPV14f9+7/uIlmydV/vPkV0eFTxaAcCILALAU2/rLju8evrvCUKm1wMQBOvmGu4F4b++4+1rxaii6OIRhsQAIHOBJjpmpmJSqR9KR8DEDlubrL6QCfJQ+Pzb2Sff0VKuSgsCIBAQgLBg3Ukh81ODa41jZS5AWiS6blG5TVhd/ztP+J9Ryn5iEkstoMACEQkoOUfm1PVvzG1ztwAWMt3Z6aqH+wk5FWXyn6q0tqglBo0icV2EACBaAS01vO6XT3g2eX8XFiPzA2ASD7bbFSv6yRieNT7kLDcHC0ttAIBEIhKQITPnp10VxVqAEx06kyj8sNOInD4H7WcaAcCMQlo/nZzyv14sQag/TeE3a9cG2k/SIreGjM1NAcBEDATeKDZqBxXqAFU2+7Quit5ruMpwMjCnCj1anMuaAECIBCHgGh/dnZqsFaoATSVO0B1bnUSURtbaOFhnzhlRVsQiEpAt5uNgWqxBtCocJiA2lhboqazp3aZf3iiLtUDFmg/cdvLfKajgvcRaJHT8380WW9izbdpRT8WpocrrcpTGwbouTBzXeRli36tn2eHb9OafixEDw84lafWEz0fRf9riV6xIO1lrOkoVvTn6fLn05oN945Oc3BodOEOZnVqkjnaua++o9kYOK3zn6P3F0Rye5LYpv0j81UAkwDrDWAP9IOXk4jf/qDW6lLHkWVJCmTsK/QYkSyvOJVbot7eaRozV/1EawP9A5sqtzxzLW81aYuyfennZW9/SfvslPj/pqncIzq9nGb/kYU3K6JHUr9JTWtPu3T4XH1gzR5zrotb871HienQKEw6tTHtfzCAJHTrUq1p72JNelyRGkgy1K59NemtTHz5rKqsiPPmpFgastSv9RZmvnx2TWUF3cp+LF1RG6emXy5sNqorOv4Tj3srSOTTUWVFaWe6Pb422rqQmDsun0eJEbSBAUQllaBdbbR1tNa0Wjl8YIJhXuoq9Jjv6PdurA/8OpXxDIOkrp9ordb6vXNTA7/JRX+9dZRotZpJDuomXvCw2rxbPeT5Om/cU/9X1OXVg17r8bQuVucZDwbQzYzoos/QZfIaVt6dpOiwLrq/1EXTzyuue8r6Os8mGidm59T0Ez1YbbunhK38xJQWqXmtLgeQ5/2oW/55/SNvSya/Iw4YQKTpk06jYCci1/9pt/9EJPRYxXFPzHvnfzH7xPqJ1lbb7ol57/wv1y/cvqerI7GczsmJKNdrDjCAdPbtyKMEh9Mscq8oFbr8suuAWust4tKxeR32dzzf7VY/6c2k6di8Dvs76R+qt94qWu7p7ppM9lflifJddYABRN5102tYG21fRkxXxBlRSD4326heG6dPVm270h/hjU9Z6d113NpY+3IimuouXpY7aB4Gs3PWfW8ASZcZWeuWKPU7Jr5bK70yyvcL33CBDDz7ivaaGIeia5vKPSzK1f6h8dYxylfn+kLvdth/velIIxf9Qmuaa9zDolztz0N/cO/D/u322hj8X77XZHOInt8pxk4OAANIeKPRbv8imr89sNm5wLSmPTza+oQwfzPav5B8tNmo/kNY22Dt29vb/3slYnzGOzRmFvpFPtKcrN5klf6x1vlC/I1o/Hdtlf5FunwvMr6UDwwgbQMIruES/ffgJvfkMBMYukT2lcHWtCK1JHwS6k0VVT0g7CafYOfXe3k/IqJ3dDehd+6Vqn6tnx/YXD0gjIXV+vcANO1lurTHizMHYAAZGMBiARa/beieE1aM/cfa31NEZ4a1Mb0wJehbG2ndRIo/HKfwxrZp6WdZNTNRPTssXlH6a2Pt1UT0XiOLPTRI9x87/SOKqDnBALIyANJCwm8Lew/78Gjrk8L89bBiCcm5s43qtzq1Cc6ZWbjj+xSjToTd26WkX+Sc2cnqjTbqr422Pk3MHe/wCz9VSu1W3WyuKUQsPAwgMwMgEubrZyfc8zvVYrjePkE0/TSsVprl+LmJ6v0dxxj3bhCRcyPWO1azVPQbXvg6XKD+2mj7XcT0k1hQdmqcxlX7LFcVzJnBADI0ACJa22xUOj6ssXh3mvZ+H1Ymp+0Ob7iSm53a1MbajxHRIeZSd9Uisf6KcmthNy4VqX//y+WPlOM90xWZHZ2S7MBpGEgy9TCADA0gWGKbmRro+BDQ4gdOtRf6BJzpfQnDIwsLpqW+bqdI2fVr0gtzjYGOL5PdfvHxhW75bO/X3SF8Qct+u+YKA8jQAALY4YCFa2OeDpuApgIlvY/BNPnLrt+UXzrb41/ES/ciYvdZmOZXzz8OXOwORGSKbyqQqX/3U2Nbz6Txk/ZPqj+P/nGX8eK2zzIHU31gAAnpmwCbduCk/RPKhwFEBBjvHz3+EUNEGbGbmeYXDCA20p07mADDAJK98i1hedLrHv2cXgp5w1CHTE3zEwaQcIqYAMMAesQAFudJlKv6wWlVUe8Y3H0ym+YnDAAGkOilraYJZjLAhPgL6B6+LBgmqDaW/CWfcRM21QcGEJfoLu1NgE07QNL+CeXjGkB8gKHLgh2HS+kln3HlmuYXDCAuURjATgRME8xkgAnxF9Q9/CLfnkSl9ZLPuAmb6gMDiEsUBtD3BmBa5tt1SqX9UtE4UxYGUOiNQLgPoDePAIhMy4Iv30lrGbxWPKoJwABgAKFzxTRBTDtw0v5RJ7J17UzLgtsFZ/ZhkYhATPXBKUBEkJ2amQDbvgOVXX/C8iXsHr4sGAye7afFzPJN9YUBmBn29D+oaYLYbmAJy5ewOwzACDDpBDMFyHr8rONDv4mwpdtxChCtMFlP8KzHN2WZNH7S/iZ9pu1J4yftb9Jn63ZcBIxYmawnSNbjm9JMGj9pf5M+0/ak8ZP2N+mzcTuWAWNUJesJkvX4plSTxk/a36TPtD1p/KT9Tfrs3I4bgSLXJesJkvX4pkSTxk/a36TPtD1p/KT9Tfos3I5bgeMUJesJkvX4plyTxk/a36TPtD1p/KT9Tfrs246HgWLVJOsJkvX4pmSTxk/a36TPtD1p/KT9Tfrs2o7HgWPXI+sJkvX4poSTxk/a36TPtD1p/KT9Tfqs2W5a9nvpaT+8EOTlRct6gmQ9vmkCJo2ftL9Jn2l70vhJ+5v02bLdtOy389N+eCXYjrplPUGyHt80AZPGT9rfpM+0PWn8pP1N+mzYblr22/Vpv7jts8zRVB/cCpyQfjhgvBbcdCtxQvw5dY//jx7viCG7NGAAGT4NaPowhe0fBul1/fgwiPm17zgCSGK+Qmuak5U3dxoiyqfBFLtLpyd4puMYWX4aLAX9hX7azKB/aFxey+KtS1JiInwaLBE/0yFI0kPErMcPTZ75G80J91Od2kT5OKhiecf0RPXejmOMedcLyXmJitCpcxr6Rd4+PVm9z0b9tXr7naTpv7pnF2XZT24PHz+JgXSv/MWepv0DRwBdM9bCwkfPTFYf7jz5W+cL8TfCQrDI+TOT1es7tRmqt97Kmh/sWmbHjunoF5JPzDaqN1ipP8Ln2TvjwefBU5lzJgcq6xGA6dPaAbz9R9u3Kqb3hYHUQt+bm6z8VVib2oh3Iyn5WCoF2T5IWvqJ6NZmo3KWlfrH2t9TRGd2wy3di3jxLyJ2o3lPfUz7H44AuiP9k72Ve8rTdZ7veO5el32015pWSu0VagBab1FudWmzzh2/Yvu6i2TJ/L7enUz0zu7k7tYrPf2kN7tbqkunr+bNnbTZrH9PmtNexkt7vDhzAAaQ6iqAFiZn5V7K+WzYzh8UaGi0dS4zdzw0fnkRTYfRQdvX1mWvtud/LdmRQEb6Rc6bnayuDJuYuesfa53HxB1PrcJ3ovT/sdM9oohuATCAhAYQLJUpUU+R4rtZ65Vh5/w7ylKX6pD21zDJQVFKJcRPzSrnUKpzy9Q+uCZAos5hkXez1geJUtXQI4wc9Ps+P7mx6RxKK7ldRv27aA592q/rl3xGv5X4UBPDONv73gDiwEqrbW2s/UUi+rtY4wl9sTlZuSpWn4wad6Wf6AvNRmV5RpJiDVsbbV9GTFfE6rSjcZZX7dNYVYiXFQwgHq/ErWv11lFay72K1ECcwRaPNBS/vVmvPhSnX9ptu9av9bzD/PZIR0hpi37ZeLXR1tGa5Wdx+W8bIo8dNEuD2R0sDCDDybbr0MOXyVKP/XscR5Z1EzY4FWDlnNCs84Zu+iftk1R/cCrginPCzN/xdFIt3fQPbrwS7d8T9dRrpxj5HaJnc4rRARgMoJuZ1EWfYPL52vuBQ3RkF913dNFEjyrlnpy3CaSlXzQ9orR7ct4mEJiXVt6drOiIbvjne5Eu/YuMnXKGAXQzG2L2CQ47hdW/dvXPs4dY244E9Bl5nQ6krX/xSEDpM/I6HQhOW0Sr1d3yz3uZLs94MICYO3Os5nWp1rR3MWtdN12NjzVucDYaXBMgNdFU7leirA7EHX+xfZb6tZ5XrCaa0+5XoqwOJNGvSY93d87/YtT8/pFfjJjXEQcMoKuZFd6pVpd9RLfPJlKXdvuvE1VWcDRAoq9ip7Iq7GahqOMF7fLUHxwNKEdf5WyprAq7WahA/bmek790rpfarcah6GAAcWbWbm2FD6zTwGaiV7LvHUQsR/nCJ5Ho00x3+CUKu4fOWustxOp2h+QuIn6YfPfJvar03NN1WiBi2XM8i/ST3qxI3S4kdzlMDxNVntx3Iz33xApeCGMVPFK9pUX7keMty4Z/vlfld841+1WH4g1AuQNhh7C1sYUWkaqkvcNgPBDodwKsdWtmaiB0OTrzZwGqbXdo3ZU816kYwyMLc6LUq/u9WMgfBNImoMlvzjUGh8PGzdwAiP1DmhODT3QSURtr/w8RvS3t5DEeCPQ7ASa6f6ZROb5QA2CiU2calR92NIBx79sk8tF+LxbyB4G0CTDzypkJN/RlMtkfAZB8ttmoXtfxFGDU+5Cw3Jx28hgPBPqegPD7m5PuvxR6BEAstzQnqh/oJOJVl8p+qtLaoJQa7PuCAQAIpERAR3hPQxAq8yMATTI916i8pvNSFVFtpHUTKf5wSrljGBDoewJMfMNMw/2ECUTmBhAI0CLHzU1WH+gkZmhk/k2s+FdEyjEJxnYQAIFwAsHyH5M+dHpqyVMmVrkYADF9rTlR+UyYmOGx9jVCdJFJMLaDAAgYCDBNNScqo1E45WMApOcqqvr69XXe0klU8MqolvbuZ6K3RBGONiAAAnsk8GBTuSdEfX4kJwMgIpHPNCerXwsrWm1k/hCt6KeKnBqKCwIgEJOApvWuuCf8/gr+XdSe+RkA0Tq1xX2T6YGQxTe6CP1AKQ69gylqgmgHAn1CYJ2v9Mkb6wO/jpNvngYQ6Lqy2ahcZhI4PDJ/sCZndbcvdzCNj+0g0GMEHvB998yNV/D/xc0rZwPQbWY6ZmZi4BGT0O0f1qwT6c/hYSETLWzvRwI6eA+jUstnNrhXdPvOhZwNYLFMa0m5x0R9tj04GiDHvcQX/2xFau9+LDRyBoGdCehNRM4/ecr50rN1/t8kdIowACKh1c017ll0K/tRxW/71LN/mjCdxCJHagpevCmvTPYmmKjR0Q4EiiEQvBmKiP9AxL9VzA8x013V55w7nrmWt6ahqBgDCBYFiL8523A+FXaHYBoJYgwQAIHOBAozgEDSogn8xrkgzpEAigkCIJAegUINYHsa/ybz7odnv8Sb0ksLI4EACEQhYIMBBIcCj5Ej78/rNdhRwKANCPQDATsMICCttSeOuobZnYy6QtAPBUKOIJAlAXsMYEeW+vdC/GVnS2Wl6a7BLMFgbBDoBwIWGsA27MHXU8RxblGkb57myv1UZ90PBUGOIJAnAWsN4OUQgrebKnL+U0h+xqzWCnlPuK2BZwcHaFP4e/HzRIlYIFA+AqUwgPJhheIXCZg+TAFSxRKAARTLv+ejwwDsLjEMwO76lF4dDMDuEsIA7K5P6dXBAOwuIQzA7vqUXh0MwO4SwgDsrk/p1cEA7C4hDMDu+pReHQzA7hLCAOyuT+nVwQDsLiEMwO76lF4dDMDuEsIA7K5P6dXBAOwuIQzA7vqUXh0MwO4SwgDsrk/p1cEA7C4hDMDu+pReHQzA7hLCAOyuT+nVwQDsLiEMwO76lF4dDMDuEsIA7K5P6dXBAOwuIdfGFlr49JbdRSqvOt1uNgaq5dXf+8p5aGS+ycoZ6v1UkWHeBET7s7NTg/jUe97gY8Tj4bH2fUJ0XIw+aAoCkQgw0f0zjcrxkRqjUSEEeHjcu0FEzi0kOoL2NAHW/K2ZKRdzy+Iqc23UO4tY/tlijZBWUgIi/IHZSfeWksrvC9lcq8s+2mtNK6X26ouMkWQuBDTprTxfXYpPvuWCu+sgHPQcHvFWipJzuh4FHUFgVwLM32lOuB8DGLsJbDeA+YOFeA0p5dotF+pKQUBrj0j+pDk1+Hgp9PaxyEUDCH5D4+3lLHRJH7NA6ikREKarZycqf5vScBgmQwI7DOB1F8mS+b29e1nRERnGw9A9TkATPbpkk3v8M9fy1h5PtSfS22EA264FzB/sk7pHKR7uieyQRK4Eght/mOgEHPrnij1RsJ0MIBipVm8dpbW+U5GDO7gSoe2vzsHOT646ZbZe/Xl/ZV7ubHczgEUTGJ9/gxZntSI6vNzpQX0uBDT9ksg/A//8udBONcgeDSCIsHhNYF9vgrW+CKsDqTLvncG09sRRX62yO76+zlt6J7H+yaSjAbyIoDYyfwg57he0+B9QpJb0Dxpk2olAcJOPYue75HtX4V+/3PPEaAAvpjd0iexLA/7pSugkUnKk1v4yVrwfHiUu9wQwq9dt0fKcUs6TpPlhzXQ3LTjfxx1+ZnJlaBHZAMqQDDSCAAjEIwADiMcLrUGgpwjAAHqqnEgGBOIRgAHE44XWINBTBGAAPVVOJAMC8QjAAOLxQmsQ6CkCMICeKieSAYF4BGAA8XihNQj0FAEYQE+VE8mAQDwCMIB4vNAaBHqKAAygp8qJZEAgHgEYQDxeaA0CPUUABtBT5UQyIBCPAAwgHi+0BoGeIgAD6KlyIhkQiEcABhCPF1qDQE8R+H8Y4JwP3XghoQAAAABJRU5ErkJggg=='),
(353, 'Microsoft Office - OOXML - Word Document Template', 'application/vnd.open', '.dotx', ''),
(354, 'Microsoft Office Binder', 'application/x-msbind', '.obd', ''),
(355, 'Microsoft Office System Release Theme', 'application/vnd.ms-o', '.thmx', ''),
(356, 'Microsoft OneNote', 'application/onenote', '.onet', ''),
(357, 'Microsoft PlayReady Ecosystem', 'audio/vnd.ms-playrea', '.pya', ''),
(358, 'Microsoft PlayReady Ecosystem Video', 'video/vnd.ms-playrea', '.pyv', ''),
(359, 'Microsoft PowerPoint', 'application/vnd.ms-p', '.ppt', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAYdUlEQVR4Xu2df4xc1XXHz73vzezahriBnTWpUAqGhPxowBRip4nSqvwRUFHViLakQH8IBUPTiB9OKCC03p2d3QhISAKx2hBDUCoqIEV11SpUkKq0qiII0Aa7JI0B86MSpt6dXVLwr92Z9+6pxsZgY8/cN3Pfee/dfd/999177jmfc+73nZn7Zp8i/IEACJSWgCpt5AgcBECAIAAoAhAoMQEIQImTj9BBAAKAGgCBEhOAAJQ4+QgdBCAAqAEQKDEBCECJk4/QQQACgBoAgRITgACUOPkIHQQgAKgBECgxAQhAiZOP0EEAAoAaAIESE4AAlDj5CB0EIACoARAoMQEIQImTj9BBAAKAGgCBEhOAAJQ4+QgdBCAAqAEQKDEBCECJk4/QQQACgBoAgRITgACUOPkIHQQgAKgBECgxAQhAiZOP0EEgMwGo1fk4MvGFStF5xLzGmHi10molka4gDYMTaDYqPXNYG29d3WxUNw2+AmYuZQLiAjA6tnAaUXhjTPFlWuvlSxlmHrHZBWAxYgp+d64RPpyHf1iz2ATEBOCUOg/v5WiCYnM9aR0WG4O/3tkFoM3GmD06VJ9u1qtb/Y0UnksQEBGAVfWF1W0TbAmIzpJwGjbfIZBEAN4avdPE4br5r6id4AcChwikLgCrNi5+LFLqnzWpVcAsT6APAeg48wzp8DeadbVH3jOs4AOBVAWgc+ePjH4cmz+71PcpAERsftDcXv0sPaTi7LzESkUlkJoAdD7zv2miH6PtzzbVfQsAERlSm+Yb4TXZeorVikggNQGojbdvJaIbixjkUvZpEAE4yIOvwfHgUq6MZLGlIgCdoz4mtR3f9ieDnuaowQXAxDgeTDMTftpKSQCizax5vZ8I/PZ6cAEgwvGg37lPw3tnAeg84Wei1gwe8kkjHf3bcBGAt1bD8WD/2JfMDHcBGI8+R8QPLhkingWSggB0IsbxoGd5T8tdZwEYnYi+w8xXpuUQ7PRHICUBwPFgf9iXzGh3ARhvP8lEa5cMEc8CSU0AcDzoWebTcddZAGrji3NE+sR03IGVfgmkKQAH18bxYL858Hl8GgLQwk968yuB9AUAx4P5ZTP7lVMQgDZn7zZWPEQgfQHA8WCZqgsC4Hm2JQTgLSQ4HvS8NpK4DwFIQqnAYwQFoBM1jgcLnPs0XIMApEExRxvCAoDjwRxzm8XSEIAsKAuuIS4AOB4UzF7+piEA+efAyYMsBADHg04pKvRkCECh02N3LjsBwPGgPRv+jYAA+JezIzzOTgBwPOh5qRzTfQiA51nNUgBwPOh5sRzDfQiA5znNQQBwPOh5zRzuPgTA82TmJAA4HvS8bg65DwHwPJG5CQCOBz2vnIPuQwA8T2OeAoDjQc+LBwLgfwLzFwAcD/pcRegAfM4eEeUvADge9LmEIAA+Z68gAoDjQX+LCALgb+4OeF6EDuAwhPj1oGf1BAHwLGHvdrdgAoDjQc/qCQLgWcIKLwA4HvSqoiAAXqXraGcL1wG87SL+uagPpQUB8CFLPXwsrgDgeNCH0oIA+JAlLwUAx4M+lBYEwIcseSoAOB4sfnFBAIqfo54eFvcjwBFu43iwoHUGAShoYpK65YkA4HgwaUIzHgcByBh42st5IwA4Hkw79anYgwCkgjE/Iz4JwEFKOB7Mr1qOXhkCUKRsDOCLfwKA48EB0iw2BQIghjYbw/4JAI4Hs6mMZKtAAJJxKuwoHwUAx4PFKScIQHFyMZAnHgtAJ14cDw6U9fQmQQDSY5mLJc8FAMeDuVTNO4tCAHJOgOvyTR0OUV21utkZHVtcZK2rrutIzjekNs03wmsk14DtYxOAAHheGRUd1l6rq7luYdTG29uJ6Izih4njwTxyBAHIg3qKaxrmtfNT1ae7CsBE9JfE/OcpLilkCseDQmB7moUA5EE9xTWZef3cVPWerh8BJhbPZKatRNo51ym6fUxTxpg9OlSfbtarW6XXgv2DBJyLojbeZsDMj4BSfP/sZPWyXh6Mjkd3MfFV+XnZ18o7TRyum/+K2tnXLAweiAAEYCBsRZpkdg/trq569ZtqfzevTqnz8F4TPUJEv1kkz3v4guPBjBIFAcgItOgyzJc3p6rf67XGyRt4WWtFfCfr+AofPg4Qmx80t1c/Sw+pWJRdyY1DAJZCATA93wzCj1JdRbZwahtbZyulr2Li85Qxv1LkI0IcD9qy6X4dAuDOsBAWmOnLc1OVbxTCGTjhDQEIgDep6u2oIbO/wvTxXVNDP1siISGMDAhAADKAnOESLwTt8FO7blHNDNfEUh4TgAB4nLwurj+jVXjBzKSaXXqhIaK0CUAA0iZaAHsmVjtCHV80MzX0bAHcgQsFJgABKHByXFwzxiwEWo/P6vDOXj8WclkDc/0nAAHwP4c9IzCkXlRkbgv2Ve6fuV3tXeLhIrw+CUAA+gTm63BDZq8m/U+K+TEitY2D8OUVRP/3Sp0WiRQe5/Y1sY5+QwAcAWK6LAHbPzyRXX3pW4cALP0cex0hBEA2fRAAWb6w7kgAAuAI0DIdAiDLF9YdCUAAHAFCAGQBwrosAQiALF90ALJ8Yd2RAATAESA6AFmAsC5LAAIgyxcdgCxfWHckAAFwBIgOQBYgrMsSgADI8kUHIMsX1h0JQAAcAaIDkAUI67IEIACyfNEByPKFdUcCEABHgOgAZAHCuiwBCIAsX3QAsnxh3ZEABMARIDoAWYCwLksAAiDLFx2ALF9YdyQAAXAEiA5AFmAS64biplb6X9nQE0rp58hEO6rx0OvL9tCeHZvUYhIbGAMCEgTQAUhQPWDT/IIouF+zuW9mqvIU/uuOGGgYdiAAAXCAd6ypxphdSumvqSDc3KyrPSmbhzkQSJUABCAtnMZEHOg7aH/YmPuq2p2WWdgBAUkCEIAU6B74z7vaXDxXr/4kBXMwAQKZEYAAOKJWTP8YBeEfv15XbzqawnQQyJwABMAFuVL3NFXwhSSv5XZZBnNBQIoABGBQsp3NPxlciW/3BwWIeUUgAAEYIAtM9A9zOvz9fu78772RV1aq8e+wpt8i5rOUMaeypuOJdGUAFzClNARMm4jeJNIvk1Jb2dC/0GLwcFpfNEMA+i+kF2Idnpv0M//I2MIZHIQ3URz/odZ6uP/lMAMEjiRgjNmnKXiAgujW5uTwDhc+EIB+6BkTUag+3qxXt9qm/XKdl0cmmmYy1xDpwDYe10GgfwKmzaTvGN4dTrz6TbW///lEEID+qN3WbFRusk3p3PW1Crawoo/YxuI6CLgSMETPahVfNEg3AAFISt/Qa3oh/KDtDbsjG1vnEJtHlA5GkprGOBBwJXDg9yasz29OVZ/pxxYEICEtZt4wN1W9o9fwzp2fiH6EzZ8QKoalSuCACBj6VHN6+IWkhiEACUgpY15XC9X397r7r7qeV5jl0dNE9OEEJjEEBEQIdD4OLNsdrkv6nQAEIEEaDKlN843wml5Da+PtTndwbQJzGAICogQU0ddnG5XrkywCAUhAyTCvnZ+qdu7ux/w7sb74IW3op/i2PwFMDJEncOCHafzRucnh522LQQAshAzxzHyj8r5eT/zVJlp/Taz+xAYb10EgKwJGqe/NT4aX29aDAFgIKcMPzE5XL+02rPOEn660duEhH1up4XqWBAyZ/ayrJ9keWIMAWLPC1zUb1Tu7DRvdGP0RK77PagYDQCBjAszqsrmp8P5ey0IAbB0A0QWzjcqjXT//j0X3as3WVivj3GM5ECAy6rvN6fAKCIBDMSgTnz47PfxiNxO1sfZ/kqZfc1gCU0FAisDTzUZlLQTAAW+1HY7svEXNd/0IMLY4z1qf4LAEpoKACAE28dzc9HANAuCAt6nDIaqrVtcOYHyxhZ/0OgDGVEECpt1sDFUhAA6IbS+mqI232cE82ey72D4wt87VkxZpJYft1bGiNUrReYb5Qk16hbNtIvifBsQeNqTrC18CWhJo26DSCZKor1qdj+O4fakx+sYg4NUua9j4uNju2nV57n8/TKTrCwJQQgF4O+Q6V2tx9CWjTF2THuqnMA+NzUMAloz/CYBDABJAkhxiK3DpBEnGdsj26MbWWZHRWwbpBmx84L8bAen6QgdQ5g7gsNhHb+ZVRkc/VJrO7KdkiyAAHX9997/rxx3h75ggABCAtwl0NlGk4sf76QSKIgCHRMBn/49ViugA+rkdCYy1Fbh0ggRC6mmy83EgVvxk0u8EbHzgvxsB6fpCB4AO4CgCtfH2jUR0a5LSLZoAdHz23f/DuUMAklSh4BhbgUsnyNW+MqbFWv+PIvWY0WZzovcX1rl6Qjv+eZKPAtJ8pP13LR3p+KX9QwdQ8A7AVQCOCs+o7w7tDa62/cuokfHWekVqs60AM98AKftvi892PfP4bQ6967rNPwhA2QSAiJjoR8O7w8/0EoHOw0LGtHbZnhi0FVjqApay/33up6OG5xF/Pz7b/IMAlFAADoR84N2G4fpe4dfG239LRH/Qa4ytwCQEIE3/+9lMxxqbW/wJHbf5BwEoqwCQYWJ1Tq//Iz+6sfVnrNS3CykAKfmfcB91HWbbYGICmNBxm38QgNIKABErddfcZPiFbghGN7Z/nRU9XkwBSMf/hPsIAtCNQN4K55pA23ybgrrGL23fEt9zzUblQ10F4GZexWG0q6gCQETO/tvyb7uec/5s7ll/rYkOoMQdgCGzON8Y6vrG4tOv5qE33hstFFUA0vDfuoNyrg9p/yAAOSc4zztIGhvId/+lN5hrhyjtHwSgxAJATNubU5WurzJbNcGjhqOZonYAafgvvcEgAK6Ec54vfYeTtt8Tn1J/1ZwMv9htzKqJ1icMqycKKwAp+O9aXrnmL4HzNv/QAZS2AzCsWJ09O1Xd1g3ByHjrKkXqrmIKQDr+J9hDPYfYNhg6AFfCOc+XTrC0/W74bEeAnXm1idb3idXFRRSAtPx3La+88pfUb5t/6ADK2QH8+wodnv9KXXX9hr/zuvNoeWumiI8CE1Fq/ifdSN3G2TYYOgBXwjnPl06wtP0j8RlWFGxeroPrem3+zpyRja0rlFJ32/D77r8tPtv1bOO3eXP0dZt/6ACWeAfQOerTrF8mrR5Txmzu9Zn/bRR1rp5o4v/WxKfZSs5WYK53QGn/bfHZrkvHb1vfdt3mHwSg4AJgS7DEdd//oUY//rvys20wVwGU9g8CAAE4gkDZ/iWY9AaDALgSznm+tMLb7GcZfhn/KagrX1v+IACuhHOeL51gm/2swq/V+aTYRI8ERGf1s6bv/vcT67HG2uKHALgSznm+dIJt9rMIv7axdTYr/XeK+NR+1/Pd/37jffd4W/wQAFfCOc+XTrDNvmT4b/3a70vKmDpr3fMtst388N1/V762+CEAroRzni+dYJt9ifBHbuDj9VD7MqP0DYPc9Q/3yXf/Xfna4ocAuBLOeb50gm323cJndUqdhvYS/ZKiaDXFvCZW6jwi89ua9DI32wdn++6/KwNb/BAAV8I5z5dOsLR9aXy+++/Kp+jx2/zDcwCWCrABdFV4afuuBW6b77v/tvhs14sev80/CAAEwFbjPa/bCsxVIJ2cy2By0eO3+QcBgAA4bRNbgUEA2uwE2HGyLT8QAAiAU4nZCgwCAAFwKrC8J0sXuLR9aX6+++/Kp+jx2/xDB4AOwGkP2AoMHQA6AKcCy3uydIFL25fm57v/rnyKHr/NP3QA6ACc9oCtwNABoANwKrC8J0sXuLR9aX6+++/Kp+jx2/xDB4AOwGkP2AoMHQA6AKcCy3uydIFL25fm57v/rnyKHr/NP3QA6ACc9oCtwNABoANwKrC8J0sXuLR9aX6+++/Kp+jx2/xDB4AOwGkP2AoMHQA6AKcCy3uydIFL25fm57v/rnyKHr/NP3QA6ACc9oCtwNABoANwKrC8J0sXuLR9aX6+++/Kp+jx2/xDB4AOwGkP2AoMHQA6AKcCy3uydIFL25fm57v/rnyKHr/NP3QA6ACc9oCtwNABoANwKrC8J0sXuLR9aX6+++/Kp+jx2/xDB4AOwGkP2AoMHQA6AKcCy3uydIFL25fm57v/rnyKHr/NP3QAtg5Ah0NUV61uw2rjiy0iXXEtJMwHgbQJKGNas9NDQ73sQgAs1KvtcGTnLWq+27DRscV51vqEtJMHeyDgSsBQ3JxvDI9CAFxIqvgDzcnhHd07gPZ/ENE5LktgLghIEFBET802KusgAA50FdEFs43Ko10FYCy6hzR/3mEJTAUBEQLKqLtnp8MrIQBOePm6ZqN6ZzcTI+PRZYr4b5yWwGQQECDArC6ZmwofhAC4wFX8YHOyekk3EyfU+T3KtHal9bZdF1cxFwQOETDG7NNhdVWzrvZAABzqwhDPzDcq7yNSXV/xdOJYdK/WfLnDMpgKAukSUOqe5mS43mYUpwA2QkRkmNfOT1Wf7voxYGLhgypWPyOtwwTmMAQEhAmYtjL84dnp4RdtC0EAbIQ61xV9qzlZubbX0NHx9u1M9OUk5jAGBIQJ3NZsVG5KsgYEIAklMvMVXX3/a3W1r9vwkzfwssUV0VOk6VcTmcQgEBAgEBNte48OP/FKXS0kMQ8BSEKpM4b52uZU9Vu9htcmFk43TI9rCmpJzWIcCKRFoPN9VajNJ2fqwy8ltQkBSEqKaKfeF54xc7va21ME6q01JqJHtVY9n8BKvixGgoCdQGfzB0znz05Vt9lHvzMCAtAPLaJbmo3KzbYpo2MLpxkKtihNZ9rG4joIuBKIDW2thPHv9XPnP7QmBKAv+qatFJ07Ozn0X7Zpp9R5eA9Hk4rNBvxYyEYL1wcjYNqk9NdXvh7Wd2xSi4PYgAD0T+050uG5tgcsDpntdAMchDeZOL5Ua728/+UwAwSOJGDI7A1McL8Ko1sHuesfbg0CMEh1MW1pbg8vpodUnHR6rc7HkYkvVIrOI+Y1McWnBkatZK2rSW1gXPkIdH7SG2t+Q1HwklZqGzM9Rjp4OOkNyEYMAmAj1OU6k/r2XCP4Yq8nBAc0jWkgkBkBCIAD6gMi8PPg6n46AYflMBUEUicAAXBH+ve8EP7p3FfVbndTsAAC2RKAAKTBm+l5CvhzzXp1axrmYAMEsiIAAUiLtDERB/obSoVTaX1Bk5ZrsAMC3QhAAFKvDfO/TOprwb7KZttTg6kvDYMg0CcBCECfwJIOV8a8zkHwoCZz34yqPEV1ZZLOxTgQyIoABCAD0p3/zqop+DcmfkIp/RxTtCNsDf1ieIh2v1KnRRwlZpAELHFMAhAAFEahCdhebFFo5z1wDgLgQZLK7CIEQDb7EABZvrDuSAAC4AjQMh0CIMsX1h0JQAAcAUIAZAHCuiwBCIAsX3QAsnxh3ZEABMARIDoAWYCwLksAAiDLFx2ALF9YdyQAAXAEiA5AFiCsyxKAAMjyRQcgyxfWHQlAABwBogOQBQjrsgQgALJ80QHI8oV1RwIQAEeA6ABkAcK6LAEIgCxfdACyfGHdkQAEwBEgOgBZgLAuSwACIMsXHYAsX1h3JAABcAQo3wEstvDqK9kklde6aTcbQ3hximABOHcAI2MLTaWDEUEfYbqkBNjEc3PTw3jVumD+nQVgdLz9JBOtFfQRpktKQBE9NduorCtp+JmE7S4AE9F3mPnKTLzFIqUioIy6e3Y6RG0JZt1ZAGobo4tJ8fcFfYTpkhJgVpfMTYUPljT8TMJ2F4A6H2ei1gxefZ1JvkqziCGzXy1UV+GVa7IpdxaAjnujY9Fm1rxe1lVYLxUBpe5tToafL1XMOQSbkgAsnMaktpPWYQ4xYMmlRsCYiIg/0pwefmGphVa0eFIRgE5QIxPt2xTTDUULEP74R4AV3T43WfkL/zz3z+PUBODkDbxsYUX0Y6XpTP8wwOOiEDBEzy7bHa579Ztqf1F8Wsp+pCYAB78LWDgtJv241mp0KUNDbDIEOg/+KKJPovWX4Xssq6kKQGeBWr21xhjzQ00BnuDKLo/er9TZ/BTq8+fq1Z94H4xHAaQuAAdEYGLhdMPBFk30MY9YwNW8CBj6KVF8Ee782SdARAA6YRz4TuD4aFIZswGnA9kn1osVjYk40HdUVTjxWl3t88LnJeakmAAc4lQbW/gABeFNhuNLNOllS4wfwhmAQOchH62CByiObsVdfwCAKU4RF4BDvo7cwMfTUHyhZjqPNJ9lTLxaabUSPyVOMZuFNGXabPgNrYOXyKhtRtFjtBg8jCf8ipGszASgGOHCCxAAgcMJQABQDyBQYgIQgBInH6GDAAQANQACJSYAAShx8hE6CEAAUAMgUGICEIASJx+hgwAEADUAAiUmAAEocfIROghAAFADIFBiAhCAEicfoYMABAA1AAIlJgABKHHyEToIQABQAyBQYgIQgBInH6GDAAQANQACJSbw/8pd0rWDTC0fAAAAAElFTkSuQmCC'),
(360, 'Microsoft PowerPoint - Add-in file', 'application/vnd.ms-p', '.ppam', ''),
(361, 'Microsoft PowerPoint - Macro-Enabled Open XML Slide', 'application/vnd.ms-p', '.sldm', ''),
(362, 'Microsoft PowerPoint - Macro-Enabled Presentation File', 'application/vnd.ms-p', '.pptm', ''),
(363, 'Microsoft PowerPoint - Macro-Enabled Slide Show File', 'application/vnd.ms-p', '.ppsm', ''),
(364, 'Microsoft Project', 'application/vnd.ms-p', '.mpp', ''),
(365, 'Microsoft Publisher', 'application/x-mspubl', '.pub', ''),
(366, 'Microsoft Schedule+', 'application/x-mssche', '.scd', ''),
(367, 'Microsoft Silverlight', 'application/x-silver', '.xap', ''),
(368, 'Microsoft Trust UI Provider - Certificate Trust Link', 'application/vnd.ms-p', '.stl', ''),
(369, 'Microsoft Trust UI Provider - Security Catalog', 'application/vnd.ms-p', '.cat', ''),
(370, 'Microsoft Visio', 'application/vnd.visi', '.vsd', ''),
(371, 'Microsoft Windows Media', 'video/x-ms-wm', '.wm', ''),
(372, 'Microsoft Windows Media Audio', 'audio/x-ms-wma', '.wma', ''),
(373, 'Microsoft Windows Media Audio Redirector', 'audio/x-ms-wax', '.wax', ''),
(374, 'Microsoft Windows Media Audio/Video Playlist', 'video/x-ms-wmx', '.wmx', ''),
(375, 'Microsoft Windows Media Player Download Package', 'application/x-ms-wmd', '.wmd', ''),
(376, 'Microsoft Windows Media Player Playlist', 'application/vnd.ms-w', '.wpl', ''),
(377, 'Microsoft Windows Media Player Skin Package', 'application/x-ms-wmz', '.wmz', ''),
(378, 'Microsoft Windows Media Video', 'video/x-ms-wmv', '.wmv', ''),
(379, 'Microsoft Windows Media Video Playlist', 'video/x-ms-wvx', '.wvx', ''),
(380, 'Microsoft Windows Metafile', 'application/x-msmeta', '.wmf', ''),
(381, 'Microsoft Windows Terminal Services', 'application/x-msterm', '.trm', ''),
(382, 'Microsoft Word', 'application/msword', '.doc', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAcmElEQVR4Xu2df5AlVXXHz7nd780sy49E5s0isQwsqKgoi/JL0KQkVWKFSkXRYJAkhiiLaEAJRhDmx5s3g4CCgBsVVjSkTK2kMJtKCizFElNGESEqKxp3AcFUsbgz780a2B/MvNd9T6pnWVxgX99+7/bt1z39nX/73vPjc+58+3Tfft1M+AMBECgtAS5t5kgcBECAIABYBCBQYgIQgBIXH6mDAAQAawAESkwAAlDi4iN1EIAAYA2AQIkJQABKXHykDgIQAKwBECgxAQhAiYuP1EEAAoA1AAIlJgABKHHxkToIQACwBkCgxAQgACUuPlIHAQgA1gAIlJgABKDExUfqIAABwBoAgRITgACUuPhIHQQgAFgDIFBiAhCAEhcfqYMABABrAARKTAACUOLiI3UQgABgDYBAiQlAAEpcfKQOApkJQK0uB5IOz2Sm00lkjdbhalZ8CJGqoAz9E2g2KrE1rE20L2o2quv694CZy5mAcwEYHVs4isi/LKTwXKXUAcsZ5iByMwvAYiDk/Wmr4d81iPjgM98EnAnAEXUZ3iXBJIX6Y6SUn28MxY3OLAAd0VrvVD6/pVmvPljcTBG5CwJOBGBVfWF1R3sbPaLjXAQNm78lkEQAnh29VYf+yfNX8VbwA4G9BFIXgFXji68LmL+liFcBs3sCPQhAFMxPSPl/0KzzTveRwUMRCKQqANGZP9DqXvzzZ1f6HgWASPSdzc3Vd9AdHGYXJTzllUBqAhBd8z+tg/vQ9mdb6p4FgIg08br5hn9xtpHCWx4JpCYAtYnONUR0WR6TXM4x9SMAe3jIxdgeXM4rI1luqQhAtNUnxJtxtz8Z9DRH9S8AOsT2YJqVKKatlAQgWC9Kzi8mgmJH3b8AEGF7sNi1TyN6awGInvDTQXsWD/mkUY7ebdgIwLPesD3YO/ZlM8NeACaC9xDJ7cuGSMESSUEAooyxPViwuqcVrrUAjE4Gt4jI2rQCgp3eCKQkANge7A37shltLwATnR8K0UnLhkjBEklNALA9WLDKpxOutQDUJhZbROrQdMKBlV4JpCkAe3xje7DXGhR5fBoC0MZPege3BNIXAGwPDq6a2XtOQQA6kn3Y8LiXQPoCgO3BMq0uCEDBq+1CAJ5Fgu3Bgq+NJOFDAJJQyvEYhwIQZY3twRzXPo3QIABpUBygDccCgO3BAdY2C9cQgCwoO/ThXACwPeiweoM3DQEYfA2sIshCALA9aFWiXE+GAOS6PObgshMAbA+aq1G8ERCA4tXseRFnJwDYHiz4Utlv+BCAglc1SwHA9mDBF8t+wocAFLymAxAAbA8WfM3sGz4EoODFHJAAYHuw4Otmb/gQgIIXcmACgO3Bgq+cPeFDAApexkEKALYHC754IADFL+DgBQDbg0VeRegAilw9Ihq8AGB7sMhLCAJQ5OrlRACwPVjcRQQBKG7tliLPQwewD0L8erBg6wkCULCCvTDcnAkAtgcLtp4gAAUrWO4FANuDhVpREIBClevFweauA3guRLxctAhLCwJQhCrFxJhfAcD2YBGWFgSgCFUqpABge7AISwsCUIQqFVQAsD2Y/8UFAch/jWIjzO8lwPPCxvZgTtcZBCCnhUkaVkEEANuDSQua8TgIQMbA03ZXGAHA9mDapU/FHgQgFYyDM1IkAdhDCduDg1stL/YMAchTNfqIpXgCgO3BPsrsbAoEwBnabAwXTwCwPZjNykjmBQKQjFNuRxVRALA9mJ/lBAHITy36iqTAAhDli+3Bvqqe3iQIQHosB2Kp4AKA7cGBrJrfOoUADLgAtu6byh+iOre72RkdW1wUpaq2flzO18Tr5hv+xS59wPb+CUAACr4yKsqvPVnnVrc0ahOdzUT0qvynie3BQdQIAjAI6in61CInzU9XH+gqAJPB50jkQym6dGQK24OOwMaahQAMgnqKPkXk/NZ09daulwCTi68XoQeJlHWtUwx7v6a01juVz29p1qsPuvYF+3sIWC+K2kRHAHNwBJhlw9xU9dy4CEYngpuF5ILBRdmT56069E+ev4q39jQLg/siAAHoC1ueJukdQzuqq564gZ/pFtURdRnepYNvENEf5inymFiwPZhRoSAAGYF26kbkvOZ09bY4Hy+7RFa0V4Y3iQo/UITLARJ9Z3Nz9R10B4dO2ZXcOARgOSwAoYebnv9aqnNgSqc23j6eWV0gJKez1r+f5y1CbA+aqml/HAJgzzAXFkTo0tZ05TO5CAZBFIYABKAwpYoPVJN+piJ04rbpoZ8vk5SQRgYEIAAZQM7QxSNexz9t29XczNAnXBWYAASgwMXrEvpPFPtvn53iueWXGjJKmwAEIG2iObCnQ37UV+FZs9NDD+UgHISQYwIQgBwXxyY0rfWCp9TEnPJvivuxkI0PzC0+AQhA8WsYm4Em/iWTvtbbXdkwex3vWubpIr0eCUAAegRW1OGa9C5F6usscg8RbxLPf3wl0f/9qk6LRIzHuYtaWMu4IQCWADHdLQHTC0/cel/+1iEAy7/Ghc4QAuC2fBAAt3xh3ZIABMASoGE6BMAtX1i3JAABsAQIAXALENbdEoAAuOWLDsAtX1i3JAABsASIDsAtQFh3SwAC4JYvOgC3fGHdkgAEwBIgOgC3AGHdLQEIgFu+6ADc8oV1SwIQAEuA6ADcAoR1twQgAG75ogNwyxfWLQlAACwBogNwCxDW3RKAALjliw7ALV9YtyQAAbAEiA7ALUBYd0sAAuCWLzoAt3xh3ZIABMASIDoAtwCTWNcUNhWr74imHzCrLaSDR6vh0PYVO2nno+t4MYkNjAEBFwTQAbigumRT/4bI26BEf2V2unI/3rrjDDQMWxCAAFjA299UrfU2ZvVp9vz1zTrvTNk8zIFAqgQgAGnh1DoQT91Iz/iN1qd4R1pmYQcEXBKAAKRAd+nNu0qf3apXf5yCOZgAgcwIQAAsUbPQfwSe/5fb6/y0pSlMB4HMCUAAbJAz39pk78Ikn+W2cYO5IOCKAASgX7LRP/+UtxZ39/sFiHl5IAAB6KMKQvTvLeW/u5cz/+9eJodUquGfiKK3kshxrPWRouggIlXpIwRMKQ0B3SGip4nU48T8oGj6Ni16d6V1oxkC0PtCeiRU/glJr/lHxhZeJZ5/OYXhnyulhnt3hxkg8HwCWuvdiryvkhdc05waftSGDwSgF3paB+Tzic169UHTtMPrckCggxkhfTGR8kzjcRwEeiegO0LqxuEd/uQTN/Azvc8nggD0Ru3aZqNyuWlKdNZX7G0UpteYxuI4CNgS0EQPKQ7P6qcbgAAkpa/pSbXgv9L0hd2R8fYbSfQ3WHkjSU1jHAjYElj6vYmoM5rT1Z/0YgsCkJCWiFzSmq7eGDc8OvMT0ffwz58QKoalSmBJBDSd1pwZfiSpYQhAAlKs9XZeqL487uy/6mOyUh8QPEBEr05gEkNAwAmB6HJgxQ7/5KT3BCAACcqgidfNN/yL44bWJjpRd/CRBOYwBAScEmCi6+calY8lcQIBSEBJi5w0P12Nzu77/Tu0vniM0vQz3O1PABND3BNY+mGavLY1NfywyRkEwEBIk8zONyovjXvirzbZ/icS/isTbBwHgawIaObb5qf880z+IAAGQqzlq3Mz1fd2GxY94acq7W14yMe01HA8SwKa9DOiqoeZHliDABirIh9tNqo3dRs2Oh78hbB8xWgGA0AgYwIifG5r2t8Q5xYCYOoAiN4+16h8s+v1/1jwZaXE2GplXHu4AwEizV9qzvgfgABYLAbW4dFzM8O/7GaiNtb5ESl6g4ULTAUBVwQeaDYqJ0EALPBWO/7I1qt5vuslwNjivCj1EgsXmAoCTgiIDlutmeEaBMACb1P5Q1TndtcOYGKxjZ/0WgDGVIcEdKfZGKpCACwQmz5MUZvoiIV5Mtm3sb00ty7VwxbpEPE7q0OmNcx0uhY5U5FaaW27JwN6B2u+Uyv6jjBtqrQrj28boqfixDVX8Wv9NHt8p9b0HSHaNORVHn8y+p1+zMlhb/yHEx28KJ3VrGkNK/qjLPmb1hduAhoWsQlg7gVgP/nV6nKghJ33aq0u8zxZ3dP/ca+DhR4mkmsrXuX2J+u8u9fp+xufafxEW6L4h3ZUbk/6eK0px+ix8XBF59ws+JvWLwSghALwXMp1qdZ0cKkmPalIDZkWbi/Ho31oJr6ypSrrenlzUi8+ou7GWfxa72bmK1ubK+voDg57iivpYIfx7w0BApC0GF3GmQAWsQN4Yaq18fbxWtNG5fERlrj2TBd6OPT0O7fXh/4nFXsGI6nHT7RFa/3O+ZmhX2QSf729RrTayCRHpu3PtH7RAZS5A9gn95Er5KWsgrtJ0bFWi1DTjyu+f8aTdW5Z2elxcmrxE/2o2vHPiNv56TG0RMNrdTmMguBb1vxf4A0CkAh/90EmgMuhA9ibffRPRH74/b7PREIPVzz/tKz/+VOLn2hLteOflvU//77xC3fuTa0TIzLeZEYHgA7geQSidppF7hOlYrePXogtelGl+HRiVm1/t7L1HT/pXaTpxKza/m7xj9TbbxAt96Z1T8Z0AoMAQABeRKA23rmCmK7qpXkSkr9rNao39DLH1di+4k/wxidX8b7Qbm2icyURzaThDwJgSdEE0PUlgK191rotSv0vE9+jlV6f5PuFR18kQ785uLO5h1Z0S1P5xya52z8y2T5BhWptKPRWj8OXmzqNTOIX2tzc7B+b5G5/FvFHuxuHdjpbeuDfdZWb1i86gJx3ALYC8KL0NH9paJd3kWlPe3S8/UFh/kIy/ZS/aTaq/xg3Ntr7DlaG/6BE/jqZzS6jXMQvcl5zunpbruKfaF8oxJ+3YoV7ALb4zDdRbP9BTQpta39/BIToe8M7/LfFicDIx+UgGW7PKlIr4inqHRVVPSzuIZ9n35f4LSJ6k31Fol3GFOPX+umhXdXD4ljkOn7LExg6AEuAtv+ggxCApZSXvm3onx+X/qETna8ponfFjTG9MCWaWxtr30aK35fGP/9zNtKKn2XD3FT13LjYBhV/baKzkYjeacPNtL4gAGUVANJCwm+Me4/86Hj7Q8L8uThEQrK21ah+sduY6JqZhbu+T7H/xZ1S/CLnt6art+Yx/tp4+2+JeV3/jMwdLASgtAJAJMw3t6b8C7shGK13ThVN349DpFlOnp+q3t/VxmRwi4istVnE3eamEr/hha+jA4y/Nt55CzF914YdOgAbegluohT2EmAPly3NRuWYboiWnk7Twa/jEHodf3Tb1dzsamOiE72Z9hWWZeg23Tr+ivJrcQ8u1QYY/6FXyu8pL3jChh0EwIbeMheAaIttbmao64+AjqjL8C4dxH500vS+hNGxxUXTVl+/JSp6/Jr04nxjqOsXo5+9+bizXz7RPAiADb0EAAveARgWiHBtItBxCE0LzJaPqXzx/vMfvyk/2+Om+uAeQInvASQ5Q5j+gU0LzDTf9QI3+R90/Lb5m+ab8oMAQABi10De/4FMCzzv8Zv+gW2Pm/hAACAAEICYNWASENt/UNfzIQCWhE0AbReIa/um9G392843xWc6buvfdr4pvkEfN+WHDgAdADoAdAD965TtGbB/z9nMNCmobf6u7Zso2fq3nW+Kz3Tc1r/tfFN8gz5uyg8dADoAdADoAPrXKdszYP+es5lpUlDb/F3bN1Gy9W873xSf6bitf9v5pvgGfdyUHzoAdADoANAB9K9TtmfA/j1nM9OkoLb5u7ZvomTr33a+KT7TcVv/tvNN8Q36uCk/dADoANABoAPoX6dsz4D9e85mpklBbfN3bd9Eyda/7XxTfKbjtv5t55viG/RxU37oANABoANAB9C/TtmeAfv3nM1Mk4La5u/avomSrX/b+ab4TMdt/dvON8U36OOm/NABoANAB4AOoH+dsj0D9u85m5kmBbXN37V9EyVb/7bzTfGZjtv6t51vim/Qx035oQNAB4AOAB1A/zplewbs33M2M00Kapu/a/smSrb+beeb4jMdt/VvO98U36CPm/JDB4AOAB0AOoD+dcr2DNi/52xmmhTUNn/X9k2UbP3bzjfFZzpu6992vim+QR835YcOAB0AOgB0AP3rlO0ZsH/P2cw0Kaht/q7tmyjZ+redb4rPdNzWv+18U3yDPm7KDx0AOoCYNZD/12rHL/D8x+9aICAAloRNAIvcAZg+TJH3D4Ms9/jxYRDLf940pi9nASChzc3pyqu7cUryaTDF/qrZKZ7rasPlp7VSiH+gnzYzxD8yKYezBFtt1rFp/eISoMyXAMyfb075H+6GIMnHQRXLm2anqvd1tTER3CwkF9gs4q5z04hf5JTZ6eoP8xh/rd55M2n6Lxt2EAAbesv602BaWPj4uenqpu6Lv32hEH8+DiGLXDg3Xb2525iRevsNrPlHlmXYz/R04heSD7Ya1VtyGX+Cz7ObuEIATIRK2gGYPq0dYTl0vHOHYnp3HCIt9LX56cqfxY2pjQW3kpL3W5biedPTip+I7mg2KmfnMv6JztcU0btsuEEAbOgt3w7guyuVf8av6rwQc/1/oA7as0qpA2IFQOvdyq+uata561dsX3aJrFg4KLibid5sWY6909OLn/Quf3d11ex1vKtbbHmO38QTAmAiVKoOQAuTt/4A5X007p8/QjIy3l7LzF1b432xmdroaOzhdTmgE4SftesEHMUvckFruro+bilkHv9E+wIm7npplXRZQwCSkuoyzgQw79uA0VaZEvU4Kb6HtV4fd83/HIK6VEd0uJlJjkyCT4gfbynvGKpz2zQ+uidAos5nkbey1keKUtXYDiOD+MOQH9ve9I6h9dwpYvxxMZvWL3YBct4BmBaki+O1ic4niOiTPdkW+kRzunJNT3McDe4rfqLLm43KtY5C6slsbbxzBTFd1dOkPk9gEAAIwPMI1OrtNVrLfYrUUC8LcKnTUHxKs159sJd5aY/tO36tFzzmUxJ1SGkHvY+92nj7eM3yg175dwsJHYBlsUwAXV8CWIbf0/TRK2RVwOG9niere5r47ODoUoCVd2qzztv6mW87xzb+6FLAF+/UuU/yrG0s/cyPHrwSHd6b9NIriQ/T+kUHgA5giUC0+EIdfMMjOi7Jwuo2RhM9pJT/tqxFIK34RdNPlfbflrUIROKlVXA3K3q9Df8XzoUAWNI0AVwOHUDUdgqrf03rzLOnE9BnZXU5kHb8S52A0mdldTkQXbaIVhvT4r/vkjetX3QAZe4A6lKt6eBS1rpuuhvfq44u3RMgNdVU/vVJdgd6tb803mX8Wi8oVlPNWf/6JLsDNvFr0pNpXfOjA+irEt0nmRS0iB1ArS4Hiu6cS6Quc3HW2Zdm1A2Q6GvYq2yIe1iol7JlGX/UDShPX+PtrmyIe1gor/Gb1i86gGXdAQgfUaehXUS/w2FwJLGsCYVPJ9F/bHrCr5cFnWSs1no3sfq6R/JtIt5Eof/YAVV66ld1WiRi2b+NHMVPepci9XUh+bbHtImo8thB2+mpR9fxYlz+0U+qd7fpEPKC1YPgDwFIsjpjxjSVPxTXwtYmFttEqmLpBtNBIHUCrHV7bmYodjsXHYABe7Xjj2y9mue7DRsdW5wXpV6SevVgEAQsCWgKm/ON4dE4MxAAE2QOX9GcGn6027DaROe/ieiNJjM4DgJZE2Ci++calZMhABbkmejtc43KN7sKgIOfulqEi6kg8BwB1vzFuRl/LQTAalHIR5uN6k3dTIxMBOcyyT9bucBkEHBAQITPaU37t0MAbOCy3N6cqp7TzcRL6nIw6/Y2RWqFjRvMBYE0CUS7Lqb3NET+cA/AQF2TzM43Ki/tvlVFdOhY8GWl5Lw0CwhbIGBFgPnW5pR/vskGBMBEiIi0yEnz09UHul4GTC68kkP+OSnlJzCHISDgmIDusJZXz80M/9LkCAJgIrSnT/psc6rykbihoxOd64To0iTmMAYEHBO4ttmoXJ7EBwQgCSXS8xVVffmTdd7dbXj03rjFlcH9pOjYRCYxCAQcEAiJNh2s/FNMr3zb6xoCkLQIIh9pTlc/Gze8NrlwtBa6V5FXS2oW40AgLQLR/Spf6VNn68OPJbUJAUhKimir2u2/yvSDkKU30gT0TaU49gms5G4xEgTMBKJ/fk/ojF5/wgwBMLPdd8TVzUblCtOU0bGFozR5G9N+uYPJL46Xk0Co6cGKH76rlzM/LgH6Wiu6w0wnzE0N/dQ0PfoV2E4Jplj0JfixkIkWjvdHQHeI1fWHbPfrpl8ldrOPDqB38ltI+Sck/W171A2I51+uw/C9Wf8Et/fUMKMIBDTpXZ72NrAfXNPPWX/fHCEA/VRcaGNzs3823cFh0unRSyxIh2cy0+kksiak8EhP8yFpv4knaTwYVwwC0U96QyVPMXmPKeZNInQPKe+upCcgU5YQABOhLseF+AuthvfhuCcE+zSNaSCQGQEIgAXqJRH4hXdRL52AhTtMBYHUCUAA7JH+myz472t9infYm4IFEMiWAAQgDd5CD5Mn78nqNdhphAwbIBARgACktQ60DsRTn2H2p9O6QZNWaLADAt0IQABSXxv610L8aW93Zb3pqcHUXcMgCPRIAALQI7Ckw1nr7eJ5tyvSX5nlyv1UZ510LsaBQFYEIAAZkI7ezqrI+08h+QGz2iIUPOq3h34zPEQ74t+Ln0FwcFFqAhCAUpc//8mbPmyR/wzyHSEEIN/1KX10EAC3SwAC4JYvrFsSgABYAjRMhwC45QvrlgQgAJYAIQBuAcK6WwIQALd80QG45QvrlgQgAJYA0QG4BQjrbglAANzyRQfgli+sWxKAAFgCRAfgFiCsuyUAAXDLFx2AW76wbkkAAmAJEB2AW4Cw7pYABMAtX3QAbvnCuiUBCIAlQHQAbgHCulsCEAC3fNEBuOUL65YEIACWANEBuAUI624JQADc8kUH4JYvrFsSgABYAnTfASy28ekrt0Uqr3XdaTaGquXN333m1h3AyNhCk5U34j5UeCgbAdFhqzUzjE+tOyy8tQCMTnR+KEQnOYwRpktKgInun2tUTi5p+pmkbS8Ak8EtIrI2k2jhpFQEWPMX52Z8rC2HVbcWgNp4cDax/IvDGGG6pARE+JzWtH97SdPPJG17AajLgTpoz+LT15nUqzRONOlneKG6Cp9cc1tyawGIwhsdC9aLkvPdhgrrpSLA/OXmlP/+UuU8gGRTEoCFo4R4MynlDyAHuFxuBLQOiOQ1zZnhR5ZbannLJxUBiJIamexcy0Ifz1uCiKd4BITputZU5e+LF3nxIk5NAF52iaxYWBncx4peXzwMiDgvBDTRQyt2+Cc/cQM/k5eYlnMcqQnAnnsBC0eFpO5VikeXMzTk5oZA9OAPE52K1t8N3/1ZTVUAIge1enuN1vpuRR6e4MqujoX3FP3zk6/OaNWrPy58MgVKIHUBWBKByYWjtXgbFdHrCsQCoQ6KgKafEYVn4cyffQGcCECUxtI9gYOCKdb6EuwOZF/YQnjUOhBP3Vhlf/LJOu8uRMzLLEhnArCXU21s4RXk+ZdrCc9RpFYsM35Ipw8C0UM+ir2vUhhcg7N+HwBTnOJcAPbGOvJxOYiGwjOV0Omk5Ditw9Ws+BD8lDjFaubSlO6IlqeU8h4jzZs00z206N2FJ/zyUazMBCAf6SIKEACBfQlAALAeQKDEBCAAJS4+UgcBCADWAAiUmAAEoMTFR+ogAAHAGgCBEhOAAJS4+EgdBCAAWAMgUGICEIASFx+pgwAEAGsABEpMAAJQ4uIjdRCAAGANgECJCUAASlx8pA4CEACsARAoMQEIQImLj9RBAAKANQACJSbw/5ploeJU7T+rAAAAAElFTkSuQmCC'),
(383, 'Microsoft Wordpad', 'application/x-mswrit', '.wri', ''),
(384, 'Microsoft Works', 'application/vnd.ms-w', '.wps', ''),
(385, 'Microsoft XAML Browser Application', 'application/x-ms-xba', '.xbap', ''),
(386, 'Microsoft XML Paper Specification', 'application/vnd.ms-x', '.xps', ''),
(387, 'MIDI - Musical Instrument Digital Interface', 'audio/midi', '.mid', ''),
(388, 'MiniPay', 'application/vnd.ibm.', '.mpy', ''),
(389, 'MO:DCA-P', 'application/vnd.ibm.', '.afp', ''),
(390, 'Mobile Information Device Profile', 'application/vnd.jcp.', '.rms', ''),
(391, 'MobileTV', 'application/vnd.tmob', '.tmo', ''),
(392, 'Mobipocket', 'application/x-mobipo', '.prc', ''),
(393, 'Mobius Management Systems - Basket file', 'application/vnd.mobi', '.mbk', ''),
(394, 'Mobius Management Systems - Distribution Database', 'application/vnd.mobi', '.dis', ''),
(395, 'Mobius Management Systems - Policy Definition Language File', 'application/vnd.mobi', '.plc', ''),
(396, 'Mobius Management Systems - Query File', 'application/vnd.mobi', '.mqy', ''),
(397, 'Mobius Management Systems - Script Language', 'application/vnd.mobi', '.msl', ''),
(398, 'Mobius Management Systems - Topic Index File', 'application/vnd.mobi', '.txf', ''),
(399, 'Mobius Management Systems - UniversalArchive', 'application/vnd.mobi', '.daf', '');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(400, 'mod_fly / fly.cgi', 'text/vnd.fly', '.fly', ''),
(401, 'Mophun Certificate', 'application/vnd.moph', '.mpc', ''),
(402, 'Mophun VM', 'application/vnd.moph', '.mpn', ''),
(403, 'Motion JPEG 2000', 'video/mj2', '.mj2', ''),
(404, 'MPEG Audio', 'audio/mpeg', '.mpga', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAdVElEQVR4Xu2df5AlVXXHz7nd783sLrAR5s2ulGVgwd8KqwLrz6j7h1hSqVgkQQGTaIAlxgIFFYg1P968WQtWUUCSCCsSLRPAUJJKSiyEEk3KAgF/sEFlgQVJFZCdeTMY3F/z3uu+J9Wzu7jgvL7d7/Z9r3v6+/7te88993PO/fZ5ffsHE34gAAKlJcClnTkmDgIgQBAAJAEIlJgABKDEwcfUQQACgBwAgRITgACUOPiYOghAAJADIFBiAhCAEgcfUwcBCAByAARKTAACUOLgY+ogAAFADoBAiQlAAEocfEwdBCAAyAEQKDEBCECJg4+pgwAEADkAAiUmAAEocfAxdRCAACAHQKDEBCAAJQ4+pg4CEADkAAiUmAAEoMTBx9RBAAKAHACBEhOAAJQ4+Jg6CEAAkAMgUGICEIASBx9TB4G+CUCtLoeRDk9jpo0ksl7rcB0rXk2kKghD7wSajUpsDGsT7Quajeq1vY+AnsuZgHMBGB1bOI7IvzSk8Gyl1MrlDHMQczMLQCsQ8v5kruHfPgj/MGa+CTgTgGPqMrxHgkkK9adJKT/fGIrrnVkAOqK13q18fmezXn2wuDOF5y4IOBGANfWFdR3t3eYRnejCadj8HYEkAnCg9dM69DfMf46fBj8QOEggcwFYM956Q8B8lyJeA8zuCaQQgMiZn5Py/6hZ593uPcMIRSCQqQBEZ/5Aq3uw+PsX+pQCQCT6O83t1Q/QrRz2z0uMlFcCmQlA9J//tzr4Mcr+/oY6tQAQkSa+dr7hX9hfTzFaHglkJgC1ic4VRHRpHie5nH3qRQD285ALsT24nDMj2dwyEYBoq0+It+NqfzLoWbbqXQB0iO3BLCNRTFsZCUCwVZScV0wExfa6dwEgwvZgsWOfhffWAhDd4aeD9gxu8skiHOlt2AjAgdGwPZge+7LpYS8AE8EHieSWZUOkYBPJQACiGWN7sGBxz8pdawEYnQyuF5FNWTkEO+kIZCQA2B5Mh33ZtLYXgInOfUJ0yrIhUrCJZCYA2B4sWOSzcddaAGoTrTkidVQ27sBKWgJZCsD+sbE9mDYGRW6fhQC08Ujv4FIgewHA9uDgotn/kTMQgI70322MeJBA9gKA7cEyZRcEoODRdiEAB5Bge7DguZHEfQhAEko5buNQAKJZY3swx7HPwjUIQBYUB2jDsQBge3CAse3H0BCAflB2OIZzAcD2oMPoDd40BGDwMbDyoB8CgO1BqxDlujMEINfhMTvXPwHA9qA5GsVrAQEoXsxe4HH/BADbgwVPlSXdhwAUPKr9FABsDxY8WZZwHwJQ8JgOQACwPVjwnDnUfQhAwYM5IAHA9mDB8+ag+xCAggdyYAKA7cGCZ85+9yEABQ/jIAUA24MFTx4IQPEDOHgBwPZgkbMIFUCRo0dEgxcAbA8WOYUgAEWOXk4EANuDxU0iCEBxY7foeR4qgEMQ4unBguUTBKBgAXuxuzkTAGwPFiyfIAAFC1juBQDbg4XKKAhAocL1+87mrgJ43kW8XLQIqQUBKEKUYnzMrwBge7AIqQUBKEKUCikA2B4sQmpBAIoQpYIKALYH859cEID8xyjWw/z+BXiB29gezGmeQQByGpikbhVEALA9mDSgfW4HAegz8KyHK4wAYHsw69BnYg8CkAnGwRkpkgDsp4TtwcFly++PDAHIUzR68KV4AoDtwR7C7KwLBMAZ2v4YLp4AYHuwP5mRbBQIQDJOuW1VRAHA9mB+0gkCkJ9Y9ORJgQUgmi+2B3uKenadIADZsRyIpYILALYHB5I1vxsUAjDgANgO31T+ENW53c3O6FirJUpVbcdx2V8TXzvf8C90OQZsL00AAlDwzKgov/ZMnee6TaM20dlORK/K/zSxPTiIGEEABkE9wzG1yCnz09UHugrAZPAPJPK3GQ7pyBS2Bx2BjTULARgE9QzHFJHz5qarN3T9CzDZOkGEHiRS1rHO0O0lTWmtdyuf39msVx90PRbs7ydgnRS1iY4A5uAIMMtNs1PVs+M8GJ0IrhOS8wfnZaqRn9ahv2H+c/x0ql5o3BMBCEBP2PLUSe8a2lVd89RVvK+bV8fUZXiPDu4gonflyfMYX7A92KdAQQD6BNrpMCIfbU5Xvx43xssukhXtVeE1osJzi/B3gER/p7m9+gG6lUOn7EpuHAKwHBJA6NGm57+O6hyYplMbb7+RWZ0vJBtZ6z/M8xYhtgdN0bQ/DgGwZ5gLCyL0qbnpypdy4QycKAwBCEBhQhXvqCa9ryJ08s7poV8ukylhGn0gAAHoA+Q+DvGY1/HfvvNybvZxTAxVYAIQgAIHr4vrP1fsv29mimeX39Qwo6wJQACyJpoDezrkHb4KT5+ZHnooB+7AhRwTgADkODg2rmmtFzylJmaVf03cw0I2Y6Bv8QlAAIofw9gZaOLHmfQWb2/lppkrec8yny6ml5IABCAlsKI216T3KFLfZZG7iXibeP6vVxH935N1ahExbucuamAt/YYAWAJEd7cETC88cTv68rcOAVj+MS70DCEAbsMHAXDLF9YtCUAALAEaukMA3PKFdUsCEABLgBAAtwBh3S0BCIBbvqgA3PKFdUsCEABLgKgA3AKEdbcEIABu+aICcMsX1i0JQAAsAaICcAsQ1t0SgAC45YsKwC1fWLckAAGwBIgKwC1AWHdLAALgli8qALd8Yd2SAATAEiAqALcAYd0tAQiAW76oANzyhXVLAhAAS4CoANwChHW3BCAAbvmiAnDLF9YtCUAALAGiAnALMIl1TWFTsfqBaLqXWT1COthRDYeeXbGbdu+4lltJbKANCLgggArABdVFm/o3RN5NSvQ3Z6Yr9+OtO85Aw7AFAQiABbylumqtdzKrL7Dnb23WeXfG5mEOBDIlAAHICqfWgXjqatrnN+Y+z7uyMgs7IOCSAAQgA7qLb95V+oy5evVnGZiDCRDoGwEIgCVqFvqPwPP/4tk6/9bSFLqDQN8JQABskDPf0GTvY0k+y20zDPqCgCsCEIBeyUaLf8rbhKv7vQJEvzwQgAD0EAUh+vc55f9ZmjP/Sy6V1ZVq+Mei6D0kciJrfawoOpxIVXpwAV1KQ0B3iOi3ROrXxPygaPo+tbzbs7rQDAFIn0iPhco/Kel//pGxhVeJ519GYfghpdRw+uHQAwReSEBrvVeRdzN5wRXNqeEdNnwgAGnoaR2Qzyc369UHTd2OrsvKQAebhfSFRMoztcdxEEhPQHeE1NXDu/zJp67ifen7E0EA0lHb0mxULjN1ic76ir3bhOm1prY4DgK2BDTRQ4rD03upBiAASelrekYt+K80fWF3ZLz9ZhJ9BytvJKlptAMBWwKLz5uIOrU5Xf15GlsQgIS0ROSiuenq1XHNozM/Ef0Iiz8hVDTLlMCiCGh6e3Pz8GNJDUMAEpBirZ/lherL487+az4tq/TK4AEiek0Ck2gCAk4IRH8HVuzyNyS9JgABSBAGTXztfMO/MK5pbaITVQefSGAOTUDAKQEm+uJso/LpJINAABJQ0iKnzE9Xo7P7kr+j6q1XK02/wNX+BDDRxD2BxQfT5HVzU8OPmgaDABgIaZKZ+UblpXF3/NUm298g4b80wcZxEOgXAc389fkp/6Om8SAABkKs5ebZzdWzujWL7vBTlfZO3ORjSjUc7ycBTXqfqOpa0w1rEABjVOSTzUb1mm7NRseDDwvLN41m0AAE+kxAhM+em/ZvihsWAmCqAIjeN9uofK/r//+x4EalxFhq9Tn2GA4EiDR/rbnZPxcCYJEMrMPjZzcPP97NRG2s81NS9CaLIdAVBFwReKDZqJwCAbDAW+34I09fzvNd/wKMteZFqSMthkBXEHBCQHQ4N7d5uAYBsMDbVP4Q1bndtQKYaLXxSK8FYHR1SEB3mo2hKgTAArHpwxS1iY5YmKcE9n9IRO+yGSOm7382G5V3x9k2zc/kv7XfdamubdFq8TvrQqb1zLRRi5ymSK2ytp3KgN7Fmr+jFf1AmLYNceWJZ6Ln9GNODovm61I9muiIlnTWsab1LLRRk5ymlDos1fA9NjbFBxcBDWBNAE0LxBQ3k/2ReufdrOkHJju9HI9eTjJXr0QC0/Vnmp/J/178MvWp1eUwCTtnaa0u9TxZZ2pvdVzoUSLZMrS7cnPS22tN40W3jYcrO2cJqUsVyXGm9jbHTfGBAORcACL3ahMdF1WA8ex/YOzYCseUYDbJa+xbl2otDC7WrOuK1JCxfYoG0Us3WPHYnKpcm+bNTymGINokldra4GKtdd3VfSSm+EAACiAALqqAJGf/3AvAgdiNjrdPDLS6LcNq4JFQ6dOfrQ/9KtWC7rHx6GTrhFC821xUAxCAHoNysJsJoKlENg1vsn+wf8ZVQKKzf1EEIPJz9LOyRqvgTlZ0gom54fhPqx3/1LidH0v7S3ZfMymjHQnu9IhOzNK+Kb9QARSgAohczLIKSHr2L5IAHBSBgMN7eq4EhLZXA/8d/V78B1MwEoFAwnuyrAQgAJZyagLYrwogw2sBic/+RROARREYb58YstyX9pqAJr2HNJ08v3noYcuUseq++HcgpPuyuiZgyl9UAAWpALKqAtKc/YsoAAd8vpSIrkizEpO88SmNPZu2o+OdS4Rpi42NpH9hIQAFEoAMqoBUZ/8sBMC2QmKt26LU/zDx3VrprYm+v1iX6pGd8OHEfwWEtje3+6+nWzk0LbqRyfZJKlSbQqH3eBy+XJSKv9GmF/83SeWoteHDWfwVQAVgimjBBMDmWkDas38eBOD3wqP5a0N7vAtMe/IjE+3zmHhrsvDzR5oN/xtxbaO9+2BV+PdK5CPJbHZpldT/8fa5zPxVq7GIjDeaoQIomABYVAGpz/65FAAiEqIfDe/y3xsnAtHNQlq3d5rvGNTPrVLVtU/WeaFbKhx43+NdRPRW2wUZ9U/i/6LgDC++Z8LqjkFUAJYRMwG0LXFN9pdyv5cqoJezf14FYJHJ4rcZ/fPiwlub6PwrEf15XBvW8i+zm6sfjmtz1GTwT9Zn/hcPkMT/yfa3SPgMmxQ25RcqgAJWAD1UAT2d/XMtAKSFhN8c9x780fH23wjzV+JDLOc2G9WvdWsTfeeBmX9iswiX7mv2f2SifT4TX2czNgTAhl6C/1CDqACiKaWpAno9++dbAIiE+bq5Kf9j3UI8Ot55qzDdE5cCwnLy3FS16wIfnQyuF5FNlmm0ZHeT/2sm22/RwvfajA0BsKGXYwFIUQX0fPbPuwAQ0SPNRuXVXQXgs7JG/GBnXAqY3vdQm+hEb9Z9hWUadese6390Y5CWYMZmbAiADb2cC0CSKsDm7J93AdCkW/ONoa5fXD7+Ahl67iVB14t70fxM73sYHWu1TFt9vaZYFv6bxoYAmAjl/BpAtMjjHtk1PCMQe/Y32S6DAFimh1V3kwBQXfyaDjo2g0AAbOjloAKIFnjcSzviqgDT2d9kO+8CQNENPNOVrp9iy6KEtkyf+O4G/4+uy0hHB00bHyAANvTyIQCSZCEv8dYg49k/etGIKUFMFzlt+1uFh/kfm1P+x7vZyOIimpV/ps4G/6O7Dlm46xepTOYX/+I0KrE7fdgGzPlfgAMLMNFiPnQqSUXDlCD5FQAtLPzG2enqtm4hzGIbLcki661NIv9T3M24tBem+EIAiiEAlHRBH5hOYsEwJUheBcC0hbb49yWDG2l6W9zmXgn9v5mEP2S21r2FKb4QgIIIABElXtRpxMKUIDkVgP9apfxTTbfvBivbM+ZbgW2WV899jf6/7CJZ0VrV3klKHdHzKPgLYINuf1/bBWLyII39hAs78rnrm35ffNEwzfhLzcW2v4nPC49rYfK2rlTeJ+MWf9RnJKOHadL5Z2qd3P/aePuvibnrHYqmkQ4eN8UHFUBxKoDIU2MVEDVKs21oSpBBVwDRVpkS9WtSfDdrvTXuP//zoaxL9Sgd/iqLx2mTLrRu7Xryf//LQn+ZxQ1IpvhCAIolAMZrAXHTWWrL0JQgtgJgu4B66V+b6KR+IUgv4ySpEHuxOzLR+RQTXdlL3xf3McUXAlAwATBVAXHTWeqmIVOCFE0Aen0lWK+LzcQvrd019dbrA033K1Ir0vbt5S8aBKB4AtBTFdDthiFTAhdJAKI3A1u9FLSHFWfil8bk2r+TWqcS3pvlXxeTfxCAAgpAL1VAt1uGTQlSFAGo1WVtqIM7sn6ttmkBm/iZ+h88Hi3+diW4K2v/Tf5BAIopAKmqgLjbhU0JUgQBqI233yisvs0kxyZdcFm1M/FLOk502+9CENzlKVqftE+Sdib/IAAFFYA0VUDcA0OmBMmzABx42u9i1rru6ok90yIz8TP1P/R4JAKtTniv8uT4NP3i2pr8gwAUVwASVQGmR4ZNCZJHARi5RA5XQ52zNatLBnHWPzRlTPzSLuS1463XdZgewEXAtOQctTcF2LRATG5Z2je+7MP0STHL8Y03SpnmH39c+Jg6De0h+gOmYB2Fsj5k3kik35/VArHzz3yjWC/2R8Y7FzPTF3vp++I+pviiAihwBdCPBDEJnCnBTP2zmEOebfT4XYPoPQDRh0mt30Rkig8EAAIQmwOmBWxKMFP/PC9eJ74l/C4AbgV2Qj+9UdcJ7tq+aca249v2N/m3HI8n+S4AHgbKSeRdJ7hr+yaMtuPb9jf5t2yPJ/suAB4HHnQCuE5w1/ZN/GzHt+1v8m/5Hk/wXYAMnmg0xQfXAHANANcABqQyppeC4JVgAwrMocOaFNT2Ipdr+yaEtuPb9jf5t8yPx34XAC8FzUH0XSe4a/smhLbj2/Y3+becj+O14AWIrusEd23fhNh2fNv+Jv9cH8eHQSwJ25bAlsM77+46wV3bNwGyHd+2v8k/18cryq89U+e5buM4/TSY4bsA0ROCYSWYtWFgig8uAuIiYKkvAmqRU+anq13fvT86EVwnJOfbLMKufU3fNRhvb9DMP7YZGwJgQy9nLwW1nMqS3U0JYqrwbPu7mFMamyJy3tx09YZufUbq7Tex5p+msZmsbYLvAoy3NzHz9cnsLd3KFB9UAKgASl0BEMstzanqmXFpUBsLbiAl59gsxBf3NW0BRu1rY+1bSPEHbcaFANjQQwVAy70CINK7hnZV1zx1Fe/rlirRbbkLhwd3MtE7LNPpYHfjdwGOrsvKVtCeUUodZjMmBMCGHgSgBAJARCLnNKerN8alSrQgO0H4ZbtKIMV3ASba5xBx178mSdMaApCUVJd2JoCmM6RpeNf2XY8/aP9N80t4/LGm8l9LdQ5M7aNrAiTqPBZ5D2t9rOlNRL1+F+CoWvirLN4MZIoPrgHgGkC5rwEciD8LfWZ2upLJu/hNImI6PjreuUSYtpjaJTkOAUhCKaaNCSAqgPjPT9vysQxf4u6a9D5faMPM9NBDiTs5aDg62TohDOk+pdRwFuZN+YsKABUAKoADOaBD3uH73ttnptjq5pteF+6aSRkNJLwH3wXolaCDfiYFtT3DubZvQmI7vm1/k3/9Ph5qerDi+af2WwQWP2qigjvwWvB+RxwVACqAF+WAJn7c4/D02amh/+5HOtbq7fVhR33b82Rd1uOZBBp/ASAAEIAlckBrveCxmpyd8a+irdzJemEu2qtLtRYGF2vWdUVqyMUYEABLqiaA+AuwPC4CdkuTqBpg0lu8vZWbZq7kPZbptNi9VpfDRHfO1qG6xMVZ/1AfTfmLCgAVACqABKtak96jSH1XSL7vMW0jqjxx+LP03I5ruRXX/Zi6DO9t02rygnXEsj4U3kii36+UWplgWOsmEABLhKbnxWsTrTaRqlgOg+4gkDmB6JsEs5uHYv9aoAIwYK92/JGnL+f5bs1Gx1rzotSRmUcPBkHAkoCmsDnfGB6NMwMBMEHm8BXNqeEd3ZrVJjo/IaI3m8zgOAj0mwAT3T/bqGyAAFiQZ6L3zTYq3+sqAA4eFbVwF11B4HkCrPmrs5v9TRAAq6SQTzYb1Wu6mRiZCM5mkn+2GgKdQcABARE+c27avwUCYAPX8MKII+tyBOv2zrx8rdZmqui7fAhorfcqv7qmWefdEACLuGqSmflG5aVELN3MHDUW3KiUfNRiGHQFgWwJJPj0WDQgLgImwG56ceTI5MIrOeRfklJ+AnNoAgKOCegOa3nN7Obhx00DQQBMhPbL5JebU5VPxDUdnehcKUSfSmIObUDAMYEtzUblsiRjQACSUCI9X1HVlz9T573dmu//nHNwPyl6fSKTaAQCDgiERNuOUP5bnqzzQhLzEIAklKI2Ip9oTle/HNe8NrlwvBa6R5FXS2oW7UAgKwLR9Spf6bfN1IefSGoTApCUFNHTaq//KtMDIdGjnTqg7ynFsXdgJR8WLUHATCBa/J7QqbPT1W3m1r9rAQFIQ4vo8maj8llTl9GxheM0ebexohNMbXEcBGwJLL7ExA//NM2Z/+CYEIBU9HWHmU5K8qKI6Cmw3RJMseiL8LBQKshonJiA7hCrL65+1q+bnkrsZhICkBj28w0fIeWfZLrB4mDrqBoQz79Mh+FZ/XoENP2U0KNIBKJHkz3t3cR+cEUvZ/1D5woB6CXyQrc1t/tn0K0cJu0evQSCdHgaM20kkfUhhcd6mleb3iuf1D7aLU8C0SO9oZLnmLwnFPM2EbqblHd70hOQiQoEwESoy3Eh/spcw/t43B2CPZpGNxDoGwEIgAXqRRF42LsgTSVgMRy6gkDmBCAA9kj/TRb8v5r7PO+yNwULINBfAhCALHgLPUqefLBZrz6YhTnYAIF+EYAAZEVa60A89SVmfzqrCzRZuQY7INCNAAQg89zQ/yvEX/D2Vraa7hrMfGgYBIGUBCAAKYElbc5aPyued4si/c0ZrtxPddZJ+6IdCPSLAASgD6Sjt7Mq8n4oJPcyq0eEgh1+e+g3w0O068k6tbCV2IcgYIglCUAAkBi5JmD6sEWunS+AcxCAAgSpzC5CANxGHwLgli+sWxKAAFgCNHSHALjlC+uWBCAAlgAhAG4BwrpbAhAAt3xRAbjlC+uWBCAAlgBRAbgFCOtuCUAA3PJFBeCWL6xbEoAAWAJEBeAWIKy7JQABcMsXFYBbvrBuSQACYAkQFYBbgLDulgAEwC1fVABu+cK6JQEIgCVAVABuAcK6WwIQALd8UQG45QvrlgQgAJYAUQG4BQjrbglAANzyRQXgli+sWxKAAFgCdF8BtNr49JXbIJXXuu40G0PV8s7f/cytK4CRsYUmK2/EvasYoWwERIdzc5uH8al1h4G3FoDRic59QnSKQx9huqQEmOj+2UZlQ0mn35dp2wvAZHC9iGzqi7cYpFQEWPNXZzf7yC2HUbcWgNp4cAaxfMuhjzBdUgIifObctH9LSaffl2nbC0BdDtNBewafvu5LvEoziCa9jxeqa/DJNbchtxaAyL3RsWCrKDnPrauwXioCzDc2p/xzSjXnAUw2IwFYOE6It5NS/gDmgCGXGwGtAyJ5bXPz8GPLbWp5m08mAhBNamSys4WFLsnbBOFP8QgI05VzU5XPFM/z4nmcmQC87CJZsbAq+DErOqF4GOBxXghooodW7PI3PHUV78uLT8vZj8wEYP+1gIXjQlL3KMWjyxka5uaGQHTjDxO9DaW/G75LWc1UAKIBavX2eq31nYo83MHVvzgWfqRo8ZOvTp2rV39W+MkUaAKZC8CiCEwuHK/Fu00RvaFALODqoAho+gVReDrO/P0PgBMBiKaxeE3g8GCKtb4IuwP9D2whRtQ6EE9dXWV/8pk67y2Ez8vMSWcCcJBTbWzhFeT5l2kJz1SkViwzfphODwSim3wUezdTGFyBs34PADPs4lwADvo6cokcTkPhaUpoIyk5UetwHStejUeJM4xmLk3pjmh5TinvCdK8TTPdTS3vdtzhl49g9U0A8jFdeAECIHAoAQgA8gEESkwAAlDi4GPqIAABQA6AQIkJQABKHHxMHQQgAMgBECgxAQhAiYOPqYMABAA5AAIlJgABKHHwMXUQgAAgB0CgxAQgACUOPqYOAhAA5AAIlJgABKDEwcfUQQACgBwAgRITgACUOPiYOghAAJADIFBiAv8PVG1Y4iXaNnwAAAAASUVORK5CYII='),
(405, 'MPEG Url', 'video/vnd.mpegurl', '.mxu', ''),
(406, 'MPEG Video', 'video/mpeg', '.mpeg', ''),
(407, 'MPEG-21', 'application/mp21', '.m21', ''),
(408, 'MPEG-4 Audio', 'audio/mp4', '.mp4a', ''),
(409, 'MPEG-4 Video', 'video/mp4', '.mp4', ''),
(410, 'MPEG4', 'application/mp4', '.mp4', ''),
(411, 'Multimedia Playlist Unicode', 'application/vnd.appl', '.m3u8', ''),
(412, 'MUsical Score Interpreted Code Invented for the ASCII designation of Notation', 'application/vnd.musi', '.mus', ''),
(413, 'Muvee Automatic Video Editing', 'application/vnd.muve', '.msty', ''),
(414, 'MXML', 'application/xv+xml', '.mxml', ''),
(415, 'N-Gage Game Data', 'application/vnd.noki', '.ngda', ''),
(416, 'N-Gage Game Installer', 'application/vnd.noki', '.n-ga', ''),
(417, 'Navigation Control file for XML (for ePub)', 'application/x-dtbncx', '.ncx', ''),
(418, 'Network Common Data Form (NetCDF)', 'application/x-netcdf', '.nc', ''),
(419, 'neuroLanguage', 'application/vnd.neur', '.nlu', ''),
(420, 'New Moon Liftoff/DNA', 'application/vnd.dna', '.dna', ''),
(421, 'NobleNet Directory', 'application/vnd.nobl', '.nnd', ''),
(422, 'NobleNet Sealer', 'application/vnd.nobl', '.nns', ''),
(423, 'NobleNet Web', 'application/vnd.nobl', '.nnw', ''),
(424, 'Nokia Radio Application - Preset', 'application/vnd.noki', '.rpst', ''),
(425, 'Nokia Radio Application - Preset', 'application/vnd.noki', '.rpss', ''),
(426, 'Notation3', 'text/n3', '.n3', ''),
(427, 'Novadigm''s RADIA and EDM products', 'application/vnd.nova', '.edm', ''),
(428, 'Novadigm''s RADIA and EDM products', 'application/vnd.nova', '.edx', ''),
(429, 'Novadigm''s RADIA and EDM products', 'application/vnd.nova', '.ext', ''),
(430, 'NpGraphIt', 'application/vnd.flog', '.gph', ''),
(431, 'Nuera ECELP 4800', 'audio/vnd.nuera.ecel', '.ecel', ''),
(432, 'Nuera ECELP 7470', 'audio/vnd.nuera.ecel', '.ecel', ''),
(433, 'Nuera ECELP 9600', 'audio/vnd.nuera.ecel', '.ecel', ''),
(434, 'Office Document Architecture', 'application/oda', '.oda', ''),
(435, 'Ogg', 'application/ogg', '.ogx', ''),
(436, 'Ogg Audio', 'audio/ogg', '.oga', ''),
(437, 'Ogg Video', 'video/ogg', '.ogv', ''),
(438, 'OMA Download Agents', 'application/vnd.oma.', '.dd2', ''),
(439, 'Open Document Text Web', 'application/vnd.oasi', '.oth', ''),
(440, 'Open eBook Publication Structure', 'application/oebps-pa', '.opf', ''),
(441, 'Open Financial Exchange', 'application/vnd.intu', '.qbo', ''),
(442, 'Open Office Extension', 'application/vnd.open', '.oxt', ''),
(443, 'Open Score Format', 'application/vnd.yama', '.osf', ''),
(444, 'Open Web Media Project - Audio', 'audio/webm', '.weba', ''),
(445, 'Open Web Media Project - Video', 'video/webm', '.webm', ''),
(446, 'OpenDocument Chart', 'application/vnd.oasi', '.odc', ''),
(447, 'OpenDocument Chart Template', 'application/vnd.oasi', '.otc', ''),
(448, 'OpenDocument Database', 'application/vnd.oasi', '.odb', ''),
(449, 'OpenDocument Formula', 'application/vnd.oasi', '.odf', ''),
(450, 'OpenDocument Formula Template', 'application/vnd.oasi', '.odft', ''),
(451, 'OpenDocument Graphics', 'application/vnd.oasi', '.odg', ''),
(452, 'OpenDocument Graphics Template', 'application/vnd.oasi', '.otg', ''),
(453, 'OpenDocument Image', 'application/vnd.oasi', '.odi', ''),
(454, 'OpenDocument Image Template', 'application/vnd.oasi', '.oti', ''),
(455, 'OpenDocument Presentation', 'application/vnd.oasi', '.odp', ''),
(456, 'OpenDocument Presentation Template', 'application/vnd.oasi', '.otp', ''),
(457, 'OpenDocument Spreadsheet', 'application/vnd.oasi', '.ods', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAe+klEQVR4Xu2de7AkVX3Hf7/TPX0vLI/I3rm7WpaygKKCLCgPBU0Uq4SCpGJQUSSJIbiLaEBeAcT7mDt3FVCQxwaFBQkpU4DBbCopsBAjWIkij4is+FjepEpw7525GNgH9850n1+q7+7iAjt9eub0mem+851/+5zf73c+v19/53Sf7tNM+IEACPQtAe7bkWPgIAACBAFAEYBAHxOAAPRx8jF0EIAAoAZAoI8JQAD6OPkYOghAAFADINDHBCAAfZx8DB0EIACoARDoYwIQgD5OPoYOAhAA1AAI9DEBCEAfJx9DBwEIAGoABPqYAASgj5OPoYMABAA1AAJ9TAAC0MfJx9BBAAKAGgCBPiYAAejj5GPoIAABQA2AQB8TgAD0cfIxdBCAAKAGQKCPCUAA+jj5GDoIQABQAyDQxwQgAH2cfAwdBLomAOWK7EY6Op6ZjiaRg7WO9mHFexKpEtLQOYFatZSYw/JY44xaNVjduQf0XMgEnAvA8MjsvkT+BRFFJyuldl3IMHsxNrMAzIVC3p/Xq/4dvYgPPvNNwJkA7F2Rwc0SjlOkzyOl/HxjKG50ZgFoitZ6k/L5/bVK8HBxR4rIXRBwIgBLKrP7NLW31iNa7iJo2PwDgTQCsK31szryj5j5Mj8LfiCwnUDmArBkdO6dIfMPFPESYHZPoA0BiIP5OSn/j2sV3uQ+MngoAoFMBSD+5w+1uhcnf/dS36YAEIm+vbY++AjdxlH3ooSnvBLITADia/4XdXgfpv3dTXXbAkBEmnj1TNU/s7uRwlseCWQmAOWx5iVEdEEeB7mQY+pEALbykDOxPLiQKyPd2DIRgHipT4jX425/OuhZtupcAHSE5cEsM1FMWxkJQLhGlKwoJoJiR925ABBhebDYuc8iemsBiJ/w02FjCg/5ZJGO9m3YCMA2b1gebB/7gulhLwBj4SeI5NYFQ6RgA8lAAOIRY3mwYHnPKlxrARgeD68TkZVZBQQ77RHISACwPNge9gXT2l4Axpr3C9HhC4ZIwQaSmQBgebBgmc8mXGsBKI/N1YnU4mzCgZV2CWQpAFt9Y3mw3RwUuX0WAtDAK729K4HsBQDLg73LZvc9ZyAATel+2PC4nUD2AoDlwX6qLghAwbPtQgC2IcHyYMFrI034EIA0lHLcxqEAxKPG8mCOc59FaBCALCj20IZjAcDyYA9z2w3XEIBuUHbow7kAYHnQYfZ6bxoC0PscWEXQDQHA8qBVinLdGQKQ6/SYg+ueAGB50JyN4rWAABQvZ6+IuHsCgOXBgpfKTsOHABQ8q90UACwPFrxYdhI+BKDgOe2BAGB5sOA1s2P4EICCJ7NHAoDlwYLXzfbwIQAFT2TPBADLgwWvnK3hQwAKnsZeCgCWBwtePBCA4iew9wKA5cEiVxFmAEXOHhH1XgCwPFjkEoIAFDl7OREALA8Wt4ggAMXN3XzkeZgB7IAQbw8WrJ4gAAVL2KvDzZkAYHmwYPUEAShYwnIvAFgeLFRFQQAKla7XBpu7GcDLIWJz0SKUFgSgCFlKiDG/AoDlwSKUFgSgCFkqpABgebAIpQUBKEKWCioAWB7Mf3FBAPKfo8QI83sJ8IqwsTyY0zqDAOQ0MWnDKogAYHkwbUK73A4C0GXgWbsrjABgeTDr1GdiDwKQCcbeGSmSAGylhOXB3lXLaz1DAPKUjQ5iKZ4AYHmwgzQ76wIBcIa2O4aLJwBYHuxOZaTzAgFIxym3rYooAFgezE85QQDyk4uOIimwAMTjxfJgR1nPrhMEIDuWPbFUcAHA8mBPquYPTiEAPU6Arfua8geowo1WdoZH5uZEqcDWj8v+mnj1TNU/06UP2N45AQhAwSujpPzycxWutxpGeay5noj2z/8wsTzYixxBAHpBPUOfWuTwmcngwZYCMB5eQyKfy9ClI1NYHnQENtEsBKAX1DP0KSIr6pPBDS0vAcbnDhKhh4mUda4zDHunprTWm5TP769Vgodd+4L9rQSsi6I81hTA7B0BZrl5eiI4OSmC4bHwWiE5rXdRtuX5WR35R8x8mZ9tqxcad0QAAtARtjx10hsHNgZLfnsFv9Qqqr0rMrhZh3cS0Z/kKfKEWLA82KVEQQC6BNqpG5FTapPBTUk+3ni27NJYFF0lKvpMES4HSPTttfXBR+g2jpyy63PjEICFUABCj9U8/wCqcGgaTnm0cQizOk1Ijmat35znJUIsD5qyaX8cAmDPMBcWROjc+mTp67kIBkEUhgAEoDCpSg5Uk36pJHTYhsmBXy2QIWEYXSAAAegC5C66eNxr+kdtuJhrXfQJVwUmAAEocPJahP5zxf6xUxM8vfCGhhFlTQACkDXRHNjTET/hq+iEqcmBR3IQDkLIMQEIQI6TYxOa1nrWU2psWvlXJb0sZOMDfYtPAAJQ/BwmjkATP8mkL/W2lG6euow3L/DhYnhtEoAAtAmsqM016c2K1PdY5G4iXiee//Qiov97pkJzRIzHuYuaWMu4IQCWANHdLQHThiduvS986xCAhZ/jQo8QAuA2fRAAt3xh3ZIABMASoKE7BMAtX1i3JAABsAQIAXALENbdEoAAuOWLGYBbvrBuSQACYAkQMwC3AGHdLQEIgFu+mAG45QvrlgQgAJYAMQNwCxDW3RKAALjlixmAW76wbkkAAmAJEDMAtwBh3S0BCIBbvpgBuOUL65YEIACWADEDcAsQ1t0SgAC45YsZgFu+sG5JAAJgCRAzALcAYd0tAQiAW76YAbjlC+uWBCAAlgAxA3ALMI11TVFNsbpHNP2UWT1KOnwiiAae32UTbXpiNc+lsYE2IOCCAGYALqjO29S/J/JuVqK/PTVZegC77jgDDcMWBCAAFvB21lVrvYFZfY09f02twpsyNg9zIJApAQhAVji1DsVTV9JLfrX+Vd6YlVnYAQGXBCAAGdCd33lX6RPrleChDMzBBAh0jQAEwBI1C/1H6Pl/9XyFX7Q0he4g0HUCEAAb5Mw31Ng7Pc1nuW3coC8IuCIAAeiUbHzyT3grcXe/U4DolwcCEIAOsiBE/15X/sfa+ed/3QWyZymI/kwUfZBElrPWy0TR7kSq1EEI6NI3BHSTiF4kUk8T88Oi6Yc0592R1Y1mCED7hfR4pPxD017zD43M7i+efyFF0SeVUoPtu0MPEHglAa31FkXeLeSFl9QmBp+w4QMBaIee1iH5fFitEjxs6vaGiuwa6nCVkD6TSHmm9jgOAu0T0E0hdeXgRn/8t1fwS+33J4IAtEft0lq1dKGpS/yvr9hbK0zvMLXFcRCwJaCJHlEcndDJbAACkJa+pufUrP9W0xd2h0Yb7ybRd7LyhtKaRjsQsCUw/76JqGNqk8HP27EFAUhJS0TOrk8GVyY1j//5iejHOPlTQkWzTAnMi4Cmo2qrBh9PaxgCkIIUa/08zwZvSvr3X3KeLNK7hg8S0dtTmEQTEHBCIL4c2GWjf0TaewIQgBRp0MSrZ6r+mUlNy2PNeHbwhRTm0AQEnBJgosunq6Xz0jiBAKSgpEUOn5kM4n/3nf4WV+bepjT9Enf7U8BEE/cE5l9MkwPqE4OPmZxBAAyENMnUTLX0+qQn/srjjX8i4b82wcZxEOgWAc1808yEf4rJHwTAQIi13DK9KvhUq2bxE36q1NiAh3xMpYbj3SSgSb8kKlhqemANAmDMipxVqwZXtWo2PBr+pbB822gGDUCgywRE+OT6pH9zklsIgGkGQHTsdLX0/ZbX/yPhjUqJcarV5dzDHQgQaf5WbZX/GQiARTGwjvabXjX4ZCsT5ZHmz0jRuyxcoCsIuCLwYK1aOhwCYIE3aPpDz17MMy0vAUbmZkSpvSxcoCsIOCEgOqrXVw2WIQAWeGvKH6AKN1rOAMbmGnil1wIwujokoJu16kAAAbBAbPowRXmsKRbm2+iqN7Lm27Wie4RpXalRenrDAL2QJE7zxisSLJ2jPcVv7hMxHcxMR2uR4xWpRW04b9nUxMfah+P408antd6kPL5dNN3jEa3jsPRUHvib4jflBzcBDQRNAJ0LgNBjRHJpySvd+lyFt5gSnuZ4uSK7SdT8lNbqAs+TfdL0adXGxMfGdstZV4bxp4jv8Xn+qnRLHvmb4jflBwKQUwGI13GZ+Et1VVrdzs5DpoJ4xfGKBGUdnqtJjytSA2313dbYVGCd2EzdJ4P4W/ma5y9qpD7lr6Y1HO/Kk/0vjj8Kz9GsK53yNwVlyg8EII8CIPRY5Om/eL4y8GtTgrM4Xh5tHKI1rVUe792uPVOBtWuvk/Y28e/Un9BjHukTNkwO/KqTeNrtMzzaWB5qtdZ2NrYzv6b8QADyJgCaHir5/jHPVbjebiHZtB+6SF7PKryLFB3Yjh1TgbVjy6Ztp/G/xmeP+A9fJEu0Cu9iRQfZcHh1X1N+IAB5EgChx0qef1S3T/7tCOKTiPzoJ0yyLG0RmgosrZ0s2nUS/6v8PlpS/vt6xT8WgZCje7OcCZjyAwHIiQDEGz2KT4d1a9rfatjxdJpF7hOlEpePtvc3FVgWJ3Y7NtqNf7vtvPCPLwcilvuzuidgyg8EICcCICTn1KvBFe0Uu6u25dHmRcT05TT2TQWWxkbWbdqJf7vvXPEfa15ARJdkwcWUHwhAPgTg0ZryD0xzt39ovHGoitTKSOiDHkdvMv1Ts9YNUep/mfhurfSaNN8v3O8MGfj9Hs31aW4KmgrMdpnUdfzb0p+a/+LRxmFKqZU6pA8oL3qz+SGweF9/9Yww/1Bpfe30ZLDOeGJXJNirGf0mi0sBU34gALkQAPnbWjX4x6RQ4i3HwkXRPyiRvzEWUFIDzd8a2OydYdoyani08Vlh/qbJl6nAbAXgtTfpso1/3r7IKbXJ4KakscbPTpA0r7Hd90GYrx180TvHxH9orLGCideY+JuOm/IDAei5AOiNJRUsTXrIZNt+gz8goveaEp7muBD9eHCj/+GkIhw6X3aXwcaUIrVLkk1TgWUuAPH5mmH8pPWLA5uDpUks5k9+Hd6VFX8iuWeRKh33TIVnW96LqchuWjc22D6xacoPBKDHAmDacCQOrzzSuIkUfzrNyZ26zfy3Df0VSe0XjzW/q4g+mjcBmI8no/iZ5ebpieDkxH9/N/y/UZvwP5/od6z5L0T08dQ53UlDCIANPSIyAbT9hxOSlfVqcH2rMONrfhZuuR9h58PTQsLvTtpHfni08TlhviaXAkDZxC8iK+qTwQ3d5x9rmF4+PTHwi1a+016G2eQHM4AezwA0yxEzE8EDLYtgPLxORFZ2fqK37hlfj9Yn/NNb+q40jxRNP7EpMFuBTPKdRfymDV+HR8I1oiRxptR5bviaWtX/u9YC0HyvMN3buX3zHxgEoMcC4DX94Q0Xc63lteBYM97Z9S02RZDQ99FatfS2lr4rspR0+Lu8CgARWcdfUn456cGfxV8KH1ee7OeEv9D62mSp5Xck4geDxA832Pg2zWAhAD0WANN+A8Mjc3Ompb5OCyReYpteNdDyJaC9KzK4WYeJH500FZjLGUAm8fdwvwdT/PFy7AuvC1veKEyTd1N+IAC9FoBqKTEHLk+geOjJBSJcHgt1jmcA9vH3UADSnMC2bSAAlgRNAG1PUNf2TcO39W/b3xSf6bit/x5fgpmGZ33cxAczAMwArGYgpgKzFUjTGWDr33gTdiy8VkhOM8WR1+MmPhAACEBfC4BpGTZ+uYiYH8rrCW6KCwJgIgQB6GsBIJZbaxPBSUllMOxwKdayPI3dIQBGRMkNTABtp7iu7ZuGb+vftr8pPtNxW//xZp8DfrAk6VHsN54tu8zt1vweMX/AFE/ejpv44BIAM4D+ngHE+Rc5tTYZ3JhUCvMisEd0GUl0OpGyPm+6JRQQAEvSJoCYAeR5GZMoZX4er23wD0iz+eeS0bl3auWtJJEPadL7ZLVxh2WZtuxuql9rJUsJ2NX4nNs1AbQdv2v7JkC2/m37m+IzHc/KvxCdV6+WLjf5a/t4RYI3EO0xFzWXMdFyJXS0KPlTIrV727Y66GDiAwHAJQAuAYgo3ga8JHRYN3YCfkNFdm3q5idI+EJiemsH53XqLhCA1Kh23tAEEDOABXEJsD35j3tN/6ikdzMsy+mV3VdKaWhJeIawXmXad6FTv6b6xQwAMwDMAHasAU0PeZF/bNdEgIiWjs4dEJFa62I2AAHoVDq39TMBxAxgQc0AXp4JKKVPmKoM/NKyfFJ3X/pFKUdeeGfWn5o31S9mAJgBYAawkxrY+mk2NVrf4F+dZnUg9Zme0HBeBEphvP9CZq9/QwAsM2MCiBnAgpwB7Fg1mX8cNKkkl1TmDgw1PZDVPQFT/WIGgBkAZgAp/iTmPw9OfIcw3e0p+gVR6andn6cXnljNcym6t9VkeLR5njB9ra1OLRpDACwpmgBiBrDgZwBWFbTtuwZPE/F/Komum5oceMRocKWUykvD+MOk1pcCpvrFDAAzAMwAjGdkdg2E+Ju7Ke+cpC3BY2/lscapRNxys9K0EUEA0pLqcAqFGQBmAG2XmMiPBjaVjkv6FsH8h2B2nf8uw6K27e/QAQJgQ8+4ZVbqZ81bRmFKkK3AmIZv69+2vyk+0/Fe+zfF1+o4E183XfU/m9S/PN74Dgmf2KmPuJ+JDy4BcAmASwCbM8ymr5JDapXg4VYmhsYapzHxtTYuIAA29FIoqO0/tClBtvZNw7f1b9vfFJ/peK/9m+JLOm78rsEovgtgwzeTvq4LzLV9EwRb/7b9TfGZjvfavym+xONCj9UmS/u3aoPvAljRzaaz6wJzbd9EIdn/wt8WnEi/kMxI7Wli2OlxfBegU3Jd7Of6BO3lh0E06bmZ6sBgK5x5/zBIJvH38LsAEIAunsidunItAIr9JVMTPN0qvrLLT4MZPk1VzvunwTKIv8ffBUj8tNmScRnWEk51WrtxP1P9YhXAQNcE0PYmnWJ579REcF/L60CX+9IzJ36iejjnHwelDOI3fRdgaDz8JoskLtd1eoLGDwXVq/7nWvVfMt54jxb+aaf2IQA25Lb1dS0ALHL69GTQcqlnqNJ4F2v+WQZDeZUJLSx8yPRksK61+DROF+JvJPl2zae172ziF5HT6pPBmpYMRhvLhbnlUp1VXrAMaIWvK51dF7gW+u7MZOnjSYMpj4Q3kJJTsxywaQkq9rV4tHmbYvpYHgUgq/hJy3dqq4JPJo1x2MEsDA8CZVnNDm05FwCttyg/WFKr8KZWw4i3pJ7dPbyLid6X0VD/a5Hyj0l6Hr1ckd102JhSSu2aQwHILP74LT9/Nlg6dRlvTuKf8XcB/ntgo38MHgXOqJpdmnEtAHHsQvLZejW4Lmkc8xtJhtHVdjMBLUzeml2Vd5bpZZSh0cZKZk6MKc01pu09klcyyT7+ef4iK+qTQeKLN/MivEf0ddv7Acy8JnjROyvp5I9jGhptfIaZr7etbVP94iaggbAJYBYFLsRP15X3Nqpww5Tw+J4AiVrBIh9krZeJUkFSn3ipTIl6mhTfzVqvSbrmf9lORYIhHa1nkmWmeFzzcR1/PD5N/OSM8t6Rhv/waGO5ZnUai3yIWC8jUqUkRttfBxbiu1npNUmP/u7If7GOfq1I9jXxNx035QcCkAMBmA9B6Iu1ydIlpoR243h5rPlFIvpKGl+mAktjI+s27cS/3TcLXTA9Wfpq1rF0Yq881ryAiDKpBVN+IAA5EYD5fzrF70n1D9FJVaXsU640DtZa7kv7xRtTgaV0m1mzduPf7lhrPet5dMT0xMAvMgumA0PxDCNiuT8tf5MLU34gADkRgK2TAH6alXdkrcIbTIl1cTx+9jzk6F7Pk33S2jcVWFo7WbTrJP4d/caXAl7oHTX9FbZ6+KbTsdjGvzO/pvxAAHIkAHEomugRpfwPd1sE4qf+Ih3e6REtb6eATQXWji2btp3G/2qfEdE6P/SP6bYIZBX/q8djyg8EIGcC8IeZgD6hW5cD5dHGIcLqX9Pc9Gu3wGxO6rR9beLfmY8o4qd8pU9IdcM0bZAJ7bKOf0dXEADLBJkAZrEKsLMQ5+8JkJqoKf/yNHenOxpmRYKyDs9lrSum1YRW9k18OoorbacM4m/lahv/8Zryr3DFf78zZOCF14Xn2PA3oTLlBzOAHM4Adgwpvi9Aoi9hr3Rz0sNCpkLY8Xj8kI/o5slE6oJO/vXb+YdpJ660bbOM3+Qzng0opS/Nkv/Q+bK7GmierFmdb8vfFD8EwEQo5wKwPTyt9RZi9T2P5IdEvI4i/6ldA3rhmQrNEbHsfBjCe1doYDPRH3EULiOWgyPho0n0caYn/NJiMxVYWju9ij9tfNb8KdyHIjk4Yj6aSB+X1Yc/TPGb8oMZgEkAevi+uCm5OA4CSQRM+w3EfSEAhhoKmv7QsxfzTKtmwyNzM6LUXihFEMgbAU1RbaY6OJwoErZBu7oJZhtXZv05ekttYvCJVvbKY83/IaJ3Z+YPhkAgIwJM9MB0tXQEBMACKBMdO10tfb+lADh4VdciXHQFgZcJsObrp1f5KyEAVkUhZ9WqwVWtTAyNhSczyT9buUBnEHBAQIRPqk/6t0IAbOCy3FqbCE5qZWKviuzBurGhW3d1bYaCvv1DIF61MO0zEdPATUBDTWiSqZlq6fWtl9qIFo+ENyolp/RPeWGkuSfAfENtwl9hihMCYCIUP58vcvjMZPBgy8uA8dm3csS/IqX8FObQBAQcE9BN1vL26VWDT5ocQQBMhLbOk66uTZS+kNR0eKx5mRCdm8Yc2oCAYwKX1qqlC9P4gACkoUR6pqSCNz1X4S2tmsdbRs0tCh8gRQemMolGIOCAQPw24x7Kf49py7ftriEAaZMg8oXaZHB1UvPy+Ox+WuheRV45rVm0A4GsCMT3q3ylj5yqDD6V1iYEIC0pomfVFn//pN1jY1PzO9KE9H2lOPEJrPRu0RIEzATik98TOqbdV5ghAGa2O7a4uFYtXWTqMjwyu68mby0rOsjUFsdBwJZApOnhkh99tJ1/flwCdERdN5np0DT7xsUf1twk4QSLPtu0c2xHoaATCJBuEqvL93zerzyxmuc6AYIZQPvUHiXlH5r23fx4NiCef6GOok9l9Qpu+yGjx0IioElv9rR3M/vhJZ386+/IAgLQSWUIra2t90+k2zhK2z3exIJ0dDwzHU0iB0cULfM079npTjxp/aJdsQnEr/RGSl5g8p5SzOtE6G5S3h1p/4BMo4cAmAi1OL71y67e55OeEOzQNLqBQNcIQAAsUM+LwG+8M9qZCVi4Q1cQyJwABMAe6b/JrP/p+ld5o70pWACB7hKAAGTBW+gx8uQT3drGO4uQYQMEYgIQgKzqQOtQPPV1Zn8yqxs0WYUGOyDQigAEIPPa0L8T4q95W0prTE8NZu4aBkGgTQIQgDaBpW3OWj8vnnerIv3tKS49QBXWafuiHQh0iwAEoAuk491ZFXk/EpKfMqtHhcIn/MbA7wcHaGPyvv5dCA4u+poABKCv05//wZs+bJH/EeQ7QghAvvPT99FBANyWAATALV9YtyQAAbAEaOgOAXDLF9YtCUAALAFCANwChHW3BCAAbvliBuCWL6xbEoAAWALEDMAtQFh3SwAC4JYvZgBu+cK6JQEIgCVAzADcAoR1twQgAG75Ygbgli+sWxKAAFgCxAzALUBYd0sAAuCWL2YAbvnCuiUBCIAlQMwA3AKEdbcEIABu+WIG4JYvrFsSgABYAsQMwC1AWHdLAALgli9mAG75wrolAQiAJUD3M4C5Bj595TZJ/WtdN2vVgaB/x+9+5NYzgKGR2Rorb8h9qPDQbwRER/X6qkF8at1h4q0FYHiseb8QHe4wRpjuUwJM9MB0tXREnw6/K8O2F4Dx8DoRWdmVaOGkrwiw5uunV/moLYdZtxaA8mh4IrF8x2GMMN2nBET4pPqkf2ufDr8rw7YXgIrspsPGFD593ZV89Y0TTfolng2W4JNrblNuLQBxeMMj4RpRssJtqLDeVwSYb6xN+Kf21Zh7MNiMBGB2XyFeT0r5PRgDXC40AlqHRPKO2qrBxxfa0PI2nkwEIB7U0HjzUhY6P28DRDzFIyBMl9UnSn9fvMiLF3FmAvDGs2WX2UXhfazooOJhQMR5IaCJHtllo3/Eb6/gl/IS00KOIzMB2HovYHbfiNS9SvHwQoaGsbkhED/4w0RHYurvhu/OrGYqALGDcqVxsNb6LkUenuDqXh4L7yk++clXx9QrwUOFH0yBBpC5AMyLwPjsflq8tYronQVigVB7RUDTL4miE/DP3/0EOBGAeBjz9wR2DydY67OxOtD9xBbCo9aheOrKgP3x5yq8pRAxL7AgnQnAdk7lkdm3kOdfqCU6SZHaZYHxw3A6IBA/5KPYu4Wi8BL863cAMMMuzgVge6xD58vuNBAdr4SOJiXLtY72YcV74lXiDLOZS1O6KVpeUMp7ijSv00x305x3B57wy0eyuiYA+RguogABENiRAAQA9QACfUwAAtDHycfQQQACgBoAgT4mAAHo4+Rj6CAAAUANgEAfE4AA9HHyMXQQgACgBkCgjwlAAPo4+Rg6CEAAUAMg0McEIAB9nHwMHQQgAKgBEOhjAhCAPk4+hg4CEADUAAj0MQEIQB8nH0MHAQgAagAE+pjA/wO9WBnxeVfcIAAAAABJRU5ErkJggg=='),
(458, 'OpenDocument Spreadsheet Template', 'application/vnd.oasi', '.ots', ''),
(459, 'OpenDocument Text', 'application/vnd.oasi', '.odt', ''),
(460, 'OpenDocument Text Master', 'application/vnd.oasi', '.odm', ''),
(461, 'OpenDocument Text Template', 'application/vnd.oasi', '.ott', ''),
(462, 'OpenGL Textures (KTX)', 'image/ktx', '.ktx', ''),
(463, 'OpenOffice - Calc (Spreadsheet)', 'application/vnd.sun.', '.sxc', ''),
(464, 'OpenOffice - Calc Template (Spreadsheet)', 'application/vnd.sun.', '.stc', ''),
(465, 'OpenOffice - Draw (Graphics)', 'application/vnd.sun.', '.sxd', ''),
(466, 'OpenOffice - Draw Template (Graphics)', 'application/vnd.sun.', '.std', ''),
(467, 'OpenOffice - Impress (Presentation)', 'application/vnd.sun.', '.sxi', ''),
(468, 'OpenOffice - Impress Template (Presentation)', 'application/vnd.sun.', '.sti', ''),
(469, 'OpenOffice - Math (Formula)', 'application/vnd.sun.', '.sxm', ''),
(470, 'OpenOffice - Writer (Text - HTML)', 'application/vnd.sun.', '.sxw', ''),
(471, 'OpenOffice - Writer (Text - HTML)', 'application/vnd.sun.', '.sxg', ''),
(472, 'OpenOffice - Writer Template (Text - HTML)', 'application/vnd.sun.', '.stw', ''),
(473, 'OpenType Font File', 'application/x-font-o', '.otf', ''),
(474, 'OSFPVG', 'application/vnd.yama', '.osfp', ''),
(475, 'OSGi Deployment Package', 'application/vnd.osgi', '.dp', ''),
(476, 'PalmOS Data', 'application/vnd.palm', '.pdb', ''),
(477, 'Pascal Source File', 'text/x-pascal', '.p', ''),
(478, 'PawaaFILE', 'application/vnd.pawa', '.paw', ''),
(479, 'PCL 6 Enhanced (Formely PCL XL)', 'application/vnd.hp-p', '.pclx', ''),
(480, 'Pcsel eFIF File', 'application/vnd.pics', '.efif', ''),
(481, 'PCX Image', 'image/x-pcx', '.pcx', ''),
(482, 'Photoshop Document', 'image/vnd.adobe.phot', '.psd', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAdmUlEQVR4Xu2df5BkVXXHz7nvdffsLj8iOz2zWpbyS1FBFiKwKjExa1WgIKlYaFAkiUXhLqIB+RVAMj96elYBRQG3UFiRImUKMCSbSkosJBGtRBEhQRB/LOzyI1Uu7kz3YGB/MNP93j2pN8vgwk6/+7rvu9395n3n37733HM+59zv3Pfue+8y4Q8EQCC3BDi3kSNwEAABggCgCEAgxwQgADlOPkIHAQgAagAEckwAApDj5CN0EIAAoAZAIMcEIAA5Tj5CBwEIAGoABHJMAAKQ4+QjdBCAAKAGQCDHBCAAOU4+QgcBCABqAARyTAACkOPkI3QQgACgBkAgxwQgADlOPkIHAQgAagAEckwAApDj5CN0EIAAoAZAIMcEIAA5Tj5CBwEIAGoABHJMAAKQ4+QjdBCAAKAGQCDHBCAAOU4+QgeBrglAuSIHkA5PZ6a1JHKc1uHhrPhgIlVAGjonUKsWYnNYHmtcUKsWN3Y+AnouZQLOBWBoZPYIIv+KkMKzlVLLlzLMXsRmFoC5QMj783rVv6cX/mHM/ibgTAAOrcjAbgnGKdSXkVJ+f2PIrndmAWiK1nqX8vl9tUrx0exGCs9dEHAiAMOV2cOb2tvsEa124TRs/o5AEgF4ufV2HfprZj7H28EPBBYIpC4Aw6Nz7wyY/10RDwOzewJtCEDkzE9J+X9Yq/Au955hhCwQSFUAov/8gVYPYPJ3L/VtCgCR6G/XthQ/SHdz2D0vMVK/EkhNAKJr/hd18CCW/d1NddsCQESaeONM1b+wu55itH4kkJoAlMea1xDRFf0Y5FL2qRMB2MtDLsT24FKujGSxpSIA0VafEG/B3f5k0NNs1bkA6BDbg2lmIpu2UhKAYJMoWZdNBNn2unMBIML2YLZzn4b31gIQPeGng8YUHvJJIx3t27ARgJdHw/Zg+9iXTA97ARgLPkIkdy0ZIhkLJAUBiCLG9mDG8p6Wu9YCMDQe3CIi69NyCHbaI5CSAGB7sD3sS6a1vQCMNX8iRCctGSIZCyQ1AcD2YMYyn4671gJQHpurE6mV6bgDK+0SSFMA9o6N7cF2c5Dl9mkIQAOv9PauBNIXAGwP9i6b3R85BQFoSvfdxogLBNIXAGwP5qm6IAAZz7YLAXgZCbYHM14bSdyHACSh1MdtHApAFDW2B/s492m4BgFIg2IPbTgWAGwP9jC33RgaAtANyg7HcC4A2B50mL3em4YA9D4HVh50QwCwPWiVor7uDAHo6/SYneueAGB70JyN7LWAAGQvZ6/yuHsCgO3BjJfKou5DADKe1W4KALYHM14si7gPAch4TnsgANgezHjN7Os+BCDjyeyRAGB7MON1s+A+BCDjieyZAGB7MOOVs9d9CEDG09hLAcD2YMaLBwKQ/QT2XgCwPZjlKsIKIMvZI6LeCwC2B7NcQhCALGevTwQA24PZLSIIQHZzN+95P6wA9kGItwczVk8QgIwl7LXu9pkAYHswY/UEAchYwvpeALA9mKmKggBkKl37O9t3K4BXXMTHRbNQWhCALGQpxsf+FQBsD2ahtCAAWchSJgUA24NZKC0IQBaylFEBwPZg/xcXBKD/cxTrYf9eArzKbWwP9mmdQQD6NDFJ3cqIAGB7MGlCu9wOAtBl4GkPlxkBwPZg2qlPxR4EIBWMvTOSJQHYSwnbg72rlv1HhgD0UzY68CV7AoDtwQ7S7KwLBMAZ2u4Yzp4AYHuwO5WRbBQIQDJOfdsqiwKA7cH+KScIQP/koiNPMiwAUbzYHuwo6+l1ggCkx7InljIuANge7EnV/G5QCECPE2A7fE35Japwo5WdoZG5OVGqaDuOy/6aeONM1b/Q5RiwvTgBCEDGK6Og/PJzFa63CqM81txCREf1f5jYHuxFjiAAvaCe4pha5KSZyeLDLQVgPLiJRD6V4pCOTGF70BHYWLMQgF5QT3FMEVlXnyze2vISYHzuWBF6lEhZ5zpFtxc1pbXepXx+X61SfNT1WLC/l4B1UZTHmgKYvSPALHdMTxTPjvNgaCy4WUjO652XbY28XYf+mpnP8fa2eqFxRwQgAB1h66dOemdpZ3H419fzS628OrQiA7t1cC8R/VE/eR7jC7YHu5QoCECXQDsdRuSc2mTx9rgx3nixLGusCG8UFX4iC5cDJPrbtS3FD9LdHDpll3PjEIClUABCT9Y8/2iqcGAKpzzaOJ5ZnScka1nrN/fzFiG2B03ZtP8dAmDPsC8siNCl9cnCl/vCGTiRGQIQgMykKt5RTfqlgtCJOyZLv1giISGMLhCAAHQBcheH2Oo1/ZN3XM21Lo6JoTJMAAKQ4eS1cP2niv1TpyZ4eumFhojSJgABSJtoH9jTIW/zVXjG1GTp8T5wBy70MQEIQB8nx8Y1rfWsp9TYtPJvjHtZyGYM9M0+AQhA9nMYG4EmfopJX+vtKdwxdR3vXuLhIrw2CUAA2gSW1eaa9G5F6jsscj8RPyae/8wKov97tkJzRIzHubOaWEu/IQCWANHdLQHTB0/cjr70rUMAln6OMx0hBMBt+iAAbvnCuiUBCIAlQEN3CIBbvrBuSQACYAkQAuAWIKy7JQABcMsXKwC3fGHdkgAEwBIgVgBuAcK6WwIQALd8sQJwyxfWLQlAACwBYgXgFiCsuyUAAXDLFysAt3xh3ZIABMASIFYAbgHCulsCEAC3fLECcMsX1i0JQAAsAWIF4BYgrLslAAFwyxcrALd8Yd2SAATAEiBWAG4BwrpbAhAAt3yxAnDLF9YtCUAALAFiBeAWYBLrmsKaYvV90fRjZvUE6WBbMSw9v2wX7dq2keeS2EAbEHBBACsAF1TnberfEnl3KNHfnJosPISv7jgDDcMWBCAAFvAW66q13sGsvsiev6lW4V0pm4c5EEiVAAQgLZxaB+KpG+glv1r/Au9MyyzsgIBLAhCAFOjOf3lX6TPrleIjKZiDCRDoGgEIgCVqFvq3wPP/6vkKv2hpCt1BoOsEIAA2yJlvrbF3fpJjuW2GQV8QcEUAAtAp2WjyT3jrcXe/U4Do1w8EIAAdZEGI/rWu/A+385//dVfIwYVi+Gei6I9JZDVrfZgoOpBIFTpwAV1yQ0A3iehFIvUMMT8qmr5Hc949ad1ohgC0X0hbQ+WfkPSaf3Bk9ijx/CspDD+qlBpofzj0AIFXE9Ba71Hk3UlecE1tYmCbDR8IQDv0tA7I5xNrleKjpm5vqMjyQAcbhPSFRMoztcfvINA+Ad0UUjcM7PTHf309v9R+fyIIQHvUrq1VC1eaukT/9RV7m4XpHaa2+B0EbAlooscVh2d0shqAACSlr+k5Neu/1XTC7uBo410k+l5W3mBS02gHArYE5t83EXVKbbL403ZsQQAS0hKRi+uTxRvimkf/+Ynoh5j8CaGiWaoE5kVA08m1DQNbkxqGACQgxVo/z7PFN8X99x++TFbo5cHDRPT2BCbRBAScEIguB5bt9NckvScAAUiQBk28cabqXxjXtDzWjFYHn0lgDk1AwCkBJvrSdLVwWZJBIAAJKGmRk2Ymi9F/90X/Vlbm3qY0/Rx3+xPARBP3BOZfTJOj6xMDT5oGgwAYCGmSqZlq4fVxT/yVxxt/T8J/bYKN30GgWwQ08+0zE/45pvEgAAZCrOXO6Q3Fj7VqFj3hpwqNHXjIx1Rq+L2bBDTpl0QVV5keWIMAGLMiF9WqxRtbNRsaDf5SWL5pNIMGINBlAiJ8dn3SvyNuWAiAaQVAdOp0tfDdltf/I8FtSolxqdXl3GM4ECDS/I3aBv8TEACLYmAdHjm9YeCpVibKI83/IUW/bzEEuoKAKwIP16qFkyAAFniLTX9w+9U80/ISYGRuRpQ6xGIIdAUBJwREh/X6hoEyBMACb035Japwo+UKYGyugVd6LQCjq0MCulmrlooQAAvEpoMpymNNsTBPJvs2tuf7VqS4ao4OFr95eMh0HDOt1SKnK1IrrG23YUBrvUt5/G3R9H2P6DEOCk/vKNELceLaDf+XOn9TfLgJaChiE8C+F4BF4itX5AAJmx/TWl3heXJ4G/O4k6ZbieTagirc+VyF93Ri4LV90vTflN80/HXpv8k/U3wQgBwKwCshV6RYDoNLNOuKIlUyFVM7v0f70CxqpD7lb6RNHH3VJv2/FPw3TZD0nd7HYuS/Di7VpMfT5r8wiik+CECeBeDl2IdGG6sDrTanthoQetIjfcaOydIvnE6gFPw3TZBu+F8ebRyvNW1WHh+a9nim+CAAEIB5AkNXybBWwX2s6FirItT0SMH3T3muwnUrO2127tR/0wRp042Omw9eJa9nFdxHio7p2MgiHU3xQQAgAK8QiCZRwOEDFiuBJwrK/4NuT/6FADrx3zRB0pyMJluRCJAf/ohJDjO1Tfq7KT4IAATgVQSiy4GQ5SftXpNGH6oUn058vlL6ZdLidNGuXf9NE8SFj3E2o8sBFnlQlIrdvkvqlyk+CAAEYD8C5bHmFUR0TdIii9oJySX1avH6dvq4atuO/6YJ4srHeBFoXkVMn0tjbFN8EIA+FwDbbUbWuiFK/S8T36+V3pTo/MKKFA9phr9q41LgiZryj0lyTsLK0caJSqn1OqD3Ky98s/khqui7+OpZYf6e0vrm6cniY8aJ0Yb/pgnSC/5HXiCl3x7U3JLGTUFTfBCAJS4A+4Wn+Rul3d4Fpk9GDY411jHxJuNkm//3L+fUJou3x/5Xq8gBJM2bbL+bIMw3D7zoXZKW/6YJYisAnfIfGm18Upi/loh/TCNTfBCAvAnA/HKdfjiw0/+TuEkUPWyjdWOH8YlBrV8s7S6uMtkiHdxHRO+xLei9/eX7K1ThtGcrPNvKXlL/TRMkdQFIyH/wcjlQBhpTitQyG2am+CAAORSA+ZDnzzb018X+1x5r/iMR/UVcG2a5Y3qieHasnZHG7aT44zaFvF9f5q/WJvxP2/pvmiAuBCAp/5VjzX9SRB+y4WaKDwKQVwEgLST8rrjvyCdZhorIuvpk8dZWGAfHGyewcMvvKdoUN7NePT1R+lkrG0n8N00QZwKQjP+nhPkmG0am+CAAuRUAouh6uj7hn996AjXfI0wPxCEyfTB1aCTYJEpiVxqdFzjfVKv6f2Pjv2mCuBOABPwrzfeKph91zoeML5tBAHIsAET0RK1aeFvLCXSVDIsf7IhDVFB+Oe7Bn5V/F2xVnhxpU8Qt+wptqU0WWp7DED0YZPK/lwJg4l+uyCrSwW9s2JnigwDkWAA06bmZaqnlicXRdtQLrwta3miL0PXyewnRFuf0hlLLl5gS+V8txM4BlysAk/+HVmRgtw46OvRzoawhADbyGRW44wJxbT92+Z5xAbBM7Xz3XvI3jy9cHgu0TZym+LACyPEKgAxL6OFxGdISTMUh8pr+0I6rudaqTXmsGR1O8RabInbZ1zRBXK4AzAJAZDu+KT4IQJ4FwLCVNjzeeLcW/nHsKoJlzcxE8aGW9xHGgpuF5DyXk9jGtmmC2E5Ak2+uxzfZhwDkVgC0sPDxcY/WDo41zmPim+MQCcn6erX49ZYrgNHG8cT8iGki9Op30wSBABgy4xpQrwoj6U0U2/h7VYCmLcAo/vJ441skfGZsDljuqk0Uz4prMzQe3CIi63udy8XG7xX/fqkvrADyuQL4zxXKPyXuUdrouPNg+fyjqLEfD40+9lnyi8Nx3/t748WybO6A5neI+f39JgIQAMuM2P4HtBzeeXfXBeLa/qsBaWHyNi1X3kVxkz/qMzja+AQzt1zav8quyLm1yeJtccmYF4GDwutIwvOJlPU/nrQS313++3vtenyTfetEQADcfhbclm+0169EPUOK72etNyV9nXalDn+pSI5IONG21nb4Ryf5+Ofw6Nw7tfLWk8gHNOnD2/3wSEJ/EjczTRBb/iZHXI9vsg8B6PNLAFMBufi9nQ9qLIwvRJfVq4Uvpe5PRYpvIDpoLmwexkSrldBaUfKnROrANMYyTRAIAG4COn1SzFSAaRR5Ozba/aTWgu3oM+AFoRO78SXgN1RkeVM3P0LCVxLTW9uJ77VtTfwhABCA3AhAJx/VfE15bPWa/slxDwbZTNb9+q6XwuBwcIGw3tDpe/MQAMuMuFZIS/esu7suEJN96wASGohePAl1cK9HtDphl8WbaXrEC/1TuyYCRLRqdO7okNTmTlYDJv6u69v1+Cb7uAeAewAUfYlWWP1zip+j3qqUPmOqUvq5lZi00XnVZ6UcesG97R7VbpogEABcAizZS4CX35a7hLWupPUZ6n3vCTCp0foO/ytJdgfamOstm86LQCGI3p9P/O4BBMCSvGuFtHTPurvrAjHZtw5gEQPR9+ZUqXm2ZnV5iv/1W7ma+uGgcUyGK3PHBJoeSnpPwMTfdX27Ht9kH5cAS/oSQPjQCpV2E/0eU3A4hXJcyLyWSJ+WdIKkJUDzx4MT3yNM93uKfkZUePrA5+mFbRt5Lq0xFuwMjTYvE6YvJrFrmiAQAFwC9PQSwHUBJpkkvWzz8rkGzxDxfygJb5maLD1u9Ge9FMqrguhgUuOlAATASDO+wVIvUNcF4tq+ZXr7rrsQf+0A5V1iepS5PNY4l4hbfqx0IbBe83c9vsk+LgH6/BJgqQtsRwoj8oPSrsJpcWcRJH2ZyTRBXPN3Pb7JPgQAAtDRHOx1Jya+ZbrqfzLOjySvM5smCAQA9wBwD6DXs73V+EqOr1WKj7b6OckHTSAAlsl1rZCW7ll3d10gru1bA+hjA6aPmgyNms816DV/1+Ob7OMSAJcAfTzFDa4JPVmbLBzVqlUGzgVw/lViCIBleZsA2q6AXNu3DD9Bd/1CfCN1cAIjHTUxfVe/388FiIKOzz8+C95RYaTZyfUEdW0/TRaL2cLBIJ0TNh3MgoNBOmebWk/XE9S1/dRAtDDU43MBYo82S3KuQU/5G85lwNFgrqs3gX3XBeLafoIQrZpow7kAg+PB11gkdruuUweih4LqVf9TrfonOdegp/wN5zIM4XDQTksjvX6uC8S1/fRILG5JRM6rTxY3tbwRN9pYLcwtt+qs/Mv0NqD5XIahscb5QvxVG0am+sIugIGuCWDubwJq+VZtQ/GjcRiHHJwOlPUHgUxbmBHPlaPNuxXThyEANgQs+0IA4gFGb/n5s8VVU9fx7lYtHZwL8F+lnf4pGX4U2HguQ7kiB+igMaWUWm5Twqb6xQoAKwCb+prvKyLr6pPF2BdvIhGYPSj8su39AGbeVHzRuyhu8kc+JT3XwDRBbFd4r4bb1rkM65n5FtvkmOKDAEAAbGuMNPFTM8p7B1W4YTIWfXVYszqPRT5ArA8jUoW4PguvAwvx/az0prhHf1+xU5Fi0nMNTBPEVgA6PZdhUIdb0vhYiyk+CAAEwDRnE/3OQldMTxa+kKix40btnGtgmiCOXV3UfHms+Vki+nwaY5vigwBAANKoM9Jaz3oerZmeKP0sFYMdGmn3XAPTBOnQjY67lSuN47SWB9M6MckUHwQAAtBxsb62Y3Qp4AXeydOf56nUjLZhqJNzDUwTpI3hrZt24r9pUFN8EAAIgKmG2vo9JHrMD/xTui0CnZ5rYJogbQVv0bhT/01DmuKDAEAATDXU9u9hyE/7Sp+R6CDStq3v38HmXAPTBEnBPaMJG/9Nxk3xQQAgAKYa6uj3+bvfpMZryr8+ye5AJ4Okca6BaYJ04lfiPhUplnVwqYtzGRZ8MMUHAYAAJK7XThpGqwGl9LXsFe6oVXhXJzZe2yfNcw1MEyQNf19rI3rIR3TzbCJ1RRpbfXE+muKDAEAAXNT4fja11nuI1Xc8ku8R8WMU+k8vL9ILz1ZojohlcSfcn2tgmiB2cPbxPwwOI5bjQuG1JPo02yf8kvplig8CYBIA5ZfilrDlsbmG6WGWpMlCOxBIk4DpgynRWBAAA/Fi0x/cfjXPtGo2NDI3I0odkmbiYAsE0iCgKazNVAeG4mxBAEykOXxLbWJgW6tm5bHmfxPRu0xm8DsIdJsAEz00XS2sgQBYkGeiU6erhe+2FICR4FZScq7FEOgKAk4IsOavT2/w10MArPDKRbVq8cZWJgbHgrOZ5B+shkBnEHBAQITPqk/6d0EAbOCy3FWbKJ7VysQhFTmIdWNHt0/btQkJfZc+gWjXRfnFYdPWK+4BGGpBk0zNVAuvb71VRbRyJLhNKTln6ZcVIswMAeZbaxP+OpO/EAATISLSIifNTBYfbnkZMD77Vg75F6SUn8AcmoCAYwK6yVrePr1h4CnTQBAAE6Hod6av1CYKn4lrOjTWvE6ILk1iDm1AwDGBa2vVwpVJxoAAJKFEeqagim96rsJ7WjWf/+7diuAhUnRMIpNoBAIOCERvYx6k/Hc/W+HZJOYhAEkoRW1EPlObLH4lrnl5fPZILfSAIq+c1CzagUBaBKL7Vb7S752qDDyd1CYEICkpou1qj39U3NdvI1PzX3QJ6LtKcewTWMmHRUsQMBOIJr8ndEq7r2BDAMxs921xda1auMrUZWhk9ghN3mZWdKypLX4HAVsCoaZHC374oXb+8y+MCQFoi75uMtMJSb57Fx3suEuCCRZ9MV4WagsyGicmoJvE6ksHP+9Xtm3kucTd9mkIAWif2hOk/BNMD1gsmI1WA+L5V+ow/Fi3XgFtPyT0yBIBTXq3p7072A+u6eS//r6xQgA6ybzQ5toW/0y6m8Ok3aOPQJAOT2emtSRyXEjhYZ7mg0WpYlIbaJc/AtErvaGSF5i8pxXzYyJ0PynvnqT/gEzEIAAmQi1+33syrffpuCcEOzSNbiDQNQIQAAvU8yLwK++CdlYCFsOhKwikTgACYI/0X2TW/3j9C7zT3hQsgEB3CUAA0uAt9CR58pFE59alMR5sgEBKBCAAKYEkrQPx1JeZ/cm0btCk5RrsgEArAhCA1GtD/0aIv+jtKWwyPTWY+tAwCAJtEoAAtAksaXPW+nnxvLsU6W9OceEhqrBO2hftQKBbBCAAXSAdfZ1VkfcDIfkxs3pCKNjmN0q/HSjRzvjv4nfBOQyRawIQgFynv/+DNx1s0f8R9LeHEID+zk/uvYMAuC0BCIBbvrBuSQACYAnQ0B0C4JYvrFsSgABYAoQAuAUI624JQADc8sUKwC1fWLckAAGwBIgVgFuAsO6WAATALV+sANzyhXVLAhAAS4BYAbgFCOtuCUAA3PLFCsAtX1i3JAABsASIFYBbgLDulgAEwC1frADc8oV1SwIQAEuAWAG4BQjrbglAANzyxQrALV9YtyQAAbAEiBWAW4Cw7pYABMAtX6wA3PKFdUsCEABLgO5XAHMNHH3lNkn5ta6btWoJB6c4LADrFcDgyGyNlTfo0EeYzikB0WG9vmEAR607zL+1AAyNNX8iRCc59BGmc0qAiR6arhbW5DT8roRtLwDjwS0isr4r3mKQXBFgzV+f3uCjthxm3VoAyqPBmcTyLYc+wnROCYjwWfVJ/66cht+VsO0FoCIH6KAxhaOvu5Kv3AyiSb/Es8VhHLnmNuXWAhC5NzQSbBIl69y6Cuu5IsB8W23CPzdXMfcg2JQEYPYIId5CSvk9iAFDLjUCWgdE8o7ahoGtSy20fosnFQGIghocb17LQpf3W4DwJ3sEhOm6+kThb7PnefY8Tk0A3nixLJtdETzIio7NHgZ43C8ENNHjy3b6a359Pb/ULz4tZT9SE4C99wJmjwhJPaAUDy1laIjNDYHowR8mei+W/m74LmY1VQGIBihXGsdpre9T5OEJru7lMfMjRZOffHVKvVJ8JPPBZCiA1AVgXgTGZ4/U4m1WRO/MEAu42isCmn5OFJ6B//zdT4ATAYjCmL8ncGAwwVpfjN2B7ic2EyNqHYinbiiyP/5chfdkwucl5qQzAVjgVB6ZfQt5/pVawrMUqWVLjB/C6YBA9JCPYu9OCoNr8F+/A4ApdnEuAAu+Dl4uB1IpPF0JrSUlq7UOD2fFB+NV4hSz2ZemdFO0vKCU9zRpfkwz3U9z3j14wq8/ktU1AeiPcOEFCIDAvgQgAKgHEMgxAQhAjpOP0EEAAoAaAIEcE4AA5Dj5CB0EIACoARDIMQEIQI6Tj9BBAAKAGgCBHBOAAOQ4+QgdBCAAqAEQyDEBCECOk4/QQQACgBoAgRwTgADkOPkIHQQgAKgBEMgxAQhAjpOP0EEAAoAaAIEcE/h/8QUa4k/yufQAAAAASUVORK5CYII='),
(483, 'PICSRules', 'application/pics-rul', '.prf', ''),
(484, 'PICT Image', 'image/x-pict', '.pic', ''),
(485, 'pIRCh', 'application/x-chat', '.chat', ''),
(486, 'PKCS #10 - Certification Request Standard', 'application/pkcs10', '.p10', ''),
(487, 'PKCS #12 - Personal Information Exchange Syntax Standard', 'application/x-pkcs12', '.p12', ''),
(488, 'PKCS #7 - Cryptographic Message Syntax Standard', 'application/pkcs7-mi', '.p7m', ''),
(489, 'PKCS #7 - Cryptographic Message Syntax Standard', 'application/pkcs7-si', '.p7s', ''),
(490, 'PKCS #7 - Cryptographic Message Syntax Standard (Certificate Request Response)', 'application/x-pkcs7-', '.p7r', ''),
(491, 'PKCS #7 - Cryptographic Message Syntax Standard (Certificates)', 'application/x-pkcs7-', '.p7b', ''),
(492, 'PKCS #8 - Private-Key Information Syntax Standard', 'application/pkcs8', '.p8', ''),
(493, 'PocketLearn Viewers', 'application/vnd.pock', '.plf', ''),
(494, 'Portable Anymap Image', 'image/x-portable-any', '.pnm', ''),
(495, 'Portable Bitmap Format', 'image/x-portable-bit', '.pbm', ''),
(496, 'Portable Compiled Format', 'application/x-font-p', '.pcf', ''),
(497, 'Portable Font Resource', 'application/font-tdp', '.pfr', ''),
(498, 'Portable Game Notation (Chess Games)', 'application/x-chess-', '.pgn', ''),
(499, 'Portable Graymap Format', 'image/x-portable-gra', '.pgm', ''),
(500, 'Portable Network Graphics (PNG)', 'image/png', '.png', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAb+klEQVR4Xu2df5BkVXXHz7nvdc/sLkiEmVm0LAML/lZARVYxJpX9Q6wgFUMiBklijIAaCxQ1QHR2pqdnLUBBQJIoKxpTphBjQioKlpgSUykLFaKywR/8WH6kSsjOdA8G99dM93v3pHqWhf0x3ee9vu92vzfvO/++e8+Pzznv2/fd+6abCX8gAAKlJcClzRyJgwAIEAQATQACJSYAAShx8ZE6CEAA0AMgUGICEIASFx+pgwAEAD0AAiUmAAEocfGROghAANADIFBiAhCAEhcfqYMABAA9AAIlJgABKHHxkToIQADQAyBQYgIQgBIXH6mDAAQAPQACJSYAAShx8ZE6CEAA0AMgUGICEIASFx+pgwAEAD0AAiUmAAEocfGROghAANADIFBiAhCAEhcfqYMABAA9AAIlJgABKHHxkToIDEwAxmtyBNn4TGbaRCKnWBtvYMNHEZkKytA/gUa90rOG41Otixr16g39e8DM1UzAuwBMTC6eQBReFlN8njFm7WqGOYzcdAFYioSC32/Ww9uHER985puANwE4riajuyWapth+lIwJ842huNHpAtAWa+0uE/KbGrXqvcXNFJH7IOBFANbXFje0bXBrQHSyj6Bh81kCSQTg6dGP2zjcuPAJfhz8QGA/gcwFYP3mpVdFzP9uiNcDs38CKQSgE8xPyIS/3ajxLv+RwUMRCGQqAJ1P/siau3DzD670KQWASOxtjfurb6OvcTy4KOEprwQyE4DOM/+vbfQDLPsHW+rUAkBElviGhXp48WAjhbc8EshMAMan2lcS0WV5THI1x9SPAOzjIRfjeHA1d0ay3DIRgM5RnxDfj93+ZNCzHNW/ANgYx4NZVqKYtjISgGirGLmgmAiKHXX/AkCE48Fi1z6L6J0FoPOGn41ac3jJJ4typLfhIgBPe8PxYHrsq2aGuwBMRe8gkltWDZGCJZKBAHQyxvFgweqeVbjOAjAxHd0oIhdmFRDspCOQkQDgeDAd9lUz2l0Apto/FKLTVg2RgiWSmQDgeLBglc8mXGcBGJ9aahKZY7IJB1bSEshSAPb5xvFg2hoUeXwWAtDCv/QOrwWyFwAcDw6vmoP3nIEAtGXwYcPjfgLZCwCOB8vUXRCAglfbhwA8jQTHgwXvjSThQwCSUMrxGI8C0Mkax4M5rn0WoUEAsqA4RBueBQDHg0Os7SBcQwAGQdmjD+8CgONBj9UbvmkIwPBr4BTBIAQAx4NOJcr1ZAhArsujBzc4AcDxoF6N4o2AABSvZgdFPDgBwPFgwVtlxfAhAAWv6iAFAMeDBW+WFcKHABS8pkMQABwPFrxnDgwfAlDwYg5JAHA8WPC+2R8+BKDghRyaAOB4sOCdsy98CEDByzhMAcDxYMGbBwJQ/AIOXwBwPFjkLsIKoMjVI6LhCwCOB4vcQhCAIlcvJwKA48HiNhEEoLi1W448DyuAAxDivwcL1k8QgIIV7NBwcyYAOB4sWD9BAApWsNwLAI4HC9VREIBClevwYHO3AngmRHy5aBFaCwJQhCr1iDG/AoDjwSK0FgSgCFUqpADgeLAIrQUBKEKVCioAOB7Mf3NBAPJfo54R5vcR4KCwcTyY0z6DAOS0MEnDKogA4HgwaUEHPA4CMGDgWbsrjADgeDDr0mdiDwKQCcbhGSmSAOyjhOPB4XXL4Z4hAHmqRh+xFE8AcDzYR5m9TYEAeEM7GMPFEwAcDw6mM5J5gQAk45TbUUUUABwP5qedIAD5qUVfkRRYADr54niwr6pnNwkCkB3LoVgquADgeHAoXfOsUwjAkAvg6r5hwhGqcaubnYnJpSUxpurqx+d8S3zDQj282KcP2F6ZAASg4J1RMeH4EzVudktjfKp9PxG9JP9p4nhwGDWCAAyDeoY+rchpC7PVe7oKwHT0tyTylxm69GQKx4OewPY0CwEYBvUMfYrIBc3Z6k1dHwGml04SoXuJjHOtMwx7RVPW2l0m5Dc1atV7ffuC/X0EnJtifKotgDk8Asxy8/xM9bxeEUxMRZ8TkvcOL8pUnh+3cbhx4RP8eKpZGNwXAQhAX9jyNMnuHNlZXf/La3lvt6iOq8nobht9i4h+J0+R94gFx4MDKhQEYECgvboReXdjtvqlXj5ecImsaa2LrxcTn1+ExwESe1vj/urb6Gsce2VXcuMQgNXQAEIPNoLwFVTjSEtnfHPr1czmvUKyia39zTwfEeJ4UKum+3UIgDvDXFgQoY80ZyufzkUwCKIwBCAAhSlV70At2b0VodftmB352SpJCWkMgAAEYACQB+jioaAdvnHHFdwYoE+4KjABCECBi9cl9J8YDt8yN8Pzqy81ZJQ1AQhA1kRzYM/GvD008dlzsyP35SAchJBjAhCAHBfHJTRr7WJgzNS8Ca/v9c9CLj4wt/gEIADFr2HPDCzxw0z2qmBP5ea5q3n3Kk8X6aUkAAFICayowy3Z3YbMN1nkTiLeJkH46Dqi/3usRktEjNe5i1pYx7ghAI4AMd0vAe0LT/x6X/3WIQCrv8aFzhAC4Ld8EAC/fGHdkQAEwBGgMh0C4JcvrDsSgAA4AoQA+AUI634JQAD88sUKwC9fWHckAAFwBIgVgF+AsO6XAATAL1+sAPzyhXVHAhAAR4BYAfgFCOt+CUAA/PLFCsAvX1h3JAABcASIFYBfgLDulwAEwC9frAD88oV1RwIQAEeAWAH4BQjrfglAAPzyxQrAL19YdyQAAXAEiBWAX4Cw7pcABMAvX6wA/PKFdUcCEABHgFgB+AWYxLqluGHYfFcsfZ/ZPEA22l6NR55cs4t2bb+Bl5LYwBgQ8EEAKwAfVJdt2l8RBTcbsV+em63cjW/d8QYahh0IQAAc4K001Vq7g9l8ioNwa6PGuzI2D3MgkCkBCEBWOK2NJDDX0d6w3vwk78zKLOyAgE8CEIAM6C5/866x5zRr1R9nYA4mQGBgBCAAjqhZ6OtREP7pkzX+taMpTAeBgROAALggZ76pwcH7k/wst4sbzAUBXwQgAP2S7dz8M8GF2N3vFyDm5YEABKCPKgjRvzVN+EdpPvmfe5kcVanGZ4mh3yWRk9na48XQkUSm0kcImFIaArZNRL8mMo8S871i6Tu0FNye1UYzBCB9Iz0Um/DUpM/8Y5OLL5EgvJzi+I+NMaPp3WEGCBxMwFq7x1DwFQqiKxszo9td+EAA0tCzNqKQX9eoVe/Vpj2/JmsjG20RshcTmUAbj+sgkJ6AbQuZ60Z3htO/vJb3pp9PBAFIR+2qRr1yuTal86lvOLhVmF6ujcV1EHAlYInuMxyf3c9qAAKQlL6lJ8xi+GLtF3bHNrdeS2K/xSYYS2oa40DAlcDy/5uIOaMxW/1JGlsQgIS0ROSS5mz1ul7DO5/8RPQ93PwJoWJYpgSWRcDSGxtbRh9KahgCkIAUW/skL1Zf2OvTf/1HZZ1dG91DRC9LYBJDQMALgc7jwJqd4cakewIQgARlsMQ3LNTDi3sNHZ9qd1YHH0xgDkNAwCsBJrpmvl75aBInEIAElKzIaQuz1c6n+4p/x9SWXmos/RS7/QlgYoh/Asv/mCavaM6MPqg5gwAohCzJ3EK98rxeb/yNT7f+gYT/TION6yAwKAKW+UsLM+G7NX8QAIUQW/nK/JbqO7sN67zhZyqtHXjJR2s1XB8kAUt2r5jqsdoLaxAAtSryoUa9en23YROboz8Rli+rZjAABAZMQITPa86GN/dyCwHQVgBEb5mvV+7o+vw/GX3RGFGXWgOuPdyBAJHlLzS2hOdDAByagW184vyW0Ye7mRifbP+IDL3GwQWmgoAvAvc06pXTIAAOeKvtcOzxK3ih6yPA5NKCGHO0gwtMBQEvBMTGzeaW0XEIgAPehglHqMatriuAqaUW/qXXATCmeiRg2436SBUC4IBY+2GK8am2OJgnzb6L7eW5Nakeu0RHSdjeEDOdwkybrMiZhsw6Z9tEavzjm5e+QWzemoWvlWxo/Fzr86xPu5Mt32YNfVeYtlValUd3jNBTvT4cssjZNX6NDzYBlSppAH0XKIsmOtTGeE2OkLj9TmvNZUEgG1x8aHzW1xY3RBH/zNcxqebftT4k9CCRXFUJKrc8UeM9Lqz6mesav8YHAlBCAXgm5ZpUx+Pow5ZtzZAZ6adBtQbr2JyYateEaLof+9oczX+/N1DnHJ2JP940lRvSfPOTFm/a6/3Gv9+PxgcCUGYBeDr3ic2tkyNrbu1nNaA1WMfFcTUZ3WXjnzPJ8WlvAG285r+vG0jowTiwf/BkbeTnmn/f1/uK/4CgND4QAAjAMoGJj8l6a6Jvs6GT0jS11mD7bY1NRWcxydfT2E4yVvOf+gay9ONKGJ7xRI2bSfz7HpM6/kMC0vhAACAAzxDoiEDE8V1pVgJagx2I18eGoOY/1Q0k9GAlCN+Yl5u/wy5V/Cv0ssYHAgABOIhA53EgZvlh0j0BrcEONO5jQ1Dzn/QG6nzRpoT0ujws+w8STc+nTBAACMBhBMan2pcR0ZVJlrfaDXiojaw3BDX/SQVASD7crFevTZLzIMckjb9bTBofCEDOBcC1AdjalhjzP0x8pzV2a6LfL6xJ9eh2/IskjwJagx2KN+sNQc1/Qn4PNEz4yiS7/WPTrVNNbC6MDW0yFL8w7y+BaXwgAKtcAA5Lz/IXRnYHF2lfGTU21bqAibdqn3Zag600P8sNQc1/MgGQv2jUq3/fK9fOV75F6+K/MSJ/rjHJ03WNDwSgbAJAREL0vdGd4Zt7iUDnZSFrWzu0Nwa1BuuGN6sNQc2/LgB2Z8VUj+31ks/yzb82+rYhOj1PN3eSWDQ+EIASCsByysu/bRhe0Cv98an2PxHR23uN0Rqs29ysNgQ1/5oAaF/4srwTP9n6Ehl+V5IbLm9jND4QgLIKAFkh4df2+h75ic2t9wnzZ30IQMdmFhuCWoNrAiAkFzbr1c93y7HzzM/CXb8PMm83/KHxaHwgAKUVACJh/lxzJnx/NwQTm9tvEKa7fAlAFhuCWoNrAmBZNi7MVO/uymAy2ipGeq6U8iwCGh8IQIkFgIgeaNQrL+3a/B+T9RJGO3wJQMeu64ag1uCaAATtcGLHFdzoluMxH48eMoGcmOeb3KU+EIASC4Alu7RQH+n6i8UnXiQjTz03WnRpsCQ3jsuGoKsArPbve9D4QAAgAF4FYKwWvbVZC2/rhdllQ1BrcG0F4Do/icANc4yWHwSgxAJAQvc3Zitdf8ps/bRMWInmXFYAR388evg5leAVj9W450qi3w1BrcEhAJWe9zgEoMwCwPx3jZnwA12P6qZbr7fC33cRgM4NKES1Zr0y08tOvxuCEIDeDazxgQCUVgCssPCr52er27oegU213svEn3MVAGvtYkj25XNb1jzay1Y/G4Jag2MFgBWA0yOYa4Npzn3b7+ZfOwLszBufbn2VhM9xFYDl+WJva8yOnKXxSLsh6MrPdb6Wz7Cva/lhBVDOFcB/rjPhGb2ey/e9/tqac30V+MBPYDF8VtYbglqDYwWAFYCTCLs2mObct/2D/VthCrauNcGHtE25sc2t85m56xty++2miT+O+ZGsNwTT+F+pFq7ztfoO+7qWH1YAq3wF0DnrN2IeJcN3srVbez3zP4OiJtVjbPxzQ3KC1sBagx36CZz1hmBa/4fm4zpf4zPs61p+EICcC8AwGijLLwQ5VACy3hDUGtz3I4Dm37V+WvyafS0+CAAE4CACWX8l2IoNnOGGoNbg2g3ke752g2rXtfi1+Vp+EAAIwDMEfHwpaLcGzmpDUGtw7QbyPV+7QbXrWvzafC0/CAAEYJnAeE2OjW30rYDoZK2pDryuNVi3Bs5qQ7Bf//1sYvaziZiG5UpjIQCuBB3nuzaY5l6zr83P4vr45tarhc2/9PPDHVr8vRo4iw1BF/8ddr7nu9YHAuBK0HG+a4No7jX72nyX60//t9+H2dqaGNPzV2S7+dHi79XAWWwIuviHABDhEaCEjwBjl8qRZqR9nmVzaT+f+lk8Ajxjw3FDEALQu4E1PhCAVS0AwsfVaGQ30W8wRRsollNi5k1E9vcMmTUuq4esnqE7dlw2BLUG15bQvue7Mtbi1+xr+UEAci4Arg2gNYjrda3BksTvsiHo6t91fh749YpByw8CAAFw6mGtwZIIwPIqoM9/GXb17zrfCV4Gm5Cafy0/CAAEQOuhnte1BksqAP1uCLr6d53vBA8C4IrP/3zfDeLbvm9Cmcbfx4agq3/X+a58ffvX7GMFgBWAUw9rDZZ0BbA/iLQbgq7+Xec7wcMKwBWf//m+G8S3fd+Eso4/7Yagq3/X+a58ffvX7GMFgBWAUw9rDZZ2BZB2Q7BZDzf0SkDz7yP+NEB9+9fsQwAgAGn69bCxWoNpN+BKztNsCDbr4TcgAN0JaPWBAEAAcicAywEl3BDUgtcEaNg/DKLdoFr8Wv6afQgABEDroZ7XtQZzaeAkG4Ja8Jp/w+H6uRme72bH90+D+eTXyUmzDwGAAGj30NAEIOmGoMsjgGF5w9xM9QfdbExMRzeKyIVOkHpM1m5QTcC0uDT7EAAIgNZDQxOApBuCLgLAIu+fn612/e2DsVrrNWz5R06QIAC+8Pm3qymob4V2te+bkG8+STcEu+Wp8bNC/7wwW3l7TxGZjG4iI+/xwdI3P80+VgBYATj1tdZg2g2YyLnDhqDm31q7x4TV9Y0a7+oWy/NrsrZlozuY6LcSxZtikG9+mn0IAAQgRbsePlRrMO0GTOq83w3BJP6F5H3NevXGXrF0RKAt8fUkcn7SmJOM881Psw8BgAAk6dOuY7QGS3IDJgmg3w3BJP6F+NGmCV5KNW5psXT2BEjMBWxlE7E9nshUtDm9rvvmp9mHAEAAXPpXPWZKcgMmDSDJvwwfaiuxf6G/bsxWrkway6DGJY6/S0AQAMdKaQB9F8jVvmP66nTffA4MoJ8NwaT8ln9ByfDrG7XqvWrSAxyQNP5uIWn1wQoAKwCndtYazLWBDwsu5YZgGv+dRwE2wemNGu9wgpLh5DTxr+RWqw8EAALg1K5ag7k28ErBpdkQTOvfEt1nTPjmvIhA2vgP5aXVBwIAASicAKTZEOznBtq3ErBn5+FxoJ/4DywoBMCpvfV3qX0XyNW+Y/rqdK3BfMWfdEOwX//LewJkZhomvCbJ6YAKqs8B/ca/351WH6wAsALoszX3TdMazLWBuwWXdEPQ1X9nNUBir+SgcnOvl4WcIPaY7Bq/Vh8IAATAqXe1BnNt4J7BJdgQzMp/541BYvPNgOQ7RLyN4vCRtVV66rEaLRGxOEGEAPjC52532P8v7p4BLJSVAFvbmt8yMtIrf6wAlO6otsOxx6/ghW7DJiaXFsSYo8vaZMg7vwQsxY2F+ugEBMClRhy/qDEzur2bifGp9n8R0WtdXGAuCPggwER3z9crGyEADnSZ6C3z9codXQXA47+KOoSNqSBAbPnz81vCnl9mgkcAtVHkQ4169fpuw8amovOY5B9VMxgAAgMmIMLnNmfDW7ACcAHPcktjpnpuNxNH1+Q5bFs7svq1XZdQMRcE9hNI8j0HnbFYASg9Y0nmFuqV5/U66jlmMvqiMfJutB8I5IYA802NmfACLR4IgEaIiKzIaQuz1Xu6PgZML76YY/4ZGRMmMIchIOCZgG2zlZfNbxl9WHMEAdAI7VsnfaYxU/lgr6ETU+2rhegjScxhDAh4JnBVo165PIkPCEASSmQXKqb6widqvKfb8BdcImuW1kV3k6FXJjKJQSDggUBMtO05Jnz9YzVeTGIeApCEUmeMyAcbs9XP9Bo+Pr14ohW6y1AwntQsxoFAVgQ6+1WhsafP1UYfSWoTApCUFNHjZk/4krmreXdPEai1TrER3WEM93wDK7lbjAQBnUDn5g+EzpifrW7TRz87AgKQhhbRFY165WPalInJxRMsBbeyoZO0sbgOAq4EYkv3VsL4D9N88u/3CQFIRd+2menU+ZmR/9amHVeT0V0SzbDYS1y/OVbzhetlJWDbxOaao54Ma9tv4KV+KEAA0lN7gEx4atL/De+sBiQIL7dx/E5jzNr07jADBA4mYMnuDmxwM4fRlf186h9oDQLQT3cJ3dq4PzyHvsZx0unjNTmCbHwmM20ikVNiio8PLB8lxlST2sC48hHo/EtvbOQppuARw7xNhO4kE9ye9ANIIwYB0Ah1uS7En23Wgw/4/DKIPkPDNBBITAACkBjV4QOXReAXwUVpVgIO7jAVBDInAAFwR/qvshi+q/lJ3uluChZAYLAEIABZ8BZ6kAJ5Rx6+RjqLdGCjPAQgAFnV2tpIAvNp5nA2qw2arEKDHRDoRgACkHlv2P8V4k8FeypbtbcGM3cNgyCQkgAEICWwpMPZ2iclCG4xZL88x5W7qcY26VyMA4FBEYAADIB059tZDQX/ISTfZzYPCEXbw9bIr0ZHaKfv75UfQHpwUWACEIACF68MoWs/PFIGBj5zhAD4pAvbzgQgAM4IexqAAPjlC+uOBCAAjgCV6RAAv3xh3ZEABMARIATAL0BY90sAAuCXL1YAfvnCuiMBCIAjQKwA/AKEdb8EIAB++WIF4JcvrDsSgAA4AsQKwC9AWPdLAALgly9WAH75wrojAQiAI0CsAPwChHW/BCAAfvliBeCXL6w7EoAAOALECsAvQFj3SwAC4JcvVgB++cK6IwEIgCNArAD8AoR1vwQgAH75YgXgly+sOxKAADgC9L8CWGrhp6/8Fqm81m27UR/BD6d4bADnFcDY5GKDTTDmMUaYLikBsXGzuWUUP7Xusf7OAjAx1f6hEJ3mMUaYLikBJrp7vl7ZWNL0B5K2uwBMRzeKyIUDiRZOSkWALX9+fkuI3vJYdWcBGN8cnUMsX/UYI0yXlIAIn9ucDW8pafoDSdtdAGpyhI1ac/jp64HUqzROLNm9vFhdj59c81tyZwHohDcxGW0VIxf4DRXWS0WA+YuNmfA9pcp5CMlmJACLJwjx/WRMOIQc4HK1EbA2IpKXN7aMPrTaUstbPpkIQCepsen2VSx0ad4SRDzFIyBMVzdnKn9VvMiLF3FmAvCCS2TN4rroB2zopOJhQMR5IWCJ7luzM9z4y2t5b15iWs1xZCYA+/YCFk+IydxlDE+sZmjIzQ+Bzos/THQ6lv5++K5kNVMB6DgYr7VOsdZ+21CAN7gGV8fCe+rc/BSaM5q16o8Ln0yBEshcAJZFYHrxRCvBrYboVQVigVCHRcDST4nis/HJP/gCeBGAThrLewJHRjNs7SU4HRh8YQvh0dpIAnNdlcPpJ2q8pxAxr7IgvQnAfk7jk4svoiC83Ep8riGzZpXxQzp9EOi85GM4+ArF0ZX41O8DYIZTvAvA/ljHLpUjaSQ+0whtIiMnWxtvYMNH4V+JM6xmLk3Ztlh5ypjgEbK8zTLdSUvB7XjDLx/FGpgA5CNdRAECIHAgAQgA+gEESkwAAlDi4iN1EIAAoAdAoMQEIAAlLj5SBwEIAHoABEpMAAJQ4uIjdRCAAKAHQKDEBCAAJS4+UgcBCAB6AARKTAACUOLiI3UQgACgB0CgxAQgACUuPlIHAQgAegAESkwAAlDi4iN1EIAAoAdAoMQE/h+ZMQfiHvfHDAAAAABJRU5ErkJggg=='),
(501, 'Portable Pixmap Format', 'image/x-portable-pix', '.ppm', ''),
(502, 'Portable Symmetric Key Container', 'application/pskc+xml', '.pskc', ''),
(503, 'PosML', 'application/vnd.ctc-', '.pml', ''),
(504, 'PostScript', 'application/postscri', '.ai', ''),
(505, 'PostScript Fonts', 'application/x-font-t', '.pfa', ''),
(506, 'PowerBuilder', 'application/vnd.powe', '.pbd', ''),
(507, 'Pretty Good Privacy', 'application/pgp-encr', '', ''),
(508, 'Pretty Good Privacy - Signature', 'application/pgp-sign', '.pgp', ''),
(509, 'Preview Systems ZipLock/VBox', 'application/vnd.prev', '.box', ''),
(510, 'Princeton Video Image', 'application/vnd.pvi.', '.ptid', ''),
(511, 'Pronunciation Lexicon Specification', 'application/pls+xml', '.pls', ''),
(512, 'Proprietary P&G Standard Reporting System', 'application/vnd.pg.f', '.str', ''),
(513, 'Proprietary P&G Standard Reporting System', 'application/vnd.pg.o', '.ei6', ''),
(514, 'PRS Lines Tag', 'text/prs.lines.tag', '.dsc', ''),
(515, 'PSF Fonts', 'application/x-font-l', '.psf', ''),
(516, 'PubliShare Objects', 'application/vnd.publ', '.qps', ''),
(517, 'Qualcomm''s Plaza Mobile Internet', 'application/vnd.pmi.', '.wg', ''),
(518, 'QuarkXpress', 'application/vnd.quar', '.qxd', ''),
(519, 'QUASS Stream Player', 'application/vnd.epso', '.esf', ''),
(520, 'QUASS Stream Player', 'application/vnd.epso', '.msf', ''),
(521, 'QUASS Stream Player', 'application/vnd.epso', '.ssf', ''),
(522, 'QuickAnime Player', 'application/vnd.epso', '.qam', ''),
(523, 'Quicken', 'application/vnd.intu', '.qfx', ''),
(524, 'Quicktime Video', 'video/quicktime', '.qt', '');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(525, 'RAR Archive', 'application/x-rar-co', '.rar', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAaqElEQVR4Xu2df5BkVXXHz73vdc/sLkqE6Vm0LH8A/lZYlR9GY1LyR6CKSsVCgwE00RLWMhYo8QfEmp3p6RkLUFRw8wNWNFomgMEilZSkRCuYSlmg4I/dKLrAiqTiEmZ6Zg3sr+nu9+5J9Sy77sJ0n/f63vv6vX7f+ffee+45n3Ped85793W3IvyBAAiUloAqbeQIHARAgCAAKAIQKDEBCECJk4/QQQACgBoAgRITgACUOPkIHQQgAKgBECgxAQhAiZOP0EEAAoAaAIESE4AAlDj5CB0EIACoARAoMQEIQImTj9BBAAKAGgCBEhOAAJQ4+QgdBCAAqAEQKDEBCECJk4/QQQACgBoAgRITgACUOPkIHQQgAKgBECgxAQhAiZOP0EEAAoAaAIESE4AAlDj5CB0EIACoARAoMQEIQImTj9BBIDMBqNX5ODLx+UrROcS8yZj4ZKXV8US6gjQMTqDZqPTNYW26fXmzUd06+A5YOcoEvAvA5NTKKUThVTHFl2it148yzGHEJgtAK2IK/nipEd41DP+wZ74JeBOAl9R5fD9HMxSbj5HWYb4xFNc7WQA6bIzZp0P11ma9ur24kcJzHwS8CMDG+srJHRPcGRCd7sNp2PwtgSQC8PTs3SYOz17+lNoNfiBwmIBzAdi4pfW6SKnvaFIbgdk/gRQC0HXmJ6TD32/W1T7/nmGHIhBwKgDd//yR0ffi4s8u9SkFgIjNN5s7q2+nO1ScnZfYKa8EnAlA957/KRN9H21/tqlOLQBEZEhtXW6EV2TrKXbLIwFnAlCb7lxLRFflMchR9mkQATjEg6/A8eAoV0ay2JwIQPeoj0ntxNP+ZNBdzhpcAEyM40GXmSimLUcCEG1jzZcVE0GxvR5cAIhwPFjs3Lvw3loAum/4mai9gJd8XKQjvQ0bAXh6NxwPpsc+MivsBWA6ehcR3z4yRAoWiAMB6EaM48GC5d2Vu9YCMDkT3czMm105BDvpCDgSABwPpsM+MrPtBWC68wMmOmtkiBQsEGcCgOPBgmXejbvWAlCbbi0R6RPduAMraQm4FIBDe+N4MG0OijzfhQC08ZHe4ZWAewHA8eDwspn9zg4EoMPZu40dDxNwLwA4HixTdUEACp5tHwLwNBIcDxa8NpK4DwFIQinHczwKQDdqHA/mOPcuXIMAuKA4RBueBQDHg0PMbRZbQwCyoOxxD+8CgONBj9kbvmkIwPBzYOVBFgKA40GrFOV6MQQg1+mRnctOAHA8KGejeDMgAMXL2TEeZycAOB4seKms6T4EoOBZzVIAcDxY8GJZw30IQMFzOgQBwPFgwWvmaPchAAVP5pAEAMeDBa+bw+5DAAqeyKEJAI4HC145h9yHABQ8jcMUABwPFrx4IADFT+DwBQDHg0WuInQARc4eEQ1fAHA8WOQSggAUOXs5EQAcDxa3iCAAxc3dqud56ACOQohPDxasniAABUvYM93NmQDgeLBg9QQBKFjCci8AOB4sVEVBAAqVrmc7m7sO4IiL+HLRIpQWBKAIWerjY34FAMeDRSgtCEARslRIAcDxYBFKCwJQhCwVVABwPJj/4oIA5D9HfT3M7y3AMW7jeDCndQYByGlikrpVEAHA8WDShGY8DwKQMXDX2xVGAHA86Dr1TuxBAJxgHJ6RIgnAIUo4HhxetTx7ZwhAnrIxgC/FEwAcDw6QZm9LIADe0GZjuHgCgOPBbCoj2S4QgGSccjuriAKA48H8lBMEID+5GMiTAgtAN14cDw6UdXeLIADuWA7FUsEFAMeDQ6ma324KARhyAmy3b+pwjOqq3cvO5FSrxVpXbffxud6Q2rrcCK/wuQdsr00AAlDwyqjosPZ4XS31CqM23dlJRK/If5g4HhxGjiAAw6DucE/DfNbyXPWBngIwE/0NMf+Fwy09mcLxoCewfc1CAIZB3eGezHzZ0lz1lp63ADOt05hpO5G2zrVDt9c0ZYzZp0P11ma9ut33XrB/iIB1UdSmOwyYwyOgFN+6OFu9pJ8Hk9PRTUz8geF5mWrn3SYOz17+lNqdahUmD0QAAjAQtjwtMnvH9lY3/vrz6mAvr15S5/H9JvoWEf1Bnjzv4wuOBzNKFAQgI9Bet2F+X3Ou+pV+e7zwSl7X3hDfyDq+tAi3A8Tmm82d1bfTHSr2yq7kxiEAo1AATA83g/A1VFeRFE5tS/v1SukPMPE5ypgX5/mIEMeDUjbtxyEA9gxzYYGZPro0V/lcLpyBE4UhAAEoTKr6O2rIHKwwnfnE3NiDIxISwsiAAAQgA8gZbvFI0Anf8sQ1qpnhntiqwAQgAAVOXg/Xf6JVeN7CrFocvdAQkWsCEADXRHNgz8RqV6jjCxbmxn6aA3fgQo4JQABynBwb14wxK4HW04s6vLHfh4Vs9sDa4hOAABQ/h30jMKR+qchcFxyo3Lpwvdo/4uEivJQEIAApgRV1uiGzX5P+N8V8D5HawUH4qw1E//dYnVpECq9zFzWxln5DACwBYrlfAtIXnvjdffStQwBGP8eFjhAC4Dd9EAC/fGHdkgAEwBKgsBwC4JcvrFsSgABYAoQA+AUI634JQAD88kUH4JcvrFsSgABYAkQH4BcgrPslAAHwyxcdgF++sG5JAAJgCRAdgF+AsO6XAATAL190AH75wrolAQiAJUB0AH4BwrpfAhAAv3zRAfjlC+uWBCAAlgDRAfgFCOt+CUAA/PJFB+CXL6xbEoAAWAJEB+AXIKz7JQAB8MsXHYBfvrBuSQACYAkQHYBfgEmsG4qbWunvsqH7lNIPkYl2VeOxPev20b5dW1UriQ3MAQEfBNAB+KC6atP8hii4VbP52sJc5X5864430DBsQQACYAFvraXGmCeU0p9RQbitWVf7HJuHORBwSgAC4AqnMREH+gY6GDaWPq32ujILOyDgkwAEwAHd1W/e1ebCpXr1xw7MwQQIZEYAAmCJWjH9axSE79lTV09ZmsJyEMicAATABrlStzRV8MEkP8ttsw3WgoAvAhCAQcl2L/7ZYDOe7g8KEOvyQAACMEAWmOhflnT4zjT/+Z93FR9fqcZ/xJreRsynK2NeypqeQ6QrA7iAJaUhYDpE9BSR/hUptZ0N/Tu1grtcPWiGAKQvpEdiHZ6R9J5/YmrlFRyEV1Mc/6nWejz9dlgBAscSMMYc0BTcRkF0bXN2fJcNHwhAGnrGRBSqM5v16nZp2QvqvD4y0TyTuYJIB9J8jINAegKmw6RvGN8bzvz68+pg+vVEEIB01K5rNipXS0u6//W1Cu5kRa+W5mIcBGwJGKKfahVfMEg3AAFISt/Q43olfLn0C7sTW9pvJDbfUjqYSGoa80DAlsDq501Yn9ucq/4kjS0IQEJazHzl0lz1hn7Tu//5ieh7uPgTQsU0pwRWRcDQW5rz448kNQwBSEBKGbNHrVRf1O+//8aP8QazPnqAiF6VwCSmgIAXAt3bgXV7w7OTPhOAACRIgyG1dbkRXtFvam260+0OPpzAHKaAgFcCiuizi43Kx5JsAgFIQMkwn7U8V+3+d1/z78R665Xa0M/wtD8BTEzxT2D1g2n8mqXZ8YelzSAAAiFDvLDcqDy/3xt/tZn2V4nVn0mwMQ4CWREwSn1leTZ8n7QfBEAgpAzftjhfvbjXtO4bfrrSfgIv+UilhvEsCRgyB1lXT5JeWIMAiFnhjzQb1Rt7TZvcEr2bFX9NNIMJIJAxAWZ1ydJceGu/bSEAUgdAdN5io3J3z/v/qejLWrPYamWce2wHAkRGfak5H14KAbAoBmXiUxfnx3/Zy0RtqvMj0vQGiy2wFAR8EXig2aicBQGwwFvthBO7r1HLPW8BplrLrPUJFltgKQh4IcAmXlqaH69BACzwNnU4RnXV7tkBTLfa+EivBWAs9UjAdJqNsSoEwAKx9MMUtekOW5gnyb6N7dW1da6+gOi5Le6crAxtUkznGOLztdbHWdtOZcDsVUZ902j6LivaMaYqjz7e/Zx7H3HNwv9R5y/Fh4eAQhFLAHMvAGvE131tOV7fuZhJX6WJT0l1HaedzPQwEV83tq9yW9LXU6UtXPov5VfyZZBxl/5L+0vxQQBKKABHQt7MldpJ0V8aY+qu32PofmmF0mpqSVe2pvnmJKmgjxl34L90gaTyJ+1kB/5LW0rxQQDKLABPxz450zot5uBOh93AQ7E2F+ypj/1cKlAX4zb+SxeIC/8kGzb+S7al+CAAEIBVAhtneLLD0bcDotOlohLGf1TthOf2OzmxtL/m8kH9ly4QH76uZXNQ/yX/pPggABCAIwS6RRhxfO/AnQDTzmoU/l7WF//hAAbxX7pApAvM5fgg/kv7S/FBACAAxxBYbUdj+kHaZwKGzH4ydOby/NgvpKL0OZ7Wf+kC8enrWrbT+i/5J8UHAYAAPIvA5JbOJ1jRdVJxHT2e5BuT0tizmZvGf+kCsfFj0LVp/Jf2kOKDAORcAGyPGZUxbdb6vxWpe4w22xL9fuFmrpx4UvyLxLcCTDubO8PX0h0qlgpyYqZ9ho715pjpbYGKX8Ra939RxbP/0gVSCP59oEvxQQBGXACeFZ5RXxrbH1wunclPbGlfqpT6onRBHxpX7202wq/2m9s9+442xH+tmd+bzGaPWY79ly4QWwHIhn9volJ8EICyCQARMdH3xveGf9hPBFYv2PHV7zkQ3hg0T27Q1ZMeq6uVXiif/r7E7xDR71pd/E8vdum/dIE4FwDn/PsTleKDAJRQAA790+7+tmF4Wb/wazPtrxOrC/vNUYb/cXG++u5+c06cif7e+j//Mzdw5L90gfgQAJf8JUGV4oMAlFUAyDCxemO/75GfmG5/QJG6qT8ivrTZqH6p15zu7yQopX4oFWr6cTf+SxeINwFwxh8dQPraSbHCd4H4tt8vVFbqpqXZ8IM9W/eZ9psMq/v62+Azl2arPS/wyZnoZmbenAJ54qku/C86fwmWFB86gNJ2AKuBP9RsVF7ZWwB40nC00A+R9H0JtelO95tpXyYV6oDj1v5LF4i/DsANf4mbFB8EoMQCYMi0lhtjPX+x+NTLeezJ50U9H+510UnflzA51WpJR31SEfcad+J/o9L3GvApAC78l9hBACRCEAA7ARjiBbQqQJb72663KS8IgA29jNb6LhDf9vti6r7AM1fp+VNm3XfTpVuAofpfcAEgB/yly0DKD24BStwBkFJ/25wNP2TzEFAqMJ8tdNE7ABf8IQASActx3wXu237v8A0rVq9fnKvu6DUnyTHg8Pw/5LXt/rbrBy8vN/yl/aX40AGUtAOQjtC6WJK8CCQVGDqAtQvMFX8IgETActx3gfu23yP8/9ygw3P7vb77gjqvb0XtBelV4CH5fyQs2/1t1w9YXs74S/tL8aEDKFUHYFhRsG29Dj7S7+LvIkn6YSCpwNABHF1g7vlDACQCluO+C9y3/e5Rk2b9K9LqHmXMtn73/EdQpfg4sG//pfTZ7m+7XvLPN39pfyk+dAA57wCkBPsYr013riKia5PYlgosiQ2fc6QOJI/+p+EvsZPigwBAAI4hUKu3N5mI70v6lWBSgUkF6nu8aAKQlr/ET8oPBAACcITA5Cd5Y6Tie4OAT5YK6/C4VGBJ7fiaVyQBGIS/xE3KDwQAArBKoFbnkziK7laaTpOK6uhxqcDS2PIxtygCMCh/iZmUHwgABIAm6u03kNHfUMQvlQrqmeNSgaW153p+EQTAhr/ES8oPBKDEArD6ab/fiT5ulJnSpMekYlprXCqwQWy6XJNnAXDBX2Il5QcCUEIBOKHOzw1M5z1M9HFF6sVSEfUblwrMxraLtXkUAJf8JUZSfiAAIy4AtTofZzp0vNbRyaR4E7M6h5U5V5NeJxVPknGpwJLY8Dln2ALgm7/ETsoPBCDnAtCrgA2Zu5cbY+dJBXDidOcbmugd0rxBx6UCky7AQfc9vM52/0HX54W/xE+KDwJQUAE45Hb/L+Tszuh+pj+OWw8qHUxIxTLIuFRgoyoAeeEv5UzKDwSgyAJgzFMxV1+751Pqf/qFUZuO3kXEt0vFMsi4VGAjLQA54C/lTMoPBKDIAkBEw25FpQIbaQHIAX8IgETActx3gbuxP7xbATf+D54k2/1t1+f9VkCKDx1AwTuAVfeTtqJbogtJ8dcHv9yevVIqsFHvAIbNX8qllB8IwCgIQJpWdEvnDq3onVLhJB2XCqwUAjBE/lKepPxAAEZEAIbVikoFVhYBGBZ/CIBEwHLcd4E7tT+EWwGn/g+QK9v9bdcf4/IQ+EvIpPjQAYxUB5DiVMDRrYBUYOXqALLnDwGQCFiO+y5wP/azOxXw43/ypNnub7t+bU+z4y+RkuJDBzBiHUDWT6WlAitbB5A1fwiARMBy3HeB+7Kf+AUhy1sBX/4nTZvt/rbre/mZFX+JkxQfOoBR7ACOxCS3oif9FdeioPXzQT8rIBVYKTuADPlDACQCluO+C9yr/QyeSnv1P0HubPe3Xd/XxQz4S4ik+NABjHQH4P+ptFRg5e4A/POHAEgELMd9F7hv+4fC93crkI3/vZNou7/t+mTl5Y+/tL8UHzqAEe8AfD+Vlgqs7B2Ab/4QAImA5bjvAvdt/3D4vp5KZ+V/rzTa7m+7Pml5+eIv7S/Fhw6gDB1AyqfSnUrrQU1BTSqu7rhUYOgAjqaY7FYgDX8pR1J+IABlEgAPT6WlAoMAHFVgHvhDACQCluO+C9y3/WeG77oVzdr/Z8Zju7/t+rTl5Zq/tL8UHzqAMnUAHm4FpAJDB7BWgWV3KyDlBwJQRgFI2opOR39CxP/UD5FUYBCANeg55I8OQCJgOd7U4RjVVbuXmdp0q02kK5bbYDkIOCegjGkvzo/1/ck3dAAC9monnNh9jVruNW1yqrXMWp/gPHswCAKWBAzFzeXG+GQ/MxAACbKKX9acHd/VuwPo/JCI3iiZwTgIZE1AEd2/2KicDQGwIK+IzltsVO7uKQBT0S2k+f0WW2ApCHghoIz64uJ8uBkCYIWXP9JsVG/sZWJiOrpEEf+D1RZYDAIeCDCri5bmwr6/CIVbAAm84tubs9WLek3r/tSzMu0nXP3aruQOxkEgCQFjzAEdVjc262ofOoAkxHrMMcQLy43K84kU9zJz4lT0Za35fRbbYCkIuCWg1C3N2fAyySg6AIlQ90cfmM9anqs+0PM2YGbl5SpWD5LWYQJzmAICngmYjjL8qsX58V9KG0EAJELdcUVfaM5WPtxv6uR053om+mgSc5gDAp4JXNdsVK5OsgcEIAklMssVXX3R43V1oNf0F17J61obovtJ02sTmcQkEPBAICba8VwdvumxulpJYh4CkIRSdw7zh5tz1S/0m16bWTnVMN2b9KO0SbfGPBBIQqD7vCrU5s0L9fFHk8w/1Nxa/vl+19vSPZfLd+sD4SsWrlf7+4pAvb3JRHS31qrvG1guHYMtEOhe/AHTuYtz1R1paEAA0tAiuqbZqHxSWjI5tXKKoeBOpek0aS7GQcCWQGxoeyWM35HmP//hPSEAqeibjlJ0xuLs2H9Jy15S5/F9HM0qNlfiw0ISLYwPRsB0SOnPHr8nrO/aqlqD2IAApKf2EOnwDOkFi8Nmu90AB+HVJo4v1lqvT78dVoDAsQQMmf2BCW5VYXTtIP/1j7YGARikupjubO4ML6Q7VJx0ea3Ox5GJz1eKziHmTTHFLw2MOp61ria1gXnlI9D9SG+s+UlFwaNaqR3MdA/p4K6k/4AkYhAAiVCPcSb1d0uN4EP93hAc0DSWgUBmBCAAFqhXReAXweVpOgGL7bAUBJwTgADYI/1nXgn/fOnTaq+9KVgAgWwJQABc8GZ6mAJ+V7Ne3e7CHGyAQFYEIACuSBsTcaA/p1Q45+oBjSvXYAcEehGAADivDfO/TOozwYHKNumtQedbwyAIpCQAAUgJLOl0ZcweDoLbNZmvLajK/VRXJulazAOBrAhAADIg3f12Vk3BfzDxfUrph5iiXWF77DfjY7T3sTq1cJSYQRKwxZoEIAAojFwTkH54JNfOF8A5CEABklRmFyEAfrMPAfDLF9YtCUAALAEKyyEAfvnCuiUBCIAlQAiAX4Cw7pcABMAvX3QAfvnCuiUBCIAlQHQAfgHCul8CEAC/fNEB+OUL65YEIACWANEB+AUI634JQAD88kUH4JcvrFsSgABYAkQH4BcgrPslAAHwyxcdgF++sG5JAAJgCRAdgF+AsO6XAATAL190AH75wrolAQiAJUB0AH4BwrpfAhAAv3zRAfjlC+uWBCAAlgD9dwCtNn76ym+SymvddJqNMfxwiscCsO4AJqZWmkoHEx59hOmSEmATLy3Nj9dKGn4mYVsLwOR05wdMdFYm3mKTUhFQRPcvNipnlyrojIO1F4CZ6GZm3pyx39iuBASUUV9cnA9RWx5zbS0AtS3RhaT46x59hOmSEmBWFy3NhbeXNPxMwrYXgDofZ6L2An76OpN8lWYTQ+agWqluxE+u+U25tQB03Zucirax5sv8ugrrpSKg1Jebs+H7SxXzEIJ1JAArpzCpnaR1OIQYsOWoETAmIuJXN+fHHxm10PIWjxMB6AY1MdO5TjF9Im8Bwp/iEWBF1y/NVj5ePM+L57EzAXjhlbxuZUP0faXptOJhgMd5IWCIfrpub3j2rz+vDubFp1H2w5kAHHoWsHJKTPperdXkKENDbH4IdF/8UURvRuvvh+9aVp0KQHeDWr29yRjzbU0B3uDKLo+F36l78VOoz12qV39c+GAKFIBzAVgVgZmVUw0Hd2qi1xWIBVwdFgFDPyOKL8B//uwT4EUAumGsPhN4TjSrjLkSpwPZJ7YQOxoTcaBvqKpw5vG6OlAIn0fMSW8CcJhTbWrlZRSEVxuOL9Kk140YP4QzAIHuSz5aBbdRHF2L//oDAHS4xLsAHPZ14hP8HBqLz9dM55Dm042JT1ZaHY+PEjvMZi5NmQ4bflLr4FEyaodRdA+1grvwhl8+kpWZAOQjXHgBAiBwNAEIAOoBBEpMAAJQ4uQjdBCAAKAGQKDEBCAAJU4+QgcBCABqAARKTAACUOLkI3QQgACgBkCgxAQgACVOPkIHAQgAagAESkwAAlDi5CN0EIAAoAZAoMQEIAAlTj5CBwEIAGoABEpMAAJQ4uQjdBCAAKAGQKDEBP4fjW6y4vf6sDgAAAAASUVORK5CYII='),
(526, 'Real Audio Sound', 'audio/x-pn-realaudio', '.ram', ''),
(527, 'Real Audio Sound', 'audio/x-pn-realaudio', '.rmp', ''),
(528, 'Really Simple Discovery', 'application/rsd+xml', '.rsd', ''),
(529, 'RealMedia', 'application/vnd.rn-r', '.rm', ''),
(530, 'RealVNC', 'application/vnd.real', '.bed', ''),
(531, 'Recordare Applications', 'application/vnd.reco', '.mxl', ''),
(532, 'Recordare Applications', 'application/vnd.reco', '.musi', ''),
(533, 'Relax NG Compact Syntax', 'application/relax-ng', '.rnc', ''),
(534, 'RemoteDocs R-Viewer', 'application/vnd.data', '.rdz', ''),
(535, 'Resource Description Framework', 'application/rdf+xml', '.rdf', ''),
(536, 'RetroPlatform Player', 'application/vnd.cloa', '.rp9', ''),
(537, 'RhymBox', 'application/vnd.jisp', '.jisp', ''),
(538, 'Rich Text Format', 'application/rtf', '.rtf', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAZIElEQVR4Xu2df4wd1XXHz70z7+3aBtzAvjVBKAFDQn6CE4gdJUqr+o+ChKpGJIUCTSsUMEojfrjhV6P17tu3i4CEBIiVlpgfSkTEjxK5ahWqkKq0qiII0Aa7hMaA+VEVU3bfrlPwr33vzdxTvTU22Pi9OzN37szcN9/9d+4995zPOfN95747s08Q/kAABEpLQJQ2cgQOAiBAEAAUAQiUmAAEoMTJR+ggAAFADYBAiQlAAEqcfIQOAhAA1AAIlJgABKDEyUfoIAABQA2AQIkJQABKnHyEDgIQANQACJSYAASgxMlH6CAAAUANgECJCUAASpx8hA4CEADUAAiUmAAEoMTJR+ggAAFADYBAiQlAAEqcfIQOAhAA1AAIlJgABKDEyUfoIAABQA2AQIkJQABKnHyEDgIQANQACJSYAASgxMlH6CCQmQDU6nwUqfBcIWgtMa9SKlwppFhOJCtIQ3ICzUalbw5r4+0rmo3qxuQrYOYgE7AuAKNjC6cQ+deHFF4spVw6yDDziE0vAK2AyfujuYb/SB7+Yc1iE7AmACfVeXgPBxMUqmtISr/YGNz1Ti8AHVZK7Za++EKzXt3ibqTw3AYBKwKwor6wsqO8zR7RGTachs13CEQRgLdH71Chv2b+RrED/EDgAIHUBWDFhtYnAyH+SZJYAcz2CcQQgK4zz5D0f7dZF7vte4YVXCCQqgB0P/kDJR/HzZ9d6mMKABGrnza3Vb9ID4swOy+xUlEJpCYA3T3/Wyr4Jdr+bFMdWwCISJHYON/wr8zWU6xWRAKpCUBtvHMzEV1fxCAH2ackArCfB1+J48FBroxosaUiAN2jPiaxDd/2R4Oe5qjkAqBCHA+mmQk3baUkAMEmlnyZmwjc9jq5ABDheNDt3KfhvbEAdJ/wU0F7Bg/5pJGO+DZMBODt1XA8GB/7wMwwF4Dx4AIifnBgiDgWSAoC0I0Yx4OO5T0td40FYHQi+AEzr0vLIdiJRyAlAcDxYDzsAzPaXADGO08y0eqBIeJYIKkJAI4HHct8Ou4aC0BtvDVHJI9Lxx1YiUsgTQHYvzaOB+PmwOXxaQhAG6/05lcC6QsAjgfzy2b2K6cgAB3O3m2seIBA+gKA48EyVRcEwPFs2xCAt5HgeNDx2ojiPgQgCqUCj7EoAN2ocTxY4Nyn4RoEIA2KOdqwLAA4Hswxt1ksDQHIgrLFNawLAI4HLWYvf9MQgPxzYORBFgKA40GjFBV6MgSg0OnRO5edAOB4UJ8N90ZAANzL2SEeZycAOB50vFSO6D4EwPGsZikAOB50vFiO4D4EwPGc5iAAOB50vGbe7T4EwPFk5iQAOB50vG4OuA8BcDyRuQkAjgcdr5z97kMAHE9jngKA40HHiwcC4H4C8xcAHA+6XEXoAFzOHhHlLwA4HnS5hCAALmevIAKA40F3iwgC4G7uFj0vQgfwLoR4e9CxeoIAOJaww90tmADgeNCxeoIAOJawwgsAjgedqigIgFPpeq+zhesADrqIfy7qQmlBAFzIUh8fiysAOB50obQgAC5kyUkBwPGgC6UFAXAhS44KAI4Hi19cEIDi56ivh8XdAhziNo4HC1pnEICCJiaqW44IAI4HoyY043EQgIyBp72cMwKA48G0U5+KPQhAKhjzM+KSAOynhOPB/KrlvStDAIqUjQS+uCcAOB5MkGZrUyAA1tBmY9g9AcDxYDaVEW0VCEA0ToUd5aIA4HiwOOUEAShOLhJ54rAAdOPF8WCirKc3CQKQHstcLDkuADgezKVq3lkUApBzAkyXb0p/iOqi3cvO6FirxVJWTdexOV+R2Djf8K+0uQZsH5kABMDxyqhIv/Z6Xcz1CqM23tlGRKcVP0wcD+aRIwhAHtRTXFMxr56fqj7dUwAmgu8T81+kuKQlUzgetAS2r1kIQB7UU1yTmS+bm6re3XMLMNE6nZm2EEnjXKfo9hFNKaV2S198oVmvbrG9FuzvJ2BcFLXxDgNmfgSE4PtnJ6sX9/NgdDy4k4kvz8/LWCvvUKG/Zv5GsSPWLAxORAACkAhbkSapXUO7qiteu03s6+XVSXUe3qOCnxHR7xXJ8z6+4Hgwo0RBADICbXUZ5kuaU9Uf9lvjxPW8pL0svINleKkL2wFi9dPmtuoX6WERWmVXcuMQgEEoAKYXmp7/caqLQBdObUP7U0LIy5l4rVDqg0U+IsTxoC6b5tchAOYMC2GBmb4xN1X5biGcgRPOEIAAOJOq/o4qUvsqTJ95Y2rouQEJCWFkQAACkAHkDJd40ev4n3/jJtHMcE0s5TABCIDDyevh+jNS+OfMTIrZwQsNEaVNAAKQNtEC2FOh2O7L8LyZqaFnC+AOXCgwAQhAgZNj4ppSasGTcnxW+nf0e1nIZA3MdZ8ABMD9HPaNQJF4SZC6xdtbuX/mVrFnwMNFeDEJQABiAnN1uCK1R5L8R8H8GJHYyp7/yjKi/3u1Ti0igce5XU2sod8QAEOAmG6XgO4fnthdffCtQwAGP8dORwgBsJs+CIBdvrBuSAACYAhQMx0CYJcvrBsSgAAYAoQA2AUI63YJQADs8kUHYJcvrBsSgAAYAkQHYBcgrNslAAGwyxcdgF2+sG5IAAJgCBAdgF2AsG6XAATALl90AHb5wrohAQiAIUB0AHYBwrpdAhAAu3zRAdjlC+uGBCAAhgDRAdgFCOt2CUAA7PJFB2CXL6wbEoAAGAJEB2AXIKzbJQABsMsXHYBdvrBuSAACYAgQHYBdgFGsKwqbUsh/YUVPCCGfJxVsr4ZDO5fspt3bN4pWFBsYAwI2CKADsEF10ab6LZF3v2R138xU5Sn81x1roGHYgAAEwADekaYqpd4QQn5beP6mZl3sTtk8zIFAqgQgAGnhVCpgT95O+/zG3LfErrTMwg4I2CQAAUiB7uJ/3pXq/Ll69VcpmIMJEMiMAATAELVg+ofA87+ysy7eMjSF6SCQOQEIgAlyIe5uCu9rUX6W22QZzAUBWwQgAEnJdm/+SW8dvt1PChDzikAAApAgC0z093PS/3KcT/73Xc/LK9XwD1nS7xPzGUKpk1nS0USyksAFTCkNAdUhoreI5CskxBZW9M/U8h5J64tmCED8QnoxlP5ZUff8I2MLp7Hn30Bh+CdSyuH4y2EGCBxKQCm1V5L3AHnBzc3J4e0mfCAAcegpFZAvPtOsV7fopp1Q56WBCqaZ1JVE0tONx3UQiE9AdZjk7cO7/InXbhP74s8nggDEo3ZLs1G5QTel+6kvhbeZBX1MNxbXQcCUgCJ6VorwvCTdAAQgKn1Fr8sF/8O6X9gd2dA+k1j9TEhvJKppjAMBUwKL75uwPLs5VX0mji0IQERazLx+bqp6e7/h3U9+IvoFbv6IUDEsVQKLIqDo883p4RejGoYARCAllNopFqof6Pfpv+IaXqaWBk8T0UcjmMQQELBCoLsdWLLLXxP1OwEIQIQ0KBIb5xv+lf2G1sY73e7gqgjmMAQErBIQRN+ZbVSuibIIBCACJcW8en6q2v10P+LfcfXWR6SiX+Pb/ggwMcQ+gcUX0/jjc5PDL+gWgwBoCCnimflG5f39nvirTbR/RCz+TAcb10EgKwJKiB/OT/qX6NaDAGgICcUPzE5XL+o1rPuEn6y038BDPrpSw/UsCShS+1hWj9c9sAYB0GaFr242qnf0Gja6IfhTFnyf1gwGgEDGBJjFxXNT/v39loUA6DoAonNmG5VHe+7/x4J7pWRtq5Vx7rEcCBApcU9z2r8UAmBQDEKFp85OD7/Uy0RtrPMfJOnTBktgKgjYIvB0s1FZDQEwwFvt+CM7bhLzPbcAY615lvJYgyUwFQSsEGAVzs1ND9cgAAZ4m9Iforpo9+wAxlttvNJrABhTLRJQnWZjqAoBMECs+2GK2niHDcyTzr6J7cW5da6eQHRMizsrhaJVgmmtIj5XSnmUsW0i+/4bOqnLj3X+lv3XmdfFhy8BNQR1AHUFZpog3fwk17uPLYdLOxcxyesl8SlJbByYo+NjYjuNubr8uO6/jpEuPghACQXgYMjruFI7PvhLpVQ96XMMugLTFajt6xCASt97HAJQZgF4O/bRidbpIXubk3QDEAC7EqYTMN3quvxAACAAiwRWTPBoh4Ofe0Rn6Irq3dd1BRbHlo2xuhvIdf91zHTxQQAgAAcJdEUg4PDxOJ2ArsB0BWr7OgQAWwCjGtMVuK7AdIvr7Ovmp319cTsQ0pNRvxMomv+H89Dlx3X/dfnXxYcOAB3AewiMbuhcx4Ju0RVX97quwKLYsDkGAoAOwKi+dAWuKzDd4rbtC6XaLOV/CxKPKak2Rfr9wnVcOe748DdxtgK6OHtdtx2/zq+819f5Z3pdFx86gIJ3AKYC857wlLhnaI93he5fRo1saF8qhLjLtAB183UFmnr8hzmU9/o6PqbXdfFBAMomAETERL8Y3uX/QT8R6D4sFAwv/p+DVJ4YRAdgeisnmw8BSMbt4CwdQNNPKNv2e4a/+NuG/mX98NQm2g8Ri/MNEfadnlv8b3uV9/o22Ub5jgYdQAk7gP0hKyYWZ/b7P/Ij4+3LBYk7bRZp3jdg3uvbZAsBSIGu7QKxbb8fAhbizrlJ/2u9xqyYaH9WsXgiBYw9TeQZf5QbxLTDs8kuim0dX3QApe0AFgN/vtmofKS3APCo4mAmSqElHaMrUNs3YN7rJ+UWdZ4uPghAiQVAkWrNN4Z6/mLxqVfw0JvvCxaiFluScboChQAkofrOHB1fCAAEAALQpwZsC5DZ7a2fDQHQM+o7QgfQtEBs2+8bHNO25lSl50+Zdd8NwBbA7B++GJaf8XRdfaEDKHEHQEL8dXPS/zq+BOxdBKYCb3wHGxqAAFgGaFogugSZ2u8dvmLB4lOzU9WtvcbgGJDIHn/Dwow4XVdf6ABK2gHojgC7WPAgEARAqzOuK6QuQJ2CmsZv236P+P5tmfTPfrUuen7Df0Kdl7aC9gweBcZ3AH3vEdMbQHcD5n3d9g1q2/6h/BQL8jYtld7V/W7+7hy8DLSfnOv1rasvbAEGfAvQPeuXLF8hKR4TSm3qt+c/iAKvAx9EAQHQ3CCuA9J1GDoFNY1fZ1/nn43rtfHO9UR0sw3bh9vUxW/KVxeDbn3dfNPreceHDqDgHYBpgcWdX6u3V6mAn4j6L8Hi2ocAHEoAAmBaQZbn6z4hTBOos285vEPMj36TVwQifNzzeGVW6+riN+Wri0O3vm6+6fW840MHgA5g/5dddT6eg+BRIel006KOM193A+Z9g8SJJcnYvOODAEAAaKTe/jQp+RNBfHKSIjaZAwGwe8yo4wsBKLEALL7t9zvBtUqoMUlyyORGTjpXV6B5f0ImjSvqvLzjgwCUUACOrfMxnup8hYmuFSQ+GLVYbYyDAKADsFFXqdm0XaA6+6aB1Op8lOrQcimDlSR4FbNYy0KdLUkuMbWdxnxd/Hl/QqYRYz8beceHDqDgHUCvAlGkHp1vDJ2jK9Djxjs/kURf0o3L63rRBcD2DWqbu44vBMBRAdjvNl/abFTv6RdC953+MGw9J6Q3YrvYktjXFajtGzDv9ZMwizNHFx8EwGUBUOqtkKuf2Hmj+J/+bWZwARE/GKdwshqrK1AIgFkmdHwhAC4LQPefezu+FdAVKAQAAmBGwHC27QJNx767W4F04k+e5LzXT+55tJm6+NABON4BLLofdSuwITifBD8UrXSyGaUrUHQAZnnQ8YUADIIAxNkKbOg8LAV92ays0putK1AIgBlrHV8IwIAIgKunAroChQBAAMwIGM62XaCp2ndwK5Bq/Alynff6CVyONUUXHzqAgeoAYpwKFGQroCtQdACx7vf3DNbxhQAMmAC4thXQFSgEAAJgRsBwtu0CtWLfoa2Alfhj5Dzv9WO4mmioLj50AAPZAbizFdAVKDqARPf9wUk6vhCAARWAqFuB4/+Ka4HX+q+83hXQFSgEAAJgRsBwtu0CtWrfga2A1fgj5D7v9SO4aDREFx86gIHuAIq/FdAVKDoAo/ufdHwhAAMuAEXfCugKFAIAATAjYDjbdoHatr8YfoG3ApnE36cG8l7fsDy103XxoQMoRQdQ3K2ArkDRAWjv8b4DdHwhACURgDhbgU6l9Zwkr2ZWetFm6woUAhCNY69ROr4QgDIJQAG3AroChQBAAMwIGM62XaC27R8efuT/IJTRuwJZx384j7zXNyxP7XRdfOgAytQBHIxV/x+Eug8IZbEV0BUoOgDtPY7vAMwQ9Z9tu0Bt2z9idFG3AuPBHxPx37rMV+d7Lvx1TqV4XRcfOgBdByD9IaqLdq9htfFWm0hWUswZTIFAKgSEUu3Z6aG+P/kGAdCgrnb8kR03iflew0bHWvMs5bGpZAxGQCBFAorC5nxjeLSfSQiADrgIP9ScHN7euwPo/DsRnakzg+sgkDUBQfTUbKOyBgJgQF4QnTPbqDzaUwDGgrtJ8lcNlsBUELBCQChx1+y0vw4CYISXr242qnf0MjEyHlwsiH9stAQmg4AFAsziwrkpv+8vQmELoAMv+MHmZPXCXsO6P7UtVPuNovzari4cXC8HAaXUXulXVzTrYjc6AIOcK+KZ+Ubl/USCe5k5biy4V0q+xGAZTAWBdAkIcXdz0r9MZxQdgI5Q92U65tXzU9Wne24DJhY+LELxHEnpRzCHISBgmYDqCMUfnZ0efkm3EARAR6h7XdD3mpOVq/oNHR3v3MpE34hiDmNAwDKBW5qNyg1R1oAARKFEar4iqx94vS729hp+4npe0loWPEWSPhHJJAaBgAUCIdHWY6T/2VfrYiGKeQhAFErdMcxXNaeq3+s3vDaxcKpiejyrV2mjuo5x5SDQ/b7Kl+pzM/Xhl6NGDAGISopoh9zrnzZzq9jTVwTq7VUqoEelFH2fwIq+LEaCgJ5A9+b3mM6enapu1Y9+ZwQEIA4topuajco3dVNGxxZOUeRtFpJO143FdRAwJRAq2lLxwy/F+eQ/sCYEIBZ91RGCzpqdHPpP3bST6jy8m4NJwWo9XhbS0cL1ZARUh4T8zvKdfn37RtFKYgMCEJ/a8yT9s3QPWBww2+0G2PNvUGF4kZRyafzlMAMEDiWgSO3xlHe/8IObk3zqv9saBCBJdTFtbm7zz6eHRRh1eq3OR5EKzxWC1hLzqpDCkz0llrOU1ag2MK58BLqv9IaS3xTkvSyF2MpMj5H0Hon6AaQjBgHQEepxnUn8zVzD+3q/JwQTmsY0EMiMAATAAPWiCPzGuyJOJ2CwHKaCQOoEIADmSP+OF/w/n/uW2GVuChZAIFsCEIA0eDO9QB5f0KxXt6RhDjZAICsCEIC0SCsVsCe/K4Q/ldYXNGm5Bjsg0IsABCD12lD/yyS+7e2tbNI9NZj60jAIAjEJQABiAos6XCi1kz3vQUnqvhlReYrqQkWdi3EgkBUBCEAGpLv/nVWS969M/IQQ8nmmYLvfHvrt8BDterVOLRwlZpAELHFEAhAAFEahCeh+2KLQzjvgHATAgSSV2UUIgN3sQwDs8oV1QwIQAEOAmukQALt8Yd2QAATAECAEwC5AWLdLAAJgly86ALt8Yd2QAATAECA6ALsAYd0uAQiAXb7oAOzyhXVDAhAAQ4DoAOwChHW7BCAAdvmiA7DLF9YNCUAADAGiA7ALENbtEoAA2OWLDsAuX1g3JAABMASIDsAuQFi3SwACYJcvOgC7fGHdkAAEwBAgOgC7AGHdLgEIgF2+6ADs8oV1QwIQAEOA9juAVhs/fWU3SeW1rjrNxhB+OMViARh3ACNjC00hvRGLPsJ0SQmwCufmpodrJQ0/k7CNBWB0vPMkE63OxFssUioCguip2UZlTamCzjhYcwGYCH7AzOsy9hvLlYCAUOKu2WkftWUx18YCUNsQnE+CH7LoI0yXlACzuHBuyn+wpOFnEra5ANT5KBW0Z/DT15nkqzSLKFL7xEJ1BX5yzW7KjQWg697oWLCJJV9m11VYLxUBIe5tTvpfLVXMOQSbkgAsnMIktpGUfg4xYMlBI6BUQMQfa04PvzhooRUtnlQEoBvUyETnFsF0XdEChD/uEWBBt85NVq51z3P3PE5NAE5cz0sWlgW/FJJOdw8DPC4KAUX07JJd/prXbhP7iuLTIPuRmgDs/y5g4ZSQ5ONSitFBhobY7BDoPvgjiD6H1t8O3yNZTVUAugvU6u1VSqmfS/LwBFd2eXR+pe7NT748e65e/ZXzwTgUQOoCsCgCEwunKvY2S6JPOsQCruZFQNGvicLz8MmffQKsCEA3jMXvBI4OJoVS63E6kH1inVhRqYA9eXtV+BOv18VeJ3weMCetCcABTrWxhQ+R59+gOLxQklwyYPwQTgIC3Yd8pPAeoDC4GZ/6CQCmOMW6ABzwdeQ6PpqGwnMl01qSfIZS4UohxXK8SpxiNgtpSnVY8ZtSei+TEluVoMeo5T2CJ/yKkazMBKAY4cILEACBdxOAAKAeQKDEBCAAJU4+QgcBCABqAARKTAACUOLkI3QQgACgBkCgxAQgACVOPkIHAQgAagAESkwAAlDi5CN0EIAAoAZAoMQEIAAlTj5CBwEIAGoABEpMAAJQ4uQjdBCAAKAGQKDEBCAAJU4+QgcBCABqAARKTOD/AWKA5rUrO6DNAAAAAElFTkSuQmCC'),
(539, 'Rich Text Format (RTF)', 'text/richtext', '.rtx', ''),
(540, 'ROUTE 66 Location Based Services', 'application/vnd.rout', '.link', ''),
(541, 'RSS - Really Simple Syndication', 'application/rss+xml', '.rss,', ''),
(542, 'S Hexdump Format', 'application/shf+xml', '.shf', ''),
(543, 'SailingTracker', 'application/vnd.sail', '.st', ''),
(544, 'Scalable Vector Graphics (SVG)', 'image/svg+xml', '.svg', ''),
(545, 'ScheduleUs', 'application/vnd.sus-', '.sus', ''),
(546, 'Search/Retrieve via URL Response Format', 'application/sru+xml', '.sru', ''),
(547, 'Secure Electronic Transaction - Payment', 'application/set-paym', '.setp', ''),
(548, 'Secure Electronic Transaction - Registration', 'application/set-regi', '.setr', ''),
(549, 'Secured eMail', 'application/vnd.sema', '.sema', ''),
(550, 'Secured eMail', 'application/vnd.semd', '.semd', ''),
(551, 'Secured eMail', 'application/vnd.semf', '.semf', ''),
(552, 'SeeMail', 'application/vnd.seem', '.see', ''),
(553, 'Server Normal Format', 'application/x-font-s', '.snf', ''),
(554, 'Server-Based Certificate Validation Protocol - Validation Policies - Request', 'application/scvp-vp-', '.spq', ''),
(555, 'Server-Based Certificate Validation Protocol - Validation Policies - Response', 'application/scvp-vp-', '.spp', ''),
(556, 'Server-Based Certificate Validation Protocol - Validation Request', 'application/scvp-cv-', '.scq', ''),
(557, 'Server-Based Certificate Validation Protocol - Validation Response', 'application/scvp-cv-', '.scs', ''),
(558, 'Session Description Protocol', 'application/sdp', '.sdp', ''),
(559, 'Setext', 'text/x-setext', '.etx', ''),
(560, 'SGI Movie', 'video/x-sgi-movie', '.movi', ''),
(561, 'Shana Informed Filler', 'application/vnd.shan', '.ifm', ''),
(562, 'Shana Informed Filler', 'application/vnd.shan', '.itp', ''),
(563, 'Shana Informed Filler', 'application/vnd.shan', '.iif', ''),
(564, 'Shana Informed Filler', 'application/vnd.shan', '.ipk', ''),
(565, 'Sharing Transaction Fraud Data', 'application/thraud+x', '.tfi', ''),
(566, 'Shell Archive', 'application/x-shar', '.shar', ''),
(567, 'Silicon Graphics RGB Bitmap', 'image/x-rgb', '.rgb', ''),
(568, 'SimpleAnimeLite Player', 'application/vnd.epso', '.slt', ''),
(569, 'Simply Accounting', 'application/vnd.accp', '.aso', ''),
(570, 'Simply Accounting - Data Import', 'application/vnd.accp', '.imp', ''),
(571, 'SimTech MindMapper', 'application/vnd.simt', '.twd', ''),
(572, 'Sixth Floor Media - CommonSpace', 'application/vnd.comm', '.csp', ''),
(573, 'SMAF Audio', 'application/vnd.yama', '.saf', ''),
(574, 'SMAF File', 'application/vnd.smaf', '.mmf', ''),
(575, 'SMAF Phrase', 'application/vnd.yama', '.spf', ''),
(576, 'SMART Technologies Apps', 'application/vnd.smar', '.teac', ''),
(577, 'SourceView Document', 'application/vnd.svd', '.svd', ''),
(578, 'SPARQL - Query', 'application/sparql-q', '.rq', ''),
(579, 'SPARQL - Results', 'application/sparql-r', '.srx', ''),
(580, 'Speech Recognition Grammar Specification', 'application/srgs', '.gram', ''),
(581, 'Speech Recognition Grammar Specification - XML', 'application/srgs+xml', '.grxm', ''),
(582, 'Speech Synthesis Markup Language', 'application/ssml+xml', '.ssml', ''),
(583, 'SSEYO Koan Play File', 'application/vnd.koan', '.skp', ''),
(584, 'Standard Generalized Markup Language (SGML)', 'text/sgml', '.sgml', ''),
(585, 'StarOffice - Calc', 'application/vnd.star', '.sdc', ''),
(586, 'StarOffice - Draw', 'application/vnd.star', '.sda', ''),
(587, 'StarOffice - Impress', 'application/vnd.star', '.sdd', ''),
(588, 'StarOffice - Math', 'application/vnd.star', '.smf', ''),
(589, 'StarOffice - Writer', 'application/vnd.star', '.sdw', ''),
(590, 'StarOffice - Writer (Global)', 'application/vnd.star', '.sgl', ''),
(591, 'StepMania', 'application/vnd.step', '.sm', ''),
(592, 'Stuffit Archive', 'application/x-stuffi', '.sit', ''),
(593, 'Stuffit Archive', 'application/x-stuffi', '.sitx', ''),
(594, 'SudokuMagic', 'application/vnd.sole', '.sdkm', ''),
(595, 'Sugar Linux Application Bundle', 'application/vnd.olpc', '.xo', ''),
(596, 'Sun Audio - Au file format', 'audio/basic', '.au', ''),
(597, 'SundaHus WQ', 'application/vnd.wqd', '.wqd', ''),
(598, 'Symbian Install Package', 'application/vnd.symb', '.sis', ''),
(599, 'Synchronized Multimedia Integration Language', 'application/smil+xml', '.smi', ''),
(600, 'SyncML', 'application/vnd.sync', '.xsm', ''),
(601, 'SyncML - Device Management', 'application/vnd.sync', '.bdm', ''),
(602, 'SyncML - Device Management', 'application/vnd.sync', '.xdm', ''),
(603, 'System V Release 4 CPIO Archive', 'application/x-sv4cpi', '.sv4c', ''),
(604, 'System V Release 4 CPIO Checksum Data', 'application/x-sv4crc', '.sv4c', ''),
(605, 'Systems Biology Markup Language', 'application/sbml+xml', '.sbml', ''),
(606, 'Tab Seperated Values', 'text/tab-separated-v', '.tsv', ''),
(607, 'Tagged Image File Format', 'image/tiff', '.tiff', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAWXElEQVR4Xu2dbYxc1XnHn+fcO7NrE+QU76yTKEoJLyElItAWbIkorcKXIFGpURqRkJC2qIGorTBxm+KoXe/Ozi4FE5oAVtWGpLSqW+IIyVWr0JRWpf3Q5sVQ4iS0GDAJlWLj3ZmFOn7bnbn3nGoWY0HMzDN3z9w559z799d7zz3P8/uf+c2ZObtrJvwDARAoLQEubedoHARAgCAALAIQKDEBCKDE4aN1EIAAsAZAoMQEIIASh4/WQQACwBoAgRITgABKHD5aBwEIAGsABEpMAAIocfhoHQQgAKwBECgxAQigxOGjdRCAALAGQKDEBCCAEoeP1kEAAsAaAIESE4AAShw+WgcBCABrAARKTAACKHH4aB0EIACsARAoMQEIoMTho3UQgACwBkCgxAQggBKHj9ZBAALAGgCBEhOAAEocPloHgYEFUKubN5FOr2Oma8iYK7ROL2DFG4hUBRjXTqDZqPTNoDbdvrXZqO5a+wwYCQK9CYgCmJxavpAo3p5S+gml1HrAHC4BWQAriaHoV1uN+JHhzoyngQD1/n8Bzq+b8RMmmaFUf5aUigErHwKyADpGa31cxfz+Zr26P58q8NSyEnjDHcCm+vIFHR3tjYguLyuYUfU9iABO13JIp/GWpTv40KhqwzzFJ3CWADbtWLksYf4XRbyp+O277zCDALrFfpdU/EvNOh93XzkqKAKB1wmg+86faPVNvPhHF21GARAZ/fXmgeqH6GFOR1clZioqgTMC6H7m/4lOvo1t/2ijziwAItLEu5Ya8dbRVorZikjgjABq0527iGh7EZv0uae1COCVfsxWHA/6nGwYta0KoHvUZ4gP4Nv+0Ye2dgHoFMeDo8+raDOeFkDygFHm5qI1F0I/axcAEY4HQ0jY7xq5+xN+Omkv4Id83ARlI4DTFeN40E10hZiVa9PJR4nMnkJ0E2ATQxBAt2scDwaYvQ8l8+RM8iVjzC0+FFPGGoYkABwPlnHxDKFnnpzufMcQbR7Cs/CINRAYmgBwPLgG+hjCtemVFpHaCBRuCAxTAK90gONBN0mGOWtXAG38Sq+78IYvABwPukszvJm5Nt0x4ZVdnIqHLwAcDxZndeTfCQSQP+O+M+QhgNMT4njQcbYhTA8BOE4pRwHgeNBxtiFMDwE4TilnAeB40HG+vk8PAThOKHcB4HjQccJ+Tw8BOM5nFALA8aDjkD2eHgJwHM7oBIDjQcdRezk9BOA4ltEJAMeDjqP2cnoIwHEsoxQAjgcdh+3h9BCA41AcCADHg44z92l6CMBxGo4EgONBx7n7Mj0E4DgJZwLA8aDj5P2YHgJwnINLAeB40HH4HkwPATgOwb0AcDzoeAk4nR4CcIqfyL0AcDzoeAk4nR4CcIrfDwHgeNDxInA4PQTgEH53ah92AK9BgD8u6ng9jHp6CGDUxH9qPs8EgONBx+th1NNDAKMm7rsAcDzoeEWMdnoIYLS8z5rNux3AmQrxx0UdL42RTA8BjARz70n8FQCOBx0vjZFMDwGMBHOIAsDxoOOlMZLpIYCRYA5TADgedLw4RjA9BDACyP2m8PcjwOuqxvGg43WS1/QQQF5kB3xuIALA8eCAeYZ2GwTgOLFgBIDjQccrJZ/pIYB8uA781JAE8EpTOB4cONwAboQAHIcUngBwPOh4yQx1eghgqDizPyw8AeB4MHvK/o6AABxnE6IAcDzoeNEMcXoIYIgw1/KogAXQbRfHg2sJ3aMxEIDjMAIXAI4HHa8f2+khAFuCluObKh6jOrd7PWZyamXFKFW1nCbX4Zp411Ij3prrJHh4LgQggFywDv7Qioprh+vc6jWiNt05QESXDP5EV3fieNAVeZt5IQAbekMYq43ZvDRXfbynAGaSPyVjfmcIU+X8CBwP5gw4l8dDALlgHfyhxpibW3PVr/T8CDCz8l5jaD+R4sGf6uZOrfVxFfP7m/XqfjcVYNasBCCArMSGfD+zeWhxtvqJfo+dnE7+3JD59JCnzutxh3Qab1m6gw/lNQGeOzwCEMDwWK7xSfrY2LHqph9/kU/1esD5dTN+Qif/RES/vMZJRj0Mx4OjJr7G+SCANYIb6jBjbmrOVf+q3zPfvs2sa5+T3mdU+qkQPg6Q0V9vHqh+iB7mdKis8LChEoAAhopzjQ8z9Gwzit9DdU6kJ9R2tH+eWX3akLmGtf5Zn48IcTwopen+OgTgPoPVCoyh32/NVb7gSTkooyQEIABPgtakT1UMXXVkbuy/PSkJZZSAAATgV8jPRZ34fUfu5KZfZaGaohKAAPxL9ruK42sXZnnRv9JQUdEIQAAeJqpTPhir9MMLc2M/8LA8lFQgAhCAp2FqrZcjpaYXVXxfv18W8rR8lBUIAQjA86A08fNMemd0svLQwj18wvNyUV5gBCCAQALTpE8oUv/IxjxGxN8zUfyjc4j+74U6rRCxCaQNlOkZAQjAs0CKVo70B0+K1m9o/UAAoSUWWL0QgN+BQQB+5xN8dRCA3xFCAH7nE3x1EIDfEUIAfucTfHUQgN8RQgB+5xN8dRCA3xFCAH7nE3x1EIDfEUIAfucTfHUQgN8RQgB+5xN8dRCA3xFCAH7nE3x1EIDfEUIAfucTfHUQgN8RQgB+5xN8dRCA3xFCAH7nE3x1EIDfEUIAfucTfHUQgN8RQgB+5xN8dRCA3xFCAH7nE3x1EIDfEZZCAJrSpmL1b0bTt5jVM6STg9V07KV1x+n4wV284ndEqA4E8iNQYAHol4mih5TRuxfmKvvwV3PyW0R4crgECicArfURZvV5juIHmnU+Hm40qBwE8idQHAFonZhI3Uun4kbrbj6WPzrMAALhEyiEAFb/cq7S17fq1SfDjwQdgMDoCAQvADb0D0kUf/KlOv9kdNgwEwgUg0DYAmD+SpOj3x7kv9UuRlzoAgSGSyBcAXRf/LPRLfh2f7gLAk8rF4EgBWCI/r6l4o9keeefuN2cS+PpryhNHzDKXGF0+k5WvIFIVcoVOboNi4DuGG2Osop+xJr3a6bHaCV6ZFhfdIcogOdSFV856Gf+2szyRUTxdp2mH1dKrQ8rfFQLAmcT0KRPkY72cJTc1Zodf9aGUVgC0DqhmK9q1qv7pabPr5vx4yaZZaO34V1eooXrQRLQOmGldsUqnjpc55Nr6SEsARDtbDYqn5Ma7b7raxPtVUSXSffiOgiEToAN/U8a6V9bqo8dyNpLOALQdFgtx++S/ofcWr19hU7oUaV4MisM3A8CoRJgrV9Kma9dmqs+nqWHYARgjNnWmqve26+52tTyxZrUf+DFn2UJ4N6iEFiVQEzvy7ITCEIA3cZ4ufqOfu/+b6ub9W2d7GOi9xQlUPQBAmsg8LQ6GV8l7ZRffW4QAtDEu5Ya8dZ+MCZnOn9iDP3eGoBhCAgUiwDT/c3Zym2DNBWGAIzZ3O+zzcTU8iVM/BQpFQ/SNO4BgWIT0KnWdNnS/NjTUp/eC0CTWVhqVN7a7yf+Ns4kf6mM+U2pWVwHgbIQMNrsbs1Xf13q13sBsDZfXZyvfrxXIz+z3WxQlfYRpdS41Cyug0BZCGitl3Wn+paXd/LRfj17LwAi85lmo3pfryYmdyQ3Gja7yxIs+gSBQQkY4htbjfhvgxYAE1272Kg82quJjVPJg0qZmwaFgvtAoDQENP9Fcz7+VNgC0OlFi/Pjz/dqojbV+S9S9AulCRWNgsDgBB5vNiqbgxZAtRNPHLqTl3p+BJhaWTJKnTc4E9wJAuUgYHTaas2P14IWQFPFY1Tnds8dwPRKG7/sU44FjS6zEtCdZmOsGrYAGhXu10BtumOyYnnt/b7/xxVSf3nX73p+m2y7Y4tev8RHWh/enwJIDUgB2wKSxud9XepP4mNbn+v5UX++b3AQgLDDsF2AtuNdvwBdzx86v7zrl54vvUFAABBA3zUEAUgvsXyvS/yl2SEAgZAESAKc93VpAeRdv+v5bfkWvX6Jj7Q+sAPADgA7AOlV5PC6JDCpNAgAOwBpjUAAVoTyHQwB4Biw7zGnZHjb5SktwLznR/04Bcj15wBsF5j0ApBeQLbzS+Pzri/v50v92V4PvX6pf6m/0n8HIAGUrkuAIQC7dzCJv+113/PLuz8IwJKw7wso7/ryfr5lPOLw0OuXGpT6gwAkgpZfImIHgB2A5RKzGg4BWOGTB0uAIQAIQF5F+d0hrU/sACzZS4AhAAjAcolZDZfWJwRghZdIAgwBQACWS8xquLQ+IQArvBCAtMBcC1CKN/T6bfuDACSC+BKwL4HQX0Ch1y8tX6k/CEAiCAFAAJZrxOVwCCBn+hJg11vgvOvL+/k5x+f9dzi2/Uv5YAdgSVgCDAHgS0DLJWY1XFqfEIAVXnwJKC0w1wKU4g29ftv+IACJIL4DwHcAlmvE5XBJcBCAZToSYNfvgHnXl/fzLeMRh4dev9Sg1B8EIBHEDgA7AMs14nI4BJAzfQlw2XcAOeMX/+6/NL+UnzTe9nre60PqDzsAywQlwHkHLJWfd33S86X6bK/b8g29fomf1B8EIBHERwCrjwCWeMXhEEB/RBCAuITsbpAA2y5Qu+ryP6aU+retXxpvyzf0+iU+Un/YAUgEsQPADsByjfQbbiswqTQIQCJkeV0CnHfAUvl51yc9X6rP9rot39Drl/hJ/WEHIBHEDgA7AMs1gh2ABUDJYLbvABalrQ4te31S/7Z8pfG2+Ydev8RH6g87AIkgdgBWOwDbF6hlPOJw6QXie/1Sg1J/EIBEEAKAACzXiMvhEEDO9CXArt9B8q4v7+fnHJ/3H+Fs+5fywQ7AkrAEGALA3wOwXGJWw6X1CQFY4cWXgNICcy1AKd7Q67ftDwKQCOI7AHwHYLlGXA6XBAcBWKYjAXb9Dph3fXk/3zIecXjo9UsNSv1BABJB7ACwA7BcIy6HQwA505cAYweALwFzXoJWgsYOwDIdCKDC/RC6FqAUr+/5SfVL16X+IACJID4CWL3DQACWC8xyOARgCVAaLgF2/QLIu768ny/xt70eev1S/1J/2AFIBLEDwA7Aco24HA4B5ExfAowdAL4EzHkJWgkaOwDLdCAAfAlouYRyHS6tTwjAEr8EGDsA7AAsl5jVcGl9QgBWeN3/LoAUsGV74t/dl+Z3LUCp/9Drt+0PApAIev4loLSALduDAKb93sFI+UrrAwKQCEIAph8CaYFhB2C5wCyHS/lAADkDzvsFIAVs2R52ANgB2C4hu/HSAs/7BSZV77o+aX6pfum6xFeaXxovzZ/39dDrl/hI/WEHIBHERwB8BLBcIy6HQwA505cA5/0OKM1v275UvzS/NN62Ptvxodcv9S/1hx2ARBA7AOwALNeIy+HhC0DFY1Tndi+ItemVNpGquISMuUHARwKsdXtxfmysX23e7wCqnXji0J281KuJyamVJaPUeT4GgJpAwCUBTWlzqTE+GbQAiNOLm7PjB3vvADpPENEvugSNuUHARwJMtG+xUdkStACY6NrFRuXRXk1snEoeVMrc5GMAqAkEXBJgzV9enI9vCVoAROYzzUb1vp4fAaaTTxoyf+0SNOYGAT8J8MeajfhrYQuAzZ7mbPWGXk28uW7eHOn2EUWq75cdfgaEqkAgHwKa9Amlqm9p1vl40ALQZBaWGpW3EnHP46iJmfZuNnxjPijxVBAIj8Ag2/9uV96fAnSL1MZsXpqrPt4rhvPqK5dGmr5PpKLwokLFIDBcAt3jP47Nzy3Ux38oPTkIARDT/c3Zym39mtk4ndyvyNwqNYzrIFB4Akx/3Jyt/NEgfYYhANJLFVV9x+E6n+zV1KbPmnP0uuQJYnr3II3jHhAoJAFNT244Gl99cBevDNJfIAIgImNua85V7++7C6ivvDtK6D/xg0GDRI97ikdAvxgpffWR+roXBu0tHAEQHVIn40sW7uETfSWwo32VYvMNIrVxUAi4DwSCJ6DpsIr1BxfqY09l6SUkAXT7urPZqPyh1ODE1PIliqO9hulS6V5cB4ECEHgiTuOPvHgH/2/WXgITgO4w05WLs2Pflxp9W92sT0wyZ1K9lZSKpftxHQRCI6BJr0Sk7l5U8Xy/X5jr11dgAlht5RlS8ZXSDzi82vTEzPK7mOLtOk0/ppRaH1rIqBcEziagjxmK/qaSRjvX8q7/2ueFKAAiQ3ubB+Lr6WFOB10eE7ebc2ksvU4p+oA25nKjzTtJmQ34CcJBCeI+FwS67/Kk+ahS/Lwh3q+I/jVW0Tf6nYhlqTNMAXQPBYj/rNWIfrffTwhmAYF7QaCMBIIVQDesVQk8Hd2aZSdQxpDRMwj0IhC0AE439XdmOf6N1t18DDGDAAhkI1AEAXS3As9SZD7arFf3Z2sfd4NAuQkUQwDdDLVOTKS+wBzPDXpCUO7o0T0IBPLbgNmC0i8a4s9HJysPSD81mO25uBsEikegODuAn8qGtX7JRNEeRXr3Alf2UZ118eJDRyBgR6CwAngtlu5fR1UU/bsh8y1m9Yyh5GDcHnt5fIyOvVCnFRwl2i0ijA6XQCkEEG484Vcu/ccU4XcYdgcQQNj5eV89BOB3RBCA3/kEXx0E4HeEEIDf+QRfHQTgd4QQgN/5BF8dBOB3hBCA3/kEXx0E4HeEEIDf+QRfHQTgd4QQgN/5BF8dBOB3hBCA3/kEXx0E4HeEEIDf+QRfHQTgd4QQgN/5BF8dBOB3hBCA3/kEXx0E4HeEEIDf+QRfHQTgd4QQgN/5BF8dBOB3hBCA3/kEXx0E4HeEEIDf+QRfHQTgd4Rcm15pE6mK32WiujAJ6E6zMVYNs/ZyVM0TU8tNVtFEOdpFl6MkYHTaas2P10Y5J+bKRoAnpzvfMUSbsw3D3SAgE2CifYuNyhb5TtzhigBPziRfMsbc4qoAzFtcAqz5y4vzMdaWxxFzbUdyPbH5msc1orRACRjDN7Tm4j2Bll+KsrlWN2/SSXsB/3V2KfIeWZOa9Clerm7Cf9k2MuRrmoi7oyankgeMMjev6QkYBAJvRID5weZs/FuA4zeB0wJYvtAQHyClYr/LRXVBENA6ITKXNufHnwui3hIXuSqA7r+Jmc5ONnR7iVmg9SERMEz3tGYrfzCkx+ExORI4I4C3bzPrls9Jvs2K3pvjfHh0wQlooh+sOxZv+fEX+VTBWy1Ee2cE8Mp3AcsXpqS+qRRPFqI7NDFSAt0f/GGiq7H1Hyl2q8leJ4Duk2r19hVa639WFOEnuKzQlmtw98VPsfpgq159slydh93tWQJYlcDM8kXaRHsV0WVht4fqR0JA01NE6Yfxzj8S2kOd5A0F0J1h9TuBc5NZ1nobTgeGyrw4D9M6MZG6t8rxzOE6nyxOY+XppKcAXkVQm1q+mKL4c9qkNyhS68qDBp32ItD9IR/F0VcpTe7Cu37Y60QUwKvtTdxuzqWx9Dpl6BpS5nKt0wtY8Qb8KnHYC0CuXneMNkeVin5Imr+nmR6jlegR/ISfTC6EOwYWQAjNoEYQAIFsBCCAbLxwNwgUigAEUKg40QwIZCMAAWTjhbtBoFAEIIBCxYlmQCAbAQggGy/cDQKFIgABFCpONAMC2QhAANl44W4QKBQBCKBQcaIZEMhGAALIxgt3g0ChCEAAhYoTzYBANgIQQDZeuBsECkUAAihUnGgGBLIRgACy8cLdIFAoAhBAoeJEMyCQjQAEkI0X7gaBQhH4f/+uLbVcQT8NAAAAAElFTkSuQmCC'),
(608, 'Tao Intent', 'application/vnd.tao.', '.tao', ''),
(609, 'Tar File (Tape Archive)', 'application/x-tar', '.tar', ''),
(610, 'Tcl Script', 'application/x-tcl', '.tcl', ''),
(611, 'TeX', 'application/x-tex', '.tex', ''),
(612, 'TeX Font Metric', 'application/x-tex-tf', '.tfm', ''),
(613, 'Text Encoding and Interchange', 'application/tei+xml', '.tei', ''),
(614, 'Text File', 'text/plain', '.txt', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAXmElEQVR4Xu2dbYwkxXnHn6rumd3j7RTY2Ysj5GBeDNjhJTG+i2w5UfgCMYpikQgHkxchG6y8+PDJxKBob3d2di3AxgZ8ihJjghwRHRchXZTIgEkUkg8Rto/E3BlbPrgD84Ejt9uz55h725npropm7wXubqereqqqu2v6v1+36qnn+T1P/efpru4ZRvgDARCoLAFW2cgROAiAAEEAUAQgUGECEIAKJx+hgwAEADUAAhUmAAGocPIROghAAFADIFBhAhCACicfoYMABAA1AAIVJgABqHDyEToIQABQAyBQYQIQgAonH6GDAAQANQACFSYAAahw8hE6CEAAUAMgUGECEIAKJx+hgwAEADUAAhUmAAGocPIROghAAFADIFBhAhCACicfoYMABAA1AAIVJgABqHDyEToIQABQAyBQYQIQgAonH6GDQG4C0GjKc0gkNzFG15OU1wqRXMw4W0vEa0jD8ASiVi01h43p7ueiVn3L8Ctg5igTcC4Ak1PLlxCF9ySU3MY5P2uUYRYRm1oAOrGk4HfbrfDpIvzDmuUm4EwALmrK8cMynqFE3E2ch+XG4K93agHoSSHEIR6yj0XN+k5/I4XnLgg4EYB1zeWLeyLYHhBd48Jp2HyHgI4AHB+9TyThhqUvsX3gBwInCFgXgHWbO1fFjP0bJ7YOmN0TyCAAfWdeIh7+RtRkh9x7hhV8IGBVAPqf/LHgL2Dz55f6jAJAJMW3o931T9BTLMnPS6xUVgLWBKB/zf+2iL+Htj/fVGcWACISxLYstcKN+XqK1cpIwJoANKZ79xPRPWUMcpR9GkYAjvGQG3E8OMqVoRebFQHoH/VJYrtxt18Pus1RwwuASHA8aDMTftqyJADxo5LLO/xE4LfXwwsAEY4H/c69De+NBaD/hJ+Iuwt4yMdGOrLbMBGA46vheDA79pGZYS4A0/EnieS2kSHiWSAWBKAfMY4HPcu7LXeNBWByJv6GlPJOWw7BTjYClgQAx4PZsI/MaHMBmO59XxKtHxkingViTQBwPOhZ5u24aywAjelOm4hfYMcdWMlKwKYAHFsbx4NZc+DzeBsC0MUrvcWVgH0BwPFgcdnMf2ULAtCT+buNFU8QsC8AOB6sUnVBADzPtgsBOI4Ex4Oe14aO+xAAHUolHuNQAPpR43iwxLm34RoEwAbFAm04FgAcDxaY2zyWhgDkQdnhGs4FAMeDDrNXvGkIQPE5MPIgDwHA8aBRiko9GQJQ6vSonctPAHA8qM6GfyMgAP7l7BSP8xMAHA96Xiqrug8B8DyreQoAjgc9L5ZV3IcAeJ7TAgQAx4Oe18y73YcAeJ7MggQAx4Oe180J9yEAnieyMAHA8aDnlXPMfQiA52ksUgBwPOh58UAA/E9g8QKA40GfqwgdgM/ZI6LiBQDHgz6XEATA5+yVRABwPOhvEUEA/M3diudl6ADehRBvD3pWTxAAzxJ2urslEwAcD3pWTxAAzxJWegHA8aBXFQUB8CpdZzpbug7gpIv4clEfSgsC4EOWUnwsrwDgeNCH0oIA+JAlLwUAx4M+lBYEwIcseSoAOB4sf3FBAMqfo1QPy3sJcIrbOB4saZ1BAEqaGF23PBEAHA/qJjTncRCAnIHbXs4bAcDxoO3UW7EHAbCCsTgjPgnAMUo4HiyuWs5cGQJQpmwM4Yt/AoDjwSHS7GwKBMAZ2nwM+ycAOB7MpzL0VoEA6HEq7SgfBQDHg+UpJwhAeXIxlCceC0A/XhwPDpV1e5MgAPZYFmLJcwHA8WAhVfPOohCAghNgunzEwzFqsu4gO5NTnY7kvG66jsv5gtiWpVa40eUasL06AQiA55VR42HjrSZrDwqjMd3bTUSXlz9MHA8WkSMIQBHULa4ppFy/NFd/caAAzMR/TVL+mcUlHZnC8aAjsKlmIQBFULe4ppTyjvZc/bGBlwAznaulpJ1E3DjXFt1e1ZQQ4hAP2ceiZn2n67Vg/xgB46JoTPckYBZHgDG5dXG2fluaB5PT8d9Kkp8tzstMK+8TSbhh6UtsX6ZZGDwUAQjAUNjKNEkcHDtYX/fmQ+zoIK8uasrxwyL+DhH9Zpk8T/EFx4M5JQoCkBNop8tIeXs0V/9W2hoXbpJrumcnj0iefMaHywGS4tvR7von6CmWOGVXceMQgFEoAEmvRkH4QWqyWBVOY3P3Vxnjn5Ukr2dC/HKZjwhxPKjKpvn/IQDmDEthQUr6Qnuu9rVSOAMnvCEAAfAmVemOChJHa5I+vH9u7McjEhLCyIEABCAHyDkusSfohR/dfx+LclwTS3lMAALgcfIGuP4SZ+GNC7NscfRCQ0S2CUAAbBMtgT2RsL0hT25emBt7uQTuwIUSE4AAlDg5Jq4JIZYDzqcXefhI2stCJmtgrv8EIAD+5zA1AkHsNUbigeBIbevCg+zwiIeL8DISgABkBObrcEHiMCf+DJPyeSK2SwbhT88m+r83mtQhYnic29fEGvoNATAEiOluCai+8MTt6qNvHQIw+jn2OkIIgNv0QQDc8oV1QwIQAEOAiukQALd8Yd2QAATAECAEwC1AWHdLAALgli86ALd8Yd2QAATAECA6ALcAYd0tAQiAW77oANzyhXVDAhAAQ4DoANwChHW3BCAAbvmiA3DLF9YNCUAADAGiA3ALENbdEoAAuOWLDsAtX1g3JAABMASIDsAtQFh3SwAC4JYvOgC3fGHdkAAEwBAgOgC3AGHdLQEIgFu+6ADc8oV1QwIQAEOA6ADcAtSxLiiJOOP/IQV9lzH+Col4bz0ZO7DmEB3au4V1dGxgDAi4IIAOwAXVFZviZ0TBVi7FEwtztR341h1noGHYgAAEwADealOFEPsZ419hQfho1GSHLJuHORCwSgACYAunELEM+MN0NGy1v8wO2jILOyDgkgAEwALdlW/e5eKWdrP+AwvmYAIEciMAATBEzST9SxyEf3Sgyd42NIXpIJA7AQiACXLGHotY8Kc6P8ttsgzmgoArAhCAYcn2N/9scCfu7g8LEPPKQAACMEQWJNE/t3n4+1k++X/hHrm2Vk9+R3L6LZLyGibE+ySnc4l4bQgXMKUyBESPiN4m4j8lxnZKQf9OneBpWzeaIQDZC2lPwsPrdK/5J6aWL5dBeC8lyR9wzsezL4cZIHAqASHEEU7BkxTE90ez43tN+EAAstATIqaQfThq1neqpv1SU54Vi3hekthIxAPVePwfBLITED1J/OHxg+HMmw+xo9nnE0EAslF7IGrV7lVN6X/qcxZsl4w+oBqL/4OAKQFB9DJnyc3DdAMQAF36gt7iy+H7Vb+wO7G5+yGS4juMBxO6pjEOBEwJrLxvIvkN0Vz9pSy2IACatKSUm9pz9YfThvc/+Ynov7D5NaFimFUCKyIg6KPR/PgeXcMQAA1STIgDbLn+3rRP/3V3y7PFWfGLRHSlhkkMAQEnBPqXA2sOhht07wlAADTSIIhtWWqFG9OGNqZ7/e7gLg1zGAICTgkwoq8utmp36ywCAdCgJKRcvzRX73+6r/p3QbNzBRf0I9zt14CJIe4JrLyYJj/Ynh1/VbUYBEBBSJBcWGrV3pP2xF9jpvv3JNkfq2Dj/yCQFwHB2LeWZsPbVetBABSEmJBPLs7XPzVoWP8JP17r7sdDPqpSw//zJCBIHJW8/ouqB9YgAMqsyM9Hrfojg4ZNbo7/UDL5hNIMBoBAzgSkZLe158KtactCAFQdANGNi63acwOv/6fixzmXylYr59xjORAgEuzvovnwMxAAg2JgIrl0cX78tUEmGlO9/yFOv2awBKaCgCsCL0at2noIgAHeei+c2HcfWxp4CTDVWZKcn2+wBKaCgBMCUiTt9vx4AwJggDfi4Rg1WXdgBzDd6eKVXgPAmOqQgOhFrbE6BMAAseqHKRrTPWlgnlT2TWzbmKuOj308aoXPDlprYnPnWcb4jTZ8OdOGeDZqjX18sDjHv00kn0lb23/+6WRV8eEmoKIyVQDVG8QsQW42jr5Vjfh+EvHw6kFfjnLBVOdKTvRD4jzUX1VjpBCxCOmqpebY7lVHN2XYSOKXidEVEIDBBCAAEIBUAhoCQERyY9Sqbxn4STwTbyEp/0JjW2sPUT2e3djc3UiMDTy+PbGQSuC1HXI0UI//4MVV8UEAIADGAtB/WWo5rF/2dpMdWM3YeU15/njc3WPrZqnN9VQbxNG+1jYLAdBG5WagqkBcJ8hNVPpWdeOz9Yms55m9jkOVXz1/3I3S5T/IA1V86ADQARh3ACsGLF2Ta2wlq/ccVBtEwx+nQyAATvGqjasKxHWC1B66HZEtPvO78upo7J46qPKr9sftiGz8z/RFFR86AHQAdjqAk1bsbtBTnbMvMKoN4nZ7q61DANSMnI5QFYhpgkydL6F/Vlv0k3zyu8TIlJIS8j/Ff5V/6AAK7gAyVdsqg1UJLkag7N2kOxFyvjcZ9bNSTv7v+K/yDwIAAdCvds2RNo/p+kvatqcZhtYw1QYrRoAhAFrJ0xnke4KLKkC7n9j2Owqd3OuM8b0+0AGgA9Cp8+xj7F2zu7mnkD2iVWdAAAxfhrGUB2dmfE9wUR3AsYTYuGvv8lTBvGx8rw90AOgAzHdBqgWTDWxDQNyGBwFAB2D0OrBpeZa9AIlouBbe3iWEKeLU+WXnr/IPHQA6AKcb5Jjx7Dfx7N5EdBeiaoMVewlGyu+bgABAANztjuOWsx7jZR3vPICUBSAAuATAJYDGDsz2iZ69Y9BwwckQCAAEAAKgs7X0r+llId8wpBPDKmMgABAACID25tG5q9+/bi3qOwa1Azk5EAIAAYAAZNo36ceCaaYa0+ov+czkioXBEAAIAAQg20ZKPRYcaErzSz6zuWI+GgIAAYAAZN5H6Tf5VjOn+yWfmV0xnAABgABAADJuItUx3+nmbH+paEZ3U4dDACAAEIAhdpTqWPDdJhsOvlZ8CJdXnQIBgABAAIbZTapjweM2nf2wyDA+4xjwTAJFP+poKY8Dzfiu8OXOT/qxYD8pbn9azLx6fK8PPAqsqAHfEwwBMN/kaRZ8rw8IAATA7Q4ZZB2XALlwVwkUBAACkEshnr4IbgLmgx0CYMhZBbDoFrvs/q2GH8eAhkWZYbqqPtABoAPIUE62huJBIFskVXYgACpCEABDQpmn41HgzMiGnwABGJ7dykwVQFwCZAWMl4GyEjMZr6pfXAKgAzCpr4xz8TpwRmDGwyEAhghVANEBaAJWHfu987YfvhBEE6nOMFX9ogNAB6BTR8ZjVMd+p77th68EMwZ+3AAEwJCkCiA6ADVg1bHf6W/7ZR2v9sDdCN/rAx0AOgB3u+Ok5eyf6Nk6hhxCGLAEBABvA+JtwPT9hx8GKU6flKdY6ADQATguT/w0mGPAqeZVHQoEAALgsD51jv3kM+kOmAiIw9A0b7KV/R4RBAAC4GaX6B/7XaFwYLhLCDdRnWFV9QkLAcgpEa6W8T3BRRWg3Zt42W8iuqqH0+36Xh/oANABWN8rto/xbNuzGTAEAKcAOAU4Y0fZ/8S221HYkwAIAAQAAnDqfnJzzW7vnoK93T8CL4vhEgCXAFY3BJHLu/Y2ThXshosOAB0AOoCTeyqPDepSYLKLAwQAAgAB6O+b/Fp0N5cY2ff+ygwIAAQAAtDf/8S2LLXCjYP2kd3f9rN/k3HI/Q8BKOqcediEZZ3nu8LnkZ+8j+nyXi+tZnyvD9wExE3ArJq4yvj8P5Hz7TgGI4IA4BKg6pcAxVyT53fPIVUgIQAQgIoLQJF35fM4dUhvkCAAoy4APByjJusOvLk13ekS8ZqFPhomQMAqASZEd3F+bCzNKO4BKJDXe+HEvvvY0qBhk1OdJcn5+VYzB2MgYIGAoCRaao1PQgBMYLLksmh2fO/gDqD330T0IZMlMBcEXBBgRDsWW7UNEAADuozoxsVW7bmBAjAVP0ZcftpgCUwFAScEmGDfXJwP74QAGOGVn49a9UcGmZiYjm9jJP/BaAlMBgEHBKRkt7bnwm0QABO4TG6LZuu3DjJxflOex0R3Pye+xmQZzAUBmwSEEEd4WF8XNdkhCIABWUFyYalVew8RG3jcd8FU/Djn8naDZTAVBOwSYOyxaDa8Q2UUpwAqQv3n3KVcvzRXf3HgZcDM8vtZwn5MnIca5jAEBBwTED0m5JWL8+OvqRaCAKgI9f/P6OvRbO2utKGT070HJdEXdMxhDAg4JvBA1Krdq7MGBECHEomlGq+/960mOzJo+IWb5JrO2fEO4vQrWiYxCAQcEEiIdp3Hw19/o8mWdcxDAHQo9cdIeVc0V/962vDGzPKlQtILnIKGrlmMAwFbBPr3q0IuPrLQHH9d1yYEQJcU0T5+JLx84UF2OFUEmt1rRUzPcc5Sn8DSXxYjQUBNoL/5A0k3LM7Vd6lHvzMCApCFFtF9Uav2V6opk1PLlwgKtjNOV6vG4v8gYEogEbSzFia/l+WT/8SaEIBM9EWPMbpucXbsh6ppFzXl+CEZzzIpNuFlIRUt/H84AqJHjH917YGwuXcL6wxjAwKQndorxMPrVA9YnDDb7wZkEN4rkuRTnPOzsi+HGSBwKgFB4nAggq0sjO8f5lP/3dYgAMNUl6Tt0e7wFnqKJbrTG015DonkJsboepLy2oSS9wWCrZWc13VtYFz1CPRf6U24/Dmj4HXO2C4p6XniwdO6H0AqYhAAFaEB/5fE/qbdCv487QnBIU1jGgjkRgACYIB6RQR+EnwuSydgsBymgoB1AhAAc6T/JJfDP2l/mR00NwULIJAvAQiADd6SXqVAfjJq1nfaMAcbIJAXAQiALdJCxDLgX2MsnLN1g8aWa7ADAoMIQACs14b4X0nsK8GR2qOqpwatLw2DIJCRAAQgIzDd4f1fr5FBsI2TeGKB1XZQkwnduRgHAnkRgADkQLr/7aycgv+UJL/LGH9FUrw37I79bHyMDr7RpA6OEnNIApZYlQAEAIVRagKqH94otfMeOAcB8CBJVXYRAuA2+xAAt3xh3ZAABMAQoGI6BMAtX1g3JAABMAQIAXALENbdEoAAuOWLDsAtX1g3JAABMASIDsAtQFh3SwAC4JYvOgC3fGHdkAAEwBAgOgC3AGHdLQEIgFu+6ADc8oV1QwIQAEOA6ADcAoR1twQgAG75ogNwyxfWDQlAAAwBogNwCxDW3RKAALjliw7ALV9YNyQAATAEiA7ALUBYd0sAAuCWLzoAt3xh3ZAABMAQoPsOoNPFT1+5TVJ1rYte1BrDD6c4LADjDmBiajliPJhw6CNMV5SAFEm7PT+On1p3mH9jAZic7n1fEq136CNMV5QAI9qx2KptqGj4uYRtLgAz8TeklHfm4i0WqRQBJtg3F+dD1JbDrBsLQGNzfAsx+Y8OfYTpihKQkt3angu3VTT8XMI2F4CmPEfE3QX89HUu+arMIoLEUbZcX4efXHObcmMB6Ls3ORU/Krm8w62rsF4pAow9Hs2Gn65UzAUEa0kAli+RxHYT52EBMWDJUSMgREwkPxDNj+8ZtdDKFo8VAegHNTHTe4BJ+mLZAoQ//hGQjB5sz9b+0j/P/fPYmgBcuEmuWT47/h7jdLV/GOBxWQgIopfXHAw3vPkQO1oWn0bZD2sCcOxewPIlCfEXOGeTowwNsbkh0H/whxF9BK2/G76rWbUqAP0FGs3utUKIf+UU4Amu/PLo/Ur9zU8hv6HdrP/A+2A8CsC6AKyIwMzypUIG2znRVR6xgKtFERD0I6LkZnzy558AJwLQD2PlnsC58SwTYhNOB/JPrBcrChHLgD9cZ+HMW012xAufR8xJZwJwglNjavkyCsJ7hUxu5cTXjBg/hDMEgf5DPpwFT1IS349P/SEAWpziXABO+DrxRXkujSU3cUnXE5fXCJFczDhbi1eJLWazlKZETwr5c86D10mwXYLR89QJnsYTfuVIVm4CUI5w4QUIgMC7CUAAUA8gUGECEIAKJx+hgwAEADUAAhUmAAGocPIROghAAFADIFBhAhCACicfoYMABAA1AAIVJgABqHDyEToIQABQAyBQYQIQgAonH6GDAAQANQACFSYAAahw8hE6CEAAUAMgUGECEIAKJx+hgwAEADUAAhUm8P+tU2rE2Rp53QAAAABJRU5ErkJggg=='),
(615, 'TIBCO Spotfire', 'application/vnd.spot', '.dxp', ''),
(616, 'TIBCO Spotfire', 'application/vnd.spot', '.sfs', ''),
(617, 'Time Stamped Data Envelope', 'application/timestam', '.tsd', ''),
(618, 'TRI Systems Config', 'application/vnd.trid', '.tpt', ''),
(619, 'Triscape Map Explorer', 'application/vnd.tris', '.mxs', ''),
(620, 'troff', 'text/troff', '.t', ''),
(621, 'True BASIC', 'application/vnd.true', '.tra', ''),
(622, 'TrueType Font', 'application/x-font-t', '.ttf', ''),
(623, 'Turtle (Terse RDF Triple Language)', 'text/turtle', '.ttl', ''),
(624, 'UMAJIN', 'application/vnd.umaj', '.umj', ''),
(625, 'Unique Object Markup Language', 'application/vnd.uoml', '.uoml', ''),
(626, 'Unity 3d', 'application/vnd.unit', '.unit', ''),
(627, 'Universal Forms Description Language', 'application/vnd.ufdl', '.ufd', ''),
(628, 'URI Resolution Services', 'text/uri-list', '.uri', ''),
(629, 'User Interface Quartz - Theme (Symbian)', 'application/vnd.uiq.', '.utz', ''),
(630, 'Ustar (Uniform Standard Tape Archive)', 'application/x-ustar', '.usta', ''),
(631, 'UUEncode', 'text/x-uuencode', '.uu', ''),
(632, 'vCalendar', 'text/x-vcalendar', '.vcs', ''),
(633, 'vCard', 'text/x-vcard', '.vcf', ''),
(634, 'Video CD', 'application/x-cdlink', '.vcd', ''),
(635, 'Viewport+', 'application/vnd.vsf', '.vsf', ''),
(636, 'Virtual Reality Modeling Language', 'model/vrml', '.wrl', ''),
(637, 'VirtualCatalog', 'application/vnd.vcx', '.vcx', ''),
(638, 'Virtue MTS', 'model/vnd.mts', '.mts', ''),
(639, 'Virtue VTU', 'model/vnd.vtu', '.vtu', ''),
(640, 'Visionary', 'application/vnd.visi', '.vis', ''),
(641, 'Vivo', 'video/vnd.vivo', '.viv', ''),
(642, 'Voice Browser Call Control', 'application/ccxml+xm', '.ccxm', ''),
(643, 'VoiceXML', 'application/voicexml', '.vxml', ''),
(644, 'WAIS Source', 'application/x-wais-s', '.src', ''),
(645, 'WAP Binary XML (WBXML)', 'application/vnd.wap.', '.wbxm', ''),
(646, 'WAP Bitamp (WBMP)', 'image/vnd.wap.wbmp', '.wbmp', '');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(647, 'Waveform Audio File Format (WAV)', 'audio/x-wav', '.wav', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAbfElEQVR4Xu2df5AlVXXHz73d780sC1Ky82bRoozyw98CifwwGqPyh6BUKhZJYRCTaAlrmRSrG4lQcXbmzZuxAEX5sUlFEY2WCWBRRSopEdQKmioLFfyxRI0LrGiqWMLOm1mD+2vee933pHp2R/fHdN/bffv26379nX/fveec+znnfvt0337zBOEPBECgtgREbVeOhYMACBAEAEUAAjUmAAGocfKxdBCAAKAGQKDGBCAANU4+lg4CEADUAAjUmAAEoMbJx9JBAAKAGgCBGhOAANQ4+Vg6CEAAUAMgUGMCEIAaJx9LBwEIAGoABGpMAAJQ4+Rj6SAAAUANgECNCUAAapx8LB0EIACoARCoMQEIQI2Tj6WDAAQANQACNSYAAahx8rF0EIAAoAZAoMYEIAA1Tj6WDgIQANQACNSYAASgxsnH0kGgMAFotflEUuGlQtBFxHyuUuHpQoqTiWQDachOoNtpJOawNd2/pttpbsvuATNHmYBzAZicWj6DyL8upPBKKeUJowxzGGvTC0AvYPL+eLHj3z+M+OCz3AScCcCL2zy+n4MZCtW1JKVfbgzVjU4vAANWSu2Tvnhjt93cXt2VInIXBJwIwMb28ukD5d3nEZ3jImjY/C0BEwE4PHqXCv0Llz4mdoEfCKwSyF0ANm7tvSYQ4huSxEZgdk8ghQBEwfyIpP+H3bbY5z4yeKgCgVwFILryB0o+jM1fXOpTCgARq690dzTfQfeKsLgo4amsBHITgOie/9cq+C7a/mJTnVoAiEiR2LbU8TcXGym8lZFAbgLQmh7cSETXlXGRoxxTFgE4xIM343hwlCvDbG25CEB01MckduBpvxn0PEdlFwAV4ngwz0xU01ZOAhDcwZKvriaCakedXQCIcDxY7dznEb21AERv+Kmgvxsv+eSRjvQ2bATgsDccD6bHPjIz7AVgOngnEd8zMkQqtpAcBCBaMY4HK5b3vMK1FoDJmeAzzLwpr4BgJx2BnAQAx4PpsI/MaHsBmB58j4kuGBkiFVtIbgKA48GKZT6fcK0FoDXdWySSG/IJB1bSEshTAA75xvFg2hxUeXweAtDHV3qHVwL5CwCOB4eXzeI95yAAAy4+bHhcJZC/AOB4sE7VBQGoeLZdCMBhJDgerHhtmIQPATChVOIxDgUgWjWOB0uc+zxCgwDkQXGINhwLAI4Hh5jbIlxDAIqg7NCHcwHA8aDD7A3fNARg+DmwiqAIAcDxoFWKSj0ZAlDq9OiDK04AcDyoz0b1RkAAqpezoyIuTgBwPFjxUlkzfAhAxbNapADgeLDixbJG+BCAiud0CAKA48GK18yR4UMAKp7MIQkAjgcrXjer4UMAKp7IoQkAjgcrXjmHwocAVDyNwxQAHA9WvHggANVP4PAFAMeDVa4idABVzh4RDV8AcDxY5RKCAFQ5eyURABwPVreIIADVzd1K5GXoAI5AiG8PVqyeIAAVS9ix4ZZMAHA8WLF6ggBULGGlFwAcD1aqoiAAlUrX8cGWrgP4TYj456JVKC0IQBWylBBjeQUAx4NVKC0IQBWyVEkBwPFgFUoLAlCFLFVUAHA8WP7iggCUP0eJEZb3FuCosHE8WNI6gwCUNDGmYVVEAHA8aJrQgsdBAAoGnre7yggAjgfzTn0u9iAAuWAcnpEqCcAhSjgeHF61HO8ZAlCmbGSIpXoCgOPBDGl2NgUC4AxtMYarJwA4HiymMsy8QADMOJV2VBUFAMeD5SknCEB5cpEpkgoLQLReHA9mynp+kyAA+bEciqWKCwCOB4dSNb91CgEYcgJs3XelP0Zt0Y+zMznV67GUTVs/LucrEtuWOv5mlz5ge20CEICKV0ZD+q1n2mIxbhmt6cEOInpZ+ZeJ48Fh5AgCMAzqOfpUzBcszTUfjRWAmeAfiPmvcnTpyBSOBx2BTTQLARgG9Rx9MvPVi3PNO2NvAWZ6ZzPTdiJpnescw17TlFJqn/TFG7vt5nbXvmD/EAHromhNDxgwh0dACL5rYbZ5ZVIEk9PBp5n4/cOLMpXnXSr0L1z6mNiVahYGZyIAAciErUyT1N6xvc2NT98iDsZF9eI2j+9XwYNE9KYyRZ4QC44HC0oUBKAg0E7dML+3O9f8QpKP07bwuv768DaW4VVVuB0gVl/p7mi+g+4VoVN2NTcOARiFAmB6ouv5r6K2CHTLaW3t/64Q8v1MfJFQ6nfKfESI40FdNu0/hwDYMyyFBWb68OJc41OlCAZBVIYABKAyqUoOVJE62GA6/9m5sZ+OyJKwjAIIQAAKgFygiye9gf+GZ28Q3QJ9wlWFCUAAKpy8mNB/JIV/ye5ZsTB6S8OK8iYAAcibaAnsqVDs9GV42e65sR+XIByEUGICEIASJ8cmNKXUsifl9IL0b0v6spCND8ytPgEIQPVzmLgCReLngtRN3oHGXbtvFvtHfLlYXkoCEICUwKo6XJHaL0l+VTA/RCQeY8//xXqi//tlm3pEAq9zVzWxlnFDACwBYrpbArp/eOLW++hbhwCMfo4rvUIIgNv0QQDc8oV1SwIQAEuAmukQALd8Yd2SAATAEiAEwC1AWHdLAALgli86ALd8Yd2SAATAEiA6ALcAYd0tAQiAW77oANzyhXVLAhAAS4DoANwChHW3BCAAbvmiA3DLF9YtCUAALAGiA3ALENbdEoAAuOWLDsAtX1i3JAABsASIDsAtQFh3SwAC4JYvOgC3fGHdkgAEwBIgOgC3AGHdLQEIgFu+6ADc8oV1SwIQAEuA6ADcAjSxrijsSiG/yYq+I4R8nFSwsxmO7Vm3j/bt3CZ6JjYwBgRcEEAH4ILqik31KyLvLsnqS7vnGo/gv+44Aw3DFgQgABbw1pqqlHpWCPkJ4fl3dNtiX87mYQ4EciUAAcgLp1IBe/JWOuh3Fj8u9uZlFnZAwCUBCEAOdFf+865Uly+2mz/MwRxMgEBhBCAAlqgF078Hnv/ne9ri15amMB0ECicAAbBBLsSdXeF9wORnuW3cYC4IuCIAAchKNtr8s94mPN3PChDzykAAApAhC0z0b4vS/9M0V/7nX8cnN5rhH7GktxDzOUKpl7Ckk4hkI0MImFIbAmpARL8mkr8gIbazov+gnnd/Xg+aIQDpC+nJUPrnmd7zT0wtv4w9/3oKwz+TUo6nd4cZIHA0AaXUAUne3eQFN3Znx3fa8IEApKGnVEC+OL/bbm7XTXthm08IVDDPpDYTSU83Hp+DQHoCasAkbx3f6888fYs4mH4+EQQgHbWbup3G9bop0VVfCu8+FvRK3Vh8DgK2BBTRj6UIL8vSDUAATOkrekYu+y/V/cLuxNb+a4nVg0J6E6amMQ4EbAmsfN+E5cXdueaP0tiCABjSYuYti3PNW5OGR1d+Ivo2Nr8hVAzLlcCKCCh6Q3d+/ElTwxAAA1JCqT1iufmipKv/xmt5vToheJSIXmFgEkNAwAmB6HZg3V7/QtNnAhAAgzQoEtuWOv7mpKGt6UHUHXzQwByGgIBTAoLokwudxrUmTiAABpQU8wVLc83o6r7m34Z27+VS0U/wtN8AJoa4J7DyxTR+1eLs+BM6ZxAADSFFvHup03hB0ht/rZn+F4nFX+hg43MQKIqAEuILS7P+e3X+IAAaQkLx3QvzzXfFDYve8JON/rN4yUdXavi8SAKK1EGWzVN1L6xBALRZ4Q91O83b4oZNbg3ezYK/pDWDASBQMAFmceXinH9XklsIgK4DILpkodP4Wuz9/1TweSlZ22oVnHu4AwEiJT7XnfevggBYFINQ4ZkL8+M/jzPRmhr8gCT9noULTAUBVwQe7XYaF0AALPA2B/7ErhvEUuwtwFRviaU8xcIFpoKAEwKswsXF+fEWBMACb1f6Y9QW/dgOYLrXx1d6LQBjqkMCatDtjDUhABaIdT9M0ZoesIV5cm1fF1tx/tVeocRXlKRvsqDHxkTjqWei77kniOtK7G1uvpDoeT0enC4UnSuYLlLEl0opT9StzehzEZ6V9CWaDR8NnpQen2lkK+UgFYqdSx/zz4q/vVw+i6SnPctPcqvLLx4CapKmAwgB0ABkeoKIbxrb17jb9PVU3T6KXrsOTxi8i0leJ4nP0I1P/pw3dzvNbXFjNkwHt0via+x8xMwWdHt3thH79mhra38zCRF7AmUSk65+IQAQgMQayCpw0T+tEFJMLcrGtjT/OcmkqH8zZhM3WqcGf6OUamd/D0M90O2MvT3+Fi94GxF/NVVcpoMlva3bbjwYN3xia+8BIeQlpubWGgcBsKFH5LxF1yUo6wY0XbYj/4+HUl22pz3236Zx2IybnOmdHbJ3X5ZuIHph5iTZPOWXbbG8VgynbeF1B9f392QXmLVXVpRfXX7RAaADyLsD+EFz4F+cdHJis9nj5m6c4ckBB1/3iM5Jbb+AK/HxMRXTeUAAUlfD0RN0AG2v0K7t65afq3+mHc3A/4OiN//qGiMRCDh8OHUnUMC9+PF5KObZgy6/6ADQAeTSAShS+0nR+UvzYz/TiY7Lz1duB0L6XpqWvYin8cetuaDTBwiAZbXpAKIDOATY5D8mWabCePrk1sFHWNBNxhOigQVtyMhVkYKjq190AOgA7DsAph3dHf6r6V4R6jbdxEz/PBnKTSHTWzwRvoilTH5RRak+S/k/gsRDSqo7jH5/cRM3Npwa/izdrUAxLfkKnwJvOSAAuoqEANgLAIn3dDv+F5NQRmf3wfrw7yXze6xSosTnxvZ71+jeKZjY2r9KCPFZc1/FPJRbiafAh44QAPMKWHOkDiBuAdRz62Xz1LhjtAjq4f+X+A0i+n3LdBy63SD69vhe/61JIrAiOOMr/6fB6I3Boo7livKzyllXv7gFQAdg1QEIxf+yMN98dxLGDTPBP1lf+Y91sPLbjP7VSX5bM/0vE4vLjUWnkCtzgZ2GwXssEAAIgJUAEPFV3U7zc3EYo99JEEJ833gTGg9UTCxem/R/8Cem++8XJD5tbLKQe/MCnzVAAIxTHztQ10LV/RaABZ+/ONuM3eCTM8FnmHmTfSaOt8BCfHpx1v9AnO2NM/3XKRbfMfVdyNP5Ak8bonXr6hcdADoAqw5A9/8SWtOD6Ntssd94M92cMeMe73YaL48XAJ5UHOxO5cPhBi1EYI5ZLAQgVfaPH6wDWPcOQPf/Eianej3dUV/WFClSvaXOWOwvLp95DY899/xgzXf84306bNELucU4emW6+kUHgA7AqgPQFZitQOrEIX//Dh/SFfKQEQKgq5lUn+dfYOkSVPYN5JqPLll5+3d1TOfKri0fdADoANABHFsDTq7UDjuLhBrWCSQEAAIAATi2Bpzcqzt8tgAB0DVC2T/XKahti+7avm7ltv5t5+vi033uwr+Tp/UOTxeSGOn4oANAB4AOYK0ayHHDOhEUnTIe/hwCYAgqbpgOIDqAhpWAWKZH+6JL9vzk2LI7uaUwI6erX3QA6ACsNrCuwLJvwHwKPLv/HB/aOXmomA8fCEC1BeA/u53Gm5OW0JoefIuI3uSqw9EJgFmZuhuVVQDyOrbLy05WQrr8QAAqLAAs6S2L7Ua0wWP/JtqDNwtF34QAZNhCuVy5c+wkMiwBApAB2pFTdACzXmFWfVjY1179V30kdQEW/lfM6+Zb4reebpWfXO7dc3yWkIGGLj/oACraAZhc/VeXltQF6ApEt4F08zPUbK5TdPEnOcvl6X2OpwlZwOjyAwGopgAYX/11XYCuQHQbSDc/S9HmOUcXv9aXxQbORUC0ASYP0OUHAlBBAUhz9dd1AboC0W0g3XzL+rWerotf78Cihc/lFkIfYdIIXX4gANUTgNRX/6QuQFcgug2km29XvvazdfHrPVg8xMvlIaI+QgiAHaPE2boCty2wtPazXP2TuoC0/o+FpZvvMDVGpm3zk/UYL+s8o0WlGKTLDzqAanUAiVf/6GFftJyko8FjTwR0BaLbQLbzU9TymkML8Z/pSm7ROdhCOWK+jg8EoEICoLv6H97c0dFc7MtBx54I6AoEApD1hzwsnh1AAHIkYGnKdoPo3Kewr736r77wYygUK28HpvA/vCtwAkTb+HX5iT7P9DTf4vTAJCbTMTo+6AAq0gGk2dREZCwWugJBB3C4QFJs6EyCYbqjU47T5RcCUA0BMN7Qq8sxFQxdgUAAfkN0c7fT3BZXLhumg9sl8TUrn5fg+G81Tl1+IQAVEADTzXzMUoxEQ1cgEIBVqike6mV6aJjy0m44XJdfCED5BeBbaR7qHbkcE+Ew+DYhJyHSFZhOQAzrOHZYUf5Nj/VIEp8km6fE/VbiaVt43cH1/T1Syth/Z27L5Mj5Oj4QgJILQPTUPs2xXtouQPdtQt0G1hWYbr5tsRfq3+jKTtztjL09bl2t6eBtRPxV23WbztfxgQCUXACSwtN91Teaq+sCdIWk28C6AtPN1/nXfV6of6N7+0gADJ8V6BaXw+c6PhCACguA7p99HF5a5leHo/m6DawrMN182xov0r/R032PuDs7vjNuXRs+GjwpPT7Tdt2m83V8IAAVFQCTq//q0my6AN0G1hWYbr5pIceNK9y/5jgwaT2tqeWzSHrRbyUW9qfjAwGoqAAYXv1XV5e5C9BtYF2B6ebb7oTi/Se/4ZcoAFv7m0mI22zXnGa+jg8EoIICkObqb9sF6DawrsB089MU81pji/effByYtJ6Jrb0HhJCX2K45zXwdHwhABQUg5dXfqgvQbWBdgenmpynmMgiA7jgwbj1FH/+txqHLDwSgYgKQ5epv0wXoNrCuwHTzqyYAK/FqjgPXWlPRx38QANvKOjzfdYHr7B+7jIxX/8xdgG4D6+LXzbdN01D8a44D11rTUa8K2y46xXwdH3QAFeoAbK7+WbsA3QbWFZhufopaXnPoMPzrjgPXFICCj//QAdhWVgk7AMurf6YuQLeBh7EBj0zt0PynOA4cxvEfBGDEBCCPq3+WLgACEFdI5seBrSEc/0EARkwAcrr6p+4CIABxhWR+HDiM4z8IwAgJQJ5X/7RdAARg7UIyPQ4c1vEfBGCEBCDnq3+qLgACkFBIBseBwzr+gwCMiAC4uPqn6QIgAAmFZHAcOKzjPwjAiAiAo6u/cRcAAYgvJJPjwKK//XdstLpTErwHoBGKrvTHqC36ccNa070+kWzkpDcwAwK5ERBK9Rfmx8aSDEIANLibA39i1w1iKW7Y5FRviaU8JbeswRAI5ERAUdhd6oxPQgBsgGpe+GhND75PRK+1cYG5IOCCgCB6ZKHTuBACYEFXEF2y0Gl8LfYWYCq4kyS/z8IFpoKAEwJCic8uzPubIABWePlD3U4z9p84TEwHVwrif7Zygckg4IAAs7hicc6/BwJgA1fwPd3Z5hVxJk5p8/OE6j8rSa6zcYO5IJAnAaXUAek3N3bbYh8EwIKsIt691Gm8gEjE/n/8DVPB56Xk91q4wVQQyJeAEHd2Z/2rdUZxCqAjFP04JPMFS3PNR+OGTswsv1SE4qckpW9gDkNAwDEBNRCKX7EwP/5znSMIgI5Q9LnBG1+T04ObmejDJuYwBgQcE7ip22lcb+IDAmBCidRSQzZf9ExbHIgbHn3po7c+eIQkvdrIJAaBgAMCIdFjz5P+6+J+muxYlxAA0yQwf7A717w9aXhrZvlMxfSwJK9lahbjQCAvAtHzKl+q1+9ujz9lahMCYEqKaJc84L9s981if6IItPvnqoC+JqVIfAPL3C1GgoCeQLT5PaaLF+aaj+lH/3YEBCANLaIbup3G3+mmTE4tn6HIu09IOls3Fp+DgC2BUNH2hh/+SZor/6pPCEAq+mogBJ23MDv2X7ppL27z+D4OZgWrLfiykI4WPs9GQA1IyE+evMdv79wmellsQADSU3ucpH+e7gWLVbNRN8Cef70Kw3dJKU9I7w4zQOBoAorUfk95dwk/uDHLVf9IaxCALNXFdF93h3853StC0+mtNp9IKrxUCLqImM8NKXyJp8TJLGXT1AbG1Y9A9JXeUPJzgrynpBCPMdNDJL37TS9AOmIQAB2hmM+ZxD8udry/TnpDMKNpTAOBwghAACxQr4jAz7xr0nQCFu4wFQRyJwABsEf6r7zs/+Xix8Vee1OwAALFEoAA5MGb6Qny+J3ddnN7HuZgAwSKIgAByIu0UgF78lNC+HN5PaDJKzTYAYE4AhCA3GtD/S+T+IR3oHGH7q3B3F3DIAikJAABSAnMdLhQag973j2S1Jd2i8Yj1BbKdC7GgUBRBCAABZCO/jurJO9bTPwdIeTjTMFOvz/2q/Ex2vvLNvVwlFhAEuBiTQIQABRGqQnoftii1MFXIDgIQAWSVOcQIQBusw8BcMsX1i0JQAAsAWqmQwDc8oV1SwIQAEuAEAC3AGHdLQEIgFu+6ADc8oV1SwIQAEuA6ADcAoR1twQgAG75ogNwyxfWLQlAACwBogNwCxDW3RKAALjliw7ALV9YtyQAAbAEiA7ALUBYd0sAAuCWLzoAt3xh3ZIABMASIDoAtwBh3S0BCIBbvugA3PKFdUsCEABLgOgA3AKEdbcEIABu+aIDcMsX1i0JQAAsAbrvAHp9/PSV2yTV17oadDtj+OEUhwVg3QFMTC13hfQmHMYI0zUlwCpcXJwfx0+tO8y/tQBMTg++x0QXOIwRpmtKQBA9stBpXFjT5ReybHsBmAk+w8ybCokWTmpFQCjx2YV5H7XlMOvWAtDaGlxOgr/sMEaYrikBZnHF4px/T02XX8iy7QWgzSeqoL8bP31dSL5q40SROiiWmxvxk2tuU24tAFF4k1PBHSz5arehwnqtCAjx+e6s/75arXkIi81JAJbPYBI7SEp/CGuAy1EjoFRAxK/szo8/OWpLK9t6chGAaFETM4ObBNNHyrZAxFM9Aizo5sXZxt9WL/LqRZybAJy2hdctrw++KySdXT0MiLgsBBTRj9ft9S98+hZxsCwxjXIcuQnAoWcBy2eEJB+WUkyOMjSszQ2B6MUfQfR6tP5u+K5lNVcBiBy02v1zlVJfl+ThDa7i8lh5T9HmJ19evNhu/rDyi6nQAnIXgBURmFk+U7F3nyR6TYVYINRhEVD0E6LwMlz5i0+AEwGIlrHyTOCkYFYotQWnA8UnthIelQrYk7c2hT/zTFscqETMIxakMwFY5dSaWj6LPP96xeEVkuS6EeOH5WQgEL3kI4V3N4XBjbjqZwCY4xTnArAa68RH+CQaCy+VTBeR5HOUCk8XUpyMrxLnmM1SmlIDVvyclN5TpMRjStBD1PPuxxt+5UhWYQJQjuUiChAAgSMJQABQDyBQYwIQgBonH0sHAQgAagAEakwAAlDj5GPpIAABQA2AQI0JQABqnHwsHQQgAKgBEKgxAQhAjZOPpYMABAA1AAI1JgABqHHysXQQgACgBkCgxgQgADVOPpYOAhAA1AAI1JgABKDGycfSQQACgBoAgRoT+H+dQ8Hii/l95gAAAABJRU5ErkJggg=='),
(648, 'Web Distributed Authoring and Versioning', 'application/davmount', '.davm', ''),
(649, 'Web Open Font Format', 'application/x-font-w', '.woff', ''),
(650, 'Web Services Policy', 'application/wspolicy', '.wspo', ''),
(651, 'WebP Image', 'image/webp', '.webp', ''),
(652, 'WebTurbo', 'application/vnd.webt', '.wtb', ''),
(653, 'Widget Packaging and XML Configuration', 'application/widget', '.wgt', ''),
(654, 'WinHelp', 'application/winhlp', '.hlp', ''),
(655, 'Wireless Markup Language (WML)', 'text/vnd.wap.wml', '.wml', ''),
(656, 'Wireless Markup Language Script (WMLScript)', 'text/vnd.wap.wmlscri', '.wmls', ''),
(657, 'WMLScript', 'application/vnd.wap.', '.wmls', ''),
(658, 'Wordperfect', 'application/vnd.word', '.wpd', ''),
(659, 'Worldtalk', 'application/vnd.wt.s', '.stf', ''),
(660, 'WSDL - Web Services Description Language', 'application/wsdl+xml', '.wsdl', ''),
(661, 'X BitMap', 'image/x-xbitmap', '.xbm', ''),
(662, 'X PixMap', 'image/x-xpixmap', '.xpm', ''),
(663, 'X Window Dump', 'image/x-xwindowdump', '.xwd', ''),
(664, 'X.509 Certificate', 'application/x-x509-c', '.der', ''),
(665, 'Xfig', 'application/x-xfig', '.fig', ''),
(666, 'XHTML - The Extensible HyperText Markup Language', 'application/xhtml+xm', '.xhtm', ''),
(667, 'XML - Extensible Markup Language', 'application/xml', '.xml', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAZmklEQVR4Xu2de4wkxX3Hf1XdM7vHU4GdvThCDuZhwA6PxPgusuXY4R8ToygWiXAweQjZYOXhwycTg6K93dnZtQAbG/ApSowJckQEREhEiQyYRMGOFGEbEnMYWz7gwPzBEW5nF8fca3enpyrqvV1u75iZX3VXVU/X9nf/narf4/P79beru7p7BeEPBECgsgREZTNH4iAAAgQBQBOAQIUJQAAqXHykDgIQAPQACFSYAASgwsVH6iAAAUAPgECFCUAAKlx8pA4CEAD0AAhUmAAEoMLFR+ogAAFAD4BAhQlAACpcfKQOAhAA9AAIVJgABKDCxUfqIAABQA+AQIUJQAAqXHykDgIQAPQACFSYAASgwsVH6iAAAUAPgECFCUAAKlx8pA4CEAD0AAhUmAAEoMLFR+ogAAFAD4BAhQlAACpcfKQOAoUJQKOpTyLVvUIIuoy0vkSp7llCilOJZA1lyE+g3aoNrGFjcvmz7VZ9Z34PmLmRCXgXgPGJxbOJ4pu61L1GSnnCRoY5jNx4AVhKNEW/N9+KHxlGfPBZbgLeBODMph49qJMp6qobScq43BjCjY4XgI5WSh2QsfhQu1nfFW6miNwHAS8CsLm5eFZHRQ9HRBf7CBo2jxIwEYDV0XtVN9668EWxF/xAYI2AcwHYvGPpwkSIf5ckNgOzfwIZBCAN5hmS8W+1m+KA/8jgIQQCTgUgPfMnSj6Jg7+40mcUACKtvtXeXf84PSS6xUUJT2Ul4EwA0mv+N1XyfSz7iy11ZgEgIkVi50Ir3lZspPBWRgLOBKAx2bmViG4qY5IbOaY8AnCEh96G7cGN3BlmuTkRgHSrT5PYjbv9ZtBdjsovAKqL7UGXlQjTliMBSO7WUl8XJoKwo84vAETYHgy79i6itxaA9Ak/lSzvw0M+LsqR3YaNAKx6w/ZgduwbZoa9AEwmnyDSD24YIoEl4kAA0oyxPRhY3V2Fay0A41PJ17XW17sKCHayEXAkANgezIZ9w4y2F4DJzg800ZYNQySwRJwJALYHA6u8m3CtBaAxuTRPJE93Ew6sZCXgUgCO+Mb2YNYahDzehQAs45Xe4bWAewHA9uDwqlm8ZwcC0NHFhw2PawTcCwC2B6vUXRCAwKvtQwBWkWB7MPDeMAkfAmBCqcRjPApAmjW2B0tcexehQQBcUByiDc8CgO3BIda2CNcQgCIoe/ThXQCwPeixesM3DQEYfg2sIihCALA9aFWiUk+GAJS6PHxwxQkAtgf5aoQ3AgIQXs2Oibg4AcD2YOCt0jN8CEDgVS1SALA9GHiz9AgfAhB4TYcgANgeDLxn1ocPAQi8mEMSAGwPBt43a+FDAAIv5NAEANuDgXfOkfAhAIGXcZgCgO3BwJsHAhB+AYcvANgeDLmLsAIIuXpENHwBwPZgyC0EAQi5eiURAGwPhttEEIBwa7cSeRlWAOsQ4u3BwPoJAhBYwY4Pt2QCgO3BwPoJAhBYwUovANgeDKqjIABBlevtwZZuBfBWiPi4aAitBQEIoUoDYiyvAGB7MITWggCEUKUgBQDbgyG0FgQghCoFKgDYHix/c0EAyl+jgRGW9xLgmLCxPVjSPoMAlLQwpmEFIgDYHjQtaMHjIAAFA3ftLhgBwPag69I7sQcBcIJxeEZCEoAjlLA9OLxuebtnCECZqpEjlvAEANuDOcrsbQoEwBvaYgyHJwDYHiymM8y8QADMOJV2VIgCgO3B8rQTBKA8tcgVScACkOaL7cFcVXc3CQLgjuVQLAUuANgeHErXHHUKARhyAWzdt2U8Qk2x3M/O+MTSkpaybuvH53xFYudCK97m0wds9yYAAQi8M2oybrzWFPP90mhMdnYT0XnlTxPbg8OoEQRgGNQd+lRab1mYqT/dVwCmkr8hrf/coUtPprA96AnsQLMQgGFQd+hTa33d/Ez9nr6XAFNLF2lNu4ikda0dht3TlFLqgIzFh9rN+i7fvmD/CAHrpmhMdjRgDo+AEPr+uen6NYMiGJ9M/k6T/szwoszkea/qxlsXvij2ZpqFwbkIQAByYSvTJLV/ZH9986t3iMP9ojqzqUcPquTbRPThMkU+IBZsDxZUKAhAQaC9utH62vZM/ZuDfJyxXW9aPrF7l5bdT4dwOUBafau9u/5xekh0vbKruHEIwEZoAE0vtKP4vdQUCZdOY8fyrwshP6NJXyaU+tUybxFie5Crpv3vEAB7hqWwoDV9fn6m9tVSBIMggiEAAQimVIMDVaQO1zS9//WZkZ9skJSQRgEEIAAFQC7QxYtRJ/7g67eIdoE+4SpgAhCAgIvXJ/RnpIgv3zct5jZeasjINQEIgGuiJbCnumJPLLtX7psZea4E4SCEEhOAAJS4ODahKaUWIykn52R816CXhWx8YG74BCAA4ddwYAaKxEuC1G3Rodr9+24XBzd4ukgvIwEIQEZgoQ5XpA5Kko8KrZ8gEs/qKP7ZiUT/90qTlogEHucOtbCWcUMALAFiul8C3AdP/Hrf+NYhABu/xkFnCAHwWz4IgF++sG5JAAJgCZCZDgHwyxfWLQlAACwBQgD8AoR1vwQgAH75YgXgly+sWxKAAFgCxArAL0BY90sAAuCXL1YAfvnCuiUBCIAlQKwA/AKEdb8EIAB++WIF4JcvrFsSgABYAsQKwC9AWPdLAALgly9WAH75wrolAQiAJUCsAPwChHW/BCAAfvliBeCXL6xbEoAAWALECsAvQFj3SwAC4JcvVgB++cK6JQEIgCVArAD8AjSxrqjblkJ+Ryv6nhDyeVLJnnp35I1NB+jAnp1iycQGxoCADwJYAfigumJT/Zwoul9qdd++mdpT+OqON9AwbEEAAmABr9dUpdTrQsgviyi+u90UBxybhzkQcEoAAuAKp1KJjuSddDhuzX9J7HdlFnZAwCcBCIADuitf3pXqqvlm/YcOzMEECBRGAAJgiVpo+tckiv/4jaZ409IUpoNA4QQgADbIhbinLaI/M/m33DZuMBcEfBGAAOQlmx7809H1uLufFyDmlYEABCBHFTTRv8zL+A+ynPl/6SZ9aq3e/V0t6bdJ64uFUu/Skk4mkrUcIWBKZQioDhG9SSR/RkLs0or+g5aiR1zdaIYAZG+kF7syvtT0mn9sYvE8HcU3U7f7h1LK0ezuMAMEjiWglDokKXqAouTW9vToHhs+EIAs9JRKKBbvbzfru7hpv9LUJyQqmdWkthHJiBuP30EgOwHV0STvHN0fT716hzicfT4RBCAbtdvardrN3JT0rC9F9LAW9B5uLH4HAVsCiug5KbpX5lkNQABM6St6TS7G7+b+w+7YjuX3kVbfFjIaMzWNcSBgS2DlfRMtP9qeqT+TxRYEwJCW1nr7/Ez9zkHD0zM/Ef0XDn5DqBjmlMCKCCj6YHt29EVTwxAAA1JCqTfEYv2dg87+m2/UJ6oTkqeJ6AIDkxgCAl4IpJcDm/bHW03vCUAADMqgSOxcaMXbBg1tTHbS1cENBuYwBAS8EhBEX5lr1W40cQIBMKCktN6yMFNPz+49/05vLp0vFf0Yd/sNYGKIfwIrL6bp985Pj77AOYMAMIQU6X0Lrdo7Bj3x15ha/gfS4k842PgdBIoioIT45sJ0fC3nDwLAEBJKPzA3W/9kv2HpE36ytvw6HvLhWg2/F0lAkTqsZf2XuQfWIABsVfTn2q36Xf2Gje9I/kgLfR9rBgNAoGACWotr5mfi+we5hQBwKwCiy+datcf7Xv9PJPdKqdmlVsG1hzsQIFLi79uz8achABbNIFT3nLnZ0Zf6mWhMdP6HJP2GhQtMBQFfBJ5ut2pbIAAWeOudeGzvLWKh7yXAxNKClvI0CxeYCgJeCGjVnZ+fHW1AACzwtmU8Qk2x3HcFMLm0jFd6LQBjqkcCqtNujdQhABaIuX9M0ZjsaAvzRCQ+1m7Fj/WzMbZj6TEh5OV2PvrNVo+1WyMf6y9uye8Q6UcH+Tbg810i+rCf+Ok/263aRwbZ5upjEL9lfT1lbmiWyw83ARmQHECuwQzq9NO2jC/q93GR0yeWLpBEPyIpYwNb5kOUSlRMFy40R3b3nNTUcaObPEeCzrcRgLFm5yNC0XfMAzMfmX5cZb5ZSwWm7x9XnwLqa56Qh5FcfhCA4QsAEelt7VZ9Z98z8VSyk7T+S5f9wT3e3NixvI2E6Lv9uRYL12DpuMZkx8cqgD37r/oeeAbn4ucExGVNfNji8oMAlEAA0peNFuP6uW82xRu9wjmlqU8bTZZfdHWz0aU/rsHSfHysAkzO/hAAIq4+EIASCEAagqszstlZxN2Kg2uwtXgcrwKMzv4QAAiA2fEwYBTX4M6WiI6uyQ0SdnrPgeOzFo/LVYDp2R8CAAEwOB4GD+Ea3JkArIRhf1eeT9jtrgPHZ308jlYBxmd/CAAEgD8eSnIJcDQMtwfosem5F5gsAuBiFZDl7A8BgAAEKADkdIn+FgBPlxhZBMDBjkCmsz8EAAIQogB42Rb0dZMxqwDYrAKynv0hABCAIAXA5TZdCsC1vfVQswqAxSog89kfAgABCFIAVm4HMt8hNH1Q5wgAd9t+xwPNIwB5VgF5zv4QAAhAsAJA7q7Z/dxTWCWbRwByrAJynf0hABCAcAVgJXIXd+197irwDdavAFlWAXnP/hAAvj54ErB024DHB2RzALsQkMGA8q4AMqwCcp/9IQAQgMBXACvh51vCu7uEGMjQRgBMVgE2Z38IAARgIwhArpt4bm8i9sfICUB6kA96ZZd5OnDg2Z+zDQGAAGwIAci6jZd1vA0kTgDSA3zQRzsGrQK4sz9nGwIAAbDp7ZW5Bg1eyBdjsp3R/W37Zd0GTN+VMDmQe3w1iD37px8asa2P7XzrBvNsgMsPNwFLfxNwNUDza3pd5BeGuAZbfVnK6GBeXwpT0TD037fKtvM9H7/W5rn8IAChCMBKnCZ39dNVS3HfGOQabO1tSdMDerUcxoJh6r9fmW3nWx+hng1w+UEAghKANNjB24KD0mlM8h/5zNqPXIOte13a+KDOIhYZ/PdMzXZ+Vl5Fj+fygwAEJwCDtwX7pmP4kc+sDco12PrvJRge2Ol9l75f+j3+pmEW/71ys52flVfR47n8IADhCQC7LdgrpWzvDpi3Kddgx30whV0FpJ6zbBtm9P+2xGznm5MazkguPwhAgALAbfMdn5Lrj4qut8812PFfTOJWAYPK0WvLMKv/4+3bzh/OYW3ulcsPAhCgAKQhc9uC69NqePis+Jp9rsF6fDIt96O9vR4ayuH/mIrbzjc/FIczkssPAhCoALBvC67m5e0fi6za5xqs1zcT86wC+j0wlMe/zQpmOIdxfq8cHwhAqAKwEvfgbcF0hN9/LZb7QanMq4B+jwxzDc59tNV2fv5Ds5iZXH4QAAiAVSdyDdbvAMyyChj0uHBe/xaXMFa8ip7M8YEAhCoA3JOBJb4EWA3NeBUw6IUhrsGxAqgNPMYhAIEKQMA3Ad8ibrIK4F4ZhgAMbmCODwQgQAEIeRvwONzsKoD7ZyJcg2MFgBWA1WWXbYNZOe87efDbfr2mleRBIOc4bOtjO995Qo4NcvlhBRDeCmDgF4L6plOCR4Ed9/aKOa7BsQLACsCq72wbzMp5z8nBvgzkHgUEgGXK9S9WAEGtAMJ9HZjt1JwDuAbHCgArgJytdWSabYNZOV8/mdv2O7rEL+MHQZxhON6QbX1s53tLzJFhLj+sAAJZAXDbfsfe5CvXJ8Ec9XJPM1yDYwWAFYBV/9k2mJXz1cnctt/xb/tlHW8T47D52Pq3nW/Droi5XH5YAQSxAsh+Rs+2YsjfilyDcWfg/J7dXKINO37b/Ln5XH4QgPILQND/GAQCwB2ifn+HAFjy5QD6bnDuG4CD3/Yz2TXQj9ogGjYfW/+2823YFTGXyw8rgFKvAFwcwDb/W5BvUa7BfAukrX/b+Tyh4Y7g8oMAlFUAzLf9zmdSyHcJYdi3XINBAAxBehrG1QcCUFIBcHsTL/tNRNN+5BoMAmBK0s84rj4QgBIKgOttPNf21iPjGgwC4OfANrXK1QcCUEIBIHJ/xna7ojgKjWswCIDpoepnHFcfCED5BMDPNbu7ewrHEOMaDALg58A2tcrVBwJQOgHwedfexa7CscC4BgtdAEwPNF/jbPlx9YEAlEoA3B+gb0/PrcBwDWbbwNyBZeufm8/59/27LT8uPwhAWQTA0xK9R3pOLzG4BrNtYO4As/XPzef8+/7dlh+XHwSgJALg6yZd7/Tc3WTkGsy2gbkDzNY/N5/z7/t3W35cfhCAEgiAz226Xum59Mc1mG0DcweYrX9uPuff9++2/Lj8IAAlEAAf235cY7pacXANZtvAXB62/rn5nH/fv9vy4/KDAAxfAJxekxs3pKN7DlyD2TYwl4+tf24+59/377b8uPwgAEMXALd35bM1pP2uA9dgtg3M5WPrn5vP+ff9uy0/Lj8IACcAMh6hpljuN6wxubRMJGu+GwH2QSArAaHU8tzsyMigeRAAhmq9E4/tvUUs9Bs2PrG0oKU8LWtxMB4EfBNQ1G0vtEbHIQA2pEX33Pb06J7+K4DOfxPR+2xcYC4I+CAgiJ6aa9W2QgAs6Aqiy+datcf7CsBEcg9J/SkLF5gKAl4ICCW+MTcbXw8BsMKrP9du1e/qZ2JsMrlGkP5HKxeYDAIeCGgtrp6fiR+EANjAFfrB9nT96n4mTmvqU4Rafl2S3GTjBnNBwCUBpdQhGdc3t5viAATAgqwivW+hVXsHkdD9zJw+kdwrpb7Wwg2mgoBbAkLc056Or+OMYheAI0RESustCzP1p/teBkwtvlt0xU9IytjAHIaAgGcCqiOUvmBudvQlzhEEgCOU/i7oa+3p2g2Dho5Pdm7XRJ83MYcxIOCZwG3tVu1mEx8QABNKpBZqsv7O15riUL/hZ2zXm5ZOTJ4iSb9mZBKDQMADgS7Rs6fI+DdfaYpFE/MQABNK6Ritb2jP1L82aHhjavEcpelJSVHD1CzGgYArAun9qliqD+xrjr5sahMCYEqKaK88FJ+373ZxcKAINJcvUQk9LqUY+ASWuVuMBAGeQHrwR5o+OjdTf5YffXQEBCALLaJb2q3aX3NTxicWz1YUPSwkXcSNxe8gYEugq2hXLe7+fpYz/5pPCEAm+qojBF06Nz3yI27amU09ekAn00Kr7XhZiKOF3/MRUB0S8iunvhE39+wUS3lsQACyU3ueZHwp94DFmtl0NaCj+GbV7X5SSnlCdneYAQLHElCkDkYqul/Eya15zvrrrUEA8nSXpofbu+Or6CHRNZ3eaOqTSHWvEIIuI60v6VL3XZESp2op66Y2MK56BNJXertS/0JQ9LIU4lmt6QmS0SOmJyCOGASAI9Tnd03ib+db0V8MekIwp2lMA4HCCEAALFCviMBPo89mWQlYuMNUEHBOAAJgj/Sf9WL8p/NfEvvtTcECCBRLAALggremFyjSn2g367tcmIMNECiKAATAFWmlEh3JrwoRz7i6QeMqNNgBgX4EIADOe0P9rybx5ehQ7W7uqUHnrmEQBDISgABkBGY6PP3vOzqKHpSk7tsnak9RUyjTuRgHAkURgAAUQDr9Oquk6Lua9PeEkM9rSvbEyyM/Hx2h/a80aQlbiQUUAS56EoAAoDFKTYD7xxalDj6A4CAAARSpyiFCAPxWHwLgly+sWxKAAFgCZKZDAPzyhXVLAhAAS4AQAL8AYd0vAQiAX75YAfjlC+uWBCAAlgCxAvALENb9EoAA+OWLFYBfvrBuSQACYAkQKwC/AGHdLwEIgF++WAH45QvrlgQgAJYAsQLwCxDW/RKAAPjlixWAX76wbkkAAmAJECsAvwBh3S8BCIBfvlgB+OUL65YEIACWALEC8AsQ1v0SgAD45YsVgF++sG5JAAJgCdD/CmBpGf/6ym+RqmtdddqtEfzjFI8NYL0CGJtYbAsZjXmMEaYrSkCr7vz87Cj+1brH+lsLwPhk5weaaIvHGGG6ogQE0VNzrdrWiqZfSNr2AjCVfF1rfX0h0cJJpQgIJb4xNxujtzxW3VoAGjuSq0jof/IYI0xXlIDW4ur5mfjBiqZfSNr2AtDUJ6lkeR/+9XUh9aqME0XqsFisb8a/XPNbcmsBSMMbn0ju1lJf5zdUWK8UASHubU/Hn6pUzkNI1pEALJ6tSewmKeMh5ACXG42AUgmRfk97dvTFjZZa2fJxIgBpUmNTnduEpi+ULUHEEx4BLej2+enaX4UXeXgROxOAM7brTYsnJt8Xki4KDwMiLgsBRfTcpv3x1lfvEIfLEtNGjsOZABy5F7B4dpfkk1KK8Y0MDbn5IZA++COIPoClvx++vaw6FYDUQaO5fIlS6t8kRXiCq7g6Bu8pPfgplh+db9Z/GHwyASXgXABWRGBq8Rylo4cl0YUBsUCowyKg6MdE3Stx5i++AF4EIE1j5Z7Aycm0UGo7dgeKL2wQHpVKdCTvrIt46rWmOBREzBssSG8CsMapMbF4LkXxzUp3r5YkN20wfkgnB4H0IR8pogeom9yKs34OgA6neBeAtVjHvqBPppHuFVLTZST1xUp1zxJSnIpXiR1Ws5SmVEcr/Qspo5dJiWeVoCdoKXoET/iVo1iFCUA50kUUIAAC6wlAANAPIFBhAhCAChcfqYMABAA9AAIVJgABqHDxkToIQADQAyBQYQIQgAoXH6mDAAQAPQACFSYAAahw8ZE6CEAA0AMgUGECEIAKFx+pgwAEAD0AAhUmAAGocPGROghAANADIFBhAhCAChcfqYMABAA9AAIVJvD/2Q7y0/ie88AAAAAASUVORK5CYII='),
(668, 'XML Configuration Access Protocol - XCAP Diff', 'application/xcap-dif', '.xdf', ''),
(669, 'XML Encryption Syntax and Processing', 'application/xenc+xml', '.xenc', ''),
(670, 'XML Patch Framework', 'application/patch-op', '.xer', ''),
(671, 'XML Resource Lists', 'application/resource', '.rl', ''),
(672, 'XML Resource Lists', 'application/rls-serv', '.rs', ''),
(673, 'XML Resource Lists Diff', 'application/resource', '.rld', ''),
(674, 'XML Transformations', 'application/xslt+xml', '.xslt', ''),
(675, 'XML-Binary Optimized Packaging', 'application/xop+xml', '.xop', ''),
(676, 'XPInstall - Mozilla', 'application/x-xpinst', '.xpi', ''),
(677, 'XSPF - XML Shareable Playlist Format', 'application/xspf+xml', '.xspf', ''),
(678, 'XUL - XML User Interface Language', 'application/vnd.mozi', '.xul', ''),
(679, 'XYZ File Format', 'chemical/x-xyz', '.xyz', ''),
(680, 'YAML Ain''t Markup Language / Yet Another Markup Language', 'text/yaml', '.yaml', ''),
(681, 'YANG Data Modeling Language', 'application/yang', '.yang', ''),
(682, 'YIN (YANG - XML)', 'application/yin+xml', '.yin', ''),
(683, 'Z.U.L. Geometry', 'application/vnd.zul', '.zir', ''),
(684, 'Zip Archive', 'application/zip', '.zip', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAYzElEQVR4Xu2df4wkxXXH36vumd3jwBfDzh62LIdfNv4R8yPY4NhyEvOHQUFRLJLgAPkhywbiWGCwHUBob3d29izAxgZ8SowPjBwRAQ4KUSITgaNgK7LAQGyOYMcHHJhIHLnd2YXA/dqd6a4XzR0HB+xMdU91dXdNf+/fq3pV9XmvP1tdNbvDhH8gAAKVJcCVXTkWDgIgQBAAigAEKkwAAqhw8rF0EIAAUAMgUGECEECFk4+lgwAEgBoAgQoTgAAqnHwsHQQgANQACFSYAARQ4eRj6SAAAaAGQKDCBCCACicfSwcBCAA1AAIVJgABVDj5WDoIQACoARCoMAEIoMLJx9JBAAJADYBAhQlAABVOPpYOAhAAagAEKkwAAqhw8rF0EIAAUAMgUGECEECFk4+lgwAEgBoAgQoTgAAqnHwsHQRyE0CjKYeSjs9iptNJ5CSt42NY8ToiVUMahifQbtUG5rAx3bm43apvGn4E9BxlAs4FMDm1fCxReEVM8flKqUNGGWYRazMLYCUSCv5gsRXeU8T8MGa5CTgTwFFNGd8t0QzF+sukVFhuDP7OziyArmitd6mQP9Zu1rf4u1LM3AUBJwJY31w+pquDuwOiE11MGjFfI5BEAK+03q7j8LSlr/B28AOBAwQyF8D6DSsfiJj/TRGvB2b3BFIIoDeZR0mFv91u8i73M8MIPhDIVAC9n/yRVg/g4c8v9SkFQCT6++2t9U/SXRznN0uMVFYCmQmg987/so5+gm1/vqlOLQAi0sSbllrhJfnOFKOVkUBmAmhMd68hoivKuMhRntMwAtjPQy7B9eAoV0aytWUigN5VnxBvxWl/MuhZthpeADrG9WCWmfAzVkYCiDaLkgv8ROD3rIcXABGuB/3OfRaztxZA7xN+OurM40M+WaQjfQwbAbwyGq4H02MfmR72ApiOPkUkd44MEc8WkoEAeivG9aBnec9qutYCmJyJvi0iF2Y1IcRJRyAjAeB6MB32kWltL4Dp7kNCdOrIEPFsIZkJANeDnmU+m+laC6AxvbJIpI7IZjqIkpZAlgLYPzauB9PmwOf2WQigg1/pLa4EshcArgeLy2b+I2cggK7kP22MeIBA9gLA9WCVqgsC8DzbLgTwChJcD3peG0mmDwEkoVTiNg4F0Fs1rgdLnPsspgYBZEGxwBiOBYDrwQJzm8fQEEAelB2O4VwAuB50mL3iQ0MAxefAagZ5CADXg1YpKnVnCKDU6TFPLj8B4HrQnA3/WkAA/uXsdTPOTwC4HvS8VFadPgTgeVbzFACuBz0vllWmDwF4ntMCBIDrQc9r5uDpQwCeJ7MgAeB60PO6OTB9CMDzRBYmAFwPel45+6cPAXiexiIFgOtBz4sHAvA/gcULANeDPlcRdgA+Z4+IihcArgd9LiEIwOfslUQAuB70t4ggAH9zt2/mZdgBHIQQvz3oWT1BAJ4l7I3TLZkAcD3oWT1BAJ4lrPQCwPWgVxUFAXiVrjdPtnQ7gFeniD8u6kNpQQA+ZGnAHMsrAFwP+lBaEIAPWfJSALge9KG0IAAfsuSpAHA9WP7iggDKn6OBMyzvK8Drpo3rwZLWGQRQ0sQknZYnAsD1YNKE5twOAsgZeNbDeSMAXA9mnfpM4kEAmWAsLohPAthPCdeDxVXLm0eGAMqUjSHm4p8AcD04RJqddYEAnKHNJ7B/AsD1YD6VkWwUCCAZp9K28lEAuB4sTzlBAOXJxVAz8VgAvfXienCorGfXCQLIjmUhkTwXAK4HC6ma1waFAApOgO3wbRWOUZM7/eJMTq2siFJ123Fc9tfEm5Za4SUux0Ds1QlAAJ5XRk2FjeebvNhvGY3p7lYiOr78y8T1YBE5ggCKoJ7hmFrk1KW5+iN9BTAT/Q2J/FWGQzoKhetBR2AHhoUAiqCe4ZgicsHiXP2Wvq8AMysniNAWImWd6wynvWoorfUuFfLH2s36FtdjIf5+AtZF0ZjuCmAWR4BZbl+YrZ8/aAaT09FNQnJRcbNMNfJ2HYenLX2Ft6fqhcZDEYAAhsJWpk5659jO+vrnrue9/WZ1VFPGd+voXiL6nTLNfMBccD2YU6IggJxAOx1G5NPtufp3B43xjstkTWdtfKOo+LM+vA6Q6O+3t9Y/SXdx7JRdxYNDAKNQAEJPtoPw/dTkyLScxobOyczqIiE5nbX+9TJfEeJ60JRN+/+HAOwZliKCCH1pca72jVJMBpPwhgAE4E2qBk9Uk95bE/rQjrmxX4zIkrCMHAhAADlAznGIp4Ju+NEdV3M7xzExlMcEIACPk9dn6o8qDs+cn+WF0VsaVpQ1AQgga6IliKdj3haq+Oz5ubHHSzAdTKHEBCCAEifHZmpa6+VAqekFFd446JeFbMZAX/8JQAD+53DgCjTx00z62mBP7fb563j3iC8Xy0tJAAJICczX5pr0bkXqX1nkfiJ+TILwV2uJ/u/ZJq0QMT7O7WtiLecNAVgCRHe3BEx/8MTt6KMfHQIY/Rx7vUIIwG36IAC3fBHdkgAEYAnQ0B0CcMsX0S0JQACWACEAtwAR3S0BCMAtX+wA3PJFdEsCEIAlQOwA3AJEdLcEIAC3fLEDcMsX0S0JQACWALEDcAsQ0d0SgADc8sUOwC1fRLckAAFYAsQOwC1ARHdLAAJwyxc7ALd8Ed2SAARgCRA7ALcAEd0tAQjALV/sANzyRXRLAhCAJUDsANwCRHS3BCAAt3yxA3DLF9EtCUAAlgCxA3ALMEl0TXFbsfqhaHqQWT1BOtpWj8deWLOLdm3bxCtJYqANCLgggB2AC6r7YuoXiYLblejb5udqD+Ov7jgDjcAWBCAAC3irddVa72BWX+Mg3Nxu8q6MwyMcCGRKAALICqfWkQTqBtobtha/yjuzCos4IOCSAASQAd19f3lX6XMWm/WfZRAOIUAgNwIQgCVqFvqXKAj/7IUmv2wZCt1BIHcCEIANcuZb2hx8LsnXctsMg74g4IoABDAs2d7DPxtciNP9YQGiXxkIQABDZEGI/nlRhX+U5if/W6+QdbV6/Pui6OMkciJrfbQoOoxI1YaYArpUhoDuEtHLROpXxLxFNP07rQT3ZHXQDAGkL6SnYhV+MOk7/8TU8vEShFdSHP+JUmo8/XDoAQKvJ6C13qMouIOC6Jr27Pg2Gz4QQBp6WkcU8ofazfoWU7e3N+WQSEcbhfQlRCowtcf/g0B6ArorpG4Y3xnOPHc9703fnwgCSEft2nardqWpS++nvuLgbmF6n6kt/h8EbAlooscVx2cPsxuAAJLS1/S8Wg7fbfqG3YkNnVNI9L2sgomkodEOBGwJ7Pt9E1FntOfqj6aJBQEkpCUily3O1W8Y1Lz3k5+IfoyHPyFUNMuUwD4JaPpoe+P4U0kDQwAJSLHWL/By/Z2Dfvqv/7Ks1YdEjxDRexOERBMQcEKg9zqwZmd4WtIzAQggQRo08aalVnjJoKaN6W5vd/CFBOHQBAScEmCiry+0al9OMggEkICSFjl1aa7e++m+6r8jmivvUZp+jtP+BDDRxD2Bfb+YJu9fnB1/0jQYBGAgpEnml1q1tw36xF9jpvN3JPznJtj4fxDIi4Bm/u7SbPhp03gQgIEQa7ljYWP9vH7Nep/wU7XODnzIx1Rq+P88CWjSe0XVjzR9YA0CMGZFLm236jf2aza5IfpTYbnNGAYNQCBnAiJ8/uJcePugYSEA0w6A6MyFVu2+vu//U9GtSolxq5Vz7jEcCBBp/k57Y/hZCMCiGFjHxy1sHH+6X4jGVPenpOg3LYZAVxBwReCRdqt2KgRggbfeDSe2X81LfV8BplaWRKnDLYZAVxBwQkB0vLi4cbwBAVjgbatwjJrc6bsDmF7p4Fd6LQCjq0MCuttujdUhAAvEpi+maEx3xSK8satpfGMAxw1M63c9/6LHp6bUj1yhdRJ2j4mZTmKm07XIWYrUWsfoE4U38cchoAGjCaCpABNlaUAj0/i28W37m9bvev5Fj78av0ZTDpW4e57W6oogkGNsGdv0N/GHACAAm/qioh/AoscfCK8p9UYcfVGzbipSY1agh+wMAQwJ7kA3E0BTAVoOT6bxbePb9jet3/X8ix4/Cb/JDZ0TI63uLmI3YOKPHQB2AElquG+boh/AosdPCm/yKlmvVfQDVnRC0j5ZtIMALCmaAJoK0HJ47AAMAE38TfmzzU+a/j0JRBw/kOdOwLR+7ACwA0hTw29qW/QDWPT4aeH1XgdilofyOhOAANJm6A3tTQBNBWg5PHYAI7QDOLCUxnT3CiK6xrY2kvQ31S92ACXfASRJsqs2rPnmhY3hhf3iT0x3LmLim2zGNxWorWBdx2etO6LU/zDx/VrpzYm+H7Ip9cO78S/zeBUwrR8CgAD6EXhw3Yvhx7dt4pXVGkxu6H5UWP/Q9lOQpgItuwDexEbzd8Z2Bxeb/iTXxHTnAibebCPPJH1NfCEACODNNaz1Dg7qpyzO8vOr4ZmYkbdL3PmpUurIJEU4qI2pQL0TABEJ0Y/Hd4afGCSB3oeFtO7scP2JQRNfCAACeAMB3WWlfnehWXtgNTTHXSxjL701+hERfdj24e/1NxWojwLYx2Xfd0eGFwxi1Jju/gMR/XEWHPvFMPGFACCA1xEQkYsW5+p9t6aTU9FmUTKwsNMUtKlAvRUAaSHhUwb9nf7JDZ2/FOZvpeGVtq2JLwQAAbxKII9DvzfiNhWovwIgEuabFmfDz/UrsckN3d8SplV3WmkfdOwAsiL2hjiuC9TRtIcJm8uhX5UEQERPtFu19/QVwFWyXsJoxzDJStrHVL/YAWAHQDrHQ78qCUCTXllqjfX9RuhXzlOWkz7Mw7SDAIahdlAfE0DbLarl9DLonu+hHwTwGgEIIIPydR1i1AWQ96FflQRAQlvbc7W+XxW3fkYmtUTzLmvYVL94BajwK0ARh36VEgDz37Znw8/3K7H1M50Pa+EHIQCXBCxjmwzq8StAIYd+1RGAFhY+eWGu/li/Eszio9Sm8jbVL3YAFdwBFHnoVxUBmK4AexwaM53vkfA5pofY5v8hABt6OXxSzXJ6Q3Qv9tCvIgL4j7UqPOPZJvc94e99nXx0SGceHwUeooTz7GIyqG+vAEUf+o22ALQwBZsPUcGlgx7+HoOJDZ3PMvPNrmvZVL94BajQK0AZDv1GTQC9u34l6lek+H7WevOgd/5X196U+hE6/m9FciwE4JqAZXyTQT3aAZTi0K9sArAsj6G64w+CDIWtmE6jIIAyHfpVXQD4k2DFPMdDj+q/AMp16FdlAeCPgg79GBbX0XcBlO3Qr6oCaDTlyFhH9wZEJ+ZZzab6xSHgCB8ClvHQr4oCaGzonCys/pFJjs7z4e+NBQFYEjcBLPEhYCkP/aokgFd+2eeLrHVTlBr4Lb2WZdq3u6l+sQMYwR1AmQ/9qiCAicvlMDXWPV+zuryIn/oHM4YALNVqAli+HUC5D/1GSwDCRzVpbDfRrzFFx1AsJ8XMpxPp31Ok1liWXibdTfWLHcCI7QDKfuhXNgGUT+CZPPevBoEALHmaAJapgHw49IMALAsyZXdT/WIHMDo7AC8O/SCAlE+wZXMIwDHAMuwAfDr0gwAsCzJldwggJbC8C9RyekTk16Ff3nxND0AZBG5fA/0jmNaPVwDPXwF8O/SDAFw+7m+ODQFY8jYBLPIniI+HfhCAZUGm7G6qX+wA/N0BeHnoBwGkfIItm0MAjgEWsQPw+dAPArAsyJTdIYCUwPIu0PTT8/vQL2++pgegCIGnz/nwPUzrxyuAZ68Avh/6QQDDP8zD9IQAhqF2UB8TwDx/gozCoR8EYFmQKbub6hc7AH92ACNx6AcBpHyCLZtDAI4B5rEDGKVDPwjAsiBTdocAUgLLu0DN0xutQ7+8+ZoegDwEbs6xuxam9eMVoOSvAKN26AcBuHvYV4sMAVjyNgF0/RPENL7l8qy7m9bvev6245v6WwMqOICJP3YAJd8BmBJYcH2R6QFyPX/b8U39i+ZrO76JPwQAAVjVmOkBMhWg1eC9b9id7sqgGKbxTf1t51d0f9P6IQAIwKpGTQ+QqQCtBocAjPhM/CEACMBYRIMaQABW+Jx3hgAsEZsAmh4Ay+GNX+xgG9+2v2n9Jn5Fj2+av+38iu5v4o8dAHYAVjVqeoBMBWg1OF4BjPhM/CEACMBYRHgFsEJUaGcIwBJ/W4Vj1OROvzCN6ZUOkapZDoPuIJA5Ada6s7BxbGxQYOwADNjr3XBi+9W81K/Z5NTKkih1eObZQ0AQsCSgKW4vtcYnIQAbkBy/qz07vq3/DqD7n0R0is0Q6AsCLggw0cMLrdppEIAFXSY6c6FVu6+vAKaiW0jJZyyGQFcQcELA9PcjeoPiFcCIXi5tt+o39ms2MR2dzyR/bwyDBiCQMwERPndxLrwTOwAb8Cx3tmfr5/YLcXhT3sK6s6Ms3wZrs1T0HR0CWus9Kqyvbzd5FwRgkVdNMr/Uqr2NiPt+5vyIqehWpeTTFsOgKwhkS4D5lvZseIEpKF4BTIR6X74lcurSXP2Rvq8BM8vv5ph/QUqFCcKhCQg4JqC7rOW9CxvHnzYNBAGYCO0/Kflme7b2hUFNJ6e71wnRl5KEQxsQcEzg2nardmWSMSCAJJRIL9VU/Z3PN3lPv+bvuEzWrKyNHiZFv5EoJBqBgAMCMdFjb1Hhh59t8nKS8BBAEkq9NiJfaM/VvzmoeWNm+Tgt9ICioJE0LNqBQFYEeudVodIfmW+OP5M0JgSQlBTRdrUnPH7+Ot49UALNzkk6ovuU4oGfwEo+LFqCgJlA7+EPhM5YmKs/Zm79WgsIIA0toqvbrdpVpi6TU8vHagruZkUnmNri/0HAlkCsaUstjP8wzU/+A2NCAKno6y4zfXBhduy/TN2Oasr4LolmWfRl+GUhEy38/3AEdJdYfX3dC2Fz2yZeGSYGBJCe2hOkwg+aPmBxIGxvNyBBeKWO4/OUUoekHw49QOD1BDTp3YEObucwumaYn/oHR4MAhqkuobvbW8Nz6C6Ok3ZvNOVQ0vFZzHQ6iZwUU3x0oHmdKFVPGgPtqkeg9yu9sZKXmIJnFPNjInQ/qeCepD+ATMQgABOhPv8vxN9abAWfH/QJwSFDoxsI5EYAArBAvU8CvwwuTrMTsBgOXUEgcwIQgD3Sf5Ll8C8Wv8o77UMhAgjkSwACyIK30JMUyKfazfqWLMIhBgjkRQACyIq01pEE6hvM4VxWBzRZTQ1xQKAfAQgg89rQ/yvEXwv21DabPjWY+dAICAIpCUAAKYElbc5avyBBcKcifds81x6mJuukfdEOBPIiAAHkQLr311kVBT8SkgeZ1RNC0bawM/bi+BjtfLZJK7hKzCEJGGJVAhAACqPUBExfbFHqyXswOQjAgyRVeYoQgNvsQwBu+SK6JQEIwBKgoTsE4JYvolsSgAAsAUIAbgEiulsCEIBbvtgBuOWL6JYEIABLgNgBuAWI6G4JQABu+WIH4JYvolsSgAAsAWIH4BYgorslAAG45YsdgFu+iG5JAAKwBIgdgFuAiO6WAATgli92AG75IrolAQjAEiB2AG4BIrpbAhCAW77YAbjli+iWBCAAS4DYAbgFiOhuCUAAbvliB+CWL6JbEoAALAG63wGsdPDVV26TVN3outtujeGLUxwWgPUOYGJquc0qmHA4R4SuKAHR8eLixnF81brD/FsLYHK6+5AQnepwjghdUQJM9PBCq3ZaRZefy7LtBTATfVtELsxlthikUgRY880LG0PUlsOsWwugsSE6h1i+53COCF1RAiJ87uJceGdFl5/Lsu0F0JRDddSZx1df55KvygyiSe/l5fp6fOWa25RbC6A3vcmpaLMoucDtVBG9UgSYb23Php+p1JoLWGxGAlg+Voi3klJhAWvAkKNGQOuISN7X3jj+1KgtrWzryUQAvUVNzHSvZaHLy7ZAzMc/AsJ03eJs7a/9m7l/M85MAO+4TNYsr41+wopO8A8DZlwWApro8TU7w9Oeu573lmVOozyPzASw/yxg+diY1ANK8eQoQ8Pa3BDoffCHiT6Crb8bvqtFzVQAvQEazc5JWusfKArwCa788uj9SL2Hn0J1xmKz/jPvF+PRAjIXwD4JzCwfpyW4WxF9wCMWmGpRBDT9nCg+Gz/580+AEwH0lrHvTOCwaJa1vgy3A/kn1osRtY4kUDfUOZx5vsl7vJjziE3SmQAOcGpMLb+LgvBKLfG5itSaEeOH5QxBoPchH8XBHRRH1+Cn/hAAM+ziXAAH5jpxuRxGY/FZSuh0UnKi1vExrHgdfpU4w2yWMpTuipaXlAqeIc2Paab7aSW4B5/wK0eychNAOZaLWYAACBxMAAJAPYBAhQlAABVOPpYOAhAAagAEKkwAAqhw8rF0EIAAUAMgUGECEECFk4+lgwAEgBoAgQoTgAAqnHwsHQQgANQACFSYAARQ4eRj6SAAAaAGQKDCBCCACicfSwcBCAA1AAIVJgABVDj5WDoIQACoARCoMIH/BxI837X+BBStAAAAAElFTkSuQmCC'),
(685, 'ZVUE Media Manager', 'application/vnd.hand', '.zmm', ''),
(686, 'Zzazz Deck', 'application/vnd.zzaz', '.zaz', ''),
(687, 'Diretório', 'directory', NULL, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAgAElEQVR4Xu3dbYxmZ1kH8PucmdktIi/t7rTEpg0vNqUiQQnRaAyKxg++JBBrEwyJGBGLSkDRSGq7s7PTlqpIINQXEL6oGEMAIVHRLyYQEqIElZAogkVbBARmt628dndmzjFbaSzS0tk51zP3uZ77x1eec93X9bvupP/cbXa74n8ECBAgQIBAcwJdcxMbmAABAgQIECgCgEtAgAABAgQaFBAAGly6kQkQIECAgADgDhAgQIAAgQYFBIAGl25kAgQIECAgALgDBAgQIECgQQEBoMGlG5kAAQIECAgA7gABAgQIEGhQQABocOlGJkCAAAECAoA7QIAAAQIEGhQQABpcupEJECBAgIAA4A4QIECAAIEGBQSABpduZAIECBAgIAC4AwQIECBAoEEBAaDBpRuZAAECBAgIAO4AAQIECBBoUEAAaHDpRiZAgAABAgKAO0CAAAECBBoUEAAaXLqRCRAgQICAAOAOECBAgACBBgUEgAaXbmQCBAgQICAAuAMECBAgQKBBAQGgwaUbmQABAgQICADuAAECBAgQaFBAAGhw6UYmQIAAAQICgDtAgAABAgQaFBAAGly6kQkQIECAgADgDhAgQIAAgQYFBIAGl25kAgQIECAgALgDBAgQIECgQQEBoMGlG5kAAQIECAgA7gABAgQIEGhQQABocOlGJkCAAAECAoA7QIAAAQIEGhQQABpcupEJECBAgIAA4A4QIECAAIEGBQSABpduZAIECBAgIAC4AwQIECBAoEEBAaDBpRuZAAECBAgIAO4AAQIECBBoUEAAaHDpRiZAgAABAgKAO0CAAAECBBoUEAAaXLqRCRAgQICAAOAOECBAgACBBgUEgAaXbmQCBAgQICAAuAMECBAgQKBBAQGgwaUbmQABAgQICADuAAECBAgQaFBAAGhw6UYmQIAAAQICgDtAgAABAgQaFBAAGly6kQkQIECAgADgDhAgQIAAgQYFBIAGl25kAgQIECAgALgDBAgQIECgQQEBoMGlG5kAAQIECAgA7gABAgQIEGhQQABocOlGJkCAAAECAoA7QIAAAQIEGhQQABpcupEJECBAgIAA4A4QIECAAIEGBQSABpduZAIECBAgIAC4AwQIECBAoEEBAaDBpRuZAAECBAgIAO4AAQIECBBoUEAAaHDpRiZAgAABAgKAO0CAAAECBBoUEAAaXLqRCRAgQICAAOAOECBAgACBBgUEgAaXbmQCBAgQICAAuAMECBAgQKBBAQGgwaUbmQABAgQICADuAAECBAgQaFBAAGhw6UYmQIAAAQICgDtAgAABAgQaFBAAGly6kQkQIECAgADgDhAgQIAAgQYFBIAGl25kAgQIECAgALgDBAgQIECgQQEBoMGlG5kAAQIECAgA7gABAgQIEGhQQABocOlGJkCAAAECAoA7QIAAAQIEGhQQABpcupEJECBAgIAA4A4QIECAAIEGBQSABpduZAIECBAgIAC4AwQIECBAoEEBAaDBpRuZAAECBAgIAO4AAQIECBBoUEAAaHDpRiZAgAABAgKAO0CAAAECBBoUEAAaXLqRCRAgQICAAOAOECBAgACBBgUEgAaXbmQCBAgQICAAuAMECBAgQKBBAQGgwaUbmQABAgQICADuAAECBAgQaFBAAGhw6UYmQIAAAQICgDtAgAABAgQaFBAAGly6kQkQIECAgADgDhAgQIAAgQYFBIAGl25kAgQIECAgALgDBAgQIECgQQEBoMGlG5kAAQIECAgA7gABAgQIEGhQQABocOlGJkCAAAECAoA7QIAAAQIEGhQQABpcupEJECBAgIAA4A4QIECAAIEGBQSABpduZAIECBAgIAC4AwQIECBAoEEBAaDBpRuZAAECBAgsfQC47OR46VD2nt2N47eP3Xh1GburumFcL2X85rEvjymlX3MNCGQVGPvynNOba+/J2r++CRCoJ7CUAeDYyXPf1Zfup8ax/HBXytPq8TqZwMIF3ru9tfYDCz/FAQQILJ3A0gSAi185Pm7tyO71Y1d+tpRy9dJtykAEHkbAK4CrQYDAQQTSB4Bv2RyP7wy7ryhl+MVS+scdBME3BJILeAVIvkDtE6ghkDcAbI79pcPO9WMZby2lv7gGnjMJzEXAK8BcNqEPAnkEUgaAY5tnn9oP/Z+UUp6Vh1qnBBYq4BVgobyKE1g+gXQBYH1j94VD2fu9vvSPXr51mIjAwQW8AhzczpcEWhTIEwCuG1eOf9ve73bj+JIWF2VmAvsQ8AqwDyQ/IUDgfwVSBIAnbo4XfXFv98+6rjzP4ggQeHgBrwBuBwEC+xWYfQA4/w//Lw077y6le85+h/I7Ag0LeAVoePlGJ3AhAvMOAOef/a/ZfUdXynMvZCi/JdCygFeAlrdvdgL7F5h1AFg/ufumMo4/t/9x/JIAgVKKVwDXgACBRxSYbQBY3zj3olK6Nz/iBH5AgMDXCXgFcCkIEHgkgVkGgMtOnH36blf+vi/9ox5pAP8/AQIPKeAVwMUgQOAbCswvAFw3rlxy9e4HV/ryHXZHgMDBBbwCHNzOlwRaEJhdAFjfOPfyUrrXtYBvRgILFvAKsGBg5QlkFphVAFjfHJ9QhnMfK6V/TGZUvROYi4BXgLlsQh8E5icwqwBw6cmd14xjecX8mHREIK2AV4C0q9M4gcUKzCYAXH7DeOy+tXN3+TP+F7tw1dsT8ArQ3s5NTGA/ArMJAMc3dk52pWzup2m/IUDgggS8AlwQlx8TaENgJgFg7I7duPPv/Ur3xDbYTUngcAW8Ahyut9MIZBCYRQA4fmLn+7uuvCcDmB4JJBXwCpB0cdomsCiBWQSASzd23zCW8fpFDakuAQKleAVwCwgQeLDALALAsY3dO/oyPsVqCBBYqIBXgIXyKk4gl0D1AHDx5njl6rB7Vy423RLIKeAVIOfedE1gEQLVA8Dxjd0XdGV8yyKGU5MAga8T8ArgUhAgcL9A9QCwvrFzSynlRvsgQOBwBLwCHI6zUwjMXaB6ADi2sfP2vpRr5w6lPwJLJOAVYImWaRQCBxWoHgAu2dj50EopzzjoAL4jQODCBbwCXLiZLwgsm0D1ALC+ce4TpXRXLBuseQjMXMArwMwXpD0CixaoHwBuOntP6fvHL3pQ9QkQ+FoBrwBuBIG2BeYQAHZK36+2vQbTE6gi4BWgCrtDCcxDoH4A2NgZ50GhCwLtCXgFaG/nJibwgIAA4C4QaFvAK0Db+zd9wwICQMPLNzqB8wJeAdwDAm0KCABt7t3UBB4s4BXAfSDQoIAA0ODSjUzg/wt4BXAnCLQnIAC0t3MTE3goAa8A7gWBxgQEgMYWblwCDyfgFcDdINCWgADQ1r5NS+AbCXgFcD8INCQgADS0bKMSeCQBrwCPJOT/J7A8AgLA8uzSJAQiBLwCRCiqQSCBgACQYElaJHCYAl4BDlPbWQTqCQgA9eydTGCuAl4B5roZfREIFBAAAjGVIrAsAl4BlmWT5iDw8AICgNtBgMBDCXgFcC8ILLmAALDkCzYegYMKeAU4qJzvCOQQEABy7EmXBGoIeAWooe5MAockIAAcErRjCGQU8AqQcWt6JrA/AQFgf05+RaBVAa8ArW7e3EsvIAAs/YoNSGCagFeAaX6+JjBXAQFgrpvRF4H5CHgFmM8udEIgTEAACKNUiMDyCngFWN7dmqxdAQGg3d2bnMCFCHgFuBAtvyWQQEAASLAkLRKYg4BXgDlsQQ8E4gQEgDhLlQgsu4BXgGXfsPmaEhAAmlq3YQlME/AKMM3P1wTmJCAAzGkbeiEwfwGvAPPfkQ4J7EtAANgXkx8RIPCAgFcAd4HAcggIAMuxR1MQOEwBrwCHqe0sAgsSEAAWBKssgWUW8AqwzNs1WysCAkArmzYngVgBrwCxnqoROHQBAeDQyR1IYDkEvAIsxx5N0a6AANDu7k1OYKqAV4Cpgr4nUFFAAKiI72gCBAgQSCwwDMNQyn19399TSvnMUMqdfVc+0o3dh4bdlfefflX3X3OeTgCY83b0RoAAAQKZBT7adeWvujK+7bOnjvzd3AYRAOa2Ef0QIECAwPIJjOVjYylvHFZW33z3Zvf5OQwoAMxhC3ogQIAAgTYEhuHzpe9v3+lXf+feze7emkMLADX1nU2AAAECTQqMw97pvutPfO7mtTeW0o01EASAGurOJECAAAECpZShlPf33d4Lt09ddMdhgwgAhy3uPAIECBAg8CCBYRi+2PUrLzm9tfqnhwkjABymtrMIECBAgMDDCXTlddvd6q+WzW44DCQB4DCUnUGAAAECBPYn8M7tfvX5ZbM7t7+fH/xXAsDB7XxJgAABAgTCBcZx+JvH33vkeXfc3p0NL/6gggLAInXVJkCAAAECBxAYx/Ku0yur1y7yXwcIAAdYjE8IECBAgMDCBbry+u1Tay9f1DkCwKJk1SVAgAABApMFup/Z3lr9o8llHqKAALAIVTUJECBAgECAwDAMX+7K+MzTt1z00YByX1NCAIgWVY8AAQIECAQKdKV84HP96vdE//cAAkDgkpQiQIAAAQKLERhftr115PbI2gJApKZaBAgQIEBgIQLDPUd2jlz1qdu6M1HlBYAoSXUIECBAgMACBcau/PbpU2uvjDpCAIiSVIcAAQIECCxQYCjDl3b6I1d+frO7O+IYASBCUQ0CBAgQIHA4Ar+xvbV2W8RRAkCEohoECBAgQOAQBMbS/cfprZWnlNKNU48TAKYK+p4AAQIECBymwFievX3z2vumHikATBX0PQECBAgQOESBoXS3n9lafdnUIwWAqYK+J0CAAAEChygw7HV3nLl19aqpRwoAUwV9T4AAAQIEDllgb2/1yrtv7f5zyrECwBQ93xIgQIAAgSoC3fO3t1bfOuVoAWCKnm8JECBAgEANga785vaptRumHC0ATNHzLQECBAgQqCPwzu2ttZ+YcrQAMEXPtwQIECBAoILA3lA+dPcta9855WgBYIqebwkQIECAQA2BoXx6+5a1y6ccLQBM0fMtAQIECBCoIDAMw5fP3HL00VOOFgCm6PmWAAECBAhUERj2treOrk45WgCYoudbAgQIECBQSWB7a23SP8MnfRwx8/rGzuS/0CCiDzUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmAQEg07b0SoAAAQIEggQEgCBIZQgQIECAQCYBASDTtvRKgAABAgSCBASAIEhlCBAgQIBAJgEBINO29EqAAAECBIIEBIAgSGUIECBAgEAmgfwB4Kaze6Xv+0zoeiVAgAABAnUFhr3traOrU3ropnwc8e2xm85+pe/7iyJqqUGAAAECBFoQGIbhy2duOfroKbPWDwAb5z7Tl+6yKUP4lgABAgQINCUwlE9v37J2+ZSZZxAAdj7cl/L0KUP4lgABAgQINCbwT9tba8+cMnP1ALB+4uxflK7/8SlD+JYAAQIECDQlMJY/37557dopM1cPAMdP7ry6G8uvTRnCtwQIECBAoCmBrrxq+9TajVNmrh8ANnZf0JXxLVOG8C0BAgQIEGhLoHv+9tbqW6fMXD0AXLZ535OHYeXjU4bwLQECBAgQaElgr1+94u7N7pNTZq4eAM43f+lN5+4a++7KKYP4lgABAgQItCAw7HV3nLl19aqps84iABw/ufsH3Ti+ZOowvidAgAABAksv0JXXb59ae/nUOWcRAI5t7vxgP5S/nTqM7wkQIECAwNILjOXZ2zevvW/qnLMIAGVz7NeHnTtL6a6YOpDvCRAgQIDAsgoMe+OdZ25de3Ip3Th1xnkEgFLK8Y2dE10pW1MH8j0BAgQIEFhigZu2t9ZujZhvNgHgCTeM6ztr5+7qS/+oiMHUIECAAAECyyRw/s//v2jvyJWfuq07EzHXbALA+WHWT+68tozllyMGU4MAAQIECCyTwFjKq09vrf161EyzCgCXnRwvHfbO/Vvp+8dGDagOAQIECBBILzAM966tHrnq05vd6ahZZhUAzg91/MTOK7quvCZqQHUIECBAgEB2gXEcf+X0zUdeFznH7AJAuW5cWb969wOlL5P+lqNIJLUIECBAgEBFgX/Y/sjqd5e3dXuRPcwvAJRSnnDi7NN2xvLBvu8vihxWLQIECBAgkElgKMNXxr486+7No/8S3fcsA8D9/ypg49yLu9L9YfTA6hEgQIAAgSwCYxl//vTWkTctot/ZBoDzw166sfuGsYzXL2JwNQkQIECAwJwFutK98XNbqwv7Y/JnHQDK5ri6Ppx7Vyn9j815SXojQIAAAQKhAuPw7u2VI88tm91uaN0HFZt3ACilPHFzvOiLw85fdqX7oUUhqEuAAAECBGYk8N6jX1j9kU++tvvKInuafQA4P/z5EPCl4dzbvQQs8iqoTYAAAQL1BYa/PvqFI9cu+h/+5+dMEQDuX8h148rxa/Zu78r4C/UXpAMCBAgQIBArcP+/8+9XXrrIZ/8Hd5wnAHy16/WNcy8ayni7vzMg9uKpRoAAAQJ1BIZhuK/ru5ct6r/2f7ip0gWA84Mcu+nsNX3f/3Ep5Vl11uVUAgQIECAQIDCUf1zphp/+zM1H/zmg2gWVSBkAHvhXAuvX7Ly0DONm6fvHX9DUfkyAAAECBKoKDP89lu7U6Y+svT76T/jb71h5A8BXJ7z8hvHY2bXdV47D8Et933/Tfgf3OwIECBAgcNgC5/9K326l//0j3epvRf7FPgeZI30AeGDox26Olxwddq8fS/firoxPOgiGbwgQIECAwCIExjLe1ZXuTUd2Vt/wqdu6M4s440JrLk0A+L/Bx279xO73lb785LDb/Wi/Mn7rhaL4PQECBAgQmCowlO7jpZR3d+P4jtMrq+8rm90wtWbk90sYAL6W55IbxytWVva+t5TxGaWUp+4N5UkrpVw69MPF/VCOlr7vI0HVIkCAAIFWBIa9YShn+9LfW/ry2VLKnaWUfx3H7sN7Kyvvv2ez+8ScJZY+AMwZX28ECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEXyiBoAAAAGjSURBVN/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKKAAFAR39EECBAgQKCWgABQS965BAgQIECgooAAUBHf0QQIECBAoJaAAFBL3rkECBAgQKCigABQEd/RBAgQIECgloAAUEveuQQIECBAoKLA/wBhsjJqwncF4wAAAABJRU5ErkJggg==');
INSERT INTO `file_type` (`id_file_type`, `desc_file_type`, `mime_file_type`, `ext_file_type`, `ico_file_type`) VALUES
(688, 'MPEG Audio', 'audio/mpeg', '.mp3', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAdVElEQVR4Xu2df5AlVXXHz7nd783sLrAR5s2ulGVgwd8KqwLrz6j7h1hSqVgkQQGTaIAlxgIFFYg1P968WQtWUUCSCCsSLRPAUJJKSiyEEk3KAgF/sEFlgQVJFZCdeTMY3F/z3uu+J9Wzu7jgvL7d7/Z9r3v6+/7te88993PO/fZ5ffsHE34gAAKlJcClnTkmDgIgQBAAJAEIlJgABKDEwcfUQQACgBwAgRITgACUOPiYOghAAJADIFBiAhCAEgcfUwcBCAByAARKTAACUOLgY+ogAAFADoBAiQlAAEocfEwdBCAAyAEQKDEBCECJg4+pgwAEADkAAiUmAAEocfAxdRCAACAHQKDEBCAAJQ4+pg4CEADkAAiUmAAEoMTBx9RBAAKAHACBEhOAAJQ4+Jg6CEAAkAMgUGICEIASBx9TB4G+CUCtLoeRDk9jpo0ksl7rcB0rXk2kKghD7wSajUpsDGsT7Quajeq1vY+AnsuZgHMBGB1bOI7IvzSk8Gyl1MrlDHMQczMLQCsQ8v5kruHfPgj/MGa+CTgTgGPqMrxHgkkK9adJKT/fGIrrnVkAOqK13q18fmezXn2wuDOF5y4IOBGANfWFdR3t3eYRnejCadj8HYEkAnCg9dM69DfMf46fBj8QOEggcwFYM956Q8B8lyJeA8zuCaQQgMiZn5Py/6hZ593uPcMIRSCQqQBEZ/5Aq3uw+PsX+pQCQCT6O83t1Q/QrRz2z0uMlFcCmQlA9J//tzr4Mcr+/oY6tQAQkSa+dr7hX9hfTzFaHglkJgC1ic4VRHRpHie5nH3qRQD285ALsT24nDMj2dwyEYBoq0+It+NqfzLoWbbqXQB0iO3BLCNRTFsZCUCwVZScV0wExfa6dwEgwvZgsWOfhffWAhDd4aeD9gxu8skiHOlt2AjAgdGwPZge+7LpYS8AE8EHieSWZUOkYBPJQACiGWN7sGBxz8pdawEYnQyuF5FNWTkEO+kIZCQA2B5Mh33ZtLYXgInOfUJ0yrIhUrCJZCYA2B4sWOSzcddaAGoTrTkidVQ27sBKWgJZCsD+sbE9mDYGRW6fhQC08Ujv4FIgewHA9uDgotn/kTMQgI70322MeJBA9gKA7cEyZRcEoODRdiEAB5Bge7DguZHEfQhAEko5buNQAKJZY3swx7HPwjUIQBYUB2jDsQBge3CAse3H0BCAflB2OIZzAcD2oMPoDd40BGDwMbDyoB8CgO1BqxDlujMEINfhMTvXPwHA9qA5GsVrAQEoXsxe4HH/BADbgwVPlSXdhwAUPKr9FABsDxY8WZZwHwJQ8JgOQACwPVjwnDnUfQhAwYM5IAHA9mDB8+ag+xCAggdyYAKA7cGCZ85+9yEABQ/jIAUA24MFTx4IQPEDOHgBwPZgkbMIFUCRo0dEgxcAbA8WOYUgAEWOXk4EANuDxU0iCEBxY7foeR4qgEMQ4unBguUTBKBgAXuxuzkTAGwPFiyfIAAFC1juBQDbg4XKKAhAocL1+87mrgJ43kW8XLQIqQUBKEKUYnzMrwBge7AIqQUBKEKUCikA2B4sQmpBAIoQpYIKALYH859cEID8xyjWw/z+BXiB29gezGmeQQByGpikbhVEALA9mDSgfW4HAegz8KyHK4wAYHsw69BnYg8CkAnGwRkpkgDsp4TtwcFly++PDAHIUzR68KV4AoDtwR7C7KwLBMAZ2v4YLp4AYHuwP5mRbBQIQDJOuW1VRAHA9mB+0gkCkJ9Y9ORJgQUgmi+2B3uKenadIADZsRyIpYILALYHB5I1vxsUAjDgANgO31T+ENW53c3O6FirJUpVbcdx2V8TXzvf8C90OQZsL00AAlDwzKgov/ZMnee6TaM20dlORK/K/zSxPTiIGEEABkE9wzG1yCnz09UHugrAZPAPJPK3GQ7pyBS2Bx2BjTULARgE9QzHFJHz5qarN3T9CzDZOkGEHiRS1rHO0O0lTWmtdyuf39msVx90PRbs7ydgnRS1iY4A5uAIMMtNs1PVs+M8GJ0IrhOS8wfnZaqRn9ahv2H+c/x0ql5o3BMBCEBP2PLUSe8a2lVd89RVvK+bV8fUZXiPDu4gonflyfMYX7A92KdAQQD6BNrpMCIfbU5Xvx43xssukhXtVeE1osJzi/B3gER/p7m9+gG6lUOn7EpuHAKwHBJA6NGm57+O6hyYplMbb7+RWZ0vJBtZ6z/M8xYhtgdN0bQ/DgGwZ5gLCyL0qbnpypdy4QycKAwBCEBhQhXvqCa9ryJ08s7poV8ukylhGn0gAAHoA+Q+DvGY1/HfvvNybvZxTAxVYAIQgAIHr4vrP1fsv29mimeX39Qwo6wJQACyJpoDezrkHb4KT5+ZHnooB+7AhRwTgADkODg2rmmtFzylJmaVf03cw0I2Y6Bv8QlAAIofw9gZaOLHmfQWb2/lppkrec8yny6ml5IABCAlsKI216T3KFLfZZG7iXibeP6vVxH935N1ahExbucuamAt/YYAWAJEd7cETC88cTv68rcOAVj+MS70DCEAbsMHAXDLF9YtCUAALAEaukMA3PKFdUsCEABLgBAAtwBh3S0BCIBbvqgA3PKFdUsCEABLgKgA3AKEdbcEIABu+aICcMsX1i0JQAAsAaICcAsQ1t0SgAC45YsKwC1fWLckAAGwBIgKwC1AWHdLAALgli8qALd8Yd2SAATAEiAqALcAYd0tAQiAW76oANzyhXVLAhAAS4CoANwChHW3BCAAbvmiAnDLF9YtCUAALAGiAnALMIl1TWFTsfqBaLqXWT1COthRDYeeXbGbdu+4lltJbKANCLgggArABdVFm/o3RN5NSvQ3Z6Yr9+OtO85Aw7AFAQiABbylumqtdzKrL7Dnb23WeXfG5mEOBDIlAAHICqfWgXjqatrnN+Y+z7uyMgs7IOCSAAQgA7qLb95V+oy5evVnGZiDCRDoGwEIgCVqFvqPwPP/4tk6/9bSFLqDQN8JQABskDPf0GTvY0k+y20zDPqCgCsCEIBeyUaLf8rbhKv7vQJEvzwQgAD0EAUh+vc55f9ZmjP/Sy6V1ZVq+Mei6D0kciJrfawoOpxIVXpwAV1KQ0B3iOi3ROrXxPygaPo+tbzbs7rQDAFIn0iPhco/Kel//pGxhVeJ519GYfghpdRw+uHQAwReSEBrvVeRdzN5wRXNqeEdNnwgAGnoaR2Qzyc369UHTd2OrsvKQAebhfSFRMoztcdxEEhPQHeE1NXDu/zJp67ifen7E0EA0lHb0mxULjN1ic76ir3bhOm1prY4DgK2BDTRQ4rD03upBiAASelrekYt+K80fWF3ZLz9ZhJ9BytvJKlptAMBWwKLz5uIOrU5Xf15GlsQgIS0ROSiuenq1XHNozM/Ef0Iiz8hVDTLlMCiCGh6e3Pz8GNJDUMAEpBirZ/lherL487+az4tq/TK4AEiek0Ck2gCAk4IRH8HVuzyNyS9JgABSBAGTXztfMO/MK5pbaITVQefSGAOTUDAKQEm+uJso/LpJINAABJQ0iKnzE9Xo7P7kr+j6q1XK02/wNX+BDDRxD2BxQfT5HVzU8OPmgaDABgIaZKZ+UblpXF3/NUm298g4b80wcZxEOgXAc389fkp/6Om8SAABkKs5ebZzdWzujWL7vBTlfZO3ORjSjUc7ycBTXqfqOpa0w1rEABjVOSTzUb1mm7NRseDDwvLN41m0AAE+kxAhM+em/ZvihsWAmCqAIjeN9uofK/r//+x4EalxFhq9Tn2GA4EiDR/rbnZPxcCYJEMrMPjZzcPP97NRG2s81NS9CaLIdAVBFwReKDZqJwCAbDAW+34I09fzvNd/wKMteZFqSMthkBXEHBCQHQ4N7d5uAYBsMDbVP4Q1bndtQKYaLXxSK8FYHR1SEB3mo2hKgTAArHpwxS1iY5YmKcE9n9IRO+yGSOm7382G5V3x9k2zc/kv7XfdamubdFq8TvrQqb1zLRRi5ymSK2ytp3KgN7Fmr+jFf1AmLYNceWJZ6Ln9GNODovm61I9muiIlnTWsab1LLRRk5ymlDos1fA9NjbFBxcBDWBNAE0LxBQ3k/2ReufdrOkHJju9HI9eTjJXr0QC0/Vnmp/J/178MvWp1eUwCTtnaa0u9TxZZ2pvdVzoUSLZMrS7cnPS22tN40W3jYcrO2cJqUsVyXGm9jbHTfGBAORcACL3ahMdF1WA8ex/YOzYCseUYDbJa+xbl2otDC7WrOuK1JCxfYoG0Us3WPHYnKpcm+bNTymGINokldra4GKtdd3VfSSm+EAACiAALqqAJGf/3AvAgdiNjrdPDLS6LcNq4JFQ6dOfrQ/9KtWC7rHx6GTrhFC821xUAxCAHoNysJsJoKlENg1vsn+wf8ZVQKKzf1EEIPJz9LOyRqvgTlZ0gom54fhPqx3/1LidH0v7S3ZfMymjHQnu9IhOzNK+Kb9QARSgAohczLIKSHr2L5IAHBSBgMN7eq4EhLZXA/8d/V78B1MwEoFAwnuyrAQgAJZyagLYrwogw2sBic/+RROARREYb58YstyX9pqAJr2HNJ08v3noYcuUseq++HcgpPuyuiZgyl9UAAWpALKqAtKc/YsoAAd8vpSIrkizEpO88SmNPZu2o+OdS4Rpi42NpH9hIQAFEoAMqoBUZ/8sBMC2QmKt26LU/zDx3VrprYm+v1iX6pGd8OHEfwWEtje3+6+nWzk0LbqRyfZJKlSbQqH3eBy+XJSKv9GmF/83SeWoteHDWfwVQAVgimjBBMDmWkDas38eBOD3wqP5a0N7vAtMe/IjE+3zmHhrsvDzR5oN/xtxbaO9+2BV+PdK5CPJbHZpldT/8fa5zPxVq7GIjDeaoQIomABYVAGpz/65FAAiEqIfDe/y3xsnAtHNQlq3d5rvGNTPrVLVtU/WeaFbKhx43+NdRPRW2wUZ9U/i/6LgDC++Z8LqjkFUAJYRMwG0LXFN9pdyv5cqoJezf14FYJHJ4rcZ/fPiwlub6PwrEf15XBvW8i+zm6sfjmtz1GTwT9Zn/hcPkMT/yfa3SPgMmxQ25RcqgAJWAD1UAT2d/XMtAKSFhN8c9x780fH23wjzV+JDLOc2G9WvdWsTfeeBmX9iswiX7mv2f2SifT4TX2czNgTAhl6C/1CDqACiKaWpAno9++dbAIiE+bq5Kf9j3UI8Ot55qzDdE5cCwnLy3FS16wIfnQyuF5FNlmm0ZHeT/2sm22/RwvfajA0BsKGXYwFIUQX0fPbPuwAQ0SPNRuXVXQXgs7JG/GBnXAqY3vdQm+hEb9Z9hWUadese6390Y5CWYMZmbAiADb2cC0CSKsDm7J93AdCkW/ONoa5fXD7+Ahl67iVB14t70fxM73sYHWu1TFt9vaZYFv6bxoYAmAjl/BpAtMjjHtk1PCMQe/Y32S6DAFimh1V3kwBQXfyaDjo2g0AAbOjloAKIFnjcSzviqgDT2d9kO+8CQNENPNOVrp9iy6KEtkyf+O4G/4+uy0hHB00bHyAANvTyIQCSZCEv8dYg49k/etGIKUFMFzlt+1uFh/kfm1P+x7vZyOIimpV/ps4G/6O7Dlm46xepTOYX/+I0KrE7fdgGzPlfgAMLMNFiPnQqSUXDlCD5FQAtLPzG2enqtm4hzGIbLcki661NIv9T3M24tBem+EIAiiEAlHRBH5hOYsEwJUheBcC0hbb49yWDG2l6W9zmXgn9v5mEP2S21r2FKb4QgIIIABElXtRpxMKUIDkVgP9apfxTTbfvBivbM+ZbgW2WV899jf6/7CJZ0VrV3klKHdHzKPgLYINuf1/bBWLyII39hAs78rnrm35ffNEwzfhLzcW2v4nPC49rYfK2rlTeJ+MWf9RnJKOHadL5Z2qd3P/aePuvibnrHYqmkQ4eN8UHFUBxKoDIU2MVEDVKs21oSpBBVwDRVpkS9WtSfDdrvTXuP//zoaxL9Sgd/iqLx2mTLrRu7Xryf//LQn+ZxQ1IpvhCAIolAMZrAXHTWWrL0JQgtgJgu4B66V+b6KR+IUgv4ySpEHuxOzLR+RQTXdlL3xf3McUXAlAwATBVAXHTWeqmIVOCFE0Aen0lWK+LzcQvrd019dbrA033K1Ir0vbt5S8aBKB4AtBTFdDthiFTAhdJAKI3A1u9FLSHFWfil8bk2r+TWqcS3pvlXxeTfxCAAgpAL1VAt1uGTQlSFAGo1WVtqIM7sn6ttmkBm/iZ+h88Hi3+diW4K2v/Tf5BAIopAKmqgLjbhU0JUgQBqI233yisvs0kxyZdcFm1M/FLOk502+9CENzlKVqftE+Sdib/IAAFFYA0VUDcA0OmBMmzABx42u9i1rru6ok90yIz8TP1P/R4JAKtTniv8uT4NP3i2pr8gwAUVwASVQGmR4ZNCZJHARi5RA5XQ52zNatLBnHWPzRlTPzSLuS1463XdZgewEXAtOQctTcF2LRATG5Z2je+7MP0STHL8Y03SpnmH39c+Jg6De0h+gOmYB2Fsj5k3kik35/VArHzz3yjWC/2R8Y7FzPTF3vp++I+pviiAihwBdCPBDEJnCnBTP2zmEOebfT4XYPoPQDRh0mt30Rkig8EAAIQmwOmBWxKMFP/PC9eJ74l/C4AbgV2Qj+9UdcJ7tq+aca249v2N/m3HI8n+S4AHgbKSeRdJ7hr+yaMtuPb9jf5t2yPJ/suAB4HHnQCuE5w1/ZN/GzHt+1v8m/5Hk/wXYAMnmg0xQfXAHANANcABqQyppeC4JVgAwrMocOaFNT2Ipdr+yaEtuPb9jf5t8yPx34XAC8FzUH0XSe4a/smhLbj2/Y3+becj+O14AWIrusEd23fhNh2fNv+Jv9cH8eHQSwJ25bAlsM77+46wV3bNwGyHd+2v8k/18cryq89U+e5buM4/TSY4bsA0ROCYSWYtWFgig8uAuIiYKkvAmqRU+anq13fvT86EVwnJOfbLMKufU3fNRhvb9DMP7YZGwJgQy9nLwW1nMqS3U0JYqrwbPu7mFMamyJy3tx09YZufUbq7Tex5p+msZmsbYLvAoy3NzHz9cnsLd3KFB9UAKgASl0BEMstzanqmXFpUBsLbiAl59gsxBf3NW0BRu1rY+1bSPEHbcaFANjQQwVAy70CINK7hnZV1zx1Fe/rlirRbbkLhwd3MtE7LNPpYHfjdwGOrsvKVtCeUUodZjMmBMCGHgSgBAJARCLnNKerN8alSrQgO0H4ZbtKIMV3ASba5xBx178mSdMaApCUVJd2JoCmM6RpeNf2XY8/aP9N80t4/LGm8l9LdQ5M7aNrAiTqPBZ5D2t9rOlNRL1+F+CoWvirLN4MZIoPrgHgGkC5rwEciD8LfWZ2upLJu/hNImI6PjreuUSYtpjaJTkOAUhCKaaNCSAqgPjPT9vysQxf4u6a9D5faMPM9NBDiTs5aDg62TohDOk+pdRwFuZN+YsKABUAKoADOaBD3uH73ttnptjq5pteF+6aSRkNJLwH3wXolaCDfiYFtT3DubZvQmI7vm1/k3/9Ph5qerDi+af2WwQWP2qigjvwWvB+RxwVACqAF+WAJn7c4/D02amh/+5HOtbq7fVhR33b82Rd1uOZBBp/ASAAEIAlckBrveCxmpyd8a+irdzJemEu2qtLtRYGF2vWdUVqyMUYEABLqiaA+AuwPC4CdkuTqBpg0lu8vZWbZq7kPZbptNi9VpfDRHfO1qG6xMVZ/1AfTfmLCgAVACqABKtak96jSH1XSL7vMW0jqjxx+LP03I5ruRXX/Zi6DO9t02rygnXEsj4U3kii36+UWplgWOsmEABLhKbnxWsTrTaRqlgOg+4gkDmB6JsEs5uHYv9aoAIwYK92/JGnL+f5bs1Gx1rzotSRmUcPBkHAkoCmsDnfGB6NMwMBMEHm8BXNqeEd3ZrVJjo/IaI3m8zgOAj0mwAT3T/bqGyAAFiQZ6L3zTYq3+sqAA4eFbVwF11B4HkCrPmrs5v9TRAAq6SQTzYb1Wu6mRiZCM5mkn+2GgKdQcABARE+c27avwUCYAPX8MKII+tyBOv2zrx8rdZmqui7fAhorfcqv7qmWefdEACLuGqSmflG5aVELN3MHDUW3KiUfNRiGHQFgWwJJPj0WDQgLgImwG56ceTI5MIrOeRfklJ+AnNoAgKOCegOa3nN7Obhx00DQQBMhPbL5JebU5VPxDUdnehcKUSfSmIObUDAMYEtzUblsiRjQACSUCI9X1HVlz9T573dmu//nHNwPyl6fSKTaAQCDgiERNuOUP5bnqzzQhLzEIAklKI2Ip9oTle/HNe8NrlwvBa6R5FXS2oW7UAgKwLR9Spf6bfN1IefSGoTApCUFNHTaq//KtMDIdGjnTqg7ynFsXdgJR8WLUHATCBa/J7QqbPT1W3m1r9rAQFIQ4vo8maj8llTl9GxheM0ebexohNMbXEcBGwJLL7ExA//NM2Z/+CYEIBU9HWHmU5K8qKI6Cmw3RJMseiL8LBQKshonJiA7hCrL65+1q+bnkrsZhICkBj28w0fIeWfZLrB4mDrqBoQz79Mh+FZ/XoENP2U0KNIBKJHkz3t3cR+cEUvZ/1D5woB6CXyQrc1t/tn0K0cJu0evQSCdHgaM20kkfUhhcd6mleb3iuf1D7aLU8C0SO9oZLnmLwnFPM2EbqblHd70hOQiQoEwESoy3Eh/spcw/t43B2CPZpGNxDoGwEIgAXqRRF42LsgTSVgMRy6gkDmBCAA9kj/TRb8v5r7PO+yNwULINBfAhCALHgLPUqefLBZrz6YhTnYAIF+EYAAZEVa60A89SVmfzqrCzRZuQY7INCNAAQg89zQ/yvEX/D2Vraa7hrMfGgYBIGUBCAAKYElbc5aPyued4si/c0ZrtxPddZJ+6IdCPSLAASgD6Sjt7Mq8n4oJPcyq0eEgh1+e+g3w0O068k6tbCV2IcgYIglCUAAkBi5JmD6sEWunS+AcxCAAgSpzC5CANxGHwLgli+sWxKAAFgCNHSHALjlC+uWBCAAlgAhAG4BwrpbAhAAt3xRAbjlC+uWBCAAlgBRAbgFCOtuCUAA3PJFBeCWL6xbEoAAWAJEBeAWIKy7JQABcMsXFYBbvrBuSQACYAkQFYBbgLDulgAEwC1fVABu+cK6JQEIgCVAVABuAcK6WwIQALd8UQG45QvrlgQgAJYAUQG4BQjrbglAANzyRQXgli+sWxKAAFgCdF8BtNr49JXbIJXXuu40G0PV8s7f/cytK4CRsYUmK2/EvasYoWwERIdzc5uH8al1h4G3FoDRic59QnSKQx9huqQEmOj+2UZlQ0mn35dp2wvAZHC9iGzqi7cYpFQEWPNXZzf7yC2HUbcWgNp4cAaxfMuhjzBdUgIifObctH9LSaffl2nbC0BdDtNBewafvu5LvEoziCa9jxeqa/DJNbchtxaAyL3RsWCrKDnPrauwXioCzDc2p/xzSjXnAUw2IwFYOE6It5NS/gDmgCGXGwGtAyJ5bXPz8GPLbWp5m08mAhBNamSys4WFLsnbBOFP8QgI05VzU5XPFM/z4nmcmQC87CJZsbAq+DErOqF4GOBxXghooodW7PI3PHUV78uLT8vZj8wEYP+1gIXjQlL3KMWjyxka5uaGQHTjDxO9DaW/G75LWc1UAKIBavX2eq31nYo83MHVvzgWfqRo8ZOvTp2rV39W+MkUaAKZC8CiCEwuHK/Fu00RvaFALODqoAho+gVReDrO/P0PgBMBiKaxeE3g8GCKtb4IuwP9D2whRtQ6EE9dXWV/8pk67y2Ez8vMSWcCcJBTbWzhFeT5l2kJz1SkViwzfphODwSim3wUezdTGFyBs34PADPs4lwADvo6cokcTkPhaUpoIyk5UetwHStejUeJM4xmLk3pjmh5TinvCdK8TTPdTS3vdtzhl49g9U0A8jFdeAECIHAoAQgA8gEESkwAAlDi4GPqIAABQA6AQIkJQABKHHxMHQQgAMgBECgxAQhAiYOPqYMABAA5AAIlJgABKHHwMXUQgAAgB0CgxAQgACUOPqYOAhAA5AAIlJgABKDEwcfUQQACgBwAgRITgACUOPiYOghAAJADIFBiAv8PVG1Y4iXaNnwAAAAASUVORK5CYII=');

-- --------------------------------------------------------

--
-- Estrutura para tabela `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `group_owner` int(11) NOT NULL,
  `group_default` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `group`
--

INSERT INTO `group` (`group_id`, `group_name`, `empresa_id`, `group_owner`, `group_default`) VALUES
(1, 'Financeiro', 2, 17, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `group_permission`
--

CREATE TABLE IF NOT EXISTS `group_permission` (
  `permission_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `readable` tinyint(1) DEFAULT '0',
  `listable` tinyint(1) DEFAULT '0',
  `writable` tinyint(1) DEFAULT '0',
  `deletable` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `group_permission`
--

INSERT INTO `group_permission` (`permission_id`, `group_id`, `object_id`, `readable`, `listable`, `writable`, `deletable`) VALUES
(1, 1, 50, 0, 0, 0, 0),
(2, 1, 51, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `group_users`
--

CREATE TABLE IF NOT EXISTS `group_users` (
  `group_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_user_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `info_labels`
--

CREATE TABLE IF NOT EXISTS `info_labels` (
  `label_id` int(11) NOT NULL,
  `label_description` varchar(45) NOT NULL,
  `label_content` text NOT NULL,
  `object` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `log_history`
--

CREATE TABLE IF NOT EXISTS `log_history` (
  `log_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `operation` varchar(50) NOT NULL,
  `ip_address` varchar(25) NOT NULL,
  `browser` varchar(50) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=642 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `log_history`
--

INSERT INTO `log_history` (`log_id`, `user_name`, `operation`, `ip_address`, `browser`, `client_id`, `datetime`) VALUES
(1, '0', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:34:04'),
(2, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:35:16'),
(3, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:36:29'),
(4, 'vagnerleitte', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:36:42'),
(5, 'vagnerleitte', 'NEW_COMPANY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:37:01'),
(6, 'vagnerleitte', 'NEW_ADDRESS_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:37:43'),
(7, 'vagnerleitte', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:37:56'),
(8, 'vagnerleitte', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:37:56'),
(9, 'vagnerleitte', 'NEW_INVOICE_SUCCESS', '172.31.21.92', 'Chrome', 1, '2015-11-10 15:38:03'),
(10, 'vagnerleitte', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 1, '2015-11-10 15:38:04'),
(11, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:44:34'),
(12, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-10 15:46:09'),
(13, '0', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 19:27:26'),
(14, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 19:29:54'),
(15, 'maicon', 'NEW_COMPANY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 19:30:22'),
(16, 'maicon', 'NEW_ADDRESS_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 19:37:10'),
(17, 'maicon', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 19:37:46'),
(18, 'maicon', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 19:37:46'),
(19, 'maicon', 'NEW_INVOICE_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:39:13'),
(20, 'maicon', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:39:14'),
(21, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:43:27'),
(22, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:46:23'),
(23, 'maicon', 'NEW_MESSAGE_READED', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:49:57'),
(24, 'maicon', 'NEW_GROUP_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:54:43'),
(25, 'maicon', 'UPDATE_GROUP_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:54:55'),
(26, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-10 19:56:04'),
(27, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:09:04'),
(28, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:12:49'),
(29, 'maicon', 'UPDATE_PROFILE_PIC_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:41:27'),
(30, 'maicon', 'NEW_MESSAGE_READED', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:43:44'),
(31, 'maicon', 'CHAT_MESSAGE_SENT', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:43:57'),
(32, 'maicon', 'NEW_MESSAGE_READED', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:43:58'),
(33, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-10 22:44:13'),
(34, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 22:44:26'),
(35, 'admin', 'UPDATE_PROFILE_PIC_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:36:03'),
(36, 'admin', 'NEW_S3_SERVER_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:42:03'),
(37, 'admin', 'ADD_STORAGE_VALUE', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:43:56'),
(38, 'admin', 'NEW_PLAN_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:46:19'),
(39, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:46:48'),
(40, '0', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:48:52'),
(41, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:51:31'),
(42, 'daniel', 'NEW_COMPANY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:52:32'),
(43, 'daniel', 'NEW_ADDRESS_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:54:34'),
(44, 'daniel', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:55:17'),
(45, 'daniel', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:55:17'),
(46, 'daniel', 'NEW_INVOICE_SUCCESS', '172.31.21.92', 'Chrome', 3, '2015-11-10 23:56:45'),
(47, 'daniel', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 3, '2015-11-10 23:56:46'),
(48, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-10 23:58:00'),
(49, 'daniel', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 3, '2015-11-11 00:00:34'),
(50, 'admin', 'NEW_SERVICE_ERROR', '172.31.21.92', 'Chrome', 0, '2015-11-11 00:06:58'),
(51, 'admin', 'UPDATE_PLAN_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-11 00:07:53'),
(52, 'daniel', 'LOG OUT', '172.31.21.92', 'Chrome', 3, '2015-11-11 00:08:53'),
(53, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 3, '2015-11-11 00:09:36'),
(54, 'admin', 'NEW_SEGMENT_DIRECTORY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-11 00:11:54'),
(55, 'admin', 'UPDATE_SEGMENT_ERROR', '172.31.21.92', 'Chrome', 0, '2015-11-11 00:13:01'),
(56, 'admin', 'UPDATE_SEGMENT_ERROR', '172.31.21.92', 'Chrome', 0, '2015-11-11 00:13:18'),
(57, 'admin', 'NEW_SEGMENT_DIRECTORY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-11 00:14:13'),
(58, 'daniel', 'LOG OUT', '172.31.21.92', 'Chrome', 3, '2015-11-11 00:15:21'),
(59, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 3, '2015-11-11 00:16:14'),
(60, 'daniel', 'LOG OUT', '172.31.21.92', 'Chrome', 3, '2015-11-11 00:24:53'),
(61, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 00:52:58'),
(62, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 00:57:57'),
(63, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-11 01:03:13'),
(64, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-11 01:08:43'),
(65, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 01:13:11'),
(66, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-11 02:49:04'),
(67, 'admin', 'UPDATE_CONTRATO', '172.31.21.92', 'Chrome', 0, '2015-11-11 03:15:53'),
(68, 'admin', 'NEW_SEGMENT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-11 03:19:33'),
(69, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-11 04:43:02'),
(70, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 04:43:28'),
(71, 'maicon', 'ADD_ADITIONAL_SPACE', '172.31.21.92', 'Chrome', 2, '2015-11-11 04:44:19'),
(72, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 04:55:19'),
(73, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-11 04:55:40'),
(74, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-11 05:08:59'),
(75, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 05:09:15'),
(76, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 05:09:59'),
(77, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-11 05:10:08'),
(78, 'admin', 'NEW_SEGMENT_DIRECTORY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-11 05:10:59'),
(79, 'admin', 'NEW_SEGMENT_DIRECTORY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-11 05:57:24'),
(80, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-11 06:00:09'),
(81, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:00:56'),
(82, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:01:44'),
(83, 'maicon', 'NEW_USER_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:04:10'),
(84, 'maicon', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:04:11'),
(85, 'maicon', 'USER_LINKED_COMPANY_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:04:11'),
(86, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:04:28'),
(87, 'pereira', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:06:01'),
(88, 'pereira', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:06:26'),
(89, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:06:46'),
(90, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:07:24'),
(91, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:07:56'),
(92, 'pereira', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:08:07'),
(93, 'pereira', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-11 06:08:23'),
(94, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-11 18:18:00'),
(95, 'maicon', 'LOG IN', '172.31.21.92', 'Safari', 2, '2015-11-11 20:18:54'),
(96, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-12 00:23:33'),
(97, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-12 02:09:09'),
(98, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-12 03:32:06'),
(99, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-13 22:26:41'),
(100, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-13 22:59:07'),
(101, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-13 23:23:19'),
(102, 'pereira', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-13 23:28:39'),
(103, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-15 04:12:57'),
(104, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-15 04:13:41'),
(105, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-15 04:14:14'),
(106, 'pereira', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-15 04:14:28'),
(107, 'pereira', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-15 04:16:03'),
(108, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-16 01:37:39'),
(109, 'maicon', 'SHARE_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-16 01:43:09'),
(110, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-16 01:44:42'),
(111, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-17 11:54:17'),
(112, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-17 13:11:48'),
(113, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-17 15:50:01'),
(114, 'maicon', 'NEW_MESSAGE_READED', '172.31.21.92', 'Chrome', 2, '2015-11-17 16:15:05'),
(115, 'maicon', 'NEW_MESSAGE_READED', '172.31.21.92', 'Chrome', 2, '2015-11-17 16:15:14'),
(116, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-17 16:41:27'),
(117, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-17 16:51:32'),
(118, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-17 21:03:39'),
(119, 'maicon', 'NEW_MESSAGE_READED', '172.31.21.92', 'Chrome', 2, '2015-11-17 21:19:35'),
(120, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-18 09:32:04'),
(121, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-18 21:37:03'),
(122, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-19 15:05:53'),
(123, '0', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 13:52:54'),
(124, 'danismgomes', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-23 13:54:08'),
(125, 'danismgomes', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:08:47'),
(126, 'danismgomes', 'NEW_COMPANY_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:10:23'),
(127, 'danismgomes', 'NEW_ADDRESS_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:11:45'),
(128, 'danismgomes', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:12:12'),
(129, 'danismgomes', 'NEW_CONTACT_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:12:12'),
(130, 'danismgomes', 'NEW_INVOICE_SUCCESS', '172.31.21.92', 'Chrome', 4, '2015-11-23 14:12:21'),
(131, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 4, '2015-11-23 14:12:22'),
(132, 'danismgomes', 'LOG OUT', '172.31.21.92', 'Chrome', 4, '2015-11-23 14:12:50'),
(134, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:52'),
(136, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:53'),
(138, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:53'),
(140, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:54'),
(142, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:54'),
(144, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:55'),
(146, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:55'),
(148, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:55'),
(150, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:56'),
(152, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:56'),
(154, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:56'),
(156, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:57'),
(158, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:57'),
(160, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:58'),
(162, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:58'),
(164, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:58'),
(166, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:59'),
(168, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:59'),
(170, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:15:59'),
(172, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:00'),
(174, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:36'),
(176, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:37'),
(178, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:37'),
(181, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:37'),
(182, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:38'),
(184, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:38'),
(186, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:38'),
(188, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:39'),
(190, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:39'),
(192, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:39'),
(194, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:40'),
(196, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:40'),
(198, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:41'),
(200, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:41'),
(202, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:41'),
(204, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:42'),
(206, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:42'),
(208, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:43'),
(210, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:43'),
(212, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:43'),
(214, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:44'),
(216, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:44'),
(218, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:44'),
(220, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:45'),
(222, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:45'),
(224, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:51'),
(226, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:51'),
(228, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:51'),
(230, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:52'),
(232, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:52'),
(234, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:52'),
(236, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:53'),
(238, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:53'),
(240, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:53'),
(242, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:54'),
(244, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:54'),
(246, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:55'),
(248, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:55'),
(250, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:55'),
(252, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:56'),
(254, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:56'),
(256, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:56'),
(258, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:57'),
(260, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:57'),
(262, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:57'),
(264, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:16:58'),
(266, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:35'),
(268, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:35'),
(270, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:35'),
(272, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:36'),
(275, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:36'),
(276, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:36'),
(278, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:37'),
(280, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:37'),
(282, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:38'),
(284, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:38'),
(286, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:38'),
(288, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:39'),
(290, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:39'),
(292, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:40'),
(294, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:40'),
(296, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:40'),
(298, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:41'),
(300, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:41'),
(302, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:41'),
(304, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:42'),
(306, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:42'),
(308, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:42'),
(310, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:43'),
(312, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:43'),
(314, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:43'),
(316, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:44'),
(318, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:50'),
(320, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:50'),
(322, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:50'),
(324, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:51'),
(326, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:51'),
(328, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:52'),
(330, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:52'),
(332, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:52'),
(334, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:53'),
(336, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:53'),
(338, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:54'),
(340, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:54'),
(342, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:54'),
(344, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:55'),
(346, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:55'),
(348, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:56'),
(350, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:56'),
(352, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:56'),
(354, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:57'),
(356, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:18:57'),
(358, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:12'),
(360, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:13'),
(362, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:13'),
(364, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:13'),
(366, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:14'),
(368, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:14'),
(370, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:14'),
(372, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:15'),
(374, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:15'),
(376, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:15'),
(378, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:16'),
(380, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:16'),
(382, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:16'),
(384, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:17'),
(386, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:17'),
(388, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:17'),
(390, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:18'),
(392, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:18'),
(394, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:19'),
(396, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:19'),
(398, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:19'),
(400, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:20'),
(402, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:20'),
(404, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:20'),
(406, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:21'),
(408, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:21'),
(410, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:21'),
(412, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:22'),
(414, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:22'),
(416, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:22'),
(418, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:23'),
(420, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:23'),
(422, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:23'),
(424, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:24'),
(426, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:24'),
(428, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:24'),
(430, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:25'),
(432, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:25'),
(434, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:25'),
(436, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:26'),
(438, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:26'),
(440, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:31'),
(442, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:32'),
(444, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:32'),
(446, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:32'),
(448, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:33'),
(450, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:33'),
(452, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:33'),
(454, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:34'),
(456, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:34'),
(458, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:34'),
(460, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:35'),
(462, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:35'),
(464, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:35'),
(466, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:35'),
(468, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:36'),
(470, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:36'),
(472, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:36'),
(474, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:37'),
(476, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:37'),
(478, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:37'),
(480, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:38'),
(482, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:53'),
(484, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:53'),
(486, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:54'),
(488, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:54'),
(490, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:54'),
(492, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:55'),
(494, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:55'),
(496, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:55'),
(498, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:56'),
(500, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:56'),
(502, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:56'),
(504, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:57'),
(506, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:57'),
(508, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:57'),
(510, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:58'),
(512, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:58'),
(514, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:58'),
(516, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:59'),
(518, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:59'),
(520, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:19:59'),
(522, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:00'),
(524, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:01'),
(526, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:01'),
(528, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:01'),
(530, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:02'),
(532, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:02'),
(534, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:02'),
(536, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:03'),
(538, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:03'),
(540, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:03'),
(542, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:04'),
(544, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:04'),
(546, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:04'),
(548, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:05'),
(550, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:05'),
(552, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:07'),
(554, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:08'),
(556, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:08'),
(558, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:11'),
(560, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:11'),
(562, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:21:11'),
(564, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:12'),
(566, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:12'),
(568, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:12'),
(570, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:13'),
(572, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:13'),
(574, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:13'),
(576, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:13'),
(578, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:14'),
(580, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:14'),
(582, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:14'),
(584, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:15'),
(586, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:15'),
(588, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:16'),
(590, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:16'),
(592, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:16'),
(594, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:17'),
(596, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:17'),
(598, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:17'),
(600, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:18'),
(602, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:18'),
(604, 'danismgomes', 'NEW_USER_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 0, '2015-11-23 14:22:18'),
(605, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-26 18:30:31'),
(606, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-26 18:38:06'),
(607, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-28 12:42:58'),
(608, 'maicon', 'GROUP_FOLDER_PERMISSIONS_CREATE_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-28 12:49:50'),
(609, 'maicon', 'GROUP_FOLDER_PERMISSIONS_CREATE_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-28 12:50:08'),
(610, 'maicon', 'SHARE_EMAIL_SUCCESS', '172.31.21.92', 'Chrome', 2, '2015-11-28 12:57:05'),
(611, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-28 12:57:59'),
(612, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-11-29 23:49:28'),
(613, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-11-29 23:51:03'),
(614, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-29 23:51:15'),
(615, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-11-29 23:51:37'),
(616, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-29 23:51:51'),
(617, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-29 23:52:02'),
(618, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-11-29 23:58:15'),
(619, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-30 16:39:31'),
(620, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-11-30 20:04:50'),
(621, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-12-01 12:45:28'),
(622, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-12-01 18:34:21'),
(623, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-12-01 19:17:33'),
(624, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 3, '2015-12-01 19:17:44'),
(625, 'daniel', 'LOG OUT', '172.31.21.92', 'Chrome', 3, '2015-12-01 19:28:14'),
(626, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-12-01 19:28:20'),
(627, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-12-01 19:38:21'),
(628, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 3, '2015-12-01 19:38:29'),
(629, 'daniel', 'LOG OUT', '172.31.21.92', 'Chrome', 3, '2015-12-01 20:35:21'),
(630, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-12-01 20:35:25'),
(631, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-12-01 20:35:27'),
(632, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 3, '2015-12-01 20:35:33'),
(633, 'daniel', 'LOG OUT', '172.31.21.92', 'Chrome', 3, '2015-12-01 20:36:41'),
(634, 'admin', 'LOG IN', '172.31.21.92', 'Chrome', 0, '2015-12-01 20:36:47'),
(635, 'admin', 'LOG OUT', '172.31.21.92', 'Chrome', 0, '2015-12-01 20:42:15'),
(636, 'daniel', 'LOG IN', '172.31.21.92', 'Chrome', 3, '2015-12-01 20:42:25'),
(637, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-12-02 13:30:08'),
(638, 'maicon', 'UPDATE_QUOTA', '172.31.21.92', 'Chrome', 2, '2015-12-02 13:30:44'),
(639, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-12-02 13:30:58'),
(640, 'maicon', 'LOG IN', '172.31.21.92', 'Chrome', 2, '2015-12-02 13:31:08'),
(641, 'maicon', 'LOG OUT', '172.31.21.92', 'Chrome', 2, '2015-12-02 13:32:36');

-- --------------------------------------------------------

--
-- Estrutura para tabela `mensagens`
--

CREATE TABLE IF NOT EXISTS `mensagens` (
  `id` int(11) NOT NULL,
  `from_user` int(11) DEFAULT NULL,
  `to_user` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `time_viewed` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `mensagens`
--

INSERT INTO `mensagens` (`id`, `from_user`, `to_user`, `content`, `sent`, `viewed`, `time_viewed`) VALUES
(1, 17, 15, 'olá', '2015-11-10 20:43:57', 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `modulo`
--

CREATE TABLE IF NOT EXISTS `modulo` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(45) NOT NULL,
  `module_system` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `modulo`
--

INSERT INTO `modulo` (`module_id`, `module_name`, `module_system`) VALUES
(1, 'admin', 0),
(2, 'usuario', 0),
(3, 'usuario_tipo', 0),
(4, 'usuario_status', 0),
(5, 'grupo', 0),
(6, 'servico', 0),
(7, 'servico_status', 0),
(8, 'plano', 0),
(9, 'plano_status', 0),
(10, 'segmento', 0),
(11, 'segmento_status', 0),
(12, 'servidor', 0),
(13, 'modulo', 0),
(14, 'acao', 0),
(15, 'permissao', 0),
(16, 'empresa', 0),
(17, 'faturamento', 0),
(18, 'arquivos', 0),
(19, 'cliente', 0),
(20, 'configuracao', 0),
(21, 'contato_tipo', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `permissao`
--

CREATE TABLE IF NOT EXISTS `permissao` (
  `permissao_id` int(11) NOT NULL,
  `permissao_module_id` int(11) NOT NULL,
  `permissao_action_id` int(11) NOT NULL,
  `permissao_type_id` int(11) NOT NULL,
  `permissao_allow` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7061 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `permissao`
--

INSERT INTO `permissao` (`permissao_id`, `permissao_module_id`, `permissao_action_id`, `permissao_type_id`, `permissao_allow`) VALUES
(4469, 1, 1, 1, 1),
(4470, 1, 2, 1, 1),
(4525, 6, 1, 1, 1),
(4526, 6, 2, 1, 1),
(4527, 6, 3, 1, 1),
(4528, 6, 4, 1, 1),
(4529, 6, 5, 1, 1),
(4530, 6, 72, 1, 1),
(4531, 6, 11, 1, 1),
(4532, 6, 12, 1, 0),
(4533, 6, 13, 1, 0),
(4534, 6, 14, 1, 0),
(4535, 6, 15, 1, 0),
(4536, 6, 6, 1, 1),
(4537, 6, 7, 1, 0),
(4538, 6, 8, 1, 0),
(4539, 6, 9, 1, 0),
(4540, 6, 10, 1, 0),
(4541, 6, 16, 1, 1),
(4542, 6, 17, 1, 1),
(4543, 6, 18, 1, 1),
(4544, 6, 19, 1, 1),
(4545, 6, 20, 1, 1),
(4546, 6, 26, 1, 1),
(4547, 6, 27, 1, 1),
(4548, 6, 28, 1, 1),
(4549, 6, 29, 1, 1),
(4550, 6, 30, 1, 1),
(4551, 7, 1, 1, 1),
(4552, 7, 2, 1, 1),
(4553, 7, 3, 1, 1),
(4554, 7, 4, 1, 1),
(4555, 7, 5, 1, 1),
(4556, 7, 72, 1, 1),
(4557, 7, 11, 1, 1),
(4558, 7, 12, 1, 0),
(4559, 7, 13, 1, 0),
(4560, 7, 14, 1, 0),
(4561, 7, 15, 1, 0),
(4562, 7, 6, 1, 1),
(4563, 7, 7, 1, 0),
(4564, 7, 8, 1, 0),
(4565, 7, 9, 1, 0),
(4566, 7, 10, 1, 0),
(4567, 7, 16, 1, 1),
(4568, 7, 17, 1, 1),
(4569, 7, 18, 1, 1),
(4570, 7, 19, 1, 1),
(4571, 7, 20, 1, 1),
(4572, 7, 26, 1, 1),
(4573, 7, 27, 1, 1),
(4574, 7, 28, 1, 1),
(4575, 7, 29, 1, 1),
(4576, 7, 30, 1, 1),
(4577, 7, 21, 1, 1),
(4578, 7, 22, 1, 1),
(4579, 7, 23, 1, 1),
(4580, 7, 24, 1, 1),
(4581, 7, 25, 1, 1),
(4582, 8, 1, 1, 1),
(4583, 8, 2, 1, 1),
(4584, 8, 3, 1, 1),
(4585, 8, 4, 1, 1),
(4586, 8, 5, 1, 1),
(4587, 8, 72, 1, 1),
(4588, 8, 11, 1, 1),
(4589, 8, 12, 1, 0),
(4590, 8, 13, 1, 0),
(4591, 8, 14, 1, 0),
(4592, 8, 15, 1, 0),
(4593, 8, 6, 1, 1),
(4594, 8, 7, 1, 0),
(4595, 8, 8, 1, 0),
(4596, 8, 9, 1, 0),
(4597, 8, 10, 1, 0),
(4598, 8, 16, 1, 1),
(4599, 8, 17, 1, 1),
(4600, 8, 18, 1, 1),
(4601, 8, 19, 1, 1),
(4602, 8, 20, 1, 1),
(4603, 8, 26, 1, 1),
(4604, 8, 27, 1, 1),
(4605, 8, 28, 1, 1),
(4606, 8, 29, 1, 1),
(4607, 8, 30, 1, 1),
(4608, 8, 21, 1, 1),
(4609, 8, 22, 1, 1),
(4610, 8, 23, 1, 1),
(4611, 8, 24, 1, 1),
(4612, 8, 25, 1, 1),
(4613, 8, 36, 1, 1),
(4614, 8, 37, 1, 1),
(4615, 8, 38, 1, 1),
(4616, 8, 39, 1, 1),
(4617, 8, 40, 1, 1),
(4756, 14, 1, 1, 1),
(4757, 14, 2, 1, 1),
(4758, 14, 3, 1, 1),
(4759, 14, 4, 1, 1),
(4760, 14, 5, 1, 1),
(4761, 14, 72, 1, 1),
(4762, 14, 11, 1, 1),
(4763, 14, 12, 1, 0),
(4764, 14, 13, 1, 0),
(4765, 14, 14, 1, 0),
(4766, 14, 15, 1, 0),
(4767, 14, 6, 1, 1),
(4768, 14, 7, 1, 0),
(4769, 14, 8, 1, 0),
(4770, 14, 9, 1, 0),
(4771, 14, 10, 1, 0),
(4772, 14, 16, 1, 1),
(4773, 14, 17, 1, 1),
(4774, 14, 18, 1, 1),
(4775, 14, 19, 1, 1),
(4776, 14, 20, 1, 1),
(4777, 14, 26, 1, 1),
(4778, 14, 27, 1, 1),
(4779, 14, 28, 1, 1),
(4780, 14, 29, 1, 1),
(4781, 14, 30, 1, 1),
(4782, 14, 21, 1, 1),
(4783, 14, 22, 1, 1),
(4784, 14, 23, 1, 1),
(4785, 14, 24, 1, 1),
(4786, 14, 25, 1, 1),
(4787, 14, 36, 1, 1),
(4788, 14, 37, 1, 1),
(4789, 14, 38, 1, 1),
(4790, 14, 39, 1, 1),
(4791, 14, 40, 1, 1),
(4792, 14, 31, 1, 1),
(4793, 14, 32, 1, 1),
(4794, 14, 33, 1, 1),
(4795, 14, 34, 1, 0),
(4796, 14, 35, 1, 0),
(4797, 14, 47, 1, 1),
(4798, 14, 48, 1, 1),
(4799, 14, 49, 1, 1),
(4800, 14, 50, 1, 1),
(4801, 14, 51, 1, 1),
(4802, 14, 41, 1, 1),
(4803, 14, 42, 1, 1),
(4804, 14, 43, 1, 1),
(4805, 14, 45, 1, 0),
(4806, 14, 46, 1, 0),
(4807, 14, 62, 1, 1),
(4808, 14, 63, 1, 1),
(4809, 14, 64, 1, 1),
(4810, 14, 65, 1, 1),
(4811, 14, 66, 1, 1),
(4812, 14, 73, 1, 1),
(5109, 3, 1, 2, 1),
(5110, 3, 2, 2, 1),
(5111, 3, 3, 2, 1),
(5112, 3, 4, 2, 1),
(5113, 3, 5, 2, 1),
(5114, 3, 72, 2, 1),
(5115, 3, 11, 2, 1),
(5116, 3, 12, 2, 1),
(5117, 3, 13, 2, 1),
(5118, 3, 14, 2, 1),
(5119, 3, 15, 2, 1),
(5157, 6, 1, 2, 1),
(5158, 6, 2, 2, 1),
(5159, 6, 3, 2, 1),
(5160, 6, 4, 2, 1),
(5161, 6, 5, 2, 1),
(5162, 6, 72, 2, 1),
(5163, 6, 11, 2, 1),
(5164, 6, 12, 2, 1),
(5165, 6, 13, 2, 1),
(5166, 6, 14, 2, 1),
(5167, 6, 15, 2, 1),
(5168, 6, 6, 2, 1),
(5169, 6, 7, 2, 1),
(5170, 6, 8, 2, 1),
(5171, 6, 9, 2, 1),
(5172, 6, 10, 2, 1),
(5173, 6, 16, 2, 1),
(5174, 6, 17, 2, 1),
(5175, 6, 18, 2, 1),
(5176, 6, 19, 2, 1),
(5177, 6, 20, 2, 1),
(5178, 6, 26, 2, 1),
(5179, 6, 27, 2, 1),
(5180, 6, 28, 2, 1),
(5181, 6, 29, 2, 1),
(5182, 6, 30, 2, 1),
(5183, 7, 1, 2, 1),
(5184, 7, 2, 2, 1),
(5185, 7, 3, 2, 1),
(5186, 7, 4, 2, 1),
(5187, 7, 5, 2, 1),
(5188, 7, 72, 2, 1),
(5189, 7, 11, 2, 1),
(5190, 7, 12, 2, 1),
(5191, 7, 13, 2, 1),
(5192, 7, 14, 2, 1),
(5193, 7, 15, 2, 1),
(5194, 7, 6, 2, 1),
(5195, 7, 7, 2, 1),
(5196, 7, 8, 2, 1),
(5197, 7, 9, 2, 1),
(5198, 7, 10, 2, 1),
(5199, 7, 16, 2, 1),
(5200, 7, 17, 2, 1),
(5201, 7, 18, 2, 1),
(5202, 7, 19, 2, 1),
(5203, 7, 20, 2, 1),
(5204, 7, 26, 2, 1),
(5205, 7, 27, 2, 1),
(5206, 7, 28, 2, 1),
(5207, 7, 29, 2, 1),
(5208, 7, 30, 2, 1),
(5209, 7, 21, 2, 1),
(5210, 7, 22, 2, 1),
(5211, 7, 23, 2, 1),
(5212, 7, 24, 2, 1),
(5213, 7, 25, 2, 1),
(5214, 8, 1, 2, 1),
(5215, 8, 2, 2, 1),
(5216, 8, 3, 2, 1),
(5217, 8, 4, 2, 1),
(5218, 8, 5, 2, 1),
(5219, 8, 72, 2, 1),
(5220, 8, 11, 2, 1),
(5221, 8, 12, 2, 1),
(5222, 8, 13, 2, 1),
(5223, 8, 14, 2, 1),
(5224, 8, 15, 2, 1),
(5225, 8, 6, 2, 1),
(5226, 8, 7, 2, 1),
(5227, 8, 8, 2, 1),
(5228, 8, 9, 2, 1),
(5229, 8, 10, 2, 1),
(5230, 8, 16, 2, 1),
(5231, 8, 17, 2, 1),
(5232, 8, 18, 2, 1),
(5233, 8, 19, 2, 1),
(5234, 8, 20, 2, 1),
(5235, 8, 26, 2, 1),
(5236, 8, 27, 2, 1),
(5237, 8, 28, 2, 1),
(5238, 8, 29, 2, 1),
(5239, 8, 30, 2, 1),
(5240, 8, 21, 2, 1),
(5241, 8, 22, 2, 1),
(5242, 8, 23, 2, 1),
(5243, 8, 24, 2, 1),
(5244, 8, 25, 2, 1),
(5245, 8, 36, 2, 1),
(5246, 8, 37, 2, 1),
(5247, 8, 38, 2, 1),
(5248, 8, 39, 2, 1),
(5249, 8, 40, 2, 1),
(5250, 9, 1, 2, 1),
(5251, 9, 2, 2, 1),
(5252, 9, 3, 2, 1),
(5253, 9, 4, 2, 1),
(5254, 9, 5, 2, 1),
(5255, 9, 72, 2, 1),
(5256, 9, 11, 2, 1),
(5257, 9, 12, 2, 1),
(5258, 9, 13, 2, 1),
(5259, 9, 14, 2, 1),
(5260, 9, 15, 2, 1),
(5261, 9, 6, 2, 1),
(5262, 9, 7, 2, 1),
(5263, 9, 8, 2, 1),
(5264, 9, 9, 2, 1),
(5265, 9, 10, 2, 1),
(5266, 9, 16, 2, 1),
(5267, 9, 17, 2, 1),
(5268, 9, 18, 2, 1),
(5269, 9, 19, 2, 1),
(5270, 9, 20, 2, 1),
(5271, 9, 26, 2, 1),
(5272, 9, 27, 2, 1),
(5273, 9, 28, 2, 1),
(5274, 9, 29, 2, 1),
(5275, 9, 30, 2, 1),
(5276, 9, 21, 2, 1),
(5277, 9, 22, 2, 1),
(5278, 9, 23, 2, 1),
(5279, 9, 24, 2, 1),
(5280, 9, 25, 2, 1),
(5281, 9, 36, 2, 1),
(5282, 9, 37, 2, 1),
(5283, 9, 38, 2, 1),
(5284, 9, 39, 2, 1),
(5285, 9, 40, 2, 1),
(5286, 9, 31, 2, 1),
(5287, 9, 32, 2, 1),
(5288, 9, 33, 2, 1),
(5289, 9, 34, 2, 1),
(5290, 9, 35, 2, 1),
(5388, 12, 1, 2, 1),
(5389, 12, 2, 2, 1),
(5390, 12, 3, 2, 1),
(5391, 12, 4, 2, 1),
(5392, 12, 5, 2, 1),
(5393, 12, 72, 2, 1),
(5394, 12, 11, 2, 1),
(5395, 12, 12, 2, 1),
(5396, 12, 13, 2, 1),
(5397, 12, 14, 2, 1),
(5398, 12, 15, 2, 1),
(5399, 12, 6, 2, 1),
(5400, 12, 7, 2, 1),
(5401, 12, 8, 2, 1),
(5402, 12, 9, 2, 1),
(5403, 12, 10, 2, 1),
(5404, 12, 16, 2, 1),
(5405, 12, 17, 2, 1),
(5406, 12, 18, 2, 1),
(5407, 12, 19, 2, 1),
(5408, 12, 20, 2, 1),
(5409, 12, 26, 2, 1),
(5410, 12, 27, 2, 1),
(5411, 12, 28, 2, 1),
(5412, 12, 29, 2, 1),
(5413, 12, 30, 2, 1),
(5414, 12, 21, 2, 1),
(5415, 12, 22, 2, 1),
(5416, 12, 23, 2, 1),
(5417, 12, 24, 2, 1),
(5418, 12, 25, 2, 1),
(5419, 12, 36, 2, 1),
(5420, 12, 37, 2, 1),
(5421, 12, 38, 2, 1),
(5422, 12, 39, 2, 1),
(5423, 12, 40, 2, 1),
(5424, 12, 31, 2, 1),
(5425, 12, 32, 2, 1),
(5426, 12, 33, 2, 1),
(5427, 12, 34, 2, 1),
(5428, 12, 35, 2, 1),
(5429, 12, 47, 2, 1),
(5430, 12, 48, 2, 1),
(5431, 12, 49, 2, 1),
(5432, 12, 50, 2, 1),
(5433, 12, 51, 2, 1),
(5434, 12, 41, 2, 1),
(5435, 12, 42, 2, 1),
(5436, 12, 43, 2, 1),
(5437, 12, 45, 2, 1),
(5438, 12, 46, 2, 1),
(5439, 12, 52, 2, 1),
(5440, 12, 53, 2, 1),
(5441, 12, 54, 2, 1),
(5442, 12, 55, 2, 1),
(5443, 12, 56, 2, 1),
(5444, 13, 1, 2, 1),
(5445, 13, 2, 2, 1),
(5446, 13, 3, 2, 1),
(5447, 13, 4, 2, 1),
(5448, 13, 5, 2, 1),
(5449, 13, 72, 2, 1),
(5450, 13, 11, 2, 1),
(5451, 13, 12, 2, 1),
(5452, 13, 13, 2, 1),
(5453, 13, 14, 2, 1),
(5454, 13, 15, 2, 1),
(5455, 13, 6, 2, 1),
(5456, 13, 7, 2, 1),
(5457, 13, 8, 2, 1),
(5458, 13, 9, 2, 1),
(5459, 13, 10, 2, 1),
(5460, 13, 16, 2, 1),
(5461, 13, 17, 2, 1),
(5462, 13, 18, 2, 1),
(5463, 13, 19, 2, 1),
(5464, 13, 20, 2, 1),
(5465, 13, 26, 2, 1),
(5466, 13, 27, 2, 1),
(5467, 13, 28, 2, 1),
(5468, 13, 29, 2, 1),
(5469, 13, 30, 2, 1),
(5470, 13, 21, 2, 1),
(5471, 13, 22, 2, 1),
(5472, 13, 23, 2, 1),
(5473, 13, 24, 2, 1),
(5474, 13, 25, 2, 1),
(5475, 13, 36, 2, 1),
(5476, 13, 37, 2, 1),
(5477, 13, 38, 2, 1),
(5478, 13, 39, 2, 1),
(5479, 13, 40, 2, 1),
(5480, 13, 31, 2, 1),
(5481, 13, 32, 2, 1),
(5482, 13, 33, 2, 1),
(5483, 13, 34, 2, 1),
(5484, 13, 35, 2, 1),
(5485, 13, 47, 2, 1),
(5486, 13, 48, 2, 1),
(5487, 13, 49, 2, 1),
(5488, 13, 50, 2, 1),
(5489, 13, 51, 2, 1),
(5490, 13, 41, 2, 1),
(5491, 13, 42, 2, 1),
(5492, 13, 43, 2, 1),
(5493, 13, 45, 2, 1),
(5494, 13, 46, 2, 1),
(5495, 13, 52, 2, 1),
(5496, 13, 53, 2, 1),
(5497, 13, 54, 2, 1),
(5498, 13, 55, 2, 1),
(5499, 13, 56, 2, 1),
(5500, 13, 57, 2, 0),
(5501, 13, 58, 2, 0),
(5502, 13, 59, 2, 0),
(5503, 13, 60, 2, 0),
(5504, 13, 61, 2, 0),
(5505, 13, 74, 2, 0),
(5506, 14, 1, 2, 1),
(5507, 14, 2, 2, 1),
(5508, 14, 3, 2, 1),
(5509, 14, 4, 2, 1),
(5510, 14, 5, 2, 1),
(5511, 14, 72, 2, 1),
(5512, 14, 11, 2, 1),
(5513, 14, 12, 2, 1),
(5514, 14, 13, 2, 1),
(5515, 14, 14, 2, 1),
(5516, 14, 15, 2, 1),
(5517, 14, 6, 2, 1),
(5518, 14, 7, 2, 1),
(5519, 14, 8, 2, 1),
(5520, 14, 9, 2, 1),
(5521, 14, 10, 2, 1),
(5522, 14, 16, 2, 1),
(5523, 14, 17, 2, 1),
(5524, 14, 18, 2, 1),
(5525, 14, 19, 2, 1),
(5526, 14, 20, 2, 1),
(5527, 14, 26, 2, 1),
(5528, 14, 27, 2, 1),
(5529, 14, 28, 2, 1),
(5530, 14, 29, 2, 1),
(5531, 14, 30, 2, 1),
(5532, 14, 21, 2, 1),
(5533, 14, 22, 2, 1),
(5534, 14, 23, 2, 1),
(5535, 14, 24, 2, 1),
(5536, 14, 25, 2, 1),
(5537, 14, 36, 2, 1),
(5538, 14, 37, 2, 1),
(5539, 14, 38, 2, 1),
(5540, 14, 39, 2, 1),
(5541, 14, 40, 2, 1),
(5542, 14, 31, 2, 1),
(5543, 14, 32, 2, 1),
(5544, 14, 33, 2, 1),
(5545, 14, 34, 2, 1),
(5546, 14, 35, 2, 1),
(5547, 14, 47, 2, 1),
(5548, 14, 48, 2, 1),
(5549, 14, 49, 2, 1),
(5550, 14, 50, 2, 1),
(5551, 14, 51, 2, 1),
(5552, 14, 41, 2, 1),
(5553, 14, 42, 2, 1),
(5554, 14, 43, 2, 1),
(5555, 14, 45, 2, 1),
(5556, 14, 46, 2, 1),
(5557, 14, 52, 2, 1),
(5558, 14, 53, 2, 1),
(5559, 14, 54, 2, 1),
(5560, 14, 55, 2, 1),
(5561, 14, 56, 2, 1),
(5562, 14, 57, 2, 0),
(5563, 14, 58, 2, 0),
(5564, 14, 59, 2, 0),
(5565, 14, 60, 2, 0),
(5566, 14, 61, 2, 0),
(5567, 14, 74, 2, 0),
(5568, 14, 62, 2, 0),
(5569, 14, 63, 2, 0),
(5570, 14, 64, 2, 0),
(5571, 14, 65, 2, 0),
(5572, 14, 66, 2, 0),
(5573, 14, 73, 2, 0),
(5725, 17, 1, 2, 1),
(5726, 17, 2, 2, 1),
(5727, 17, 3, 2, 1),
(5728, 17, 4, 2, 1),
(5729, 17, 5, 2, 1),
(5730, 17, 72, 2, 1),
(5731, 17, 11, 2, 1),
(5732, 17, 12, 2, 1),
(5733, 17, 13, 2, 1),
(5734, 17, 14, 2, 1),
(5735, 17, 15, 2, 1),
(5736, 17, 6, 2, 1),
(5737, 17, 7, 2, 1),
(5738, 17, 8, 2, 1),
(5739, 17, 9, 2, 1),
(5740, 17, 10, 2, 1),
(5741, 17, 16, 2, 1),
(5742, 17, 17, 2, 1),
(5743, 17, 18, 2, 1),
(5744, 17, 19, 2, 1),
(5745, 17, 20, 2, 1),
(5746, 17, 26, 2, 1),
(5747, 17, 27, 2, 1),
(5748, 17, 28, 2, 1),
(5749, 17, 29, 2, 1),
(5750, 17, 30, 2, 1),
(5751, 17, 21, 2, 1),
(5752, 17, 22, 2, 1),
(5753, 17, 23, 2, 1),
(5754, 17, 24, 2, 1),
(5755, 17, 25, 2, 1),
(5756, 17, 36, 2, 1),
(5757, 17, 37, 2, 1),
(5758, 17, 38, 2, 1),
(5759, 17, 39, 2, 1),
(5760, 17, 40, 2, 1),
(5761, 17, 31, 2, 1),
(5762, 17, 32, 2, 1),
(5763, 17, 33, 2, 1),
(5764, 17, 34, 2, 1),
(5765, 17, 35, 2, 1),
(5766, 17, 47, 2, 1),
(5767, 17, 48, 2, 1),
(5768, 17, 49, 2, 1),
(5769, 17, 50, 2, 1),
(5770, 17, 51, 2, 1),
(5771, 17, 41, 2, 1),
(5772, 17, 42, 2, 1),
(5773, 17, 43, 2, 1),
(5774, 17, 45, 2, 1),
(5775, 17, 46, 2, 1),
(5776, 17, 52, 2, 1),
(5777, 17, 53, 2, 1),
(5778, 17, 54, 2, 1),
(5779, 17, 55, 2, 1),
(5780, 17, 56, 2, 1),
(5781, 17, 57, 2, 0),
(5782, 17, 58, 2, 0),
(5783, 17, 59, 2, 0),
(5784, 17, 60, 2, 0),
(5785, 17, 61, 2, 0),
(5786, 17, 74, 2, 0),
(5787, 17, 62, 2, 0),
(5788, 17, 63, 2, 0),
(5789, 17, 64, 2, 0),
(5790, 17, 65, 2, 0),
(5791, 17, 66, 2, 0),
(5792, 17, 73, 2, 0),
(5793, 17, 67, 2, 1),
(5794, 17, 68, 2, 1),
(5795, 17, 69, 2, 1),
(5796, 17, 70, 2, 1),
(5797, 17, 71, 2, 1),
(5798, 17, 78, 2, 1),
(5799, 17, 79, 2, 1),
(5800, 17, 80, 2, 1),
(5801, 17, 81, 2, 1),
(5802, 17, 82, 2, 1),
(5803, 17, 83, 2, 1),
(5804, 17, 84, 2, 0),
(5805, 17, 85, 2, 0),
(5806, 17, 86, 2, 1),
(5807, 17, 87, 2, 1),
(5892, 1, 1, 3, 1),
(5893, 1, 2, 3, 1),
(5900, 3, 1, 3, 1),
(5901, 3, 2, 3, 1),
(5902, 3, 3, 3, 1),
(5903, 3, 4, 3, 1),
(5904, 3, 5, 3, 1),
(5905, 3, 72, 3, 1),
(5906, 3, 11, 3, 0),
(5907, 3, 12, 3, 0),
(5908, 3, 13, 3, 0),
(5909, 3, 14, 3, 0),
(5910, 3, 15, 3, 0),
(5911, 4, 1, 3, 1),
(5912, 4, 2, 3, 1),
(5913, 4, 3, 3, 1),
(5914, 4, 4, 3, 1),
(5915, 4, 5, 3, 1),
(5916, 4, 72, 3, 1),
(5917, 4, 11, 3, 0),
(5918, 4, 12, 3, 0),
(5919, 4, 13, 3, 0),
(5920, 4, 14, 3, 0),
(5921, 4, 15, 3, 0),
(5922, 4, 6, 3, 0),
(5923, 4, 7, 3, 0),
(5924, 4, 8, 3, 0),
(5925, 4, 9, 3, 0),
(5926, 4, 10, 3, 0),
(5948, 6, 1, 3, 1),
(5949, 6, 2, 3, 1),
(5950, 6, 3, 3, 1),
(5951, 6, 4, 3, 1),
(5952, 6, 5, 3, 1),
(5953, 6, 72, 3, 1),
(5954, 6, 11, 3, 0),
(5955, 6, 12, 3, 0),
(5956, 6, 13, 3, 0),
(5957, 6, 14, 3, 0),
(5958, 6, 15, 3, 0),
(5959, 6, 6, 3, 0),
(5960, 6, 7, 3, 0),
(5961, 6, 8, 3, 0),
(5962, 6, 9, 3, 0),
(5963, 6, 10, 3, 0),
(5964, 6, 16, 3, 0),
(5965, 6, 17, 3, 0),
(5966, 6, 18, 3, 0),
(5967, 6, 19, 3, 0),
(5968, 6, 20, 3, 0),
(5969, 6, 26, 3, 0),
(5970, 6, 27, 3, 0),
(5971, 6, 28, 3, 0),
(5972, 6, 29, 3, 0),
(5973, 6, 30, 3, 0),
(5974, 7, 1, 3, 1),
(5975, 7, 2, 3, 1),
(5976, 7, 3, 3, 1),
(5977, 7, 4, 3, 1),
(5978, 7, 5, 3, 1),
(5979, 7, 72, 3, 1),
(5980, 7, 11, 3, 0),
(5981, 7, 12, 3, 0),
(5982, 7, 13, 3, 0),
(5983, 7, 14, 3, 0),
(5984, 7, 15, 3, 0),
(5985, 7, 6, 3, 0),
(5986, 7, 7, 3, 0),
(5987, 7, 8, 3, 0),
(5988, 7, 9, 3, 0),
(5989, 7, 10, 3, 0),
(5990, 7, 16, 3, 0),
(5991, 7, 17, 3, 0),
(5992, 7, 18, 3, 0),
(5993, 7, 19, 3, 0),
(5994, 7, 20, 3, 0),
(5995, 7, 26, 3, 0),
(5996, 7, 27, 3, 0),
(5997, 7, 28, 3, 0),
(5998, 7, 29, 3, 0),
(5999, 7, 30, 3, 0),
(6000, 7, 21, 3, 0),
(6001, 7, 22, 3, 0),
(6002, 7, 23, 3, 0),
(6003, 7, 24, 3, 0),
(6004, 7, 25, 3, 0),
(6005, 8, 1, 3, 1),
(6006, 8, 2, 3, 1),
(6007, 8, 3, 3, 1),
(6008, 8, 4, 3, 1),
(6009, 8, 5, 3, 1),
(6010, 8, 72, 3, 1),
(6011, 8, 11, 3, 0),
(6012, 8, 12, 3, 0),
(6013, 8, 13, 3, 0),
(6014, 8, 14, 3, 0),
(6015, 8, 15, 3, 0),
(6016, 8, 6, 3, 0),
(6017, 8, 7, 3, 0),
(6018, 8, 8, 3, 0),
(6019, 8, 9, 3, 0),
(6020, 8, 10, 3, 0),
(6021, 8, 16, 3, 0),
(6022, 8, 17, 3, 0),
(6023, 8, 18, 3, 0),
(6024, 8, 19, 3, 0),
(6025, 8, 20, 3, 0),
(6026, 8, 26, 3, 0),
(6027, 8, 27, 3, 0),
(6028, 8, 28, 3, 0),
(6029, 8, 29, 3, 0),
(6030, 8, 30, 3, 0),
(6031, 8, 21, 3, 0),
(6032, 8, 22, 3, 0),
(6033, 8, 23, 3, 0),
(6034, 8, 24, 3, 0),
(6035, 8, 25, 3, 0),
(6036, 8, 36, 3, 0),
(6037, 8, 37, 3, 0),
(6038, 8, 38, 3, 0),
(6039, 8, 39, 3, 0),
(6040, 8, 40, 3, 0),
(6041, 9, 1, 3, 1),
(6042, 9, 2, 3, 1),
(6043, 9, 3, 3, 1),
(6044, 9, 4, 3, 1),
(6045, 9, 5, 3, 1),
(6046, 9, 72, 3, 1),
(6047, 9, 11, 3, 0),
(6048, 9, 12, 3, 0),
(6049, 9, 13, 3, 0),
(6050, 9, 14, 3, 0),
(6051, 9, 15, 3, 0),
(6052, 9, 6, 3, 0),
(6053, 9, 7, 3, 0),
(6054, 9, 8, 3, 0),
(6055, 9, 9, 3, 0),
(6056, 9, 10, 3, 0),
(6057, 9, 16, 3, 0),
(6058, 9, 17, 3, 0),
(6059, 9, 18, 3, 0),
(6060, 9, 19, 3, 0),
(6061, 9, 20, 3, 0),
(6062, 9, 26, 3, 0),
(6063, 9, 27, 3, 0),
(6064, 9, 28, 3, 0),
(6065, 9, 29, 3, 0),
(6066, 9, 30, 3, 0),
(6067, 9, 21, 3, 0),
(6068, 9, 22, 3, 0),
(6069, 9, 23, 3, 0),
(6070, 9, 24, 3, 0),
(6071, 9, 25, 3, 0),
(6072, 9, 36, 3, 0),
(6073, 9, 37, 3, 0),
(6074, 9, 38, 3, 0),
(6075, 9, 39, 3, 0),
(6076, 9, 40, 3, 0),
(6077, 9, 31, 3, 0),
(6078, 9, 32, 3, 0),
(6079, 9, 33, 3, 0),
(6080, 9, 34, 3, 0),
(6081, 9, 35, 3, 0),
(6082, 15, 67, 1, 1),
(6083, 15, 68, 1, 1),
(6084, 15, 69, 1, 1),
(6085, 15, 70, 1, 1),
(6086, 15, 71, 1, 1),
(6094, 4, 1, 2, 1),
(6095, 4, 2, 2, 1),
(6096, 4, 89, 2, 1),
(6097, 4, 90, 2, 1),
(6098, 4, 6, 2, 0),
(6099, 4, 7, 2, 0),
(6100, 4, 8, 2, 0),
(6101, 4, 9, 2, 0),
(6102, 4, 10, 2, 0),
(6117, 1, 1, 2, 1),
(6118, 1, 2, 2, 1),
(6119, 1, 89, 2, 1),
(6120, 1, 90, 2, 1),
(6121, 1, 91, 2, 1),
(6122, 1, 1, 5, 1),
(6123, 1, 2, 5, 1),
(6124, 1, 89, 5, 1),
(6125, 1, 90, 5, 1),
(6126, 1, 91, 5, 1),
(6133, 16, 78, 2, 1),
(6134, 16, 79, 2, 1),
(6135, 16, 80, 2, 1),
(6136, 16, 81, 2, 1),
(6137, 16, 82, 2, 1),
(6138, 16, 92, 2, 1),
(6139, 1, 1, 4, 1),
(6140, 1, 2, 4, 1),
(6141, 1, 89, 4, 0),
(6142, 1, 90, 4, 0),
(6143, 1, 91, 4, 0),
(6156, 17, 83, 1, 1),
(6157, 17, 84, 1, 1),
(6158, 17, 85, 1, 1),
(6159, 17, 86, 1, 1),
(6160, 17, 87, 1, 1),
(6161, 17, 94, 1, 1),
(6162, 17, 95, 1, 1),
(6163, 9, 31, 1, 1),
(6164, 9, 32, 1, 1),
(6165, 9, 33, 1, 1),
(6166, 9, 34, 1, 1),
(6167, 9, 35, 1, 1),
(6168, 11, 31, 1, 1),
(6169, 11, 32, 1, 1),
(6170, 11, 33, 1, 1),
(6171, 11, 34, 1, 1),
(6172, 11, 35, 1, 1),
(6173, 11, 41, 1, 1),
(6174, 11, 42, 1, 1),
(6175, 11, 43, 1, 1),
(6176, 11, 45, 1, 1),
(6177, 11, 46, 1, 1),
(6178, 16, 31, 1, 1),
(6179, 16, 32, 1, 1),
(6180, 16, 33, 1, 1),
(6181, 16, 34, 1, 1),
(6182, 16, 35, 1, 1),
(6183, 16, 41, 1, 1),
(6184, 16, 42, 1, 1),
(6185, 16, 43, 1, 1),
(6186, 16, 45, 1, 1),
(6187, 16, 46, 1, 1),
(6188, 16, 78, 1, 1),
(6189, 16, 79, 1, 1),
(6190, 16, 80, 1, 1),
(6191, 16, 81, 1, 1),
(6192, 16, 82, 1, 1),
(6193, 16, 92, 1, 1),
(6194, 3, 31, 1, 1),
(6195, 3, 32, 1, 1),
(6196, 3, 33, 1, 1),
(6197, 3, 34, 1, 1),
(6198, 3, 35, 1, 1),
(6199, 3, 41, 1, 1),
(6200, 3, 42, 1, 1),
(6201, 3, 43, 1, 1),
(6202, 3, 45, 1, 1),
(6203, 3, 46, 1, 1),
(6204, 3, 78, 1, 1),
(6205, 3, 79, 1, 1),
(6206, 3, 80, 1, 1),
(6207, 3, 81, 1, 1),
(6208, 3, 82, 1, 1),
(6209, 3, 92, 1, 1),
(6210, 3, 11, 1, 1),
(6211, 3, 12, 1, 1),
(6212, 3, 13, 1, 1),
(6213, 3, 14, 1, 1),
(6214, 3, 15, 1, 1),
(6215, 4, 31, 1, 1),
(6216, 4, 32, 1, 1),
(6217, 4, 33, 1, 1),
(6218, 4, 34, 1, 1),
(6219, 4, 35, 1, 1),
(6220, 4, 41, 1, 1),
(6221, 4, 42, 1, 1),
(6222, 4, 43, 1, 1),
(6223, 4, 45, 1, 1),
(6224, 4, 46, 1, 1),
(6225, 4, 78, 1, 1),
(6226, 4, 79, 1, 1),
(6227, 4, 80, 1, 1),
(6228, 4, 81, 1, 1),
(6229, 4, 82, 1, 1),
(6230, 4, 92, 1, 1),
(6231, 4, 11, 1, 1),
(6232, 4, 12, 1, 1),
(6233, 4, 13, 1, 1),
(6234, 4, 14, 1, 1),
(6235, 4, 15, 1, 1),
(6236, 4, 6, 1, 1),
(6237, 4, 7, 1, 1),
(6238, 4, 8, 1, 1),
(6239, 4, 9, 1, 1),
(6240, 4, 10, 1, 1),
(6241, 10, 47, 2, 0),
(6242, 10, 48, 2, 0),
(6243, 10, 49, 2, 0),
(6244, 10, 50, 2, 0),
(6245, 10, 51, 2, 0),
(6246, 10, 93, 2, 0),
(6247, 11, 47, 2, 0),
(6248, 11, 48, 2, 0),
(6249, 11, 49, 2, 0),
(6250, 11, 50, 2, 0),
(6251, 11, 51, 2, 0),
(6252, 11, 93, 2, 0),
(6253, 11, 41, 2, 0),
(6254, 11, 42, 2, 0),
(6255, 11, 43, 2, 0),
(6256, 11, 45, 2, 0),
(6257, 11, 46, 2, 0),
(6258, 15, 47, 2, 0),
(6259, 15, 48, 2, 0),
(6260, 15, 49, 2, 0),
(6261, 15, 50, 2, 0),
(6262, 15, 51, 2, 0),
(6263, 15, 93, 2, 0),
(6264, 15, 41, 2, 0),
(6265, 15, 42, 2, 0),
(6266, 15, 43, 2, 0),
(6267, 15, 45, 2, 0),
(6268, 15, 46, 2, 0),
(6269, 15, 67, 2, 0),
(6270, 15, 68, 2, 0),
(6271, 15, 69, 2, 0),
(6272, 15, 70, 2, 0),
(6273, 15, 71, 2, 0),
(6274, 17, 83, 3, 1),
(6275, 17, 84, 3, 0),
(6276, 17, 85, 3, 0),
(6277, 17, 86, 3, 1),
(6278, 17, 87, 3, 1),
(6279, 17, 94, 3, 0),
(6280, 17, 95, 3, 0),
(6304, 5, 16, 1, 0),
(6305, 5, 17, 1, 0),
(6306, 5, 18, 1, 0),
(6307, 5, 19, 1, 0),
(6308, 5, 20, 1, 0),
(6353, 19, 98, 1, 1),
(6354, 19, 99, 1, 1),
(6359, 19, 98, 4, 1),
(6360, 19, 99, 4, 1),
(6402, 5, 16, 2, 1),
(6403, 5, 17, 2, 1),
(6404, 5, 18, 2, 1),
(6405, 5, 19, 2, 1),
(6406, 5, 20, 2, 1),
(6407, 5, 96, 2, 1),
(6408, 5, 97, 2, 1),
(6409, 5, 108, 2, 1),
(6410, 5, 109, 2, 1),
(6411, 5, 110, 2, 1),
(6412, 5, 16, 3, 1),
(6413, 5, 17, 3, 1),
(6414, 5, 18, 3, 1),
(6415, 5, 19, 3, 1),
(6416, 5, 20, 3, 1),
(6417, 5, 96, 3, 1),
(6418, 5, 97, 3, 1),
(6419, 5, 108, 3, 1),
(6420, 5, 109, 3, 1),
(6421, 5, 110, 3, 1),
(6422, 5, 16, 4, 1),
(6423, 5, 17, 4, 1),
(6424, 5, 18, 4, 1),
(6425, 5, 19, 4, 1),
(6426, 5, 20, 4, 1),
(6427, 5, 96, 4, 1),
(6428, 5, 97, 4, 1),
(6429, 5, 108, 4, 1),
(6430, 5, 109, 4, 1),
(6431, 5, 110, 4, 1),
(6445, 1, 1, 6, 1),
(6446, 1, 2, 6, 1),
(6447, 1, 89, 6, 0),
(6448, 1, 90, 6, 0),
(6449, 1, 91, 6, 0),
(6509, 20, 101, 6, 0),
(6510, 20, 102, 6, 0),
(6511, 20, 103, 6, 0),
(6512, 20, 104, 6, 0),
(6513, 20, 105, 6, 0),
(6514, 20, 111, 6, 0),
(6515, 20, 112, 6, 0),
(6516, 20, 114, 6, 0),
(6517, 20, 115, 6, 1),
(6527, 18, 88, 2, 1),
(6528, 18, 100, 2, 1),
(6529, 18, 116, 2, 1),
(6530, 18, 88, 3, 1),
(6531, 18, 100, 3, 1),
(6532, 18, 116, 3, 1),
(6533, 18, 88, 4, 1),
(6534, 18, 100, 4, 1),
(6535, 18, 116, 4, 1),
(6539, 18, 88, 1, 1),
(6540, 18, 100, 1, 0),
(6541, 18, 116, 1, 1),
(6680, 13, 57, 1, 1),
(6681, 13, 58, 1, 1),
(6682, 13, 59, 1, 1),
(6683, 13, 60, 1, 2),
(6684, 13, 61, 1, 1),
(6685, 13, 74, 1, 1),
(6701, 10, 47, 1, 1),
(6702, 10, 48, 1, 1),
(6703, 10, 49, 1, 1),
(6704, 10, 50, 1, 1),
(6705, 10, 51, 1, 1),
(6706, 10, 93, 1, 1),
(6707, 10, 128, 1, 1),
(6708, 10, 129, 1, 1),
(6709, 10, 130, 1, 1),
(6858, 2, 3, 2, 1),
(6859, 2, 4, 2, 1),
(6860, 2, 5, 2, 1),
(6861, 2, 72, 2, 1),
(6862, 2, 106, 2, 1),
(6863, 2, 107, 2, 1),
(6864, 2, 127, 2, 1),
(6865, 2, 136, 2, 1),
(6866, 2, 3, 0, 0),
(6867, 2, 4, 0, 0),
(6868, 2, 5, 0, 0),
(6869, 2, 72, 0, 0),
(6870, 2, 106, 0, 0),
(6871, 2, 107, 0, 0),
(6872, 2, 127, 0, 0),
(6873, 2, 136, 0, 1),
(6874, 2, 3, 1, 1),
(6875, 2, 4, 1, 1),
(6876, 2, 5, 1, 1),
(6877, 2, 72, 1, 1),
(6878, 2, 106, 1, 1),
(6879, 2, 107, 1, 1),
(6880, 2, 127, 1, 1),
(6881, 2, 136, 1, 1),
(6882, 2, 3, 3, 1),
(6883, 2, 4, 3, 1),
(6884, 2, 5, 3, 1),
(6885, 2, 72, 3, 1),
(6886, 2, 106, 3, 1),
(6887, 2, 107, 3, 1),
(6888, 2, 127, 3, 1),
(6889, 2, 136, 3, 1),
(6890, 12, 52, 1, 1),
(6891, 12, 53, 1, 1),
(6892, 12, 54, 1, 1),
(6893, 12, 55, 1, 1),
(6894, 12, 56, 1, 1),
(6895, 12, 138, 1, 1),
(6896, 21, 139, 1, 1),
(6897, 21, 140, 1, 1),
(6898, 21, 141, 1, 1),
(6899, 21, 142, 1, 1),
(6900, 21, 143, 1, 1),
(6925, 20, 101, 1, 0),
(6926, 20, 102, 1, 0),
(6927, 20, 103, 1, 0),
(6928, 20, 104, 1, 0),
(6929, 20, 105, 1, 0),
(6930, 20, 111, 1, 0),
(6931, 20, 112, 1, 0),
(6932, 20, 114, 1, 1),
(6933, 20, 115, 1, 1),
(6934, 20, 117, 1, 1),
(6935, 20, 118, 1, 1),
(6936, 20, 119, 1, 0),
(6937, 20, 120, 1, 0),
(6938, 20, 122, 1, 1),
(6939, 20, 123, 1, 1),
(6940, 20, 124, 1, 1),
(6941, 20, 125, 1, 1),
(6942, 20, 126, 1, 1),
(6943, 20, 131, 1, 1),
(6944, 20, 132, 1, 1),
(6945, 20, 133, 1, 1),
(6946, 20, 134, 1, 1),
(6947, 20, 135, 1, 1),
(6948, 20, 144, 1, 1),
(6949, 20, 145, 1, 1),
(6977, 20, 101, 2, 1),
(6978, 20, 102, 2, 1),
(6979, 20, 103, 2, 1),
(6980, 20, 104, 2, 1),
(6981, 20, 105, 2, 1),
(6982, 20, 111, 2, 1),
(6983, 20, 112, 2, 1),
(6984, 20, 114, 2, 1),
(6985, 20, 115, 2, 1),
(6986, 20, 117, 2, 0),
(6987, 20, 118, 2, 0),
(6988, 20, 119, 2, 1),
(6989, 20, 120, 2, 1),
(6990, 20, 122, 2, 0),
(6991, 20, 123, 2, 0),
(6992, 20, 124, 2, 0),
(6993, 20, 125, 2, 0),
(6994, 20, 126, 2, 0),
(6995, 20, 131, 2, 0),
(6996, 20, 132, 2, 0),
(6997, 20, 133, 2, 0),
(6998, 20, 134, 2, 0),
(6999, 20, 135, 2, 1),
(7000, 20, 144, 2, 0),
(7001, 20, 145, 2, 0),
(7002, 20, 146, 2, 1),
(7003, 20, 147, 2, 1),
(7004, 20, 148, 2, 1),
(7005, 20, 101, 3, 0),
(7006, 20, 102, 3, 0),
(7007, 20, 103, 3, 0),
(7008, 20, 104, 3, 0),
(7009, 20, 105, 3, 0),
(7010, 20, 111, 3, 0),
(7011, 20, 112, 3, 0),
(7012, 20, 114, 3, 1),
(7013, 20, 115, 3, 1),
(7014, 20, 117, 3, 0),
(7015, 20, 118, 3, 0),
(7016, 20, 119, 3, 0),
(7017, 20, 120, 3, 0),
(7018, 20, 122, 3, 0),
(7019, 20, 123, 3, 0),
(7020, 20, 124, 3, 0),
(7021, 20, 125, 3, 0),
(7022, 20, 126, 3, 0),
(7023, 20, 131, 3, 0),
(7024, 20, 132, 3, 0),
(7025, 20, 133, 3, 0),
(7026, 20, 134, 3, 0),
(7027, 20, 135, 3, 1),
(7028, 20, 144, 3, 0),
(7029, 20, 145, 3, 0),
(7030, 20, 146, 3, 0),
(7031, 20, 147, 3, 0),
(7032, 20, 148, 3, 1),
(7033, 20, 101, 4, 0),
(7034, 20, 102, 4, 0),
(7035, 20, 103, 4, 0),
(7036, 20, 104, 4, 0),
(7037, 20, 105, 4, 0),
(7038, 20, 111, 4, 0),
(7039, 20, 112, 4, 0),
(7040, 20, 114, 4, 1),
(7041, 20, 115, 4, 1),
(7042, 20, 117, 4, 0),
(7043, 20, 118, 4, 0),
(7044, 20, 119, 4, 0),
(7045, 20, 120, 4, 0),
(7046, 20, 122, 4, 0),
(7047, 20, 123, 4, 0),
(7048, 20, 124, 4, 0),
(7049, 20, 125, 4, 0),
(7050, 20, 126, 4, 0),
(7051, 20, 131, 4, 0),
(7052, 20, 132, 4, 0),
(7053, 20, 133, 4, 0),
(7054, 20, 134, 4, 0),
(7055, 20, 135, 4, 1),
(7056, 20, 144, 4, 0),
(7057, 20, 145, 4, 0),
(7058, 20, 146, 4, 0),
(7059, 20, 147, 4, 0),
(7060, 20, 148, 4, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `plano`
--

CREATE TABLE IF NOT EXISTS `plano` (
  `plan_id` int(11) NOT NULL,
  `plan_name` varchar(45) NOT NULL,
  `plan_desc` text,
  `plan_price` decimal(10,2) NOT NULL,
  `plan_startdate` datetime DEFAULT NULL,
  `plan_enddate` datetime DEFAULT NULL,
  `plan_status_id` int(11) NOT NULL,
  `plan_order` int(11) DEFAULT NULL,
  `plano_max_users` int(11) NOT NULL,
  `server_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `plano`
--

INSERT INTO `plano` (`plan_id`, `plan_name`, `plan_desc`, `plan_price`, `plan_startdate`, `plan_enddate`, `plan_status_id`, `plan_order`, `plano_max_users`, `server_id`) VALUES
(1, 'BASIC', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet auctor est. Nulla facilisi. Ut fermentum facilisis iaculis. Donec in nunc sapien. Suspendisse non massa id purus imperdiet suscipit sit amet in enim. Donec sed sapien diam. Mauris eu arcu massa. Aliquam congue dignissim interdum. Donec eget hendrerit elit, vitae vulputate nisl. Sed ac orci posuere, efficitur sem vitae, hendrerit nunc. Vestibulum quam leo, mollis at euismod vel, tempus quis augue. Sed consectetur mi et elit posuere blandit. Donec vestibulum blandit consectetur. Fusce mauris augue, interdum eu pharetra sit amet, vulputate vel ex. Nam feugiat rutrum nulla. Donec eleifend est libero, et pellentesque lectus commodo a.</p>\n\n<p>Vivamus risus erat, blandit sed vulputate eu, venenatis non urna. Duis sit amet elementum felis. Curabitur elementum rutrum eros, non aliquam mi mattis at. Nunc viverra tristique scelerisque. Mauris a semper sem, a ornare leo. Praesent vehicula viverra felis, et tristique mauris ultricies sit amet. Sed eget risus tellus. Sed non pretium mi, id porta augue. Praesent maximus enim sed diam pulvinar, vitae rutrum odio lacinia. Morbi vel erat vulputate, dapibus ex sed, tincidunt metus.</p>\n\n<p>Duis cursus eget magna quis porta. Nullam pharetra condimentum nisi sed mollis. Vivamus diam lacus, faucibus id leo euismod, tempor pellentesque massa. Mauris et tellus leo. Mauris mollis vel sapien eget suscipit. Mauris lobortis placerat dui, id tempor augue porttitor vitae. Nam a facilisis libero. Vestibulum sit amet dictum sem, eget malesuada lectus. Nullam dapibus ipsum lacus, sit amet facilisis mauris tincidunt non. Vivamus facilisis mauris at nunc consectetur euismod. Quisque porttitor ipsum dignissim purus suscipit, nec sollicitudin massa dapibus. Maecenas condimentum accumsan nisi. Nam tempus urna tortor, vulputate venenatis lacus eleifend vitae. Nulla venenatis nisl ut porttitor feugiat.</p>\n\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam eros nisi, ullamcorper nec ante sed, tincidunt placerat libero. Vivamus semper vestibulum neque sed hendrerit. Quisque sapien ipsum, auctor id luctus et, condimentum eu risus. Vestibulum ac lectus diam. Phasellus eleifend euismod purus sit amet imperdiet. Phasellus consectetur congue sapien vel luctus. Donec pretium a magna vitae dictum. Fusce in elit in arcu blandit tristique. Proin tincidunt lobortis ultricies. Nunc tempus ligula quis aliquet tristique. Integer porttitor vitae libero et tincidunt. Aliquam vulputate porta tortor id maximus.</p>\n\n<p>Sed nunc diam, tristique in ipsum quis, venenatis commodo justo. Cras facilisis nisl vitae enim dictum, a ultricies urna dapibus. Nulla rutrum convallis scelerisque. Aliquam luctus magna felis, vitae luctus turpis ornare ut. Integer ut metus libero. Duis accumsan tempor sapien eget porta. Phasellus at nisl sit amet nunc cursus aliquam vel vitae tortor. Integer id libero ac velit molestie placerat nec vel orci. Nunc ligula sapien, ullamcorper vel condimentum quis, faucibus vitae eros. Aliquam vel vestibulum ante, in eleifend orci. Sed pharetra purus quis urna fermentum finibus. Phasellus ac neque suscipit arcu varius aliquet sit amet a elit.</p>\n', '10.00', NULL, NULL, 1, NULL, 5, 1),
(2, 'Avançaado', '<p>Armazenamento</p>\n', '30.00', NULL, NULL, 1, NULL, 5, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `plano_servico`
--

CREATE TABLE IF NOT EXISTS `plano_servico` (
  `plan_service_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `plano_servico`
--

INSERT INTO `plano_servico` (`plan_service_id`, `plan_id`, `service_id`) VALUES
(2, 1, 1),
(3, 1, 3),
(4, 1, 12),
(7, 2, 1),
(8, 2, 3),
(9, 2, 19);

-- --------------------------------------------------------

--
-- Estrutura para tabela `plano_status`
--

CREATE TABLE IF NOT EXISTS `plano_status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `status_editable` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `plano_status`
--

INSERT INTO `plano_status` (`status_id`, `status_name`, `status_editable`) VALUES
(1, 'Ativo', 0),
(2, 'Inativo', 0),
(3, 'Cancelado', 0),
(4, 'Status 1', 0),
(5, 'teste 2', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `quantidade_armazenamento`
--

CREATE TABLE IF NOT EXISTS `quantidade_armazenamento` (
  `quant_id` int(11) NOT NULL,
  `quant_desc` varchar(45) NOT NULL,
  `quant_valor` varchar(100) NOT NULL,
  `quant_preco` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `quantidade_armazenamento`
--

INSERT INTO `quantidade_armazenamento` (`quant_id`, `quant_desc`, `quant_valor`, `quant_preco`) VALUES
(1, '1GB', '1073741824', '10.00'),
(2, '1GB', '2073741824', '20.00'),
(3, '5GB', '5073741824', '50.00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `segmento`
--

CREATE TABLE IF NOT EXISTS `segmento` (
  `segmento_id` int(11) NOT NULL,
  `segmento_name` varchar(45) NOT NULL,
  `segmento_status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `segmento`
--

INSERT INTO `segmento` (`segmento_id`, `segmento_name`, `segmento_status_id`) VALUES
(1, 'Serviços', 2),
(2, 'Escritório', 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `segmento_diretorio`
--

CREATE TABLE IF NOT EXISTS `segmento_diretorio` (
  `dir_id` int(11) NOT NULL,
  `dir_name` varchar(30) NOT NULL,
  `dir_parent_id` int(11) DEFAULT NULL,
  `dir_segmento_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `segmento_diretorio`
--

INSERT INTO `segmento_diretorio` (`dir_id`, `dir_name`, `dir_parent_id`, `dir_segmento_id`) VALUES
(1, 'Financeiro', NULL, 1),
(2, 'clientes', 1, 1),
(3, 'RH', NULL, 1),
(4, 'RH', NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `segmento_status`
--

CREATE TABLE IF NOT EXISTS `segmento_status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `status_editable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `segmento_status`
--

INSERT INTO `segmento_status` (`status_id`, `status_name`, `status_editable`) VALUES
(1, 'Inativo', 0),
(2, 'Ativo', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `servico`
--

CREATE TABLE IF NOT EXISTS `servico` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(45) NOT NULL,
  `service_desc` text,
  `service_price` decimal(10,2) NOT NULL,
  `service_recursive` tinyint(1) DEFAULT '1',
  `service_storage` varchar(15) DEFAULT NULL,
  `service_status_id` int(11) NOT NULL,
  `service_editable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `servico`
--

INSERT INTO `servico` (`service_id`, `service_name`, `service_desc`, `service_price`, `service_recursive`, `service_storage`, `service_status_id`, `service_editable`) VALUES
(1, 'Gerenciador de Arquivos', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nVivamus eget orci vitae orci aliquet consectetur at at quam.\nVestibulum nec magna in elit dignissim cursus.\nSuspendisse ac lectus id erat mollis iaculis sed et lacus.', '10.00', 1, '-1', 1, 0),
(2, 'Backup Automático', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nVivamus eget orci vitae orci aliquet consectetur at at quam.\nVestibulum nec magna in elit dignissim cursus.\nSuspendisse ac lectus id erat mollis iaculis sed et lacus.', '10.00', 1, '-1', 1, 0),
(3, 'Armazenamento Externo	', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nVivamus eget orci vitae orci aliquet consectetur at at quam.\nVestibulum nec magna in elit dignissim cursus.\nSuspendisse ac lectus id erat mollis iaculis sed et lacus.', '0.00', 1, '-1', 1, 0),
(4, 'Taxa de Adesão *	', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nVivamus eget orci vitae orci aliquet consectetur at at quam.\nVestibulum nec magna in elit dignissim cursus.\nSuspendisse ac lectus id erat mollis iaculis sed et lacus.', '100.00', 0, '-1', 1, 0),
(12, '1 GB', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur faucibus tellus lectus, non dapibus purus hendrerit ut. In ante lacus, molestie et quam sed, gravida volutpat tellus. Donec eleifend laoreet laoreet. Proin suscipit augue in leo elementum ', '0.00', 1, '1073741824', 1, 1),
(14, '50 GB', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nVivamus eget orci vitae orci aliquet consectetur at at quam.\nVestibulum nec magna in elit dignissim cursus.\nSuspendisse ac lectus id erat mollis iaculis sed et lacus.', '22.90', 1, '53687091200', 1, 1),
(15, '100GB', 'Morbi bibendum, dolor sit amet convallis feugiat, sapien neque suscipit justo, in auctor lacus odio quis leo. Vestibulum aliquet malesuada enim tincidunt fringilla. Quisque lacus urna, pretium nec nulla vel, rutrum venenatis orci. Praesent porttitor conse', '69.00', 1, '107374182400', 1, 1),
(17, '1TB', 'Sed ac vestibulum augue. Aenean sollicitudin risus et leo venenatis aliquet. Nam venenatis enim sit amet arcu hendrerit, eget malesuada nibh aliquam. Phasellus faucibus mi et mi placerat pretium. In vitae turpis sit amet felis luctus suscipit. Proin phare', '99.00', 0, '137438953472', 1, 1),
(18, '500 GB', 'Sed ac vestibulum augue. Aenean sollicitudin risus et leo venenatis aliquet. Nam venenatis enim sit amet arcu hendrerit, eget malesuada nibh aliquam. Phasellus faucibus mi et mi placerat pretium. In vitae turpis sit amet felis luctus suscipit. Proin phare enim sit amet arcu hendrerit, eget malesuada nibh aliquam. Phasellus faucibus mi et mi placerat pretium. In vitae turpis sit amet felis luctus suscipit. \nPhasellus faucibus mi et mi placerat pretium. In vitae turpis sit amet felis luctus suscipit enim sit amet arcu hendrerit, eget malesuada nibh aliquam. ', '49.00', 1, '536870912000', 1, 1),
(19, '5GB', 'armazenamneto', '20.00', 1, '5073741824', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `servico_status`
--

CREATE TABLE IF NOT EXISTS `servico_status` (
  `service_status_id` int(11) NOT NULL,
  `service_status_name` varchar(45) NOT NULL,
  `service_status_editable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `servico_status`
--

INSERT INTO `servico_status` (`service_status_id`, `service_status_name`, `service_status_editable`) VALUES
(1, 'Ativo', 0),
(2, 'Inativo', 0),
(3, 'Obsoleto', 1),
(4, 'Expirado', 1),
(5, 'Extinto', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `servidor`
--

CREATE TABLE IF NOT EXISTS `servidor` (
  `server_id` int(11) NOT NULL,
  `server_type` int(11) NOT NULL,
  `server_path` varchar(255) DEFAULT NULL,
  `server_key` varchar(45) DEFAULT NULL,
  `server_secret` varchar(65) DEFAULT NULL,
  `server_address` varchar(255) DEFAULT NULL,
  `server_port` varchar(10) DEFAULT NULL,
  `server_username` varchar(45) DEFAULT NULL,
  `server_password` varchar(255) DEFAULT NULL,
  `server_is_default` tinyint(1) NOT NULL DEFAULT '0',
  `client_id` int(11) DEFAULT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `servidor`
--

INSERT INTO `servidor` (`server_id`, `server_type`, `server_path`, `server_key`, `server_secret`, `server_address`, `server_port`, `server_username`, `server_password`, `server_is_default`, `client_id`, `is_admin`) VALUES
(1, 1, '/home/files', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1),
(2, 2, NULL, 'AKIAJWRXNPGQ23SOUNGA', 'AtYswx97hNnrZ+BNd9RUlok+5+sDn+LOz9fJ8LYc', NULL, NULL, NULL, NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `share`
--

CREATE TABLE IF NOT EXISTS `share` (
  `share_id` int(11) NOT NULL,
  `object` varchar(255) NOT NULL,
  `user` varchar(100) DEFAULT NULL,
  `group` int(11) DEFAULT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  `shared_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(155) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `share`
--

INSERT INTO `share` (`share_id`, `object`, `user`, `group`, `expires`, `shared_at`, `email`) VALUES
(1, 'c9xZUM5NIjSh2EvuBJxfm2nXvoAK7C', NULL, NULL, NULL, '2015-11-15 23:43:08', 'maicon.marilia@gmail.com'),
(2, '5KzQQ5M7fjcnRnpHt6IOaYpE9I0CTe', NULL, NULL, NULL, '2015-11-28 10:57:04', 'xpardall@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `user_id` int(11) NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_login` varchar(20) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_picture` varchar(255) DEFAULT NULL,
  `user_trusted_mail` tinyint(1) NOT NULL DEFAULT '0',
  `user_register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_hash` varchar(255) DEFAULT NULL,
  `user_status_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_plan_id` int(11) DEFAULT NULL,
  `user_key` varchar(100) NOT NULL,
  `owner` tinyint(1) NOT NULL DEFAULT '0',
  `encerrado_em` timestamp NULL DEFAULT NULL,
  `redefinition_key` varchar(255) DEFAULT NULL,
  `user_update_day` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`user_id`, `user_fullname`, `user_login`, `user_email`, `user_password`, `user_picture`, `user_trusted_mail`, `user_register_date`, `user_hash`, `user_status_id`, `user_type_id`, `user_plan_id`, `user_key`, `owner`, `encerrado_em`, `redefinition_key`, `user_update_day`) VALUES
(12, 'Administrador', 'admin', 'maicon.marilia@gmail.com', 'S1WFd9Jphh/HdY9GXWlR2TU+zKvjaoXhoGDGpYGnjAkMiwp9O11RrK7ljo5TRsErk5E+I8Eqhmd9Z4lU53fTHQ==', 'e58cb586b0405b218df4b68286a354fb.jpg', 1, '2015-05-30 15:43:12', NULL, 2, 1, NULL, '', 0, NULL, NULL, NULL),
(15, 'Suporte', 'suporte12', 'suporte12@ged.com.br', 'S1WFd9Jphh/HdY9GXWlR2TU+zKvjaoXhoGDGpYGnjAkMiwp9O11RrK7ljo5TRsErk5E+I8Eqhmd9Z4lU53fTHQ==', NULL, 0, '2015-10-26 07:02:27', 'k8GjXC4VtEI+rMskzCKDpExtafuyflaF9VkV8uUDgmtR2InTNme28dKkk+NADQ7Y8EZzGf3+bSJn5FXauinrqQ==', 2, 6, NULL, '6zcM5UpsXwVMHQ0HYHwb2O6yvyKPplpJYjD2GOssa7Lzz5GiBARBKnO9AWR1zREQYIWlV9kNIpLwY28XKe9dpXzP1KNhokD9gjh6', 0, NULL, NULL, NULL),
(16, 'Vagner Leitte', 'vagnerleitte', 'vagner.leite.silva@gmail.com', 'SZ5LAeLdzi2fpeuK1q7VYEXVScGQjMm68bq36ZSmdXziU8e4oN00ONSUt6h1nYrC74e5qNbHJsoxZyTmt1k7Jw==', NULL, 1, '2015-11-10 01:34:04', NULL, 7, 2, 1, 'JVJnI9rUu171lyAsy77sZDbN41w7gFudTCTjmHm7n0OSvg9sjXDaNsyIIfRHuiNhAj0eeCYoTLEl44KIA3Btb3vLl7njkAPeqXVU', 1, NULL, NULL, NULL),
(17, 'maicon nicodemo', 'maicon', 'maicon_digitec@hotmail.com', 'N4RYua0Hb0xIV6rN0CAPRsdJb5U6U/PyDYvLa+ntx7OcsC/qSoTnW0sYnelEdWeXNXxiJTRxvyMJBpoMqM9UNQ==', '3101ca90885ff16d0680d28936fe5d24.jpg', 1, '2015-11-10 05:27:25', NULL, 7, 2, 1, 'K474M85zmOW654Eg2LHidpwUUA9vmouJ1KicIfvxkyRwDbt43Dvl1GaJyseKCzRpejMq1xhAPudB8lLTiTk7RII4bB2RZ70lBHMc', 1, NULL, NULL, NULL),
(18, 'daniel costa', 'daniel', 'contato@centraldenotas.com.br', '8jt1TE1Ex2Guz6MnGzxz3gBAYNqqET/zg8qasGyn0tkML/eUf+d11vc47zN3qw4Xj31gM8T+CKn+V4+vUriUUA==', NULL, 1, '2015-11-10 09:48:51', NULL, 7, 2, 2, 'z7zqjsaPAJgwmpxR3uuXP0SzW7tZmg12ArNR3TKaZ3NkMOv8qpdeq2Yym3zQthXY2LwjSJqlmgI3qckpepnee54m6CbeMSwEdFC2', 1, NULL, NULL, NULL),
(19, 'fabio pereira', 'pereira', 'comercial@centraldenotas.com.br', 'OOw1fVhN4p+685uydtV4qfKCj5voMxyYRPNh9ujtpPBbcKfLE+zkxwEHjLgYHWhzKFfmrr0x14mzBrXOfS9JLw==', NULL, 1, '2015-11-11 04:04:10', NULL, 7, 4, NULL, '3ds5hj5KTKCgXE0ggnP31kMzb4LDrQX3cyZayGvhgYUmeBX3eQ5Kpl1lwifvOwLWT8PSxyej8nSsFiBWWLePFEYUP39zg0hvwZ8F', 0, NULL, NULL, NULL),
(20, 'Danielle ', 'danismgomes', 'danielle.alisp@gmail.com', 'P/k+ImP6nkegkRKe13d66Z8LKbJ+piRbfEol+ECdOlES+KYyVfJqOiWHmVJC7OxFy62rrtElWqTPHFBV259InQ==', NULL, 1, '2015-11-23 11:52:49', NULL, 7, 2, 1, 'xeTp1efst2Ened3SE45RRQ8u52yCCxPBx6HcE8boo3SpU41SSOQ2jRP09DkSsPq1k8AbZUV3tUlbisWRiJIfOWEAvzgm6Vl7qOWy', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_contato`
--

CREATE TABLE IF NOT EXISTS `usuario_contato` (
  `usuario_contato_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `contato_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_endereco`
--

CREATE TABLE IF NOT EXISTS `usuario_endereco` (
  `user_endereco_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `endereco_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_quota`
--

CREATE TABLE IF NOT EXISTS `usuario_quota` (
  `quota_id` int(11) NOT NULL,
  `quota_size` bigint(20) NOT NULL DEFAULT '-1',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario_quota`
--

INSERT INTO `usuario_quota` (`quota_id`, `quota_size`, `user_id`) VALUES
(5, 1073741824, 14),
(8, 1073741824, 17),
(9, 0, 18),
(11, 1073741824, 19);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_status`
--

CREATE TABLE IF NOT EXISTS `usuario_status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `status_editable` tinyint(1) NOT NULL DEFAULT '0',
  `status_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario_status`
--

INSERT INTO `usuario_status` (`status_id`, `status_name`, `status_editable`, `status_admin`) VALUES
(1, 'Inativo', 0, 0),
(2, 'Ativo', 0, 0),
(3, 'Bloqueado', 0, 0),
(4, 'Banido', 0, 0),
(5, 'Desativado', 0, 0),
(6, 'Pendente de Confirmação', 0, 0),
(7, 'Pendente de Configuração', 0, 0),
(8, 'Conta pendente de Exclusão', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_tipo`
--

CREATE TABLE IF NOT EXISTS `usuario_tipo` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(20) NOT NULL,
  `type_desc` varchar(255) DEFAULT NULL,
  `type_editable` tinyint(1) NOT NULL DEFAULT '0',
  `type_has_permissions` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario_tipo`
--

INSERT INTO `usuario_tipo` (`type_id`, `type_name`, `type_desc`, `type_editable`, `type_has_permissions`) VALUES
(1, 'SUPERUSUARIO', 'Usuário Administrativo', 0, 0),
(2, 'CLIADMIN', 'Cliente  Administrador de Empresa', 0, 1),
(3, 'CLIPESSOA', 'Cliente Administrador Pessoal', 0, 1),
(4, 'USUARIO', 'Usuário Comum', 0, 1),
(5, 'INDEFINIDO', 'Usuário indefinido', 0, 0),
(6, 'SUPORTE', 'Suporte aos usuários.', 0, 1);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `acao`
--
ALTER TABLE `acao`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `fk_acao_module_id_idx` (`action_module_id`);

--
-- Índices de tabela `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`bank_id`);

--
-- Índices de tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`client_id`),
  ADD UNIQUE KEY `client_key` (`client_key`),
  ADD UNIQUE KEY `client_volume` (`client_volume`),
  ADD KEY `fk_cliente_user_id_idx` (`client_user_id`),
  ADD KEY `fk_cliente_empresa_id_idx` (`client_empresa_id`),
  ADD KEY `server_id` (`server_id`);

--
-- Índices de tabela `compania`
--
ALTER TABLE `compania`
  ADD PRIMARY KEY (`company_id`);

--
-- Índices de tabela `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`conf_id`),
  ADD KEY `fk_configuration_user_id_idx` (`user_id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`contato_id`),
  ADD KEY `fk_contato_tipo_id_idx` (`tipo_id`);

--
-- Índices de tabela `contato_tipo`
--
ALTER TABLE `contato_tipo`
  ADD PRIMARY KEY (`tipo_id`);

--
-- Índices de tabela `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`contrato_id`),
  ADD KEY `fk_contrato_cliente_idx` (`contrato_cliente`);

--
-- Índices de tabela `download_link`
--
ALTER TABLE `download_link`
  ADD PRIMARY KEY (`link_id`);

--
-- Índices de tabela `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`empresa_id`),
  ADD UNIQUE KEY `empresa_key` (`empresa_key`),
  ADD UNIQUE KEY `empresa_cnpj` (`empresa_cnpj`),
  ADD KEY `fk_empresa_segmento_id_idx` (`empresa_segmento_id`),
  ADD KEY `fk_empresa_user_id_idx` (`empresa_user_id`);

--
-- Índices de tabela `empresa_contato`
--
ALTER TABLE `empresa_contato`
  ADD PRIMARY KEY (`empresa_contato_id`),
  ADD KEY `fk_empresa_contato_empresa_id_idx` (`empresa_id`),
  ADD KEY `fk_empresa_contato_id_idx` (`contato_id`);

--
-- Índices de tabela `empresa_endereco`
--
ALTER TABLE `empresa_endereco`
  ADD PRIMARY KEY (`empresa_endereco_id`),
  ADD KEY `fk_empresa_endereco_id_idx` (`empresa_id`),
  ADD KEY `fk_empresa_endereco_id_idx1` (`endereco_id`);

--
-- Índices de tabela `empresa_usuario`
--
ALTER TABLE `empresa_usuario`
  ADD PRIMARY KEY (`id_empresa_usuario`),
  ADD KEY `fk_empresa_usuario_id_idx` (`id_usuario`),
  ADD KEY `fk_empresa_id_usuario_idx` (`id_empresa`);

--
-- Índices de tabela `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`endereco_id`);

--
-- Índices de tabela `espaco_adicional`
--
ALTER TABLE `espaco_adicional`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_espaco_adicional_1_idx` (`quant_id`),
  ADD KEY `fk_espaco_adicional_2_idx` (`client_id`);

--
-- Índices de tabela `faturas`
--
ALTER TABLE `faturas`
  ADD PRIMARY KEY (`fatura_id`),
  ADD KEY `fk_fatura_status_idx` (`status_id`);

--
-- Índices de tabela `fatura_itens`
--
ALTER TABLE `fatura_itens`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `fatura_id` (`fatura_id`);

--
-- Índices de tabela `fatura_status`
--
ALTER TABLE `fatura_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Índices de tabela `file_log`
--
ALTER TABLE `file_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Índices de tabela `file_object`
--
ALTER TABLE `file_object`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `storage_key` (`storage_key`),
  ADD KEY `owner` (`owner`);

--
-- Índices de tabela `file_type`
--
ALTER TABLE `file_type`
  ADD PRIMARY KEY (`id_file_type`);

--
-- Índices de tabela `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`group_id`);

--
-- Índices de tabela `group_permission`
--
ALTER TABLE `group_permission`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `fk_group_object_id_idx` (`object_id`),
  ADD KEY `fk_group_permission_id_idx` (`group_id`);

--
-- Índices de tabela `group_users`
--
ALTER TABLE `group_users`
  ADD PRIMARY KEY (`group_user_id`),
  ADD KEY `fk_group_usuario_group_idx` (`group_id`),
  ADD KEY `fk_group_group_user_id_idx` (`user_id`);

--
-- Índices de tabela `info_labels`
--
ALTER TABLE `info_labels`
  ADD PRIMARY KEY (`label_id`);

--
-- Índices de tabela `log_history`
--
ALTER TABLE `log_history`
  ADD PRIMARY KEY (`log_id`);

--
-- Índices de tabela `mensagens`
--
ALTER TABLE `mensagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mensagens_from_user_idx` (`from_user`),
  ADD KEY `fk_mensagens_to_user_idx` (`to_user`);

--
-- Índices de tabela `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`module_id`);

--
-- Índices de tabela `permissao`
--
ALTER TABLE `permissao`
  ADD PRIMARY KEY (`permissao_id`),
  ADD KEY `fk_permissao_module_id_idx` (`permissao_module_id`),
  ADD KEY `fk_permissao_user_type_id_idx` (`permissao_type_id`),
  ADD KEY `fk_permissao_action_id_idx` (`permissao_action_id`);

--
-- Índices de tabela `plano`
--
ALTER TABLE `plano`
  ADD PRIMARY KEY (`plan_id`),
  ADD KEY `fk_plano_status_idx` (`plan_status_id`),
  ADD KEY `server_id` (`server_id`);

--
-- Índices de tabela `plano_servico`
--
ALTER TABLE `plano_servico`
  ADD PRIMARY KEY (`plan_service_id`),
  ADD KEY `fk_plano_servico_id_idx` (`service_id`),
  ADD KEY `fk_plano_servico_plan_id_idx` (`plan_id`);

--
-- Índices de tabela `plano_status`
--
ALTER TABLE `plano_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Índices de tabela `quantidade_armazenamento`
--
ALTER TABLE `quantidade_armazenamento`
  ADD PRIMARY KEY (`quant_id`);

--
-- Índices de tabela `segmento`
--
ALTER TABLE `segmento`
  ADD PRIMARY KEY (`segmento_id`),
  ADD UNIQUE KEY `segmento_name` (`segmento_name`),
  ADD KEY `fk_segmento_status_id_idx` (`segmento_status_id`);

--
-- Índices de tabela `segmento_diretorio`
--
ALTER TABLE `segmento_diretorio`
  ADD PRIMARY KEY (`dir_id`),
  ADD KEY `fk_segmento_diretorio_id_idx` (`dir_segmento_id`),
  ADD KEY `dir_parent_id` (`dir_parent_id`);

--
-- Índices de tabela `segmento_status`
--
ALTER TABLE `segmento_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Índices de tabela `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`service_id`),
  ADD KEY `fk_servico_status_id_idx` (`service_status_id`);

--
-- Índices de tabela `servico_status`
--
ALTER TABLE `servico_status`
  ADD PRIMARY KEY (`service_status_id`);

--
-- Índices de tabela `servidor`
--
ALTER TABLE `servidor`
  ADD PRIMARY KEY (`server_id`),
  ADD KEY `fk_servidor_client_id_idx` (`client_id`);

--
-- Índices de tabela `share`
--
ALTER TABLE `share`
  ADD PRIMARY KEY (`share_id`),
  ADD KEY `fk_share_object_hash_idx` (`object`),
  ADD KEY `fk_share_user_key_idx` (`user`),
  ADD KEY `fk_share_group_id_idx` (`group`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_login` (`user_login`),
  ADD UNIQUE KEY `user_key` (`user_key`),
  ADD KEY `fk_ged_user_status_idx` (`user_status_id`),
  ADD KEY `fk_usuario_user_type_idx` (`user_type_id`),
  ADD KEY `user_plan_id` (`user_plan_id`);

--
-- Índices de tabela `usuario_contato`
--
ALTER TABLE `usuario_contato`
  ADD PRIMARY KEY (`usuario_contato_id`),
  ADD KEY `fk_usuario_contato_usuario_idx` (`usuario_id`),
  ADD KEY `fk_usuario_contato_idcontato_idx` (`contato_id`);

--
-- Índices de tabela `usuario_endereco`
--
ALTER TABLE `usuario_endereco`
  ADD PRIMARY KEY (`user_endereco_id`),
  ADD KEY `fk_usuario_endereco_user_idx` (`user_id`),
  ADD KEY `fk_usuario_endereco_end_idx` (`endereco_id`);

--
-- Índices de tabela `usuario_quota`
--
ALTER TABLE `usuario_quota`
  ADD PRIMARY KEY (`quota_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Índices de tabela `usuario_status`
--
ALTER TABLE `usuario_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Índices de tabela `usuario_tipo`
--
ALTER TABLE `usuario_tipo`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `acao`
--
ALTER TABLE `acao`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT de tabela `banco`
--
ALTER TABLE `banco`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `compania`
--
ALTER TABLE `compania`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `configuration`
--
ALTER TABLE `configuration`
  MODIFY `conf_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `contato_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de tabela `contato_tipo`
--
ALTER TABLE `contato_tipo`
  MODIFY `tipo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `contrato`
--
ALTER TABLE `contrato`
  MODIFY `contrato_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `download_link`
--
ALTER TABLE `download_link`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
  MODIFY `empresa_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `empresa_contato`
--
ALTER TABLE `empresa_contato`
  MODIFY `empresa_contato_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `empresa_endereco`
--
ALTER TABLE `empresa_endereco`
  MODIFY `empresa_endereco_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `empresa_usuario`
--
ALTER TABLE `empresa_usuario`
  MODIFY `id_empresa_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `endereco`
--
ALTER TABLE `endereco`
  MODIFY `endereco_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `espaco_adicional`
--
ALTER TABLE `espaco_adicional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `faturas`
--
ALTER TABLE `faturas`
  MODIFY `fatura_id` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `fatura_itens`
--
ALTER TABLE `fatura_itens`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de tabela `fatura_status`
--
ALTER TABLE `fatura_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `file_log`
--
ALTER TABLE `file_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT de tabela `file_object`
--
ALTER TABLE `file_object`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT de tabela `file_type`
--
ALTER TABLE `file_type`
  MODIFY `id_file_type` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=689;
--
-- AUTO_INCREMENT de tabela `group`
--
ALTER TABLE `group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `group_permission`
--
ALTER TABLE `group_permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `group_users`
--
ALTER TABLE `group_users`
  MODIFY `group_user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `info_labels`
--
ALTER TABLE `info_labels`
  MODIFY `label_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `log_history`
--
ALTER TABLE `log_history`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=642;
--
-- AUTO_INCREMENT de tabela `mensagens`
--
ALTER TABLE `mensagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `modulo`
--
ALTER TABLE `modulo`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de tabela `permissao`
--
ALTER TABLE `permissao`
  MODIFY `permissao_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7061;
--
-- AUTO_INCREMENT de tabela `plano`
--
ALTER TABLE `plano`
  MODIFY `plan_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `plano_servico`
--
ALTER TABLE `plano_servico`
  MODIFY `plan_service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de tabela `plano_status`
--
ALTER TABLE `plano_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `quantidade_armazenamento`
--
ALTER TABLE `quantidade_armazenamento`
  MODIFY `quant_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `segmento`
--
ALTER TABLE `segmento`
  MODIFY `segmento_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `segmento_diretorio`
--
ALTER TABLE `segmento_diretorio`
  MODIFY `dir_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `segmento_status`
--
ALTER TABLE `segmento_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `servico`
--
ALTER TABLE `servico`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de tabela `servico_status`
--
ALTER TABLE `servico_status`
  MODIFY `service_status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `servidor`
--
ALTER TABLE `servidor`
  MODIFY `server_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `share`
--
ALTER TABLE `share`
  MODIFY `share_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de tabela `usuario_contato`
--
ALTER TABLE `usuario_contato`
  MODIFY `usuario_contato_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `usuario_endereco`
--
ALTER TABLE `usuario_endereco`
  MODIFY `user_endereco_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `usuario_quota`
--
ALTER TABLE `usuario_quota`
  MODIFY `quota_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de tabela `usuario_status`
--
ALTER TABLE `usuario_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `usuario_tipo`
--
ALTER TABLE `usuario_tipo`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `ged`.`xml_file` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hash` VARCHAR(255) NOT NULL,
  `file_name` VARCHAR(255) NOT NULL,
  `full_path` VARCHAR(255) NOT NULL,
  `extension` VARCHAR(10) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `processed` TINYINT NOT NULL,
  PRIMARY KEY (`id`));
