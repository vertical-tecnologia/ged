
/*Importante para o projeto local*/
/*Existe uma trigger setada para um usuário inexistente no banco local e pode dar problema na momento de faturar o plano.
  Para solucionar esse problema você pode tanto alterar o usuário atribuido pela trigger ou realizar um drop e cria-la 
  novamente com o seu usuário.
 */
DROP TRIGGER update_number;

DELIMITER $$
CREATE TRIGGER `update_number` BEFORE INSERT ON `faturas`
 FOR EACH ROW BEGIN
DECLARE nosso, proximo INT(11);
SELECT MAX(nosso_numero) INTO nosso FROM faturas;

IF(nosso IS NULL)
THEN
	SELECT bank_nosso_atual INTO nosso FROM banco;
END IF;

SET proximo = nosso +1;

SET new.nosso_numero = proximo;

UPDATE banco SET bank_nosso_atual = proximo;

END
$$
DELIMITER ;

INSERT INTO acao (action_name, action_module_id)
VALUES 
('company', 1),
('personal', 1);

INSERT INTO permissao  (permissao_module_id, permissao_action_id, permissao_type_id, permissao_allow)
VALUES
(1, (SELECT action_id FROM acao WHERE action_name = 'company' AND action_module_id = 1), 2, 1),
(1, (SELECT action_id FROM acao WHERE action_name = 'personal' AND action_module_id = 1), 2, 1);

ALTER TABLE empresa
ADD COLUMN empresa_cpf_responsavel CHAR(11) NULL AFTER empresa_ie;

ALTER TABLE empresa
ADD COLUMN empresa_rg_responsavel CHAR(9) NULL AFTER empresa_cpf_responsavel;

ALTER TABLE empresa 
CHANGE empresa_razao empresa_razao VARCHAR(155)  NULL;

ALTER TABLE empresa 
CHANGE empresa_fantasia empresa_fantasia VARCHAR(155)  NULL;

ALTER TABLE empresa 
CHANGE empresa_cnpj empresa_cnpj VARCHAR(14)  NULL;

INSERT INTO acao(action_id, action_name, action_module_id) VALUES(149, 'personal', 1);
INSERT INTO acao(action_id, action_name, action_module_id) VALUES(150, 'company', 1);

ALTER TABLE xml_file
ADD COLUMN server_id INT(11) NULL DEFAULT 2;

INSERT INTO acao (action_name, action_module_id)
VALUES 
('xml', 20);

INSERT INTO permissao  (permissao_module_id, permissao_action_id, permissao_type_id, permissao_allow)
VALUES
(2, (SELECT action_id FROM acao WHERE action_name = 'xml' AND action_module_id = 20), 2, 1);

CREATE TABLE usuario_xml_tag (
	id INT(11) NOT NULL AUTO_INCREMENT,
	tag VARCHAR(300) NOT NULL,
	xml_type INT(11) NOT NULL,
	user_key VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
)ENGINE=INNODB AUTO_INCREMENT=1 CHARSET='latin1';

ALTER TABLE xml_file
ADD COLUMN content TEXT NOT NULL;

UPDATE compania SET company_razao = 'Central de Notas - Central de Notas Armazenamento Digital Ltda - ME', company_fantasia = 'Central de Notas', company_email = 'financeiro@centraldenotas.com.br',
company_cnpj = '14.566.416/0001-86', company_ie = 'ISENTO',  company_endereco = 'Av Sigismundo Nunes De Oliveira, 570, Casa 140 Cond Terra Nova, Jardim Nazareth', company_cep = '17512-752', company_cidade= 'MARILIA', company_estado='SP'
WHERE company_id = 1;

ALTER TABLE empresa
ADD COLUMN total_server INT(2) NOT NULL DEFAULT 0;

INSERT INTO acao (action_name, action_module_id)
VALUES 
('servidor', 16);

INSERT INTO permissao  (permissao_module_id, permissao_action_id, permissao_type_id, permissao_allow)
VALUES
(1, (SELECT action_id FROM acao WHERE action_name = 'servidor' AND action_module_id = 16), 1, 1);

ALTER TABLE servidor
ADD COLUMN empresa_id INT(11) NULL;

ALTER TABLE servidor
ADD CONSTRAINT FK_empresa_servidor FOREIGN KEY (empresa_id) REFERENCES empresa(empresa_id);

ALTER TABLE file_object
ADD COLUMN server_id INT (11) NULL;

ALTER TABLE file_object
ADD CONSTRAINT FK_File_Object_Server  FOREIGN KEY (server_id) REFERENCES servidor(server_id);

ALTER TABLE servidor
ADD description VARCHAR(50) NOT NULL AFTER server_id;

ALTER TABLE  servidor
ADD COLUMN directory_monitor VARCHAR(100) NULL;

ALTER TABLE  servidor
ADD COLUMN enable INT(11) NULL;

ALTER TABLE  servidor
ADD COLUMN ENABLE INT(11) NULL DEFAULT 1;


ALTER TABLE cliente
CHANGE client_key client_key VARCHAR(50) NOT NULL;

ALTER TABLE fatura_itens 
CHANGE  item_valor item_valor DECIMAL(10,0) NOT NULL DEFAULT 0

CREATE TABLE `ci_sessions` (
  `session_id` VARCHAR(40) NOT NULL DEFAULT '0',
  `ip_address` VARCHAR(45) NOT NULL DEFAULT '0',
  `user_agent` VARCHAR(120) NOT NULL,
  `last_activity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` LONGTEXT NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

ALTER TABLE xml_file
CHANGE file_name file_name VARCHAR(255) NULL 

ALTER TABLE xml_file
CHANGE full_path full_path VARCHAR(255) NULL 

ALTER TABLE xml_file
CHANGE extension extension VARCHAR(10) NULL

ALTER TABLE xml_file
CHANGE processed processed TINYINT(4) NOT NULL DEFAULT 0
