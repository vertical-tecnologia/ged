
$(document).on('click', '.menu-explorer', function(event){
	event.preventDefault();

	if ($(this).find('i').hasClass('fa fa-caret-down') === true) {
		var file_id = $(this).data('id');
		console.log($('ul[data-parent="'+file_id+'"]'));
		$('ul[data-parent="'+file_id+'"]').remove();
		$(this).find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right');
		return true;
	}

	childrenDir($(this));
});


function childrenDir($element) {

	var file_id = $element.data('id');
	var server_id = $element.data('server');
	var li      = $element.parent('li');
	$element.find('i').removeClass('fa fa-caret-right').addClass('fa fa-caret-down');

	$.getJSON(base_url+'arquivos/subDirectory/', {file: file_id, server: server_id})
	.done(function(directories) {
		var html = '<ul data-parent="'+file_id+'" style="list-style-type: none; margin-left:-25px;">';
		$.each(directories, function(index, diretory){

			if ($('.menu-explorer[data-id='+diretory.id+']').length > 0) {
				return true;
			}

			var param = (server_id !== '') ? '/?server='+server_id : '';
			html += '<li><a data-id="'+diretory.id+'" data-server="'+server_id+'" class="menu-explorer" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a> ';
			html += '<a class="ext_disabled" href="'+base_url+'arquivos/folder/'+diretory.hash+''+param+'"><i class="fa fa-folder" aria-hidden="true"></i> '+diretory.original_name+'<b class=""></b></a></li>';
		});
		html += '</ul>';
		li.after(html);
	})
	.fail(function( jqxhr, textStatus, error ) {
		var err = textStatus + ", " + error;
		console.log( "Request Failed: " + err );
	});
}


$(document).on('click', '.editTag', function(event){
	event.preventDefault();

	$.ajax({
		cache: false,
		method: "POST",
		url: base_url + "arquivos/getTag",
		data: {id: this.id}
	}).done(function (dado) {
		$("#idTag").val(dado.label_id);
		$("#label_description").val(dado.label_description);
		$("#label_content").val(dado.label_content);
	});
});

// $(document).on('click', '.idFolder', function(event){
// 	event.preventDefault();
	
// 	var folderHash = $(this).attr('id');

// 	$.getJSON(base_url+'arquivos/permissionByDirectory/', {hash: folderHash})
// 	.done(function(permissions) {
// 		$('.ver, .detalhes, .compartilhar, .excluir').removeClass('hidden')

// 		if(permissions[0] === '0'){
// 			$('.ver').addClass('hidden');
// 		}

// 		if(permissions[1] === '0'){
// 			$('.detalhes').addClass('hidden');
// 		}

// 		if(permissions[2] === '0'){
// 			$('.compartilhar').addClass('hidden');
// 		}

// 		if(permissions[3] === '0'){
// 			$('.excluir').addClass('hidden');
// 		}
// 	})
// 	.fail(function( jqxhr, textStatus, error ) {
// 		$('.ver, .detalhes, .compartilhar, .excluir').removeClass('hidden')
// 	});
// });
