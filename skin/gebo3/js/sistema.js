var ver_contrato = function(event, contrato){
    event.preventDefault();
    var overlay = document.getElementById('loading_data');
    overlay.style.display="block";
    overlay.style.position="fixed";
    $.ajax({

        url: contrato.getAttribute('href'),
        type: 'GET',
        dataType: 'html',
        success: function(retorno) {

        var obj = '  <div id="e-document"><span id="doc-close"><i class="icon-remove"></i>fechar</span><span id="doc-print"><i class="icon-print"></i>imprimir</span>'
                     +retorno
                     +'</div>';
        overlay.innerHTML=obj;

            var btnprint = document.getElementById('doc-print');

            btnprint.addEventListener('click', function(){
                window.open(contrato.getAttribute('href'));
            });

            var btnclose = document.getElementById('doc-close');

            btnclose.addEventListener('click', function(){
                overlay.style.display="none";
                overlay.innerHTML='';
            });
            var altura = $( document ).height() - ($( document ).height() / 4 );
            $('.contrato').height(altura);
        }

    });

}

function volume_armazenado() {

    $.ajax({

        url: $('#url').val()+'cliente/quantidade_utilizada',
        type: 'POST',
        dataType: 'html',
        data: {},
        success: function(vet) {

            //$('#qtd_utilizada').attr('title', vet);
            if ($('#qtd_utilizada')) $('#qtd_utilizada').html(vet);

        }

    });

}

function salvar_permissao_diretorio(obj) {

    //ALTERA VALOR
    obj.checked == false ? obj.value = 0 : obj.value = 1;

    var myRadio    = $('input[name=usuario_diretorio]');
    var id_usuario = myRadio.filter(':checked').attr('id').replace('u_', '');

    var post_data = {
        id_diretorio               : $('#id_diretorio').val(),
        id_usuario_diretorio       : $('#id_usuario_diretorio').val(),
        id_usuario                 : id_usuario,
        ler_usuario_diretorio      : $('#ler_usuario_diretorio').val(),
        escrever_usuario_diretorio : $('#escrever_usuario_diretorio').val(),
        esconder_usuario_diretorio : $('#esconder_usuario_diretorio').val(),
        bloquear_usuario_diretorio : $('#bloquear_usuario_diretorio').val()
    };

    console.log(post_data);

    $.ajax({

        url: $('#url').val()+'usuario_diretorio/alterar/',
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            }

        }

    });

}

function salvar_usuario_permissao() {

    var post_data = {
        id_usuario : $('#id_usuario').val(),
        id_telas   : $('#id_telas').val()
    };

    $.ajax({

        url: $('#url').val()+'usuario_tela/inserir',
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

            }

        }

    });

}

function salvar_usuario_tipo_permissao() {

    var post_data = {
        id_usuario_tipo : $('#id_usuario_tipo').val(),
        id_telas        : $('#id_telas').val()
    };

    $.ajax({

        url: $('#url').val()+'tela_usuario_tipo/inserir',
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

            }

        }

    });

}

function busca_cep(obj) {

    val = obj.value.replace(/\D/g, '');

    if (val.length == 8) {

        exibir_loading(true);
        $.ajax({

            url: $('#url').val()+'cep',
            type: 'POST',
            dataType: 'json',
            data: {cep : val},
            success: function(vet) {

                var dados = vet. dados;

                if (vet.error == 1) {

                    $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });
                    exibir_loading(false);

                } else {

                   $('#logradouro_endereco').val(dados.tipo_cep + ' ' + dados.logradouro_cep);
                   $('#bairro_endereco').val(dados.bairro_ini_cep);
                   $('#cidade_endereco').val(dados.cidade_cep);
                   $('#uf_endereco').val(dados.uf_cep);
                   $('#cod_ibge_endereco').val(dados.cod_ibge_cep);

                   $('#numero_endereco').focus();
                   exibir_loading(false);

                }

            }

        });

    }

}

function exibir_loading(show) {

    //var loading = $('#loading_data');
    var loading = document.getElementById('loading_data');

    if (show == true) {
        loading.style.display = 'block';
    } else {
        loading.style.display = 'none';
    }

}

exibir_loading(false);

function salvar_usuario_contato() {

    var post_data = {
        id_usuario      : $('#id_usuario').val(),
        id_contato_tipo : $('#id_contato_tipo').val(),
        desc_contato    : $('#desc_contato').val()
    };

    $.ajax({
        url: $('#url').val()+'usuario_contato/inserir',
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                cancelar_contato();
                buscar_grid('usuario', 'contato', $('#id_usuario').val());

            }

        }

    });

}

function cancelar_contato() {

    $('#id_contato').val('');
    $('#id_contato_tipo').val('');
    $('#desc_contato').val('');

}

function salvar_cliente_contato() {

    var aux_request = '';

    var post_data = {
        id_contato      : $('#id_contato').val(),
        id_cliente      : $('#id_cliente').val(),
        id_contato_tipo : $('#id_contato_tipo').val(),
        desc_contato    : $('#desc_contato').val()
    };

    if ($('#id_contato').val() != '') {
        aux_request = $('#url').val()+'contato/alterar';
    } else {
        aux_request = $('#url').val()+'cliente_contato/inserir';
    }

    $.ajax({

        url: aux_request,
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                cancelar_contato();
                buscar_grid('cliente', 'contato', $('#id_cliente').val());

            }

        }

    });

}

function salvar_usuario_endereco() {

    var post_data = {
        id_usuario           : $('#id_usuario').val(),
        complemento_endereco : $('#complemento_endereco').val(),
        logradouro_endereco  : $('#logradouro_endereco').val(),
        cod_ibge_endereco    : $('#cod_ibge_endereco').val(),
        bairro_endereco      : $('#bairro_endereco').val(),
        cidade_endereco      : $('#cidade_endereco').val(),
        uf_endereco          : $('#uf_endereco').val(),
        numero_endereco      : $('#numero_endereco').val(),
        cep_endereco         : $('#cep_endereco').val()
    };

    $.ajax({

        url: $('#url').val()+'usuario_endereco/inserir',
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                cancelar_endereco();
                buscar_grid('usuario', 'endereco');

            }

        }

    });

}

function salvar_cliente_endereco() {

    var aux_request = '';

    var post_data = {
        id_cliente           : $('#id_cliente').val(),
        id_endereco          : $('#id_endereco').val(),
        complemento_endereco : $('#complemento_endereco').val(),
        logradouro_endereco  : $('#logradouro_endereco').val(),
        cod_ibge_endereco    : $('#cod_ibge_endereco').val(),
        bairro_endereco      : $('#bairro_endereco').val(),
        cidade_endereco      : $('#cidade_endereco').val(),
        uf_endereco          : $('#uf_endereco').val(),
        numero_endereco      : $('#numero_endereco').val(),
        cep_endereco         : $('#cep_endereco').val()
    };

    if ($('#id_endereco').val() != '') {
        aux_request = $('#url').val()+'endereco/alterar';
    } else {
        aux_request = $('#url').val()+'cliente_endereco/inserir';
    }

    $.ajax({

        url: aux_request,
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                cancelar_endereco();
                buscar_grid('cliente', 'endereco', $('#id_cliente').val());

            }

        }

    });

}

function excluir_usuario_endereco(endereco) {

    var post_data = {
        endereco  : endereco
    };

    $.ajax({

        url: $('#url').val()+'cliente_endereco/excluir',
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                buscar_grid('usuario', 'endereco');

            }

        }

    });

}

function cancelar_endereco() {

    $('#id_endereco').val('');
    $('#complemento_endereco').val('');
    $('#logradouro_endereco').val('');
    $('#cod_ibge_endereco').val('');
    $('#bairro_endereco').val('');
    $('#cidade_endereco').val('');
    $('#uf_endereco').val('');
    $('#numero_endereco').val('');
    $('#cep_endereco').val('');

}

function salvar_cliente_servico() {

    var aux_request = '';

    var post_data = {
        id_cliente : $('#id_cliente').val(),
        id_servico : $('#id_servico').val(),
        id_cliente_servico : $('#id_cliente_servico').val(),
        data_ini_cliente_servico : $('#data_ini_cliente_servico ').val(),
        data_fim_cliente_servico : $('#data_fim_cliente_servico ').val()
    };

    if ($('#id_cliente_servico').val() != '') {
        aux_request = $('#url').val()+'cliente_servico/alterar';
    } else {
        aux_request = $('#url').val()+'cliente_servico/inserir';
    }

    $.ajax({

        url: aux_request,
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                cancelar_cliente_servico();
                buscar_grid('cliente', 'servico', $('#id_cliente').val());

            }

        }

    });

}

function cancelar_cliente_servico() {

    $('#id_servico').val('');
    $('#id_cliente_servico').val('');
    $('#data_ini_cliente_servico').val('');
    $('#data_fim_cliente_servico').val('');

}

function cancelar_grid(form) {

    for (var i = 0; i< form.length; i++) {

        if(form[i].type == 'checkbox') {

            form[i].checked = false;
            $('#tb-'+form[i].getAttribute('name')).toggleButtons('setState', false);
            form[i].value ='0';

        } else {

            form[i].value = '';

        }

    }

    for (var i = 0; i < form.length; i++) {

        if (form[i].type != 'hidden' && form[i].readOnly != 'readonly' && !form[i].readOnly  && !form[i].disabled) {

            form[i].focus();
            break;

        }

    }

}

function buscar_grid(nome, aba, id) {

    var nome_aba = aba.split('_');

    if (nome_aba[1] != undefined && nome_aba[1] != null) {
        aba = nome_aba[1];
    }

    $.ajax({

        url: $('#url').val() + nome + '/aba_'+aba+'/'+id,
        type: 'POST',
        dataType: 'html',
        success: function(grid) {

            $('#dados_aba_'+aba).html(grid, function(){
                initDataTables();
            });

        }

    });

}

function editar_grid(nome, id) {

    $.ajax({

        url: $('#url').val() + nome + '/editar/'+id,
        type: 'POST',
        dataType: 'json',
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                for (valor in vet) {

                    if (valor == nome) {

                        for (col in vet[valor][0]) {

                            if ($('#'+col)) {

                                if ($('#'+col).attr('type') == 'checkbox') {

                                    if (document.getElementById(col).parentNode.parentNode.cssClass = 'toggle-buttons') {

                                        if (vet[valor][0][col] == 1){

                                            $('#tb-'+col).toggleButtons('setState', true);


                                        } else {

                                            $('#tb-'+col).toggleButtons('setState', false);
                                        }
                                    }

                                    $('#'+col).attr('checked', vet[valor][0][col] == 1  ? 'checked' : '');
                                    $('#'+col).val(vet[valor][0][col] != null ? vet[valor][0][col] : '');

                                } else {

                                     $('#'+col).val(vet[valor][0][col] != null ? vet[valor][0][col] : '');

                                }

                            }

                        }

                    }

                }

            }

        }

    });

}

function excluir_grid(nome, id) {

    var classe = $('#classe').val();

    smoke.confirm('Você tem certeza que deseja excluir este registro?<br /><br />',function(e) {

        if (e) {

            $.ajax({

                url: $('#url').val() + nome + '/excluir',
                type: 'POST',
                dataType: 'json',
                data: {id_tabela : id},
                success: function(vet) {

                    if (vet.error == 1) {

                        $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

                    } else {

                        $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                        buscar_grid(classe, nome, $('#id_'+classe).val());

                    }

                }

            });

        }

    }, {ok:"Excluir", cancel:"Cancelar"});

}

var tab_forms = document.getElementsByClassName('tab_form');

for (var i = 0; i < tab_forms.length; i++) {

    tab_forms[i].addEventListener('submit', function(e){
        e.preventDefault();
    });

}

function salvar_grid(form, nome) {

    var paramId     = document.getElementById(form.getAttribute('data-param-id'));
    var table_key   = document.getElementById(form.getAttribute('data-param-parent'));
    var url         = document.getElementById('url');
    var aux_request = url.value + (paramId == null || paramId.value == null || paramId.value == 0 ?  form.getAttribute('action') : form.getAttribute('data-action'));
    var _id         = (table_key != undefined && table_key != null ? ' "' + table_key.getAttribute('name') + '" : "' + table_key.value + '", ' : '');
    var post_data   = JSON.parse('{' + _id + serialize(form) + '}');

    $.ajax({

        url: aux_request,
        type: 'POST',
        dataType: 'json',
        data: post_data,
        success: function(vet) {

            if (vet.error == 1) {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

            } else {

                $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                buscar_grid($('#classe').val(), nome, table_key.value);
                cancelar_grid(form);

            }

        }

    });

    return false;

}

var btnImprimir   = document.getElementById('btn-print-action');
var btnVisualizar = document.getElementById('btn-view-action');

if (btnImprimir != undefined && btnImprimir != null) {

    btnImprimir.addEventListener('click', function() {

        imprimir();

    });

}

if (btnVisualizar != undefined && btnVisualizar != null) {

    btnVisualizar.addEventListener('click', function(){

        visualizar();

    });

}

function imprimir() {

    var count = 0;

    for (var i = 0; i < document.getElementById("total_reg").value; i++) {

        if (document.getElementById("id_tabela_"+i).checked == true) {
            count++;
        }

    }

    if (count > 0) {

        var dados = serialize(document.getElementById('frm_grid'));

        smoke.alert('Preparando impressão... aguarde: ');
        document.getElementById('maincontainer').style.display = 'none';

    } else {

        var msg = "Para imprimir  você deve selecionar algum registro!<br /><br /><b>Nenhum registro Selecionado!</b><br /><br />";
        smoke.alert(msg, {ok:"Fechar"});

    }

}

function visualizar() {

    var count = 0;

    for (var i = 0; i < document.getElementById("total_reg").value; i++) {

        if (document.getElementById("id_tabela_"+i).checked == true) {
            count++;
        }

    }

    if (count > 0) {

       smoke.alert('Preparando a visualização... aguarde: ');

       var frm_data = getCheckboxValues(document.getElementById('frm_grid'));
       var post_data = JSON.parse('{ "id_tabela" : "'+ frm_data  + '"}');

        $.ajax({
            url: $('#url').val()+$('#classe').val()+'/visualizar',
            type: 'POST',
            data: post_data,
            success: function(vet) {

                if (vet.error == 1) {

                    $.sticky(vet.msg, {autoclose : 5000, position: "top-center", type: "st-error" });

                } else {

                    customPrintTable(vet);

                    smokeObj = document.getElementsByClassName('smoke-alert');
                    if(smokeObj != null && smokeObj != null)
                    smoke.destroy(null, smokeObj[0].getAttribute('id').replace('smoke-out-', ''));

                }

            }

        });

    } else {

        var msg = "Para visualizar  você deve selecionar algum registro!<br /><br /><b>Nenhum registro Selecionado!</b><br /><br />";
        smoke.alert(msg, {ok:"Fechar"});

    }

}


function getCheckboxValues(form) {

    var values = [];

    for (var i=0; i<form.elements.length; i++) {

        if (form.elements[i].checked && form.elements[i].value != 'on') {

            values.push(form.elements[i].value);

        }

    }

    return values;

}

function serialize (form) {

    if (!form || form.nodeName !== "FORM") {
        return;
    }

    var i, j, q = [];

    for (i = form.elements.length - 1; i >= 0; i = i - 1) {

        if (form.elements[i].name === '') {
                continue;
        }

        switch (form.elements[i].nodeName) {

            case 'INPUT':

                switch (form.elements[i].type) {

                    case 'text':
                    case 'number':
                    case 'hidden':
                    case 'password':
                    case 'button':
                    case 'reset':
                    case 'submit':
                        q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
                        break;

                    case 'checkbox':
                    case 'radio':

                        if (form.elements[i].checked) {

                            q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');

                        }

                        break;

                }

                break;

            case 'file':
                break;

            case 'TEXTAREA':
                q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
                break;

            case 'SELECT':

                switch (form.elements[i].type) {

                    case 'select-one':
                        q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
                        break;

                    case 'select-multiple':

                        for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {

                            if (form.elements[i].options[j].selected) {
                                q.push('"' + form.elements[i].name + '":"' + form.elements[i].options[j].value + '"');
                            }

                        }

                        break;

                }

                break;

            case 'BUTTON':

                switch (form.elements[i].type) {

                    case 'reset':
                    case 'submit':
                    case 'button':
                        q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
                        break;

                }

                break;

            }

    }

    return q.toString();

}

function request_focus() {

    var inputElements = document.getElementsByTagName('input');

    for (var i = 0; i < inputElements.length; i++) {

        if (inputElements[i].type != 'hidden' && inputElements[i].readOnly != 'readonly' && !inputElements[i].readOnly  && !inputElements[i].disabled) {

            inputElements[i].focus();
            break;

        }

    }

}

var sortColumns = function () {

    var cols = new Array();
    var tableColumns  = document.getElementsByClassName('col-sortable');

    for (var i = 0; i < tableColumns.length; i++) {

        var content = tableColumns[i].textContent.toLowerCase();
        var res = content.match('^.*dat[a|as|e|es].*$');

        if (res != null) {

            cols.push(i+1);

        }

    }

    return cols;

}

initDataTables = function() {

    var enablePagination = ($('.mediaTable').attr('data-enable-pagination') != undefined ? $('.mediaTable').attr('data-enable-pagination') : false);

    $("table[id^='grid_aba_'], #grid").each(function() {

        //$('.mediaTable').mediaTable({menuTitle: 'Colunas'});

        $(this).dataTable({
            "iDisplayLength" : 4,
            "bDestroy"       : true,
            "sDom"           : '<"dt-top-row pull-right"T><"clear">rt<"dt-row dt-bottom-row"p>',
            "oTableTools"    : {
                "sSwfPath"   : $('#url').val()+"/skin/gebo/swf/copy_csv_xls_pdf.swf",
                "aButtons"   : [
                    {
                        "sExtends"     : "print",
                        "sButtonClass" : "btn",
                        "sButtonText"  : "Visualizar Impressão",
                    },
                    {
                        "sExtends"     : "copy",
                        "sButtonClass" : "btn",
                        "sButtonText"  : "Copiar",
                    },
                    {
                        "sExtends"     : "csv",
                        "sButtonClass" : "btn",
                        "sButtonText"  : " CSV",
                    },
                    {
                        "sExtends"     : "xls",
                        "sButtonClass" : "btn",
                        "sButtonText"  : "Excel",
                    },
                    {
                        "sExtends"        : "pdf",
                        "sButtonClass"    : "btn",
                        "sPdfOrientation" : "landscape",
                    }
                ]

            },
            "bPaginate": enablePagination,
            "oLanguage": {
                "sEmptyTable"  : ' ',
                "sZeroRecords" : ' ',
                "oPaginate"    : {
                    "sFirst"    : "Primeira",
                    "sPrevious" : "Anterior",
                    "sNext"     : "Próxima",
                    "sLast"     : "Última"
                }
            },
            "sPaginationType" : "bootstrap",
            "aoColumnDefs"    : [
                {
                    "bSortable" : false,
                    "aTargets"  : [ "no-sort" ]
                },
                {
                    "sType"    : "title-string",
                    "aTargets" : sortColumns()
                }
            ]

        });

    });

};

printing = true;

customPrintTable = function(mtable) {

    printScreen = document.getElementById('print_background');
    printScreen.childNodes[0].innerHTML=mtable;
    printScreen.style.display='block';
    var h;

    if (printScreen.childNodes[0].offsetHeight) {

        h = printScreen.childNodes[0].offsetHeight + 100;
        printScreen.style.height= h + 'px';

    } else if(printScreen.childNodes[0].pixelHeight) {

        h = printScreen.childNodes[0].style.pixelHeight + 100;
        printScreen.style.pixelHeight=h;

    }

};

hidePrintTable = function() {

    printScreen = document.getElementById('print_background');

    if (printScreen != null && printScreen != undefined) {

        printScreen.childNodes[0].innerHTML = '';
        printScreen.style.display = 'none';

    }

}

document.onkeydown = function(event) {

    var tecla = event.keyCode ? event.keyCode : event.which ? event.which : void 0;

    if (tecla == 27 && printing == true) {

        document.getElementById('maincontainer').style.display='block';
        hidePrintTable();
        printing = false;

    }

};

/**
* @function: getBytesWithUnit()
* @purpose: Converts bytes to the most simplified unit.
* @param: (number) bytes, the amount of bytes
* @returns: (string)
*/
var getBytesWithUnit = function( bytes ) {

    if (isNaN(bytes)) {
        return;
    }

    var units = [ ' bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB' ];
    var amountOf2s = Math.floor( Math.log( +bytes )/Math.log(2) );

    if (amountOf2s < 1) {
        amountOf2s = 0;
    }

    var i = Math.floor( amountOf2s / 10 );
    bytes = +bytes / Math.pow( 2, 10*i );

    // Rounds to 3 decimals places.
    if (bytes.toString().length > bytes.toFixed(3).toString().length) {
        bytes = bytes.toFixed(3);
    }

    return bytes + units[i];

};

function ObjectLength_Modern( object ) {
    return Object.keys(object).length;
}

function ObjectLength_Legacy( object ) {

    var length = 0;

    for (var key in object) {

        if (object.hasOwnProperty(key)) {
            ++length;
        }

    }

    return length;

}

var ObjectLength = Object.keys ? ObjectLength_Modern : ObjectLength_Legacy;

(function() {

    var blockFinishButton = function() {

        if ($('#termos').attr('checked') == 'checked')
            $('.finish').removeAttr('disabled');
        else
            $('.finish').attr('disabled','disabled');

    }

    blockFinishButton();

    $('#termos').change(function() {
        blockFinishButton();
    });

    $("[data-slider]")
        .each(function () {
            var input = $(this);
            $("<span>")
                .addClass("output")
                .insertAfter($(this));
        })
        .bind("slider:ready slider:changed", function (event, data) {

            var valor_servico;
            $.each(servicos, function(index, value) {

                if (servicos[index].qtd_servico == data.value.toFixed(2)) {

                    $('#id_servico').val(servicos[index].id_servico);
                    valor_servico = servicos[index].valor_servico;

                }

            });

            $('#servico').val(data.value);
            $('#val_id_servico').html(getBytesWithUnit(1024 * 1024 * data.value));
            $('#val_valor_id_servico').html(' Valor: R$ ' + valor_servico);

        }
    );

    initDataTables();
    request_focus();

    if ($('.normal-toggle-button')) {

        $('.normal-toggle-button').toggleButtons({
             label: {
                 enabled  : "SIM",
                 disabled : "NÃO"
             }
        });

    }

})();
