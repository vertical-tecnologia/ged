'use strict';

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}

function is_touch_device() {
    return !!('ontouchstart' in window);
}
var base_url = document.getElementById('url').value;
var the_div = document.getElementsByTagName('body')[0];
var menuState = false;
var contextMenuClassName = "context-menu";
var menu = document.getElementById("context-menu");
var contextMenuActive = "context-menu--active";
var menuActiveClass = "context-menu--active"
var clickCoordsX;
var clickCoordsY;
var menuWidth;
var menuHeight;
var windowWidth;
var windowHeight;


var clickCount = 0,
    mouseMove = function () {
        clear();
    },
    clear = function () {
        clickCount = 0;
        the_div.removeEventListener('mousemove', mouseMove);
        clearTimeout(singleClickTimer);
    },
    singleClickTimer;

the_div.addEventListener('contextmenu', function (e) {
    var el = e.srcElement || e.target;
    el = checkClick(el, 'el') || checkClick(el, 'file-action');

    if(el == false)return;


    elementsSelect(null, el);
    e.preventDefault();

    var selected = document.querySelectorAll('div.el.active');
    if (selected.length > 1 && menuState == true){
        menuState = false;
        menu.classList.remove(menuActiveClass);
        return false;
    }


    if(el.classList.contains('el') && menuState == false && selected.length==1){

        if(menuState == false){
            menuState = true;
            positionMenu(el, menu);
            menu.classList.add(menuActiveClass);

        }
    }

}, false);

var getPosition = function () {
    var positionX = 0;
    var positionY = 0;

    var e = window.event;

    if (e.pageX || e.pageY) {
        positionX = e.pageX;
        positionY = e.pageY;
    } else if (e.clientX || e.clientY) {
        positionX = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        positionY = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }


    return {
        x: positionX,
        y: positionY
    }
};

var toggleMenuOff =  function (menu) {
    if (menuState == true) {
        menuState = false;
        menu.classList.remove(menuActiveClass);
    }
};

var positionMenu = function (e, menu) {


    var clickCoords = getPosition();

    clickCoordsX = clickCoords.x;
    clickCoordsY = clickCoords.y;

    menuWidth = menu.offsetWidth + 4;
    menuHeight = menu.offsetHeight + 4;

    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;

    if ((windowWidth - clickCoordsX) < menuWidth) {
        menu.style.left = windowWidth - menuWidth + "px";
    } else {
        menu.style.left = clickCoordsX + "px";
    }

    if ((windowHeight - clickCoordsY) < menuHeight) {
        menu.style.top = windowHeight - menuHeight + "px";
    } else {
        menu.style.top = clickCoordsY + "px";
    }

};

the_div.addEventListener('click', function (e) {
    var el = e.srcElement || e.target;

    el = checkClick(el, 'el') || checkClick(el, 'file-action') || checkClick(el, 'fmgrid') || checkClick(el, 'context-menu__link');


    if(el != false && menuState == true && checkClick(el, 'file-action') == false)
        toggleMenuOff(menu);

    var dataCallback;
    clickCount++;

    if (clickCount === 1) {
        
        if (el != false) {
            dataCallback = el.getAttribute('data-bind-click');

            if (dataCallback != null && dataCallback != undefined) {
                e.preventDefault();
                permissionsFile(el);
            }
        } else {
            toggleMenuOff(menu);
        }

        singleClickTimer = setTimeout(function () {

            if (typeof window[dataCallback] == 'function')
                window[dataCallback](el.dataset, el);
            else
                toggleMenuOff(menu);

            clickCount = 0;
            clear();
        }, 250);

    } else if (clickCount === 2) {
        clear();
    }
}, false);


var createPreloader =  function(element){

    if(element == null && element == undefined)
        element = document.getElementById('maincontainer');

    var preLoader = document.createElement('div');
    var loader = '<div class="preloader" id="preloader"><div class="sk-spinner sk-spinner-rotating-plane"></div></div>';
    preLoader.innerHTML = loader;
    element.appendChild(preLoader);
};
var hidePreloader = function() {
    var node = document.getElementById('preloader');
    node.parentNode.removeChild(node);
};

var unSelectAll = function(){
    var elements = document.querySelectorAll('.el.active');
    for(var i=0;i<elements.length;i++){
        elements[i].classList.remove('active');
        closeActtionBar();
    }
}

var elementsSelect = function (dataset, el) {
    console.log(elementsSelect);
    unSelectAll();
    if (el.classList.contains('active')) {
        el.classList.remove('active');
    } else {
        el.classList.add('active');
    }

    var menu = document.getElementsByClassName(' file-action-menu');
    for (var i = 0; i < menu.length; i++) {
        menu[i].classList.add('hide');
    }

    var selected = document.querySelectorAll('div.el.active');
    var actionBar = document.querySelector('.navbar-fixed-bottom');

    if (selected.length > 0) {
        var menu;
        if (selected.length == 1) {

            if (selected[0].dataset.type == 1)
                menu = document.getElementById('single-file');
            else
                menu = document.getElementById('single-folder');

            menu.classList.remove('hide');
        } else if (selected.length > 1) {

        }

        if (actionBar != undefined && actionBar != null) {
            actionBar.classList.remove('hide');
        }
    } else {
        if (actionBar != undefined && actionBar != null) {
            actionBar.classList.add('hide');
        }
    }
}

var closeActtionBar = function () {
    var menu = document.getElementsByClassName(' file-action-menu');
    for (var i = 0; i < menu.length; i++) {
        menu[i].classList.add('hide');
    }

    var actionBar = document.querySelector('.navbar-fixed-bottom');
    if (actionBar != undefined && actionBar != null) {
        actionBar.classList.add('hide');
    }
}

var checkClick = function (el, className) {

    if (el.classList.contains(className))
        return el;

    while (el = el.parentNode) {

        if (el.classList != undefined && el.classList != null){
            if(el.classList.contains(className)){
                return el;
            }
        }
            else return false;
    }

    return false;
};

var preview = function(params, el){

    createPreloader();

    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    var linkGenerator = el.dataset.href + selected[0].dataset.id;

    var types = [
        '.mpg',
        '.mpga',
        '.gif',
        '.png',
        '.jpg',
        '.jpeg',
        '.pdf',
    ];

    $.ajax({
        url: linkGenerator,
        dataType: 'JSON',
        method: 'GET',
        success: function (response) {

            var body = document.getElementsByTagName('body');
            // body[0].style.overflow='hidden';

            var uri = el.getAttribute('href') +  response.key;
            if(types.contains(selected[0].dataset.extension))
                url = base_url + 'arquivos/preview/' + response.key;
            else
                var url = "https://docs.google.com/gview?url=" + uri + '/?&embedded=true';

            var element = document.createElement('div');

            element.innerHTML = '<div class="dialog dialog-close">'+
                '<div class="dialog-overlay dialog-black"></div>' +
                '<div class="acbar"><button class="close-preview" onclick="destroyPanel()"></button></div>' +
                '<iframe frameborder="0" src="'+url+'" style="width:100%; height:100%; position:relative;z-index:9050;top:51px; left:0;"></iframe>' +
                '</div>' +
                '</div>';
            var mainContainer = document.getElementsByTagName('body')[0];

            mainContainer.appendChild(element);

            hidePreloader();
        }
    });

};


var share = function (params, el) {
    createPreloader();
    var selected = document.querySelectorAll('div.el.active');

    console.log(selected);
    if (selected.length != 1)
        return false;

    var url = base_url + 'arquivos/share/' + selected[0].dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            gedchosen.init();
            hidePreloader();
        }
    });

};

var shareThis = function(el){

    var users = $("#sh_user_id");
    var groups = $("#sh_group_id");
    var email = $("#sh_email");
    createPreloader();

    var url = base_url + 'arquivos/setshare/' + el.dataset.id;

    $.ajax({
        url: url,
        data :{
            user_id : users.val(),
            group_id : groups.val(),
            emails :  email.val(),
            object : el.dataset.id
        },
        method: 'POST',
        success: function (response) {
            var msg = document.getElementById('share-msg');

            if(response.error){
                msg.classList.add('label-danger');

            } else {

                users.attr('disabled', true).trigger("liszt:updated");
                groups.attr('disabled', true).trigger("liszt:updated");

                var item = document.getElementById("_"+el.dataset.id);
                msg.classList.add('label-success');
                var close = document.querySelector('#bt-label');
                if (close != null && close != undefined)
                    close.innerHTML = 'Fechar';
                item.parentNode.removeChild(item);

            }

            msg.innerHTML=response.msg;

            hidePreloader();
        }
    });

}

var catalog = function(params, el){

    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    var url = el.getAttribute('href') + selected[0].dataset.id;
    window.location.href=url;

}

var gedchosen = {
    init: function(){
        $(".chosen-select").chosen({
            allow_single_deselect: true,
            disable_search_threshold: 5,
            width: "95%"
        });

        this.update();


        $(".chosen-select").on('change' ,function(evt, params){
            var el = evt.currentTarget || evt.srcElement;
            try{
                var callbacks = el.getAttribute('data-callback').split('|');
                if(callbacks != undefined){
                    if(typeof window[callbacks[0]][callbacks[1]] == 'function')
                        window[callbacks[0]][callbacks[1]](evt, params)
                }
            } catch(e){

            }

        });

    },
    update : function(){
        $('.chosen-select').on('chosen:updated', function () {
            if ($('.chosen-select').attr('readonly')) {
                var wasDisabled = $('.chosen-select').is(':disabled');

                $('.chosen-select').attr('disabled', 'disabled');
                $('.chosen-select').data('chosen').search_field_disabled();

                if (wasDisabled) {
                    $('.chosen-select').attr('disabled', 'disabled');
                } else {
                    $('.chosen-select').removeAttr('disabled');
                }
            }
        });

        $(".chosen-select").trigger('chosen:updated');

    }
};

var detail = function (params, el) {

    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    createPreloader();

    var url = base_url + 'arquivos/detail/' + selected[0].dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            hidePreloader();
        }
    });

}

var trash = function (params, el) {
    createPreloader();
    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    var url = base_url + 'arquivos/trash/' + selected[0].dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            hidePreloader();
        }
    });

};

var sendToTrash = function (element) {
    var el = element;

    var url = base_url + 'arquivos/sendtotrash/' + el.dataset.id;
    var target = document.getElementById(el.dataset.target);


    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {

            if (response.error != null && response.error != undefined)
                target.innerHTML = '<h2>' + response.error + '</h2>';
            else {
                target.innerHTML = '<div class="col-sm-12 "><h2>' + response.success + '</h2></div>';
                var item = document.getElementById(el.dataset.id);

                item.parentNode.removeChild(item);

                closeActtionBar();
            }

            var close = document.querySelector('#bt-label');
            if (close != null && close != undefined)
                close.innerHTML = 'Fechar';
            el.parentNode.removeChild(el);


        }
    });

}


var recover = function(evt, element){
    createPreloader();
    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    var url = base_url + 'arquivos/recover/' + selected[0].dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            hidePreloader();
        }
    });
}

var recovery = function(el){
    createPreloader();

    var url = base_url + 'arquivos/recovery/' + el.dataset.id;

    $.ajax({
        url: url,
        data :{
            object : el.dataset.id
        },
        method: 'POST',
        success: function (response) {
            var msg = document.getElementById('recover-msg');

            if(response.error){
                msg.classList.add('label-danger');
                msg.innerHTML=response.error;

            } else {

                var item = document.getElementById(el.dataset.id);
                var close = document.querySelector('#bt-label');
                if (close != null && close != undefined)
                    close.innerHTML = 'Fechar';
                item.parentNode.removeChild(item);
                el.parentNode.removeChild(el);
                msg.innerHTML=response.success;
                closeActtionBar();
            }


            hidePreloader();
        }
    });

}

var emptyTrash = function(element){
    event.preventDefault();
    var url = element.getAttribute('href');
    createPreloader();
    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            hidePreloader();
        }
    });
}

var cleanTrash = function(element){
    event.preventDefault();
    var url = element.dataset.href;
    createPreloader();

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var msg = document.getElementById('recover-msg');
            if(response.error){
                msg.classList.add('label-danger');
                msg.innerHTML=response.error;
            } else {
                msg.classList.add('label-success');
                msg.innerHTML=response.success;
                var itens = document.querySelectorAll('.el');
                for(var i=0; i<itens.length;i++){
                    var item = itens[i]; item.parentNode.parentNode.removeChild(item.parentNode);
                }

                var btnEmpty = document.getElementById('btn-empty');
                btnEmpty.parentNode.removeChild(btnEmpty);
                var close = document.querySelector('#bt-label');
                if (close != null && close != undefined)
                    close.innerHTML = 'Fechar';
            }

            hidePreloader();
        }
    });
}

var trash_folder = function(evt, element){
    createPreloader();
    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    var url = base_url + 'arquivos/trash_folder/' + selected[0].dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            hidePreloader();
        }
    });

}

var sendFolderToTrash = function(element){

    createPreloader();

    var url = base_url + 'arquivos/send_trash_folder/' + element.dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var msg = document.getElementById('recover-msg');
            if(response.error){
                msg.classList.add('label-danger');
                msg.innerHTML=response.error;
            } else {
                msg.classList.add('label-success');
                msg.innerHTML=response.success;

                var item = document.getElementById(element  .dataset.id);
                item.parentNode.removeChild(item);

                var close = document.querySelector('#bt-label');
                if (close != null && close != undefined)
                    close.innerHTML = 'Fechar';

                var close = document.querySelector('#details');
                if (close != null && close != undefined)
                    close.parentNode.removeChild(close);

                element.parentNode.removeChild(element);

                msg.innerHTML=response.success;
                closeActtionBar();
            }

            hidePreloader();
        }
    });

}

var download = function(evt, element){
    createPreloader();
    var selected = document.querySelectorAll('div.el.active');
    if (selected.length != 1)
        return false;

    var url = base_url + 'arquivos/download/' + selected[0].dataset.id;

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            var element = document.createElement('div');
            element.innerHTML = response;
            var mainContainer = document.getElementsByTagName('body')[0];
            mainContainer.appendChild(element);
            hidePreloader();
        }
    });
}

var destroyPanel = function (selector) {
    var selector = selector != null ? selector : 'dialog'
    var dialog = document.getElementsByClassName(selector);

    var body = document.getElementsByTagName('body');
    // body[0].style.overflow='hidden';

    for (var i = 0; i < dialog.length; i++) {
        dialog[i].parentNode.removeChild(dialog[i]);
    }
};

window.addEventListener('mouseup', function () {
    the_div.removeEventListener('mousemove', mouseMove);
});

window.addEventListener('keyup', function (e) {
    if(e.which == 27){

        try{
            destroyPanel();
        } catch (e){

        }
    }
});

var gedsidebar = {
    init: function() {
        var $body = $('body'),
            $sidebar_switch = $('.sidebar_switch');

        // sidebar onload state
        if($(window).width() > 979){
            if(!$body.hasClass('sidebar_hidden')) {
                if( $.cookie('gebo_sidebar') == "hidden") {
                    $body.addClass('sidebar_hidden');
                    $sidebar_switch.toggleClass('on_switch off_switch').attr('title','Show Sidebar').attr('data-original-title', "Show Sidebar");
                }
            } else {
                $sidebar_switch.toggleClass('on_switch off_switch').attr('title','Show Sidebar').attr('data-original-title', "Show Sidebar");
            }
        } else {
            $body.addClass('sidebar_hidden');
            $sidebar_switch.removeClass('on_switch').addClass('off_switch');
        }

        gedsidebar.info_box();

        //* sidebar visibility switch
        $sidebar_switch.click(function(){
            $sidebar_switch.removeClass('on_switch off_switch');
            if( $body.hasClass('sidebar_hidden') ) {
                $.cookie('gebo_sidebar', null);
                $body.removeClass('sidebar_hidden');
                $sidebar_switch.addClass('on_switch').show();
                $sidebar_switch.attr( 'title', "Hide Sidebar").attr('data-original-title', "Hide Sidebar");
            } else {
                $.cookie('gebo_sidebar', 'hidden');
                $body.addClass('sidebar_hidden');
                $sidebar_switch.addClass('off_switch');
                $sidebar_switch.attr( 'title', "Show Sidebar" ).attr('data-original-title', "Show Sidebar");
            }
            gedsidebar.info_box();
            //gedsidebar.update_scroll();
            $(window).resize();
        });
        //* prevent accordion link click
        $('.sidebar .accordion-toggle').click(function(e){e.preventDefault()});

        $(window).on("debouncedresize", function() {
            gedsidebar.scrollbar();
        });

    },
    info_box: function(){
        var s_box = $('.sidebar_info');
        var s_box_height = s_box.actual('height');
        s_box.css({
            'height'        : s_box_height
        });
        $('.push').height(s_box_height);
        $('.sidebar_inner').css({
            'margin-bottom' : '-'+s_box_height+'px',
            'min-height'    : '100%'
        });
    },
    make_active: function() {
        var thisAccordion = $('#side_accordion');
        thisAccordion.find('.panel-heading').removeClass('sdb_h_active');
        var thisHeading = thisAccordion.find('.panel-body.in').prev('.panel-heading');
        if(thisHeading.length) {
            thisHeading.addClass('sdb_h_active');
        }
    },
    scrollbar: function() {
        var $sidebar_inner_scroll = $('.sidebar_inner_scroll');
        if($sidebar_inner_scroll.length) {
            $sidebar_inner_scroll.slimScroll({
                height: 'auto',
                alwaysVisible: true,
                opacity: '0.2',
                wheelStep: is_touch_device() ? 40 : 1
            });
        }
    }
};

gedsidebar.init();
var $editable = $('.editable');
$editable.editable($editable.attr('data-href'), {
    type      : 'textarea',
    cancel    : 'Cancelar',
    submit    : 'Salvar',
    cssclass  : 'form-control-editable',
    indicator : '<img src="'+$editable.attr('data-indicator')+'">',
    tooltip   : 'Clique para alterar... '
});



/* [ ---- Gebo Admin Panel - gallery grid ---- ] */

$(document).ready(function() {
    //* init grid
    gebo_gal_grid.init();
    gebo_gal_grid.large();

});

var gebo_gal_grid = {
    init: function() {
        $('.wmk_grid').each(function(){
            $(this).find('.yt_vid,.self_vid,.vimeo_vid').append('<span class="vid_ico"/>');
        });
    },
    large: function() {
        $('#large_grid > ul').imagesLoaded(function() {
            // Prepare layout options.
            var options = {
                autoResize: true, // This will auto-update the layout when the browser window is resized.
                container: $('#large_grid'), // Optional, used for some extra CSS styling
                offset: 6, // Optional, the distance between grid items
                itemWidth: 220, // Optional, the width of a grid item (li)
                flexibleItemWidth: false,
            };

            // Get a reference to your grid items.
            var handler = $('#large_grid > ul > li');

            // Call the layout function.
            handler.wookmark(options);


            $('#large_grid > ul > li').on('mouseenter',function(){
                $(this).addClass('act_tools');
            }).on('mouseleave',function(){
                $(this).removeClass('act_tools');
            });

            $('#large_grid ul li > a').attr('rel', 'gallery').colorbox({
                slideshowAuto : false,
                maxWidth	: '80%',
                maxHeight	: '80%',
                opacity		: '0.1',
                loop		: true,
                slideshow   : true,

            });
        });
    },
    mixed: function() {
        $('#mixed_grid > ul').imagesLoaded(function() {
            // Prepare layout options.
            var options = {
                autoResize: true, // This will auto-update the layout when the browser window is resized.
                container: $('#mixed_grid'), // Optional, used for some extra CSS styling
                offset: 6, // Optional, the distance between grid items
                itemWidth: 220, // Optional, the width of a grid item (li)
                flexibleItemWidth: false
            };

            // Get a reference to your grid items.
            var handler = $('#mixed_grid > ul > li');

            // Call the layout function.
            handler.wookmark(options);

            $('#mixed_grid > ul > li').on('mouseenter',function(){
                $(this).addClass('act_tools');
            }).on('mouseleave',function(){
                $(this).removeClass('act_tools');
            });

            $('#mixed_grid ul li > a').not('.int_video').attr('rel', 'mixed_gallery').colorbox({
                maxWidth	    : '80%',
                maxHeight	    : '80%',
                opacity		    : '0.3',
                photo		    : true,
                loop		    : false,
                fixed		    : true
            });

            if($(window).width() < 768 ) {
                var videoW = '90%',
                    videoH = '90%';
            } else {
                var videoW = '640px',
                    videoH = '360px';
            }

            $('#mixed_grid .int_video').attr('rel', 'mixed_gallery').colorbox({
                width    		: videoW,
                height	   		: videoH,
                opacity		    : '0.3',
                inline		    : true,
                loop		    : false,
                scrolling		: false,
                fixed		    : true,
                onComplete 		: function() {
                    $.colorbox.resize();
                }
            });

        });
    }  
};

function permissionsFile($element){

    var folderHash = $($element).attr('data-id');

// console.log($element);
    $.getJSON(base_url+'arquivos/permissionByDirectory/', {hash: folderHash})
    .done(function(permissions) {
        $('.ver, .detalhes, .compartilhar, .excluir').removeClass('hidden')

        if(permissions[0] === '0'){
            $('.ver').addClass('hidden');
        }

        if(permissions[1] === '0'){
            $('.detalhes').addClass('hidden');
        }

        if(permissions[2] === '0'){
            $('.compartilhar').addClass('hidden');
        }

        if(permissions[3] === '0'){
            $('.excluir').addClass('hidden');
        }
    })
    .fail(function( jqxhr, textStatus, error ) {
        $('.ver, .detalhes, .compartilhar, .excluir').removeClass('hidden')
    });
}

