(function () {
"use strict";

    var fmng = {
        lang: "pt_br",
        baseUrl: document.getElementById('url').value,
        fm: document.getElementById('fm'),
        fmClass: "fm",
        fmgrid: document.getElementById('fmgrid'),
        fmel: document.getElementsByClassName('el'),
        ubxClass: "ubx",
        ubx: document.getElementById('ubx'),
        taskItemInContext: null,
        itemClassName: 'el',
        contextMenuClassName: "context-menu",
        contextMenuItemClassName: "context-menu__item",
        contextMenuLinkClassName: "context-menu__link",
        contextMenuActive: "context-menu--active",
        icButton : 'ic',
        menu: document.getElementById("context-menu"),
        superMenu: document.getElementById("super-context-menu"),
        menuState: 0,
        panelStatus: 0,
        menuActive: 'context-menu--active',
        menuPosition: 0,
        menuPositionX: 0,
        menuPositionY: 0,
        clickCoords: 0,
        clickCoordsX: 0,
        clickCoordsY: 0,
        dropAreaClass: "fm",
        filesUpload: document.getElementById("get_file"),
        dropArea: document.getElementById("fm"),
        fileList: document.getElementById("ubx"),
        language: {
            pt_br: {
                "filename": "Nome",
                "filetype": "Tipo",
                "filemime": "Mime",
                "filehash": "Hash",
                "fileext": "Extensão",
                "filesize": "Tamanho do arquivo",
                "filemodified": "Modificado",
                "lastOpened": "Aberto",
                "fileusermod": "",
                "owner": "Proprietário",
                "locateAt": "Local"
            }
        },
        init: function () {
            var fmel = this.fmel;
            var fm = this.fm;
            this.resize(fm);
            this.selectItem(fmel, fm);
            this.keyupListener();
            this.contextListener();
            this.clickListener();
            this.populateGrid();

            this.drop(this);
            this.uploadEvents();
            if (this.panelStatus != 0) {
                this.openDetailsPanel(e);
            }

        },
        drop: function (fmel) {
            var dropArea = fmel.dropArea;
            dropArea.addEventListener("dragleave", function (evt) {
                var target = evt.srcElement || evt.target;

                var isDropArea = fmng.clickInsideElement(evt, fmel.dropAreaClass);
                if (target && target === dropArea) {
                    this.classList.remove("over");
                }
                evt.preventDefault();
                evt.stopPropagation();
            }, false);

            dropArea.addEventListener("dragenter", function (evt) {
                this.classList.add("over");
                evt.preventDefault();
                evt.stopPropagation();
            }, false);

            dropArea.addEventListener("dragover", function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
            }, false);

            dropArea.addEventListener("drop", function (evt) {
                fmel.traverseFiles(evt.dataTransfer.files);
                this.classList.remove("over");
                evt.preventDefault();
                evt.stopPropagation();
            }, false);
        },
        uploadFile: function (file) {

            var fmng = this;
            var divIt = document.createElement("div"),
                divFn = document.createElement("div"),
                img,
                progressBarContainer = document.createElement("progress"),
                divPct = document.createElement("div"),
                //divCtr = document.createElement("div"),
                //divCln = document.createElement("div"),
                divClf = document.createElement("div"),
                reader,
                xhr,
                fileInfo;

            divIt.classList.add('it');
            divFn.classList.add('fn');
            progressBarContainer.setAttribute('value', '0');
            progressBarContainer.setAttribute('max', '100');
            divPct.setAttribute('id', 'pct');
            divPct.innerHTML = "0%";
            //divCtr.classList.add('ctr');
            //divCln.classList.add('cln');
            divClf.classList.add('clearfix');

            divIt.appendChild(divFn);
            divIt.appendChild(progressBarContainer);
            divIt.appendChild(divPct);
            //divCtr.appendChild(divCln);
            //divIt.appendChild(divCtr);
            divIt.appendChild(divClf);
            /*
             If the file is an image and the web browser supports FileReader,
             present a preview in the file list
             */
            //if (typeof FileReader !== "undefined" && (/image/i).test(file.type)) {
            //    img = document.createElement("img");
            //    li.appendChild(img);
            //    reader = new FileReader();
            //    reader.onload = (function (theImg) {
            //        return function (evt) {
            //            theImg.src = evt.target.result;
            //        };
            //    }(img));
            //    reader.readAsDataURL(file);
            //}

            // Uploading - for Firefox, Google Chrome and Safari
            xhr = new XMLHttpRequest();

            // Update progress bar
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    progressBarContainer.setAttribute('value', (evt.loaded / evt.total) * 100);
                    divPct.innerHTML = Math.floor((evt.loaded / evt.total) * 100) + "%";
                }
                else {
                    // No data to calculate on
                }
            }, false);

            xhr.addEventListener("load", function () {
                progressBarContainer.className += " uploaded";
                fmng.populateGrid();
                setTimeout(function(){
                    document.getElementsByClassName('uc')[0].removeChild(divIt)
                }, 5000);
            }, false);

            var base_url = this.baseUrl;

            xhr.open("POST", base_url + "arquivos/upload/", true);

            // Set appropriate headers

            xhr.setRequestHeader("X-File-Name", file.name);
            xhr.setRequestHeader("X-File-Size", file.size);
            xhr.setRequestHeader("X-File-Type", file.type);

            var fd = new FormData();
            fd.append('file', file);

            xhr.send(fd);

            divFn.innerHTML = file.name;
            document.getElementsByClassName('uc')[0].appendChild(divIt)
            this.fileList.classList.add('active');
        },
        uploadEvents: function () {
            var fmng = this;
            this.filesUpload.addEventListener("change", function () {
                fmng.traverseFiles(this.files);

            });
        },
        traverseFiles: function (files) {
            if (typeof files !== "undefined") {
                for (var i = 0, l = files.length; i < l; i++) {
                    this.uploadFile(files[i]);
                }
            }
            else {
                this.fileList.innerHTML = "No support for the File API in this web browser";
            }
        },
        resize: function (element) {
            var width = window.innerWidth || document.body.clientWidth;
            var height = window.innerHeight || document.body.clientHeight;
            element.style.height = Math.floor(height - (height / 4)) + 'px';

        },
        clickListener: function () {
            var fmng = this;
            document.addEventListener("mouseup", function (e) {
                var clickeElIsLink = fmng.clickInsideElement(e, fmng.contextMenuLinkClassName);
                var clickIcButton = fmng.clickInsideElement(e, fmng.icButton);

                if(clickIcButton){
                    e.preventDefault();
                    var action = clickIcButton.getAttribute('id');
                    if(typeof fmng[action] == 'function'){
                        fmng[action]();
                    }
                }

                if (clickeElIsLink) {
                    e.preventDefault();
                    fmng.menuItemListener(clickeElIsLink);
                } else {
                    var button = e.which || e.button;
                    if (button === 1) {
                        fmng.toggleMenuOff();
                    }
                }
            });
        },
        menuItemListener: function (link) {
            var fmng = this;

            if(typeof fmng[link.getAttribute('data-action')] == 'function'){

                if(link.getAttribute('data-context')){
                    fmng[link.getAttribute('data-action')](fmng.taskItemInContext);
                } else {
                    fmng[link.getAttribute('data-action')](link);
                }
            }

            fmng.toggleMenuOff();
        },
        download : function(el){
            if(el.getAttribute('data-p-t')!='file')
                return false;

            var id = el.getAttribute('data-id');
            var url = this.baseUrl+'arquivos/download/'+id;

            console.log(url);
            window.open(url, "_blank");

        },
        clickInsideElement: function (e, className) {

            var el = e.srcElement || e.target;
            if (el.classList.contains(className)) {
                return el;
            } else {
                while (el = el.parentNode) {
                    if (el.classList && el.classList.contains(className)) {
                        return el;
                    }
                }
            }

            return false;
        },
        positionMenu: function (e, menu) {

            var fmng = this;
            var clickCoords = fmng.getPosition(e);
            fmng.clickCoordsX = clickCoords.x;
            fmng.clickCoordsY = clickCoords.y;

            fmng.menuWidth = menu.offsetWidth + 4;
            fmng.menuHeight = menu.offsetHeight + 4;

            var windowWidth = window.innerWidth;
            var windowHeight = window.innerHeight;

            if ((windowWidth - fmng.clickCoordsX) < fmng.menuWidth) {
                menu.style.left = windowWidth - fmng.menuWidth + "px";
            } else {
                menu.style.left = fmng.clickCoordsX + "px";
            }

            if ((windowHeight - fmng.clickCoordsY) < fmng.menuHeight) {
                menu.style.top = fmng.windowHeight - fmng.menuHeight + "px";
            } else {
                menu.style.top = fmng.clickCoordsY + "px";
            }

        },
        getPosition: function (e) {
            var posx = 0;
            var posy = 0;

            if (!e) var e = window.event;

            if (e.pageX || e.pageY) {
                posx = e.pageX;
                posy = e.pageY;
            } else if (e.clientX || e.clientY) {
                posx = e.clientX + document.body.scrollLeft +
                document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop +
                document.documentElement.scrollTop;
            }

            return {
                x: posx,
                y: posy
            }
        },
        contextListener: function () {
            var fmng = this;
            document.addEventListener("contextmenu", function (e) {
                e.preventDefault();
                fmng.taskItemInContext = fmng.clickInsideElement(e, fmng.itemClassName);
                if (fmng.taskItemInContext != false) {

                    fmng.toggleMenuOn(fmng.menu);
                    fmng.positionMenu(e, fmng.menu);
                    fmng.selectItemToggle(e);
                } else {
                    if (fmng.clickInsideElement(e, fmng.fmClass) != false && fmng.taskItemInContext == false);
                    fmng.toggleMenuOn(fmng.superMenu);
                    fmng.positionMenu(e, fmng.superMenu);
                }
            });
        },
        keyupListener: function () {
            var fmng = this;
            window.onkeyup = function (e) {
                if (e.keyCode === 27) {
                    fmng.toggleMenuOff();
                }
            }
        },
        toggleMenuOn: function (menu) {
            if (this.menuState !== 1) {
                this.menuState = 1;
                menu.classList.add(this.menuActive);
            }
        },
        toggleMenuOff: function (menu) {
            if (this.menuState !== 0) {
                this.menuState = 0;
                this.menu.classList.remove(this.menuActive);
                this.superMenu.classList.remove(this.menuActive);
            }
        },
        selectItemToggle: function (e) {
            var el = e.target || e.srcElement;

            while (el = el.parentNode) {
                if (el.classList && el.classList.contains(fmng.itemClassName)) {
                    el = el;
                    break;
                }
            }

            this.selectItemOff();

            if (el.classList.contains('el') && !el.classList.contains('active'))
                el.classList.add('active');
            else if (el.classList.contains('el') && el.classList.contains('active'))
                el.classList.remove('active');

        },
        selectItemOn: function (e) {
            this.selectItemOff();
            e.classList.add('active');
        },
        selectItemOff: function (e) {

            if (e != null) {
                e.classList.remove('active');
                return;
            }

            var itens = document.getElementsByClassName(this.itemClassName);
            for (var i = 0; i < itens.length; i++) {
                itens[i].classList.remove('active');
            }
            ;

        },
        selectItem: function (fmel, fm) {
            var fmng = this;
            document.addEventListener("click", function (e) {
                var el = e.srcElement || e.target;

                while (el = el.parentNode) {
                    if (el.classList && el.classList.contains(fmng.itemClassName)) {
                        el = el;
                        break;
                    }
                }

                if (fmng.clickInsideElement(e, fmng.itemClassName)) {
                    if (fmng.panelStatus != 0) {
                        fmng.openDetailsPanel(el);
                    }

                    e.preventDefault();

                    if (el.classList.contains('active'))
                        fmng.selectItemOff(el);
                    else
                        fmng.selectItemOn(el);

                    if (fmng.menuState !== 0) {
                        fmng.toggleMenuOff();
                    }
                } else {
                    fmng.selectItemOff(el);
                    if (fmng.menuState !== 0) {
                        fmng.toggleMenuOff();
                    }
                }
            });
        },
        openPreviewPanel: function (e) {
            var ovl = document.createElement('div');
            var body = document.getElementsByTagName('body');
            ovl.classList.add('ovl');
            ovl.setAttribute('id', 'ovl-box');
            body[0].classList.add('bovl');
            body[0].appendChild(ovl);

            ovl.addEventListener('click', function (e) {
                fmng.closePreviewPanel();
            });

        },
        closePreviewPanel: function (e) {
            var ovl = document.getElementById('ovl-box');
            ovl.parentNode.removeChild(ovl);
            var body = document.getElementsByTagName('body');
            body[0].classList.remove('bovl');
        },
        createPreloader: function(element){
            var preloader = '<div class="preloader" id="preloader"><div class="sk-spinner sk-spinner-rotating-plane"></div></div>';
            element.innerHTML=preloader;
        },
        hidePreloader : function() {
            document.getElementById('preloader').style.display='none';
        },
        openDetailsPanel: function (e) {

            var fmng = this;
            var lang = this.language[this.lang];
            var dpn = document.getElementById('dtpn');
            while (dpn.firstChild) dpn.removeChild(dpn.firstChild);
            this.createPreloader(dpn);
            var cldpn = document.createElement('div');
            cldpn.classList.add('x');
            cldpn.setAttribute('id', ':x');
            cldpn.innerHTML = "x";
            var divclf = document.createElement('div');
            divclf.classList.add('clearfix');

            var objs = null;
            var hash = e.getAttribute('data-id').replace(':id', '');

            var do_panel = function (obj) {
                var divH = document.createElement('div');
                divH.classList.add('h');
                var divI = document.createElement('div');
                divI.classList.add('i');
                var imgIc = document.createElement('img');
                imgIc.setAttribute('src', obj.fileDetails.fileico);
                divI.appendChild(imgIc);
                var divT = document.createElement('div');
                divT.classList.add('t');
                var divSp = document.createElement('span');
                divSp.innerHTML = obj.fileDetails.filename;
                divT.appendChild(divSp);
                divH.appendChild(divI);
                divH.appendChild(divT);
                divH.appendChild(cldpn);
                divH.appendChild(divclf);
                dpn.appendChild(divH);

                var divPnd = document.createElement('div');
                divPnd.classList.add('pnd');
                var objSize = Object.keys(obj.fileDetails).length


                for (var i = 0; i < objSize; i++) {

                    var key = Object.keys(obj.fileDetails)[i];
                    if (lang[key] == undefined) continue;

                    var dsc = document.createElement('div');
                    var lb = document.createElement('div');
                    lb.classList.add('lb');
                    var ds = document.createElement('div');
                    ds.classList.add('ds');

                    lb.innerHTML = lang[key];
                    ds.innerHTML = obj.fileDetails[key];
                    dsc.appendChild(lb);
                    dsc.appendChild(ds);
                    dsc.setAttribute('title', obj.fileDetails[key]);
                    divPnd.appendChild(dsc);
                }
                var divclf1 = divclf.cloneNode(true);
                divPnd.appendChild(divclf1)
                dpn.appendChild(divPnd);
                dpn.classList.add('show');
                fmng.fmgrid.style.marginRight = '400px';
                fmng.ubx.style.marginRight = '420px';
                fmng.hidePreloader();
                fmng.panelStatus = 1;
                cldpn.addEventListener('click', function (e) {
                    fmng.closeDetailsPanel();
                });

            }

            if (localStorage.getItem('item-' + hash) != null) {
                do_panel(JSON.parse(localStorage.getItem('item-' + hash)));
                return;
            }
            ;


            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    objs = JSON.parse(xhr.responseText);
                    var obj = objs[hash];

                    localStorage.setItem('item-' + hash, JSON.stringify(obj));

                    do_panel(obj);
                }
            }
            xhr.open('GET', this.baseUrl + 'arquivos/details/' + hash, true);
            xhr.send(null);

        },
        closeDetailsPanel: function (e) {

            this.fmgrid.style.marginRight = '0px';
            this.ubx.style.marginRight = '0px';
            this.panelStatus = 0;
            var dpn = document.getElementById('dtpn');
            while (dpn.firstChild) dpn.removeChild(dpn.firstChild);
            dtpn.classList.remove('show');
        },
        elementExists :  function(className, propId, propValue){
            var el = document.getElementsByClassName(className);
            var exists = false;
            for(var i=0;i<el.length;i++){
                var thisProp = el[i].getAttribute(propId);
                if(thisProp == ':id'+propValue){
                    return true;
                }
            }

            return false;
        },
        createElementsItem: function (obj) {
            if(this.elementExists('el', 'data-id', obj.objHash) == true) return;

            var elDiv = document.createElement('div');
            elDiv.classList.add('el');
            elDiv.setAttribute('data-id', ":id" + obj.objHash);
            elDiv.setAttribute('data-p-t', obj.objType);
            var aDiv = document.createElement('div');
            aDiv.classList.add('a');
            var imgUrl = null;
            if (obj.objImage != null && obj.objIsImage == true) {
                imgUrl = this.baseUrl + 'thumb/show/' + '150/150/' + obj.objImage
            } else
                if (obj.objIsImage == true && obj.objImage == null || obj.objIsImage == false) {
                    if (obj.objIcon != null) {
                        imgUrl = obj.objIcon
                    }
            }

            if (imgUrl != null) {
                aDiv.style.backgroundImage = "url('" + imgUrl + "')";
                aDiv.style.backgroundSize = 'cover';
                aDiv.style.backgroundRepeat = "no-repeat";
            }
            var nDiv = document.createElement('div');
            nDiv.classList.add('n');
            var fnDiv = document.createElement('div');
            fnDiv.classList.add('f-n');
            fnDiv.innerHTML = obj.objName;
            fnDiv.setAttribute('title', obj.objName)

            nDiv.appendChild(fnDiv);
            //if(imgEl != undefined)
            //aDiv.appendChild(imgEl);

            elDiv.appendChild(aDiv)
            elDiv.appendChild(aDiv);
            nDiv.appendChild(fnDiv)
            elDiv.appendChild(nDiv);

            return elDiv;
        },
        populateGrid: function () {
            var fmng = this;
            var grid = this.fmgrid;
            var obj = null;
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    obj = JSON.parse(xhr.responseText);

                    for (var i = 0; i < obj.item.length; i++) {
                        var elem = fmng.createElementsItem(obj.item[i]);
                        if(typeof elem == 'object')
                        grid.appendChild(elem);
                    }
                }
            }
            xhr.open('GET', fmng.baseUrl + 'arquivos/fetch', true);
            xhr.send(null);

        },
        uploadPanel: function (e) {

            var input = document.getElementById('get_file').click();

        },
        toggleUc: function (uc) {
            if (uc.classList.contains('active')) {
                while(uc.firstChild) uc.removeChild(uc.firstChild);
                uc.classList.remove('active');
                uc.classList.add('inactive');
            } else {
                uc.classList.remove('active');
                uc.classList.add('inactive');
            }
        },
        toggleUpx: function (upx) {
            if (upx.classList.contains('active')) {
                upx.classList.remove('active');
            } else {
                upx.classList.add('active');
            }
        },
        destroyPanel : function(selector){
            var selector = selector != null ? selector : 'dialog'
            var dialog = document.getElementsByClassName(selector);
            for(var i=0;i<dialog.length;i++){
                dialog[i].parentNode.removeChild(dialog[i]);
            }
        },
        ic_cl : function(){
            var panel = document.getElementById('ubx');
            if(panel != undefined)
                panel.classList.remove('active');
        },
        createPanel : function(e){
            console.log(e);

            var fmng = this;
            var strDrv = e.getAttribute('data-id') || fmng.fm.getAttribute('data-id');

            $.ajax({
                url : fmng.baseUrl+e.getAttribute('data-u'),
                method : 'POST',
                data : {
                    cmd : e.getAttribute('data-cmd'),
                    context : e.getAttribute('data-context-cmd'),
                    id : strDrv
                },
                success : function(data){
                    var body  = document.getElementsByTagName('body');
                    var div = document.createElement('div');
                    div.innerHTML=data;
                    body[0].appendChild(div);

                    fmng.registerEvent('.dialog-action-button', 'click', e.getAttribute('data-context-cmd'))
                    fmng.registerEvent('#textField-folder', 'keyup', 'enable')
                }
            });
        },
        mkdir : function(e){
            var fmng = this;
            var elements = document.getElementsByClassName(e.getAttribute('data-field-class'));
            var data = new Object();
            for(var i=0;i<elements.length;i++){
                data[elements[i].getAttribute('name')] = elements[i].value;
            }

            data['cmd'] = 'mkdir';
            data['context'] = 'action';


            $.ajax({
                url : e.getAttribute('data-url'),
                dataType: "JSON",
                method : "POST",
                data : data,
                success : function(data){
                    console.log(data);
                    fmng.destroyPanel();
                }
            });

            console.log(data);
        },
        enable : function(e){
            var value = e.value;
            var selector = e.getAttribute('target');
            var el = selector.charAt(0) == '.' ? document.getElementsByClassName(selector.replace('.','')) : document.getElementById(selector.replace('#', ''));



            if(value.length > 0){

                if(selector.charAt(0) == '.'){
                    for(var i=0;i<el.length;i++) {
                        el[i].removeAttribute('disabled');
                    }
                } else
                    el.removeAttribute('disabled');
            } else {
                if(selector.charAt(0) == '.'){
                    for(var i=0;i<el.length;i++) {
                        el[i].setAttribute('disabled', 'disabled');
                    }
                } else
                    el.setAttribute('disabled', 'disabled');
            }

        },
        registerEvent : function(selector, event, callback, params){

            var el = selector.charAt(0) == '.' ? document.getElementsByClassName(selector.replace('.','')) : document.getElementById(selector.replace('#', ''));

            if(typeof el.length == 'number'){
                if(el !=  null)
                    for(var i=0;i<el.length;i++){

                        params = params == null ? el[i] : params;

                        el[i].addEventListener(event, function(){
                            if(typeof fmng[callback] == 'function'){
                                fmng[callback](params);
                            } else {
                                console.log('callback '+callback+' does not exists.');
                            }
                        });
                    }

            } else {
                if(el !=  null)
                    el.addEventListener(event, function(){
                        if(typeof fmng[callback] == 'function'){
                            fmng[callback](params == null ? el : params);
                        } else {
                            console.log('callback '+callback+' does not exists.');
                        }
                    });
            }

        }

    }

    fmng.init();


})();