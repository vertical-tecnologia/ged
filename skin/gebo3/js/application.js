function is_touch_device() {
	return !!('ontouchstart' in window);
}

gedalert = {
	init: function(type, title, message, btnText){
		var confirmText = btnText != null || btnText != undefined || btnText != '' ? btnText : 'OK';
		swal({title: title, text: message, type: type, confirmButtonText: confirmText });
	}
};

gedToggleEnable = {
	init : function(element, target, condition, type){
		var theTarget = document.getElementById(target);
		if(condition == element.value){

			if(typeof theTarget == 'object'){
				if(type == 'hide'){
					this.addClass(theTarget);
				} else {
					theTarget.setAttribute('disabled', 'disabled');
					$(".chosen-select").trigger('chosen:updated');
					gedalert.init('error', 'Oooooooops!', 'Parece que algo deu errado.', 'okay :(');
				}

			};
		} else {
			if(typeof theTarget == 'object'){
				if(type == 'hide'){
					this.removeClass(theTarget);
				} else {
					theTarget.removeAttribute('disabled');
					$(".chosen-select").trigger('chosen:updated');
					gedalert.init('error', 'Oooooooops!', 'Parece que algo deu errado.', 'okay :(');
				}

			};
		}
	},
	enable : function(target){
		document.getElementById(target).disabled = false;
	},
	disable : function(target){
		document.getElementById(target).disabled = true;
	},
	addClass : function(target){
		if(!target.classList.contains('hide'))
			target.classList.add('hide');
	},
	removeClass : function(target){
		if(target.classList.contains('hide'))
			target.classList.remove('hide');
	},

}

// bootstrap switch
gedswitch = {
	init: function() {
		if($('.bs-switch').length) {
			$('.bs-switch').bootstrapSwitch();
		}
	}
};

gedpeity = {
	init: function() {
		$.fn.peity.defaults.line = {
			strokeWidth: 1,
			delimiter: ",",
			height: 32,
			max: null,
			min: 0,
			width: 50
		};
		$.fn.peity.defaults.bar = {
			delimiter: ",",
			height: 32,
			max: null,
			min: 0,
			spacing: 1,
			width: 50
		};
		$(".p_bar_up span").peity("bar",{
			colours: ["#6cc334"]
		});
		$(".p_bar_down span").peity("bar",{
			colours: ["#e11b28"]
		});
		$(".p_line_up span").peity("line",{
			colour: "#b4dbeb",
			strokeColour: "#3ca0ca"
		});
		$(".p_line_down span").peity("line",{
			colour: "#f7bfc3",
			strokeColour: "#e11b28"
		});
	}
};


gedsidebar = {
	init: function() {
		var $body = $('body'),
			$sidebar_switch = $('.sidebar_switch');

		// sidebar onload state
		if($(window).width() > 979){
			if(!$body.hasClass('sidebar_hidden')) {
				if( $.cookie('gebo_sidebar') == "hidden") {
					$body.addClass('sidebar_hidden');
					$sidebar_switch.toggleClass('on_switch off_switch').attr('title','Show Sidebar').attr('data-original-title', "Show Sidebar");
				}
			} else {
				$sidebar_switch.toggleClass('on_switch off_switch').attr('title','Show Sidebar').attr('data-original-title', "Show Sidebar");
			}
		} else {
			$body.addClass('sidebar_hidden');
			$sidebar_switch.removeClass('on_switch').addClass('off_switch');
		}

		gedsidebar.info_box();

		//* sidebar visibility switch
		$sidebar_switch.click(function(){
			$sidebar_switch.removeClass('on_switch off_switch');
			if( $body.hasClass('sidebar_hidden') ) {
				$.cookie('gebo_sidebar', null);
				$body.removeClass('sidebar_hidden');
				$sidebar_switch.addClass('on_switch').show();
				$sidebar_switch.attr( 'title', "Hide Sidebar").attr('data-original-title', "Hide Sidebar");
			} else {
				$.cookie('gebo_sidebar', 'hidden', {path: '/'});
				$body.addClass('sidebar_hidden');
				$sidebar_switch.addClass('off_switch');
				$sidebar_switch.attr( 'title', "Show Sidebar" ).attr('data-original-title', "Show Sidebar");
			}
			gedsidebar.info_box();
			//gebo_sidebar.update_scroll();
			$(window).resize();
		});
		//* prevent accordion link click
		$('.sidebar .accordion-toggle').click(function(e){e.preventDefault()});

		$(window).on("debouncedresize", function() {
			gedsidebar.scrollbar();
		});

		$(window).resize();

	},
	info_box: function(){
		var s_box = $('.sidebar_info');
		var s_box_height = s_box.actual('height');
		s_box.css({
			'height'        : s_box_height
		});
		$('.push').height(s_box_height);
		$('.sidebar_inner').css({
			'margin-bottom' : '-'+s_box_height+'px',
			'min-height'    : '100%'
		});
	},
	make_active: function() {
		var thisAccordion = $('#side_accordion');
		thisAccordion.find('.panel-heading').removeClass('sdb_h_active');
		var thisHeading = thisAccordion.find('.panel-body.in').prev('.panel-heading');
		if(thisHeading.length) {
			thisHeading.addClass('sdb_h_active');
		}
	},
	scrollbar: function() {
		var $sidebar_inner_scroll = $('.sidebar_inner_scroll');
		var $main_inner_scroll = null;//;$('.main_content');
		if($sidebar_inner_scroll.length) {
			$sidebar_inner_scroll.slimScroll({
				height: 'auto',
				alwaysVisible: true,
				opacity: '0.2',
				wheelStep: is_touch_device() ? 40 : 20
			});
		}
	}
};

gedflist = {
	init: function(){
		//*typeahead
		var list_source = [];
		$('.user_list li').each(function(){
			var search_name = $(this).find('.sl_name').text();
			//var search_email = $(this).find('.sl_email').text();
			list_source.push(search_name);
		});
		$('.user-list-search').typeahead({local: list_source, limit:5});

		var options = {
				valueNames: [ 'sl_name', 'sl_status', 'sl_email' ],
				page: 10,
				plugins: [
					[ 'paging', {
						pagingClass	: "bottomPaging",
						innerWindow: 1,
						outerWindow: 2
					} ]
				]
			},
			userList = new List('user-list', options);

		$('#filter-online').on('click',function() {
			$('ul.filter li').removeClass('active');
			$(this).parent('li').addClass('active');
			userList.filter(function(item) {
				return item.values().sl_status == "online";
			});
			return false;
		});
		$('#filter-offline').on('click',function() {
			$('ul.filter li').removeClass('active');
			$(this).parent('li').addClass('active');
			userList.filter(function(item) {
				return item.values().sl_status == "offline";
			});
			return false;
		});
		$('#filter-none').on('click',function() {
			$('ul.filter li').removeClass('active');
			$(this).parent('li').addClass('active');
			userList.filter();
			return false;
		});

		$('#user-list').on('click','.sort',function(){
				$('.sort').parent('li').removeClass('active');
				if($(this).parent('li').hasClass('active')) {
					$(this).parent('li').removeClass('active');
				} else {
					$(this).parent('li').addClass('active');
				}
			}
		);
	}
};

gedtable = {
	sync : function(){
		var syncElements = document.getElementsByClassName('sync');
		for(var i=0;i<syncElements.length;i++){
			syncElements[i].addEventListener('click', function(e){
				e.preventDefault();
				var parent_field = document.getElementById('parent_field').value;
				var id_field = document.getElementById(parent_field).value;
				var url = e.target.getAttribute('data-arg-href') + id_field;
				var target = e.target.getAttribute('data-target');
				gedtable.populate(url, target);

			});
		}
	},
	tables :{
		table_contato : {
			columns : [
				"tipo_desc",
				"contato_valor"
			],
			id : "contato_id",
			actions : [
				"delete"
			]

		},
		table_endereco : {
			columns : [
				"endereco_alias",
				"endereco_cidade"
			],
			id : "endereco_id",
			actions :[
				"delete"
			]
		}
	},
	populate : function(url, target){
		var dTable = this;
		$.ajax({
			url : url,
			method : 'GET',
			dataType : 'JSON',
			success : function(data){
				var targetEl = document.getElementById(target);
				var columns = dTable.tables[target].columns;
				var actions = dTable.tables[target].actions;

				var tbody = gedtable.tbodyNode();
				for(var i = 0;i<data.length;i++){
					var tr = gedtable.trNode();

					for(var k=0;k<columns.length;k++){
						var td = gedtable.tdNode();
						td.innerHTML = data[i][columns[k]];
						tr.appendChild(td);
					}
					for(var j=0;j<actions.length;j++){

						console.log(data[i]);
						var td = gedtable.tdNode();
						var a = gedtable.aNode();
						a.innerHTML = data[i][actions[j]+'_label'];
						a.setAttribute('href', data[i][actions[j]+'_url']);
						a.setAttribute('data-arg-href', data[i][actions[j]+'_arg_url']);
						a.setAttribute('data-arg-value', data[i][actions[j]+'_arg_value']);
						a.setAttribute('data-callback', data[i][actions[j]+'_callback']);
						a.setAttribute('data-target', data[i][actions[j]+'_target']);
						a.setAttribute('data-ajax', true);

						td.appendChild(a);
						td.className = 'text-right';
						tr.appendChild(td);
					}
					tbody.appendChild(tr);
				}
				gedtable.emptyNode(target);
				targetEl.appendChild(tbody);
				ajaxlink.init();
			}
		});
	},
	aNode : function(){
		return document.createElement('a');
	},
	tbodyNode : function(){
		return document.createElement('tbody');
	},
	tdNode : function(){
		return document.createElement('td');
	},
	trNode : function(){
		return document.createElement('tr');
	},
	emptyNode : function(target){
		var element = document.getElementById(target);
		if(element != undefined && element != null)
		while (element.firstChild) element.removeChild(element.firstChild);
	}
}

ajaxlink = {
	init : function(){
		var linkElement = document.getElementsByTagName('a');
		for(var i=0;i<linkElement.length;i++){
			var is_ajax = linkElement[i].getAttribute('data-ajax');

			if(is_ajax == "true"){
				linkElement[i].addEventListener('click', function(e){
					e.preventDefault();
					var href = this.getAttribute('href');
					var dataHref = this.getAttribute('data-arg-href');
					var target = this.getAttribute('data-target');
					var dataCallback = this.getAttribute('data-callback').split('|');

					$.ajax({
						url : href,
						method : 'GET',
						success : function(data){
							if(data.error == 1){
								swal(data.title, data.msg, 'error');
							} else {
								if(typeof window[dataCallback[0]][dataCallback[1]] == 'function'){
									window[dataCallback[0]][dataCallback[1]](dataHref, target);
									ajaxlink.init();
								}
							}
						}
					});

				})

			}
		}
	}
};

ui = {
	server_box : function(e, value){
		var el = e.currentTarget || e.srcElement;
		if(el != undefined && el != null){

			var boxes = document.getElementsByClassName('sbox');
			for(var i=0;i<boxes.length;i++){
				if(!boxes[i].classList.contains('hide'))
					boxes[i].classList.add('hide');
			}

			if(value == undefined){
				return false;
			}

			var box = document.getElementById('storage'+value.selected);
			if(box.classList.contains('hide'))
				box.classList.remove('hide');
			console.log(box);
		}
	}
}


gedchosen = {
	init: function(){
		$(".chosen-select").chosen({
			allow_single_deselect: false
		});

		$('.chosen-select').on('chosen:updated', function () {
			if ($('.chosen-select').attr('readonly')) {
				var wasDisabled = $('.chosen-select').is(':disabled');

				$('.chosen-select').attr('disabled', 'disabled');
				$('.chosen-select').data('chosen').search_field_disabled();

				if (wasDisabled) {
					$('.chosen-select').attr('disabled', 'disabled');
				} else {
					$('.chosen-select').removeAttr('disabled');
				}
			}
		});

		$(".chosen-select").trigger('chosen:updated');


		$(".chosen-select").on('change' ,function(evt, params){
			var el = evt.currentTarget || evt.srcElement;
			if(el.getAttribute('data-callback') != null){
				var callbacks = el.getAttribute('data-callback').split('|');
				if(callbacks != undefined){
					if(typeof window[callbacks[0]][callbacks[1]] == 'function')
						window[callbacks[0]][callbacks[1]](evt, params)
				}
			}
		});

	}
};

gedmask = {
	init : function(){
		$(".date").mask("99/99/9999",{placeholder:"dd/mm/aaaa"});
		var SPMaskBehavior = function (val) {
				return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			},
			spOptions = {
				onKeyPress: function(val, e, field, options) {
					field.mask(SPMaskBehavior.apply({}, arguments), options);
				}
			};

		$('.sp_celphones').mask(SPMaskBehavior, spOptions);
		$(".cep").mask("99999-999");
		$(".cpf").mask("999.999.999-99", {'placeholder': '000.000.000-00'});
		$(".cnpj").mask("99.999.999.9999/99", {'placeholder': '00.000.000/0001-00'});
	}
};

imageControl = {
	base_url : document.getElementById('url').value,
	preview : function (element) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(element.files[0]);

		oFReader.onload = function(oFREvent) {
			document.getElementById("uploadPreview").src = oFREvent.target.result;
		};
		this.sendPicture(element, 'uploadPreview');
	},
	sendPicture : function(id, target){
		var base_url = this.base_url;
		$('#user_picture_form').ajaxForm({
			target:'#'+target,
			complete : function(data){
				var response = JSON.parse(data.responseText);
				console.log(response);
				var type = response.error == 1 ? 'error' : 'success';
				swal({title: 'Hey', text: response.msg, type: type });

				if(response.fullpath != '')
					document.getElementById("uploadPreview").src = response.fullpath;
			}
		}).submit();
	},
	select : function(id){
		$('#'+id).click();
	}
};

gedtree = {
	init : function(){
		$(".tree").fancytree({
			checkbox : false,
			extensions: ["edit"],
			triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"],
			edit: {
				beforeEdit: function(event, data) {
				},
				save: function(event, data) {

					var par_id = data.node.parent.key.replace('id_', '');

					var dir_segmento_id = $('#segmento_id').val();
					var dir_name = data.input.val();
					var dir_id = data.node.key.replace('id_', '');
					var dir_parent_id = par_id.indexOf("root_") > -1 ? $('#home_id').val()   : par_id;

					$.ajax({
						url  : $('#url').val()+'segmento/inserirdiretorio/',
						data : {
							dir_id : dir_id,
							dir_segmento_id : dir_segmento_id,
							dir_name : dir_name,
							dir_parent_id : dir_parent_id,
							_tree    : 1
						},
						type : 'POST',
						dataType : 'JSON',
						success : function(retorno) {

							var id_diretorio = id_diretorio_segmento;
							$.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

							$.ajax({
								url  : $('#url').val()+'segmento/getdiretorionome/'+id_diretorio,
								type : 'GET',
								success    : function (dados_retorno){
									$('#id_'+id_diretorio).text(dados_retorno);
									var tree = $(".tree").fancytree("getTree");
									tree.reload();

								}
							});
						}
					});
				}
			}
		});


		this.fancyMenu();
	},
	fancyMenu : function(){
		$('.tree').contextmenu({
			delegate: "span.fancytree-node",
			//menu: "#options",
			menu: [
				{title: "Novo Diretório Raiz", cmd: "newRootDir", uiIcon: "ui-icon-folder-collapsed"},
				{title: "Novo Diretório", cmd: "newDir", uiIcon: "ui-icon-folder-collapsed"},
				{title: "Excluir", cmd: "rmDir", uiIcon: "ui-icon-trash" }
			],
			beforeOpen: function(event, ui) {
				console.log(ui);
				var node = $.ui.fancytree.getNode(ui.target);
				//node.setFocus();
				node.setActive();
			},
			select: function(event, ui) {

				var fnc = ui.cmd;
				var fn = window["gedtree"][fnc];
				if (typeof fn === "function") fn();
			}
		});
	},
	fancyDebug : function(stat){
		$.ui.fancytree.debugLevel = stat; // silence debug output
	},
	newRootDir : function() {
		var rootNode = $(".tree").fancytree("getRootNode"),
			dir_name = "Novo diretorio",
			newData = {title: dir_name, folder : true},
			childNode = rootNode.addChildren(newData);
		//console.log(childNode);
		var dir_segmento_id = $('#segmento_id').val();
		var dir_parent_id = $('#home_id').val();

		$.ajax({
				url  : $('#url').val()+'segmento/inserirdiretorio/',
				data : {
					dir_parent_id : dir_parent_id,
					dir_segmento_id : dir_segmento_id,
					dir_name : dir_name,
					_tree : 1
				},
				type : 'POST',
				dataType : 'JSON',
				success : function(retorno) {

					$.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

					$.ajax({
						url  : $('#url').val()+'segmento/diretorios/'+retorno.dir_segmento_id,
						type : 'GET',
						success    : function (dados_retorno){
							$('.tree ul.ui-fancytree-source.ui-helper-hidden').append(dados_retorno);
							var tree = $(".tree").fancytree("getTree");
							tree.reload();

						}
					});

				}
			}
		);
	}, newDir : function(){
		var tree = $(".tree").fancytree("getTree"),
			dir_name = "Novo diretorio",
			node = tree.getActiveNode(),
			newData = {title: dir_name, folder : true},
			newSibling = node.addChildren(newData);
		var id_segmento = $('#segmento_id').val();
		var dir_parent_id = node.key;
		$.ajax({
				url  : $('#url').val()+'segmento/inserirdiretorio/',
				data : {
					dir_parent_id : dir_parent_id.replace('id_', ''),
					dir_segmento_id : id_segmento,
					dir_name : dir_name,
					_tree : 1
				},
				type : 'POST',
				dataType : 'JSON',
				success : function(retorno) {

					$.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

					$.ajax({
						url  : $('#url').val()+'segmento/diretorios/'+retorno.dir_segmento_id,
						type : 'GET',
						success    : function (dados_retorno){
							var obj = $('#'+dir_parent_id + ' ul');
							//console.log(obj.length);
							if (obj.length == 0) {
								alert('node não existe');
								var ulist = document.createElement('ul');
								var ilist = document.getElementById(dir_parent_id);
								ilist.appendChild(ulist);

								$('#'+dir_parent_id+' ul').append(dados_retorno);

							} else {
								$('#'+dir_parent_id + ' ul').append(dados_retorno);
							}
							//console.log($('#'+id_diretorio_segmento_pai+ ' ul'));
							var tree = $(".tree").fancytree("getTree");
							tree.reload();

						}
					});
				}
			}
		);
	},
	rmDir : function(){
		var tree = $(".tree").fancytree("getTree");
		node = tree.getActiveNode();
		id_diretorio_segmento = node.key;

		$.ajax({
			url  : $('#url').val()+'segmento/excluir/',
			data : {
				id_tabela : id_diretorio_segmento.replace('id_','')
			},
			type : 'POST',
			dataType : 'JSON',
			success : function(retorno) {

				//console.log('diretório excluido');
				$.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
				node.remove();
				//console.log(id_diretorio_segmento);
				$('#'+id_diretorio_segmento).remove();

			}
		});
	}
};

gedInvoices = {
	loadClient : function(obj, params){
		var el = obj.srcElement || obj.currentTarget;
		var url = el.getAttribute('data-href');

		$.ajax({
			url : url,
			method : 'post',
			dataType : 'JSON',
			data : {
				'client_id' : params.selected
			},
			success : function(data){
				var inputs = document.getElementsByName('service_id[]');
				for(var i=0;i<inputs.length;i++){
					inputs[i].checked = false;
				}

				var fatura_vencimento = document.getElementById('fatura_vencimento');
				fatura_vencimento.value = data.cliente.client_data_vencimento;

				for(var i=0;i<data.servicos.length;i++){
					var inputEl = document.getElementById('service_id_'+data.servicos[i].servico_id);
					if(inputEl != undefined && inputEl != null)
						inputEl.checked = true;
				}
			}
		});
	}
};

gedmodules = {
	modules :document.getElementsByClassName('module_id'),
	type_id : document.getElementById('type_id'),
	action_id : document.getElementsByClassName('permissao_action_id'),
	action_box : document.getElementById('permission-box'),
	init : function(){
		var modules = this.modules;
		var type_id = this.type_id;
		var action_box = this.action_box;

		for(var i=0;i<modules.length;i++){
			modules[i].addEventListener('change', function(e){
				while (action_box.firstChild) action_box.removeChild(action_box.firstChild);
				$.ajax({
					url : e.currentTarget.getAttribute('data-href'),
					dataType : 'JSON',
					method : 'POST',
					data : {
						type_id : type_id.value,
						module_id : e.currentTarget.value
					},
					success : function(data){

						var list = document.createElement('ul');
						list.className = 'list-group';
						for(var j=0;j<data.length;j++){
							var item_list = document.createElement('li');
							var input_list = document.createElement('input');
							var label_list = document.createElement('label');
							label_list.className="checkbox";
							label_list.setAttribute('for', 'action_id-' + data[j].action_id);
							input_list.setAttribute('type', 'checkbox');
							input_list.setAttribute('name', 'action_id');
							input_list.className = 'permissao_action_id';
							input_list.setAttribute('id', 'action_id-' + data[j].action_id);
							input_list.setAttribute('value', data[j].action_id);
							input_list.checked = data[j].permissao_allow == true ? true : false;

							item_list.className = 'list-group-item';
							label_list.innerHTML = " " + data[j].action_name;
							item_list.appendChild(input_list);
							item_list.appendChild(label_list);
							list.appendChild(item_list);
						}
						action_box.appendChild(list);
					}
				});

			});
		}

	},
	buttonClick : function(){
		var btnSave = document.getElementById('button-save');
		if(btnSave == undefined)
			return false;

		var action_id = this.action_id;
		var type_id = this.type_id;
		var module_id = this.modules;

		var data = new Object();
		var permissions = new Object();
		btnSave.addEventListener('click', function(e){


			for(var l=0;l<module_id.length;l++){
				if(module_id[l].checked){
					data.module_id = module_id[l].value;
				}
			}

			if(data.module_id == undefined){
				gedalert.init('error', 'Ooops!', 'Por favor, selecione um módulo para alterar as permissões');
				return false;
			};
			btnSave.setAttribute('disabled', 'disabled');

			for(var k=0;k<action_id.length;k++){
				permissions['action_id_'+action_id[k].value] = action_id[k].checked;
			}

			data.permissions = permissions;
			data.type_id = type_id.value;

			$.ajax({
				url : e.currentTarget.getAttribute('data-href'),
				dataType : 'JSON',
				method : 'POST',
				data : data,
				success : function(data){
					gedalert.init(data.type, data.title, data.message, data.btnText);
					btnSave.removeAttribute('disabled');
				}
			});


		});
	}
};

gedcep = {
	init : function(){
		var cepElements = document.getElementsByClassName('cep');
		for(var i=0;cepElements.length < i;i++){
			element = cepElements[i];
			cepElements[i].addEventListener('blur', function(element){
				this.search(element);
			});
		}
	},
	search: function(obj){
		val = obj.value.replace(/\D/g, '');

		if (val.length == 8) {
			gedform.disabledFields();
			$.ajax({

				url: document.getElementById('url').value +'cep',
				type: 'POST',
				dataType: 'json',
				data: {cep : val},
				success: function(vet) {
					var dados = vet.dados;
					console.log(vet);
					gedform.enableFields();
					if (vet.error == 1) {
						$.sticky(vet.msg);
						document.getElementById('endereco_logradouro').focus();
					} else {
						document.getElementById('endereco_logradouro').value = dados.tipo_cep + ' ' + dados.logradouro_cep;
						document.getElementById('endereco_bairro').value = dados.bairro_ini_cep;
						document.getElementById('endereco_cidade').value = dados.cidade_cep;
						document.getElementById('endereco_estado').value = dados.uf_cep;
						document.getElementById('endereco_num').focus();
					}

				},
				error : function(){
					gedform.enableFields();
					swal({title: 'Ooooops!', text: 'Não foi possível buscar o endereço. Favor inserir os dados manualmente.', type: 'error', confirmButtonText: 'OK' });
				}

			});

		}
	}
}

gedform = {
	serialize : function(form){

		if (!form || form.nodeName !== "FORM") {
			return;
		}

		var i, j, q = [];

		for (i = form.elements.length - 1; i >= 0; i = i - 1) {

			if (form.elements[i].name === '') {
				continue;
			}

			switch (form.elements[i].nodeName) {

				case 'INPUT':

					switch (form.elements[i].type) {

						case 'text':
						case 'number':
						case 'hidden':
						case 'password':
						case 'button':
						case 'reset':
						case 'submit':
							q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
							break;

						case 'checkbox':
						case 'radio':

							if (form.elements[i].checked) {

								q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');

							}

							break;

					}

					break;

				case 'file':
					break;

				case 'TEXTAREA':
					q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
					break;

				case 'SELECT':

					switch (form.elements[i].type) {

						case 'select-one':
							q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
							break;

						case 'select-multiple':

							for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {

								if (form.elements[i].options[j].selected) {
									q.push('"' + form.elements[i].name + '":"' + form.elements[i].options[j].value + '"');
								}

							}

							break;

					}

					break;

				case 'BUTTON':

					switch (form.elements[i].type) {

						case 'reset':
						case 'submit':
						case 'button':
							q.push('"' + form.elements[i].name + '":"' + form.elements[i].value + '"');
							break;

					}

					break;

			}

		}

		return q.toString();

	},
	disabledFields : function(){
		var elements = document.getElementsByTagName('input');
		for (var i = 0; i < elements.length; i++) {
			elements[i].disabled = true;
		}
	},
	enableFields : function(){
		var elements = document.getElementsByTagName('input');
		for (var i = 0; i < elements.length; i++) {
			elements[i].disabled = false;
		}
	}
}


gedFlist = {
	init: function() {
		var list_source = [];
		$('.user_list li').each(function() {
			var search_name = $(this).find('.sl_name').text();
			list_source.push(search_name);
		});
		$('.user-list-search').typeahead({
			local: list_source,
			limit: 5
		});
		var options = {
				valueNames: ['sl_name', 'sl_status', 'sl_email'],
				page: 10,
				plugins: [
					['paging', {
						pagingClass: "bottomPaging",
						innerWindow: 1,
						outerWindow: 2
					}]
				]
			},
			userList = new List('user-list', options);
		$('#filter-online').on('click', function() {
			$('ul.filter li').removeClass('active');
			$(this).parent('li').addClass('active');
			userList.filter(function(item) {
				return item.values().sl_status == "online";
			});
			return false;
		});
		$('#filter-offline').on('click', function() {
			$('ul.filter li').removeClass('active');
			$(this).parent('li').addClass('active');
			userList.filter(function(item) {
				return item.values().sl_status == "offline";
			});
			return false;
		});
		$('#filter-none').on('click', function() {
			$('ul.filter li').removeClass('active');
			$(this).parent('li').addClass('active');
			userList.filter();
			return false;
		});
		$('#user-list').on('click', '.sort', function() {
			$('.sort').parent('li').removeClass('active');
			if ($(this).parent('li').hasClass('active')) {
				$(this).parent('li').removeClass('active');
			} else {
				$(this).parent('li').addClass('active');
			}
		});
	}
};


ged_crumbs = {
	init: function() {
		$('#jCrumbs').jBreadCrumb({
			endElementsToLeaveOpen: 0,
			beginingElementsToLeaveOpen: 0,
			timeExpansionAnimation: 500,
			timeCompressionAnimation: 500,
			timeInitialCollapse: 500,
			previewWidth: 30
		});
	}
};

//* style switcher
gebo_style_sw = {
	init: function() {
		if($('.style_switcher').length) {
			var base_url = document.getElementById('url').value;
			var $body = $('body');
			var css_folder = $('#css').val();

			$body.append('<a class="ssw_trigger" href="javascript:void(0)"><i class="glyphicon glyphicon-cog"></i></a>');
			var defLink = $('#link_theme').clone();

			//$('input[name=ssw_sidebar]:first,input[name=ssw_layout]:first,input[name=ssw_menu]:first').attr('checked', true);

			$(".ssw_trigger").click(function(){
				$(".style_switcher").toggle("fast");
				$(this).toggleClass("active");
				return false;
			});

			// colors
			$('.style_switcher .jQclr').click(function() {
				$(this).closest('div').find('.style_item').removeClass('style_active');
				$(this).addClass('style_active');
				var style_selected = $(this).attr('title');
				$('#link_theme').attr('href',css_folder+'/'+style_selected+'.css');


				$.ajax({
					url : base_url + 'configuracao/switch_theme/' + style_selected
				});

			});

			// backgrounds
			$('.style_switcher .jQptrn').click(function(){
				$(this).closest('div').find('.style_item').removeClass('style_active');
				$(this).addClass('style_active');
				var style_selected = $(this).attr('title');
				$.ajax({
					url : base_url + 'configuracao/switch_background/' + style_selected
				});
				if($(this).hasClass('jQptrn')) { $('body').removeClass('ptrn_a ptrn_b ptrn_c ptrn_d ptrn_e').addClass(style_selected); }


			});
			//* layout
			$('input[name=ssw_layout]').click(function(){
				var layout_selected = $(this).val();
				$body.removeClass('gebo-fixed').addClass(layout_selected);
			});
			//* sidebar position
			$('input[name=ssw_sidebar]').click(function(){
				var sidebar_position = $(this).val();
				$body.removeClass('sidebar_right');
				if(sidebar_position != '' && sidebar_position != null){
					$body.addClass(sidebar_position);
					$('#footer').addClass('footer-left');
				}

				$(window).resize();

				var base_url = document.getElementById('url').value;
				$.ajax({
					url : base_url + 'configuracao/set_sidebar/' + sidebar_position
				});

			});

			//* reset
			$('#resetDefault').click(function(){
				$('body').attr('class', '');
				console.log(defLink);
				$('.style_item').removeClass('style_active').filter(':first-child').addClass('style_active');

				$('#link_theme').replaceWith(defLink);
				$('#link_theme').attr('href',css_folder+'/'+$('.style_item:first-child').attr('title')+'.css');
				var base_url = document.getElementById('url').value;

				$.ajax({
					url : base_url + 'configuracao/switch_theme/' + $('.style_item:first-child').attr('title')
				});

				$.ajax({
					url : base_url + 'configuracao/set_sidebar/'
				});


				$('.ssw_trigger').removeClass('active');
				$(".style_switcher").hide();
				return false;
			});
			//* menu show
			$('input[name=ssw_menu]').click(function(){
				var menu_show = $(this).val();
				$body.removeClass('menu_hover').addClass(menu_show);
			});

		}
	}
};

var goToHref = function(element){
	var el = element;
	var href = el.dataset.href;
	if(href != undefined && href != null)
		window.location.href=href;
}

jQuery(document).ready(function($){
	var body = $( 'body' );

	gebo_style_sw.init();
	$('#service_id').on('change', function(evt, params) {

		var options = evt.currentTarget.options;
		var result = [];
		var sum = 0;

		for (var i=0, iLen=options.length; i<iLen; i++) {
			opt = options[i];

			if (opt.selected) {
				sum += parseFloat(opt.getAttribute('data-value'));
			}
			var targetId = opt.getAttribute('data-target');
		}

		document.getElementById(targetId).value = sum.toFixed(2);

	});



	gedflist.init();

	gedmodules.init();
	gedmodules.buttonClick();
	//you can now use $ as your jQuery object.
	gedcep.init();
	gedchosen.init();
	gedswitch.init();
	gedpeity.init();
	gedsidebar.init();

	gedmask.init();
	gedtree.fancyDebug(0);
	gedtree.init();
	ajaxlink.init();
	gedtable.sync();
	ged_crumbs.init();

	if(document.getElementById('plan_desc'))
		CKEDITOR.replace( 'plan_desc' );


	var base_css = document.getElementById('css').value;

	$('.datatable').dataTable({
		"sDom": "T<'clearfix'><'row'<'col-sm-6'<'dt_actions'>l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
		"sPaginationType": "bootstrap",
		"tableTools": {
			"sSwfPath": base_css + "/swf/copy_csv_xls_pdf.swf"
		},
		language : {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
		},
	});

	$('.add-tag').click(function(){
		addNewTag($(this));
	});

	$('tbody').on('click', '.remove-tag', function(event){
		event.preventDefault();
		removeTag($(this));
	});

	function addNewTag($element) {
		var $input = $element.parent('.input-group-btn').siblings('input');

		if ($input.val() === '') {
			return false;
		}

		var $tbody = $('#'+$element.data('table')+' tbody');
		var tr = '<tr>'+
					'<td>'+$input.val()+'</td>'+
					'<input type="hidden" name="'+$element.data('scanner')+'[]"value="'+$input.val()+'">'+
					'<td class="text-center"><a class="remove-tag" href="#">Excluir</a></td>'+
				'</tr>';

		$input.val('');
		$tbody.find('.none-tag').remove();
		$tbody.append(tr);
	}

	function removeTag($element) {
		$element.parents('tr').remove();
	}

});

