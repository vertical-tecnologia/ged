var base_url = document.getElementById('url').value;

var $filequeue,
    $filelist;

$(document).ready(function() {
    $filequeue = $(".filelist.queue");
    $filelist = $(".filelist.complete");
    var curdir = document.getElementById('curdir');
    var maxSize = document.getElementById('max');
    var data = {
        folder : curdir != null && curdir != undefined ? curdir.value : null
    };

    $(".upload").upload({
        maxSize: maxSize != null ? maxSize.value : 0,
        postData : data,
        maxQueue: 10
    }).on("start.upload", onStart)
        .on("complete.upload", onComplete)
        .on("filestart.upload", onFileStart)
        .on("fileprogress.upload", onFileProgress)
        .on("filecomplete.upload", onFileComplete)
        .on("fileerror.upload", onFileError);
});

function onStart(e, files) {
    console.log("Start");
    var html = '';
    for (var i = 0; i < files.length; i++) {
        html += '<li data-index="' + files[i].index + '"><span class="file">' + files[i].name + '</span>' +
        '<div class="progress-wrap progress" data-progress-percent="0"><span class="percent">0%</span><div class="progress-bar progress"></div></div>' +
        '</li>';
    }
    $filequeue.append(html);
}

function onComplete(e) {
    console.log("Complete");
    // All done!
}

function onFileStart(e, file) {
    console.log("File Start");
    $filequeue.find("li[data-index=" + file.index + "]")
        .find(".percent").text("0%");
}

function onFileProgress(e, file, percent) {
    console.log("File Progress");
    $filequeue.find("li[data-index=" + file.index + "]")
        .find(".percent").text(percent+"%");

    $filequeue.find("li[data-index=" + file.index + "]")
        .find('.progress-wrap').attr('data-progress-percent', percent);

    var getPercent = percent / 100;
    var getProgressWrapWidth = $('.progress-wrap').outerWidth();
    var progressTotal = getPercent * getProgressWrapWidth;
    var animationLength = 0;

    // on page load, animate percentage bar to data percentage length
    // .stop() used to prevent animation queueing
    $filequeue.find("li[data-index=" + file.index + "]")
        .find('.progress-bar').css({
        left: progressTotal
    });

}

function onFileComplete(e, file, response) {
    console.log("File Complete");
    console.log(typeof response);
    console.log(response);

    if(typeof response == 'object' && response.error == undefined){
        var $target = $filequeue.find("li[data-index=" + file.index + "]");
        $target.find(".file").text(file.name);
        $target.find(".progress").addClass('success')//.text('Enviado')
        $target.appendTo($filelist);
    }else if(typeof response == 'object' && response.error != undefined){
        swal({title: "Erro!", text: response.error, type: "error", confirmButtonText: "Ok" });
        $filequeue.find("li[data-index=" + file.index + "]")
            .find(".progress").text('Erro').addClass('error');
    } else {
        swal({title: "Erro!", text: 'Resposta invalida do sevidor.', type: "error", confirmButtonText: "Ok" });
        $filequeue.find("li[data-index=" + file.index + "]")
            .find(".progress").text('Erro').addClass('error');
    }


}

function onFileError(e, file, error) {
    if(error == 'Too large')
        error = 'O arquivo excede o tamanho máximo permitido ou o seu espaço não é suficiente para armazenar o arquivo.';

    if(typeof error == 'object')
        swal({title: "Erro!", text: error.msg, type: "error", confirmButtonText: "Ok" });
    else
        swal({title: "Erro!", text: error, type: "error", confirmButtonText: "Ok" });
    $filequeue.find("li[data-index=" + file.index + "]")
        .find(".progress").text('Erro').addClass('error');
}
