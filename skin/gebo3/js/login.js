window.ParsleyValidator.addCatalog('pt-br', window.ParsleyConfig.i18n['pt-br'], true);
window.ParsleyValidator.setLocale('pt-br');

var fill_contract = function() {

    $('input, select').each(function( index ) {

        var el_id = $(this).attr('id');

        if ($(this).attr('type') == 'text' || $(this).attr('type') == 'number' && $(this).attr('type') != 'password') {

            $('#c_'+$(this).attr('id')).html($(this).val());

        } else if($(this).prop('tagName') == 'select'){

            $('#c_'+el_id).html($('#'+el_id+' option:selected').text());

        } else if($(this).prop('type') == 'checkbox'){

            if ($(this).is(':checked')) {

                $('#c_'+el_id).html(' x ');

            }

        } else if($(this).attr('type') == 'hidden') {

            if (document.getElementById('val_'+el_id) != undefined) {

                $('#c_val_'+el_id).html($('#val_'+el_id).text());
                $('#c_valor_'+el_id).html($('#val_valor_'+el_id).text());

            }

        }

        $('#contrato_cliente_servico').val($('.contrato').html());

    });

};

$('select, input[type="checkbox"]').change(function(){
    fill_contract();
});

$('input').blur(function(){
    fill_contract();
});

fill_contract();


$(":checkbox, .to-labelauty").labelauty();

$('.service-checkbox').on('change', function(){
    var element = $(this);

    if(element.context.checked == true){
        $('.service-checkbox').attr('checked', false);
        document.getElementById(element.context.id).checked = true;
        if(element.context.value != 'service_str'){
            $('#service-plan').slideUp();
            $('#id_servico').attr('disabled', true);
        } else {
            $('#service-plan').slideDown();
            $('#id_servico').removeAttr('disabled');
        }

    }

});

$("[data-slider]")
    .each(function () {
        var input = $(this);
        $("<span>")
            .addClass("output")
            .insertAfter($(this));
    })
    .bind("slider:ready slider:changed", function (event, data) {

        var valor_servico;
        $.each(servicos, function(index, value) {

            if (servicos[index].qtd_servico == data.value.toFixed(2)) {

                $('#id_servico').val(servicos[index].id_servico);
                valor_servico = servicos[index].valor_servico;

            }

        });

        $('#servico').val(data.value);
        $('#val_id_servico').html(getBytesWithUnit(1024 * 1024 * data.value));
        $('#val_valor_id_servico').html(' Valor: R$ ' + valor_servico);

    }
);

$('.cep').blur(function(e){
    var element = e.currentTarget;
    var cep = element.value;
    cep = cep.replace(/\D/g, "");

    if(cep.length == 8){
        var logradouro_empresa = document.getElementById('logradouro_empresa') != null ? document.getElementById('logradouro_empresa') : document.getElementById('logradouro_responsavel');
        var numero_empresa = document.getElementById('numero_empresa') != null ? document.getElementById('numero_empresa') : document.getElementById('numero_responsavel');
        var bairro_empresa = document.getElementById('bairro_empresa') != null ? document.getElementById('bairro_empresa') : document.getElementById('bairro_responsavel');
        var cidade_empresa = document.getElementById('cidade_empresa') != null ? document.getElementById('cidade_empresa') : document.getElementById('cidade_responsavel');
        var uf_endereco = document.getElementById('uf_endereco');

        logradouro_empresa.setAttribute('disabled', '');
        numero_empresa.setAttribute('disabled', '');
        bairro_empresa.setAttribute('disabled', '');
        cidade_empresa.setAttribute('disabled', '');
        uf_endereco.setAttribute('disabled', '');

        $.ajax({
            method : "POST",
            dataType : 'JSON',
            url : element.getAttribute('cep-url'),
            data : {"cep" : cep}
        }).done(function(data){

            logradouro_empresa.removeAttribute('disabled');
            numero_empresa.removeAttribute('disabled');
            bairro_empresa.removeAttribute('disabled');
            cidade_empresa.removeAttribute('disabled');
            uf_endereco.removeAttribute('disabled');

            if(data.error == 0){

                logradouro_empresa.value = data.dados.logradouro_cep;
                bairro_empresa.value = data.dados.bairro_ini_cep;
                cidade_empresa.value = data.dados.cidade_cep;
                uf_endereco.value = data.dados.uf_cep;

                numero_empresa.focus();

            } else {
                logradouro_empresa.value = "";
                bairro_empresa.value = "";
                cidade_empresa.value = "";
                uf_endereco.value = "";
                logradouro_empresa.focus();
                alertify.log(data.msg);
            }


        });
    }

});

$('.cnpj, .cpf').blur(function(e){
    var element = e.currentTarget;
    var cnpj_cpf = element.value;
    if(cnpj_cpf.length == 18 || cnpj_cpf.length == 14){
        cnpj_cpf = cnpj_cpf.replace(/\D/g,"");

        $.ajax({
            method: "POST",
            dataType : 'JSON',
            url: element.getAttribute('validation-url'),
            data: { "value" : cnpj_cpf }
        })
        .done(function( data ) {
            if(data.exists == true){
                alertify.set({labels :{ok : 'OK'}})
                alertify.alert(data.msg);
                element.value='';
                element.focus();
            }
        });

    }
});

$('#termos').on('change', function(e){
    if(this.checked == true){
        $('#finish-registration').removeAttr('disabled');
    } else {
        $('#finish-registration').attr('disabled', '');
    }
});
$('#finish-registration').attr('disabled', '');
$('#finish-registration').on('click', function(e){
    var element = this;
    console.log(this);
    var contract = document.getElementById('termos');
    if(contract.checked == false){
        element.setAttribute('disabled', '');
        e.preventDefault();
        alertify.error("Você precisa aceitar os termos do contrato para continuar.");
    }
});

/**
 * @function: getBytesWithUnit()
 * @purpose: Converts bytes to the most simplified unit.
 * @param: (number) bytes, the amount of bytes
 * @returns: (string)
 */
var getBytesWithUnit = function( bytes ) {

    if (isNaN(bytes)) {
        return;
    }

    var units = [ ' bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB' ];
    var amountOf2s = Math.floor( Math.log( +bytes )/Math.log(2) );

    if (amountOf2s < 1) {
        amountOf2s = 0;
    }

    var i = Math.floor( amountOf2s / 10 );
    bytes = +bytes / Math.pow( 2, 10*i );

    // Rounds to 3 decimals places.
    if (bytes.toString().length > bytes.toFixed(3).toString().length) {
        bytes = bytes.toFixed(3);
    }

    return bytes + units[i];

};
if ($('#simple_wizard')) {

    $('.next').on('click, touch, keypress', function () {
        var current = $(this).data('currentBlock'),
               next = $(this).data('nextBlock');


        // only validate going forward. If current group is invalid, do not go further
        // .parsley().validate() returns validation result AND show errors

        if (false === $('#simple_wizard').parsley().validate('block' + current))
            return;
        // validation was ok. We can go on next step.
        $('.block' + current)
            .removeClass('show')
            .addClass('hidden');

        $('.block' + next)
            .removeClass('hidden')
            .addClass('show');

    });

}



$('.email').blur(function(){
    element = $(this);
    var email = element.context.value;
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var valid = re.test(email);
    if(valid == true){
        $.ajax({
            method: "POST",
            dataType : 'JSON',
            url: element.context.getAttribute('validation-url'),
            data: { "value" : email }
        })
            .done(function( data ) {
                if(data.exists == true){
                   alertify.set({labels :{ok : 'OK'}})
                   alertify.alert(data.msg);
                   element.context.value='';
                   element.context.focus();
                }
            });

    }
});