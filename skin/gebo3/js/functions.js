//JQuery
$(function() {

    //DATEPICKER

  $.datepicker.regional[""].dateFormat = 'dd/mm/yy';

  $( ".data" ).datepicker(getDataTraducao());

  $(".cep").inputmask("99999-999");

  $(".real").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: false, allowZero: true, allowNegative: true});

});

function tamanhoPagina() {

    var myWidth = 0, myHeight = 0;

    if (typeof( window.innerWidth ) == 'number') {//Não-IE
        myHeight = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ) {//IE 6+ in 'standards compliant mode'
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight )) {//IE 4 compatible
        myHeight = document.body.clientHeight;
    }

    if (document.getElementById("dicas")) {

        myHeight = myHeight - 296;
        document.getElementById("conteudo").style.height = myHeight + "px";
        document.getElementById("dicas").style.height    = (document.getElementById("toolbar").clientHeight - 120) + "px";

    } else {

        if (document.getElementById("conteudo").clientHeight < 735) {
            if (document.getElementById("conteudo").clientHeight > 450) {
                myHeight = document.getElementById("conteudo").clientHeight;//VALIDAR
            } else {
                myHeight -= 196;
            }
        } else {
            myHeight = document.getElementById("conteudo").clientHeight;
        }

        document.getElementById("conteudo").style.height = myHeight + "px";

    }

}

function troca_src(obj,img) {
    obj.src = img;
}

function validaLogin() {

    msg = '';

    if (document.getElementById("login_usuario").value == "") {
        msg += "<span>Campo Login não preenchido</span><br />";
    }

    if (document.getElementById("senha_usuario").value == "") {
        msg += "<span>Campo Senha não preenchido</span><br />";
    }

    if (msg != "") {
        msg = '<div id="topoValida">O seguinte campo encontra-se com problemas:</div>'+msg;
        jQuery.facebox(msg);
        return false;
    } else {
        return true;
    }

}

function tipoColuna() {

    //TODO - ADD ATRIBUTO MAXLENGTH, DINAMICO = AO TIPO
    //$("#buscar").attr('maxlength',10);

    var index = document.getElementById('nome_coluna').selectedIndex;
    var tipo  = document.getElementById('nome_coluna').options[index].getAttribute('tipo');
    var fake  = document.getElementById('busca_fake').innerHTML;

    fake = fake.replace('campo_fake','buscar');
    fake = fake.replace('campo_fake','buscar');

    if (tipo == 'data') {//ok

        document.getElementById('busca').innerHTML = fake;
        $("#buscar").attr('maxlength',10);
        $("#buscar").attr('onkeypress','return mascaraData(this, event)');
        $("#buscar").datepicker(getDataTraducao());

    } else if (tipo == 'numero') {

        document.getElementById('busca').innerHTML = fake;
        $("#buscar").attr('onkeypress','return soNumero(event)');

    } else if (tipo == 'real') {

        document.getElementById('busca').innerHTML = fake;
        $("#buscar").maskMoney({symbol:"R$",decimal:",",thousands:"."});

    } else if (tipo == 'peso') {

        document.getElementById('busca').innerHTML = fake;
        $("#buscar").maskMoney({symbol:"", decimal:",",thousands:"",precision:2})

    } else if (tipo == 'letra') {

        document.getElementById('busca').innerHTML = fake;
        $("#buscar").attr('onkeypress','return soLetra(event)');

    } else {

        document.getElementById('busca').innerHTML = fake;

    }

    document.getElementById('tipo_coluna').value = tipo;

}

function limparLogin() {

    document.getElementById("login_usuario").value = '';
    document.getElementById("senha_usuario").value = '';

}

function limparPesquisa() {

    var fake  = document.getElementById('busca_fake').innerHTML;

    fake = fake.replace('campo_fake','buscar');
    fake = fake.replace('campo_fake','buscar');

    document.getElementById('busca').innerHTML   = fake;
    document.getElementById("nome_coluna").value = '';
    document.getElementById('tipo_coluna').value = '';
    document.getElementById('campo_fake').value  = '';
    document.getElementById('buscar').value      = '';

}

function busca(tela) {

    document.getElementById("frm_grid").action = document.getElementById("url").value + "index.php/" + tela + "/listar";
    document.frm_grid.submit();

}

function check_all() {

    for (var i = 0; i < document.getElementById("total_reg").value; i++) {

        if (document.getElementById('checa_todos').checked == false) {
            document.getElementById("id_tabela_"+i).checked = false;
        } else {
            document.getElementById("id_tabela_"+i).checked = true;
        }

    }

}

function ativarUsuario() {

    document.getElementById("frm_grid").action = document.getElementById("url").value + "index.php/admin/ativar";
    document.frm_grid.submit();

}

function bloquearUsuario() {

    document.getElementById("frm_grid").action = document.getElementById("url").value + "index.php/admin/bloquear";
    document.frm_grid.submit();

}

function deletar() {

    var count = 0;

    for (var i = 0; i < document.getElementById("total_reg").value; i++) {

        if (document.getElementById("id_tabela_"+i).checked == true) {
            count++;
        }

    }

    if (count > 0) {

        smoke.confirm('Você tem certeza que deseja excluir este registro?<br /><br />',function(e) {
            if (e) {

                document.frm_grid.submit();

            }

        }, {ok:"Excluir", cancel:"Cancelar"});

    } else {

        var msg = "Para excluir você deve selecionar algum registro!<br /><br /><b>Nenhum registro Selecionado!</b><br /><br />";
        smoke.alert(msg, {ok:"Fechar"});

    }

}

function getGaleria(id) {

    $.ajax({
        type: "POST",
        url: document.getElementById("url").value+"index.php/galeria/index/"+id,
        success: function(msg) {
            jQuery.facebox(msg);
        }
    });

}

function mascaraData(data, event) {

    if (soNumero(event) == false) {
        return false;
    }

    return formataCampo(data, '00/00/0000', event);

}

//formata de forma generica os campos
function formataCampo(campo, Mascara, event) {

    var boleanoMascara;
    var tecla = event.keyCode ? event.keyCode : event.which ? event.which : void 0;

    exp = /\-|\.|\/|\(|\)| /g;
    campoSoNumeros = campo.value.toString().replace(exp, "");

    var posicaoCampo   = 0;
    var NovoValorCampo = '';
    var TamanhoMascara = campoSoNumeros.length;

    if (tecla != 8) { // backspace

        for(i = 0; i <= TamanhoMascara; i++) {

            boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                || (Mascara.charAt(i) == "/"))
            boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(")
                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))

            if (boleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                TamanhoMascara++;
            } else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
            }

        }

        campo.value = NovoValorCampo;
        return true;

    } else {

        return true;

    }

}

function verificaData(data) {

    // Formato dd/mm/yyyy
    vet = data.split("/");

    dia = vet[0];
    mes = vet[1];
    ano = vet[2];

    msg = '';

    if ((dia < '01')||(dia < '01' || dia > 30) && (  mes == '04' || mes == '06' || mes == '09' || mes == 11 ) || dia > 31) {
        msg = "Dia inválido";
    } else if (mes < 01 || mes > 12 ) {
        msg = "Mês inválido";
    } else if (mes == 2 && ( dia < 01 || dia > 29 || ( dia > 28 && (parseInt(ano / 4) != ano / 4)))) {
        msg = "Dia inválido, ano não é Bissexto";
    }

    return msg;

}

function soLetra(event) {

    var vet = new Array();
    vet[0]  = 0;//setas <- ->
    vet[1]  = 8;//backspace
    vet[2]  = 32;//espaço
    vet[3]  = 192;//À
    vet[4]  = 193;//Á
    vet[5]  = 194;//Â
    vet[6]  = 195;//Ã
    vet[7]  = 196;//Ä
    vet[8]  = 199;//Ç
    vet[9]  = 200;//È
    vet[10] = 201;//É
    vet[11] = 202;//Ê
    vet[12] = 203;//Ë
    vet[13] = 204;//Ì
    vet[14] = 205;//Í
    vet[15] = 206;//Î
    vet[16] = 207;//Ï
    vet[17] = 209;//Ñ
    vet[18] = 210;//Ò
    vet[19] = 211;//Ó
    vet[20] = 212;//Ô
    vet[21] = 213;//Õ
    vet[22] = 214;//Ö
    vet[23] = 217;//Ù
    vet[24] = 218;//Ú
    vet[25] = 219;//Û
    vet[26] = 220;//Ü
    vet[27] = 224;//à
    vet[28] = 225;//á
    vet[29] = 226;//â
    vet[30] = 227;//ã
    vet[31] = 228;//ä
    vet[32] = 231;//ç
    vet[33] = 232;//è
    vet[34] = 233;//é
    vet[35] = 234;//ê
    vet[36] = 235;//ë
    vet[37] = 236;//ì
    vet[38] = 237;//í
    vet[39] = 238;//î
    vet[40] = 239;//ï
    vet[41] = 241;//ñ
    vet[42] = 242;//ò
    vet[43] = 243;//ó
    vet[44] = 244;//ô
    vet[45] = 245;//õ
    vet[46] = 246;//ö
    vet[47] = 249;//ù
    vet[48] = 250;//ú
    vet[49] = 251;//û
    vet[50] = 252;//ü
    vet[51] = 360;//Ũ
    vet[52] = 361;//ũ
    vet[53] = 9;//tab
    vet[54] = 39;//->
    vet[55] = 37;//<-

    var tecla = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

    var caract = new RegExp(/^[a-z]+$/i);
    caract = caract.test(String.fromCharCode(tecla));

    if (!caract) {

        if (caract == '%') return false;

        for (var i = 0; i < vet.length; i++) {
            if (tecla == vet[i]) return true;
        }

        return false;

    } else {
        return true;
    }

}

function soNumero(event) {

    var vet = new Array();
    vet[0]  = 0;//setas <- ->
    vet[1]  = 8;//backspace
    vet[2]  = 9;//tab
    vet[3]  = 39;//->
    vet[4]  = 37;//<-

    var tecla = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

    var caract = new RegExp(/^[0-9]+$/i);
    caract = caract.test(String.fromCharCode(tecla));

    if (!caract) {

        if (caract == '%') return false;

        for (var i = 0; i < vet.length; i++) {
            if (tecla == vet[i]) return true;
        }

        return false;

    } else {
        return true;
    }

}

function getDataTraducao() {

    return {
        dateFormat: 'dd/mm/yy',
        autoClose: true,
        dayNames: [
            'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
        ],
        dayNamesMin: [
            'D','S','T','Q','Q','S','S','D'
        ],
        dayNamesShort: [
            'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
        ],
        monthNames: [
            'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
            'Outubro','Novembro','Dezembro'
        ],
        monthNamesShort: [
            'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'
        ],
        nextText: '>',
        prevText: '<'
    };

}

function FormataCpfCnpj(obj) {

    //Remove tudo o que não é dígito
    v = obj.value.replace(/\D/g,"");

    if (v.length <= 11) { //CPF

        //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d)/,"$1.$2");

        //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
        v = v.replace(/(\d{3})(\d)/,"$1.$2");

        //Coloca um hífen entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

    } else { //CNPJ

        //Coloca ponto entre o segundo e o terceiro dígitos
        v = v.replace(/^(\d{2})(\d)/,"$1.$2");

        //Coloca ponto entre o quinto e o sexto dígitos
        v = v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");

        //Coloca uma barra entre o oitavo e o nono dígitos
        v = v.replace(/\.(\d{3})(\d)/,".$1/$2");

        //Coloca um hífen depois do bloco de quatro dígitos
        v = v.replace(/(\d{4})(\d)/,"$1-$2");

    }

    obj.value = v;

}
