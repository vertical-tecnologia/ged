gedValidate = {
    init :function(){
        $("#reg-form").validate({
            rules : {
                email :{
                    required: true
                },
                login_usuario : {
                    required: true
                },
                senha_usuario : {
                    required: true
                },
                user_fullname : {
                    required : true
                },
                user_login : {
                    required : true,
                    minlength : 6
                },
                user_password : {
                    required : true,
                    minlength: 6
                },
                user_password_confirmation : {
                    required : true,
                    minlength: 6,
                    equalTo: "#user_password"
                },
                user_email : {
                    required : true,
                    email : true
                },
                t_and_c: {
                    required : true
                }
            },
            messages : {
                login_usuario : {
                    required: 'Informe seu login'
                },
                senha_usuario : {
                    required: 'Informe sua senha'
                },
                user_fullname : {
                    required : 'Informe seu nome.'
                },
                user_login : {
                    required : 'Escolha um nome de usuario',
                    minlength : 'O nome de usuário deve ter pelo menos 6 caracteres'
                },
                user_password : {
                    required : 'Escolha uma senha',
                    minlength : 'A senha deve ter pelo menos 6 caracteres'

                },
                user_password_confirmation : {
                    required : 'Confirme a senha',
                    equalTo : 'Senhas não conferem',
                    minlength : 'Confirmação da senha deve ter pelo menos 6 caracteres'
                },
                email : {
                    required : 'Informe seu email',
                    email : 'Email invalido'
                },
                user_email : {
                    required : 'Informe seu email',
                    email : 'Email invalido'
                },
                t_and_c : {
                    required : "Você tem que aceitar os termos de uso."
                }
            }
        });
    }
};

// preloader
$(window).load(function(){
    $('.preloader').fadeOut(1000); // set duration in brackets    
});

$(function() {
    new WOW().init();
    $('.templatemo-nav, .soft-scroll').singlePageNav({
    	offset: 70,
        filter: ':not(.external)'
    });

    $.cookie('gebo_sidebar', null);


    /* Hide mobile menu after clicking on a link
    -----------------------------------------------*/
    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });

    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'fa fa-check-circle'
                },
                off: {
                    icon: 'fa fa-circle-thin'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {
            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});