<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class syslog
 * @package		App\Libraries
 */

class syslog {

    public static function generate_log($operation)
    {
        $CI =& get_instance();
        //  $this->agent->version()
        $data = array(
            "user_name" => $CI->session->userdata("USER_NAME"),
            "operation" => $operation,
            "ip_address" => $_SERVER['SERVER_ADDR'],
            "browser" => $CI->agent->browser(),
            "browser" => $CI->agent->browser(),
            "client_id" => $CI->session->userdata('CLID')
        );

        $CI->db->insert("log_history", $data);

        if ($CI->db->insert_id() > 0)
            return true;
        else
            return false;

    }

}
