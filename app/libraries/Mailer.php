<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Libraries
 * Date: 16/09/15
 * Time: 11:56
 */
class Mailer
{

    public static $message;
    public static $to;
    public static $toName;
    public static $subject;

    public static function send_mail() {
        $mail = new PHPMailer();
        $mail->SMTPDebug = false;
        $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
        $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
        $mail->SMTPSecure = "ssl"; //Estabelecemos qual protocolo de segurança será usado.
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Host = "cpl01.main-hosting.eu"; //Podemos usar o servidor do gMail para enviar.

        $mail->Port = 465; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = "ged@gedcloud.com.br"; //Usuário do gMail
        $mail->Password = "centrall1"; //Senha do gMail
        $mail->SetFrom('ged@gedcloud.com.br', 'GED - Contato'); //Quem está enviando o e-mail.
        $mail->AddReplyTo("ged@gedcloud.com.br","GED - Contato"); //Para que a resposta será enviada.
        $mail->Subject = self::$subject; //Assunto do e-mail.
        $mail->Body = self::$message;
        $mail->AddAddress(self::$to, self::$toName);

        if(!$mail->Send()) {
            return $data["message"] = "ocorreu um erro durante o envio: " . $mail->ErrorInfo;
        } else {
            return true;
        }

    }
}