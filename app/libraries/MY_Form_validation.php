<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class MY_Form_validation
 * @package		App\Libraries
 */

class MY_Form_validation extends CI_Form_validation 
{

    protected $CI;

    public function __construct($rules = array())
    {
        parent::__construct($rules);
        $this->CI =& get_instance();
        log_message('debug', '*** Hello from MY_Form_validation ***');
    }

    public function unique_cnpj_cpf($argCnpjCPF)
    {
        $argCnpjCPF = str_replace(array('.', '-', '/'), '', $argCnpjCPF);
        $where = array('empresa_cnpj' => $argCnpjCPF);

        if (strlen(trim($argCnpjCPF)) == 11)
            $where = array('empresa_cpf_responsavel' => $argCnpjCPF);

        if (isset($_POST['empresa_id']) && $_POST['empresa_id'])
            $where['empresa_id !='] = $_POST['empresa_id'];

        $this->CI->db->from('empresa');
        $this->CI->db->where($where);

        if ($this->CI->db->count_all_results() > 0 || empty($argCnpjCPF)) {
            $this->CI->form_validation->set_message(__FUNCTION__, '%s já cadastrado.');
            return false;
        }
        return true;
    }

    public function unique_rg($argRG)
    {
        $argRG = str_replace(array('.', '-'), '', $argRG);
        $where = array('empresa_rg_responsavel' => $argRG);
        
        if (isset($_POST['empresa_id']) && $_POST['empresa_id'])
            $where['empresa_id !='] = $_POST['empresa_id'];

        $this->CI->db->from('empresa');
        $this->CI->db->where($where);

        if ($this->CI->db->count_all_results() > 0 || empty($argRG)) {
            $this->CI->form_validation->set_message(__FUNCTION__, '%s já cadastrado.');
            return false;
        }
        return true;
    }

}