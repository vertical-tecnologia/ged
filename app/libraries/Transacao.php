<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Transacao
 * @package		App\Libraries
 */

class Transacao {

    public static function log_historico($sql, $tipo) { 
        
		$CI =& get_instance();
        $db = $CI->load->database('zorbit', true);

        $db->set('ID_USUARIO', $CI->session->userdata('id_usuario'));
        $db->set('SQL_HISTORICO_TRANSACAO', $sql);
        $db->set('TIPO_HISTORICO_TRANSACAO', $tipo);
        $db->insert('ZORBIT_HISTORICO_TRANSACAO');

        return $CI->db->insert_id() > 0 ? true : false;

    }

}

?>
