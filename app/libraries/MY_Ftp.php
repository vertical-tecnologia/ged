<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Ftp extends CI_FTP 
{
	public function __construct()
	{
		parent::__construct();
	}

	private function setFtp($ftp)
	{
		$config['hostname'] = $ftp['server_address'];
		$config['username'] = $ftp['server_username'];
		$config['password'] = $ftp['server_password'];
		$config['port']     = $ftp['server_port'];
		$config['passive']  = TRUE;
		$config['debug']    = FALSE;
		return $config;
	}

	public function testConnection($ftp)
	{
		$config = $this->setFtp($ftp);
		$connected = $this->connect($config);
		$this->close();
		return $connected;
	}

	public function connection($ftp)
	{
		$config = $this->setFtp($ftp);
		return $this->connect($config);
	}

	public function sizeFile($path)
	{
		return ftp_size($this->conn_id, $path);
	}

	public function list_files_dirs($path = '.')
	{
		if (!$this->_is_conn())
			return FALSE;

		return ftp_nlist($this->conn_id, $path);
	}
}
