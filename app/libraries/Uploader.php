<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Libraries
 * Date: 20/06/15
 * Time: 00:10
 */

class Uploader {

    public $driver;
    protected $key;
    private $securekey, $iv;


    public $original_name;
    public $filename;
    public $filesize;
    public $filemime;
    public $fileextension;
    public $basepath;
    public $basename;
    public $encoding;
    public $tmpname;
    public $isimage;
    public $imagewidth;
    public $imageheight;
    public $filetags;

    public function __construct($driver = null)
    {
        $this->key = defined('APP_KEY') ? APP_KEY : 'tQ9mSQg0Mn8QQJW7Mm4uTDqpJWJjBjd3';
        $this->driver = $driver != null ? $driver : __DIR__.'../upload';
        $this->securekey = hash('sha256', $this->key, TRUE);
        $this->iv = mcrypt_create_iv(32);
    }


    public function driver()
    {
        return $this->driver;
    }

    public function hash($input) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
    }
    public function realpath($input) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
    }

    public function newName($name)
    {
        return hash('sha512', $name.time());
    }

    public function file_info()
    {
        
    }

    private function setError($error)
    {
        switch($error){
            case 0:
                $this->errors[] = 'UPLOAD_ERR_OK';
                break;
            case 1:
                $this->errors[] = 'UPLOAD_ERR_INI_SIZE';
                break;
            case 2:
                $this->errors[] = 'UPLOAD_ERR_FORM_SIZE';
                break;
            case 3:
                $this->errors[] = 'UPLOAD_ERR_PARTIAL';
                break;
            case 4:
                $this->errors[] = 'UPLOAD_ERR_NO_FILE';
                break;
            case 6:
                $this->errors[] = 'UPLOAD_ERR_NO_TMP_DIR';
                break;
            case 7:
                $this->errors[] = 'UPLOAD_ERR_CANT_WRITE';
                break;
            case 8:
                $this->errors[] = 'UPLOAD_ERR_EXTENSION';
                break;
        }

    }
    public function do_upload($field = 'file')
    {

        if(!isset($_FILES[$field])){
            die;
        }

        if($_FILES[$field]['error'] != 0){
            $this->setError($_FILES[$field]['error']);
            return false;
        }

        $this->original_name = $_FILES[$field]['name'];
        $this->tmpname = $_FILES[$field]['tmp_name'];
        $this->filesize = $_FILES[$field]['size'];
        $this->filemime = $this->get_filemime($this->tmpname);
        $this->encoding = $this->get_encoding($this->tmpname);
        $this->fileextension = $this->get_fileextension();
        $this->isimage = $this->is_image($this->tmpname);
        $this->filename = $this->newName($this->original_name);

        $move = $this->move_file();

        if($move === true){
            return array(
                'original_name' => $this->original_name,
                'filename' => $this->filename,
                'tmpname' => $this->tmpname,
                'filesize' => $this->filesize,
                'filemime' => $this->filemime,
                'encoding' => $this->encoding,
                'fileextension' => $this->fileextension,
                'isimage' => $this->isimage,
                'basename' => $this->basename,
                'basepath' => $this->basepath,
            );
        }else
            return false;
    }

    public function move_file()
    {
        return move_uploaded_file($this->tmpname, $this->driver.$this->filename);
    }

    public function get_fileextension()
    {
        return pathinfo($this->original_name, PATHINFO_EXTENSION);
    }

    public function is_image($path)
    {
        $dimensions = getimagesize($path);
        if(is_array($dimensions)){
            $this->imagewidth = $dimensions[0];
            $this->imageheight = $dimensions[1];

            return true;
        }
        return false;
    }

    public function get_filemime($path)
    {
        $finfo = new finfo;
        return $finfo->file($path, FILEINFO_MIME_TYPE);
    }

    public function get_encoding($path)
    {
        $finfo = new finfo;
        return $finfo->file($path, FILEINFO_MIME);
    }

    public function display_errors($tag=null, $closeTag=null)
    {

        $tag != null ? $tag : '<p>';
        $closeTag != null ? $closeTag : '</p>';

        $output = "";

        foreach($this->errors as $value){
            $output = $tag.$value;$closeTag;
        }

        return $output;
    }
}