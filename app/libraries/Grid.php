<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Grid
 * @package		App\Libraries
 */

class Grid {

    public static function getGrid($obj, &$col, $url=null, $limit=true) {// E COMERCIAL p/ ELE MANTER O VALOR

        $i = 0;

        // ORDENA VETOR PELAS COLUNAS PASSADAS
        ksort($col);

        foreach ($obj as $key => $dado) {

            $x = 0;

            foreach ($dado as $k => $v) {

                if ($x == 0) {
                    $vet['id_dado'] = $v;
                    $url_nova = ($url == null) ? base_url().uri_segment(1).'/'.'editar/'.$v : $url;
                }

                $y = 0;

                foreach ($col as $coluna) {// ORDENA PELA ORDEM DAS COLUNAS

                    $url_nova = str_replace('{'.$k.'}',$v,$url_nova);

                    if (isset($coluna['nome_coluna'])) {

                        if ($coluna['nome_coluna'] == $k) {

                            if ($limit === true) {

                                $aux_val1 = word_limiter($v, 4);
                                $aux_val2 = substr($v, 0, 25);

                                $aux_v = strlen($aux_val1) < strlen($aux_val2) ? $aux_val1 : $aux_val2;

                            } else {

                                $aux_v = $v;

                            }

                            $aux_data = valida_tipos($v, 'data') ? type_format($v, 'data') : $v;

                            $vet['dados_col'][$y]['nome_dado']   = $aux_v;
                            $vet['dados_col'][$y]['desc_coluna'] = $coluna['desc_coluna'];
                            $vet['dados_col'][$y]['title_dado']  = $aux_data;
                            $vet['dados_col'][$y]['url_padrao']  = $url_nova;// TUDO FAZER DINAMICO

                        }

                    }

                    $y++;

                }

                $x++;

            }

            // ORDENA VETOR PELAS COLUNAS PASSADAS
            ksort($vet['dados_col']);

            $vet_dados[$i] = $vet;

            if (($i % 2) == 0) {
                $vet_dados[$i]['classe'] = 'bg_grid_linha1';
            } else{
                $vet_dados[$i]['classe'] = 'bg_grid_linha2';
            }

            $vet_dados[$i]['i'] = $i;

            $i++;

        }

        if (empty($vet_dados)) {
            $vet_dados = null;
        }

        return $vet_dados;

    }

    public static function getGridAba($nome, $obj, &$col, $url=null) {// E COMERCIAL p/ ELE MANTER O VALOR

        $i = 0;

        // ORDENA VETOR PELAS COLUNAS PASSADAS
        ksort($col);

        foreach ($obj as $key => $dado) {

            $x = 0;

            foreach ($dado as $k => $v) {

                if ($x == 0) {
                    $vet['id_dado'] = $v;
                    $url_nova = ($url == null) ? base_url().$nome.'/'.'editar/'.$v : $url;
                }

                $y = 0;

                foreach ($col as $coluna) {// ORDENA PELA ORDEM DAS COLUNAS

                    $url_nova = str_replace('{'.$k.'}',$v,$url_nova);

                    if (isset($coluna['nome_coluna'])) {

                        if ($coluna['nome_coluna'] == $k) {

                            $aux_val1 = word_limiter($v, 4);
                            $aux_val2 = substr($v, 0, 25);

                            $vet['dados_col'][$y]['nome_dado']  = strlen($aux_val1) < strlen($aux_val2) ? $aux_val1 : $aux_val2;
                            $vet['dados_col'][$y]['title_dado'] = $v;
                            $vet['dados_col'][$y]['url_padrao'] = $url_nova;// TUDO FAZER DINAMICO

                        }

                    }

                    $y++;

                }

                $x++;

            }

            // ORDENA VETOR PELAS COLUNAS PASSADAS
            ksort($vet['dados_col']);

            $vet_dados[$i] = $vet;

            $vet_dados[$i]['i'] = $i;

            $i++;

        }

        if (empty($vet_dados)) {
            $vet_dados = null;
        }

        return $vet_dados;

    }

}
