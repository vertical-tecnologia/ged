<?php
require_once APPPATH.'/libraries/LibFactoryServer.php';

class LibLocal extends LibFactoryServer
{

	protected $filesystem;

	public function __construct()
	{
		parent::__construct();
		$this->path_default = 'Home';
	}

	public function listFiles($path, $search = '', $trash = false)
	{
		$object            = new ObjectEntity();
		$object->hash      = $path;
		$object->server_id = $this->CI->session->userdata('SRID');

		$this->trash  = $trash;
		$this->search = $search;

		$arquivos = $this->getObjects($object, $path);
		return array('arquivos' => $arquivos, 'breads' => $this->breads);
	}

	public function uploadFile($folder, &$file, $uploaded)
	{
		$storedObject = $this->filesystem->create_object($uploaded['tmp_name'], $file->hash);
		return $storedObject;
	}

	public function loadFileSystem($serverId = null, $strDriver)
	{
		$serverEntity = new ServerEntity();
		$serverEntity->server_id = $serverId;
		$server = $this->CI->arquivos_model->get_server($serverEntity, true);

		$home = $server->server_path;
		$this->CI->load->library('GedLocalFileSystem');
		$this->filesystem = new GedLocalFileSystem($home, $strDriver);
		class_alias('GedLocalFileSystem', 'GedFileSystem');
	}

	public function viewFile($object)
	{
		if ($this->filesystem->object_exists($object->hash)) {
			$download = $this->filesystem->open($object->hash);
			return $download;
		}
		return false;
	}

	public function thumb($hash, $arrSize)
	{
		$result = $this->filesystem->get_object($hash);

		$obj = new ObjectEntity();
		$obj->hash = $hash;

		$this->CI->load->model('arquivos_model');
		$object = $this->CI->arquivos_model->fetch($obj);
		
		if (!$object)
			return false;

		$serverEntity = new ServerEntity();
		$serverEntity->server_id = $object[0]->server_id;
		$this->CI->load->model('servidor_model');
		$server = $this->CI->servidor_model->fetch($serverEntity);

		$local_path = "{$server->server_path}/{$object[0]->storage_key}/{$object[0]->hash}";
		header("Cache-Control: private");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Type: " . mime_content_type($local_path));
		header("Content-Length: " . filesize($local_path));
		header("Content-Disposition: inline; filename=" . $object[0]->original_name);
		readfile ($local_path);
	}

	public function download($object, $open = false)
	{
		// if ($this->filesystem->object_exists($object->hash)){
		// 	$this->CI->load->library('LibFactoryServer');
		// 	$this->CI->libfactoryserver->use_link($download);
		// }
		$this->filesystem->download($object->hash, ['fileName' => $object->original_name]);
	}

	public function cleanTrash($object)
	{
		$removed = $this->filesystem->remove_object($object->hash);
		if ($removed['msg'] == self::$FILE_REMOVED){
			$entity = new ObjectEntity();
			$entity->hash = $removed['source'];

			if (!$this->CI->arquivos_model->delete_object($entity)) {
				return self::$UNABLE_REMOVE_FILE;
			}

		} else {
			return self::$UNABLE_REMOVE_FILE;
		}
		return self::$FILE_REMOVED;
	}

	public function viewDocument($object, $download)
	{
		$this->CI->load->model('arquivos_model');
		if ($this->filesystem->object_exists($object->hash))
			$this->CI->arquivos_model->use_link($download);

		$serverEntity = new ServerEntity();
		$serverEntity->server_id = $object->server_id;
		$this->CI->load->model('servidor_model');
		$server = $this->CI->servidor_model->fetch($serverEntity);

		$local_path = "{$server->server_path}/{$object->storage_key}/{$object->hash}";
		header("Cache-Control: private");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Type: " . mime_content_type($local_path));
		header("Content-Length: " . filesize($local_path));
		header("Content-Disposition: inline; filename=" . $object->original_name);
		readfile ($local_path);
		exit();

		// $this->filesystem->public_open($object->hash, $server->server_path, $object->storage_key, $object->original_name);
		// return true;
	}

	public function moveFile($output_path, $actual_path)
	{
		return false;
	}

	public function newDiretory($path, $diretory)
	{
		return true;
	}
}
