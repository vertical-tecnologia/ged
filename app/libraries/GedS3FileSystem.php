<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package        App\Libraries
 * Date: 20/07/15
 * Time: 12:25
 */
use Aws\S3\S3Client;

class GedS3FileSystem
{

    public static $UNSUPPORTED_SERVER               = "UNSUPPORTED_SERVER";
    public static $FILE_MOVED                       = 'FILE_MOVED';
    public static $FILE_REMOVED                     = 'FILE_REMOVED';
    public static $UNABLE_MOVE_FILE                 = 'UNABLE_MOVE_FILE';
    public static $UNABLE_REMOVE_FILE               = 'UNABLE_REMOVE_FILE';
    public static $ERROR_NO_WRITABLE                = 'ERROR_NO_WRITABLE';
    public static $ERROR_NO_READABLE                = 'ERROR_NO_READABLE';
    public static $ERROR_NOT_EXISTS                 = 'ERROR_NOT_EXISTS';
    public static $ERROR_FILE_NOT_UPLOADED          = 'ERROR_FILE_NOT_UPLOADED';
    public static $ERROR_USER_DIR_NOT_WRITABLE      = 'ERROR_USER_DIR_NOT_WRITABLE';
    public static $ERROR_FILE_NOT_CREATED           = 'ERROR_FILE_NOT_CREATED';
    public static $ERROR_FILE_NOT_READABLE          = 'ERROR_FILE_NOT_READABLE';
    public static $ERROR_FILE_NOT_WRITABLE          = 'ERROR_FILE_NOT_WRITABLE';
    public static $ERROR_UPLOADED_FILE_NOT_READABLE = 'ERROR_UPLOADED_FILE_NOT_READABLE';
    public static $ERROR_UPLOADED_FILE_NOT_WRITABLE = 'ERROR_UPLOADED_FILE_NOT_WRITABLE';
    public static $ERROR_SRC_FILE_NOT_READABLE      = 'ERROR_SRC_FILE_NOT_READABLE';
    public static $ERROR_SRC_FILE_NOT_WRITABLE      = 'ERROR_SRC_FILE_NOT_WRITABLE';
    public static $ERROR_DST_FILE_NOT_READABLE      = 'ERROR_DST_FILE_NOT_READABLE';
    public static $ERROR_DST_FILE_NOT_WRITABLE      = 'ERROR_DST_FILE_NOT_WRITABLE';
    public static $ERROR_FILE_ALREADY_EXISTS        = 'ERROR_FILE_ALREADY_EXISTS';
    public static $SERVER_STATUS                    = false;
    public static $FILE_EXISTS                      = 'FILE_EXISTS';
    public static $FILE_UPLOADED                    = 'FILE_UPLOADED';
    public static $DIRECTORY_CREATED                = 'DIRECTORY_CREATED';
    public static $DELETE_PERMISSION_DENIED         = 'DELETE_PERMISSION_DENIED';

    private $homePath;
    private $storage;
    private $access_key;
    private $secret;
    private $instance;

    public function __construct($bucket = null, $access_key = null, $secret = null, $verify = true)
    {
        if ($bucket == null || $access_key == null || $secret == null)
            return;
        else {
            $this->homePath = $bucket;
            $this->access_key = $access_key;
            $this->secret = $secret;

        }

        try {
            $this->instance = S3Client::factory(array(
                'region' => 'us-east-1',
                'key' => $this->access_key,
                'secret' => $this->secret,
                'curl.options' => [CURLOPT_VERBOSE => true]
            ));
        } catch (Exception $error) {
            self::$SERVER_STATUS = false;
        }


        if (!$this->bucketExists($bucket)) {
            $this->create_bucket($bucket);
        };

        if ($this->bucketExists($bucket)) {
            self::$SERVER_STATUS = true;
        } else {
            self::$SERVER_STATUS = false;
        }

    }

    protected function bucketExists($bucket)
    {
        return $this->instance->doesBucketExist($bucket);
    }

    protected function create_bucket($bucket)
    {
        $this->instance->createBucket(array('Bucket' => $bucket));
    }

    protected function _file_exists($object = null, $withKey = true)
    {
        if($withKey == true)
            $bucket = $this->homePath . strtolower($this->access_key);
        else
            $bucket = $this->homePath;
        return $this->instance->doesObjectExist($bucket, $object);
    }

    public function create_object($file, $hash, $originalName)
    {
        $return = array();
        $return['action'] = 'upload';
        $return['source'] = $file;
        $return['destination'] = $hash;

        if ($this->_file_exists($hash)) {
            $return['status'] = false;
            $return['msg'] = self::$ERROR_FILE_ALREADY_EXISTS;
            return $return;
        }

        $result = $this->instance->putObject(array(
            'Bucket' => $this->homePath,
            'Key' => $hash,
            'SourceFile' => $file,
            'Metadata' => array(
                'OriginalName' => $originalName,
                'MimeType' => $this->get_mime($hash, $file)
            )
        ));

        $return['status'] = false;
        $return['msg'] = self::$ERROR_NOT_EXISTS;

        $iterator = $this->instance->getIterator('ListObjects', array(
            'Bucket' => $this->homePath
        ));

        foreach ($iterator as $object) {
            if ($object['Key'] == $hash) {
                $return['status'] = true;
                $return['msg'] = self::$FILE_UPLOADED;
                break;
            }
        }

        return $return;
    }

    public function object_exists($object, $withKey = true)
    {
        return $this->_file_exists($object, $withKey);
    }

    public function object_datetime($object)
    {
        return date('Y-m-d H:i:s');
    }

//
    public function is_image($object, $tmp = null)
    {

        $dimensions = getimagesize($tmp);
        if (is_array($dimensions)) {
            return true;
        }
        return false;
    }
//
//    public function file_size($object)
//    {
//        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;
//        return filesize($driver.$object);
//    }
//
    public function get_mime($object, $tmp = null)
    {
        $fInfo = new finfo;
        return $fInfo->file($tmp, FILEINFO_MIME_TYPE);
    }

    public function download($object, $public = false)
    {

        $iterator = $this->instance->getIterator('ListObjects', array(
            'Bucket' => $this->homePath
        ));

        $return['msg'] = self::$ERROR_NOT_EXISTS;

        foreach ($iterator as $obj) {
            if ($obj['Key'] == $object) {
                $return['msg'] = self::$FILE_EXISTS;
            }
        }

        $tmp_file = UPLOAD_PATH . $object;

        if ($return['msg'] == self::$FILE_EXISTS) {

            $result = $this->instance->getObject(array(
                'Bucket' => $this->homePath,
                'Key' => $object,
                'SaveAs' => $tmp_file
            ));

            header("Cache-Control: private");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");

            if ($public == true)
                header("Content-Type: " . $result['Metadata']['mimetype']);
            else
                header("Content-Type: application/stream");

            header("Content-Length: " . $result['ContentLength']);
            header("Content-Disposition: inline; filename=" . $result['Metadata']['originalname']);

            readfile($tmp_file);
            exit();

        } else {
            return self::$ERROR_NOT_EXISTS;
        }
    }

    public function get_object($object)
    {
        $tmp = "/tmp/";

        if(!file_exists(FCPATH."tmp/")){
            mkdir(FCPATH."tmp/");
        }

        $return = array();
        $return['msg'] = self::$FILE_EXISTS;

        if ($this->_file_exists($object)) {
            $return['status'] = false;
            $return['msg'] = self::$ERROR_NOT_EXISTS;
            return $return;
        }

// var_dump($this->homePath, $object, $tmp.$object); exit;
        $result = $this->instance->getObject(array(
            'Bucket' => $this->homePath,
            'Key'    => $object,
            'SaveAs' => $tmp.$object
        ));

        chmod($tmp.$object, 0777);

        $return['action'] = 'get';
        $return['source'] = $object;
        $return['meta'] = $result['Metadata'];
        $return['real_path'] = $tmp.$object;

        return $return;

    }

    public function open($object)
    {
        $this->download($object, true);
    }

    public function public_open($object)
    {
        $this->download($object, true);
    }

    public function remove_object($object)
    {
        $return = array();
        $return['action'] = 'upload';
        $return['source'] = $object;

        $this->instance->deleteObject(array(
            'Bucket' => $this->homePath,
            'Key' => $object
        ));

        $return['msg'] = self::$FILE_REMOVED;
        return $return;
    }
}
