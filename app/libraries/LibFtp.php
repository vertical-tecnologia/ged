<?php

require_once APPPATH.'/libraries/LibFactoryServer.php';

class LibFtp extends LibFactoryServer
{
	protected $ftp = array();
	

	public function __construct()
	{
		parent::__construct();
		$this->path_default = '.';
	}

	public function setFtp($serverId)
	{
		$this->server_id = $serverId;
		$entity = new ServerEntity();
		$entity->server_id = $serverId;
		$server = $this->CI->servidor_model->fetch($entity);

		if (!$server)
			return false;

		$this->ftp = array(
			'server_address'  => $server->server_address,
			'server_username' => $server->server_username,
			'server_password' => base64_decode($server->server_password),
			'server_port'     => $server->server_port,
		);
	}

	public function listFiles($path, $search = '', $trash = false)
	{

		if (strtolower($path) == 'home')
			$path = '.';

		$object            = new ObjectEntity();
		$object->hash      = $path;
		$object->server_id = $this->server_id;

		$this->trash  = $trash;
		$this->search = $search;

		$arquivos = $this->getObjects($object, $path);

		return array('arquivos' => $arquivos, 'breads' => $this->breads);
	}

	protected function convertFileToObject($files)
	{
		$this->CI->load->library('ftp');
		$arrObjects = array();
		foreach ($files as $file) {
			$object                = new ObjectEntity();
			$object->original_name = end(explode('/', $file));
			$object->full_path     = $file;
			$object->ico_file_type = '';

			$size = $this->CI->ftp->sizeFile($file);
			$object->size = ($size > 0) ? $size : 0;

			$arrObjects[] = $object;
			unset($object);
		}
		return $arrObjects;
	}

	public function uploadFile($folder, &$file, $uploaded)
	{
		$parent = $this->CI->arquivos_model->get_parent($folder);
		$full_path = '.';

		if ($parent)
			$full_path = preg_replace('/^(home|Home)/', '.', $parent->full_path);

		$this->CI->load->library('ftp');
		$return = array(
			'action'      => 'upload',
			'source'      => $uploaded['tmp_name'],
			'destination' => $file->original_name);

		if (!$this->CI->ftp->connection($this->ftp))
			return array_merge($return , array('status' => false, 'msg' =>'Conexão com servidor FTP inválida.'));

		$destination = "{$full_path}/{$file->original_name}";

		if (!$this->CI->ftp->upload($uploaded['tmp_name'], $destination, 'binary'))
			return array_merge($return , array('status' => false, 'msg' =>'Falha no envio do arquivo ao FTP.'));

		return array_merge($return , array('status' => true, 'msg' =>'Sucesso ao enviar o arquivo ao FTP.'));
	}

	public function newDiretory($path, $diretory)
	{
		$parent = $this->CI->arquivos_model->get_parent($path);
		$full_path = '.';

		if ($parent)
			$full_path = preg_replace('/^(home|Home)/', '.', $parent->full_path);

		$this->CI->load->library('ftp');
		if (!$this->CI->ftp->connection($this->ftp))
			return array('status' => false, 'msg' =>'Conexão com servidor FTP inválida.');

		if (!$this->CI->ftp->mkdir("{$full_path}/{$diretory->original_name}", 0775))
			return array('status' => false, 'msg' =>'Falha ao criar o diretório no FTP.');

		return true;
	}

	public function thumb($hash, $arrSize)
	{
		$this->CI->load->model('arquivos_model');
		$entity = new ObjectEntity();
		$entity->hash = $hash;
		$object = $this->CI->arquivos_model->getObject($entity);

		$remote_path = preg_replace('/^(home|Home)/', '.', $object->full_path);
		$local_path  = FCPATH."tmp/{$hash}{$object->ext_file_type}";

		// $config = array(
		// 	'source_image' => $local_path,
		// 	'new_image'    => $object->original_name,
		// 	'width'        => $arrSize['width'],
		// 	'height'       => $arrSize['height']
		// );

		// $this->CI->load->library('image_lib', $config);
		if (!$this->downloadFTP($remote_path, $local_path)) {
			$this->CI->image_lib->set_error('Falha ao realizar o download por FTP.');
			echo $this->CI->image_lib->display_errors();
			return;
		}

		// if (!$this->CI->image_lib->resize()) {
		// 	var_dump($this->CI->image_lib->display_errors()); exit;
		// 	echo $this->CI->image_lib->display_errors();
		// 	return;
		// }

		$fp = fopen($local_path, 'rb');
		header('Content-Type: '. $this->get_mime($local_path));
		header("Content-Length: " . filesize($local_path));

		fpassthru($fp);
		return;
	}

	public function download($object, $open = false)
	{
		$remote_path = preg_replace('/^(home|Home)/', '.', $object->full_path);
		$local_path  = FCPATH."tmp/{$object->hash}{$object->ext_file_type}";

		if (!$this->downloadFTP($remote_path, $local_path))
			return false;

		header("Cache-Control: private");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		header("Content-Length: " . filesize($local_path));
		header("Content-Disposition: inline; filename=" . $object->original_name);
		readfile($local_path);
	}

	protected function downloadFTP($remote_path, $local_path)
	{
		$this->CI->load->library('ftp');
		if (!$this->CI->ftp->connection($this->ftp))
			return false;

		if (!$this->CI->ftp->download($remote_path, $local_path, 'binary'))
			return false;

		return true;
	}

	public function cleanTrash($object)
	{
		$this->CI->load->library('ftp');
		if (!$this->CI->ftp->connection($this->ftp))
			return self::$UNABLE_REMOVE_FILE;

		$remote_path = preg_replace('/^(home|Home)/', '.', $object->full_path);
		$action = ($object->type == TYPE_FILE) ? 'delete_file' : 'delete_dir';

		if (!$this->CI->ftp->{$action}($remote_path))
			return self::$UNABLE_REMOVE_FILE;

		$entity = new ObjectEntity();
		$entity->hash = $object->hash;

		if (!$this->CI->arquivos_model->delete_object($entity))
			return self::$UNABLE_REMOVE_FILE;

		return self::$FILE_REMOVED;
	}

	public function viewFile($object)
	{
		$remote_path = preg_replace('/^(home|Home)/', '.', $object->full_path);
		$local_path  = FCPATH."tmp/{$object->hash}{$object->ext_file_type}";

		if (!$this->downloadFTP($remote_path, $local_path))
			return false;

		$this->setHeaderView($object, $local_path);
	}

	public function viewDocument($object)
	{
		$remote_path = preg_replace('/^(home|Home)/', '.', $object->full_path);
		$local_path  = FCPATH."uploads/{$object->hash}";
		if (!$this->downloadFTP($remote_path, $local_path))
			return false;

		$this->setHeaderView($object, $local_path);
	}

	protected function setHeaderView($object, $local_path)
	{
		header("Cache-Control: private");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Type: " . mime_content_type($local_path));
		header("Content-Length: " . filesize($local_path));
		header("Content-Disposition: inline; filename=" . $object->original_name);
		readfile($local_path); exit;
	}

	public function searchFileXML($serverId)
	{
		$this->setFtp($serverId);
		$this->CI->load->library('ftp');

		if (!$this->CI->ftp->connection($this->ftp))
			return false;

		$result = $this->CI->servidor_model->getServerInfo($serverId);

		if (!$result)
			return false;

		$path  = $result[0]['directory_monitor'];

		$directory = $this->CI->arquivos_model->fileExists('Home/'.$path, $result[0]['user_key'], $result[0]['client_volume'], $serverId, TYPE_DIR);

		if (!$directory) {

			$dirInfo = array(
				'original_name' => $path,
				'full_path'     => "Home/{$path}",
				'in_trash'      => false,
				'type'          => TYPE_DIR,
				'owner'         => $result[0]['user_key'],
				'storage_key'   => $result[0]['client_volume'],
				'server_id'     =>  $serverId,
				'mime'          => $this->CI->arquivos_model->getObjectMime('directory', null)->id_file_type,
				'is_image'      => false,
				'extension'     => null,
			);
			$directory = $this->createFileObject($dirInfo, '.');
			$this->CI->arquivos_model->storeObject($directory);
		}

		$files = $this->CI->ftp->list_files_dirs($path);

		if (!$files) {
			$this->CI->ftp->close();
			return false;
		}

		$arrFiles = array();
		foreach ($files as $file) {

			if (!preg_match('/[.].*$/', $file))
				continue;

			$name = str_replace("{$path}/", '', $file);
			$extension = pathinfo($name, PATHINFO_EXTENSION);
			$name_tmp = hash('adler32', date('Y-m-d H:i:s')).'.'.$extension;

			$file = end(explode('/', $file));

			$fileInfo = array(
				'original_name' => $name,
				'size'          => $this->CI->ftp->sizeFile($file),
				'full_path'     => "Home/{$path}/{$file}",
				'in_trash'      => false,
				'type'          => TYPE_FILE,
				'owner'         => $result[0]['user_key'],
				'storage_key'   => $result[0]['client_volume'],
				'tmp'           => FCPATH."tmp/{$name_tmp}",
				'parent'        => $directory->id,
				'server_id'     => $serverId,
			);

			if (!$this->CI->ftp->download("{$path}/{$file}", $fileInfo['tmp'], 'binary'))
				continue;

			$object = $this->createFileObject($fileInfo, '.');

			if ($this->CI->arquivos_model->fileExists($object->full_path, $result[0]['user_key'], $result[0]['client_volume'], $serverId)) {
				unlink($fileInfo['tmp']);
				continue;
			}

			// if (strtolower($extension) == 'xml') {
			if (in_array($extension, ['XML', 'xml'])) {
				$arrFile = array(
					'hash'        => $object->hash,
					'user_key'    => $result[0]['user_key'],
					'storage_key' => $result[0]['client_volume'],
					'content'     => file_get_contents($fileInfo['tmp']),
					'server_id'   =>  $serverId,
				);

				$this->CI->arquivos_model->scheduleFileToBeProcessed($arrFile);
			}

			$this->CI->arquivos_model->storeObject($object);
			unlink($fileInfo['tmp']);
		}
		$this->CI->ftp->close();
	}

	public function moveFile($output_path, $actual_path)
	{
		$this->CI->load->library('ftp');
		$output_path = preg_replace('/^(home|Home)/', '.', $output_path);
		$actual_path = preg_replace('/^(home|Home)/', '.', $actual_path);

		if (!$this->CI->ftp->connection($this->ftp))
			return false;

		$response = $this->CI->ftp->move($output_path, $actual_path);

		$this->CI->ftp->close();
		return $response;
	}

}