<?php

/**
 * Class MY_Profiler
 * @package		App\Libraries
 */

class MY_Profiler extends CI_Profiler {

    function run() {

        $CI =& get_instance();

        $output = "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
        //$output = "";

        $output .= "<div id='codeigniter_profiler' style='clear:both;background-color:#fff;padding:10px;'>";
        $output .= $this->_compile_uri_string();
        $output .= $this->_compile_controller_info();
        $output .= $this->_compile_memory_usage();
        $output .= $this->_compile_benchmarks();
        $output .= $this->_compile_get();
        $output .= $this->_compile_post();
        $output .= $this->_compile_queries();
        $output .= '</div>';

        $log = str_replace('/','-',$CI->uri->uri_string);
        $log = substr($log,-1,1) == '-' ? substr($log,0,strlen($log)-1) : $log;

        $file = fopen(FCPATH.APPPATH.'logs/profiler'.$log.'.php', "w+");

        fwrite($file,$output);
        fclose($file);

    }

}