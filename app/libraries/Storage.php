<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Libraries
 * Date: 29/06/15
 * Time: 11:01
 */

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;

class Storage {

    private $ci;

    public function storage()
    {
        $this->ci = &get_instance();

    }

    public function root_dir()
    {
        $filesystem = new Filesystem(new Adapter(STORAGE_INI));

        if(!$filesystem->has($this->ci->session->userdata('STR_DRIVER'))){
            $filesystem->createDir($this->ci->session->userdata('STR_DRIVER'));
            if($filesystem->has($this->ci->session->userdata('STR_DRIVER'))){
                return true;
            } else {
                return false;
            }

        } else {
            $mime = $filesystem->getMimetype($this->ci->session->userdata('STR_DRIVER'));
            if($mime == 'directory')
                return true;
        }
        return false;

    }
}