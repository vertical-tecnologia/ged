<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Libraries
 * Date: 14/07/15
 * Time: 15:56
 */
class GedLocalFileSystem
{

    public static $FILE_MOVED                       = 'FILE_MOVED';
    public static $INVALID_TYPE                     = 'INVALID_TYPE';
    public static $UNABLE_MOVE_FILE                 = 'UNABLE_MOVE_FILE';
    public static $FILE_REMOVED                     = 'FILE_REMOVED';
    public static $UNABLE_REMOVE_FILE               = 'UNABLE_REMOVE_FILE';
    public static $FILE_RECOVERED                   = 'FILE_RECOVERED';
    public static $UNABLE_RECOVER_FILE              = 'UNABLE_RECOVER_FILE';
    public static $ERROR_NO_WRITABLE                = 'ERROR_NO_WRITABLE';
    public static $ERROR_NO_READABLE                = 'ERROR_NO_READABLE';
    public static $ERROR_NOT_EXISTS                 = 'ERROR_NOT_EXISTS';
    public static $ERROR_FILE_NOT_UPLOADED          = 'ERROR_FILE_NOT_UPLOADED';
    public static $ERROR_USER_DIR_NOT_WRITABLE      = 'ERROR_USER_DIR_NOT_WRITABLE';
    public static $ERROR_FILE_NOT_CREATED           = 'ERROR_FILE_NOT_CREATED';
    public static $ERROR_FILE_NOT_READABLE          = 'ERROR_FILE_NOT_READABLE';
    public static $ERROR_FILE_NOT_WRITABLE          = 'ERROR_FILE_NOT_WRITABLE';
    public static $ERROR_UPLOADED_FILE_NOT_READABLE = 'ERROR_UPLOADED_FILE_NOT_READABLE';
    public static $ERROR_UPLOADED_FILE_NOT_WRITABLE = 'ERROR_UPLOADED_FILE_NOT_WRITABLE';
    public static $ERROR_SRC_FILE_NOT_READABLE      = 'ERROR_SRC_FILE_NOT_READABLE';
    public static $ERROR_SRC_FILE_NOT_WRITABLE      = 'ERROR_SRC_FILE_NOT_WRITABLE';
    public static $ERROR_DST_FILE_NOT_READABLE      = 'ERROR_DST_FILE_NOT_READABLE';
    public static $ERROR_DST_FILE_NOT_WRITABLE      = 'ERROR_DST_FILE_NOT_WRITABLE';
    public static $ERROR_FILE_ALREADY_EXISTS        = 'ERROR_FILE_ALREADY_EXISTS';
    public static $SERVER_STATUS                    = false;
    public static $FILE_EXISTS                      = 'FILE_EXISTS';
    public static $FILE_UPLOADED                    = 'FILE_UPLOADED';
    public static $DIRECTORY_CREATED                = 'DIRECTORY_CREATED';
    public static $TRASH_CLEANED                    = 'TRASH_CLEANED';
    public static $UNABLE_TO_CLEAN_TRASH            = 'UNABLE_TO_CLEAN_TRASH';
    public static $DELETE_PERMISSION_DENIED         = 'DELETE_PERMISSION_DENIED';



    private $homePath;
    private $storage;

    public function __construct($home = null, $storage = null, $verify = true)
    {
        if ($home == null || $storage == null)
            return;
        else {
            $this->homePath = $home;
            $this->storage = $storage;
        }

        if (!$this->_is_readable()) {
            self::$SERVER_STATUS = false;
            return self::$ERROR_NO_READABLE;
        } else if (!$this->_is_writable()) {
            self::$SERVER_STATUS = false;
            return self::$ERROR_NO_WRITABLE;
        }


        if (!$this->_file_exists()) {
            $this->_create_home_dir();
        } else {
            if (!$this->_is_readable($this->homePath . DIRECTORY_SEPARATOR . $this->storage)) {
                self::$SERVER_STATUS = false;
                return self::$ERROR_USER_DIR_NOT_WRITABLE;
            }
        };
    }


    protected function _file_exists($object = null)
    {
        $object = $object != null ? $object : $this->homePath . DIRECTORY_SEPARATOR . $this->storage;
        return file_exists($object);
    }


    protected function _is_readable($path = null)
    {
        $path = $path != null ? $path : $this->homePath;
        return is_readable($path);
    }

    protected function _is_writable()
    {
        return is_writable($this->homePath);
    }

    protected function _create_home_dir()
    {
        if ($this->_is_writable($this->homePath)) {
            mkdir($this->homePath . DIRECTORY_SEPARATOR . $this->storage);
        }
    }

    public function get_object($object)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;

        $return = array();
        $return['action'] = 'get';
        $return['source'] = $object;

        if($this->_file_exists($driver.$object)){
            $return['status'] = true;
            $return['real_path'] = $driver.$object;
            $return['msg'] = self::$FILE_EXISTS;
        } else {
            $return['status'] = false;
            $return['msg'] = self::$ERROR_NOT_EXISTS;
        }

        return $return;
    }

    public function create_object($source, $object)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage;

        $return = array();
        $return['action'] = 'upload';
        $return['source'] = $source;
        $return['destination'] = $object;

        if($this->_file_exists($object)){
            $return['status'] = false;
            $return['msg'] = self::$ERROR_FILE_ALREADY_EXISTS;
        }

        if (move_uploaded_file($source, $driver . DIRECTORY_SEPARATOR . $object)) {

            if (!$this->_is_readable($driver . DIRECTORY_SEPARATOR . $object))
                $return['readable'] = self::$ERROR_UPLOADED_FILE_NOT_READABLE;
            if (!$this->_is_writable($driver . DIRECTORY_SEPARATOR . $object))
                $return['writable'] = self::$ERROR_UPLOADED_FILE_NOT_WRITABLE;

            $return['status'] = true;
            $return['msg'] = self::$FILE_UPLOADED;

            return $return;

        } else {
            $return['status'] = false;
            $return['msg']   = self::$ERROR_FILE_NOT_UPLOADED;
        }

        return $return;
    }

    public function move_object($source, $destination)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage . DIRECTORY_SEPARATOR;

        $return = array();
        $return['action'] = 'move';
        $return['source'] = $source;
        $return['destination'] = $destination;

        if (!$this->object_exists($source)) {
            $return['status'] = false;
            $return['msg'] = self::$ERROR_NOT_EXISTS;
            return $return;
        } else {

            if ($this->object_exists($destination)) {
                $return['status'] = false;
                $return['msg'] = self::$ERROR_FILE_ALREADY_EXISTS;
                return $return;
            } else {
                if (rename($driver . $source, $driver . $destination)) {
                    $return['status'] = false;
                    $return['msg'] = self::$FILE_MOVED;
                } else {
                    $return['status'] = false;
                    $return['msg'] = self::$UNABLE_MOVE_FILE;
                }
            }

        }

        return $return;

    }

    public function remove_object($source)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage . DIRECTORY_SEPARATOR;

        $return = array();
        $return['action'] = 'remove';
        $return['source'] = $source;

        if (!$this->object_exists($source)) {
            $return['status'] = false;
            $return['msg'] = self::$ERROR_NOT_EXISTS;
            return $return;
        } else {

                if (unlink($driver.$source)) {
                    $return['status'] = false;
                    $return['msg'] = self::$FILE_REMOVED;
                } else {
                    $return['status'] = false;
                    $return['msg'] = self::$UNABLE_REMOVE_FILE;
                }

        }

        return $return;
    }

    public function object_exists($object)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage;
        return $this->_file_exists($driver . DIRECTORY_SEPARATOR . $object);
    }

    public function set_permission($object, $permissions)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage . DIRECTORY_SEPARATOR ;
        if($this->_file_exists($driver . $object)){
            chmod ($driver.$object, $permissions);
        }
    }


    public function object_datetime($object)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;
        if($this->_file_exists($driver . DIRECTORY_SEPARATOR . $object)){
            return date("Y-m-d h:i:s", filectime($driver.$object));
        } else {
            return self::$ERROR_NOT_EXISTS;
        }
    }

    public function is_image($object, $tmp = null)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;
        $dimensions = getimagesize($driver.$object);
        if(is_array($dimensions)){
            return true;
        }
        return false;
    }

    public function file_size($object)
    {

        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;

        return filesize($driver.$object);
    }

    public function get_mime($object, $tmp = null)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        @ $mime = finfo_file($finfo, $driver.$object);

        return $mime;
    }

    public function download($object, $external = array(), $open=false)
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;

        if($this->_file_exists($driver.$object)) {

            if(count($external) > 0){
                $fileName = basename($external['fileName']);
            } else {
                var_dump($driver.$object);
                $fileName = basename($driver.$object);
            }

            $fileSize = $this->file_size($object);

            header("Cache-Control: private");
            if($open == true){
                header("Content-Type: application/stream");
            } else {
                header("Content-Type: application/force-download");
                header("Content-Type: application/download");
                header("Content-Type: application/octet-stream");
            }

            header("Content-Length: ".$fileSize);
            header("Content-Disposition: attachment; filename=".$fileName);

            readfile ($driver.$object);
            exit();
        }
        else {
            return self::$ERROR_NOT_EXISTS;
        }
    }

    public function open($object, $external = array())
    {
        $driver = $this->homePath . DIRECTORY_SEPARATOR . $this->storage.DIRECTORY_SEPARATOR;

        if($this->_file_exists($driver.$object)) {

            if(count($external) > 0){
                $fileName = basename($external['fileName']);
            } else {
                $fileName = basename($driver.$object);
            }

            $fileSize = $this->file_size($object);
            $mime = $this->get_mime($object);
            header("Cache-Control: private");
            header("Content-Type: " . $mime);
            header("Content-Length: ".$fileSize);
            header("Content-Disposition: inline; filename=".$fileName);

            readfile ($driver.$object);
            exit();
        }
        else {
            return self::$ERROR_NOT_EXISTS;
        }
    }

    public function public_open($object, $homePath, $storage, $fileName)
    {
        $driver = $homePath . DIRECTORY_SEPARATOR . $storage.DIRECTORY_SEPARATOR;

        if($this->_file_exists($driver.$object)) {

            $fileName = basename($fileName);

            $fileSize = $this->file_size($object);
            $mime = $this->get_mime($object);

            header("Cache-Control: private");
            header("Content-Type: " . $mime);
            header("Content-Length: ".$fileSize);
            header("Content-Disposition: inline; filename=".$fileName);

            readfile ($driver.$object);
            exit();
        }
        else {
            return self::$ERROR_NOT_EXISTS;
        }
    }
}