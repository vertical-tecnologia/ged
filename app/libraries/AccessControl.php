<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class AccessControl
 * @package		App\Libraries
 */

class AccessControl {

    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->library("session");
        $this->CI->load->library('user_agent');
        $this->CI->load->library('encrypt');
    }

    public function verify($module, $action)
    {
        if($this->has_permission($module, $action) === false){
            redirect('admin');
        }
    }

    private function has_permission($module, $action){

        $type_id = $this->CI->session->userdata('USER_TYPE_ID');
        if($module != null && $action != null){

            $module_id = $this->get_module_id($module);
            $action_id = $this->get_action_id($action, $module_id->module_id);

            if(count($module_id) == 0){
                return true;
            } else {
                $permission = $this->get_permission($module_id->module_id,$action_id->action_id, $type_id);

                return @$permission->permissao_allow === 0 ? false : true;
            }

        } else {
            return false;
        }

    }

    private function get_module_id($module)
    {
        $this->CI->db->select('module_id');
        $this->CI->db->from('modulo');
        $this->CI->db->where('module_name', $module);
        return $this->CI->db->get()->row();
    }

    private function get_action_id($action, $module)
    {
        $this->CI->db->select('action_id');
        $this->CI->db->from('acao');
        $this->CI->db->where('action_name', $action);
        $this->CI->db->where('action_module_id', $module);
        return $this->CI->db->get()->row();
    }

    private function get_permission($module, $action, $type)
    {
        $this->CI->db->select('permissao_allow');
        $this->CI->db->from('permissao');
        $this->CI->db->where('permissao_module_id', $module);
        $this->CI->db->where('permissao_action_id', $action);
        $this->CI->db->where('permissao_type_id', $type);

        return $this->CI->db->get()->row();
    }
}
