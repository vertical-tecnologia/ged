<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Authenticator
 * @package		App\Libraries
 */
class Authenticator {

    function Authenticator() {
        $CI =& get_instance();
        //  load libraries
        $CI->load->database();
        $CI->load->library("session");
        $CI->load->library('user_agent');
        $CI->load->library('encrypt');
    }

    function login($username, $password){

        if($username == null || $password == null)
            return false;

        $CI =& get_instance();
        $query = $this->fetch($username);

        if(count($query) != 1) {
            //  their username and password combination were not found in the databse
            $CI->session->set_flashdata('error', 'Usuário e/ou senha inválidos.');
            return false;
        } else {

            if($query->user_trusted_mail == FALSE){
                $CI->session->set_flashdata('error', 'Por favor confirme seu email antes de usar o sistema.');
                redirect('login');
                return;
            }

            if($query->user_status_id == 3){
                $CI->session->set_flashdata('error', 'Seu acesso esta bloqueado. Favor entre em contato com o suporte.');
                redirect('login');
                return;
            }

            if($query->user_status_id == 8){
                $CI->session->set_flashdata('error', 'Essa conta foi encerrada. Caso queira restaurá-la entre em contato com o suporte.');
                redirect('login');
                return;
            }

            if(($CI->encrypt->decode($query->user_password) == $password) === false){
                $CI->session->set_flashdata('error', 'Usuário e/ou senha inválidos.');
                return false;
            }

            $CI->session->set_userdata("USER_ID", $query->user_id);
            $CI->session->set_userdata("IS_OWNER", $query->owner);
            $CI->session->set_userdata("USER_NAME", $query->user_login);
            $CI->session->set_userdata("USER_EMAIL", $query->user_email);
            $CI->session->set_userdata("USER_TYPE_ID", $query->user_type_id);
            $CI->session->set_userdata("USER_FULLNAME", $query->user_fullname);
            $CI->session->set_userdata("USER_ROLE", $query->type_name );
            $CI->session->set_userdata("USER_PICTURE", $query->user_picture );
            $CI->session->set_userdata("USER_STATUS", $query->user_status_id );
            $CI->session->set_userdata("USER_KEY", $query->user_key);

            if(in_array($query->user_type_id, array(2, 4))){
                $empresa = $this->get_empresas($query->user_id, $query->type_name, $query->owner);

                $cliente = $this->get_client($empresa->empresa_id);

                $servidor = $this->get_servidor($cliente->server_id);

                $CI->session->set_userdata("EID", $empresa->empresa_id);
                $CI->session->set_userdata("EKEY", $empresa->empresa_key);
                $CI->session->set_userdata("STR_DRIVER", $cliente->client_volume);
                $CI->session->set_userdata("STR_KEY", $cliente->client_key);
                $CI->session->set_userdata("CLID", $cliente->client_id);
                $CI->session->set_userdata("QDS", $cliente->quota_enabled);
                $CI->session->set_userdata("SRID", $servidor->server_id);
                $CI->session->set_userdata("OWN", $query->owner);
            }

            $this->__update_log_history("LOG IN");

            return true;
        }
    }

    public function fetch($user_login)
    {
        $CI =& get_instance();

        $CI->db->select('usuario.user_id, usuario.owner, usuario.user_password, usuario.user_login,usuario.user_trusted_mail, usuario.user_fullname, usuario.user_email, usuario.user_status_id,usuario.user_type_id, usuario_tipo.type_name, usuario.user_key, usuario.user_picture');
        $CI->db->join('usuario_tipo', 'usuario.user_type_id = usuario_tipo.type_id');
        $CI->db->where('usuario.user_login', $user_login);
        $CI->db->or_where('usuario.user_email', $user_login);
        $CI->db->from('usuario');

        return $CI->db->get()->row();
    }

    public function get_empresas($user_id, $user_type, $owner = false)
    {
        $CI =& get_instance();

        $CI->db->select('empresa.empresa_key, empresa.empresa_id');

        if($user_type == 'CLIADMIN' && $owner == true){

            $CI->db->where('empresa.empresa_user_id', $user_id);
            $CI->db->from('empresa');

        } else if($user_type == 'USUARIO' || $user_type == 'CLIADMIN' && $owner == false){
            $CI->db->join('empresa_usuario', 'empresa_usuario.id_empresa = empresa.empresa_id');
            $CI->db->where('empresa_usuario.id_usuario', $user_id);
            $CI->db->from('empresa');
        }

        return $CI->db->get()->row();
    }

    public function get_servidor($server_id)
    {
        $CI =& get_instance();

        $CI->db->select('servidor.server_id, servidor.server_type');
        $CI->db->from('servidor');
        $CI->db->where('servidor.server_id', $server_id);

        return $CI->db->get()->row();
    }

    public function get_client($empresa_id)
    {
        $CI =& get_instance();

        $CI->db->select('cliente.client_volume, cliente.client_key, cliente.client_id, cliente.server_id, cliente.quota_enabled');
        $CI->db->from('cliente');
        $CI->db->where('cliente.client_empresa_id', $empresa_id);

        return $CI->db->get()->row();
    }

    function logout(){
        $CI =& get_instance();
        $this->__update_log_history("LOG OUT");
        $CI->session->unset_userdata("USER_NAME");
        $CI->session->sess_destroy();

    }

    function is_logged_in() {

        $CI =& get_instance();
        return ($CI->session->userdata("USER_NAME") != '') ? true : false;
    }

    function is_admin() {
        $CI =& get_instance();
        return ($CI->session->userdata("USER_ROLE") == 'CLIADMIN') ? true : false;
    }

    function is_superuser() {
        $CI =& get_instance();
        return ($CI->session->userdata("USER_ROLE") == 'SUPERUSUARIO') ? true : false;
    }

    function get_userdata() {
        $CI =& get_instance();
        if(!$this->is_logged_in()) {
            return false;
        } else {
            $query = $CI->db->get_where("usuario", array("id" => $CI->session->userdata("USER_ID")));
            return $query->row();
        }
    }

    function set_current_url($url) {
        $CI =& get_instance();
        $CI->session->set_userdata("CURRENT_URL", $url);
    }

    function get_current_url() {
        $CI =& get_instance();
        return $CI->session->userdata("CURRENT_URL");
    }

    function __update_log_history($operation) {
        $CI =& get_instance();
        //  $this->agent->version()
        $data = array(
            "user_name" => $CI->session->userdata("USER_NAME"),
            "operation" => $operation,
            "ip_address" => $_SERVER['SERVER_ADDR'],
            "browser" => $CI->agent->browser(),
            "browser" => $CI->agent->browser(),
            "client_id" => $CI->session->userdata('CLID')
        );

        $CI->db->insert("log_history", $data);
    }
}