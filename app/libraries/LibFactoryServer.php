<?php

require_once APPPATH.'/libraries/LibServer.php';

class LibFactoryServer implements LibServer
{
	protected $CI;
	protected $factory;
	protected $server_id;
	protected $path_default;
	protected $breads;
	protected $trash;
	protected $search;

	public static $UNABLE_REMOVE_FILE  = 'UNABLE_REMOVE_FILE';
	public static $FILE_REMOVED        = 'FILE_REMOVED';
	public static $INVALID_TYPE        = 'INVALID_TYPE';
	public static $ERROR_NOT_EXISTS    = 'ERROR_NOT_EXISTS';
	public static $UNABLE_RECOVER_FILE = 'UNABLE_RECOVER_FILE';
	public static $FILE_RECOVERED      = 'FILE_RECOVERED';

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model(array('servidor_model', 'arquivos_model'));
	}

	public function setTypeServer($serverId = '', $drive = null)
	{
		$entity = new ServerEntity();
		$entity->server_id = $serverId;
		$server = $this->CI->servidor_model->fetch($entity);

		// if ((!$serverId || $serverId == SERVER_LOCAL) && $this->CI->session->userdata('SRID') == SERVER_LOCAL) {
		if (SERVER_DEFAULT == SERVER_LOCAL) {
			if (!$serverId || $serverId == SERVER_LOCAL) {
				$this->CI->load->library('LibLocal');
				$this->factory = 'liblocal';
				$this->CI->{$this->factory}->loadFileSystem($this->server_id, $drive);
				return true;
			}
		}

		if (!$serverId || SERVER_DEFAULT == $serverId) {
			$this->CI->load->library('LibS3');
			$this->factory = 'libs3';
			$this->CI->{$this->factory}->loadFileSystem($this->server_id, $drive);
			return true;
		}

		if ($server->server_type == SERVER_FTP) {
			$this->CI->load->library('LibFtp');
			$this->factory = 'libftp';
			$this->CI->{$this->factory}->setFtp($server->server_id);
			return true;
		}

		return false;
	}

	public function listFiles($path, $search = '', $trash = false)
	{
		return $this->CI->{$this->factory}->listFiles($path, $search, $trash);
	}

	public function uploadFile($folder, &$file, $uploaded)
	{
		return $this->CI->{$this->factory}->uploadFile($folder, $file, $uploaded);
	}

	public function newDiretory($path, $diretory)
	{
		return $this->CI->{$this->factory}->newDiretory($path, $diretory);
	}

	public function sendFolderTrash($object)
	{
		if ($object->type != 2) {
			$this->CI->db->trans_rollback();
			return self::$INVALID_TYPE;
		}

		$this->CI->db->trans_commit();
		$item = new ObjectEntity();
		$item->id = $object->id;

		$childrens = $this->CI->arquivos_model->get_childrens($item);

		if ($childrens) {
			$arrObjectId = array();
			foreach ($childrens as $children)
				$arrObjectId[] = $children->id;

			if (!$this->CI->arquivos_model->sendAllObjectToTrash($arrObjectId)) {
				$this->CI->db->trans_rollback();
				return self::$UNABLE_REMOVE_FILE;
			}
		}

		return $this->sendToTrash($object);
		// return $this->CI->{$this->factory}->sendFolderTrash($object);
	}

	public function sendToTrash($object)
	{
		$object->in_trash = true;
		if (!$this->CI->arquivos_model->sendObjectToTrash($object)) {
			$this->CI->db->trans_rollback();
			return self::$UNABLE_REMOVE_FILE;
		}

		$this->CI->db->trans_commit();
		return self::$FILE_REMOVED;
	}

	protected function getObjects(ObjectEntity &$object, $path)
	{
		if ($path != $this->path_default && !$this->CI->arquivos_model->dir_exists($object->hash))
			return false;

		$currentDir = $this->CI->arquivos_model->getObject($object);
		$object->hash = null;
		$object->original_name = $this->search;

		if (count($currentDir) > 0) {
			$object->parent = $currentDir->id;
			$this->breads = $this->CI->arquivos_model->get_parents($object);
			$object->parent = $currentDir->id;
		}

		if ($this->trash)
			$object->in_trash = 1;

		$is_search = $this->search ? true : false;
		$arquivos = $this->CI->arquivos_model->getObjectList($object, $is_search);

		foreach ($arquivos as $indice => $arquivo) {
			$arquivos[$indice]->size    = By2Mb($arquivo->size);
			$arquivos[$indice]->created = data_formatada($arquivo->created, 3);

			$arquivos[$indice]->file_url = base_url() . 'arquivos/folder/' . $arquivo->hash;

			if (!is_null($object->server_id))
				$arquivos[$indice]->file_url .= "/?server={$object->server_id}";

			if ($arquivo->type == 1)
				$arquivos[$indice]->file_url = base_url() . 'arquivos/view/' . $arquivo->hash;
		}

		return $arquivos;
	}

	public function createFileObject($file, $folder)
	{
		$parent = $this->CI->arquivos_model->get_parent($folder);

		$object = new ObjectEntity();
		foreach (get_object_vars($object) as $property => $value) {
			if (!array_key_exists($property, $file))
				continue;
			$object->{$property} = $file[$property];
		}

		if (is_null($object->hash))
			$object->hash = random_string('alnum', 30);

		if (is_null($object->extension))
			$object->extension = $this->getObjectExtension($object->original_name);

		if (is_null($object->parent))
			$object->parent = isset($parent->id) && $parent->id != '' ? $parent->id : null;

		if (is_null($object->full_path)) {
			$object->full_path = isset($parent->full_path) && $parent->full_path != '' ? $parent->full_path : $folder;
			$object->full_path .= DIRECTORY_SEPARATOR . $object->original_name;
		}

		if (is_null($object->full_path))
			$object->full_path = isset($parent->full_path) && $parent->full_path != '' ? $parent->full_path : $folder;

		if ($this->server_id)
			$object->server_id = $this->server_id;

		if (!$this->server_id && $this->CI->session->userdata('SRID') == SERVER_LOCAL)
			$object->server_id = SERVER_LOCAL;

		$object->created = date('Y-m-d H:i:s');

		if (is_null($object->mime) && isset($file['tmp'])) {
			// $mime   = $this->get_mime($object->hash, $file['tmp']);
			$mime   = $this->get_mime($file['tmp']);
			$mimeId = $this->CI->arquivos_model->getObjectMime($mime, $object->extension)->id_file_type;
			$object->mime = $mimeId != '' ? $mimeId : 1;
		}

		if (is_null($object->in_trash)) 
			$object->in_trash = false;

		if (is_null($object->is_image))
			$object->is_image = (isset($file['tmp']) && is_array(getimagesize($file['tmp']))) ? true : false;

		return $object;
	}

	// protected function get_mime($object, $tmp = null)
	protected function get_mime($tmp = null)
	{
		$fInfo = new finfo;
		return $fInfo->file($tmp, FILEINFO_MIME_TYPE);
	}

	protected function getObjectExtension($basePath)
	{
		return pathinfo($basePath, PATHINFO_EXTENSION);
	}

	public function setServer($server_id)
	{
		$this->server_id = $server_id;
	}

	public function recoveryFiles($object)
	{
		if (!$object)
			return self::$ERROR_NOT_EXISTS;

		$object->in_trash = false;
		if (!$this->CI->arquivos_model->sendObjectToTrash($object))
			return self::$UNABLE_RECOVER_FILE;

		$log             = new ObjectLog();
		$log->log_action = 'recovery_file';
		$log->user_key   = $this->CI->session->userdata('USER_KEY');
		$log->hash       = $object->hash;
		$log->datetime   = date('Y-m-d h:i:s');

		$this->CI->arquivos_model->generate_log($log);
		return self::$FILE_RECOVERED;
	}

	public function viewFile($object)
	{
		return $this->CI->{$this->factory}->viewFile($object);
	}

	public function thumb($hash, $arrSize)
	{
		return $this->CI->{$this->factory}->thumb($hash, $arrSize);
	}

	public function download($object, $open = false)
	{
		return $this->CI->{$this->factory}->download($object, $open);
	}

	public function cleanTrash($object)
	{
		return $this->CI->{$this->factory}->cleanTrash($object);
	}

	public function viewDocument($object, $download)
	{
		return $this->CI->{$this->factory}->viewDocument($object, $download);
	}

	public function moveFile($output_path, $actual_path)
	{
		return $this->CI->{$this->factory}->moveFile($output_path, $actual_path);
	}
}