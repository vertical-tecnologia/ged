<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Debug
 * @package		App\Libraries
 */

class Debug {

	public static function Test($vet=null,$tipo=1) {
        @header('Content-Type: text/html; charset=utf-8');

        echo "<pre>";
		if ($tipo == 1) {
    		print_r($vet);
		} else if ($tipo == 2) {
		    var_dump($vet);
		}
		echo "</pre>";
	}
	
}

class Dd {

    public static function Test($vet=null,$tipo=1) {
        @header('Content-Type: text/html; charset=utf-8');

        echo "<pre>";
        if ($tipo == 1) {
            print_r($vet);
        } else if ($tipo == 2) {
            var_dump($vet);
        }
        echo "</pre>";
        die;
    }

}

?>