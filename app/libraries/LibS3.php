<?php
require_once APPPATH.'/libraries/LibFactoryServer.php';

class LibS3 extends LibFactoryServer
{

	protected $filesystem;

	public function __construct()
	{
		parent::__construct();
		$this->path_default = 'Home';
	}

	public function listFiles($path, $search = '', $trash = false)
	{
		$object       = new ObjectEntity();
		$object->hash = $path;
		$arquivos     = array();
		$this->trash  = $trash;
		$this->search = $search;

		// if (!$this->CI->arquivos_model->dir_exists($object->hash) && $object->hash != $this->path_default)
		// 	return false;

		// $currentDir = $this->CI->arquivos_model->getObject($object);
		// $object->hash = null;
		// $object->original_name = $search;

		// if (count($currentDir) > 0) {
		// 	$object->parent = $currentDir->id;
		// 	$breads = $this->CI->arquivos_model->get_parents($object);
		// 	$object->parent = $currentDir->id;
		// }

		// if ($trash) {
		// 	$object = new ObjectEntity();
		// 	$object->in_trash = 1;
		// }

		// $arquivos = $this->CI->arquivos_model->getObjectList($object);

		$arquivos =  $this->getObjects($object, $path);

		foreach ($arquivos as $indice => $arquivo) {
			if ($arquivo->is_image == 1)
				$arquivos[$indice]->ico_file_type = base_url() . 'thumb/show/150/150/' . $arquivo->hash;
		}

		return array('arquivos' => $arquivos, 'breads' => $this->breads);
	}

	public function uploadFile($folder, &$file, $uploaded)
	{
		$storedObject = $this->filesystem->create_object($uploaded['tmp_name'], $file->hash, $file->original_name);

		// $file->created = $this->filesystem->object_datetime($file->hash);
		// $mime = $this->filesystem->get_mime($file->hash, $uploaded['tmp_name']);

		// $mimeId = $this->CI->arquivos_model->getObjectMime($mime, $file->extension)->id_file_type;

		// $file->mime = $mimeId != '' ? $mimeId : 1;

		// $file->in_trash = false;
		// $file->is_image = $this->filesystem->is_image($file->hash, $uploaded['tmp_name']);

		return $storedObject;
	}

	public function loadFileSystem($serverId = null, $strDriver)
	{
		$serverEntity = new ServerEntity();
		$serverEntity->server_id = $serverId;
		$server = $this->CI->arquivos_model->get_server($serverEntity, true);

		switch (@$server->server_type) {

			case 1:
				$storage = $strDriver;
				// $str_key = $this->session->userdata('STR_KEY');
				$home = $server->server_path;
				$this->CI->load->library('GedLocalFileSystem');
				$this->filesystem = new GedLocalFileSystem($home, $storage);
				class_alias('GedLocalFileSystem', 'GedFileSystem');
				break;

			case 2:
				$home = $strDriver;
				$this->CI->load->library('GedS3FileSystem');
				$this->filesystem = new GedS3FileSystem($home, $server->server_key, $server->server_secret);
				class_alias('GedS3FileSystem', 'GedFileSystem');
				break;
		};
	}

	public function newDiretory($path, $diretory)
	{
		return true;
	}

	public function viewFile($object)
	{
		if ($this->filesystem->object_exists($object->hash, $bool)) {
			$download = $this->filesystem->open($object->hash, $bool);
			return $download;
		}
		return false;
	}

	public function thumb($hash, $arrSize)
	{
		$object = $this->filesystem->get_object($hash);
		$fileName = $object['real_path']."__". $arrSize['width'] . 'x' . $arrSize['height'] . $object['meta']['originalname'];

		touch($fileName);
		chmod($fileName, 0777);

		$config = array(
			'source_image' => $object['real_path'],
			'new_image'    => $fileName,
			'width'        => $arrSize['width'],
			'height'       => $arrSize['height']
		);

		$this->CI->load->library('image_lib', $config);
		if ( ! $this->CI->image_lib->resize()) {
			echo $this->CI->image_lib->display_errors();
			return;
		}

		$fp = fopen($fileName, 'rb');
		header('Content-Type: '. $this->get_mime($fileName));
		header("Content-Length: " . filesize($fileName));

		fpassthru($fp);
		return;
	}

	public function download($object, $open = false)
	{
		// if ($this->filesystem->object_exists($object->hash)){
		// 	$this->CI->load->library('LibFactoryServer');
		// 	$this->CI->libfactoryserver->use_link($download);
		// }
		$this->filesystem->download($object->hash, $open);
	}

	public function cleanTrash($object)
	{
		$removed = $this->filesystem->remove_object($object->hash);
		if ($removed['msg'] == self::$FILE_REMOVED){
			$entity = new ObjectEntity();
			$entity->hash = $removed['source'];

			if (!$this->CI->arquivos_model->delete_object($entity)) {
				return self::$UNABLE_REMOVE_FILE;
			}

		} else {
			return self::$UNABLE_REMOVE_FILE;
		}
		return self::$FILE_REMOVED;
	}

	public function viewDocument($object, $download)
	{
		if ($this->filesystem->object_exists($object->hash))
			$this->CI->arquivo_model->use_link($download);

		$this->download($object, true);
		$local_path  = FCPATH."uploads/{$object->hash}";
		header("Cache-Control: private");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Type: " . mime_content_type($local_path));
		header("Content-Length: " . filesize($local_path));
		header("Content-Disposition: inline; filename=" . $object->original_name);
		readfile ($local_path);
		exit();

		// $this->filesystem->public_open($object->hash, $server->server_path, $object->storage_key, $object->original_name);
		// return true;
	}

	public function moveFile($output_path, $actual_path)
	{
		return false;
	}

}
