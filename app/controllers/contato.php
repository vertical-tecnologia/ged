<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 15/06/15
 * Time: 14:25
 */

class contato extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));

        $this->load->model('contato_model');
    }

    public function usuario()
    {
        $entity = new ContatoEntity();
        $entity->contato_user_id = $this->input->post('contato_user_id') != '' ? $this->input->post('contato_user_id') : $this->uri->segment(3);

        $contatos = $this->contato_model->fetchAll($entity);

        for($i=0;$i<count($contatos);$i++){
            $contatos[$i]->delete_url   = base_url()."contato/excluir/".$contatos[$i]->contato_id;
            $contatos[$i]->delete_arg_url   = base_url()."contato/listar/".$contatos[$i]->user_id;
            $contatos[$i]->delete_label = 'excluir';
            $contatos[$i]->delete_arg_value = $contatos[$i]->user_id;
            $contatos[$i]->delete_callback = 'gedtable|populate';
            $contatos[$i]->delete_target = 'table_contato';

        }

        echo json_encode($contatos);
    }

    public function listar(){
        $entity = new ContatoEntity();
        $entity->contato_empresa_id = $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);

        $contatos = $this->contato_model->fetchAll($entity);

        for($i=0;$i<count($contatos);$i++){
            $contatos[$i]->delete_url   = base_url()."contato/excluir/".$contatos[$i]->contato_id;
            $contatos[$i]->delete_arg_url   = base_url()."contato/listar/".$contatos[$i]->empresa_id;
            $contatos[$i]->delete_label = 'excluir';
            $contatos[$i]->delete_arg_value = $contatos[$i]->empresa_id;
            $contatos[$i]->delete_callback = 'gedtable|populate';
            $contatos[$i]->delete_target = 'table_contato';

        }

        echo json_encode($contatos);

    }

    public function save()
    {

        $entity = new ContatoEntity();
        $entity->contato_id = $this->input->post('contato_id');
        $entity->contato_valor = $this->input->post('contato_valor');
        $entity->tipo_id = $this->input->post('tipo_id');

        $save = $this->contato_model->save($entity);


        if($save != FALSE){

            if($this->input->post('empresa_id') != ''){
                $contato = new ContatoEmpresaEntity();
                $contato->contato_id = $save->contato_id;
                $contato->empresa_id = $this->input->post('empresa_id');
                $contato->principal = false;

                if($this->contato_model->save_empresa($contato) === TRUE)
                    echo json_encode(array('error' => 0, 'msg' => 'Contato cadastrado com sucesso.', 'title' => 'Sucesso'));
                else
                    echo json_encode(array('error' => 1, 'msg' => 'Erro ao cadastrar conato.', 'title' => 'Erro'));

                redirect('empresa/visualizar/'.$contato->empresa_id);

            }

            if($this->input->post('usuario_id') != ''){
                $contato = new ContatoUsuarioEntity();
                $contato->contato_id = $save->contato_id;
                $contato->usuario_id = $this->input->post('usuario_id');


                if($this->contato_model->save_usuario($contato) === TRUE)
                    echo json_encode(array('error' => 0, 'msg' => 'Contato cadastrado com sucesso.', 'title' => 'Sucesso'));
                else
                    echo json_encode(array('error' => 1, 'msg' => 'Erro ao cadastrar conato.', 'title' => 'Erro'));

                redirect('perfil');

            }
        }
    }

    public function excluir()
    {

        $entity = new ContatoEntity();
        $entity->contato_id =  $this->uri->segment(3);
        $contato = $this->contato_model->fetch($entity);


        if($this->uri->segment(4) == 'empresa')
            $return = $this->uri->segment(4) . '/visualizar/' . $contato->empresa_id ;
        else
            $return = 'perfil';

        if($contato->principal == true){
            $this->session->set_flashdata('warning', 'O contato principal não pode ser excluído.');
            redirect($return);
        }


        if($this->contato_model->excluir($entity) === TRUE){

            $this->session->set_flashdata('success', 'Contato excluído com sucesso.');
            redirect($return);

        } else {
            $this->session->set_flashdata('error', 'Erro ao excluir contato.');
            redirect($return);
        }

    }
}
