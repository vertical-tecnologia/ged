<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 27/05/15
 * Time: 01:18
 */

class plano extends CI_Controller{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();

        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

        if(!$this->authenticator->is_superuser()) {
            redirect('admin/index');
        }

        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('plano_model');
    }

    public function index() {

        $this->get_view('plano_model', 'index');

    }

    public function cadastro(){

        $this->get_view('plano_model', 'cadastro');
    }

    public function save()
    {
        $plano = new PlanoEntity();
        $plano->plan_id = $this->input->post('plan_id');
        $plano->plan_name = $this->input->post('plan_name');
        $plano->plan_desc = $this->input->post('plan_desc');
        $plano->plan_price = $this->input->post('plan_price');
        $plano->plan_startdate = data_formatada($this->input->post('plan_startdate'), 6);
        $plano->plan_enddate = data_formatada($this->input->post('plan_enddate'), 6);
        $plano->plan_status_id = $this->input->post('plan_status_id');
        $plano->service_id = $this->input->post('service_id');
        $plano->server_id = $this->input->post('server_id');
        $plano->plano_max_users = $this->input->post('plano_max_users');
        $retorno = $this->plano_model->save($plano);


        if($retorno === TRUE){
            redirect('plano');
        } else {
            return $this->cadastro();
        }
    }

    public function editar()
    {
        $plano = new PlanoEntity();

        $plano->plan_id = $this->uri->segment(3);

        $this->get_view('plano_model', 'edit', $plano);
    }

    public function excluir()
    {
        $plano = new PlanoEntity();
        $plano->plan_id = $this->uri->segment(3);

        $retorno = $this->plano_model->delete($plano);

        if($retorno === TRUE){
            redirect('plano');
        }

    }

    public function get_view($controller, $action, $params = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }
}