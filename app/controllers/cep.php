<?php

/**
 * Class cep
 * @package		App\Controllers
 */

class cep extends CI_Controller {

    public $vet_dados = null;

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->conn = $this->load->database('cep', TRUE);

        $this->conn->from('cep')->where('cod_cep', $this->input->post('cep'));

        $vet = $this->conn->get()->row();
        
        if (!empty($vet)) {

            $this->vet_dados['msg']   = 'Cep encontrado.';
            $this->vet_dados['dados'] = $vet;
            $this->vet_dados['error'] = 0;

        } else {

            $this->vet_dados['msg']   = 'Cep não encontrado.';
            $this->vet_dados['dados'] = null;
            $this->vet_dados['error'] = 1;

        }

        echo json_encode($this->vet_dados);

    }

}

/* End of file cep.php */
/* Location: ./app/controllers/cep.php */
