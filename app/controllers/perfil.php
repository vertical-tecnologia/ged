<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 24/06/15
 * Time: 10:37
 */

class perfil extends CI_Controller{

    public function __construct()
    {
        parent::__construct();

        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }


        $this->load->model('perfil_model');
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $this->get_view('perfil_model', 'profile_data');
    }

    public function image()
    {

        $config['upload_path'] = './public/static/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '2048';
        $config['overwrite']  = FALSE;
        $config['encrypt_name'] = TRUE;
        $config['max_width']  = '2048';
        $config['max_height']  = '1536';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('user_picture'))
        {
            $error = array('error' => 1, 'msg' => $this->upload->display_errors('', ''));
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($error));


        }
        else
        {
            $data = $this->upload->data();
            
            $usuario = new UsuarioEntity();
            $usuario->user_picture = $data['file_name'];



            $path = base_url().'public/static/'.$data['file_name'];
            if($this->perfil_model->update_picture($usuario)){
                $this->session->set_userdata('USER_PICTURE', $usuario->user_picture);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => 1, 'msg' => 'Foto alterada com sucesso.', 'fullpath'=>$path)));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('error' => 1, 'msg' => 'Erro ao alterar foto de perfil.')));
            }


        }
    }

    protected function crop_image($image){

        $this->load->library('image_lib');

        $image_config['image_library'] = 'gd2';
        $image_config['source_image'] = $image;
        $image_config['new_image'] = $image;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $image_config['width'] = 600;
        $image_config['height'] = 600;
        $image_config['x_axis'] = '0';
        $image_config['y_axis'] = '0';

        $this->image_lib->clear();
        $this->image_lib->initialize($image_config);

        if (!$this->image_lib->crop()){
            return false;
        }else{
            return true;
        }
    }

    public function update_pass()
    {

        if($this->input->post('new_password') != $this->input->post('cnew_password')){
            $this->session->set_flashdata('error', 'Nova senha e confirmaçao nao conferem.');
            redirect('perfil');
            return;
        }

        $entity = new UsuarioEntity();
        $entity->user_password = $this->input->post('actual_password');
        $return = $this->perfil_model->compare_password($entity);
        if($entity->user_password == $this->encrypt->decode($return->user_password)){
                $usuario = new UsuarioEntity();
                $usuario->user_password = $this->encrypt->encode($this->input->post('new_password'));
                if($this->perfil_model->update_pass($usuario)){
                    $this->session->set_flashdata('success', 'Senha alterada com sucesso.');
                } else {
                    $this->session->set_flashdata('error', 'Erro ao tentar alterar senha.');
                }
        } else {
            $this->session->set_flashdata('error', 'Sua atual inválida.');
        }

        redirect('perfil');
        return;

    }

    public function get_view($controller, $action) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action();

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }
}