<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 09/06/15
 * Time: 18:39
 */

class empresa extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->authenticator->is_logged_in()) {
			$this->authenticator->set_current_url($this->uri->uri_string());
			redirect('auth/login');
		}
		
		$this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

		$this->template_skin_model->getTemplateSkin();
		$this->vet_dados = base_dir($this->session->userdata('skin'));
		$this->load->model('empresa_model');
	}

	public function index()
	{
		$this->get_view('empresa_model', 'index');
	}

	public function cadastro()
	{
		$this->get_view('empresa_model', 'cadastro');
	}

	public function editar()
	{
		$this->get_view('empresa_model', 'editar');
	}

	public function visualizar()
	{

		$this->get_view('empresa_model', 'visualizar');
	}

	public function endereco()
	{
		$return = [
			'mtitle' => 'Cadastrar Endereço',
			'content' => $this->empresa_model->endereco()
		];

		echo json_encode($return);
	}

	public function contato()
	{
		$return = [
			'mtitle' => 'Cadastrar Contato',
			'content' => $this->empresa_model->contato()
		];

		echo json_encode($return);
	}

	public function save()
	{
		$entity = new EmpresaEntity();
		$entity->empresa_id = $this->input->post('empresa_id');

		$arrResult = $this->empresa_model->fetch($entity);

		$entity->empresa_fantasia    = isset($_POST['empresa_fantasia']) ? $_POST['empresa_fantasia'] : $arrResult->empresa_fantasia;
		$entity->empresa_razao       = isset($_POST['empresa_razao']) ? $_POST['empresa_razao'] : $arrResult->empresa_razao;
		$entity->empresa_ie          = isset($_POST['empresa_ie']) ? $_POST['empresa_ie'] : $arrResult->empresa_ie;
		$entity->empresa_cnpj        = isset($_POST['empresa_cnpj']) ? clean_string($_POST['empresa_cnpj']) : $arrResult->empresa_cnpj;
		$entity->empresa_segmento_id = isset($_POST['empresa_segmento_id']) ? $_POST['empresa_segmento_id'] : $arrResult->empresa_segmento_id;
		$entity->empresa_user_id     = isset($_POST['empresa_user_id']) ? $_POST['empresa_user_id'] : $arrResult->empresa_user_id;

		$entity->empresa_cpf_responsavel = isset($_POST['empresa_cpf_responsavel']) ? clean_string($_POST['empresa_cpf_responsavel']) : $arrResult->empresa_cpf_responsavel;
		$entity->empresa_rg_responsavel = isset($_POST['empresa_rg_responsavel']) ? clean_string($_POST['empresa_rg_responsavel']) : $arrResult->empresa_rg_responsavel;

		$total = (int) $this->input->post('total_servidor');
		if (is_int($total) && $total > 0) {
			// $arrDados = array('total_server' => $qntServidor);
			$entity->total_server = $total;
			// $this->empresa_model->saveTotalServer($entity->empresa_user_id, $arrDados);
		}
		$this->empresa_model->save($entity);
		redirect('empresa');
	}

	public function excluir()
	{
		$entity = new EmpresaEntity();
		$entity->empresa_id = $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);

		if($this->empresa_model->excluir($entity) == true)
			$this->session->set_flashdata('success', 'Empresa excluída com sucesso.');
		else
			$this->session->set_flashdata('error', 'Erro ao excluir empresa.');

		redirect('empresa');
	}

	public function servidor($argEmpresaId)
	{
		$this->get_view('empresa_model', 'servidor', $argEmpresaId);
	}

	public function get_view($controller, $action, $params = null)
	{

		if ($this->input->is_ajax_request()) {

			echo $this->$controller->$action();

		} else {

			$componentes = array();

			$componentes[0] = new stdClass();
			$componentes[0]->LOCAL_COMPONENTE = 'conteudo';
			$componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

			$componentes[1] = new stdClass();
			$componentes[1]->LOCAL_COMPONENTE = 'head';
			$componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

			$componentes[2] = new stdClass();
			$componentes[2]->LOCAL_COMPONENTE = 'topo';
			$componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

			$componentes[3] = new stdClass();
			$componentes[3]->LOCAL_COMPONENTE = 'js_arquivos';
			$componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

			$componentes[4] = new stdClass();
			$componentes[4]->LOCAL_COMPONENTE = 'menu';
			$componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

			$componentes[5] = new stdClass();
			$componentes[5]->LOCAL_COMPONENTE = 'botoes';
			$componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

			$componentes[6] = new stdClass();
			$componentes[6]->LOCAL_COMPONENTE = 'titulo_tela';
			$componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

			for ($i = 0; $i < count($componentes); $i++) {

				if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
					$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
				} else {
					$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
				}

			}

			$this->parser->parse('template/' . $this->session->userdata('template') . '/template_view', $this->vet_dados);

		}
	}

}