<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 20/06/15
 * Time: 14:37
 */

class Thumb extends CI_Controller{

	private $driver;
	private $width;
	private $height;
	private $square;
	private $filesystem;

	public function __construct()
	{
		parent::__construct();
		$this->storage = $this->session->userdata('STR_DRIVER');
		$this->str_key = $this->session->userdata('STR_KEY');
		// $this->loadFileSystem();
	}

	public function loadFileSystem()
	{
		$server = $this->get_server(new ServerEntity());
		$storage = $this->session->userdata('STR_DRIVER');
		$str_key = $this->session->userdata('STR_KEY');

		switch ($server->server_type) {

			case 1:
				$home = $server->server_path;
				$this->load->library('GedLocalFileSystem');
				$this->filesystem = new GedLocalFileSystem($home, $storage, $str_key);
				class_alias('GedLocalFileSystem', 'GedFileSystem');
				break;
			case 2:

				$home = $this->session->userdata('STR_DRIVER');
				$this->load->library('GedS3FileSystem');
				$this->filesystem = new GedS3FileSystem($home, $server->server_key, $server->server_secret);
				class_alias('GedS3FileSystem', 'GedFileSystem');

				break;
			case 3:
				break;

		};
	}

	public function show()
	{
		$this->driver = $this->session->userdata('STR_DRIVER');
		$this->width  = $this->uri->segment(3);
		$this->height = $this->uri->segment(4);
		$this->square = $this->uri->segment(6) == '1' ? false : true;

		$arrSize = array(
			'width'  => (int) $this->uri->segment(3),
			'height' => (int) $this->uri->segment(4));
		$this->validate_proportions();
		$entity = new ObjectEntity();
		$hash = explode('.', $this->uri->segment(5));
		$entity->hash = $hash[0];

		$this->load->model('arquivos_model');
		$file = $this->arquivos_model->getObject($entity);

		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($file->server_id, $this->driver);
		return $this->libfactoryserver->thumb($file->hash, $arrSize);
	}

	public function large()
	{
		$this->driver = $this->session->userdata('STR_DRIVER');
		$arrSize = array(
			'width'  => (int) $this->uri->segment(3),
			'height' => (int) $this->uri->segment(4));

		if ($arrSize['width']== 0 || $arrSize['height'] == 0) {
			echo 'Error';
			return;
		}

		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(5);

		$this->load->model('arquivos_model');
		$file = $this->arquivos_model->getObject($entity);

		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($file->server_id, $this->driver);
		return $this->libfactoryserver->thumb($file->hash, $arrSize);

	}

	public function get_mime($path)
	{
		$finfo = new finfo;
		return $finfo->file($path, FILEINFO_MIME_TYPE);
	}

	public function validate_proportions()
	{
		$this->width =  $this->width == null ? 100 : $this->width;
		$this->height = $this->height == null ? 100 : $this->height;

//        $this->width =  $this->width > 500 ? 500 : $this->width;
//        $this->height = $this->height > 500 ? 500 : $this->height;

	}

	public function get_server(ServerEntity $entity)
	{
		$this->db->from('servidor');
		if($entity->server_id != '')
			$this->db->where('server_id', $entity->server_id);
		else
			$this->db->where('server_id', $this->session->userdata('SRID'));

		return $this->db->get()->row();
	}
}