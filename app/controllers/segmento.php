<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 27/05/15
 * Time: 02:36
 */

class segmento extends CI_Controller{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();

        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        if(!$this->authenticator->is_superuser()) {
            redirect('admin/index');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('segmento_model');
        $this->load->model('segmento_diretorio_model');

    }

    public function index() {

        $this->get_view('segmento_model', 'index');

    }

    public function cadastro() {

        $this->get_view('segmento_model', 'cadastro');

    }

    public function editar()
    {
        $segmento = new SegmentoEntity();
        $segmento->segmento_id = $this->uri->segment(3);
        $this->get_view('segmento_model', 'edit', $segmento);

    }

    public function diretorios()
    {
        $segmento = new SegmentoEntity();
        $segmento->segmento_id = $this->uri->segment(3);

        if($this->input->is_ajax_request()){
            echo $this->segmento_model->directory($segmento);
            return;
        }

        $this->get_view('segmento_model', 'directory', $segmento);

    }

    public function inserirdiretorio()
    {
        $diretorio = new DiretorioEntity();
        $diretorio->dir_id = $this->input->post('dir_id') != false ? $this->input->post('dir_id') : null;
        $diretorio->dir_name = $this->input->post('dir_name') != false ? $this->input->post('dir_name') : null;
        $diretorio->dir_parent_id = $this->input->post('dir_parent_id') != false ? $this->input->post('dir_parent_id') : null;
        $diretorio->dir_segmento_id = $this->input->post('dir_segmento_id') != false ? $this->input->post('dir_segmento_id') : null;

        $this->segmento_diretorio_model->save($diretorio);
        echo json_encode($diretorio);

    }

    public function excluir()
    {
        $segmento = new SegmentoEntity();
        $segmento->segmento_id = $this->uri->segment(3);

        $retorno = $this->segmento_model->delete($segmento);

        if($retorno === TRUE){

        }
        redirect('segmento');

    }

    public function save()
    {
        $segmento = new SegmentoEntity();
        $segmento->segmento_id = $this->input->post('segmento_id');
        $segmento->segmento_name = $this->input->post('segmento_name');
        $segmento->segmento_status_id = $this->input->post('segmento_status_id');

        $retorno = $this->segmento_model->save($segmento);

        if($retorno === TRUE){
            redirect('segmento');
        } else {
            return $this->cadastro();
        }


    }

    public function novo_diretorio()
    {
        $segmento = new SegmentoEntity();
        $segmento->segmento_id = $this->uri->segment(3);

        $this->get_view('segmento_model', 'novo_diretorio', $segmento);
    }

    public function excluir_diretorio()
    {
        $this->segmento_model->excluir_diretorio();
    }

    public function save_diretorio()
    {
        $this->segmento_model->save_diretorio();
    }

    public function get_view($controller, $action ,$params =null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }


}