<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 06/06/15
 * Time: 18:26
 */

class acao extends CI_Controller{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        if(!$this->authenticator->is_superuser()) {
            redirect('admin/index');
        }
        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('action_model');
    }

    public function index(){
        $this->get_view('action_model', 'index');
    }

    public function cadastro(){
        $this->get_view('action_model', 'cadastro');
    }

    public function save()
    {
        $entity = new ActionEntity();
        $entity->action_id = $this->input->post('action_id');
        $entity->action_name = $this->input->post('action_name');
        $entity->action_module_id = $this->input->post('action_module_id');

        $return = $this->action_model->save($entity);

        if($return === true){
            redirect('acao');
        } else {
            return $this->cadastro();
        }
    }

    public function excluir()
    {
        $entity = new ActionEntity();
        $entity->action_id = $this->input->post('action_id') != '' ? $this->input->post('action_id') : $this->uri->segment(3);

        $this->action_model->delete($entity);

        redirect('acao');
    }

    public function load_action()
    {
        $entity = new stdClass();
        $entity->permissao_module_id = $this->input->post('module_id') != '' ? $this->input->post('module_id') : $this->uri->segment(3);
        $entity->permissao_type_id = $this->input->post('type_id') != '' ? $this->input->post('type_id') : $this->uri->segment(4);

        $acaoPermissao = $this->action_model->fetchAcaoPermissao($entity);


        $entity = new ActionEntity();
        $entity->action_module_id = $this->input->post('module_id') != '' ? $this->input->post('module_id') : $this->uri->segment(3);
        $acao = $this->action_model->fetchAll($entity);

        for($i=0;$i<count($acao);$i++){
            if(!isset($acaoPermissao[$i]))
                $acaoPermissao[$i] = $acao[$i];
        }

        echo json_encode($acaoPermissao);
    }

    public function get_view($controller, $action, $params = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }
}