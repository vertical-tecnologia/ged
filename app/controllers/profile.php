<?php 

class profile extends CI_Controller
{
	private $vet_dados;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cadastro_model');
		$this->template_skin_model->getTemplateSkin();
	}

	public function index()
	{
		$user_type = $this->session->userdata('USER_TYPE_ID');
		$arrProfile = array(USER_SUPERUSUARIO, USER_SUPORTE, USER_USUARIO, USER_INDEFINIDO);

		if (array_search($user_type, $arrProfile) !== false)
			redirect('admin');

		$profile = $this->uri->segment(2);

		if (!is_bool($profile) && $profile == PROFILE_COMPANY) 
			redirect('admin');

		if (!is_bool($profile) && $profile == PROFILE_PERSONAL) 
			redirect();

		$this->load->model('empresa_model');

		$user_data = $this->session->userdata('USER_ID');
		$entity = new EmpresaEntity();
		$entity->empresa_user_id = $user_data;
// var_dump($this->empresa_model->fetchAll($entity));
		if ($this->empresa_model->fetchAll($entity))
			return redirect('admin');

		$this->vet_dados['config_item'] = 'empresa';
		$this->vet_dados['page_title']  = 'Cadastre sua empresa.';
		$this->get_empty_view('profile/index_view');
	}

	public function get_empty_view($view)
	{
		$componentes = array();

		$componentes[0] = new stdClass();
		$componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
		$componentes[0]->CONFIG_COMPONENTE = $this->parser->parse($view, $this->vet_dados, TRUE);

		$componentes[1] = new stdClass();
		$componentes[1]->LOCAL_COMPONENTE  = 'head';
		$componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

		$componentes[2] = new stdClass();
		$componentes[2]->LOCAL_COMPONENTE  = 'topo';
		$componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

		$componentes[3] = new stdClass();
		$componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
		$componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

		$componentes[4] = new stdClass();
		$componentes[4]->LOCAL_COMPONENTE  = 'menu';
		$componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getSide();

		$componentes[5] = new stdClass();
		$componentes[5]->LOCAL_COMPONENTE  = 'botoes';
		$componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

		$componentes[6] = new stdClass();
		$componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
		$componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

		for ($i = 0; $i < count($componentes); $i++) {

			if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
				$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
			} else {
				$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
			}
		}

		return $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

	}
}