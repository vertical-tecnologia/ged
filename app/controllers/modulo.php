<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 06/06/15
 * Time: 16:57
 */

class modulo extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        if(!$this->authenticator->is_superuser()) {
            redirect('admin/index');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());


        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('module_model');
        $this->load->model('permissao_model');
    }

    public function index(){
        $this->get_view('module_model', 'index');
    }

    public function cadastro(){
        $this->get_view('module_model', 'cadastro');
    }

    public function save()
    {
        $entity = new ModuleEntity();
        $entity->module_id = $this->input->post('module_id');
        $entity->module_name = $this->input->post('module_name');
        $entity->module_system = 0;

        $return = $this->module_model->save($entity);

        if($return === TRUE){
            redirect('modulo');
        } else{
            return $this->cadastro();
        }
    }

    public function excluir()
    {
        $entity = new ModuleEntity();
        $entity->module_id = $this->input->post('module_id') != '' ? $this->input->post('module_id') : $this->uri->segment(3);

        $this->module_model->delete($entity);

        redirect('modulo');
    }

    public function load_permission()
    {
        $object = new stdClass();
        $object->module_id = $this->input->post('module_id');
        $object->type_id = $this->input->post('type_id');

            echo json_encode($this->permissao_model->get_permissions($object));
    }

    public function save_permission()
    {
        $object = new stdClass();
        $object->module_id = $this->input->post('module_id');
        $object->type_id = $this->input->post('type_id');
        $object->permissions = $this->input->post('permissions');



        if(true === $this->permissao_model->save_permission($object)){
            $data = [
                'type' =>'success',
                'title' => 'Sucesso',
                'message' => 'Permissões salvas.',
            ];
        } else {
            $data = [
                'type' =>'error',
                'title' => 'Erro',
                'message' => 'Não foi possível salvar as permissões.',
            ];
        }

        echo json_encode($data);

    }

    public function get_view($controller, $action, $params = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }
}