<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 27/05/15
 * Time: 01:15
 */

class cadastro extends CI_Controller{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('cadastro_model');
        $this->vet_dados = base_dir('public');

        if($this->authenticator->is_logged_in() === true) {
            redirect('admin/');
        }

    }

    public function index()
    {
        $this->get_view('cadastro_model', 'plans');
    }

    public function plano()
    {
        $this->get_view('cadastro_model', 'plano');
    }

    public function login()
    {
        $this->get_view('cadastro_model', 'login');
    }

    public function forgot()
    {
        $email = $this->input->post('email');

        if($email == '')
            $this->get_view('cadastro_model', 'forgot', true);
        else
            $this->cadastro_model->forgot();
    }


    public function reset()
    {
        $key = $this->cadastro_model->redefinition_key($this->uri->segment(2));

        if(count($key) != 1){
            $this->session->set_flashdata('error', 'Link invalido.');
            redirect('forgot');
            return;
        }

        $password = $this->input->post('password');
        $confirmation = $this->input->post('confirmation');

        if($password == '' && $confirmation == ''){
            $this->get_view('cadastro_model', 'reset', true);
            return;
        }
        else{
            if($password != $confirmation){
                $this->session->set_flashdata('error', 'Senha e confirmação não conferem.');
                $this->get_view('cadastro_model', 'reset', true);
                redirect('reset/'.$this->uri->segment(2));
                return;
            }
            $this->cadastro_model->reset();
        }
    }



    public function save()
    {
        $this->vet_dados = base_dir('public');

        $entity                     = new UsuarioEntity();
        $entity->user_id            = $this->input->post('user_id');
        $entity->user_fullname      = $this->input->post('user_fullname');
        $entity->user_email         = $this->input->post('user_email');
        $entity->user_password      = $this->encrypt->encode($this->input->post('user_password'));
        $entity->user_login         = $this->input->post('user_login');
        $entity->user_trusted_mail  = false;
        $entity->user_register_date = date("Y-m-d h:i:s");
        $entity->user_hash          =  $this->encrypt->encode($entity->user_email. "," . date("Y-m-d h:i:s"));
        $entity->user_group_id      = null;
        $entity->user_type_id       = 2;
        $entity->user_status_id     = 6;
        $entity->user_plan_id       = $this->input->post('plan_id');
        $entity->user_key           = random_string('alnum', 100);
        $entity->owner              = true;

        if($this->cadastro_model->email_exists($entity)){
            $this->session->set_flashdata('warning', 'Este email já está em uso.');
            redirect('cadastro/plano/'.$this->input->post('plan_id'));
            return;
        }

        if($this->cadastro_model->login_exists($entity)){
            $this->session->set_flashdata('warning', 'Este login já está em uso.');
            redirect('cadastro/plano/'.$this->input->post('plan_id'));
            return;
        }


        $usuario = $this->cadastro_model->save($entity);

        $this->vet_dados['usuario'] = array($usuario);

        if($usuario->user_id != ''){
            if($this->enviar_email($usuario)){
                $this->session->set_flashdata('success', 'Usuário cadastrado com sucesso. Por favor verifique seu email.');
            } else{
                $this->session->set_flashdata('error', 'Erro ao tentar enviar email de confirmação.');
            }
            redirect('cadastro');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar enviar email de confirmação.');
            redirect('cadastro');
        }
    }

    public function confirm()
    {
        $hash = urldecode(urldecode($this->uri->segment(3)));
        $entity = new UsuarioEntity();
        $entity->user_hash = $hash;

        $this->get_view('cadastro_model', 'confirm', $entity);
    }

    protected function enviar_email($usuario){

        $this->load->library('Mailer');

        $usuario->user_hash = urlencode(urlencode($usuario->user_hash));
        $this->vet_dados['usuario'] = array($usuario);

        Mailer::$message = $this->parser->parse('template/public/new_email_view', $this->vet_dados, TRUE);
        Mailer::$subject = "[GED] - Cadastro de Usuário!";
        Mailer::$to = $usuario->user_email;
        Mailer::$toName = $usuario->user_fullname;
        $m = Mailer::send_mail();

        if($m == true){
            syslog::generate_log("NEW_USER_EMAIL_SUCCESS");
            return true;
        }
        else
        {
            syslog::generate_log("NEW_USER_EMAIL_ERROR");
            return false;
        }
    }

    public function get_view($controller, $action, $params = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getLoginHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getLoginJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/public/template_view',$this->vet_dados);

        }

    }
}