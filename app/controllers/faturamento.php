<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 25/06/15
 * Time: 15:36
 */

class faturamento extends CI_Controller{

    public function __construct()
    {
        parent::__construct();

        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

        $this->load->model('faturamento_model');
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $this->get_view('faturamento_model', 'index');
    }

    public function cadastro()
    {
        $this->get_view('faturamento_model', 'cadastro');
    }

    public function detalhes()
    {
        $id = (int) $this->uri->segment(3);
        
        if($id > 0){
            $entity = new FaturaEntity();
            $entity->fatura_id = $id;
            $this->get_view('faturamento_model', 'detalhes', $entity);

        } else {
            $this->session->set_flashdata('error', 'Fatura não encontrada.');
            redirect('faturamento');
        }
    }

    public function cliente()
    {
        $client_id = $this->input->post('client_id') != '' ? $this->input->post('client_id') : $this->uri->segment(3);
        $cliente = new ClienteEntity();
        $cliente->client_id = $client_id;
        $cliente = $this->faturamento_model->get_cliente($cliente);
        $plano = new PlanoEntity();
        $plano->plan_id = $cliente->client_plan_id;
        $servico = $this->faturamento_model->get_cliente_servicos($plano);

        $data = array();

        if(date('d') > $cliente->client_data_vencimento){
            $mes = date('m') + '1';
        } else{
            $mes = date('m');
        }

        $date = $cliente->client_data_vencimento.'/'.$mes.'/'.date('Y');

        $data['cliente'] = array(
            'client_id' => $cliente->client_id,
            'client_data_vencimento' => $date
        );
        for($i=0;$i<count($servico);$i++){
            $data['servicos'][$i] = new stdClass();
            $data['servicos'][$i]->servico_id    = $servico[$i]->service_id;
            $data['servicos'][$i]->service_price = $servico[$i]->service_price;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function enviar()
    {
        $entity = new FaturaEntity();
        $entity->fatura_id = $this->uri->segment(3);
        $enviar = $this->faturamento_model->enviar_fatura($entity);

        if($enviar === true){
            $entity->status_id = 2;
            $this->faturamento_model->update_status($entity);
            $this->session->set_flashdata('success', 'Fatura enviada ao cliente com sucesso.');
        }
        else
            $this->session->set_flashdata('error', 'Erro ao tentar enviar fatura ao cliente.');

        redirect('faturamento/detalhes/'.$entity->fatura_id);

    }

    public function save()
    {
        $entity = new FaturaEntity();

        $entity->fatura_id = $this->input->post('fatura_id');
        $entity->client_id = $this->input->post('client_id');
        $entity->fatura_emissao = $this->input->post('fatura_emissao');
        $entity->fatura_vencimento = type_format($this->input->post('fatura_vencimento'), 'data');
        $entity->fatura_obs = $this->input->post('fatura_obs');
        $entity->status_id = 1;
        $entity->fatura_hash = random_string('alnum', 30);
        $entity->fatura_itens = $this->input->post('service_id');


        $fatura = $this->faturamento_model->save($entity);


        if($fatura->fatura_id > 0){
            $entity->fatura_id = $fatura->fatura_id;
            if($this->faturamento_model->saveItens($entity)){
                redirect('faturamento/detalhes/'.$fatura->fatura_id);
            } else {
                $this->session->set_flashdata('error', 'Erro ao cadastrar fatura.');
                return $this->cadastro();
            }
        } else {
            return $this->cadastro();
        }

    }

    public function boleto()
    {
        $entity = new FaturaEntity();
        $entity->fatura_id = $this->uri->segment(3);
        $this->faturamento_model->boleto($entity);
    }


    public function get_view($controller, $action, $params = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }


}
