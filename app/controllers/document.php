<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 21/07/15
 * Time: 15:29
 */

class document extends CI_Controller{

	public $filesystem;

	public function __construct()
	{
		parent::__construct();
	}

	public function open()
	{
		$download = new DownloadEntity();
		$download->key = $this->uri->segment(3);
		$link = $this->has_link($download);

		if(!$link  || $link->single == 1){

			$download->link_id = $link->link_id;
			$download->downloaded = date('y-m-d h:i:s', time());
			$download->user_agent = $this->input->user_agent();
			$download->ip_address = getRealIP();
			$obj = new ObjectEntity();
			$obj->hash = $link->hash;
			$object = $this->getObject($obj);
			$server = $this->get_server_path($object->storage_key);

			$this->load->library('LibFactoryServer');
// var_dump($object->server_id); 
			$this->libfactoryserver->setTypeServer($object->server_id, $object->storage_key);
// exit;
			$this->libfactoryserver->viewDocument($object, $download);
			return true;

		} else {
			return null;
		}
	}

	// public function loadFileSystem($str_driver, $server)
	// {
	//     $storage = $str_driver;
	//     $str_key = $server->client_key;

	//     switch (@$server->server_type) {

	//         case 1:
	//             $home = $server->server_path;
	//             $this->load->library('GedLocalFileSystem');
	//             $this->filesystem = new GedLocalFileSystem($home, $storage, $str_key);
	//             class_alias('GedLocalFileSystem', 'GedFileSystem');
	//             break;
	//         case 2:
	//             $home = $this->session->userdata('STR_DRIVER') != null ? $this->session->userdata('STR_DRIVER') : $str_driver;
	//             $this->load->library('GedS3FileSystem');
	//             $this->filesystem = new GedS3FileSystem($home, $server->server_key, $server->server_secret);
	//             class_alias('GedS3FileSystem', 'GedFileSystem');

	//             break;
	//         case 3:
	//             break;

	//     };


	// }

	public function has_link(DownloadEntity $entity)
	{
		$this->db->from('download_link');

		if($entity->hash != '')
			$this->db->where('hash', $entity->hash);

		if($entity->expires != '')
			$this->db->where('expires', $entity->expires);

		if($entity->key != '')
			$this->db->where('key', $entity->key);

		if($entity->downloaded != '')
			$this->db->where('downloaded', $entity->downloaded);

		if($entity->downloads != '')
			$this->db->where('downloads', $entity->downloads);

		if($entity->single != '')
			$this->db->where('single', $entity->single);


		$row = $this->db->get()->row();

		if(count($row) > 0)
			return $row;
		else
			return false;
	}

	public function use_link(DownloadEntity $entity)
	{
		$this->db->set('downloads',  1);
		$this->db->set('downloaded', $entity->downloaded);
		$this->db->set('ip_address', $entity->ip_address);
		$this->db->set('user_agent', $entity->user_agent);

		$this->db->where('link_id', $entity->link_id);

		if($this->db->update('download_link'))
			return true;
		else
			return false;
	}

	public function getObject(ObjectEntity $entity, $details = false)
	{
		if ($details == true) {
			$this->db->select('source.*, file_type.*, usuario.user_id as uid, usuario.user_fullname as uname, parent.hash as parent_id');
			$this->db->join('file_object as parent', 'source.parent = parent.id', 'left');
		}
		$this->db->from('file_object as source');
		$this->db->join('file_type', 'file_type.id_file_type = source.mime');

		$this->db->where('source.hash', $entity->hash);

		if ($details == true) {
			$this->db->join('usuario', 'usuario.user_key = source.owner');
		}

		return $this->db->get()->row();

	}


	public function get_server_path($client_volume)
	{
		$this->db->select('servidor.*, cliente.client_key');
		$this->db->from('servidor');
		$this->db->join('cliente', 'cliente.server_id = servidor.server_id');
		$this->db->where('cliente.client_volume', $client_volume);

		return $this->db->get()->row();
	}
}
