<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 27/05/15
 * Time: 01:18
 */

class usuario extends CI_Controller{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();

        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('usuario_model');
    }

    public function excluir()
    {
        $this->get_view('usuario_model', 'excluir');
    }

    public function safe_delete()
    {
        $this->usuario_model->safe_delete();

        redirect('usuario');
    }


    public function endereco()
    {
        $return = [
            'mtitle' => 'Cadastrar Endereço',
            'content' => $this->usuario_model->endereco()
        ];

        echo json_encode($return);
    }

    public function contato()
    {
        $return = [
            'mtitle' => 'Cadastrar Contato',
            'content' => $this->usuario_model->contato()
        ];

        echo json_encode($return);
    }

    public function index() {

        $this->get_view('usuario_model', 'index');

    }

    public function visualizar() {

        $usuario = new UsuarioEntity();
        $usuario->user_id = $this->uri->segment(3);

        $this->get_view('usuario_model', 'visualizar', $usuario);

    }

    public function empresa() {

        $empresa = new EmpresaEntity();
        $empresa->empresa_user_id = $this->uri->segment(3);
        $this->get_view('usuario_model', 'empresa', $empresa);

    }

    public function cadastro(){

        $this->get_view('usuario_model', 'cadastro');
    }

    public function confirmar()
    {
        $this->usuario_model->confirmar();
    }
    
    public function save()
    {
        $usuario = new UsuarioEntity();
        $usuario->user_id = $this->input->post('user_id');

        if((int) $usuario->user_id == 0){
            $validation = $this->form_validation->run('usuario');
        } else {
            $validation = $this->form_validation->run('usuario_update');
        }

        if ($validation  == FALSE) {
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->input->post('user_id') == '')
                return $this->cadastro();
            else
                return $this->editar($usuario);
        }


        $usuario->user_type_id = $this->input->post('user_type_id');
        $usuario->user_fullname = $this->input->post('user_fullname');
        $usuario->user_login = $this->input->post('user_login');
        $usuario->user_email = $this->input->post('user_email');
        $usuario->user_group_id = $this->input->post('user_group_id');
        $usuario->user_type_id = $this->input->post('user_type_id');
        $usuario->user_status_id = $this->input->post('user_status_id');
        $usuario->user_password = $this->input->post('user_password') != '' ? $this->input->post('user_password') : random_string('alnum', 6);

        $retorno = $this->usuario_model->save($usuario);

        if($retorno != false){

            if($this->input->post('empresa_id') != '' && ($this->input->post('user_id') == '')){
                $usuarioEmpresa = new EmpresaUsuarioEntity();
                $usuarioEmpresa->id_empresa = $this->input->post('empresa_id');
                $usuarioEmpresa->id_usuario = $retorno->user_id;

                if($this->usuario_model->saveUsuarioEmpresa($usuarioEmpresa) == true){
                    $this->session->set_flashdata('success', 'Usuário cadastrado com sucesso');
                    redirect('usuario');
                }

            } else {
                $this->db->trans_commit();
                redirect('usuario');
            }

        } else {
            return $this->cadastro();
        }
    }

    public function editar($user)
    {
        $usuario = new UsuarioEntity();
        $usuario->user_id = $this->uri->segment(3) != '' ? $this->uri->segment(3) : $user;
        $this->get_view('usuario_model', 'edit', $usuario);
    }

    public function get_view($controller, $action, $params = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }


    public function check_email($email)
    {
        return $this->usuario_model->check_email($email);
    }

    public function check_login($login)
    {
        return $this->usuario_model->check_login($login);
    }
}