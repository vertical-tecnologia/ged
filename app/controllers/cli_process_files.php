<?php
class cli_process_files extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('arquivos_model');
        $this->arquivos_model->processScheduleFiles();
    }
}