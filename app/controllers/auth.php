<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class auth
 * @package		App\Controllers
 */

class auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir('public');
        $this->load->model('auth_model');
    }

    public function login() 
    {
        if($this->authenticator->is_logged_in()) {
            if($this->authenticator->is_admin()) {
                redirect('admin/index');
            }
        }

        if ($this->authenticator->login($this->input->post('login_usuario'), $this->input->post('senha_usuario')))
        {
            $redirect_url = $this->authenticator->get_current_url();
            $this->authenticator->set_current_url('');

            if($redirect_url != '') {
                redirect($redirect_url);
            } else {
//                if($this->authenticator->is_admin()) {
                    redirect('profile');
//                }
            }
        } else {
            redirect('login');
            return;
        }

    }

    public function logout() {
        $this->authenticator->logout();
        redirect('login');
    }

    public function access_denied() {
        $this->load->view('access_denied');
    }


    public function get_view($controller, $action) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action();

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getLoginHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getLoginJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/public/template_view',$this->vet_dados);

        }

    }

}