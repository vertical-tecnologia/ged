<?php
/**
 * Created by PhpStorm.
 * @package		App\Controllers
 * User: vagner
 * Date: 27/05/15
 * Time: 01:15
 */

use Aws\S3\S3Client;
use Aws\Sdk;

class arquivos extends CI_Controller{

	public $vet_dados;

	public function __construct()
	{
		parent::__construct();
		if(!$this->authenticator->is_logged_in()) {
			$this->authenticator->set_current_url($this->uri->uri_string());
			redirect('auth/login');
		}

		$this->vet_dados = base_dir($this->session->userdata('skin'));
		$this->load->model('arquivos_model');

		$this->load->library('GedLocalFileSystem');
	}

	public function index()
	{
		$this->folder();
	}

	public function edit()
	{
		$this->get_view('arquivos_model', 'edit');
	}

	public function folder()
	{
		$this->get_view('arquivos_model', 'index');
	}

	public function view()
	{
		$this->get_view('arquivos_model', 'view');
	}

	public function upload()
	{
		$this->get_view('arquivos_model', 'upload');
	}

	public function ftp()
	{
		$this->get_view('arquivos_model', 'ftp');
	}

	public function elfinder()
	{
		$this->arquivos_model->elfinder();
	}

	public function save_label($id = null)
	{
		$idTag = $_POST["idTag"];
	 
		$entity = new ObjectEntity();
		
		$entity->hash = $this->uri->segment(3);
		
		if($idTag != ""){
			$result = $this->arquivos_model->add_tag($_POST);
			
			redirect('arquivos/edit/' . $entity->hash );
		}

		$object = $this->arquivos_model->getObject($entity);
		if($this->input->is_ajax_request()){
			echo $this->arquivos_model->detail();
			return;
		} else {

			if($entity->hash == '' || $object->id == ''){
				$this->session->set_flashdata('error', 'Objeto não existe.');
				redirect('arquivos');
				return;
			}

			$result = $this->arquivos_model->add_tag();

			if($result != true)
				$this->session->set_flashdata('error', 'Erro ao cadastrar TAG.');

		}

		redirect('arquivos/edit/' . $entity->hash );
	}

	public function remove_tag()
	{
		$tag_id = $this->uri->segment(3);
		$object = $this->arquivos_model->remove_tag($tag_id);

		if($object != false)
			redirect('arquivos/edit/' . $object);
		else
			redirect('arquivos');
	}

	public function detail()
	{
		if($this->input->is_ajax_request()){
			echo $this->arquivos_model->detail();
			return;
		}

		$this->get_view('arquivos_model', 'detail');
	}

	public function share()
	{
		if($this->input->is_ajax_request()){
			echo $this->arquivos_model->share();
			return;
		}

		$this->get_view('arquivos_model', 'share');
	}

	public function trash()
	{
		if($this->input->is_ajax_request()){
			echo $this->arquivos_model->trash();
			return;
		}

		$this->get_view('arquivos_model', 'trash');
	}

	public function sendtotrash()
	{
		if($this->input->is_ajax_request()){

			$deleted = $this->arquivos_model->sendtotrash();

			if($deleted == GedLocalFileSystem::$DELETE_PERMISSION_DENIED){
				$retorno = array(
					'error' => 'Você não tem permissão para excluir este arquivo.',
				);
			}
			else if($deleted == GedLocalFileSystem::$FILE_REMOVED){
				$retorno = array(
					'success' => 'Arquivo excluído com sucesso!',
				);
			} else {
				$retorno = array(
					'error' => 'Não foi possível excluir o arquivo!',
				);
			}

			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($retorno));
			return;
		}

		$this->get_view('arquivos_model', 'sendtotrash');
	}

	public function newdir()
	{
		$this->get_view('arquivos_model', 'new_directory');
	}

	public function export()
	{
		$this->get_view('arquivos_model', 'export');
	}

	public function downloadxml($hash, $example)
	{
		echo $this->arquivos_model->download_xml($hash, $example);
	}

	public function download()
	{
		if($this->input->is_ajax_request()){
			echo $this->arquivos_model->download();
			return;
		}

		$this->get_view('arquivos_model', 'download');
	}

	public function open()
	{
		$embbed = $this->uri->segment(4) == 'true' ? true : false;

		if ($this->input->is_ajax_request() && !$embbed){
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($this->arquivos_model->open_link()));
			return;
		}

		if ($embbed == true){
			echo $this->arquivos_model->open();
			return;
		}

		$this->get_view('arquivos_model', 'open_link');
	}


	public function get()
	{
//        if($this->input->is_ajax_request()){
			echo $this->arquivos_model->get();
			return;
//        }

//        $this->get_view('arquivos_model', 'get');
	}

	public function saveDir()
	{
		$directory = $this->arquivos_model->saveDir();
		$server = $this->input->get('server');

		if ($directory != false) {
			redirect('arquivos/folder/'.$this->uri->segment(3)."?server={$server}");
			return true;
		}
		redirect('arquivos/newdir/'.$this->uri->segment(3)."?server={$server}");
	}

	public function setshare()
	{
		$entity = new ShareEntity();
		$entity->object = $this->input->post('object');
		$entity->group = $this->input->post('group_id');
		$entity->user = $this->input->post('user_id');
		$entity->email = $this->input->post('emails');
		$entity->shared_at = date('Y-m-d H:i:s');

		if($this->arquivos_model->shared($entity) === false){
			$return = [
				'msg' => 'Erro ao compartilhar pasta/arquivo.',
				'error' => true
			];
		} else {
			$return = [
				'msg' => 'Compartilhado com sucesso.',
				'success' => true
			];
		}



		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));
	}

	public function change()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$entity->description = $this->input->post('value');

		$return = $this->arquivos_model->update_description($entity);

		if($return != false)
			$data =  $return->description;
		else
			$data = 'Erro ao alterar descriçao do arquivo.';

		echo $data;
	}

	public function send()
	{
		$upload = $this->arquivos_model->send();

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($upload));
	}

	public function recover()
	{
		$recover = $this->arquivos_model->recover();

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($recover));
	}

	public function recovery()
	{
		if($this->input->is_ajax_request()){

			$recovered = $this->arquivos_model->recovery();

			if($recovered == GedLocalFileSystem::$FILE_RECOVERED){
				$retorno = array(
					'success' => 'Arquivo recuperado com sucesso!',
				);
			} else {
				$retorno = array(
					'error' => 'Não foi possível recuperar o arquivo!',
				);
			}

			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($retorno));
			return;
		}

		$this->get_view('arquivos_model', 'sendtotrash');
	}

	public function clean_trash()
	{
		if($this->input->is_ajax_request()){
			echo $this->arquivos_model->clean_trash();
			return;
		}
		$this->get_view('arquivos_model', 'clean_trash');
	}

	public function empty_trash()
	{
		if($this->input->is_ajax_request()){

			$recovered = $this->arquivos_model->empty_trash();
			if($recovered == GedLocalFileSystem::$FILE_REMOVED){
				$retorno = array(
					'success' => 'Lixeira esvaziada com sucesso!',
				);
			} else {
				$retorno = array(
					'error' => 'Erro ao esvaziar lixeira!',
				);
			}

			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($retorno));
			return;
		}

		$this->get_view('arquivos_model', 'sendtotrash');
	}

	public function trash_folder()
	{
		$recover = $this->arquivos_model->trash_folder();

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($recover));
	}

	public function send_trash_folder()
	{
		if($this->input->is_ajax_request()){

			$recovered = $this->arquivos_model->send_trash_folder();


			if($recovered == GedLocalFileSystem::$FILE_REMOVED){
				$retorno = array(
					'success' => 'Pasta excluída com sucesso!',
				);
			} else if($recovered == GedLocalFileSystem::$INVALID_TYPE){
				$retorno = array(
					'error' => 'Tipo inválido!',
				);
			} else {
				$retorno = array(
					'error' => 'Erro ao excluir pasta!',
				);
			}

			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($retorno));
			return;
		}

		$this->get_view('arquivos_model', 'trash_folder');
	}


	public function preview()
	{
		echo $this->arquivos_model->preview();
	}

	public function get_view($controller, $action, $params = null) {

		if ($this->input->is_ajax_request()) {

			echo $this->$controller->$action();

		} else {

			$componentes = array();

			$componentes[0] = new stdClass();
			$componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
			$componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

			$componentes[1] = new stdClass();
			$componentes[1]->LOCAL_COMPONENTE  = 'head';
			$componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

			$componentes[2] = new stdClass();
			$componentes[2]->LOCAL_COMPONENTE  = 'topo';
			$componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

			$componentes[3] = new stdClass();
			$componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
			$componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

			$componentes[4] = new stdClass();
			$componentes[4]->LOCAL_COMPONENTE  = 'menu';
			$componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

			$componentes[5] = new stdClass();
			$componentes[5]->LOCAL_COMPONENTE  = 'botoes';
			$componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

			$componentes[6] = new stdClass();
			$componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
			$componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

			for ($i = 0; $i < count($componentes); $i++) {

				if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
					$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
				} else {
					$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
				}

			}

			$this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

		}

	}

	public function set_view()
	{
		$redirectTo = $this->uri->segment(3) != '' ? '/'.$this->uri->segment(3) : "";
		$this->arquivos_model->set_view();

		if ($this->input->get('server'))
			$redirectTo .= '?server='.$this->input->get('server');

		redirect('arquivos' . $redirectTo."");
	}


	public function compartilhamento()
	{
		$this->get_view('arquivos_model', 'compartilhamentos');
	}

	public function galeria()
	{
		$this->get_view('arquivos_model', 'galeria');
	}

	public function subDirectory()
	{
		$file_id   = $this->input->get('file');
		$server_id = $this->input->get('server');

		$this->load->model('arquivos_model');
		$directories = $this->arquivos_model->getChildrenDir($file_id, $server_id);
		// $file_id = $this->input->get('file');
		$this->output->set_content_type('application/json')
		->set_output(json_encode($directories, true));
		return;
	}

	public function getTag()
	{
		if ($_POST['id']) {
			$tagID = $_POST['id'];

			$dados = $this->arquivos_model->get_tag($tagID);

			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($dados));
			return;
		}
	}

	public function permissionByDirectory()
	{
		$folderHash = $this->input->get('hash');

		$this->load->model('arquivos_model');
		$folder = $this->arquivos_model->findFolderByHash($folderHash);
		$folderPermission = $this->arquivos_model->verifyPermissionId($folder);
		$permissions = array();
		if(!empty($folderPermission)){
			$permissions[] = $folderPermission->readable;
			$permissions[] = $folderPermission->listable;
			$permissions[] = $folderPermission->writable;
			$permissions[] = $folderPermission->deletable;
		}

		// $this->output->set_content_type('application/json')
		// ->set_output(json_encode($permissions, true));
		echo json_encode($permissions, true);
		exit;
	}

	public function recentFiles()
	{
		$this->get_view('arquivos_model', 'recentFiles');
	}
}
