<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 18/09/15
 * Time: 18:11
 */
class Shared extends CI_Controller
{
    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $this->vet_dados['shared_hash'] = $this->uri->segment(2);
        $data =  $this->encrypt->decode(urldecode(urldecode($this->uri->segment(2))));
        $data = explode('>', $data);
        $this->get_view('visualize' , $data);
    }

    public function download()
    {
        $this->vet_dados['shared_hash'] = $this->uri->segment(2);
        $data =  $this->encrypt->decode(urldecode(urldecode($this->uri->segment(2))));
        $data = explode('>', $data);


        $this->load->model('arquivos_model');
        $entity = new ObjectEntity();
        $entity->hash = $data[0];
        $breads = null;

        $object = $this->arquivos_model->getObject($entity, true);

        $this->get_driver($object);

        $entity = new ObjectEntity();
        $entity->hash = $object->hash;

        $external = ['fileName' => $object->original_name];

        $open = $this->arquivos_model->open_and_download($entity, $external);
    }

    protected function visualize($data){

        $this->load->model('arquivos_model');
        $entity = new ObjectEntity();
        $entity->hash = $data[0];
        $breads = null;

        $object = $this->arquivos_model->getObject($entity, true);
        if(count($object) == 0){
            return $this->parser->parse('arquivos/file_not_exits', $this->vet_dados, TRUE);
        }

        if (isset($object->parent) && !is_null($object->parent)) {
            $parent = new ObjectEntity();
            $parent->parent = $object->parent;
            $breads = $this->arquivos_model->get_parents($parent);
        }

        $this->vet_dados['current_directory_url'] = isset($object->parent) && !is_null($object->parent) ? 'folder/'.$object->parent_id : 'folder/Home';

        $this->get_driver($object);

        if ($object->is_image == 1) {
            $object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
        }

        $object->size = By2Mb($object->size);

        if($object->in_trash){
            $this->vet_dados['current_directory_url'] = 'lixeira';
            $this->vet_dados['breads'] = '';
        } else
            $this->vet_dados['breads'] = $breads;


        $this->vet_dados['arquivo'] = array($object);

        $this->vet_dados['media_preview'] = null;

        return $this->parser->parse('arquivos/file_public_detail_view', $this->vet_dados, TRUE);
    }

    protected function get_driver($object)
    {
        $client = $this->get_client_server($object);
        if(count($client) > 0 )
        {
            $this->session->set_userdata('SRID', $client->server_id);
            $this->session->set_userdata('STR_DRIVER', $object->storage_key);
        }

    }

    protected function get_client_server()
    {
        $this->db->select('servidor.*');
        $this->db->from('cliente');
        $this->db->join('servidor', 'servidor.server_id = cliente.server_id');

        return $this->db->get()->row();
    }

    protected function get_view($action, $data = null) {

        define('SIDEBAR_POSITION', null);
        define('BACKGROUND_PATTERN', null);
        define('FOOTER_POSITION', null);

        if ($this->input->is_ajax_request()) {

            echo $this->$action($data);

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$action($data);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }
}