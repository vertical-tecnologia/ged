<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 27/05/15
 * Time: 17:22
 */

class servico extends CI_Controller{

    public function __construct()
    {
        parent::__construct();

        if(!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        if(!$this->authenticator->is_superuser()) {
            redirect('admin/index');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());


        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model("servico_model");
    }

    public function index()
    {
        $this->get_view("servico_model", "index");
    }

    public function cadastro()
    {
        $this->get_view("servico_model", "cadastro");
    }

    public function editar()
    {
        $servico = new ServicoEntity();
        $servico->service_id = $this->uri->segment(3);

        $this->get_view("servico_model", "editar", $servico);
    }

    public function save()
    {
        $servico = new ServicoEntity();
        $servico->service_id = $this->input->post('service_id');
        $servico->service_name = $this->input->post('service_name');
        $servico->service_desc = $this->input->post('service_desc');
        $servico->service_price = $this->input->post('service_price');
        $servico->service_recursive = $this->input->post('service_recursive');
        $servico->service_storage = $this->input->post('service_storage');
        $servico->service_status_id = $this->input->post('status_id');

        $return = $this->servico_model->save($servico);


        if($return === TRUE){
            redirect('servico');
            return;
        } else {
            return $this->cadastro();
        }
    }

    public function excluir()
    {
        $servico = new ServicoEntity();
        $servico->service_id = (int) $this->uri->segment(3);

        $return  = $this->servico_model->delete($servico);


        if($return === TRUE){
            redirect('servico');
            return;
        } else {
            redirect('servico/cadastro');
            return;
        }
    }



    public function get_view($controller, $action, $param = null) {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($param);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }
}