<?php
/**
 * Created by PhpStorm.
 * @package		App\Controllers
 * User: vagner
 * Date: 08/07/15
 * Time: 21:37
 */

class boleto extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('faturamento_model');

        $entity = new FaturaEntity();
        $entity->fatura_hash = $this->uri->segment(2);

        $this->faturamento_model->boleto($entity);

    }


}