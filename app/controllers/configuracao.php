<?php
/**
 * Created by PhpStorm.
 * @package		App\Controllers
 * User: vagner
 * Date: 20/07/15
 * Time: 20:31
 */

class configuracao extends CI_Controller{

	public function __construct()
	{
		parent::__construct();

		if(!$this->authenticator->is_logged_in()) {
			$this->authenticator->set_current_url($this->uri->uri_string());
			redirect('auth/login');
		}


		$this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());

		$this->template_skin_model->getTemplateSkin();
		$this->vet_dados = base_dir($this->session->userdata('skin'));
		$this->load->model("configuracao_model");
	}

	public function index()
	{
		redirect('configuracao/quota');
	}

	public function banco()
	{
		$this->get_view('configuracao_model', 'banco');
	}

	public function espaco()
	{
		$this->get_view('configuracao_model', 'espaco');
	}

	public function home()
	{
		$this->get_view('configuracao_model', 'home');
	}

	public function contrato()
	{
		$this->get_view('configuracao_model', 'contrato');
	}

	public function save_contrato()
	{
		$this->configuracao_model->save_contrato();
		redirect('configuracao/contrato');
	}

	public function save_banco()
	{
		$this->configuracao_model->save_banco();
		redirect('configuracao/banco');
	}

	public function save_home()
	{
		$this->configuracao_model->save_home();
		$this->session->set_flashdata('success', 'Informações alteradas com sucesso.');
		redirect('configuracao/home');
	}

	public function executavel()
	{
		$this->get_view('configuracao_model', 'executavel');
	}

	public function save_executavel()
	{
		$this->configuracao_model->save_executavel();
		redirect('configuracao/executavel');
	}

	public function adicional()
	{
		$this->get_view('configuracao_model', 'adicional');
	}


	public function armazenamento()
	{
		$this->get_view('configuracao_model', 'armazenamento');
	}

	public function save_armazenamento()
	{
		$retorno = $this->configuracao_model->add_armazenamento();

		if($retorno === TRUE){
			redirect('configuracao/armazenamento');
		} else {
			return $this->armazenamento();
		}
	}

	public function excluir_armazenamento()
	{
		$this->configuracao_model->excluir_armazenamento();
	}

	public function ftp()
	{
		$this->get_view('configuracao_model', 'ftp');
	}

	public function quota_edit()
	{
		$this->get_view('configuracao_model', 'quota_edit');
	}

	public function quota_user()
	{
		$this->configuracao_model->save_quota_user();
		redirect('configuracao/quota');
	}

	public function save_ftp()
	{
		$this->configuracao_model->update_ftp_server();
		redirect('arquivos/ftp');
	}

	public function save_storage()
	{
		$return = $this->configuracao_model->save_storage();

		if($return == true)
			$this->session->set_flashdata('success', 'Espaço alterado com sucesso.');
		else
			$this->session->set_flashdata('Error', 'Erro ao alterar espaço.');

		redirect('configuracao/espaco');
	}

	public function save_adicional()
	{
		$this->configuracao_model->save_adicional();
		redirect('admin');
	}

	public function quota()
	{
		$this->get_view('configuracao_model', 'quota');
	}

	public function meu_contrato()
	{
		$this->get_view('configuracao_model', 'meu_contrato');
	}

	public function switch_theme()
	{
		if($this->uri->segment(3) == '' && !$this->input->is_ajax_request()){
			redirect('admin');
			return;
		}

		$this->db->set('theme', $this->uri->segment(3));
		$this->db->where('user_id', $this->session->userdata('USER_ID'));
		$this->db->update('configuration');

		if(!$this->input->is_ajax_request())
			redirect('admin');
	}

	public function switch_background()
	{
		if($this->uri->segment(3) == '' && !$this->input->is_ajax_request()){
			redirect('admin');
			return;
		}

		$this->db->set('background', $this->uri->segment(3));
		$this->db->where('user_id', $this->session->userdata('USER_ID'));
		$this->db->update('configuration');

		if(!$this->input->is_ajax_request())
			redirect('admin');
	}

	public function set_sidebar()
	{
		$value = $this->uri->segment(3) != '' ? $this->uri->segment(3) : null;
		$this->db->set('sidebar_position', $value);
		$this->db->where('user_id', $this->session->userdata('USER_ID'));
		$this->db->update('configuration');

		if(!$this->input->is_ajax_request())
			redirect('admin');
	}

	public function save_quota()
	{
		if($this->configuracao_model->save_quota() != false){
			$this->session->set_flashdata('success', 'Atualizações salvas com sucesso.');
		} else {
			$this->session->set_flashdata('error', 'Erro ao aplicar atualizações.');
		};

		redirect('configuracao/quota');
	}

	public function xml()
	{
		$this->get_view('configuracao_model', 'xml');
	}

	public function get_view($controller, $action, $params = null) {

		if ($this->input->is_ajax_request()) {

			echo $this->$controller->$action();

		} else {

			$componentes = array();

			$componentes[0] = new stdClass();
			$componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
			$componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

			$componentes[1] = new stdClass();
			$componentes[1]->LOCAL_COMPONENTE  = 'head';
			$componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

			$componentes[2] = new stdClass();
			$componentes[2]->LOCAL_COMPONENTE  = 'topo';
			$componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

			$componentes[3] = new stdClass();
			$componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
			$componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

			$componentes[4] = new stdClass();
			$componentes[4]->LOCAL_COMPONENTE  = 'menu';
			$componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

			$componentes[5] = new stdClass();
			$componentes[5]->LOCAL_COMPONENTE  = 'botoes';
			$componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

			$componentes[6] = new stdClass();
			$componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
			$componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

			for ($i = 0; $i < count($componentes); $i++) {

				if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
					$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
				} else {
					$this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
				}

			}

			$this->parser->parse('template/'.$this->session->userdata('template').'/template_view',$this->vet_dados);

		}

	}

}