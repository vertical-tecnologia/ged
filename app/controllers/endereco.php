<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 12/06/15
 * Time: 14:23
 */

class endereco extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('endereco_model');
    }

    public function listar(){
        $entity = new EnderecoEntity();
        if($this->input->post('parent') == 'user' || $this->uri->segment(4) == 'user')
            $entity->user_id = $this->input->post('user_id') != '' ? $this->input->post('user_id') : $this->uri->segment(3);
        else
            $entity->empresa_id = $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);

        $enderecos = $this->endereco_model->fetchAll($entity);

        for($i=0;$i<count($enderecos);$i++){
            $enderecos[$i]->delete_url   = base_url()."endereco/excluir/".$enderecos[$i]->endereco_id;
            if($this->input->post('parent') == 'user' || $this->uri->segment(4) == 'user'){
                $enderecos[$i]->delete_arg_value = $enderecos[$i]->user_id;
                $enderecos[$i]->delete_arg_url   = base_url()."endereco/listar/".$enderecos[$i]->user_id.'/user';
            }
            else{
                $enderecos[$i]->delete_arg_value = $enderecos[$i]->empresa_id;
                $enderecos[$i]->delete_arg_url   = base_url()."endereco/listar/".$enderecos[$i]->empresa_id;
            }
            $enderecos[$i]->delete_label = 'excluir';

            $enderecos[$i]->delete_callback = 'gedtable|populate';
            $enderecos[$i]->delete_target = 'table_endereco';

        }

        echo json_encode($enderecos);
    }

    public function save()
    {
        $entity = new EnderecoEntity();
        $entity->endereco_id = $this->input->post('endereco_id');
        $entity->endereco_cep = $this->input->post('endereco_cep');
        $entity->endereco_logradouro = $this->input->post('endereco_logradouro');
        $entity->endereco_num = $this->input->post('endereco_num');
        $entity->endereco_bairro = $this->input->post('endereco_bairro');
        $entity->endereco_cidade = $this->input->post('endereco_cidade');
        $entity->endereco_estado = $this->input->post('endereco_estado');
        $entity->endereco_complemento = $this->input->post('endereco_complemento');
        $entity->endereco_alias = $this->input->post('endereco_alias');
        $entity->endereco_principal = $this->input->post('endereco_principal');

        $save = $this->endereco_model->save($entity);

        if($save != FALSE){
            if($this->input->post('empresa_id') != ''){
                $endereco = new EnderecoEmpresaEntity();
                $endereco->endereco_id = $save->endereco_id;
                $endereco->empresa_id = $this->input->post('empresa_id');
                $endereco->principal = false;

                if($this->endereco_model->save_empresa($endereco) != false)
                    $this->session->set_flashdata('success', 'Endereço cadastrado com sucesso.');
                else
                    $this->session->set_flashdata('error', 'Erro ao cadastrar endereço.');

                redirect('empresa/visualizar/' . $endereco->empresa_id);


            } else  if($this->input->post('user_id') != ''){

                $endereco = new UsuarioEnderecoEntity();
                $endereco->endereco_id = $save->endereco_id;
                $endereco->user_id = $this->input->post('user_id');


                if($this->endereco_model->save_user($endereco) != false)
                    $this->session->set_flashdata('success', 'Endereço cadastrado com sucesso.');
                else
                    $this->session->set_flashdata('error', 'Erro ao cadastrar endereço.');

                redirect('perfil');


            }
        }
    }

    public function excluir()
    {
        $entity = new EnderecoEntity();
        $entity->endereco_id = $this->input->post('endereco_id') != '' ? $this->input->post('endereco_id') : $this->uri->segment(3);

        $endereco = $this->endereco_model->fetch($entity);

        if($this->uri->segment(4) == 'empresa')
            $return = $this->uri->segment(4) . '/visualizar/' . $endereco->empresa_id ;
        else
            $return = 'perfil';

        if($endereco->principal == true){
            $this->session->set_flashdata('warning', 'O endereço principal não pode ser excluído.');
            redirect($return);
        }


        if($this->endereco_model->excluir($entity) === TRUE){

            $this->session->set_flashdata('success', 'Endereço excluído com sucesso.');
            redirect($return);

        } else {
            $this->session->set_flashdata('error', 'Erro ao excluir endereço.');
            redirect($return);
        }

    }


}