<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Controllers
 * Date: 27/05/15
 * Time: 02:36
 */
class grupo extends CI_Controller
{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();

        if (!$this->authenticator->is_logged_in()) {
            $this->authenticator->set_current_url($this->uri->uri_string());
            redirect('auth/login');
        }

        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());


        $this->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('grupo_model');
    }

    public function index()
    {

        $this->get_view('grupo_model', 'index');

    }

    public function cadastro()
    {

        $this->get_view('grupo_model', 'cadastro');

    }

    public function permissoes()
    {
        $this->get_view('grupo_model', 'permissao');
    }

    public function usuarios()
    {
        $entity = new GroupEntity();
        $entity->group_id = $this->uri->segment(3);

        $this->get_view('grupo_model', 'usuarios', $entity);
    }

    public function editar()
    {
        $entity = new GroupEntity();
        $entity->group_id = $this->uri->segment(3);

        $this->get_view('grupo_model', 'edit', $entity);

    }

    public function save_usuarios()
    {
        $entity = new GroupUserEntity();
        $entity->group_id = $this->input->post('group_id');
        $entity->user_id = $this->input->post('usuarios');


        $return = $this->grupo_model->save_usuarios($entity);

        if ($return == true) {
            $this->session->set_flashdata('success', 'Atualizações salvas.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar atualizar informações do grupo.');
        }

        redirect('grupo/usuarios/' . $entity->group_id);
    }

    public function excluir()
    {

        $retorno = $this->grupo_model->delete($this->uri->segment(3));

        if ($retorno === TRUE) {
            redirect('grupo');
        }   

    }

    public function save()
    {
        $entity = new GroupEntity();
        $entity->group_id = $this->input->post('group_id');
        $entity->group_name = $this->input->post('group_name');
        $entity->empresa_id = $this->session->userdata('EID');
        $entity->group_owner = $this->session->userdata('USER_ID');


        $retorno = $this->grupo_model->save($entity);

        if ($retorno === FALSE) {
            return $this->cadastro();
        } else {
            redirect('grupo');
        }


    }

    public function folder_permissions()
    {
        $entity = new GroupPermissionEntity();
        $entity->group_id = $this->uri->segment(3);
        $entity->object_id = $this->uri->segment(4);


        $permisison = $this->grupo_model->get_folder_permissions($entity);

        $permisison->readable = (bool)$permisison->readable;
        $permisison->writable = (bool)$permisison->writable;
        $permisison->listable = (bool)$permisison->listable;
        $permisison->deletable = (bool)$permisison->deletable;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($permisison));
    }

    public function save_folder_permissions()
    {
        $entity = new GroupPermissionEntity();
        var_dump($_POST);

        if (!$this->input->post('permission_id') || $this->input->post('permission_id') == 'undefined')
            $_POST['permission_id'] = null;

        $entity->permission_id = $this->input->post('permission_id');
        $entity->group_id  = $this->input->post('group_id');
        $entity->object_id = $this->input->post('folder_id');
        $entity->deletable = $this->input->post('del') == 'on' ? true : false;
        $entity->writable  = $this->input->post('write') == 'on' ? true : false;
        $entity->listable  = $this->input->post('list') == 'on' ? true : false;
        $entity->readable  = $this->input->post('read') == 'on' ? true : false;

        $update = $this->grupo_model->update_permission($entity);

        if($update != false)
            $this->session->set_flashdata('success','Permissoes salvas.');
        else
            $this->session->set_flashdata('error','Erro ao alterar permissoes.');

        redirect('grupo/permissoes/'.$entity->group_id);
    }

    public function get_view($controller, $action, $params = null)
    {

        if ($this->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$controller->$action($params);

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->template_skin_model->getMenu();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            $this->parser->parse('template/' . $this->session->userdata('template') . '/template_view', $this->vet_dados);

        }

    }


}