<?php 

class sincronizacaoxml extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function searchFile()
	{
		$this->load->model(array('empresa_model', 'servidor_model'));
		$arrCompany = $this->empresa_model->fetchAll(new EmpresaEntity());

		$arrCompanyId = array();
		foreach ($arrCompany as $company)
			$arrCompanyId[] = $company->empresa_id;

		$where = array(
			'servidor.server_type'                   => SERVER_FTP,
			'servidor.enable'                       => true,
			'servidor.directory_monitor IS NOT NULL' => NULL);

		$arrServers = $this->servidor_model->fetchAll(new ServerEntity(), 'servidor.server_id', $where, array('column' => 'servidor.empresa_id', 'values' => $arrCompanyId));

		if (!$arrServers)
			return false;

		$this->load->library('LibFtp');
		foreach ($arrServers as $server)
			$this->libftp->searchFileXML($server->server_id);
	}
}