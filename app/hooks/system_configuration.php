<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Hooks
 * Date: 28/07/15
 * Time: 11:48
 */

class system_configuration {

    private $CI;
    public $vet_dados;
    private $arrClassesPermitidas = array('profile');

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->template_skin_model->getTemplateSkin();
        $this->vet_dados = base_dir($this->CI->session->userdata('skin'));
        $this->CI->form_validation->set_error_delimiters('<div class="error">', '</div>');

    }

    public function verify_configuration()
    {

        $controller = $this->CI->router->fetch_class();

        if (array_search($controller, $this->arrClassesPermitidas) !== false)
            return true;

        if($this->CI->session->userdata('USER_ROLE') == 'CLIADMIN' || $this->CI->session->userdata('IS_OWNER') == false)
        {
            $this->verify_quota();
        }

        if($this->CI->session->userdata('USER_ROLE') != 'CLIADMIN' || $this->CI->session->userdata('IS_OWNER') == false)
            return;


        if (!in_array($this->CI->router->fetch_class(), array('cep', 'auth'))){

            $this->tem_empresa();

            $this->sou_membro();

            $this->tem_endereco();

            $this->tem_contatos();

            $this->tem_cliente();

            $this->tem_contrato();
// if ($controller == 'admin') {
//     echo $this->CI->session->userdata('USER_ROLE').'<br>';
//     echo 'before tem_fatura'; 
// }
            $this->tem_fatura();
// if ($controller == 'admin') {
//     echo $this->CI->session->userdata('USER_ROLE').'<br>';
//     echo 'after tem_fatura'; 
//     exit;
// }
        }

    }

    protected function tem_cliente()
    {
        $this->CI->db->from('cliente');
        $this->CI->db->where('client_empresa_id', $this->CI->session->userdata("EID"));

        $row = $this->CI->db->get()->row();

        if(count($row) == 0){
            $e = $this->get_cli_empresa();

            $plan = $this->get_plan();
            $entity = new ClienteEntity();
            $entity->client_empresa_id = $this->CI->session->userdata("EID");
            $entity->client_key = random_string('alnum', '32');
            $entity->client_volume = clean_string($e->empresa_fantasia, true).random_string('alnum', '32');
            $entity->client_local = true;
            $entity->server_id = $plan->server_id;
            $entity->client_plan_id = $plan->plan_id;
            $entity->client_data_vencimento = 15;
            $cliente = $this->create_client($entity);

            if(false != $cliente){

                $this->CI->session->set_userdata("STR_DRIVER", $cliente->client_volume);
                $this->CI->session->set_userdata("STR_KEY", $cliente->client_key);
                $this->CI->session->set_userdata("CLID", $cliente->client_id);
                $this->CI->session->set_userdata("SRID", $plan->server_id);

                $this->createDirectories();
            }
        }

    }

    protected function get_cli_empresa()
    {
        $this->CI->db->from('empresa');
        $this->CI->db->where('empresa.empresa_id', $this->CI->session->userdata("EID"));

        return $row = $this->CI->db->get()->row();
    }

    protected function tem_contrato()
    {
        $cli = $this->CI->session->userdata('CLID');

        $this->CI->db->from('contrato');
        $this->CI->db->where('contrato_cliente', $cli);
        $row = $this->CI->db->get()->row();

        if(count($row) == 0){

            if($this->CI->input->post('action') == 'save'){
                $this->save_contrato();
            }

            $contrato = $this->get_contrato();

            $empresa = $this->get_contract_data($cli);
            setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');

            $empresa->data_adesao = strftime('%d de %B de %Y', strtotime('today'));
            foreach($empresa as $key => $val){
                $contrato->modelo_contrato = str_replace('{'.$key.'}', $val, $contrato->modelo_contrato);
            }

            $this->vet_dados['contrato'] = array($contrato);
            echo $this->get_empty_view('new_contrato');
            exit;
        }
    }

    public function new_contrato(){

        $this->vet_dados['config_item'] = 'contrato';
        $this->vet_dados['page_title'] = 'Termos de uso e Contrato.';

        $this->vet_dados['user_id'] = $this->CI->session->userdata('USER_ID');
        $this->vet_dados['user_name'] = $this->CI->session->userdata('USER_NAME');

        $this->vet_dados['___services'] = $this->services_plan(true);
        $this->vet_dados['_plan'] = array($this->get_plan());

        return $this->CI->parser->parse('config/new_contrato_view', $this->vet_dados, TRUE);
    }

    public function save_contrato()
    {
        $accept = (bool) $this->CI->input->post('accept');
        $plan = $this->get_plan();

        if(false == $accept){
            $this->CI->session->set_flashdata('warning', 'Voce deve aceitar os termos de uso antes de usar o GED');
            redirect('save_contrato');
            return;
        }

        $clid = $this->CI->session->userdata('CLID');

        $contrato = new ContratoEntity();
        $contrato->contrato_adesao = date('Y-m-d h:i:s');
        $contrato->contrato_ativo = true;
        $contrato->contrato_cliente = $clid;
        $contrato->accepted = $accept;
        $contrato->contrato_valor = $plan->plan_price;
        $contrato->text = $this->CI->input->post('contrato_text');

        $this->create_contrato($contrato);

        redirect('admin');
    }

    protected function tem_empresa()
    {
        $user = $this->CI->session->userdata('USER_ID');

        $this->CI->db->from('empresa');
        $this->CI->db->where('empresa_user_id', $user);
        $row = $this->CI->db->get()->row();

        if(count($row) == 0){
            if ($this->CI->input->post('action') == 'save'){
                $this->save_empresa();
            }

            echo $this->get_empty_view('new_empresa');
            exit;
        }
        else{
            $this->CI->session->set_userdata("EID", $row->empresa_id);
            $this->CI->session->set_userdata("EKEY", $row->empresa_key);
            return true;
        }

    }

    public function new_empresa()
    {
        $this->CI->load->model('segmento_model');
        $segmentEntity = new SegmentoEntity();
        $segmentEntity->segmento_status_id = 2;
        $this->vet_dados['segmento']    = $this->CI->segmento_model->fetchAll($segmentEntity);
        $this->vet_dados['config_item'] = 'empresa';
        $this->vet_dados['page_title']  = 'Cadastre sua empresa.';

        $profile = $this->CI->uri->segment(2);

        if ($profile == STR_PROFILE_PERSONAL) {       
            $vet['empresa_cpf_responsavel'] = set_value('empresa_cpf_responsavel');
            $vet['empresa_rg_responsavel']  = set_value('empresa_rg_responsavel');
        }

        if ($profile == STR_PROFILE_COMPANY) {
            $vet['empresa_razao']    = set_value('empresa_razao');
            $vet['empresa_fantasia'] = set_value('empresa_fantasia');
            $vet['empresa_cnpj']     = set_value('empresa_cnpj');
            $vet['empresa_ie']       = set_value('empresa_ie');
        }

        $vet['empresa_segmento_id']    = set_value('empresa_segmento_id');
        $vet['empresa_user_id']        = $this->CI->session->userdata('USER_ID');
        $vet['empresa_id']             = set_value('empresa_id');
        $vet['client_data_vencimento'] = set_value('client_data_vencimento');
        $vet['disabled']               = "";

        $this->vet_dados['user_id']   = $this->CI->session->userdata('USER_ID');
        $this->vet_dados['user_name'] = $this->CI->session->userdata('USER_NAME');

        $this->vet_dados['empresa'] = array($vet);

        if ($profile == STR_PROFILE_PERSONAL)
            return $this->CI->parser->parse('config/new_empresa_personal_view', $this->vet_dados, TRUE);
        return $this->CI->parser->parse('config/new_empresa_view', $this->vet_dados, TRUE);
    }

    public function save_empresa()
    {
        $validation = $this->CI->input->post('empresa_cpf_responsavel') ? 'empresa_cpf' : 'empresa';
        if ($this->CI->form_validation->run($validation) == false)
            return false;

        $entity                      = new EmpresaEntity();
        $entity->empresa_cnpj        = clean_string($this->CI->input->post('empresa_cnpj'));
        $entity->empresa_razao       = $this->CI->input->post('empresa_razao');
        $entity->empresa_fantasia    = $this->CI->input->post('empresa_fantasia');
        $entity->empresa_ie          = strtoupper($this->CI->input->post('empresa_ie'));
        $entity->empresa_segmento_id = $this->CI->input->post('empresa_segmento_id');
        $entity->empresa_user_id     = $this->CI->input->post('empresa_user_id');
        $entity->empresa_key         = random_string('alnum');
        $entity->empresa_principal   = 1;

        $cpf = $this->CI->input->post('empresa_cpf_responsavel');
        $entity->empresa_cpf_responsavel = str_replace(array('.', '-'), '', $cpf);
        $rg = $this->CI->input->post('empresa_rg_responsavel');
        $entity->empresa_rg_responsavel  = str_replace(array('.', '-'), '', $rg);

        $this->CI->load->model('empresa_model');
        if ($this->CI->empresa_model->save($entity) != false)
            $this->CI->session->set_userdata('EID', $entity->empresa_id);
        else 
            $this->CI->session->set_flashdata('error', "Erro ao tentar cadastrar empresa");

        redirect('admin');
    }

    protected function sou_membro()
    {
        $member = $this->get_member();

        if(count($member) == 0){

            $this->CI->db->set('id_empresa', $this->CI->session->userdata("EID"));
            $this->CI->db->set('id_usuario', $this->CI->session->userdata("USER_ID"));
            $this->CI->db->insert('empresa_usuario');
            }
    }

    protected function tem_endereco(){
        $enderecos = $this->get_address();
        if(count($enderecos) == 0){

            if($this->CI->input->post('action') == 'save'){

                if($this->save_endereco() == false){
                    echo $this->get_empty_view('new_endereco');
                    exit();
                }

                return;
            }

            echo $this->get_empty_view('new_endereco');
            exit();
        }

    }

    public function new_endereco()
    {

        $this->vet_dados['page_title'] = 'Cadastre o endereço de sua empresa.';
        $this->vet_dados['config_item'] = 'endereco';

        $vet['endereco_id'] = set_value('endereco_id');
        $vet['endereco_cep'] = set_value('endereco_cep');
        $vet['endereco_logradouro'] = set_value('endereco_logradouro');
        $vet['endereco_num'] = set_value('endereco_num');
        $vet['endereco_bairro'] = set_value('endereco_bairro');
        $vet['endereco_cidade'] = set_value('endereco_cidade');
        $vet['endereco_estado'] = set_value('endereco_estado');
        $vet['endereco_complemento'] = set_value('endereco_complemento');
        $vet['endereco_alias'] = set_value('endereco_alias');
        $vet['empresa_id'] = $this->CI->session->userdata('EMPRESA_ID');
//
        $vet['disabled'] = "";

        $this->vet_dados['endereco'] = array($vet);

        $this->vet_dados['user_id'] = $this->CI->session->userdata('USER_ID');
        $this->vet_dados['user_name'] = $this->CI->session->userdata('USER_NAME');

        return $this->CI->parser->parse('config/new_empresa_endereco_view', $this->vet_dados, TRUE);
    }

    protected function tem_contatos()
    {
        $contacts = $this->get_contacts();

        if(count($contacts) < 2){

            if($this->CI->input->post('action') == 'save'){
                if($this->save_empresa_contatos() == false ){
                    echo $this->get_empty_view('new_contato');
                    exit;
                }
            }

            echo $this->get_empty_view('new_contato');
            exit;
        }
    }

    public function new_contato()
    {

        $this->vet_dados['page_title'] = 'Cadastre os contatos de sua empresa.';
        $this->vet_dados['config_item'] = 'contato';

        $vet['email_empresa'] = set_value('email_empresa');
        $vet['telefone_empresa'] = set_value('telefone_empresa');

        $vet['disabled'] = "";

        $this->vet_dados['user_id'] = $this->CI->session->userdata('USER_ID');
        $this->vet_dados['user_name'] = $this->CI->session->userdata('USER_NAME');

        $this->vet_dados['contato'] = array($vet);
        return $this->CI->parser->parse('config/new_empresa_contatos_view', $this->vet_dados, TRUE);
    }

    public function save_empresa_contatos()
    {
        if($this->CI->form_validation->run('contato_initial') == false){
            return false;
        }

        $this->CI->load->model('contato_model');

        $email = new ContatoEntity();
        $email->tipo_id = 2;
        $email->contato_valor = $this->CI->input->post('email_empresa');
        $email->contato_principal = 1;



        $email_save = $save = $this->CI->contato_model->save($email);

        if($email_save != false){
            $emailEmpresa = new ContatoEmpresaEntity();
            $emailEmpresa->empresa_id = $this->CI->session->userdata('EID');
            $emailEmpresa->contato_id = $email_save->contato_id;
            $emailEmpresa->principal = 1;

            $mailEmp = $this->CI->contato_model->save_empresa($emailEmpresa);
        }


        $telefone = new ContatoEntity();
        $telefone->tipo_id = 1;
        $telefone->contato_valor = $this->CI->input->post('telefone_empresa');
        $telefone->contato_principal = 1;

        $telefone_save = $save = $this->CI->contato_model->save($telefone);

        if($telefone_save != false){
            $telefoneEmpresa = new ContatoEmpresaEntity();
            $telefoneEmpresa->empresa_id = $this->CI->session->userdata('EID');
            $telefoneEmpresa->contato_id = $telefone_save->contato_id;
            $telefoneEmpresa->principal = 1;

            $telEmp = $this->CI->contato_model->save_empresa($telefoneEmpresa);
        }

        if($mailEmp == true && $telEmp == true)
            $this->CI->session->set_flashdata('success', 'Contatos salvos com sucesso.');

        redirect('admin');

    }

    public function get_empty_view($action) {

        if ($this->CI->input->is_ajax_request()) {

            echo $this->$controller->$action();

        } else {

            $componentes = array();

            $componentes[0] = new stdClass();
            $componentes[0]->LOCAL_COMPONENTE  = 'conteudo';
            $componentes[0]->CONFIG_COMPONENTE = $this->$action();

            $componentes[1] = new stdClass();
            $componentes[1]->LOCAL_COMPONENTE  = 'head';
            $componentes[1]->CONFIG_COMPONENTE = $this->CI->template_skin_model->getHead();

            $componentes[2] = new stdClass();
            $componentes[2]->LOCAL_COMPONENTE  = 'topo';
            $componentes[2]->CONFIG_COMPONENTE = $this->CI->template_skin_model->getTopo();

            $componentes[3] = new stdClass();
            $componentes[3]->LOCAL_COMPONENTE  = 'js_arquivos';
            $componentes[3]->CONFIG_COMPONENTE = $this->CI->template_skin_model->getJS();

            $componentes[4] = new stdClass();
            $componentes[4]->LOCAL_COMPONENTE  = 'menu';
            $componentes[4]->CONFIG_COMPONENTE = $this->CI->template_skin_model->getSide();

            $componentes[5] = new stdClass();
            $componentes[5]->LOCAL_COMPONENTE  = 'botoes';
            $componentes[5]->CONFIG_COMPONENTE = $this->CI->template_skin_model->getBotoes();

            $componentes[6] = new stdClass();
            $componentes[6]->LOCAL_COMPONENTE  = 'titulo_tela';
            $componentes[6]->CONFIG_COMPONENTE = $this->CI->template_skin_model->getTituloTela();

            for ($i = 0; $i < count($componentes); $i++) {

                if (!isset($this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE])) {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] = $componentes[$i]->CONFIG_COMPONENTE;
                } else {
                    $this->vet_dados[$componentes[$i]->LOCAL_COMPONENTE] .= $componentes[$i]->CONFIG_COMPONENTE;
                }

            }

            return $this->CI->parser->parse('template/'.$this->CI->session->userdata('template').'/template_view',$this->vet_dados);

        }

    }

    protected function get_member()
    {
        $this->CI->db->from('empresa_usuario');
        $this->CI->db->where('id_usuario', $this->CI->session->userdata('USER_ID'));

        $row = $this->CI->db->get()->row();

        return $row;

    }

    protected function get_address()
    {
        $this->CI->db->from('empresa_endereco');
        $this->CI->db->where('empresa_id', $this->CI->session->userdata('EID'));
        $this->CI->db->where('principal', 1);

        return $this->CI->db->get()->result();
    }

    public function create_client(ClienteEntity $entity)
    {
        $this->CI->db->set('client_key', $entity->client_key);
        $this->CI->db->set('client_local', $entity->client_local);
        $this->CI->db->set('client_volume', $entity->client_volume);
        $this->CI->db->set('client_empresa_id', $entity->client_empresa_id);
        $this->CI->db->set('server_id', $entity->server_id);
        $this->CI->db->set('client_data_vencimento', $entity->client_data_vencimento);
        $this->CI->db->set('client_plan_id', $entity->client_plan_id);

        $this->CI->db->insert('cliente');
        $entity->client_id = $this->CI->db->insert_id();

        if($entity->client_id > 0)
            return $entity;
        else
            return false;
    }

    public function save_endereco()
    {
        if($this->CI->form_validation->run('endereco_initial') == false){
            return false;
        }

        $this->CI->load->model('endereco_model');

        $entity = new EnderecoEntity();
        $entity->endereco_id = $this->CI->input->post('endereco_id');
        $entity->endereco_cep = $this->CI->input->post('endereco_cep');
        $entity->endereco_logradouro = $this->CI->input->post('endereco_logradouro');
        $entity->endereco_num = $this->CI->input->post('endereco_num');
        $entity->endereco_bairro = $this->CI->input->post('endereco_bairro');
        $entity->endereco_cidade = $this->CI->input->post('endereco_cidade');
        $entity->endereco_estado = $this->CI->input->post('endereco_estado');
        $entity->endereco_complemento = $this->CI->input->post('endereco_complemento');
        $entity->endereco_alias = $this->CI->input->post('endereco_alias');
        $entity->endereco_principal = true  ;

        $save = $this->CI->endereco_model->save($entity);

        if ($save != FALSE) {

            if ($this->CI->session->userdata('EID') != '') {

                $endereco = new EnderecoEmpresaEntity();
                $endereco->endereco_id = $save->endereco_id;
                $endereco->empresa_id = $this->CI->session->userdata('EID');
                $endereco->principal = 1;
                $empresa_endereco = $this->CI->endereco_model->save_empresa($endereco);

                if ($empresa_endereco != false)
                    $this->CI->session->set_flashdata('success', 'Endereço cadastrado com sucesso.');
                else
                    $this->CI->session->set_flashdata('success', 'Erro ao cadastrar endereço.');


            }
        } else {
            $this->CI->session->set_flashdata('success', 'Erro ao cadastrar endereço.');
        }

        redirect('admin');
    }

    // public function choose_profile()
    // {
    //     $this->vet_dados['config_item'] = 'empresa';
    //     $this->vet_dados['page_title']  = 'Cadastre sua empresa.';

    //     if (!isset($_GET['profile']))
    //         return $this->CI->parser->parse('config/choose_profile_view', $this->vet_dados, TRUE);
    // }

    protected function create_contrato(ContratoEntity $entity)
    {
        $this->CI->db->set('contrato_adesao', $entity->contrato_adesao);
        $this->CI->db->set('contrato_ativo', $entity->contrato_ativo);
        $this->CI->db->set('contrato_cliente', $entity->contrato_cliente);
        $this->CI->db->set('contrato_valor', $entity->contrato_valor);
        $this->CI->db->set('accepted', $entity->accepted);
        $this->CI->db->set('texto', $entity->text);
        $this->CI->db->insert('contrato');

        $entity->contrato_id = $this->CI->db->insert_id();

        if($entity->contrato_id > 0){
            return $entity;
        } else {
            return false;
        }

    }

    protected function get_contacts()
    {
        $this->CI->db->from('empresa_contato');
        $this->CI->db->join('contato', 'contato.contato_id = empresa_contato.contato_id');
        $this->CI->db->where_in('contato.tipo_id', array(1, 2));
        $this->CI->db->where('principal', 1);
        $this->CI->db->where('empresa_contato.empresa_id', $this->CI->session->userdata('EID'));

        return $this->CI->db->get()->result();
    }

    protected function get_contrato()
    {
        $this->CI->db->select('modelo_contrato');
        $this->CI->db->from('compania');

        return $this->CI->db->get()->row();

    }

    protected function get_plan()
    {
        $this->CI->db->from('plano');
        $this->CI->db->join('usuario', 'usuario.user_plan_id = plano.plan_id');
        $this->CI->db->where('usuario.user_id', $this->CI->session->userdata('USER_ID'));

        return $this->CI->db->get()->row();
    }

    public function tem_fatura()
    {
        if(count($this->has_invoice()) == 0){

            $this->CI->load->model('faturamento_model');

            $services = $this->services_plan();

            $entity = new FaturaEntity();

            $entity->fatura_emissao = date('Y-m-d h:i:s');
            $entity->fatura_vencimento = date('Y-m-d h:i:s', time() + ((60*60*24) * 10));
            $entity->client_id = $this->CI->session->userdata('CLID');
            $entity->status_id = 1;
            $entity->fatura_obs = 'Fatura automatica gerada no cadastro do cliente.';
            $entity->fatura_hash = random_string('alnum', 255);

            $fatura = $this->CI->faturamento_model->save($entity);
            foreach($services as $service){
                $fatura->fatura_itens[] = $service->service_id;
            }
// if ($this->CI->router->fetch_class() == 'admin') {
//     echo '<br>into has_invoice'; 
//     var_dump($entity);
// }
            $this->CI->faturamento_model->saveItens($entity);
            $this->CI->faturamento_model->enviar_fatura($entity);
            redirect('admin');

        }
    }

    protected function has_invoice()
    {
        $this->CI->db->from('faturas');
        $this->CI->db->where('client_id', $this->CI->session->userdata('CLID'));
        return $this->CI->db->get()->result();
    }

    protected function services_plan($name = false)
    {
        if($name == true)
        $this->CI->db->select('servico.service_id, servico.service_name');
        else
        $this->CI->db->select('servico.service_id');

        $this->CI->db->from('servico');
        $this->CI->db->join('plano_servico', 'plano_servico.service_id = servico.service_id');
        $this->CI->db->join('cliente', 'cliente.client_plan_id = plano_servico.plan_id');

        $this->CI->db->where('cliente.client_id', $this->CI->session->userdata('CLID'));
// if ($this->CI->router->fetch_class() == 'admin') {
// $this->CI->db->get()->result();
// echo $this->CI->db->last_query(); exit;
// }
        return $this->CI->db->get()->result();

    }


    protected function createDirectories()
    {
        $segmento_id = $this->getSegmento();
        $this->getSegmentDirectories($segmento_id->empresa_segmento_id);
    }

    protected function getSegmento()
    {
        $this->CI->db->select('empresa_segmento_id');
        $this->CI->db->from('empresa');
        $this->CI->db->where('empresa_id', $this->CI->session->userdata('EID'));
        return $this->CI->db->get()->row();
    }

    protected function getSegmentDirectories($segmento_id, $parent = null)
    {
        $query = $this->CI->db->get_where('segmento_diretorio', array('dir_parent_id' => $parent, 'dir_segmento_id'=> $segmento_id));
        $html = "";

        $old = null;

        $id = 'NULL';

        foreach ($query->result() as $row)
        {

            $parent_path = $old != null ? $old .'/'.$row->dir_name : 'Home/' . $row->dir_name;

            $html .= "INSERT INTO file_object (hash, original_name, full_path, type, mime, size, owner, storage_key, parent) VALUES (";
            $html .= "'".random_string('alnum', 32)."', '$row->dir_name', '$parent_path', 2, 687, 0, '".$this->CI->session->userdata('USER_KEY')."', '".$this->CI->session->userdata('STR_DRIVER')."', $id" ;
            $html .= ");";
            $parent = $row->dir_id;
            $old = $parent_path;
            $this->CI->db->query($html);
            $id = $this->CI->db->insert_id();
            $html = $this->getSegmentDirectories($segmento_id, $parent);
        }
        return $html;
    }


    protected function get_contract_data($id)
    {

        $this->CI->db->select('contato.contato_valor, endereco.endereco_logradouro, endereco.endereco_num, endereco_bairro, endereco.endereco_cep, endereco.endereco_estado, endereco.endereco_cidade, endereco.endereco_complemento, endereco.endereco_alias, empresa.empresa_razao as empresa_razao, empresa.empresa_fantasia, empresa.empresa_cnpj, empresa.empresa_ie');
        $this->CI->db->from('cliente');
        $this->CI->db->join('empresa', 'empresa.empresa_id = cliente.client_empresa_id');
        $this->CI->db->join('empresa_endereco', 'empresa_endereco.empresa_id = empresa.empresa_id');
        $this->CI->db->join('endereco', 'endereco.endereco_id = empresa_endereco.endereco_id');
        $this->CI->db->join('empresa_contato', 'empresa_contato.empresa_id = empresa.empresa_id');
        $this->CI->db->join('contato', 'contato.contato_id = empresa_contato.contato_id');
        $this->CI->db->where('client_id', $id);
        $this->CI->db->where('contato.tipo_id', 1);

        return $this->CI->db->get()->row();
    }

    protected function verify_quota()
    {
        if($this->CI->session->userdata("QDS") && count($this->get_quota()) == 0){

            $this->CI->db->set('quota_size', 1000000);
            $this->CI->db->set('user_id', $this->CI->session->userdata('USER_ID'));

            $this->CI->db->insert('usuario_quota');
        }

    }

    protected function get_quota()
    {
        $this->CI->db->from('usuario_quota');
        $this->CI->db->where('usuario_quota.user_id', $this->CI->session->userdata('USER_ID'));
        return $this->CI->db->get()->result();
    }

}