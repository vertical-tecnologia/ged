<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Hooks
 * Date: 14/08/15
 * Time: 12:21
 */
class user_customization
{
    private $CI;
    public $vet_dados;

    public function __construct(){
        $this->CI = &get_instance();
    }

    public function user_customization()
    {
        $this->CI->load->library('authenticator');
        if($this->CI->authenticator->is_logged_in() !== true) {
            return;
        }

        $custom = $this->get_user_customization();

        if($custom == false)
            $this->set_default_configuration();

        else {
            $custom = $this->get_user_customization();
            $this->filemanager_view($custom);
        }

        $this->valid_server_user();
    }

    public function get_user_customization()
    {
        $this->CI->db->from('configuration');
        $this->CI->db->where('user_id', $this->CI->session->userdata('USER_ID'));

        $row = $this->CI->db->get()->row();

        if(count($row) == 0)
            return false;
        else
            return $row;

    }

    public function set_default_configuration()
    {
        $id= (int)$this->CI->session->userdata('USER_ID');

        if($id == 0){
            return;
        }


        $this->CI->db->set('user_id', $id);
        $this->CI->db->insert('configuration');
        if($this->CI->db->insert_id() > 0)
            return true;
        else
            return false;
    }


    public function filemanager_view($custom)
    {
        $filemanager_class = $custom->filemanager_view == 1 ?  " list " : "";
        $filemanager_ico = $custom->filemanager_view == 1 ?  " fa-th-large " : " fa-list ";

        $this->CI->session->set_userdata('theme', $custom->theme);
        $this->CI->session->set_userdata('filemanager_view', $filemanager_class);
        $this->CI->session->set_userdata('filemanager_ico', $filemanager_ico);
    }


    public function valid_server_user()
    {
        $server = $this->CI->input->get('server');

        if (!$server)
            return true;

        $where = array(
            'server_id'  => $server,
            'empresa_id' => $this->CI->session->userdata('EID'));

        $this->CI->load->model('empresa_model');
        $result = $this->CI->empresa_model->fetchMyServer($where);

        if (empty($result) && $this->CI->session->userdata('SRID') != SERVER_LOCAL)
            redirect('arquivos');
        return true;
    }
}