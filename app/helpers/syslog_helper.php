<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		App\Helpers
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */


if (!function_exists('__update_log_history')) {

    function __update_log_history($operation)
    {
        $CI =& get_instance();
        //  $this->agent->version()
        $data = array(
            "user_name" => $CI->session->userdata("USER_NAME"),
            "operation" => $operation,
            "ip_address" => $_SERVER['SERVER_ADDR'],
            "browser" => $CI->agent->browser(),
            "client_id" => $CI->session->userdata('CLID')
        );

        $CI->db->insert("log_history", $data);
    }
}