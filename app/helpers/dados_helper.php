<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		App\Helpers
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Formats a numbers as bytes, based on size, and adds the appropriate suffix
 *
 * @access	public
 * @param	mixed	// will be cast as int
 * @return	string
 */
if ( ! function_exists('type_format')) {

    function type_format($valor, $tipo) {

        if (trim($valor) === '' || $valor === null || empty($valor)) {

            return $valor;

        }

        if ($tipo == 'data_br') {

            return data_formatada($valor, 5);//retorna DD/MM/YYYY

        } else if ($tipo == 'data') {

            return data_formatada($valor, 6);//retorna YYYY-MM-DD

        } else if ($tipo == 'numero') {

            return preg_replace('/\D/', '', $valor);

        } else if ($tipo == 'brl') {

            return number_format($valor, 2, ',','.');

        } else if ($tipo == 'volume') {

            return number_format($valor, 3, ',','.');

        } else if (in_array($tipo, array('real', 'peso', 'money'))) {

            return str_replace(',', '.', str_replace('.', '', $valor));

        } else if ($tipo == 'cpf') {

            $aux = preg_replace('/\D+/', '', $valor);
            $aux = str_pad($aux, 11, '0', STR_PAD_LEFT);

            return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '${1}.${2}.${3}-${4}', $aux);

        } else if ($tipo == 'cnpj') {

            $aux = preg_replace('/\D+/', '', $valor);
            $aux = str_pad($aux, 14, '0', STR_PAD_LEFT);

            return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/', '${1}.${2}.${3}/${4}-${5}', $aux);

        } else if ($tipo == 'cpf_cnpj') {

            if (valida_tipos($valor, 'cpf')) {

                $aux = preg_replace('/\D+/', '', $valor);
                $aux = str_pad($aux, 11, '0', STR_PAD_LEFT);

                return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '${1}.${2}.${3}-${4}', $aux);

            } else if (valida_tipos($valor, 'cnpj')) {

                $aux = preg_replace('/\D+/', '', $valor);
                $aux = str_pad($aux, 14, '0', STR_PAD_LEFT);

                return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/', '${1}.${2}.${3}/${4}-${5}', $aux);

            }

        } else if ($tipo == 'cep') {

            $aux = preg_replace('/\D+/', '', $valor);
            $aux = str_pad($aux, 8, '0', STR_PAD_LEFT);

            return preg_replace('/(\d{5})(\d{3})/', '${1}-${2}', $aux);

        } else if ($tipo == 'letra') {

            return $valor;

        } else if ($tipo == 'sim_nao') {

            return $valor == 1 ? 'Sim' : 'Não';

        } else if ($tipo == 'true_false') {

            return $valor == 1 ? true : false;

        } else {

            return $valor;

        }

    }

}

if (!function_exists('formata_string')) {

    function formata_texto($var_str) {

        $str = str_replace('Ã', 'a', $var_str);
        $str = str_replace('ã', 'a', $str);
        $str = str_replace('Á', 'a', $str);
        $str = str_replace('á', 'a', $str);
        $str = str_replace('Â', 'a', $str);
        $str = str_replace('â', 'a', $str);
        $str = str_replace('À', 'a', $str);
        $str = str_replace('à', 'a', $str);
        $str = str_replace('Ä', 'a', $str);
        $str = str_replace('ä', 'a', $str);
        $str = str_replace('É', 'e', $str);
        $str = str_replace('é', 'e', $str);
        $str = str_replace('Ê', 'e', $str);
        $str = str_replace('ê', 'e', $str);
        $str = str_replace('Í', 'i', $str);
        $str = str_replace('í', 'i', $str);
        $str = str_replace('Õ', 'o', $str);
        $str = str_replace('õ', 'o', $str);
        $str = str_replace('Ó', 'o', $str);
        $str = str_replace('ó', 'o', $str);
        $str = str_replace('Ô', 'o', $str);
        $str = str_replace('ô', 'o', $str);
        $str = str_replace('Ö', 'o', $str);
        $str = str_replace('ö', 'o', $str);
        $str = str_replace('Ú', 'u', $str);
        $str = str_replace('ú', 'u', $str);
        $str = str_replace('Ç', 'c', $str);
        $str = str_replace('ç', 'c', $str);

        return $str;

    }

}

if (!function_exists('get_estados')) {

    function get_estados() {

        $vet = array();

        $vet['AC'] = 'Acre';
        $vet['AL'] = 'Alagoas';
        $vet['AM'] = 'Amazonas';
        $vet['AP'] = 'Amapá';
        $vet['BA'] = 'Bahia';
        $vet['CE'] = 'Ceará';
        $vet['DF'] = 'Distrito Federal';
        $vet['ES'] = 'Espírito Santo';
        $vet['GO'] = 'Goiás';
        $vet['MA'] = 'Maranhão';
        $vet['MG'] = 'Minas';
        $vet['MS'] = 'Mato Grosso do Sul';
        $vet['MT'] = 'Mato Grosso';
        $vet['PA'] = 'Pará';
        $vet['PB'] = 'Paraíba';
        $vet['PE'] = 'Pernambuco';
        $vet['PI'] = 'Piauí';
        $vet['PR'] = 'Paraná';
        $vet['RJ'] = 'Rio de Janeiro';
        $vet['RN'] = 'Rio Grande do Norte';
        $vet['RO'] = 'Rondônia';
        $vet['RR'] = 'Roraima';
        $vet['RS'] = 'Rio Grande do Sul';
        $vet['SC'] = 'Santa Catarina';
        $vet['SE'] = 'Sergipe';
        $vet['SP'] = 'São Paulo';
        $vet['TO'] = 'Tocantins';

        return $vet;

    }

}

if (!function_exists('get_meses')) {

    function get_meses($id=null) {

        $vet = array();

        $vet['01'] = 'Janeiro';
        $vet['02'] = 'Fevereiro';
        $vet['03'] = 'Março';
        $vet['04'] = 'Abril';
        $vet['05'] = 'Maio';
        $vet['06'] = 'Junho';
        $vet['07'] = 'Julho';
        $vet['08'] = 'Agosto';
        $vet['09'] = 'Setembro';
        $vet['10'] = 'Outubro';
        $vet['11'] = 'Novembro';
        $vet['12'] = 'Dezembro';

        if ($id === null) {
            return $vet;
        } else if (isset($vet[$id])) {
            return $vet[$id];
        } else {
            return null;
        }

    }

}

if (!function_exists('valida_data')) {

    function valida_data($data) {

        if (trim($data) === '' || $data === null || empty($data)) {

            return $data;

        }

        //$data = '2013-01-20';
        //$data = '10/12/2013';

        if ($aux = strpos($data, '/') && (in_array(strlen($data), array(10, 16, 19)))) {

            if ($aux == 2) {#20/10/2013
                $vet = array_reverse(explode('/', $data));
            } else if ($aux == 4) {#2013/10/20
                $vet = explode('/', $data);
            }

        } else if ($aux = strpos($data, '-') && (in_array(strlen($data), array(10, 16, 19)))) {

            if ($aux == 2) {#20-10-2013
                $vet = array_reverse(explode('-', $data));
            } else if ($aux == 4) {#2013-10-20
                $vet = explode('-', $data);
            }

        } else {

            return false;

        }

        list($y, $m, $d) = $vet;

        //$d = substr($d, 0, 2);
        $y = substr($y, 0, 4);

        /*Debug::Test($aux);
        Debug::Test($data);
        Debug::Test($vet);
        Debug::Test($y);
        Debug::Test($m);
        Debug::Test($d);*/

        return checkdate($m, $d, $y);

    }

}

if (!function_exists('valida_cpf')) {

    function valida_cpf($cpf) {

        if (empty($cpf)) {
            return false;
        }

        $cpf = preg_replace('/\D+/', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if ($cpf == '00000000000') {
            return false;
        } else if ($cpf == '11111111111') {
            return false;
        } else if ($cpf == '22222222222') {
            return false;
        } else if ($cpf == '33333333333') {
            return false;
        } else if ($cpf == '44444444444') {
            return false;
        } else if ($cpf == '55555555555') {
            return false;
        } else if ($cpf == '66666666666') {
            return false;
        } else if ($cpf == '77777777777') {
            return false;
        } else if ($cpf == '88888888888') {
            return false;
        } else if ($cpf == '99999999999') {
            return false;
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {

                    $d += $cpf{$c} * (($t + 1) - $c);

                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {

                    return false;

                }

            }

            return true;

        }

    }

}

if (!function_exists('valida_cnpj')) {

    function valida_cnpj($cnpj) {

        if (empty($cnpj)) {
            return false;
        }

        $cnpj = preg_replace('/\D+/', '', $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);

        if ($cnpj == '00000000000000') {
            return false;
        } else if ($cnpj == '11111111111111') {
            return false;
        } else if ($cnpj == '22222222222222') {
            return false;
        } else if ($cnpj == '33333333333333') {
            return false;
        } else if ($cnpj == '44444444444444') {
            return false;
        } else if ($cnpj == '55555555555555') {
            return false;
        } else if ($cnpj == '66666666666666') {
            return false;
        } else if ($cnpj == '77777777777777') {
            return false;
        } else if ($cnpj == '88888888888888') {
            return false;
        } else if ($cnpj == '99999999999999') {
            return false;
        } else {

            $calcular = 0;
            $calcularDois = 0;

            for ($i = 0, $x = 5; $i <= 11; $i++, $x--) {

                $x = ($x < 2) ? 9 : $x;

                $number    = substr($cnpj, $i, 1);
                $calcular += $number * $x;

            }

            for ($i = 0, $x = 6; $i <= 12; $i++, $x--) {

                $x = ($x < 2) ? 9 : $x;

                $numberDois    = substr($cnpj, $i, 1);
                $calcularDois += $numberDois * $x;

            }

            $digitoUm   = (($calcular % 11) < 2)     ? 0 : 11 - ($calcular % 11);
            $digitoDois = (($calcularDois % 11) < 2) ? 0 : 11 - ($calcularDois % 11);

            if ($digitoUm != substr($cnpj, 12, 1) || $digitoDois != substr($cnpj, 13, 1)) {
                return false;
            }

            return true;

        }

    }

}

if (!function_exists('formata_tipos')) {

    function formata_tipos($val, $tipo) {

        switch ($tipo) {

            case 'numero':
                return preg_replace('/\D+/', '', $val);
            case 'decimal':
                $aux = preg_replace('/[a-zA-Z]+/', '', $val);
                $aux = (float) str_replace(',', '.', str_replace('.', '', $aux));
                return $aux > 0 ? $aux : '';
            case 'cpf':
                $aux = preg_replace('/\D+/', '', $val);
                $aux = str_pad($aux, 11, '0', STR_PAD_LEFT);
                return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '${1}.${2}.${3}-${4}', $aux);
            case 'cnpj':
                $aux = preg_replace('/\D+/', '', $val);
                $aux = str_pad($aux, 14, '0', STR_PAD_LEFT);
                return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/', '${1}.${2}.${3}/${4}-${5}', $aux);
                break;
            case 'cep':
                $aux = preg_replace('/\D+/', '', $val);
                $aux = str_pad($aux, 8, '0', STR_PAD_LEFT);
                return preg_replace('/(\d{5})(\d{3})/', '${1}-${2}', $aux);

        }

        return $val;

    }

}

if (!function_exists('valida_tipos')) {

    function valida_tipos($val, $tipo, $arr=array()) {

        switch ($tipo) {

            case 'numero':
                return ctype_digit($val);
            case 'decimal':
                return is_float($val);
            case 'texto':
                return ctype_print($val);
            case 'cep':
                return preg_replace('/\D+/', '', $val) > 0;
            case 'cpf':
                return valida_cpf(preg_replace('/\D+/', '', $val));
            case 'cnpj':
                return valida_cnpj(preg_replace('/\D+/', '', $val));
            case 'uf':
                return in_array($val, array_keys(get_estados()));
            case 'data':
                return valida_data($val);
            case 'enum':
                return in_array($val, $arr);

        }

        return true;

    }

}

function access($attr, $path, $data, $volume) {

    return strpos(basename($path), '.') === 0    // if file/folder begins with '.' (dot)
        ? !($attr == 'read' || $attr == 'write') // set read+write to false, other (locked+hidden) set to true
        :  null;                                 // else elFinder decide it itself

}

function By2M($size){
    $size = ($size * 1024) * 1024;
    $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
}

function By2Mb($size){
    $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . @ $filesizename[$i] : '0 Bytes';
}


function toBytes($size){

    $CI =& get_instance();
    $CI->lang->load('number');

    if (strpos($size, 'TB'))
    {
        return round($size * 1099511627776, 1);
    }
    elseif (strpos($size, 'GB'))
    {
        return round($size * 1073741824, 1);

    }
    elseif (strpos($size, 'MB'))
    {
        return round($size * 1048576, 1);
    }
}


if( ! function_exists('clean_string')){
    function clean_string($string, $hyphenize = false) {
        if($hyphenize == true)
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}

if( !function_exists('generate_username')) {
    function generate_username($input){
        $login = explode('-', strtolower(clean_string($input, true)));

        $username =  isset($login[0]) ? $login[0] : '';
        $username .= isset($login[1]) ? $login[1] : '';

        return $username;
    }
}


if( ! function_exists('getRealIP')) {

    function getRealIP() {
        $headers = array ('HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'HTTP_VIA', 'HTTP_X_COMING_FROM', 'HTTP_COMING_FROM', 'HTTP_CLIENT_IP' );

        foreach ( $headers as $header ) {
            if (isset ( $_SERVER [$header]  )) {

                if (($pos = strpos ( $_SERVER [$header], ',' )) != false) {
                    $ip = substr ( $_SERVER [$header], 0, $pos );
                } else {
                    $ip = $_SERVER [$header];
                }
                $ipnum = ip2long ( $ip );
                if ($ipnum !== - 1 && $ipnum !== false && (long2ip ( $ipnum ) === $ip)) {
                    if (($ipnum - 184549375) && // Not in 10.0.0.0/8
                        ($ipnum  - 1407188993) && // Not in 172.16.0.0/12
                        ($ipnum  - 1062666241)) // Not in 192.168.0.0/16
                        if (($pos = strpos ( $_SERVER [$header], ',' )) != false) {
                            $ip = substr ( $_SERVER [$header], 0, $pos );
                        } else {
                            $ip = $_SERVER [$header];
                        }
                    return $ip;
                }
            }

        }
        return $_SERVER ['REMOTE_ADDR'];
    }
}



if( ! function_exists('formatter')){

    function formatter($val, $mask){
            $maskared = '';
            $k = 0;
            for($i = 0; $i<=strlen($mask)-1; $i++)
            {
                if($mask[$i] == '#')
                {
                    if(isset($val[$k]))
                        $maskared .= $val[$k++];
                }
                else
                {
                    if(isset($mask[$i]))
                        $maskared .= $mask[$i];
                }
            }
            return $maskared;
    }
}
/* End of file dados_helper.php */
/* Location: ./system/helpers/dados_helper.php */
