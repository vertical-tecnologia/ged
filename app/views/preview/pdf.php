<style>
    html, *{
        overflow: hidden;
        margin: 0;
        padding: 0;
    }
    div.preview{
        overflow: hidden;
        margin: 0 auto;
        width: 100%;
        height: 99%!important;
    }

    .preview-pdf{
        overflow: hidden;
        max-width: 1800px;
        max-height: 1500px;
        width:100%;
        height:100%;
        display: block;
    }
</style>
<div class="preview">
    {object}
    <embed src="{url}arquivos/open/{hash}/true" class="preview-pdf" type='application/pdf'>
    {/object}
</div>