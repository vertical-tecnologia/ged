<style>
    html, *{
        overflow: hidden;
        margin: 0;
        padding: 0;
    }
    div.preview{
        overflow: hidden;
        margin: 0;
        padding: 1em 0 0 0;
        width: 100%;
        height: 100%!important;
        background-color: black;
    }

    .preview-image{
        overflow: hidden;
        max-width: 800px;
        max-height: 500px;
        margin: 0 auto;
        text-align: center;
        display: block;
    }
</style>
<div class="preview">
    {object}
    <img src="{url}thumb/large/1250/600/{hash}" alt="Preview do arquivo" class="preview-image">
    {/object}
</div>