{object}
<html>
<head>
    <link rel="stylesheet" href="{skin}/lib/jplayer/dist/skin/blue.monday/css/jplayer.blue.monday.min.css">
    <script type="text/javascript" src="{skin}/js/jquery.min.js"></script>
    <script type="text/javascript" src="{skin}/lib/jplayer/dist/jplayer/jquery.jplayer.min.js"></script>

    <style>
        html, *{
            overflow: hidden;
            margin: 0;
            padding: 0;
        }
        div.preview{
            overflow: hidden;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%!important;
            background-color: black;
        }

        .jp-audio {
            height: auto;
            margin: 10% auto;
            text-align: center;
            display: block;
            width: auto;
            max-width: 800px;
        }
        .jp-controls-holder{
            width: 90%;
        }
        .jp-audio .jp-type-single .jp-progress {
            left: 110px;
            width: 90%;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#jquery_jplayer_1").jPlayer({
                ready: function () {
                    $(this).jPlayer("setMedia", {
                        title: "{original_name}",
                        mp3: "{url}arquivos/get/{download_key}",
                        autoPlay : true,
                    });
                },
                cssSelectorAncestor: "#jp_container_1",
                swfPath: "/js",
                supplied: "mp3",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                remainingDuration: true,
                toggleDuration: true
            });
        });
    </script>
</head>
<body>
<div class="preview">

    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
    <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
        <div class="jp-type-single">
            <div class="jp-gui jp-interface">
                <div class="jp-volume-controls">
                    <button class="jp-mute" role="button" tabindex="0">mute</button>
                    <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                    <div class="jp-volume-bar">
                        <div class="jp-volume-bar-value"></div>
                    </div>
                </div>
                <div class="jp-controls-holder">
                    <div class="jp-controls">
                        <button class="jp-play" role="button" tabindex="0">play</button>
                        <button class="jp-stop" role="button" tabindex="0">stop</button>
                    </div>
                    <div class="jp-progress">
                        <div class="jp-seek-bar">
                            <div class="jp-play-bar"></div>
                        </div>
                    </div>
                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                    <div class="jp-toggles">
                        <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                    </div>
                </div>
            </div>
            <div class="jp-details">
                <div class="jp-title" aria-label="title">&nbsp;</div>
            </div>
            <div class="jp-no-solution">
                <span>Update Required</span>
                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
            </div>
        </div>
    </div>
</div>
</body>
</html>
{/object}