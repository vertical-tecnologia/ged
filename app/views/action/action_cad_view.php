<div id="jCrumbs" class="breadCrumb action">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/acao"> <i class="fa fa-user"></i> Ações</a>
        </li>
        <li>
            <a href="{url}/acao/cadastro"> <i class="fa fa-user"></i> Cadastro </a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Nova Ação</h3>
    </div>
</div>

<div class="row table-content">


    <div class="col-sm-12 col-md-12">
        <?=form_open('acao/save');?>
            {action}
            <input type="hidden" name="action_id" id="action_id" value="{action_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="action_module_id">Módulo</label><br/>
                        <select name="action_module_id" id="action_module_id" class="form-control chosen-select" data-placeholder="Selecione um módulo" autofocus="autofocus">
                            <option value=""></option>
                            {modules}
                            <option value="{module_id}">{module_name}</option>
                            {/modules}
                        </select>
                        <?=form_error('action_module_id')?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="action_name">Ação</label><br/>
                        <input type="text" name="action_name" id="action_name" value="{action_name}" class="form-control" />
                        <?=form_error('action_name')?>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}acao" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/action}
        </form>
    </div>
</div>
