<div class="login_box">


    <form action="{url}auth/login/"  onsubmit="return validaLogin()" method="post" id="login_form">
        <div class="top_b">LOGIN</div>
        <div class="cnt_b">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-user"></i></span>
                    <input class="form-control input_login input-sm" type="text" id="username" name="login_usuario" placeholder="Usuário" value="" />
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-lock"></i></span>
                    <input class="form-control input-sm input_login" type="password" id="senha_usuario" name="senha_usuario" placeholder="Senha" value="" />
                </div>
            </div>
        </div>
        <div class="btm_b clearfix" style="text-align: center;">
            <button class="btn btn-default btn-sm" type="submit">Entrar</button>
        </div>
        <div class="links_b links_btm clearfix">
        <span class="linkform">
            <a href="{url}cadastro">Cadastre-se</a> |
            <a class="modal_link" href="#pass_form">Esqueceu sua senha?</a>
        </span>
        </div>
    </form>

    <form id="pass_form" class="form_container" style="display:none">
        <div class="top_b">Recupere sua senha.</div>
        <div class="alert alert-info alert-login">
            Por favor, ensira seu endereço de e-mail. Você irá receber um e-mail contendo as instruções para criar uma nova senha.
        </div>
        <div class="alert alert-success alert-dismissable alert-login" style="display:none"><strong></strong></div>
        <div class="cnt_b" id="forgot_password_container">
            <div class="formRow clearfix">
                <div class="input-group">
                    <span class="input-group-addon input-sm">@</span>
                    <input type="e-mail" placeholder="E-mail" id="forgot_password_user_email" class="form-control input-sm" />
                </div>
            </div>
        </div>
        <div class="btm_b tac">
            <button class="btn btn-default" id="forgot_password_button" type="submit">Enviar</button>
        </div>
        <div class="links_b links_btm clearfix">
            <span class="linkform" style="display:none"><a href="#login_form">Voltar pra tela de login</a></span>
        </div>
</div>
</form>