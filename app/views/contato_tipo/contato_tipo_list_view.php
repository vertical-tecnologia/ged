<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/usuarios"> <i class="fa fa-user"></i> Tipos de Contato</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Tipos de Contato</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}contato_tipo/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Tipo de Contato</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Descrição</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {contato_tipo}
            <tr>
                <td>{tipo_id}</td>
                <td>{tipo_desc}</td>
                <td class="text-right">
                    <a class="ext_disabled" href="{url}contato_tipo/editar/{tipo_id}">editar</a>
                    <a class="ext_disabled" href="{url}contato_tipo/excluir/{tipo_id}">excluir</a>
                </td>
            </tr>
            {/contato_tipo}
            </tbody>
        </table>
    </div>
</div>
