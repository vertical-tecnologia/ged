<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}contato_tipo/"> <i class="fa fa-users"></i> Tipo de Contato</a>
        </li>
        <li>
            <a href="{url}contato_tipo/cadastro"> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Tipo de Usuário</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}contato_tipo/save" method="POST" enctype="application/x-www-form-urlencoded">
            {contato_tipo}
            <input type="hidden" name="tipo_id" id="tipo_id" value="{tipo_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="tipo_desc">Nome:</label><br/>
                        <input type="text" name="tipo_desc" id="tipo_desc" value="{tipo_desc}" class="form-control" autofocus/>
                        <?=form_error('tipo_desc')?>
                    </div>
                </div>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}contato_tipo" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/contato_tipo}
        </form>
    </div>
</div>
