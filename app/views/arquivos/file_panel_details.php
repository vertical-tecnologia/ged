{object}
<div class="dialog dialog-close">
    <div class="dialog-overlay"></div>
    <div class="dialog-main dialog-md dialog-close" tabindex="-1">
        <div class="dialog-header">
            <p class="dialog-title" id="dialog-title">{original_name}</p>
        </div>
        <button class="dialog-button dialog-button-close"
                onclick="destroyPanel()"
                title="Fechar">
            <i class="fa fa-times"></i>
        </button>
        <div class="dialog-inner">
            <div class="dialog-content">

                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <img src="{ico_file_type}" alt="Icone do tipo de arquivo" class="file-ico"/>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td>Criado em:</td>
                                    <td> {created}</td>
                                </tr>
                                <tr>
                                    <td>Caminho:</td>
                                    <td> {full_path}</td>
                                </tr>
                                <tr>
                                    <td>Extensao:</td>
                                    <td> {extension}</td>
                                </tr>
                                <tr>
                                    <td>Tipo:</td>
                                    <td> {desc_file_type}</td>
                                </tr>
                                <tr>
                                    <td>Tamanho:</td>
                                    <td> {size}</td>
                                </tr>
                                <tr>
                                    <td>Proprietario</td>
                                    <td>{uname}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="dialog-actions">
                <div class="dialog-actionsRight">
                    <button class="dialog-action-button dialog-action-button-primary"
                            onclick="destroyPanel()"
                        >
                        <span id="bt-label">Fechar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{/object}