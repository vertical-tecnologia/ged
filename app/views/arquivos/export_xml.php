<div class="fm" id="fm">
    <div class="fmbc">
        <div class="bcit pull-left none-content" data-action="uploadPanel">
            <a href="<?php echo base_url(); ?>">
                <i class="fa fa-home "></i> Home
            </a>
        </div>
        <div class="bcit pull-right none-content" data-action="uploadPanel">
            <a href="{url}arquivos/view/{hash}<?php echo isset($server_id) ? "/?server={$server_id}": null;?>">
                <i class="fa fa-chevron-left"></i> Voltar
            </a>
            <a href="{url}arquivos/edit/{hash}">
                <i class="fa fa-pencil"></i> Alterar Dados do Arquivo
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading"><?php echo $original_name; ?></h3>
            <h3 class="heading">Modelos de XML para Exportação</h3>
            <div class="col-sm-6 col-md-6 col-xs-6">
                <div class="form-group">
                    <h2><span class="label label-default">Modelo 1</span></h2>
                    <textarea class="form-control" rows="10" id="example1" disabled>
                        <?php
                        echo file_get_contents(base_url() . "/skin/public/xml/example1.xml");
                        ?>
                    </textarea>
                </div>
                <div class="col-sm-offset-4 col-sm-10">
                    <a href="{url}arquivos/downloadxml/<?php echo $hash ?>/example1" class="btn btn-primary">Download</a>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-xs-6">
                <div class="form-group">
                    <h2><span class="label label-default">Modelo 2</span></h2>
                    <textarea class="form-control" rows="10" id="example1" disabled>
                        <?php
                        echo file_get_contents(base_url() . "/skin/public/xml/example2.xml");
                        ?>
                    </textarea>
                </div>
                <div class="col-sm-offset-4 col-sm-10">
                    <a href="{url}arquivos/downloadxml/<?php echo $hash ?>/example2" class="btn btn-primary">Download</a>
                </div>
            </div>
        </div>
    </div>
</div>
