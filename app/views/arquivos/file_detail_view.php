<div class="fm" id="fm">
    {arquivo}
    <div class="fmbc">
        <div class="bcit">
            <a href="{url}arquivos<?php echo isset($server_id) ? "/?server={$server_id}": null;?>">
                <i class="fa fa-home"></i> Home
            </a>
        </div>
        {breads}
        <?php
         if ($ownDirectory == true || $owner == '1' || ($directoryPermissions[0] != null && $directoryPermissions[0]->writable == '1')) :?>
            <div class="bcit pull-right none-content" data-action="uploadPanel">
                <a href="{url}arquivos/edit/{hash}">
                    <i class="fa fa-pencil"></i> Alterar Dados do Arquivo
                </a>
            </div>
        <?php endif; ?>
        <div class="bcit pull-right none-content" data-action="uploadPanel">
            <a href="{url}arquivos/{current_directory_url}<?php echo isset($server_id) ? "/?server={$server_id}": null;?>">
                <i class="fa fa-chevron-left"></i> Voltar
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{original_name}</h3>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="vcard">
                    <img class="file-info thumbnail" src="{ico_file_type}" alt="">

                    <ul>
                        {media_preview}
                        <li class="v-heading">
                            Detalhes do arquivo
                        </li>
                        <li>
                            <span class="item-key">Tipo:</span>
                            <div class="vcard-item">{desc_file_type}</div>
                        </li>
                        <li>
                            <span class="item-key">Caminho:</span>
                            <div class="vcard-item">{full_path}</div>
                        </li>
                        <li>
                            <span class="item-key">Criado em: </span>
                            <div class="vcard-item">{created}</div>
                        </li>
                        <li>
                            <span class="item-key">Propriet&aacute;rio:</span>
                            <div class="vcard-item">{uname}</div>
                        </li>

                        <li>
                            <span class="item-key">Descriç&atilde;o: </span>
                            <div class="vcard-item editable" data-href="{url}arquivos/change/{hash}" data-tooltip="Clique para alterar" data-indicator="{skin}/img/preloader.gif">
                                {description}
                            </div>
                        </li>
                        <li class="v-heading">
                            Dados Personalizados (Metadados)
                        </li>
                        {no_custom_data}
                        <?php foreach ($custom as $key => $tags) : 
                                foreach ($tags as $value) :?>
                            <li>
                                <span class="item-key"><?php echo isset($value) ? $value->label_description : '' ; ?></span>
                                <div class="vcard-item"> <?php echo isset($value) ? $value->label_content : '' ; ?> </div>
                            </li>
                        <?php 
                                endforeach;
                            endforeach;?>
                        <li class="v-heading">
                            Atividades Recentes
                        </li>
                        <li>
                            <ul class="sepH_b item-list list-unstyled">
                                {file_log_list}
                                <li><strong>{username}</strong> <a title="{original_name}" data-placement="right">{log_action_desc}</a> em {datetime}</li>
                                {/file_log_list}

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {/arquivo}
</div>