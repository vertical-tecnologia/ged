<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>

    </ul>
</div>
<div class="fm" id="fm" data-bind-click="unSelectAll" data-free="{free}">

    <div class="fmbc">
        <div class="bcit pull-right none-content">
            <a href="{url}arquivos/set_view/recentFiles?server={server_id}">
                <i class="fa {fm_ico_class}"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="fmgrid" class="fmgrid" data-bind-click="unSelectAll">
        {empty}

        {arquivos}
        <a href="{file_url}" id="{hash}" class="idFolder">
            <div class="el {fm_class}" data-bind-click="elementsSelect" data-id="{hash}" data-type="{type}" data-extension="{ext_file_type}">
                <div class="a"
                     style="background-image: url('{ico_file_type}'); background-size: cover; background-repeat: no-repeat"></div>
                <div class="n">

                    <div class="f-n">
                        {original_name}
                    </div>
                    <div class="dtn">
                        {created}
                    </div>
<!--                     <div class="dtsz">
                        {size}
                    </div> -->
                </div>
            </div>
        </a>
        {/arquivos}
    </div>
</div>
<div class="navbar navbar-default navbar-fixed-bottom hide">
    <div class="container">
        <div id="footer-body">
            <ul class="nav navbar-nav pull-right hide file-action-menu" id="single-file">
                <li class="ver">
                    <a href="{url}document/open/" data-href="{url}arquivos/open/" class="file-action" data-bind-click="preview"><i class="fa fa-eye"></i> Visualizar</a>
                </li>
                <li>
                    <a href="{url}arquivos/view/" class="file-action" data-bind-click="catalog"><i class="fa fa-list-alt"></i> Cat&aacute;logo</a>
                </li>
                <li>
                    <a href="#" class="file-action" data-bind-click="download"><i class="fa fa-download"></i> Baixar</a>
                </li>
                <li>
                    <a href="#" class="file-action" data-bind-click="detail"><i class="fa fa-info-circle"></i> Detalhes</a>
                </li>
                <li>
                    <a href="#" class="file-action" data-bind-click="share"><i class="fa fa-share-alt"></i> Compartilhar</a>
                </li>
                <li>
                    <a href="#" class="file-action " data-bind-click="trash"><i class="fa fa-trash-o"></i> Excluir</a>
                </li>
            </ul>
        </div>
        

        <div id="footer-body">
            <ul class="nav navbar-nav pull-right hide file-action-menu" id="single-folder">
                <li>
                    <a href="{url}arquivos/view/" class="file-action" data-bind-click="catalog"><i class="fa fa-list-alt"></i> Cat&aacute;logo</a>
                </li>
                <li class="detalhes">
                    <a href="#" class="file-action" data-bind-click="detail"><i class="fa fa-info-circle"></i> Detalhes</a>
                </li>
                <!-- <li class="compartilhar">
                    <a href="#" class="file-action" data-bind-click="share"><i class="fa fa-share-alt"></i> Compartilhar</a>
                </li> -->
                <li class="excluir">
                    <a href="#" class="file-action" data-bind-click="trash_folder"><i class="fa fa-trash-o"></i> Excluir Pasta</a>
                </li>
            </ul>
        </div>

    </div>
</div>


<nav id="context-menu" class="context-menu">
    <ul class="context-menu__items">
        <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-bind-click="download"><i class="fa fa-download"></i> Baixar</a>
        </li>
        <li class="context-menu__item ver">
            <a href="{url}document/open/" class="context-menu__link" data-href="{url}arquivos/open/" class="file-action" data-bind-click="preview"><i class="fa fa-eye"></i> Visualizar</a>
        </li>
        <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-bind-click="share"><i class="fa fa-share"></i> Compartilhar</a>
        </li>
        <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-bind-click="trash"><i class="fa fa-times"></i> Excluir</a>
        </li>
        <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-bind-click="detail"><i class="fa fa-info-circle"></i> Detalhes</a>
        </li>
    </ul>
</nav>