<div class="dialog dialog-close">
    <div class="dialog-overlay"></div>
    <div class="dialog-main dialog-md dialog-close" tabindex="-1">
        <div class="dialog-header">
            <p class="dialog-title" id="dialog-title">Esvaziar a lixeira</p>
        </div>
        <button class="dialog-button dialog-button-close"
                onclick="destroyPanel()"
                title="Fechar">
            <i class="fa fa-times"></i>
        </button>
        <div class="dialog-inner">
            <div class="dialog-content">

                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <img src="{trash_img}" alt="Icone do tipo de arquivo" class="file-ico"/>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <div>
                            <?php if($totalFiles > 0): ?>
                            <h3>Deseja esvaziar a lixeira?</h3>
                            <?php endif; ?>
                            <p>{msg}</p>

                            <span id="recover-msg" class="label"></span>

                        </div>

                    </div>
                </div>

            </div>

            <div class="dialog-actions">
                <div class="dialog-actionsRight">
                    <?php if($totalFiles > 0): ?>
                    <button class="dialog-action-button" onclick="destroyPanel()">
                        <span id="bt-label">Cancelar</span>
                    </button>
                    <button class="dialog-action-button dialog-action-button-primary"
                            id="btn-empty"
                            data-href="{url}arquivos/lixeira/empty<?php echo isset($server_id) ? "/?server=$server_id" : null;?>"
                            onclick="cleanTrash(this)">
                        <span id="bt-label">Esvaziar Lixeira</span>
                    </button>
                    <?php else: ?>
                    <button class="dialog-action-button"
                            onclick="destroyPanel()"
                        >
                        <span id="bt-label">Fechar</span>
                    </button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>