<div class="fm" id="fm" data-bind-click="unSelectAll">

    <div class="fmbc">
        <div class="bcit">
            <a href="{url}arquivos<?php echo isset($server_id) ? "/?server={$server_id}" : null;?>">
                <i class="fa fa-home"></i> Home
            </a>
        </div>
        {breads}

        <div class="bcit pull-right none-content">

            <a href="{url}arquivos/lixeira/clear<?php echo isset($server_id) ? "/?server={$server_id}" : null;?>"
               onclick="emptyTrash(this)"
                >
                <i class="fa fa-trash"></i> Esvaziar Lxeira
            </a>
        </div>

        <div class="clearfix"></div>
    </div>
    <div id="fmgrid">
        {empty}

        {arquivos}
        <a href="{file_url}" id="{hash}">
            <div class="el" data-bind-click="elementsSelect" data-id="{hash}" data-type="{type}">
                <div class="a"
                     style="background-image: url('{ico_file_type}'); background-size: cover; background-repeat: no-repeat"></div>
                <div class="n">

                    <div class="f-n">
                        {original_name}
                    </div>
                </div>
            </div>
        </a>
        {/arquivos}
    </div>
</div>
<div class="navbar navbar-default navbar-fixed-bottom hide">
    <div class="container">
        <div id="footer-body">
            <ul class="nav navbar-nav pull-right hide file-action-menu" id="single-file">
                <li>
                    <a href="#" class="file-action" data-bind-click="recover"><i class="fa fa-undo"></i> Restaurar Arquivo</a>
                </li>
            </ul>
        </div>

        <div id="footer-body">
            <ul class="nav navbar-nav pull-right hide file-action-menu" id="single-folder">

                <li>
                    <a href="#" class="file-action" data-bind-click="recover"><i class="fa fa-undo"></i> Restaurar Pasta</a>
                </li>
            </ul>
        </div>

    </div>
</div>