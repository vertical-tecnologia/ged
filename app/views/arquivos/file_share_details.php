{object}
<div class="dialog dialog-close">
    <div class="dialog-overlay"></div>
    <div class="dialog-main dialog-md dialog-close" tabindex="-1">
        <div class="dialog-header">
            <p class="dialog-title" id="dialog-title">Compartilhar "{original_name}"</p>
        </div>
        <button class="dialog-button dialog-button-close"
                onclick="destroyPanel()"
                title="Fechar">
            <i class="fa fa-times"></i>
        </button>
        <div class="dialog-inner">
            <div class="dialog-content">

                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <img src="{ico_file_type}" alt="Icone do tipo de arquivo" class="file-ico"/>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <div>
                            <legend>Compartilhar com:</legend>
                            <div>
                                <form>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab">Usuários</a></li>
                                    <li role="presentation"><a href="#group" aria-controls="group" role="tab" data-toggle="tab">Grupos</a></li>
                                    <li role="presentation"><a href="#email" aria-controls="email" role="tab" data-toggle="tab">Email</a></li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="user">
                                        <select name="sh_user_id[]" id="sh_user_id" class="form-control chosen-select  " data-placeholder="Selecione os usuários para compartilhar." multiple>
                                            {usuario}
                                            <option value="{user_key}" {selected}>{user_fullname}</option>
                                            {/usuario}
                                        </select>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="group">
                                        <select name="sh_group_id[]" id="sh_group_id" class="form-control chosen-select " data-placeholder="Selecione os grupos para compartilhar." multiple>
                                            {group}
                                            <option value="{group_id}" {selected}>{group_name}</option>
                                            {/group}
                                        </select>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="email">
                                        <input type="email" placeholder="Digite o email para compartilhar..." class="form-control" id="sh_email" name="sh_email">
                                    </div>

                                </div>
                                </form>

                            </div>
                            <br/>
                            <span id="share-msg" class="label"></span>

                        </div>

                    </div>
                </div>

            </div>

            <div class="dialog-actions">
                <div class="dialog-actionsRight">
                    <button class="dialog-action-button"
                            onclick="destroyPanel()"
                        >
                        <span id="bt-label">Cancelar</span>
                    </button>
                    <button class="dialog-action-button dialog-action-button-primary"
                            onclick="shareThis(this)"
                            data-id="{hash}"
                            id="_{hash}"
                        >
                        <span id="bt-label">Compartilhar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{/object}