<div class="fm" id="fm">
    {arquivo}
    <div class="fmbc">
        <div class="bcit pull-right none-content" data-action="uploadPanel">
            <a href="{url}arquivos/view/{hash}<?php echo isset($server_id) ? "/?server={$server_id}": null;?>">
                <i class="fa fa-chevron-left"></i> Voltar
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{original_name}</h3>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="vcard">
                    <img class="file-info thumbnail" src="{ico_file_type}" alt="">

                    <ul>
                        <li class="v-heading">
                            Detalhes do arquivo
                        </li>
                        <li>
                            <span class="item-key">Tipo:</span>
                            <div class="vcard-item">{desc_file_type}</div>
                        </li>
                        <li>
                            <span class="item-key">Caminho:</span>
                            <div class="vcard-item">{full_path}</div>
                        </li>
                        <li>
                            <span class="item-key">Criado em: </span>
                            <div class="vcard-item">{created}</div>
                        </li>
                        <li>
                            <span class="item-key">Propriet&aacute;rio:</span>
                            <div class="vcard-item">{uname}</div>
                        </li>

                        <li>
                            <span class="item-key">Descriç&atilde;o: </span>
                            <div class="vcard-item editable" data-href="{url}arquivos/change/{hash}" data-tooltip="Clique para alterar" data-indicator="{skin}/img/preloader.gif">
                                {description}
                            </div>
                        </li>
                        <li class="v-heading">
                            Dados Personalizados
                        </li>
                        <li>
                            <form action="{url}arquivos/save_label/{hash}" method="post">
                                <input type="hidden" name="idTag" id="idTag" value="">
                                <input type="hidden" name="hash" id="hash" value="{hash}">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="label_description" name="label_description" placeholder="TAG">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <textarea class="form-control" id="label_content" name="label_content" placeholder="Descrição"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Salvar</button>
                            </form>
                        </li>
                        {no_custom_data}
                        {custom}
                        <li>
                            <span id="tag-id" class="hidden">{label_id}</span>
                            <span class="item-key">{label_description}</span>
                            <div class="vcard-item ">
                                <div class="pull-right">
                                    <a href="{url}arquivos/remove_tag/{label_id}" class="btn btn-danger pull-right"><i class="fa fa-times"></i>
                                    </a>
                                </div>
                                {label_content}
                                <div class="pull-right col-md-1">
                                    <button class="btn btn-md btn-primary pull-right"><i class="fa fa-pencil-square-o editTag" id="{label_id}"></i>
                                    </button>
                                </div> 
                            </div>
                        </li>
                        {/custom}
                    </ul>
                </div>
                <div class="col-md-12">
                    <a href="{url}arquivos/export/{hash}" class="btn btn-lg btn-primary pull-left">Exportar XML</a>
                </div>
            </div>
        </div>
    </div>
    {/arquivo}
</div>