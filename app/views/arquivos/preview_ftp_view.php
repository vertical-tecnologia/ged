<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}contato_tipo/"> <i class="fa fa-server"></i> FTP</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Acesso FTP <a href="{url}configuracao/ftp" class="pull-right btn">Configurar Servidor</a></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12">
        <div id="elfinder"></div>
    </div>
</div>
