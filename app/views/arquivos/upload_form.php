
<div class="fm" id="fm">

	<div class="fmbc">
		<div class="bcit">
			<i class="fa fa-home"></i> Home
		</div>
		<div class="bcit pull-right none-content" data-action="uploadPanel">
			<a href="{url}arquivos/folder/{current_directory}<?php echo isset($server_id) ? "/?server={$server_id}" : null;?>">
				<i class="fa fa-chevron-left"></i> Voltar
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="fmgrid">

		<div class="formSep">
			<p><span class="label label-default">Enviar arquivos</span></p>
			<form action="#" method="GET">

				<!-- <?php //if (isset($servers) && $servers) :?> -->
<!-- 				<div class="sepH_b">
					<label for="server_id">Escolha o servidor:</label>
					<select id="server_id" name="server_id">
					<?php //foreach ($servers as $server) :?>
						<option value="<?php// echo $server->server_id;?>"><?php //echo $server->server_address;?></option>
					<?php //endforeach;?>
					</select>
				</div> -->
				<!-- <?php //endif;?> -->
				<input type="hidden" id="max" value="{free}"/>
				<input type="hidden" id="curdir" name="curdir" value="{current_directory}"/>
				<div id="upload_file" class="upload" data-upload-options='{"action":"{url}arquivos/send<?php echo isset($server_id) ? "?server={$server_id}" : null;?>", "label" : "Arraste arquivos aqui para enviar ou clique para selecionar.", "leave":"Uploads em andamento. Se voce sair os uploads serao cancelados."}'"></div>
				<input type="hidden" id="folder" name="folder" value="{folder}"/>
				<div class="filelists">
					<h5>Enviados</h5>
					<ol class="filelist complete">
					</ol>
					<h5>Na fila</h5>
					<ol class="filelist queue">
					</ol>
				</div>
			</form>
		</div>
	</div>
</div>
