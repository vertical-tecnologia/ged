<div class="fm" id="fm">

    <div class="fmbc">
        <div class="bcit">
            <i class="fa fa-home"></i> Home
        </div>
        <div class="bcit pull-right none-content" data-action="uploadPanel">
            <a href="{url}arquivos/folder/{current_directory}<?php echo isset($server_id) ? "?server={$server_id}" : null;?>">
                <i class="fa fa-chevron-left"></i> Voltar
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="fmgrid">

        <div class="formSep">
            <h2>Novo Diretório</h2>
            <form action="{url}arquivos/savedir/{current_directory}<?php echo isset($server_id) ? "/?server={$server_id}" : null;?>" method="POST" autocomplete="off">
                <input type="hidden" id="curdir" name="curdir" value="{current_directory}"/>
                <input type="text" class="upload" autofocus="true" placeholder="Nome do Diretório" name="directory" id="directory" required="true"/>
                <button type="submit" class="btn bnt-success pull-right"> CRIAR </button>
            </form>
            <div class="clearfix"></div>
        </div>

    </div>
</div>