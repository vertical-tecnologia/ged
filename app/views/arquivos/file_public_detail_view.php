<div class="fm" id="fm">
    {arquivo}
    <div class="fmbc">
        <div class="bcit">
            <a href="{url}arquivos/">
                <i class="fa fa-home"></i> Home
            </a>
        </div>
        {breads}

        <div class="clearfix"></div>
    </div>
</div>
<div class="row">

    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{original_name}</h3>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="vcard">
                    <img class="file-info thumbnail" src="{ico_file_type}" alt="">

                    <ul>
                        {media_preview}
                        <li class="v-heading">
                            Detalhes do arquivo <a href="{url}download/{shared_hash}" class="pull-right btn btn-xs btn-primary"><i class="fa fa-download"></i> Baixar Arquivo</a>
                        </li>
                        <li>
                            <span class="item-key">Tipo:</span>
                            <div class="vcard-item">{desc_file_type}</div>
                        </li>
                        <li>
                            <span class="item-key">Caminho:</span>
                            <div class="vcard-item">{full_path}</div>
                        </li>
                        <li>
                            <span class="item-key">Criado em: </span>
                            <div class="vcard-item">{created}</div>
                        </li>
                        <li>
                            <span class="item-key">Propriet&aacute;rio:</span>
                            <div class="vcard-item">{uname}</div>
                        </li>

                        <li>
                            <span class="item-key">Descriç&atilde;o: </span>
                            <div class="vcard-item editable" data-href="{url}arquivos/change/{hash}" data-tooltip="Clique para alterar" data-indicator="{skin}/img/preloader.gif">
                                {description}
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {/arquivo}
</div>