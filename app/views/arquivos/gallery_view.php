<div class="fm" id="fm" data-bind-click="unSelectAll" data-free="{free}">

    <div class="fmbc">
        <div class="bcit">
            <a href="{url}arquivos<?php echo (int) ($server_id) ? "/?server={$server_id}" : null;?>">
                <i class="fa fa-home"></i> Home
            </a>
        </div>

        <div class="clearfix"></div>
    </div>
    <h3 class="heading">Galeria de Fotos</h3>
    <div id="large_grid" class="wmk_grid">
        <ul>
            {arquivos}
            <li class="thumbnail">
                <a href="{url}thumb/show/800/500/{hash}.{extension}" title="{original_name}" >
                    <?php $url_img = (int) $server_id ? "{ico_file_type}" : "{url}thumb/show/250/250/{hash}"; ?>
                    <img src="<?php echo $url_img; ?>" alt="">
                    <!-- <img src="{url}thumb/show/250/250/{hash}" alt=""> -->
                </a>
            </li>
            {/arquivos}
        </ul>
    </div>
</div>
