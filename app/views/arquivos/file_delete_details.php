{object}
<div class="dialog dialog-close">
    <div class="dialog-overlay"></div>
    <div class="dialog-main dialog-md dialog-close" tabindex="-1">
        <div class="dialog-header">
            <p class="dialog-title" id="dialog-title">Excluir "{original_name}"</p>
        </div>
        <button class="dialog-button dialog-button-close"
                onclick="destroyPanel()"
                title="Fechar">
            <i class="fa fa-times"></i>
        </button>
        <div class="dialog-inner">
            <div class="dialog-content">

                    <div class="row" id="div-result">
                    <div class="col-sm-12 ">
                        <h2>Voce tem certeza que deseja excluir esse arquivo?</h2>
                    </div>

                    <div class="col-sm-4 col-md-4 margin-1em">
                        <img src="{ico_file_type}" alt="Icone do tipo de arquivo" class="file-ico"/>
                    </div>
                    <div class="col-sm-8 col-md-8 margin-1em">


                    </div>
                </div>

            </div>

            <div class="dialog-actions">
                <div class="dialog-actionsRight">
                    <button class="dialog-action-button"
                            id="closePanel"
                            onclick="destroyPanel()"
                        >
                        <span id="bt-label">Cancelar</span>
                    </button>
                    <button class="dialog-action-button dialog-action-button-primary"
                            data-id="{hash}"
                            data-target="div-result"
                            onclick="sendToTrash(this)"
                        >
                        <span id="bt-label">Excluir</span>
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>
{/object}