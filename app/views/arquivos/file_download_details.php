{object}
<div class="dialog dialog-close">
    <div class="dialog-overlay"></div>
    <div class="dialog-main dialog-md dialog-close" tabindex="-1">
        <div class="dialog-header">
            <p class="dialog-title" id="dialog-title">Download</p>
        </div>
        <button class="dialog-button dialog-button-close"
                onclick="destroyPanel()"
                title="Fechar">
            <i class="fa fa-times"></i>
        </button>
        <div class="dialog-inner">
            <div class="dialog-content">

                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <img src="{ico_file_type}" alt="Icone do tipo de arquivo" class="file-ico"/>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <h4>Baixar arquivo <strong>{original_name}</strong></h4>
                        <hr/>
                        {message}
                    </div>
                </div>

            </div>

            <div class="dialog-actions">
                <div class="dialog-actionsRight">
                    <button class="dialog-action-button"
                            onclick="destroyPanel()"
                        >
                        <span id="bt-label">Fechar</span>
                    </button>

                    <a class="dialog-action-button dialog-action-button-primary"
                            onclick="destroyPanel()"
                            target="_blank"
                            href="{url}arquivos/get/{key}"
                        >
                        <i class="fa fa-download"></i>
                        <span id="bt-label">Baixar</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
{/object}