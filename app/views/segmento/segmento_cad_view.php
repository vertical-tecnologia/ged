<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}segmento"> <i class="fa fa-list-alt"></i> Segmentos </a>
        </li>
        <li>
            <a href="{url}segmento/cadastro"> <i class="fa fa-list"></i> Cadastro </a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Segmento</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}segmento/save" method="POST" enctype="application/x-www-form-urlencoded">
            {segmento}
            <input type="hidden" name="segmento_id" id="segmento_id" value="{segmento_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="segmento_name">Nome</label><br/>
                        <input type="text" name="segmento_name" id="segmento_name" value="{segmento_name}" class="form-control" autofocus/>
                        <?=form_error('segmento_name');?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="segmento_name">Status</label><br/>
                        <select name="segmento_status_id" id="segmento_status_id" class="form-control chosen-select">
                            <option></option>
                            {segmento_status}
                            <option value="{status_id}" {selected}>{status_name}</option>
                            {/segmento_status}
                        </select>
                        <?=form_error('segmento_status_id');?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}segmento" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success" type="submit" name="action" value="<?=$this->router->fetch_method();?>">Salvar</button>
                </div>
            </div>
            {/segmento}
        </form>
    </div>
</div>
