<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/segmento"> <i class="fa fa-user"></i> Segmentos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Segmentos</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}segmento/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Segmento</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th>Status</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {segmento}
            <tr>
                <td>{segmento_id}</td>
                <td>{segmento_name}</td>
                <td>{status_name}</td>
                <td class="text-right">
                    <a class="btn btn-xs ext_disabled" href="{url}segmento/diretorios/{segmento_id}">diretórios</a>
                    <a class="ext_disabled" href="{url}segmento/editar/{segmento_id}">editar</a>
                    <a class="ext_disabled" href="{url}segmento/excluir/{segmento_id}">excluir</a>
                </td>
            </tr>
            {/segmento}
            </tbody>
        </table>
    </div>
</div>
