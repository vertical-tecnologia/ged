<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}servico"> <i class="fa fa-cogs"></i> Serviços</a>
        </li>
        <li>
            <a href="{url}servico/cadastro"> <i class="fa fa-cog"></i> Cadastro de Serviço</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Serviço</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}servico/save" method="POST" enctype="application/x-www-form-urlencoded">
            {servico}
            <input type="hidden" name="service_id" id="service_id" value="{service_id}"/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="service_name">Serviço *</label>
                                <input type="text" name="service_name" id="service_name" value="{service_name}" class="form-control" {field_disabled}/>
                                <?=form_error('service_name');?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="service_desc">Descrição *</label>
                                <textarea name="service_desc" id="service_desc" rows="10" class="form-control">{service_desc}</textarea>
                                <?=form_error('service_desc');?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="service_price">Preço</label>
                                <input type="text" name="service_price" id="service_price" value="{service_price}" class="form-control"/>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="service_storage">Quantidade de Armazenamento </label>
                                <select name="service_storage" id="service_storage" class="form-control chosen-select">
                                    {quantidades}
                                    <option value="{quant_valor}" {selected}>{quant_desc}</option>
                                    {/quantidades}
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="service_recursive">Serviço Mensal?</label>
                                <input type="checkbox" name="service_recursive" {service_recursive_checked} id="service_recursive" value="1" class="form-control bs-switch"data-on-text="SIM" data-off-text="N&Atilde;O"  />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="status_id">Status do Serviço *</label>
                                <select name="status_id" id="status_id" class="form-control chosen-select" data-placeholder="Selecione o status do serviço">
                                    <option></option>
                                    {service_status}
                                        <option value="{service_status_id}" {selected} {field_disabled}>{service_status_name}</option>
                                    {/service_status}
                                </select>
                                <?=form_error('status_id');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}servico" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/servico}
        </form>
    </div>
</div>
