<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/servico"> <i class="fa fa-cogs"></i> Serviços</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Serviço</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}servico/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Servico</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th>Preço</th>
            <th>Status</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {servico}
            <tr>
                <td>{service_id}</td>
                <td>{service_name}</td>
                <td>{service_price}</td>
                <td>{service_status_name}</td>
                <td class="text-right">
                    {link_upd}
                    {link_del}
                </td>
            </tr>
            {/servico}
            </tbody>
        </table>
    </div>
</div>
