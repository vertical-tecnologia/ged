{contato}
<div class="modal-content-form">
    <div class="col-sm-12 col-md-12">
        <form method="POST" id="modal-form" enctype="application/x-www-form-urlencoded">
            <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>
            <input type="hidden" name="user_id" id="user_id" value="{user_id}"/>
            <input type="hidden" name="contato_id" id="contato_id" value="{contato_id}"/>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <label for="tipo_id">Tipo de Contato </label>
                    <select name="tipo_id" id="tipo_id" class="form-control chosen-select" data-placeholder="Selecione o tipo de contato">
                        <option></option>
                        {type}
                        <option value="{tipo_id}">{tipo_desc}</option>
                        {/type}
                    </select>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <label for="contato_valor">Contato</label>
                    <input type="text" name="contato_valor" id="contato_valor" value="{contato_valor}" class="form-control" maxlength="45" placeholder="" autofocus/>
                    <?=form_error('contato_valor');?>
                </div>
            </div>
        </form>
    </div>
</div>
{/contato}