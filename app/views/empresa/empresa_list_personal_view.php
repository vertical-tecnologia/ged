<div id="jCrumbs" class="breadCrumb action">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}empresa"> <i class="fa fa-building"></i> Empresas</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Empresas</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        {button_add}
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Tipo</th>
            <th>Nome do Responsável</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {empresa}
            <tr>
                <td><a href="{url}empresa/visualizar/{empresa_id}">{empresa_id}</a></td>
                <td><a href="{url}empresa/visualizar/{empresa_id}">{tipo}</a></td>
                <td><a href="{url}empresa/visualizar/{empresa_id}">{user_fullname}</a></td>
                <td class="text-right">
                    {link_upd}
                    {link_del}
                </td>
            </tr>
            {/empresa}
            </tbody>
        </table>
    </div>
</div>
