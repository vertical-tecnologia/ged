{empresa}<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}empresa" class="ext_disabled"> <i class="fa fa-building"></i> Empresas</a>
        </li>
        <li>
            <a href="{url}empresa/visualizar/{empresa_id}" class="ext_disabled"> <i class="fa fa-building-o"></i> Visualizar</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title} </h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-5">

        <div class="vcard">
<!--            <img class="thumbnail" src="http://www.placehold.it/80x80/0088CC/fafafa?text=LOGO" alt="">-->
            <input type="hidden" name="parent_field" id="parent_field" value="empresa_id"/>
            <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>
            <ul class="no-thumb">
                <li class="v-heading">
                    {empresa_fantasia}
                </li>
                <li>
                    <span class="item-key">Razão Social</span>
                    <div class="vcard-item">{empresa_razao}</div>
                </li>
                <li>
                    <span class="item-key">CNPJ</span>
                    <div class="vcard-item">{empresa_cnpj}</div>
                </li>
                <li>
                    <span class="item-key">Inscrição Estadual</span>
                    <div class="vcard-item">{empresa_ie}</div>
                </li>
                <li>
                    <span class="item-key">Responsável</span>
                    <div class="vcard-item">{user_fullname}</div>
                </li>
                <li>
                    <span class="item-key">Segmento</span>
                    <div class="vcard-item">{seg_name}</div>
                </li>
            </ul>
        </div>
        {/empresa}
    </div>
    <div class="col-sm-12 col-md-7">
        <h3 class="heading">Detalhes</h3>
        <div class="w-box" id="w_sort04">
            <div class="w-box-header">
                Endereços

                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-default btn-xs"   data-toggle="modal" data-target="#modalEmpresaEndereco">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="w-box-content">
                <table class="table table-condensed" id="table_endereco">
                    <tbody>
                    {enderecos}
                    <tr>
                        <td>{endereco_alias}</td>
                        <td>{endereco_cidade}</td>
                        <td class="text-right">
                            <a href="{url}endereco/excluir/{endereco_id}/empresa" >excluir</a>
                        </td>
                    </tr>
                    {/enderecos}
                    </tbody>
                </table>
            </div>
        </div>

        <div class="w-box" id="w_sort04">
            <div class="w-box-header">
                Contatos

                <div class="pull-right">
                    <div class="btn-group">
<!--                        <button class="btn btn-default btn-xs" data-href="{url}empresa/contato" data-href-save="{url}contato/save"  data-toggle="modal" data-target="#gedModal" data-arg-href="{url}contato/listar/" data-callback="gedtable|populate|table_contato">-->
                        <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEmpresaContato">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="w-box-content">
                <table class="table table-condensed" id="table_contato">
                   <tbody>
                    {contatos}
                    <tr>
                        <td>{tipo_desc}</td>
                        <td>{contato_valor}</td>
                        <td  class="text-right">
                            <a href="{url}contato/excluir/{contato_id}/empresa">excluir</a>
                        </td>
                    </tr>
                    {/contatos}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEmpresaContato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="{url}contato/save/empresa" id="modal-form" enctype="application/x-www-form-urlencoded">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id="myModalContent">
                <div class="modal-content-form">
                    <div class="col-sm-12 col-md-12">

                            <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <label for="tipo_id">Tipo de Contato </label>
                                    <select name="tipo_id" id="tipo_id" class="form-control chosen-select" data-placeholder="Selecione o tipo de contato">
                                        <option></option>
                                        {type}
                                        <option value="{tipo_id}">{tipo_desc}</option>
                                        {/type}
                                    </select>

                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <label for="contato_valor">Contato</label>
                                    <input type="text" name="contato_valor" id="contato_valor" value="" class="form-control" maxlength="45" placeholder="" autofocus/>
                                    <?=form_error('contato_valor');?>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="mBtn-close" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="submit" id="mBtn-save" class="btn btn-primary">Salvar</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="modalEmpresaEndereco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="{url}endereco/save/empresa" id="modal-form" enctype="application/x-www-form-urlencoded">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" id="myModalContent">
                    <div class="modal-content-form">
                        <div class="col-sm-12 col-md-12">
                            <form method="POST" id="modal-form" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_alias">Apelido </label>
                                        <input type="text" name="endereco_alias" id="endereco_alias" value="" class="form-control" maxlength="45" placeholder="Ex: casa, escritório" autofocus/>
                                        <?=form_error('endereco_alias');?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_cep">CEP</label>
                                        <input type="text" name="endereco_cep" id="endereco_cep" value="" class="form-control cep" maxlength="9" placeholder="CEP: 00000-000" onblur="gedcep.search(this)"/>
                                        <?=form_error('endereco_cep');?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-9">
                                        <label for="endereco_logradouro">Logradouro </label>
                                        <input type="text" name="endereco_logradouro" id="endereco_logradouro" value="" maxlength="155" class="form-control" placeholder="Ex: Av, Rua, Alameda"/>
                                        <?=form_error('endereco_logradouro');?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <label for="endereco_num">Número</label>
                                        <input type="text" name="endereco_num" id="endereco_num" value="" maxlength="10" class="form-control" placeholder="Número"/>
                                        <?=form_error('endereco_num');?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_bairro">Bairro </label>
                                        <input type="text" name="endereco_bairro" id="endereco_bairro" value="" maxlength="155" class="form-control" placeholder="Bairro"/>
                                        <?=form_error('endereco_bairro');?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_cidade">Cidade</label>
                                        <input type="text" name="endereco_cidade" id="endereco_cidade" value="" maxlength="155" class="form-control" placeholder="Cidade"/>
                                        <?=form_error('endereco_cidade');?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div>
                                            <label for="endereco_estado">Estado </label>
                                            <input type="text" name="endereco_estado" id="endereco_estado" value="" maxlength="2" class="form-control" placeholder="Estado"/>
                                            <?=form_error('endereco_estado');?>
                                        </div>
                                        <!--                                        <div>-->
                                        <!--                                            <input type="checkbox" name="endereco_principal" id="endereco_principal" value="1" />-->
                                        <!--                                            <label for="endereco_principal" class="checkbox"> Endereço principal </label>-->
                                        <!--                                        </div>-->
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <label for="endereco_complemento">Complemento</label>
                                        <textarea type="text" name="endereco_complemento" id="endereco_complemento" maxlength="155" class="form-control" placeholder="Ex: próximo ao supermercado."></textarea>
                                        <?=form_error('endereco_complemento');?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="mBtn-close" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="mBtn-save" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>