<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}empresa" class="ext_disabled"> <i class="fa fa-building"></i> Empresas</a>
        </li>
        <li>
            <a href="{url}empresa/chama_cadastro" class="ext_disabled"> <i class="fa fa-building-o"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title} <small class=" badge badge-danger pull-right">{head_msg}</small></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        {empresa}
        <form action="{url}empresa/save/{empresa_id}" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="formSep">

                <div class="col-m-12 col-md-6">
                    <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_cpf_responsavel">CPF</label>
                            <input type="text" name="empresa_cpf_responsavel" id="empresa_cpf_responsavel" value="{empresa_cpf_responsavel}" class="form-control" {disabled} {readonly}/>
                            <?=form_error('empresa_cpf_responsavel');?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_rg_responsavel">RG</label>
                            <input type="text" name="empresa_rg_responsavel" id="empresa_rg_responsavel" value="{empresa_rg_responsavel}" class="form-control" {disabled} {readonly}/>
                            <?=form_error('empresa_rg_responsavel');?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_segmento_id">Segmento</label>

                            <select name="empresa_segmento_id" id="empresa_segmento_id" class="form-control chosen-select" data-placeholder="Selecione um Segmento" {disabled} {readonly}>
                                <option></option>
                                {segmento}
                                <option value="{segmento_id}" {selected}>{segmento_name}</option>
                                {/segmento}
                            </select>
                            <?=form_error('empresa_segmento_id');?>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_user_id">Usuário Responsável</label>
                            <select name="empresa_user_id" id="empresa_user_id" class="form-control chosen-select" data-placeholder="Selecione o Usuário responsável pela empresa" {disabled} {readonly}>
                                <option></option>
                                {usuario}
                                <option value="{user_id}" {selected}>{user_fullname}</option>
                                {/usuario}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="total_servidor">Qnt. Servidores:</label>
                    <input type="number" min="0" max="99" name="total_servidor" class="form-control" value="{total_server}" {disabled}>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}empresa" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success" {disabled}>Salvar</button>
                </div>
            </div>
            {/empresa}
        </form>
    </div>
</div>
