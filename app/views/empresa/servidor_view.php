<div id="jCrumbs" class="breadCrumb module">
	<ul>
		<li>
			<a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
		</li>
		<li>
			<a href="{url}empresa" class="ext_disabled"> <i class="fa fa-building"></i> Empresas</a>
		</li>
		<li>
			<a href="{url}empresa/servidor" class="ext_disabled"> <i class="fa fa-building-o"></i> Servidor</a>
		</li>
	</ul>
</div>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<h3 class="heading">Cadastro de Servidores</h3>
	</div>
</div>

<div class="row">
	<form action="{url}empresa/servidor/{empresa_id}" method="POST" enctype="application/x-www-form-urlencoded">
		<div class="formSep">
			<div class="col-md-2">
				<label for="total_servidor">Qnt. Servidores:</label>
				<input type="number" min="0" max="99" name="total_servidor" class="form-control" value="{total_servidor}">
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-12">
			<a href="/empresa" class="btn btn-danger">Cancelar</a>
			<button class="btn btn-success">Salvar</button>
		</div>
	</form>
</div>
