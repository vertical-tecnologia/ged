<div id="jCrumbs" class="breadCrumb module">
	<ul>
		<li>
			<a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
		</li>
		<li>
			<a href="{url}empresa" class="ext_disabled"> <i class="fa fa-building"></i> Empresas</a>
		</li>
		<li>
			<a href="{url}empresa/cadastro" class="ext_disabled"> <i class="fa fa-building-o"></i> Cadastro</a>
		</li>
	</ul>
</div>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<h3 class="heading">{page_title} <small class=" badge badge-danger pull-right">{head_msg}</small></h3>
	</div>
</div>

<div class="row table-content">
	<div class="col-sm-12 col-md-12">
		{empresa}
		<form action="{url}empresa/save/{empresa_id}" method="POST" enctype="application/x-www-form-urlencoded">
			<div class="formSep">

				<div class="col-m-12 col-md-6">
					<input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<label for="empresa_razao">Razão Social</label>
							<input type="text" name="empresa_razao" id="empresa_razao" value="{empresa_razao}" class="form-control"/>
							<?=form_error('empresa_razao');?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<label for="empresa_fantasia">Nome Fantasia</label>
							<input type="text" name="empresa_fantasia" id="empresa_fantasia" value="{empresa_fantasia}" class="form-control" />
							<?=form_error('empresa_fantasia');?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<label for="empresa_cnpj">CNJP</label>
							<input type="text" name="empresa_cnpj" id="empresa_cnpj" value="{empresa_cnpj}" class="form-control" {readonly}/>
							<?=form_error('empresa_cnpj');?>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<label for="empresa_ie">IE</label>
							<input type="text" name="empresa_ie" id="empresa_ie" value="{empresa_ie}" class="form-control" {disabled}/>
							<?=form_error('empresa_ie');?>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">

					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<label for="empresa_segmento_id">Segmento</label>

							<select name="empresa_segmento_id" id="empresa_segmento_id" class="form-control chosen-select" data-placeholder="Selecione um Segmento" {disabled}>
								<option></option>
							{segmento}
								<option value="{segmento_id}" {selected}>{segmento_name}</option>
							{/segmento}
							</select>
							<?=form_error('empresa_segmento_id');?>
						</div>
						<div class="col-xs-12 col-sm-12">
							<label for="empresa_user_id">Usuário Responsável</label>
							<select name="empresa_user_id" id="empresa_user_id" class="form-control chosen-select" data-placeholder="Selecione o Usuário responsável pela empresa" {disabled}>
								<option></option>
								{usuario}
								<option value="{user_id}" {selected}>{user_fullname}</option>
								{/usuario}
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<label for="total_servidor">Qnt. Servidores:</label>
					<input type="number" min="0" max="99" name="total_servidor" class="form-control" value="{total_server}" {disabled}>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<a href="{url}empresa" class="btn btn-danger">Cancelar</a>
					<button class="btn btn-success">Salvar</button>
				</div>
			</div>
			{/empresa}
		</form>
	</div>
</div>
