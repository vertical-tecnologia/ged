<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}empresa" class="ext_disabled"> <i class="fa fa-building"></i> Empresas</a>
        </li>
        <li>
            <a href="{url}empresa/cadastro" class="ext_disabled"> <i class="fa fa-building-o"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Selecione o tipo de empresa<small class=" badge badge-danger pull-right">{head_msg}</small></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        
        <form action="<?php echo base_url().'empresa/'.STR_PROFILE_PERSONAL;?>" method="POST" enctype="application/x-www-form-urlencoded">
            

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="<?php echo base_url().'empresa/'.STR_PROFILE_COMPANY;?>" class="btn btn-danger">Pessoa Jurídica</a>
                    <button class="btn btn-success">Pessoa Física</button>
                </div>
            </div>
            
        </form>
    </div>
</div>