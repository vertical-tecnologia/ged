<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/usuarios"> <i class="fa fa-user"></i> Usuários</a>
        </li>
        <li>
            <a href="{url}/usuarios/cadastro"> <i class="fa fa-user"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Usuário</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}usuario_status/save" method="POST" enctype="application/x-www-form-urlencoded">
            {usuario_status}
            <input type="hidden" name="status_id" id="status_id" value="{status_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="status_name">Status</label><br/>
                        <input type="text" name="status_name" id="status_name" value="{status_name}" class="form-control" autofocus/>
                        <?=form_error('status_name')?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}usuario_status" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/usuario_status}
        </form>
    </div>
</div>
