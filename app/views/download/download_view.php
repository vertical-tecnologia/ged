<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/download/windows"> <i class="fa fa-windows"></i> Download</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Instruções de Download</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <div class="list-group pull-right">
            <?php if($path != ''): ?>
            <a href="{url}download/windows" target="_blank" class="list-group-item btn btn-primary"><i class="fa fa-windows"></i> Baixar o GED para windows</a>
            <?php endif; ?>
            <?php if($linux_path != ''): ?>
            <a href="{url}download/linux" target="_blank" class="list-group-item btn btn-primary"><i class="fa fa-linux"></i> Baixar o GED para Linux</a>
            <?php endif; ?>
            <?php if($macos_path != ''): ?>
            <a href="{url}download/macos" target="_blank" class="list-group-item btn btn-primary"><i class="fa fa-apple"></i> Baixar o GED para Mac OS</a>
            <?php endif; ?>
        </div>
        {instrucoes}
    </div>
</div>
