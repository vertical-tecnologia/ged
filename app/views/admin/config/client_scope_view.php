<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}servico"> <i class="fa fa-cogs"></i> Configuração</a>
        </li>
        <li>
            <a href="{url}servico/cadastro"> <i class="fa fa-list"></i> Escolha o tipo de uso</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2 class="heading">Configuração Inicial</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <p>Precisamos configurar o ged para o primeiro uso. Por favor siga as instruções abaixo.</p>

        <form action="{url}admin" method="post" enctype="application/x-www-form-urlencoded">

            <h3> Você deseja usar o ged para?</h3>

            <div class="row prod-chooser">
                <div class="col-xs-6">
                    <div class="col-sm-12 p-header"><p>Uso Pessoal</p></div>
                    <button type="submit" name="scope" class="btn btn-primary btn-radio" value="pessoal">Escolher</button>
                </div>
                <div class="col-xs-6">
                    <div class="col-sm-12 p-header"><p>Uso Empresarial</p></div>
                    <button type="submit" name="scope" class="btn btn-primary btn-radio" value="empresa">Escolher</button>
                </div>
            </div>
            <div class="clearfix"></div>

        </form>

    </div>
</div>