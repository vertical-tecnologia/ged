<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}servico"> <i class="fa fa-cogs"></i> Configuração</a>
        </li>
        <li>
            <a href="{url}servico/cadastro"> <i class="fa fa-list"></i> Dados Pessoais</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2 class="heading">Dados Pessoais</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3>Precisamos de mais algumas informações suas.</h3>

        <form action="{url}admin" method="post" enctype="application/x-www-form-urlencoded">

            <div class="row table-content">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <fieldset>
                        <legend>Localização</legend>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <label for="endereco_cep">CEP:</label>
                                <input class="form-control" type="text" name="endereco_cep" id="endereco_cep"/>
                            </div>
                                <div class="col-xs-12 col-sm-12 col-md-9">
                                <label for="endereco_logradouro">Rua/Av:</label>
                                <input class="form-control" type="text" name="endereco_logradouro" id="endereco_logradouro"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <label for="endereco_num">Numero:</label>
                                <input class="form-control" type="text" name="endereco_num" id="endereco_num"/>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <label for="endereco_bairro">Bairro:</label>
                                <input class="form-control" type="text" name="endereco_bairro" id="endereco_bairro"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <label for="endereco_num">Cidade:</label>
                                <input class="form-control" type="text" name="endereco_num" id="endereco_num"/>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <label for="endereco_estado">Estado:</label>
                                <input class="form-control" type="text" name="endereco_cidade" id="endereco_cidade"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="endereco_estado">Complemento:</label>
                                <textarea class="form-control" name="endereco_complemento" id="endereco_complemento"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">

                </div>
            </div>

        </form>
    </div>
</div>