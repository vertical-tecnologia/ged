<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>

    </ul>
</div>

<?php if ($this->session->userdata('USER_ROLE') != 'USUARIO'): ?>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Resumo Administrativo</h3>
        </div>
    </div>
    <div class="row table-content">
        <div class="col-sm-12 tac">
            <ul class="ov_boxes">
                <li>
                    <div class="p_bar_up p_canvas"><span>{novos}{total}{/novos}</span></div>
                    <div class="ov_text">
                        <strong>{total_novos}</strong>
                        Novos Usuários (mes)
                    </div>
                </li>
                <li>
                    <div class="p_bar_down p_canvas"><span>{novos_arquivos}{total}{/novos_arquivos}</span></div>
                    <div class="ov_text">
                        <strong>{total_arquivos_novos} </strong>
                        Novos Arquivos
                    </div>
                </li>
                <li>
                    <div class="p_bar_down p_canvas"><span>{arquivos_usuario}{total}{/arquivos_usuario}</span></div>
                    <div class="ov_text">
                        <strong>{total_arquivos_usuario} </strong>
                        Top Arquivos por usuario
                    </div>
                </li>
            </ul>
        </div>
    </div>
<?php endif; ?>

<div class="row table-content">
    <div class="col-sm-8 col-lg-8">
        <h3 class="heading">Tipos de arquivos
            <small>mais usados</small>
        </h3>

        <div id="tipo-chart">

        </div>

        <table class="table table-condensed hide" id="tipotable">
            <thead>
            <th>Tipo</th>
            <th>Quantidade Por Tipo</th>
            </thead>
            <tbody>
            {tipos}
            <tr>
                <th>{desc_file_type}</th>
                <td>{total}</td>
            </tr>
            {/tipos}

            </tbody>
        </table>

    </div>
    <div class="col-sm-4 col-lg-4" id="user-list">
        <h3 class="heading">Usuarios </h3>

        <div class="row">
            <div class="col-lg-12">
                <div class="input-group input-group-sm sepH_b">
						<span class="input-group-addon">
							<i class="glyphicon glyphicon-user"></i>
						</span>
                    <input class="user-list-search search form-control input-sm" placeholder="Buscar Usuário"
                           type="text">
                </div>
            </div>
        </div>
        <ul class="list user_list">

            {usuario}
            <li>
                <img src="{user_picture}" class="circle pull-right" alt="" style="width: 30px;height: 30px"/>
                <!--                <span class="label label-success pull-right sl_status">online</span>-->
                <a href="{url}mensagens/user/{user_login}" class="sl_name">{user_fullname}</a><br>
                <small class="s_color sl_email">{user_login}</small>
            </li>
            {/usuario}
        </ul>
        <ul class="pagination paging bottomPaging"></ul>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-lg-6">
        <h3 class="heading">Usuários
        </h3>

        <div id="users-chart">

        </div>

        <table class="table table-condensed hide" id="usuariosTable">
            <thead>
            <th>Tipo</th>
            <th>Quantidade Por Tipo</th>
            </thead>
            <tbody>
            {usuarios}
            <tr>
                <th>{user_fullname}</th>
                <td>{total}</td>
            </tr>
            {/usuarios}

            </tbody>
        </table>

    </div>
    <?php if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') : ?>
    <div class="col-sm-6 col-lg-6">
        <h3 class="heading">Empresas
        </h3>

        <div id="empresas-chart">

        </div>

        <table class="table table-condensed hide" id="empresasTable">
            <thead>
            <th>Empresas</th>
            <th>Quantidade Por Empresas</th>
            </thead>
            <tbody>
            {empresas}
            <tr>
                <th>{empresa_fantasia}</th>
                <td>{total}</td>
            </tr>
            {/empresas}

            </tbody>
        </table>

    </div>
    <?php endif;?>


        <div class="col-sm-12 col-md-12">
            <div id="tipoChartBars">
            </div>

            <table class="table table-condensed hide" id="tipotablebars">
                <thead>
                <th>Tipo</th>
                <th>Quantidade Por Tipo</th>
                </thead>
                <tbody>
                {tipos_bar}
                <tr>
                    <th>{desc_file_type}</th>
                    <td>{total}</td>
                </tr>
                {/tipos_bar}
                </tbody>
            </table>
        </div>
</div>