<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row table-content">
            <div class="col-sm-12 tac">
                <ul class="ov_boxes">
                    <li>
                        <div class="p_bar_up p_canvas"><span>0</span></div>
                        <div class="ov_text">
                            <strong>0</strong>
                            Arquivos
                        </div>
                    </li>
                    <li>
                        <div class="p_bar_down p_canvas"><span>0</span></div>
                        <div class="ov_text">
                            <strong>0 </strong>
                            Usuários
                        </div>
                    </li>
                    <li>
                        <div class="p_line_up p_canvas"><span>0</span></div>
                        <div class="ov_text">
                            <strong>0</strong>
                            Acessos
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8">
                <h3 class="heading">Status do Serviço</h3>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4">
                <h3 class="heading">Configuração</h3>

                <div class="w-box-header">
                    Endereços
                    <div class="pull-right">
                        <div class="btn-group">
                            <a class="btn btn-default btn-xs" href="#">
                                <span class="fa fa-plus"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="w-box-content">
                    <table class="table table-striped table_vam no-th">
                        <tbody>
                        <tr>
                            <td>Nenhum endereço cadastrado.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="w-box-header margin-top-1em">
                    Servidores
                    <div class="pull-right">
                        <div class="btn-group">
                            <a class="btn btn-default btn-xs" href="#">
                                <span class="fa fa-plus"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="w-box-content">
                    <table class="table table-striped table_vam no-th">
                        <tbody>
                        <tr>
                            <td>Nenhum servidor cadastrado.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="w-box-header margin-top-1em">
                    Usuários
                    <div class="pull-right">
                        <div class="btn-group">
                            <a class="btn btn-default btn-xs" href="#">
                                <span class="fa fa-plus"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="w-box-content">
                    <table class="table table-striped table_vam no-th">
                        <tbody>
                        <tr>
                            <td>Nenhum usuário cadastrado.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>