<div class="dialog dialog-close">
    <div class="dialog-overlay"></div>
    <div class="dialog-main dialog-sm dialog-close" tabindex="-1">
        <div class="dialog-header">
            <p class="dialog-title" id="dialog-title">Pasta</p>
        </div>
        <button class="dialog-button dialog-button-close"
                title="Fechar">
            <i class="fa fa-home"></i>
        </button>
        <div class="dialog-inner">
            <div class="dialog-content">
                <div>
                    <div class="textField">
                        <label class="d-label" id="d-label">Nome da pasta</label>
                        <input type="hidden" name="str_id" value="{str_id}"/>
                        <input class="textField-field"
                               name="folder"
                               autofocus
                               data-event="keyup"
                               type="text"
                               data-param="field"
                               id="textField-folder"
                               target="#save"
                               placeholder="Digite o nome da pasta"/>
                    </div>
                    <div class="action-status app-font" id="action-status"></div>
                </div>
            </div>

            <div class="dialog-actions">
                <div class="dialog-actionsRight">
                    <button class="dialog-action-button dialog-action-button-primary"
                            id="save"
                            disabled
                            data-action="mkdir"
                            data-url="arquivos/cmd"
                            data-cmd="panel"
                            data-field-class="textField-field"
                            data-context-cmd="dir"
                        >
                        <span id="bt-label">Criar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>