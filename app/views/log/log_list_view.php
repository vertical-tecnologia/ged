<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}log" class="ext_disabled"> <i class="fa fa-file-text"></i> Log</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Log </h3>
    </div>
    <div class="col-sm-12 col-md-12">
        {filter_link}
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>Data/Hora</th>
            <th>Usuario</th>
            <th>Açao</th>
            <th>IP</th>
            <th>Browser</th>
            </thead>

            <tbody>
            {log}
            <tr>
                <td>{datetime}</td>
                <td>{user_name}</td>
                <td>{operation}</td>
                <td>{ip_address}</td>
                <td>{browser}</td>
            </tr>
            {/log}
            </tbody>
        </table>
    </div>
</div>
