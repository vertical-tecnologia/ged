<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}usuario" class="ext_disabled"> <i class="fa fa-users"></i> Usuário</a>
        </li>
        <li>
            <a href="{url}usuario/empresa" class="ext_disabled"> <i class="fa fa-user"></i> Empresa</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Dados da Empresa</h3>

        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_br1" data-toggle="tab">Empresa</a></li>
                <li><a href="#tab_br2" data-toggle="tab">Localização</a></li>
                <li><a href="#tab_br3" data-toggle="tab">Contato</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_br1">
                    <form action="{url}usuario/save" method="POST" enctype="application/x-www-form-urlencoded">
                        <input type="hidden" name="empresa_user_id" id="empresa_user_id" value="{empresa_user_id}"/>
                        <div class="col-m-12 col-md-6">
                            {empresa}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label for="empresa_fantasia">Nome Fantasia</label>
                                    <input type="text" name="empresa_fantasia" id="empresa_fantasia" value="{empresa_fantasia}" class="form-control"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label for="empresa_razao">Razão Social</label>
                                    <input type="text" name="empresa_razao" id="empresa_razao" value="{empresa_razao}" class="form-control"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <label for="empresa_cnpj">CNPJ</label>
                                    <input type="text" name="empresa_cnpj" id="empresa_cnpj" value="{empresa_cnpj}" class="form-control cnpj"/>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <label for="empresa_ie">IE</label>
                                    <input type="text" name="empresa_ie" id="empresa_ie" value="{empresa_ie}" class="form-control"/>
                                </div>
                            </div>
                            {/empresa}

                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label for="empresa_segmento_id">Segmento</label>
                                    <select type="text" name="empresa_segmento_id" id="empresa_segmento_id" value="{empresa_segmento_id}" class="form-control chosen-select">
                                        {segmento}
                                        <option value="{segmento_id}">{segmento_name}</option>
                                        {/segmento}
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <a href="{url}usuario" class="btn btn-danger">Cancelar</a>
                                    <button class="btn btn-success">Salvar</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab_br2">
                    {empresa_endereco}
                </div>
                <div class="tab-pane" id="tab_br3">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et tellus felis, sit amet interdum tellus. Suspendisse sit amet scelerisque dui. Vivamus faucibus magna quis augue venenatis ullamcorper. Proin eget mauris eget orci lobortis luctus ac a sem. Curabitur feugiat, eros consectetur egestas iaculis,
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>