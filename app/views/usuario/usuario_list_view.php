<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/usuarios"> <i class="fa fa-user"></i> Usuários</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Usuários</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        {filter_link}
        {button_add}
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Cadastrado em</th>
            <th>Alterado em</th>
            <th>Grupo</th>
            <th>Status</th>
            <th><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {usuarios}
            <tr>
                <td><a href="{url}usuario/visualizar/{user_id}">{user_id}</a></td>
                <td><a href="{url}usuario/visualizar/{user_id}">{user_fullname}</a></td>
                <td><a href="{url}usuario/visualizar/{user_id}">{user_email}</a></td>
                <td><a href="{url}usuario/visualizar/{user_id}">{user_register_date}</a></td>
                <td><a href="{url}usuario/visualizar/{user_id}">{user_update_day}</a></td>
                <td><a href="{url}usuario/visualizar/{user_id}">{type_name}</a></td>
                <td><a href="{url}usuario/visualizar/{user_id}">{status_name}</a></td>
                <td>
                    {link_mail}
                    {link_edt}
                    {link_del}
                </td>
            </tr>
            {/usuarios}
            </tbody>
        </table>
    </div>
</div>
