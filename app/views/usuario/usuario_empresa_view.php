<li class="v-heading">
    Empresa
    <a href="{url}usuario/empresa/{user_id}" class="btn btn-xs btn-primary pull-right"><i class="fa fa-edit"></i> Editar dados da empesa</a>
</li>
{empresa_dados}
<li>
    <span class="item-key">Razão Social</span>
    <div class="vcard-item">{empresa_razao}</div>
</li>
<li>
    <span class="item-key">Nome Fantasia</span>
    <div class="vcard-item">{empresa_fantasia}</div>
</li>
<li>
    <span class="item-key">CNPJ</span>
    <div class="vcard-item">{empresa_cnpj}</div>
</li>
<li>
    <span class="item-key">Inscrição Estadual</span>
    <div class="vcard-item">{empresa_ie}</div>
</li>
<li>
    <span class="item-key">Segmento</span>
    <div class="vcard-item">{segmento_name}</div>
</li>
{/empresa_dados}