<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}usuario" class="ext_disabled"> <i class="fa fa-users"></i> Usu&aacute;rios</a>
        </li>
        <li>
            <a href="{url}usuario/excluir" class="ext_disabled"> <i class="fa fa-times"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Excluir Usuário <span class="badge badge-danger">Essa operação não pode ser desfeita.</span></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        {usuario}

            <div class="col-sm-12 col-md-6">
                <h4>{user_fullname}</h4>
                <p>{user_email}</p>
                <p><strong>Cadastrado em:</strong></p>
                <p>{user_register_date}</p>
                <div class="well">
                    Todos os documentos do usuário serão transferidos para o administrador da conta.
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
            <h3>Se voce excluir um usuário os seguintes dados serão excluídos:</h3>
            <ul class="list-group">
                    <li class="list-group-item">Contatos</li>
                    <li class="list-group-item">Endereços</li>
                    <li class="list-group-item">Mensagens</li>
                    <li class="list-group-item">Grupos criados pelo usuário</li>
                    <li class="list-group-item">Compartilhamentos criados pelo usuário</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}usuario" class="btn btn-danger">Cancelar</a>
                    <a href="{url}usuario/safe_delete/{user_id}" class="btn btn-success">Excluir</a>
                </div>
            </div>
            {/usuario}
    </div>
</div>
