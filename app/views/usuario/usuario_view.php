{user}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}usuario/"> <i class="fa fa-users"></i> Usuários </a>
        </li>
        <li>
            <a href="{url}usuario/visualizar/{user_id}"> <i class="fa fa-user"></i> Dados do Usuário</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="hpanel hblue">
            <div class="panel-body">
                <form action="{url}perfil/image" method="post" id="user_picture_form" enctype="multipart/form-data">
                    <input type="file" id="user_picture" name="user_picture" class="hide" accept="image/x-png, image/gif, image/jpeg" onchange="imageControl.preview(this)"/>
                    <img alt="Foto de perfil" class="img-avatar" id="uploadPreview" src="{user_picture}">
                </form>
                <h3><a>{user_fullname}</a></h3>
                {main_address}
                <div class="text-muted font-bold m-b-xs">{endereco_cidade}, {endereco_estado}</div>
                {/main_address}
                <input type="hidden" id="parent_field" value="user_id"/>
                <input type="hidden" id="user_id" name="user_id" value="<?=$this->session->userdata('USER_ID')?>"/>
            </div>
            <?php if($type_id != 1 && $type_id != 6): ?>
            <div class="panel-body margin-top-1em">
                <legend>Empresas vinculadas</legend>
                {empresa}
                <h5>{empresa_razao}</h5>
                {/empresa}
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="col-lg-8">

        <div class="hpanel hblue">
            <div class="panel-body">
                <h3>Dados Pessoais <small class="pull-right">({verified})</small></h3>
                <hr/>
                <table>
                    <tbody>
                    <tr>
                        <td class="table-desc">Email:</td>
                        <td>{user_email}</td>
                    </tr>
                    <tr>
                        <td class="table-desc">Data de cadastro:</td>
                        <td>{user_register_date}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="hpanel margin-top-1em">
            <div class="panel-body">
                <div class="w-box" id="w_sort04">
                    <div class="w-box-header">
                        Endereços
<!--                        <div class="pull-right">-->
<!--                            <div class="btn-group">-->
<!--                                <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalUsuarioEndereco">-->
<!--                                    <i class="fa fa-plus"></i>-->
<!--                                </button>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                    <div class="w-box-content">
                        <table class="table table-condensed" id="table_endereco">
                            <tbody>
                            {enderecos}
                            <tr>
                                <td>{endereco_alias}</td>
                                <td>{endereco_cidade}</td>
                                <td class="text-right">
                                    <a href="{url}endereco/excluir/{endereco_id}" data-ajax="true" data-target="table_endereco" data-callback="gedtable|populate" data-arg-value="{user_id}" data-arg-href="{url}endereco/listar/{user_id}/user/">excluir</a>
                                </td>
                            </tr>
                            {/enderecos}
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="w-box" id="w_sort04">
                    <div class="w-box-header">
                        Contatos
<!--                        <div class="pull-right">-->
<!--                            <div class="btn-group">-->
<!--                                <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalUsuarioContato">-->
<!--                                    <i class="fa fa-plus"></i>-->
<!--                                </button>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                    <div class="w-box-content">
                        <table class="table table-condensed" id="table_contato">
                            <tbody>
                            {contatos}
                            <tr>
                                <td>{tipo_desc}</td>
                                <td>{contato_valor}</td>
                                <td  class="text-right">
                                    <a href="{url}contato/excluir/{contato_id}" data-ajax="true" data-target="table_contato" data-callback="gedtable|populate" data-arg-value="{user_id}" data-arg-href="{url}contato/usuario/{user_id}">excluir</a>
                                </td>
                            </tr>
                            {/contatos}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
{/user}
