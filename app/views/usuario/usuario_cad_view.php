<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}usuario" class="ext_disabled"> <i class="fa fa-users"></i> Grupos</a>
        </li>
        <li>
            <a href="{url}usuario/cadastro" class="ext_disabled"> <i class="fa fa-user"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Usuário</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}usuario/save" method="POST" enctype="application/x-www-form-urlencoded">
            {usuario}
            <div class="formSep">

            <div class="col-m-12 col-md-6">
            <input type="hidden" name="user_id" id="user_id" value="{user_id}"/>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="user_fullname">Nome Completo</label>
                        <input type="text" name="user_fullname" id="user_fullname" value="{user_fullname}" class="form-control" placeholder="Nome Completo"/>
                        <?php echo form_error('user_fullname'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="user_fullname">Login</label>
                        <input type="text" name="user_login" id="user_login" value="{user_login}" class="form-control" placeholder="Login"{readonly}/>
                        <?php echo form_error('user_login'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="user_email">Email</label>
                        <input type="text" name="user_email" id="user_email" value="{user_email}" class="form-control" placeholder="email@email.com"/>
                        <?php echo form_error('user_email'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="user_status_id">Status de Usuário</label>
                        <select name="user_status_id" id="user_status_id" class="form-control chosen-select">
                            <option value="">Selecione o status do Usuário</option>
                            {usuario_status}
                            <option value="{status_id}"{selected}>{status_name}</option>
                            {/usuario_status}
                        </select>
                        <?php echo form_error('user_status_id'); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">

                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="user_type_id">Tipo de Usuário</label>
                        <select name="user_type_id" id="user_type_id" onchange="gedToggleEnable.init(this, 'empresa_id_box', 6, 'hide')" class="form-control chosen-select" data-placeholder="Selecione o Tipo do Usuário">
                            <option></option>
                            {usuario_tipo}
                            <option value="{type_id}"{selected}>{type_name}</option>
                            {/usuario_tipo}
                        </select>
                        <?php echo form_error('user_type_id'); ?>
                    </div>
                </div>
                <div class="row" id="empresa_id_box">
                    <div class="col-xs-12 col-sm-12">
                        <label for="user_type_id">Empresa</label>
                        <select name="empresa_id" id="empresa_id" class="form-control chosen-select" data-placeholder="Selecione uma empresa para o usuário">
                            <option></option>
                            {empresas}
                            <option value="{empresa_id}"{selected}>{empresa_razao}</option>
                            {/empresas}
                        </select>
                        <?php echo form_error('empresa_id'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        <label for="user_type_id">Senha</label>
                        <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Informe uma senha para o usuário."/>
                        <?php echo form_error('user_password'); ?>
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        <label for="user_type_id">Confirme a Senha</label>
                        <input type="password" name="cuser_password" id="cuser_password" class="form-control" placeholder="Informe uma senha para o usuário."/>
                        <?php echo form_error('cuser_password'); ?>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}usuario" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/usuario}
        </form>
    </div>
</div>
