<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/usuarios"> <i class="fa fa-user"></i> Usuários</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Tipos de Usuários</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}usuario_tipo/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Tipo de Usuário</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {usuario_tipo}
            <tr>
                <td>{type_id}</td>
                <td>{type_name}</td>
                <td>{type_desc}</td>
                <td class="text-right">
                    {link_upd}
                    {link_del}
                </td>
            </tr>
            {/usuario_tipo}
            </tbody>
        </table>
    </div>
</div>
