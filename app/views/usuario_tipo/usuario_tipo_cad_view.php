<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}usuario_tipo/"> <i class="fa fa-users"></i> Tipo de Usuario</a>
        </li>
        <li>
            <a href="{url}usuario_tipo/cadastro"> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Tipo de Usuário</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}usuario_tipo/save" method="POST" enctype="application/x-www-form-urlencoded">
            {usuario_tipo}
            <input type="hidden" name="type_id" id="type_id" value="{type_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="type_name">Nome:</label><br/>
                        <input type="text" name="type_name" id="type_name" value="{type_name}" class="form-control" autofocus/>
                        <?=form_error('type_name')?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="type_desc">Descrição:</label><br/>
                        <textarea type="text" name="type_desc" id="type_desc" class="form-control">{type_desc}</textarea>
                        <?=form_error('type_desc')?>
                    </div>
                </div>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}usuario_tipo" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/usuario_tipo}
        </form>
    </div>
</div>
