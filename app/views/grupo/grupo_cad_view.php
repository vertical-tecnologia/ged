<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}grupo" class="ext_disabled"> <i class="fa fa-users"></i> Grupos</a>
        </li>
        <li>
            <a href="{url}grupo/cadastro" class="ext_disabled"> <i class="fa fa-user"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Grupo</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}grupo/save" method="POST" enctype="application/x-www-form-urlencoded">
            {grupo}
            <input type="hidden" name="group_id" id="group_id" value="{group_id}"/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="status_name">Nome do Grupo</label><br/>
                        <input type="text" name="group_name" id="group_name" value="{group_name}" class="form-control" autofocus/>
                        <?=form_error('group_name');?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}grupo" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/grupo}
        </form>
    </div>
</div>
