<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}grupo" class="ext_disabled"> <i class="fa fa-users"></i> Grupos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Grupos</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}grupo/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Grupo</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th>Proprietario</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {grupo}
            <tr>
                <td>{group_id}</td>
                <td>{group_name}</td>
                <td>{group_owner_name}</td>
                <td class="text-right">
                    {link_per}
                    {link_usr}
                    {link_upd}
                    {link_del}
                </td>
            </tr>
            {/grupo}
            </tbody>
        </table>
    </div>
</div>
