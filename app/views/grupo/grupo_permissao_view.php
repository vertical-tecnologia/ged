{grupo}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}grupo" class="ext_disabled"> <i class="fa fa-users"></i> Grupos</a>
        </li>
        <li>
            <a href="{url}grupo/usuarios/{group_id}" class="ext_disabled"> <i class="fa fa-user"></i> Usuários do Grupo</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Permissões do Grupo <strong>{group_name}</strong></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">

        <div class="pull-right">
            <a href="{url}grupo" class="btn btn-primary"> voltar</a>
        </div>
        <div class="clearfix"></div>

        <form action="{url}grupo/save_folder_permissions" method="POST" enctype="application/x-www-form-urlencoded">

            <input type="hidden" name="group_id" id="group_id" value="{group_id}"/>
            <input type="hidden" name="folder_id" id="folder_id" value=""/>
            <input type="hidden" name="permission_id" id="permission_id" value=""/>
            <div class="formSep">
                <div class="row">
                    <div class="col-sm-12 col-md-10 col-md-10">
                        <p><span class="label label-default">Diretórios</span></p>
                        <div id="js_tree">
                            <ul>
                    <?php foreach ($arrHtmlDirs as $index => $directory) :?>
                            <li><strong><?php echo !is_null($directory['description']) ? $directory['description'] : 'Arquivos';?></strong></li>
                            <?php echo $directory['html'];?>
                           <!-- {objects} -->
                    <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 col-lg-2">
                        <p><span class="label label-default">Permissões</span></p>
                        <div>
                            <input type="checkbox" id="list" name="list" {g_read}/>
                            <label for="list" class="checkbox"> Listar </label>
                        </div>
                        <div>
                            <input type="checkbox" id="read" name="read" {g_write}/>
                            <label for="read" class="checkbox"> Ler </label>
                        </div>
                        <div>
                            <input type="checkbox" id="write" name="write" {g_del}/>
                            <label for="write" class="checkbox"> Escrever/Alterar </label>
                        </div>
                        <div>
                            <input type="checkbox" id="del" name="del" {g_del}/>
                            <label for="del" class="checkbox"> Excluir </label>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}grupo" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>
{/grupo}
