{grupo}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}grupo" class="ext_disabled"> <i class="fa fa-users"></i> Grupos</a>
        </li>
        <li>
            <a href="{url}grupo/usuarios/{group_id}" class="ext_disabled"> <i class="fa fa-user"></i> Usuários do Grupo</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Usuários do Grupo <strong>{group_name}</strong></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <div class="pull-right">
            <a href="{url}grupo" class="btn btn-primary"> voltar</a>
        </div>
        <div class="clearfix"></div>
        <form action="{url}grupo/save_usuarios" method="POST" enctype="application/x-www-form-urlencoded">

            <input type="hidden" name="group_id" id="group_id" value="{group_id}"/>
            <div class="formSep">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <p><span class="label label-default">Usuários</span></p>
                        <div class="sepH_b">
                            <select id="grupo-usuarios" name="usuarios[]" multiple="multiple">
                                {usuario}
                                <option value="{user_id}"{selected}>{user_fullname}</option>
                                {/usuario}
                            </select>
                        </div>
                        <div class="btn-toolbar sepH_a">
                            <div class="btn-group">
                                <a href="#" id="select-all" class="btn btn-default btn-xs">Selecionar Tudo</a>
                                <a href="#" id="deselect-all" class="btn btn-default btn-xs">Deselecionar Tudo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}grupo" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>
{/grupo}
