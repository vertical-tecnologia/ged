<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}segmento/diretorios/{segmento_id}"> <i class="fa fa-user"></i> Status dos Segmentos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Status dos Segmentos</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}segmento_status/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Status</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {segmento_status}
            <tr>
                <td>{status_id}</td>
                <td>{status_name}</td>
                <td class="text-right">
                    <a class="ext_disabled" href="{url}segmento_status/editar/{status_id}">editar</a>
                    <a class="ext_disabled" href="{url}segmento_status/excluir/{status_id}">excluir</a>
                </td>
            </tr>
            {/segmento_status}
            </tbody>
        </table>
    </div>
</div>
