{segmento}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}segmento/novo_diretorio/{segmento_id}"> <i class="fa fa-user"></i> Diretórios dos Segmentos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Diretório</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">

        <form action="{url}segmento/save_diretorio/{segmento_id}" method="POST" enctype="application/x-www-form-urlencoded">
            <input type="hidden" name="segmento_id" id="segmento_id" value="{segmento_id}"/>
            <input type="hidden" name="dir_parent_id" id="dir_parent_id" value="{dir_parent_id}"/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="dir_name">Nome do Diretorio</label><br/>
                        <input type="text" name="dir_name" id="dir_name" value="" class="form-control" autofocus/>
                        <?=form_error('dir_name');?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}segmento/diretorios/{segmento_id}" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success" type="submit" name="action" value="<?=$this->router->fetch_method();?>">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>
{/segmento}