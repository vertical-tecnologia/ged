<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}segmento/"> <i class="fa fa-building"></i> Segmentos</a>
        </li>
        <li>
            <a href="{url}segmento/diretorios/{segmento_id}"> <i class="fa fa-folder"></i> Diretórios dos Segmentos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Diretórios</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-8">
        <div class="css-treeview">
            {diretorio}
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <a href="{url}segmento/novo_diretorio/{segmento_id}" class="btn btn-primary pull-right">Novo diretorio</a>
    </div>
</div>
