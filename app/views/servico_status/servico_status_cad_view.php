<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/servico"> <i class="fa fa-cogs"></i> Serviços</a>
        </li>
        <li>
            <a href="{url}/servico_status"> <i class="fa fa-cog"></i> Status de Serviços</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Status de Serviço</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}servico_status/save" method="POST" enctype="application/x-www-form-urlencoded">
            {servico_status}
            <input type="hidden" name="service_status_id" id="service_status_id" value="{service_status_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="service_status_name">Status</label><br/>
                        <input type="text" name="service_status_name" id="service_status_name" value="{service_status_name}" class="form-control" autofocus/>
                        <?=form_error('service_status_name');?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}servico_status" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/servico_status}
        </form>
    </div>
</div>
