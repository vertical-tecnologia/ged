<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/servico"> <i class="fa fa-cogs"></i> Serviços</a>
        </li>
        <li>
            <a href="{url}/servico_status"> <i class="fa fa-cog"></i> Status de Serviços</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Status de Serviço</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}servico_status/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Status</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {servico_status}
            <tr>
                <td>{service_status_id}</td>
                <td>{service_status_name}</td>
                <td class="text-right">
                    {link_upd}
                    {link_del}
                </td>
            </tr>
            {/servico_status}
            </tbody>
        </table>
    </div>
</div>
