<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <div class="col-sm-6 col-md-6 text-right">
            <a class="btn btn-lg btn-success" href="<?php echo base_url().'admin/'.STR_PROFILE_COMPANY;?>">Sua Empresa</a>
        </div>
        <div class="col-sm-6 col-md-6 text-left">
            <a class="btn btn-lg btn-info" href="<?php echo base_url().'admin/'.STR_PROFILE_PERSONAL;?>">Uso Pessoal</a>
        </div>
    </div>
</div>
