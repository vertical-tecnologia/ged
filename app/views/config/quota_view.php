<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao" class="ext_disabled"> <i class="fa fa-cog"></i> Configuraçao</a>
        </li>
        <li>
            <a href="{url}configuracao/quota" class="ext_disabled"> <i class="fa fa-hdd-o"></i> Cota</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Cotas de Usuario</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">

        <form action="{url}configuracao/save_quota" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="formSep">

                <div class="col-m-12 col-md-6">
                    <div>
                        <input type="checkbox" id="quota_enabled" name="quota_enabled" {checked}/>
                        <label for="quota_enabled" class="checkbox"> Habilitar Cota?</label>
                    </div>

                    <div class="margin-top-1em">
                        <table class="table">
                            <tr>
                                <td>Espaço disponível para cota.</td>
                                <td><span class="badge badge-success pull-right">{free_space}</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php if($quota_status != null && $quota_status != false): ?>
                <div class="col-sm-12 col-md-6">
                    <table class="table table-condensed table-bordered">
                        {quota}
                        <tr>
                            <td>
                                <a href="{url}configuracao/quota_edit/{user_id}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a> {user_fullname}
                            </td>
                            <td>{quota_size}</td>
                            <td>
                                <span class="badge bage-info pull-right">{user_login}</span>
                            </td>
                        </tr>
                    {/quota}
                    </table>


                </div>
                <?php endif;?>
                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}empresa" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success" {disabled}>Salvar</button>
                </div>
            </div>

        </form>
    </div>
</div>
