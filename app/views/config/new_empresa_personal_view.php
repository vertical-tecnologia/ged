<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        {empresa}
        <form action="{url}admin/personal" method="POST" enctype="multipart/form-data">
            <div class="formSep">
                <div class="col-m-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">
                            <label for="empresa_cpf_responsavel">CPF do Responsável*</label>
                            <input type="text" name="empresa_cpf_responsavel" id="empresa_cpf_responsavel" onblur="validaCPF(this)" value="{empresa_cpf_responsavel}" class="form-control cpf" {disabled} {readonly}/>
                            <?=form_error('empresa_cpf_responsavel');?>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <label for="empresa_rg_responsavel">RG do Responsável*</label>
                            <input type="text" name="empresa_rg_responsavel" id="empresa_rg_responsavel" value="{empresa_rg_responsavel}" class="form-control rg" {disabled} {readonly}/>
                            <?=form_error('empresa_rg_responsavel');?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_segmento_id">Segmento</label>
                            <select name="empresa_segmento_id" id="empresa_segmento_id" class="form-control chosen-select" data-placeholder="Selecione um Segmento" {disabled} {readonly}>
                                <option></option>
                                {segmento}
                                <option value="{segmento_id}">{segmento_name}</option>
                                {/segmento}
                            </select>
                            <?=form_error('empresa_segmento_id');?>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_user_id">Usuário Responsável</label>
                            <input type="hidden" id="empresa_user_id" name="empresa_user_id" value="{user_id}"/>
                            <input type="text" value="{user_name}" class="form-control" {disabled}/>
                        </div>

                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="hidden" value="save" name="action" id="action"/>
                    <a class="btn btn-info pull-left" href="<?php echo base_url();?>profile">< Voltar</a>
                    <button class="btn btn-info pull-right" {disabled} type="submit">Avançar ></button>
                </div>
            </div>
            {/empresa}
        </form>
    </div>
</div>
<script>

    function validaCPF($cpf = null) {
 
        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = ereg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }
    
</script>