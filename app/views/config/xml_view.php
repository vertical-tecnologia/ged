<div id="jCrumbs" class="breadCrumb module">
	<ul>
		<li>
			<a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
		</li>
		<li>
			<a href="{url}configuracao" class="ext_disabled"> <i class="fa fa-cog"></i> Configuraçao</a>
		</li>
		<li>
			<a href="{url}configuracao/quota" class="ext_disabled"> <i class="fa fa-hdd-o"></i> Arquivo XML</a>
		</li>
	</ul>
</div>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<h3 class="heading">Cadastro de Tags XML</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-{class}">
			{content}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 col-md-12">
		<form action="/configuracao/xml" method="POST" enctype="application/x-www-form-urlencoded">
			<div class="formSep">
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-12">
							<label>Scanner 1:</label>
						</div>
						<div class="col-md-10 sepH_c">
							<!-- <input name="scanner[<?php //echo XML_SCANNER1;?>]" class="form-control" value="{scanner_1}"> -->
							<div class="input-group input-group-md">
								<input class="form-control" type="text">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default add-tag" data-table="table-scanner-<?php echo XML_SCANNER1;?>" data-scanner="scanner[<?php echo XML_SCANNER1;?>]">+</button>
								</div>
							</div>
						</div>
						<!-- <textarea name="scanner[<?php //echo XML_SCANNER1;?>]" class="form-control">{scanner_1}</textarea> -->
						<div class="col-md-10">
							<table id="table-scanner-<?php echo XML_SCANNER1;?>" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tag</th>
										<th class="text-center">Ação</th>
									</tr>
								</thead>
								<tbody>
								{if scanner<?php echo XML_SCANNER1;?> == TRUE}
									{scanner<?php echo XML_SCANNER1;?>}
									<tr>
										<td>{tag}</td>
										<input type="hidden" name="scanner[<?php echo XML_SCANNER1;?>][]" value="{tag}">
										<td class="text-center"><a class="remove-tag" href="#">Excluir</a></td>
									</tr>
									{/scanner<?php echo XML_SCANNER1;?>}
								{else}
									<tr class="none-tag"><td colspan="2"> Nenhuma tag informada.</td></tr>
								{/if}
								</tbody>
							</table>
						</div>
					</div>

					<div class="col-md-6">
						<div class="col-md-12">
							<label>Scanner 2:</label>
						</div>
						<div class="col-md-10 sepH_c">
							<div class="input-group input-group-md">
								<input class="form-control" type="text">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default add-tag" data-table="table-scanner-<?php echo XML_SCANNER2;?>" data-scanner="scanner[<?php echo XML_SCANNER2;?>]" >+</button>
								</div>
							</div>
						</div>

						<div class="col-md-10">
							<table id="table-scanner-<?php echo XML_SCANNER2;?>" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tags</th>
										<th class="text-center">Ação</th>
									</tr>
								</thead>
								<tbody>
								{if scanner<?php echo XML_SCANNER2;?> == TRUE}
									{scanner<?php echo XML_SCANNER2;?>}
									<tr>
										<td>{tag}</td>
										<input type="hidden" name="scanner[<?php echo XML_SCANNER2;?>][]" value="{tag}">
										<td class="text-center"><a class="remove-tag" href="#">Excluir</a></td>
									</tr>
									{/scanner<?php echo XML_SCANNER2;?>}
								{else}
									<tr class="none-tag"><td colspan="2"> Nenhuma tag informada.</td></tr>
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a href="{url}" class="btn btn-danger">Cancelar</a>
					<button type="submit" name="btnsalvar" class="btn btn-success">Salvar</button>
				</div>
			</div>
		</form>
	</div>
</div>
