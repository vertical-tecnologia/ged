<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao" class="ext_disabled"> <i class="fa fa-cog"></i> Configuração</a>
        </li>
        <li>
            <a href="{url}configuracao/adicional" class="ext_disabled"> <i class="fa fa-hdd-o"></i> Espaço Adicional</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading"> Quantidades de Armazenamento</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-6">

        <form action="{url}configuracao/save_armazenamento" method="POST" enctype="application/x-www-form-urlencoded">
            {qtde}
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <label for="quant_desc">Descriçao</label>
                    <input type="text" name="quant_desc" id="quant_desc" value="{quant_desc}" class="form-control" {disabled} placeholder="Descrição"/>
                    <?=form_error('quant_desc');?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <label for="quant_preco">Preço</label>
                    <input type="text" name="quant_preco" id="quant_preco" value="{quant_preco}" class="form-control" {disabled} placeholder="Valor cobrado pelo armazenamento"/>
                    <?=form_error('quant_preco');?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <label for="quant_valor">Quantidade (em bytes)</label>
                    <input type="text" name="quant_valor" id="quant_valor" value="{quant_valor}" class="form-control" {disabled} placeholder="Quantidade de espaço disponíbilizado"/>
                    <?=form_error('quant_valor');?>
                </div>
            </div>
            <div class="margin-1em">
                <button class="btn btn-success pull-right" {disabled}>Adicionar</button>
            </div>
            {/qtde}
        </form>
    </div>

    <div class="col-sm-12 col-md-6">
        <table class="table table-condensed">
            <thead>
            <th>#</th>
            <th>Descriçao</th>
            <th>Preço</th>
            <th>Quantidade (em bytes)</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {quantidades}
            <tr>
                <td>{quant_id}</td>
                <td>{quant_desc}</td>
                <td>{quant_preco}</td>
                <td>{quant_valor} bytes</td>
                <td class="text-right">
                    <a class="ext_disabled" href="{url}configuracao/excluir_armazenamento/{quant_id}">excluir</a>
                </td>
            </tr>
            {/quantidades}
            </tbody>
        </table>
    </div>
</div>
