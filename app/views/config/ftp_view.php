<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao/contrato"> <i class="fa fa-file"></i> Contrato</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12 col-lg-12">
        {server}
        <form action="{url}configuracao/save_ftp/" method="POST" enctype="application/x-www-form-urlencoded" class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control" id="ftp_host" name="ftp_host" placeholder="Endereço do servidor" value="{ftp_host}">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="ftp_user" name="ftp_user" placeholder="Usuario" value="{ftp_user}">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="ftp_password" name="ftp_password" placeholder="Senha do FTP" value="{ftp_password}">
                </div>
                <button type="submit" class="btn btn-default">Salvar</button>

        </form>
        {/server}
    </div>
</div>
