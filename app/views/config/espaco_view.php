<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao" class="ext_disabled"> <i class="fa fa-cog"></i> Configuração</a>
        </li>
        <li>
            <a href="{url}configuracao/adicional" class="ext_disabled"> <i class="fa fa-hdd-o"></i> Espaço Adicional</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading"> Espaço Total Disponivel</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-6 col-md-offset-3">

        <form action="{url}configuracao/save_storage" method="POST" enctype="application/x-www-form-urlencoded">
            {espaco}
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <label for="quant_desc">Espaço total (em Bytes)</label>
                    <input type="text" name="espaco" id="espaco" value="{storage_size}" class="form-control" placeholder="Ex: 10219472389709"/>
                    <?=form_error('quant_desc');?>
                </div>
            </div>

            <div class="margin-1em">
                <button class="btn btn-success pull-right" {disabled}>Alterar</button>
            </div>
            {/espaco}
        </form>
    </div>

</div>
