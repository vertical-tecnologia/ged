
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>
<div class="row table-content">
    <div class="col-sm-12 col-md-12 col-lg-12">
        {contato}
        <form action="{url}save_contatos/" method="POST" enctype="application/x-www-form-urlencoded">
        <div class="formSep">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="email_empresa">Email </label>
                        <input type="text" name="email_empresa" id="email_empresa" value="{email_empresa}" maxlength="155" class="form-control" placeholder="example@email.com"/>
                        <?=form_error('email_empresa');?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="telefone_empresa">Telefone</label>
                        <input type="text" name="telefone_empresa" id="telefone_empresa" value="{telefone_empresa}" maxlength="15" class="form-control sp_celphones" placeholder=""/>
                        <?=form_error('telefone_empresa');?>
                    </div>
                </div>

                <input type="hidden" name="contato_principal" id="contato_principal" value="1"/>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="hidden" value="save" name="action" id="action"/>
                    <button class="btn btn-success pull-right" type="submit">Salvar</button>
                </div>
            </div>
        </form>
        {/contato}
    </div>
</div>