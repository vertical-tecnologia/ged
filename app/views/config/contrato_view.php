<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao/contrato"> <i class="fa fa-file"></i> Contrato</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12 col-lg-12">

        <form action="{url}configuracao/save_contrato/" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-9">
                        <textarea name="contrato" id="contrato">{contrato}</textarea>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <h4>Variaveis Disponiveis</h4>
                        <p class="well">Para o auto-preenchimento automático do contrato você pode usar as seguintes variáveis para substituir as marcações pelos valores cadastrados no banco de dados.
                            <br>
                            Use o seguinte formato:
                            <br>
                            <strong>{</strong>nome_da_variavel<strong>}</strong>
                        </p>

                        <ul class="list-unstyled">
                            <li>contato_valor</li>
                            <li>endereco_logradouro</li>
                            <li>endereco_num</li>
                            <li>endereco_bairro</li>
                            <li>endereco_cep</li>
                            <li>endereco_estado</li>
                            <li>endereco_cidade</li>
                            <li>endereco_complemento</li>
                            <li>endereco_alias</li>
                            <li>empresa_razao</li>
                            <li>empresa_fantasia</li>
                            <li>empresa_cnpj</li>
                            <li>empresa_ie</li>
                            <li>data_adesao</li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                    <br/>
                    <input type="hidden" value="save" name="action" id="action"/>
                    <button class="btn btn-success pull-right" type="submit">Atualizar Contrato</button>
                </div>
            </div>
        </form>
    </div>
</div>
