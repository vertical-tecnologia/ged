<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao/contrato"> <i class="fa fa-file"></i> Contrato</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12 col-lg-12">


            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {contrato}
                    </div>
            </div>
    </div>
</div>
