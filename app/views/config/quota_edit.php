{user}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao" class="ext_disabled"> <i class="fa fa-cog"></i> Configuraçao</a>
        </li>
        <li>
            <a href="{url}configuracao/quota" class="ext_disabled"> <i class="fa fa-hdd-o"></i> Cota</a>
        </li>
        <li>
            <a href="{url}configuracao/quota_edit/{user_id}" class="ext_disabled"> <i class="fa fa-edit"></i> Editar Cota</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Definir Cota para o usuario {user_fullname}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">

        <form action="{url}configuracao/quota_user" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="formSep">
                <input type="hidden" id="user_id" name="user_id" value="{user_id}">

                <div class="col-m-12 col-md-6">

                    <div class="margin-top-1em">
                        <table class="table">
                            <tr>
                                <td>Cota individual</td>
                                <td>
                                    <input type="text" class="form-control uppercase" name="quota_user" id="quota_user" placeholder="Ex: 100MB">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-m-12 col-md-6">
                    <h3>Valores Aceitos</h3>
                    <ul>
                        <li>&tilde; MB</li>
                        <li>&tilde; GB</li>
                        <li>&tilde; TB</li>
                    </ul>

                    <div class="well">Qualquer valor númerico entre maior que zero sequido de um sufixo da lista acima. </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}configuracao/quota" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success" {disabled}>Salvar</button>
                </div>
            </div>

        </form>
    </div>
</div>
{/user}