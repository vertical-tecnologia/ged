<a id="termos" name="termos"></a>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>
<div class="row table-content">
    <div class="col-sm-12 col-md-12 col-lg-12">
        {contrato}
        <form action="{url}save_contrato/" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="well">
                            <a id="termos" name="termos"></a>
                            {modelo_contrato}

                            <div class="well">
                                {_plan}
                                <a id="plano" name="plano"></a>
                                <h3>Plano: {plan_name} <span class="pull-right">R$ {plan_price}</span></h3>
                                {/_plan}
                            </div>

                            <h3><strong>Serviços Contratados</strong></h3>
                            <a name="services" id="services"></a>
                            <ul class="list-group">
                                {___services}
                                <li class="list-group-item">{service_name}</li>
                                {/___services}
                            </ul>

                        </div>
                       <textarea class="hidden" name="contrato_text" readonly="readonly">{modelo_contrato}
                            <div class="well">
                                {_plan}
                                <a id="plano" name="plano"></a>
                                <h3>Plano: {plan_name} <span class="pull-right">R$ {plan_price}</span></h3>
                                {/_plan}
                            </div>

                            <h3><strong>Serviços Contratados</strong></h3>
                            <a name="services" id="services"></a>
                            <ul class="list-group">
                                {___services}
                                <li class="list-group-item">{service_name}</li>
                                {/___services}
                            </ul>
                       </textarea>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 no-print">
                        <ul class="affix list-unstyled">
                            <li>
                                <button type="button" class="btn btn-warning" onclick="window.print()"><i class="fa fa-print"> imprimir</i></button>
                            </li>
                            <li>
                                <a href="#termos">Termos do contrato</a>
                            </li>
                            <li>
                                <a href="#plano">Plano</a>
                            </li>
                            <li>
                                <a href="#services">Serviços</a>
                            </li>
                        </ul>
                    </div>


                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div>
                        <input type="checkbox" id="accept" name="accept" {checked}/>
                        <label for="accept" class="checkbox"> Aceito os termos do contrato </label>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                    <br/>
                    <input type="hidden" value="save" name="action" id="action"/>
                    <button class="btn btn-success pull-right" type="submit">Começar a usar</button>
                </div>
            </div>
        </form>
        {/contrato}
    </div>
</div>
