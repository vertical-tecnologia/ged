<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/configuracao/executavel"> <i class="fa fa-windows"></i> Configurar executavel</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Instruções de Download</h3>
    </div>
    <form action="{url}configuracao/save_executavel" method="post" enctype="multipart/form-data">
    <div class="col-sm-12 col-md-8">
        <textarea name="instrucoes" id="instrucoes" class="form-control">{instrucoes}</textarea>
    </div>
    <div class="col-sm-12 col-md-4">
        <ul class="list-group pull-right margin-all-3em">
            <li class="list-group-item">
                <label for="path">Windows</label>
                <input type="file" name="path" id="path" class="form-control">
            </li>
            <li class="list-group-item">
                <label for="linux_path">Linux</label>
                <input type="file" name="linux_path" id="linux_path" class="form-control">
            </li>
            <li class="list-group-item">
                <label for="macos_path">Mac OS</label>
                <input type="file" name="macos_path" id="macos_path" class="form-control">
            </li>
        </ul>
        <div class="margin-all-3em">
            <button class="btn btn-success pull-right">Salvar</button>
        </div>
    </div>
    </form>
</div>
