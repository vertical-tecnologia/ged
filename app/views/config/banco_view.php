<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao/banco"> <i class="fa fa-bank"></i> Dados Bancários</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}configuracao/save_banco" method="POST" enctype="application/x-www-form-urlencoded">
            {bank}
            <div class="formSep">

                <div class="col-m-12 col-md-6">
                    <input type="hidden" name="bank_id" id="bank_id" value="{bank_id}"/>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="user_fullname">Apelido</label>
                            <input type="text" name="bank_alias" id="bank_alias" value="{bank_alias}" class="form-control" placeholder="Apelido"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="user_fullname">Nome do Banco</label>
                            <input type="text" name="bank_nome" id="bank_nome" value="{bank_nome}" class="form-control" placeholder="Nome do banco"{readonly}/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="bank_codigo">Código do Banco</label>
                            <input type="text" name="bank_codigo" id="bank_codigo" value="{bank_codigo}" class="form-control" placeholder="Codigo do banco"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-10">
                            <label for="bank_conta">Número da Conta</label>
                            <input type="text" name="bank_conta" id="bank_conta" value="{bank_conta}" class="form-control" placeholder="Numero da Conta"/>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <label for="bank_cdigito">Dígito</label>
                            <input type="text" name="bank_cdigito" id="bank_cdigito" value="{bank_cdigito}" class="form-control" placeholder="Digito"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-10">
                            <label for="bank_acodigo">Agência</label>
                            <input type="text" name="bank_acodigo" id="bank_acodigo" value="{bank_acodigo}" class="form-control" placeholder="Agencia"/>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <label for="bank_adigito">Dígito</label>
                            <input type="text" name="bank_adigito" id="bank_adigito" value="{bank_adigito}" class="form-control" placeholder="Digito"/>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="bank_carteira">Carteira</label>
                            <input type="text" name="bank_carteira" id="bank_carteira" value="{bank_carteira}" class="form-control" placeholder="Carteria"{readonly}/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label for="bank_nosso_ini">Nosso número inicial</label>
                            <input type="text" name="bank_nosso_ini" id="bank_nosso_ini" value="{bank_nosso_ini}" class="form-control" placeholder="Nosso numero inicial"/>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label for="bank_nosso_fim">Nosso número final</label>
                            <input type="text" name="bank_nosso_fim" id="bank_nosso_fim" value="{bank_nosso_fim}" class="form-control" placeholder="Nosso numero final"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="bank_nosso_atual">Nosso número atual</label>
                            <input type="text" name="bank_nosso_atual" id="bank_nosso_atual" value="{bank_nosso_atual}" class="form-control" readonly/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label for="bank_contrato">N° do contrato</label>
                            <input type="text" name="bank_contrato" id="bank_contrato" value="{bank_contrato}" class="form-control" placeholder="Nosso numero inicial"/>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label for="bank_responsavel">Responsável</label>
                            <input type="text" name="bank_responsavel" id="bank_responsavel" value="{bank_responsavel}" class="form-control" placeholder="Nosso numero final"/>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}usuario" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/bank}
        </form>
    </div>
</div>
