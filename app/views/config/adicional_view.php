<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao" class="ext_disabled"> <i class="fa fa-cog"></i> Configuração</a>
        </li>
        <li>
            <a href="{url}configuracao/adicional" class="ext_disabled"> <i class="fa fa-hdd-o"></i> Espaço Adicional</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading"> Contratar espaço adicional?</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">

        <form action="{url}configuracao/save_adicional" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="box">
                <script>servicos = {quantidades}</script>

                <div class="col-m-12 col-md-12">
                    <p class="sepH_a">Armazenamento: <strong class="val_id_servico" id="val_id_servico"></strong> <strong id="val_valor_id_servico"></strong> </p>

                    <div class="ui_slider1"></div>
                    <input type="text" id="slider_id_servico" data-slider="true" value="" data-slider-values="{val}" data-slider-snap="true" />
                    <input type="hidden" id="quant_size" name="quant_size" />
                    <input type="hidden" id="quant_id" name="quant_id" />
                    <input type="hidden" id="quant_preco" name="quant_preco" />
                </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <br><br><br>
            <div class="margin-1em">
                    <a href="{url}" class="btn btn-danger pull-right">Cancelar</a>
                    <button class="btn btn-success pull-right" {disabled}>Contratar</button>
            </div>
        </form>
    </div>
</div>
