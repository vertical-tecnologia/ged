<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        {empresa}
        <form action="{url}admin/company" method="POST" enctype="multipart/form-data">
            <div class="formSep">

                <div class="col-m-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_cnpj">CNJP</label>
                            <input type="text" name="empresa_cnpj" id="empresa_cnpj" onblur="validarCNPJ(this, true)" onkeyup="validarCNPJ(this)" value="{empresa_cnpj}" class="form-control cnpj" {disabled} {readonly}/>
                            <?=form_error('empresa_cnpj');?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_razao">Razão Social</label>
                            <input type="text" name="empresa_razao" id="empresa_razao" value="{empresa_razao}" class="form-control" {disabled}/>
                            <?=form_error('empresa_razao');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_fantasia">Nome Fantasia</label>
                            <input type="text" name="empresa_fantasia" id="empresa_fantasia" value="{empresa_fantasia}" class="form-control" {disabled}/>
                            <?=form_error('empresa_fantasia');?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_ie">IE</label>
                            <input type="text" name="empresa_ie" id="empresa_ie" value="{empresa_ie}" class="form-control text-uppercase" {disabled} {readonly}/>
                            <?=form_error('empresa_ie');?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_segmento_id">Segmento</label>

                            <select name="empresa_segmento_id" id="empresa_segmento_id" class="form-control chosen-select" data-placeholder="Selecione um Segmento" {disabled} {readonly}>
                                <option></option>
                                {segmento}
                                <option value="{segmento_id}">{segmento_name}</option>
                                {/segmento}
                            </select>
                            <?=form_error('empresa_segmento_id');?>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label for="empresa_user_id">Usuário Responsável</label>
                            <input type="hidden" id="empresa_user_id" name="empresa_user_id" value="{user_id}"/>
                            <input type="text" value="{user_name}" class="form-control" disabled/>
                        </div>

                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="hidden" value="save" name="action" id="action"/>
                    <a class="btn btn-info pull-left" href="<?php echo base_url();?>profile">< Voltar</a>
                    <button class="btn btn-info pull-right" {disabled} type="submit">Avançar ></button>
                </div>
            </div>
            {/empresa}
        </form>
    </div>
</div>
<script>

    function validarCNPJ(element, validateNow){

        var cnpj = element.value;


        if(cnpj.length < 18 && validateNow == false)
            return;

        if(doValidation(cnpj) == false){
            element.focus();
            if(element.nextSibling.nextSibling.classList.contains('error')){
                element.nextSibling.nextSibling.innerHTML = "CNPJ inválido.";
            }
            element.parentNode.classList.add('has-error', 'error');
        } else {
            element.parentNode.classList.remove('has-error', 'error');
            element.parentNode.removeChild(element.nextSibling.nextSibling);

        }

    }

    function doValidation(cnpj) {


        cnpj = cnpj.replace(/[^\d]+/g,'');

        if(cnpj == '') return false;

        if (cnpj.length != 14)
            return false;

        // LINHA 10 - Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")
            return false; // LINHA 21

        // Valida DVs LINHA 23 -
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false; // LINHA 49

        return true; // LINHA 51

    }
</script>