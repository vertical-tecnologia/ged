<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}configuracao/contrato"> <i class="fa fa-file"></i> Contrato</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">{page_title}</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12 col-lg-12">

        <form action="{url}configuracao/save_home/" method="POST" enctype="application/x-www-form-urlencoded">
            {home_data}
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <label for="home_title">Titulo da Home Page (max 80 caracteres)</label>
                        <input type="text" value="{home_title}" class="form-control" name="home_title" id="home_title" maxlength="80">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <label for="home_title">Descriçao (max 255 caracteres)</label>
                        <textarea name="home_subtitle" id="home_subtitle" class="form-control"maxlength="255">{home_subtitle}</textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                    <br/>
                    <input type="hidden" value="save" name="action" id="action"/>
                    <button class="btn btn-success pull-right" type="submit">Atualizar Contrato</button>
                </div>
            </div>
            {/home_data}
        </form>
    </div>
</div>
