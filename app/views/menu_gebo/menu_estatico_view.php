
<?php
$userdata = $this->session->userdata('USER_ROLE');
 ?>
<?php $userdata = $this->session->userdata('USER_ROLE'); if($userdata != 'Indefinido'): ?>
<a href="javascript:void(0)" class="sidebar_switch on_switch bs_ttip" data-placement="auto right" data-viewport="body" title="Hide Sidebar">Sidebar switch</a>
<?php endif;?>
<div class="sidebar">
	<div class="sidebar_inner_scroll">
		<div class="sidebar_inner">

			<form action="{url}arquivos/" class="input-group input-group-sm" method="get">
				<?php if (isset($server_id) && $server_id) : ?> 
					<input name="server" type="hidden" value="<?php echo $server_id;?>">
				<?php endif; ?>
				<input autocomplete="off" name="q" id="q" class="search_query form-control input-sm" size="16" placeholder="Procurar..." type="text">
				<span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button></span>
			</form>

			<?php $userdata = $this->session->userdata('USER_ROLE'); if($userdata == ''): ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a class="link-no-accordion-toggle">
						<i class="glyphicon glyphicon-file"></i> Arquivos
					</a>
				</div>
				<div class="accordion-body collapse in" id="panelArquivos">
					<div class="panel-body">
						<ul class="nav nav-pills nav-stacked">
							<li class="active">
								<a class="ext_disabled"> Compartilhado Comigo<b class=""></b></a>
							</li>
							<li>
								<a href="{url}cadastro/" class="ext_disabled"> Cadastre-se no Ged</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php endif;?>

			<?php $userdata = $this->session->userdata('USER_ROLE'); if($userdata != '' && $userdata != 'Indefinido'): ?>

			<div id="side_accordion" class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading<?=in_array($this->router->fetch_class() , array('', 'admin')) ? ' active' : ''?>">
						<a href="{url}"  class="link-no-accordion-toggle ext_disabled">
							<i class="glyphicon glyphicon-home"></i> Home
						</a>
					</div>
				</div>
				<?php if ($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO' && $this->session->userdata('USER_ROLE') != 'SUPORTE'): ?>
				<div class="panel panel-default">
					<div class="panel-heading <?=in_array($this->router->fetch_class() , array('arquivos')) && !$this->input->get('server') ? ' active' : ''?>">
						<a href="{url}arquivos" class="link-no-accordion-toggle">
							<i class="glyphicon glyphicon-file"></i> Arquivos
						</a>
					</div>

					<div class="accordion-body collapse<?=in_array($this->router->fetch_class() , array('arquivos')) && !$this->input->get('server') ? ' in' : ''?>" id="panelArquivos">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('index', 'folder', 'view')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos"> Meus Arquivos<b class=""></b></a>
								</li>
								<ul style="list-style-type: none; margin-left:-25px;">
								<?php foreach($directories as $dir) : ?>
									<li>
										<a data-id="<?php echo $dir['id'];?>" data-server="" class="menu-explorer" href="#">
											<i class="<?php echo isset($subHtmlDirs[$dir['id']]) ? 'fa fa-caret-down' : 'fa fa-caret-right';?>" aria-hidden="true"></i>
										</a>
										<a class="ext_disabled" href="{url}arquivos/folder/<?php echo $dir['hash'];?>">
											<i class="fa fa-folder" aria-hidden="true"></i>
											<?php echo $dir['original_name'];?>
											<b class=""></b>
										</a>
									</li>
									<?php echo isset($subHtmlDirs[$dir['id']]) ? $subHtmlDirs[$dir['id']] : null;?>
								<?php endforeach;?>
								</ul>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('galeria')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> Galeria<b class=""></b></a>
								</li>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('compartilhamento')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/compartilhamento"><i class="fa fa-share-alt" aria-hidden="true"></i> Compartilhamento<b class=""></b></a>
								</li>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('arquivos_recentes')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/recentFiles"><i class="fa fa-file-text-o" aria-hidden="true"></i> Arquivos Recentes<b class=""></b></a>
								</li>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && in_array($this->uri->segment(2) , array('lixeira')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/lixeira"><i class="fa fa-trash-o" aria-hidden="true"></i> Lixeira<b class=""></b></a>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<?php foreach ($servers as $server): ?>

				<div class="panel panel-default">
					<div class="panel-heading<?=in_array($this->router->fetch_class() , array("arquivos/?server={$server['server_id']}")) && $this->input->get('server') == $server['server_id'] ? ' active' : '';?>">
						<a href="{url}arquivos<?php echo isset($server['server_id']) ? "/?server={$server['server_id']}" : null;?>" class="link-no-accordion-toggle">
							<i class="glyphicon glyphicon-hdd"></i> Servidor - <?php echo $server['description'];?>
						</a>
					</div>

					<div class="accordion-body collapse<?= in_array($this->router->fetch_class() , array("arquivos")) && $this->input->get('server') == $server['server_id'] ? ' in' : ''?>" id="panelArquivos">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('index', 'folder', 'view')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos<?php echo isset($server['server_id']) ? "/?server={$server['server_id']}" : null;?>"> Meus Arquivos<b class=""></b></a>
								</li>
								<ul style="list-style-type: none; margin-left:-25px;">
								<?php foreach($directories as $dir) : ?>
									<li>
										<a data-id="<?php echo $dir['id'];?>" data-server="<?php echo $server['server_id'];?>" class="menu-explorer" href="#">
											<i class="<?php echo isset($subHtmlDirs[$dir['id']]) ? 'fa fa-caret-down' : 'fa fa-caret-right';?>" aria-hidden="true"></i>
										</a>
										<a class="ext_disabled" href="{url}arquivos/folder/<?php echo "{$dir['hash']}/?server={$server['server_id']}";?>"> 
											<i class="fa fa-folder" aria-hidden="true"></i>
											<?php echo $dir['original_name'];?><b class=""></b>
										</a>
									</li>
									<?php echo isset($subHtmlDirs[$dir['id']]) ? $subHtmlDirs[$dir['id']] : null;?>
								<?php endforeach;?>
								</ul>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('galeria')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/galeria<?php echo isset($server['server_id']) ? "/?server={$server['server_id']}" : null;?>"><i class="fa fa-picture-o" aria-hidden="true"></i> Galeria<b class=""></b></a>
								</li>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('compartilhamento')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/compartilhamento<?php echo isset($server['server_id']) ? "/?server={$server['server_id']}" : null;?>"><i class="fa fa-share-alt" aria-hidden="true"></i> Compartilhamento<b class=""></b></a>
								</li>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && $this->uri->segment(2) != 'lixeira' && in_array($this->router->fetch_method() , array('arquivos_recentes')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/recentFiles<?php echo isset($server['server_id']) ? "/?server={$server['server_id']}" : null;?>"><i class="fa fa-file-text-o" aria-hidden="true"></i> Arquivos Recentes<b class=""></b></a>
								</li>

								<li class="<?=in_array($this->router->fetch_class() , array('arquivos')) && in_array($this->uri->segment(2) , array('lixeira')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}arquivos/lixeira<?php echo isset($server['server_id']) ? "/?server={$server['server_id']}" : null;?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Lixeira<b class=""></b></a>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<?php endforeach;?>
				<?php endif;?>

				<div class="panel panel-default">
					<div class="panel-heading<?=$this->router->fetch_class() == 'mensagens' ? ' active' : ''?>">
						<a href="{url}mensagens/"  class="link-no-accordion-toggle ext_disabled">
							<i class="glyphicon glyphicon-envelope"></i> Mensagens
						</a>
					</div>
				</div>

				<?php if (in_array($this->session->userdata('USER_ROLE') ,array('SUPERUSUARIO', 'CLIADMIN' , 'SUPORTE'))) : ?>
					<div class="panel panel-default">
						<div class="panel-heading<?=$this->router->fetch_class() == 'empresa' ? ' active' : ''?>">
							<a href="{url}empresa/"  class="link-no-accordion-toggle ext_disabled">
								<i class="glyphicon glyphicon-tasks"></i> Empresas
							</a>
						</div>
					</div>
				<?php endif;?>

				<?php if (in_array($this->session->userdata('USER_ROLE') ,array('SUPERUSUARIO', 'CLIADMIN', 'USUARIO', 'SUPORTE'))) : ?>
				<div class="panel panel-default">
					<div class="panel-heading<?=in_array($this->router->fetch_class() , array('usuario', 'grupo')) ? ' active' : ''?>">
						<a href="#panelClients" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
							<i class="glyphicon glyphicon-user"></i>
							Usuarios
						</a>
					</div>
					<div class="accordion-body collapse<?=in_array($this->router->fetch_class() , array('usuario', 'grupo', 'cliente')) ? ' in' : ''?>" id="panelClients">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<?php if (in_array($this->session->userdata('USER_ROLE') ,array('SUPERUSUARIO', 'CLIADMIN', 'SUPORTE'))) : ?>
								<li class="<?=in_array($this->router->fetch_class() , array('usuario')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}usuario"> Usuários<b class=""></b></a>
								</li>
								<?php if (in_array($this->session->userdata('USER_ROLE'),array('SUPERUSUARIO'))): ?>
								<li class="<?=in_array($this->router->fetch_class() , array('cliente')) ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}cliente"> Clientes<b class=""></b></a>
								</li>
								<?php endif;?>
								<?php endif;?>
								<?php if (in_array($this->session->userdata('USER_ROLE'),array('USUARIO', 'CLIADMIN','CLIPESSOA'))): ?>
								<li class="<?=$this->router->fetch_class() == 'grupo' ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}grupo/"> Grupos <b class=""></b></a>
								</li>
								<?php endif;?>

							</ul>
						</div>
					</div>
				</div>
				<?php endif;?>

				<?php if ($this->session->userdata('id_usuario_tipo') == 2): ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a href="#panelAccount" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
								<i class="glyphicon glyphicon-th"></i> Minha Conta
							</a>
						</div>
						<div class="accordion-body collapse" id="panelAccount">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked">
									<li class="">
										<a class="ext_disabled" href="{url}cliente/editar/<?=$this->session->userdata('id_cliente')?>"> Conta <b class=""></b></a>
									</li>
									<li class="">
										<a class="ext_disabled" href="{url}lancamento/listar"> Minha Fatura<b class=""></b></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				<?php endif;?>

				<?php if (in_array($this->session->userdata('USER_ROLE') ,array('SUPERUSUARIO', 'CLIADMIN', 'SUPORTE'))) : ?>
					<div class="panel panel-default">
						<div class="panel-heading<?=$this->router->fetch_class() == 'log' ? ' active' : ''?>">
							<a href="{url}log/"  class="link-no-accordion-toggle ext_disabled">
								<i class="glyphicon glyphicon-list-alt"></i> Log
							</a>
						</div>
					</div>
				<?php endif;?>

				<?php if (in_array($this->session->userdata('USER_ROLE'),array('SUPERUSUARIO', 'CLIADMIN','CLIPESSOA', 'SUPORTE'))): ?>
					<div class="panel panel-default">
						<div class="panel-heading<?=$this->router->fetch_class() == 'faturamento' ? ' active' : ''?>">
							<a href="#panelFinnace" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
								<i class="glyphicon glyphicon-usd"></i> Faturamento
							</a>
						</div>
						<div class="accordion-body collapse<?=in_array($this->router->fetch_class(), array('faturamento')) == true ? ' in' : ''?>" id="panelFinnace">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked">
									<li class="<?=$this->router->fetch_class() == 'faturamento' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}faturamento/"> Faturamento <b class=""></b></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				<?php endif;?>



				<?php if (in_array($this->session->userdata('USER_ROLE'),array('SUPERUSUARIO', 'CLIADMIN','CLIPESSOA'))): ?>

				<div class="panel panel-default">
					<div class="panel-heading <?=in_array($this->router->fetch_class() , array('configuracao', 'permissao','acao', 'modulo', 'servidor', 'segmento', 'segmento_status', 'plano', 'plano_status', 'servico', 'servico_status','usuario_status', 'usuario_tipo')) ? ' active' : ''?>">
					<a href="#panelSettings" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
							<i class="glyphicon glyphicon-cog"></i> Configurações
						</a>
					</div>
					<div class="accordion-body collapse<?=in_array($this->router->fetch_class(), array('contato_tipo', 'configuracao','permissao','acao', 'modulo', 'servidor', 'segmento', 'segmento_status', 'plano', 'plano_status', 'servico', 'servico_status','usuario_status','grupo', 'usuario_tipo')) == true ? ' in' : ''?>" id="panelSettings">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">

								<?php if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') : ?>
									<li class="nav-header">Home</li>
									<li class="<?=$this->router->fetch_method() == 'contrato' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/home"> Configurar Home Page<b class=""></b></a>
									</li>
									<li class="nav-header">Contratos</li>
									<li class="<?=$this->router->fetch_method() == 'contrato' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/contrato"> Modelos de Contrato<b class=""></b></a>
									</li>
									<li class="nav-header">Armazenamento</li>
									<li class="<?=$this->router->fetch_method() == 'armazenamento' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/armazenamento"> Quantidades de armazenamento<b class=""></b></a>
									</li>

									<li class="<?=$this->router->fetch_method() == 'espaco' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/espaco"> Espaço disponível<b class=""></b></a>
									</li>

									<li class="nav-header">Dados Bancários</li>
									<li class="<?=$this->router->fetch_method() == 'banco' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/banco"> Boleto<b class=""></b></a>
									</li>

									<li class="nav-header">Executável</li>
									<li class="<?=$this->router->fetch_method() == 'executavel' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/executavel"> Configuar Executável<b class=""></b></a>
									</li>
								<?php endif;?>

								<?php if ($this->session->userdata('USER_ROLE') == 'CLIADMIN') : ?>
									<li class="nav-header">Espaço e Uso</li>
									<li class="<?=$this->router->fetch_method() == 'quota' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}configuracao/quota"> Cota de Uso<b class=""></b></a>
									</li>
									<!-- <li class="nav-header">Arquivo XML</li> -->
									<!-- <li class="<?php //$this->router->fetch_method() == 'xml' ? 'active' : '';?>"> -->
										<!-- <a class="ext_disabled" href="{url}configuracao/xml">Cadastro Tags XML<b class=""></b></a> -->
									<!-- </li> -->
								<?php endif;?>
								<?php if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') : ?>
									<li class="nav-header">Contatos</li>
									<li class="<?=$this->router->fetch_class() == 'contato_tipo' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}contato_tipo/"> Tipos de contato<b class=""></b></a>
									</li>

									<li class="nav-header">Usuários e Grupos</li>
									<li class="<?=$this->router->fetch_class() == 'usuario_status' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}usuario_status/"> Status de Usuários<b class=""></b></a>
									</li>
									<li class="<?=$this->router->fetch_class() == 'usuario_tipo' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}usuario_tipo/"> Tipos de Usuários<b class=""></b></a>
									</li>

									<li class="nav-header">Serviços</li>
									<li class="<?=$this->router->fetch_class() == 'servico_status' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}servico_status/"> Status de Serviço<b class=""></b></a>
									</li>
									<li class="<?=$this->router->fetch_class() == 'servico' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}servico/"> Serviços<b class=""></b></a>
									</li>
									<li class="nav-header">Planos</li>
									<li class="<?=$this->router->fetch_class() == 'plano_status' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}plano_status/"> Status de Planos<b class=""></b></a>
									</li>
									<li class="<?=$this->router->fetch_class() == 'plano' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}plano/"> Planos<b class=""></b></a>
									</li>

									<li class="nav-header">Segmentos</li>
									<li class="<?=$this->router->fetch_class() == 'segmento_status' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}segmento_status/"> Status de Segmentos<b class=""></b></a>
									</li>
									<li class="<?=$this->router->fetch_class() == 'segmento' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}segmento/"> Segmentos<b class=""></b></a>
									</li>
								<?php endif;?>
								<?php if (in_array($this->session->userdata('USER_ROLE'), ['SUPERUSUARIO', 'CLIADMIN'])) : ?>
									<li class="nav-header">Servidores</li>
									<li class="<?=$this->router->fetch_class() == 'servidor' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}servidor/"> Servidores<b class=""></b></a>
									</li>
								<?php endif;?>
								<?php if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') : ?>
									<li class="nav-header">Permissões de Acesso</li>
									<li class="<?=$this->router->fetch_class() == 'modulo' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}modulo/"> Módulos<b class=""></b></a>
									</li>
									<li class="<?=$this->router->fetch_class() == 'acao' ? 'active' : ''?>">
									<a class="ext_disabled" href="{url}acao/"> Ações<b class=""></b></a>
									</li>
									<li class="<?=$this->router->fetch_class() == 'permissao' ? 'active' : ''?>">
										<a class="ext_disabled" href="{url}permissao/"> Permissões<b class=""></b></a>
									</li>

								<?php endif;?>
							</ul>
						</div>
					</div>
				</div>
				<?php endif;?>


			</div>

			<div class="push"></div>
			<?php endif;?>
		</div>
		<?php $userdata = $this->session->userdata('USER_ROLE'); if($userdata != '' && $userdata != 'Indefinido' && $userdata != 'SUPORTE'): ?>
		<div class="sidebar_info">
			<ul class="list-unstyled">
				<li>
					<span class="act act-warning">{total_files}</span>
					<strong>Total de  arquivos</strong>
				</li>
				<li>
					<span class="act act-success">{size_files}</span>
					<strong>Armazenamento Utilizado</strong>
				</li>
			<?php if (in_array($this->session->userdata('USER_ROLE'),array('SUPERUSUARIO'))): ?>
				<li>
					<span class="act act-success">{total_storage}</span>
					<strong>Armazenamento Total</strong>
					<div class="progress">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{total_percent_free}" aria-valuemin="0" aria-valuemax="100" style="width: {total_percent_free}%;">
							{total_percent_free}%
						</div>
					</div>
				</li>
			<?php endif;?>
			<?php if (in_array($this->session->userdata('USER_ROLE'),array('USUARIO', 'CLIADMIN','CLIPESSOA'))): ?>
				<li>
					<span class="act act-success">{quota_size}</span>
					<strong>Cota {shared}</strong>
				</li>

				<li>
					<span class="act act-success">{free_space}</span>
					<strong>Espaço livre individual{shared}</strong>
				</li>
				<?php if (in_array($this->session->userdata('USER_ROLE'),array('CLIADMIN','CLIPESSOA'))): ?>
				<li>
					<span class="act act-success">{free_total_space}</span>
					<strong>Espaço livre total</strong>
				</li>
				<?php endif;?>
				<li>
					<strong>Utilizado {shared}</strong>
					<div class="progress">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{percent_free}" aria-valuemin="0" aria-valuemax="100" style="width: {percent_free}%;">
							{percent_free}%
						</div>
					</div>
				</li>
			<?php endif;?>
			</ul>
			<?php //if (in_array($this->session->userdata('USER_ROLE'),array('CLIADMIN'))): ?>
			<!-- <a href="{url}configuracao/adicional" class="btn">Contratar espaço adicional</a> -->
			<?php //endif;?>
		</div>
		<?php endif;?>
	</div>

</div>
