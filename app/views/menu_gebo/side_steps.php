
<div class="sidebar">
    <div class="sidebar_inner_scroll">
        <div class="sidebar_inner">
            <div class="margin-top-1em">

                <ul class="list-group steps">
                    <li><h3>Configurações Iniciais</h3></li>
                    <li class="list-group-item<?=$config_item == 'empresa' ? ' active' : ''?>">Configurar Empresa</li>
                    <li class="list-group-item<?=$config_item == 'endereco' ? ' active' : ''?>">Endereço</li>
                    <li class="list-group-item<?=$config_item == 'contato' ? ' active' : ''?>">Telefone e email</li>
                    <li class="list-group-item<?=$config_item == 'contrato' ? ' active' : ''?>">Termos de Uso</li>
                </ul>
            </div>
        </div>
    </div>
</div>

