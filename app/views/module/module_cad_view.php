<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/modulo"> <i class="fa fa-user"></i> Módulos</a>
        </li>
        <li>
            <a href="{url}/modulo/cadastro"> <i class="fa fa-user"></i> Cadastro </a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Módulo</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}modulo/save" method="POST" enctype="application/x-www-form-urlencoded">
            {module}
            <input type="hidden" name="module_id" id="module_id" value="{module_id}"/><br/>
            <div class="formSep">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="module_name">Módulo</label><br/>
                        <input type="text" name="module_name" id="module_name" value="{module_name}" class="form-control" autofocus/>
                        <?=form_error('module_name');?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}modulo" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/module}
        </form>
    </div>
</div>
