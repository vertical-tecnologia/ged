<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/module"> <i class="fa fa-user"></i> Módulos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Módulos</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}modulo/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Módulo</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Módulo</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {module}
            <tr>
                <td>{module_id}</td>
                <td>{module_name}</td>
                <td class="text-right">
                    <a class="ext_disabled" href="{url}modulo/excluir/{module_id}">excluir</a>
                </td>
            </tr>
            {/module}
            </tbody>
        </table>
    </div>
</div>
