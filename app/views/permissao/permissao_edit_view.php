{user_type}
<div id="jCrumbs" class="breadCrumb action">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}permissao"> <i class="fa fa-lock"></i> Permissões</a>
        </li>
        <li>
            <a href="{url}permissao/editar/{type_id}"> <i class="fa fa-edit"></i> Editar Permissão</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Permissões para <strong>{type_desc}</strong></h3>
    </div>
</div>
<input type="hidden" id="type_id" value="{type_id}"/>
{/user_type}
<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="heading">Módulos</h4>
                {module}
                <div>
                    <input value="{module_id}" name="module_id" id="module_id-{module_id}" class="module_id" type="radio" data-href="{url}acao/load_action/{module_id}">
                    <label class="radio" for="module_id-{module_id}">
                    {module_name}
                    </label>
                </div>
                {/module}
            </div>
            <div class="col-sm-12 col-md-6">
                <h4 class="heading">Permissões</h4>
                <div id="permission-box">

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="pull-right">
                    <button value="save" id="button-save" class="btn btn-success" data-href="{url}modulo/save_permission/">salvar</button>
                    <a class="btn btn-danger" href="{url}permissao">cancelar</a>
                </div>
            </div>
        </div>
    </div>
</div>
