<div id="jCrumbs" class="breadCrumb action">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/permissao"> <i class="fa fa-lock"></i> Permissões</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Permissões</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Grupo</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {user_type}
            <tr>
                <td>{type_id}</td>
                <td>{type_desc}</td>
                <td class="text-right">
                    <a class="ext_disabled" href="{url}permissao/editar/{type_id}">gerenciar permissões</a>
                </td>
            </tr>
            {/user_type}
            </tbody>
        </table>
    </div>
</div>
