{cliente}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/cliente"> <i class="fa fa-cubes"></i> Clientes</a>
        </li>
        <li>
            <a href="{url}/cliente/visualizar/{client_id}"> <i class="fa fa-list-alt"></i> Detalhes do Cliente</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Detalhes do Cliente</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        {button_add}
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">

        <legend>Empresa</legend>
        <table class="table table-condensed table-responsive table-user-information">
            <tbody>
            <tr>
                <td>Razao Social</td>
                <td>{empresa_razao}</td>
            </tr>

            <tr>
                <td>Nome Fantasia</td>
                <td>{empresa_fantasia}</td>
            </tr>

            <tr>
                <td>CNPJ</td>
                <td>{empresa_cnpj}</td>
            </tr>
            <tr>
                <td>Data de Cadastro</td>
                <td>{empresa_register_date}</td>
            </tr>

            </tbody>
        </table>

        <legend>Plano</legend>
        <table class="table table-condensed table-responsive table-user-information">
            <tbody>
            <tr>
                <td>Plano:</td>
                <td>{plan_name}</td>
            </tr>
            <tr>
                <td>Valor:</td>
                <td>R$ {plan_price}</td>
            </tr>
            <tr>
                <td>Máximo de Usuários</td>
                <td>{plano_max_users}</td>
            </tr>
            </tbody>
        </table>

        <legend>Servidor</legend>
        <table class="table table-condensed table-responsive table-user-information">
            <tbody>
                <tr>
                    <td>Tipo do Servidor</td>
                    <td>{server_type_desc}</td>
                </tr>
            </tbody>
        </table>

    </div>

</div>
{/cliente}