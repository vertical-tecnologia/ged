<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/cliente"> <i class="fa fa-cubes"></i> Clientes</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Clientes</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        {button_add}
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Empresa</th>
            <th>Plano</th>
            <th>Tipo de Servidor</th>
            <th>Cadastrado em</th>
            </thead>

            <tbody>
            {clientes}
            <tr>
                <td><a href="{url}cliente/visualizar/{client_id}">{client_id}</a></td>
                <td><a href="{url}cliente/visualizar/{client_id}">{empresa_fantasia}</a></td>
                <td><a href="{url}cliente/visualizar/{client_id}">{plan_name}</a></td>
                <td><a href="{url}cliente/visualizar/{client_id}">{server_type_desc}</a></td>
                <td><a href="{url}cliente/visualizar/{client_id}">{empresa_register_date}</a></td>
            </tr>
            {/clientes}
            </tbody>
        </table>
    </div>
</div>
