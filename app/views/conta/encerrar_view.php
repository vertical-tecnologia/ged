<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}perfil" class="ext_disabled"> <i class="fa fa-users"></i> Conta</a>
        </li>
        <li>
            <a href="{url}conta/encerrar" class="ext_disabled"> <i class="fa fa-user-times"></i> Encerrar Conta</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Encerrar minha conta.</span></h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        {usuario}
        <form action="{url}conta/finish/" method="post" enctype="multipart/form-data">
            <div class="col-sm-12 col-md-4">
                <h4>{user_fullname}</h4>

                <p>{user_email}</p>

                <p><strong>Cadastrado em:</strong></p>

                <p>{user_register_date}</p>

                <div class="well">
                    Você terá 14 dias para reativar sua conta. Para isso basta fazer login novamente.
                </div>

                <div class="clearfix"></div>
                <label for="password">
                    Digite sua senha para confirmar.
                </label>
                <input type="password" name="password" id="password" placeholder="Sua senha" class="form-control" autocomplete="off" autofocus="true"/>
            </div>

            <div class="col-sm-12 col-md-8">
                <h3>Se voce excluir sua conta os seguintes dados serão excluídos:</h3>
                <ul class="list-group">
                    <li class="list-group-item">Usuários</li>
                    <li class="list-group-item">Empresa</li>
                    <li class="list-group-item">Arquivos</li>
                    <li class="list-group-item">Mensagens</li>
                    <li class="list-group-item">Grupos</li>
                    <li class="list-group-item">Compartilhamentos</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}" class="btn btn-danger">Cancelar</a>
                    <button type="submit" class="btn btn-success">Encerrar minha conta</button>
                </div>
            </div>

        </form>
        {/usuario}
    </div>
</div>
