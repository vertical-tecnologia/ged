<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}plano" class="ext_disabled"> <i class="fa fa-list-alt"></i> Planos</a>
        </li>
        <li>
            <a href="{url}plano/cadastro" class="ext_disabled"> <i class="fa fa-edit"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Plano</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}plano/save" method="POST" enctype="application/x-www-form-urlencoded">
            {plano}
            <div class="formSep">

            <div class="col-m-12 col-md-6">
            <input type="hidden" name="plan_id" id="plan_id" value="{plan_id}"/>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 ">
                        <label for="plan_name">Nome do Plano</label>
                        <input type="text" name="plan_name" id="plan_name" value="{plan_name}" class="form-control" tabindex="1" autofocus="autofocus" placeholder="Nome do Plano"/>
                        <?=form_error('plan_name');?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <label for="plan_price">Valor do Plano</label>
                        <input type="text" name="plan_price" id="plan_price" value="{plan_price}" tabindex="-1" class="form-control" readonly/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="plan_status_id">Status do Plano</label>
                        <select name="plan_status_id" id="plan_status_id"  tabindex="4" class="form-control chosen-select">
                            <option value="">Selecione o status do Plano</option>
                            {plano_status}
                            <option value="{status_id}" {selected}>{status_name}</option>
                            {/plano_status}
                        </select>
                        <?=form_error('plan_status_id');?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label for="service_id">Serviços</label>
                        <select name="service_id[]" id="service_id" tabindex="5" data-placeholder="Selecione um serviço" class="form-control chosen-select" multiple>
                            {servico}
                            <option value="{service_id}"{selected} data-target="plan_price" data-value="{service_price}">{service_name} - R$ {service_price}</option>
                            {/servico}
                        </select>
                        <?=form_error('service_id');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 ">
                        <label for="plano_max_users">Número de Usuários</label>
                        <input type="number" name="plano_max_users" id="plano_max_users" value="{plano_max_users}" class="form-control" tabindex="6" autofocus="autofocus" placeholder="Quantidade de Usuários"/>
                        <?=form_error('plano_max_users');?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="row">

                    <div class="col-xs-12 col-sm-12">
                        <label for="plan_desc">Descrição do Plano</label>
                        <textarea type="text" name="plan_desc"  tabindex="7" id="plan_desc" class="form-control editor">{plan_desc}</textarea>
                        <?=form_error('plan_desc');?>
                    </div>

                    <div class="col-xs-12 col-sm-12">
                        <label for="server_id">Servidor</label>
                        <select name="server_id" id="server_id" tabindex="8" class="form-control chosen-select" data-placeholder="Escolha o Servidor para o Plano">
                            <option value=""></option>
                            {servidor}
                            <option value="{server_id}"{selected}>{server_type_desc}</option>
                            {/servidor}
                        </select>
                        <?=form_error('server_id');?>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}plano" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/plano}
        </form>
    </div>
</div>
