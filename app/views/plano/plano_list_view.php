<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/Plano"> <i class="fa fa-list-alt"></i> Planos</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Planos</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}plano/cadastro" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo Plano</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Nome</th>
            <th>Valor</th>
            <th>Status</th>
            <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {planos}
            <tr>
                <td>{plan_id}</td>
                <td>{plan_name}</td>
                <td>{plan_price}</td>
                <td>{status_name}</td>
                <td class="text-right">
                    <a class="ext_disabled" href="{url}plano/editar/{plan_id}">editar</a>
                    <a class="ext_disabled" href="{url}plano/excluir/{plan_id}">excluir</a>
                </td>
            </tr>
            {/planos}
            </tbody>
        </table>
    </div>
</div>
