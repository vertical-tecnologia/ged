        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>GED - Central de Notas</title>
    
        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="{skin}/bootstrap/css/bootstrap.min.css" />
        <!-- theme color-->
            <link rel="stylesheet" href="{skin}/css/blue.css" />
        <!-- tooltip -->    
            <link rel="stylesheet" href="{skin}/lib/qtip2/jquery.qtip.min.css" />
        <!-- main styles -->
            <link rel="stylesheet" href="{skin}/css/simple-slider.css" />
        <link rel="stylesheet" href="{skin}/css/simple-slider-volume.css" />
        <link rel="stylesheet" href="{skin}/css/alertify.core.css" />
        <link rel="stylesheet" href="{skin}/css/alertify.default.css" />

        <link rel="stylesheet" href="{skin}/css/jquery-labelauty.css" />
        <link rel="stylesheet" href="{skin}/css/style.css" />
        <link rel="stylesheet" href="{skin}/css/login.css" />
        <!-- favicon -->
        <link rel="shortcut icon" href="favicon.ico" />

        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="{skin}/css/ie.css" />
        <![endif]-->
        <!--[if lt IE 9]>
            <script src="{skin}/js/ie/html5.js"></script>
            <script src="{skin}/js/ie/respond.min.js"></script>
        <![endif]-->
        