<br />
<table border="0" cellspacing="0" cellpadding="5" width="100%" id="grid_aba_{nome_aba}" class="table table-striped table-bordered mediaTable" data-enable-pagination="true">
    <thead>
        <tr>
            {vet_col}
                <th><span class="optional">{desc_coluna}</span></th>
            {/vet_col}<?php
            if (!empty($col_especial)) {?>
                {col_especial}
                    <th class="no-sort"><span class="optional">{desc_coluna}</span></th>
                {/col_especial}<?php
            }?>
        </tr>
    </thead>
    <tbody><?php
    if (!empty($vet_dados)) {?>
        {vet_dados}
            <tr id="linha_aba_{nome_aba}_{id_dado}">
                {dados_col}
                    <!--<td><span>{nome_dado}</span></td>-->
                    <td><span><a href="{url_padrao}" class="ext_disabled" title="{title_dado}">{nome_dado}</a></span></td>
                {/dados_col}<?php
                if (!empty($botoes_especiais)) {?>
                    {botoes_especiais}
                        <td>
                            <span class="nome_dado">
                                <a href="{link}" class="ext_disabled">{nome_link}</a>
                            </span>
                        </td>
                    {/botoes_especiais}<?php
                }?>
            </tr>
        {/vet_dados}<?php
    }?>
    </tbody>
</table>

<script type="text/javascript">

    if (typeof initDataTables === 'function') {

        initDataTables();

    }

</script>
