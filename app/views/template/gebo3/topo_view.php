<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
<div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand ext_disabled pull-left" href="{url}"><img src="{img}/logo.png" border="0" /> GED</a>


                    <ul class="nav navbar-nav navbar-right">
                        <?php $userdata = $this->session->userdata('USER_ROLE');
                        if($userdata == null): ?>
                        <li>
                            <a href="{url}cadastro/" title="Cadastre se no GED."> Cadastre-se</a>
                        </li>
                        <?php endif;
                        $userdata = $this->session->userdata('USER_ROLE');
                        if($userdata != null): ?>
                        <!-- <li>
                            <a href="{url}download/" class="fa fa-download" title="Baixe o GED client"> Download do GED </a>
                        </li> -->
                        <li>
                                <a href="{url}mensagens" class="my_typs" title="Novas mensagens"><span class="badge badge-warning">{total_nao_lidas}</span> <i class="fa fa-envelope-o"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <strong><?php
                                    echo '<span class="hidden-phone">'.$this->session->userdata('USER_NAME').'</span>';?>
                                </strong>
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </a>
                            <ul class="dropdown-menu" style="width: 320px">
                                <li>
                                    <div class="navbar-login">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p class="text-center">
                                                    <img src="<?=$this->session->userdata('USER_PICTURE') !='' ? base_url() .'public/static/' .$this->session->userdata('USER_PICTURE') : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAGJ0lEQVR4Xu2bZah1RRSGn8/uTuzCDhRBRRFbRDAxEQO7EwsTsRvEBgtbfxhYoIItdv+wCwwUu4Pnsvdl7nz7nLP3mTn3nBvr33fPxLvevWbNivmmMMFlygTXn0kCJi1ggjMweQRG0QBmB7YGNgZWB5YG5ij2/wH4CHgNeBx4APh5NLCNhgUsB5wA7ArMXFOpX4FbgfOA92vO6WpYLwlQ2TOBI4HpukIHfwEXA6cDv3e5RttpvSJgWeBeYNVMoF8Htgc+zLTe8DK9IGAN4BFggQqwHwO3AU8AbwPfFmPmA1Yu/INHZYmKuV8BmwNv5CQhNwF++WcqlP8EOB64C/i3gwLTADsX53+xaKwkrJfTEnIS4Jl/ocLsdWb7A780/HKzAdcCu0TzvCnWAf5ouF7l8JwEnA8cF+1yEXBsItBLCkcaLnMucGLiukPTcxHgVfdO5O398rtnACnG24GdgrW8HVYEPkhdPxcB1wP7BGA88zq1pmbfSh+DKAleNBjg8fBoJUkOAgSncwqDHM/tHUnIpp6sNd0S/FlyF0wlOQcBXluaeyledcvU8PZN+Zm28P6LBxO9Le5sulA4PgcBmuK+waLnACelgGozN3a0VwMHpuyVg4AXgbUDEAYrj6WAajN3S+Ch4PfngXVT9spBgNHcvAGIRYAvU0C1masT/Cz4/evCD3S9XQ4C/gSmDxDMCPi3Xohrh0mR+/i3rmWsE/BPQqY5RFoOAr4BTGZKGc0j4PGbv+vPn4mAfjrBlyIH3JiLHBZwDbBfsHO2OL1Cmwui3MK9D2isdTAhBwFGfeb4pRgGW+/rlPY2xW0gZN0wTJEHIhAybTUUniXQaLeIlKbKVo3fA7gp+MFQeKHU4mkOCxBTHA16V6+UCi5Q1uqxyZAOtpSrgINSmc1FgJUgAYbxgCmseUKqiNF4f8dgIYshKwDmHUmSiwBB6Pwse4VyKXA08F+XKMXnGodH888CTulyzRHTchIwE2BsbtMjFL+eydJPDQFr9tYZwi/vEi8XdcEs0WZOAgSn93+2Ij7/vGiOeCyM3tqJ3l4nalYZnnnneLOYeL3SkMyWw3MT4EarAY+2SFI+LQoltr/eAowiFaO5VYBNiopwXA0OFbBldllBUHLFqRcElJZwD2CPoFeiVR0C3JeyQa8IEJM+4TTgmOh2aIJXb38/sBkwZ4uJXsFHAL81Wbgc2y0BmuhRRT3uQsDubiuxPObt4LmetSZIzdygxwqQkaXzDLddx+AnFh3jtoBW0Ui6IcCz6hlfuNjJHuAONXY1Ytwqao/PBej0vi/CXJ2b/uHBFsVOiTi1ID+MOdz+C2BT4L0aWIaHNCXAZqd9vbACJOvtnFYTPHXHrlVUiA2GQtGpbgi8W3ehJgTYsPSej03QHr79/9EWy/E3A9tEGxuG2z+sdRzqEmCio/Jxuzs10kslzUbqFRWVYX3C+nXeFNQl4AZgzwitPTvD3EGQKytIuC6qU1TirEOADu7uaLZdHxOdbmP83KRpCTrj+Dj477ZxQicCjMf1qqXHF/ibRXvadzyDJPoEy3OhY/RmWL5d+6wTAXGGZwKyZvG6Y5CUL7GITV8VXpFnAye3AtuOABuPlqDCpucZxYOlQVS+xBS3z8wXlgye44zA3o4As7HwetOcLHz05LVWRkYNlnw34AcspaUVtCLAOF6F5wkWORjQ244F8Wmet1QpBkgGa1M9q2lFgNVWc/dSbEC4wKB//RKvVuAHDBMoX5j4SKvWEfBK2S4YacITv/8ZdEu4HDgsAKny4TOboZ+qLGAG4Lsoc9O7vjroGkf4fEn2XPA3M8y5gb/DcVUEGEI+FQzqR7KTg2t1s18R9g7NEUJSKi3AAoYmX8qNwF45EPVhDf2Y/qwUaxjmL8NSZQEWIuzClHJokXD0AX/ylr5RtJ9YijnN3p0IsKobPjvZCHgyGUp/FtgCeDjY+mlgg04EWLkNCxyWuo0Ix6KYB4QVIjtJS3UiQG9Zt3Y31kj5MS6uVvkAGxeml+NRvAJH1BKrCPDlVdKzkwFmzmtxREmvigCLCL68qPoPDwOsW0doKm9p3T5D22uw40rjaUCngsh40rVSl0kCxv0n7qDg/4zyCFAOcj9oAAAAAElFTkSuQmCC'?>" alt="" class="user_avatar" />
                                                </p>
                                            </div>
                                            <div class="col-lg-8">
                                                <p class="text-left"><strong><?=$this->session->userdata('USER_FULLNAME');?></strong></p>
                                                <p class="text-left small"><?=$this->session->userdata('USER_EMAIL');?></p>
                                                <p class="text-left">
                                                    <a href="{url}perfil" class="btn btn-primary btn-block btn-sm">Meus dados</a>
                                                </p>
                                                <?php if($userdata != 'SUPERUSUARIO'): ?>
                                                <p class="text-left">
                                                    <a href="{url}meu_contrato" class="btn btn-default btn-block btn-sm">Contrato</a>
                                                </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="{url}auth/logout" class="btn btn-danger btn-block">Sair</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                    </ul>


        </div>
    </div>
</nav>