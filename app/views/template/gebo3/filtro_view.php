<div class="modal hide fade" id="myTasks">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>Filtro</h3>
    </div>
    <div class="modal-body">
        {filtro_tela}
    </div>
    <div class="modal-footer">
        <script type="text/javascript">
            function buscar(tela) {
                document.getElementById("frm_grid").action = $('#url').val() + tela + "/listar";
                document.frm_grid.submit();
            }
        </script>
        <input type="button" class="btn" value="Filtrar" onclick="buscar('{tela}')" />
    </div>
</div>
