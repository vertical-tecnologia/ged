 		<script src="{skin}/js/jquery.min.js"></script>
        <script src="{skin}/js/jquery.actual.min.js"></script>
        <script src="{skin}/lib/validation/jquery.validate.js"></script>
		<script src="{skin}/bootstrap/js/bootstrap.min.js"></script>
		<script src="{skin}/js/jquery.validate.min.js"></script>
		<script src="{skin}/js/additional-methods.min.js"></script>
        <script src="{skin}/js/jquery-labelauty.js"></script>
        <script src="{skin}/js/simple-slider.min.js"></script>
		<script src="{skin}/js/jquery.mask.min.js"></script>
        <script src="{skin}/js/alertify.js"></script>
        <script src="{skin}/js/parsley-external.js"></script>
        <script src="{skin}/js/parsley.min.js"></script>
        <script src="{skin}/js/i18n/pt-br.js"></script>
        <script src="{skin}/js/login.js"></script>

        <script>

            $(document).ready(function(){

            alertify.set({
                labels :{
                    ok : 'Sim',
                    cancel : 'Não'
                }
            });

            <?php
            $userdata = $this->session->userdata('vet_dados');
            $err = $this->session->userdata('msg_erro');
            $msg_error = isset($err) ? $err : null;

//            echo 'console.log('.json_encode($this->session->all_userdata()).')';
            if((isset($userdata['error']) && $userdata['error'] == 1) || $msg_error != null){
                echo 'alertify.set({labels: {ok : \'OK\'}});';
                echo "alertify.alert(".json_encode(isset($userdata["msg"]) ? $userdata["msg"] : $msg_error).");";
            }
            $arr = array();
            $arr['vet_dados']['error'] = '';
            $arr['vet_dados']['msg'] = '';
            $this->session->unset_userdata($arr);
            ?>


            $(".date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

            $('.sp_celphones').mask(SPMaskBehavior, spOptions);
			$(".cep").mask("99999-999");
			$(".cpf").mask("999.999.999-99", {'placeholder': '000.000.000-00'});
			$(".cnpj").mask("99.999.999.9999/99", {'placeholder': '00.000.000/0001-00'});


				//* boxes animation
				form_wrapper = $('.login_box');
				function boxHeight() {
					form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);
				};
				form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
                $('.linkform a,.link_reg a').on('click',function(e){
					var target	= $(this).attr('href'),
						target_height = $(target).actual('height');
					$(form_wrapper).css({
						'height'		: form_wrapper.height()
					});
					$(form_wrapper.find('form:visible')).fadeOut(400,function(){
						form_wrapper.stop().animate({
                            height	 : target_height,
							marginTop: ( - (target_height/2) - 24)
                        },500,function(){
                            $(target).fadeIn(400);
                            $('.links_btm .linkform').toggle();
							$(form_wrapper).css({
								'height'		: ''
							});
                        });
					});
					e.preventDefault();
				});

				//* validation
				$('#login_form').validate({
					onkeyup: false,
					errorClass: 'error',
					validClass: 'valid',
					rules: {
						username: { required: true, minlength: 3 },
						password: { required: true, minlength: 3 }
					},
					highlight: function(element) {
						$(element).closest('.form-group').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('.form-group').append(error);
					}
				});
            });

        </script>
