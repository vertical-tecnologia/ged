<!doctype html>
<html lang="pt-br">
    <head>
        {head}
        <script>
//            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
//
//            ga('create', 'UA-63659633-1', 'auto');
//            ga('send', 'pageview');

        </script>
    </head>
    <body class="full_width <?=(SIDEBAR_POSITION);?> <?=(BACKGROUND_PATTERN);?>">
    <?php $userdata = $this->session->userdata('USER_ROLE'); if($userdata != '' && $userdata != 'Indefinido'): ?>
    <div class="style_switcher">
        <div class="sepH_c">
            <p>Colors:</p>
            <div class="clearfix">
                <a href="javascript:void(0)" class="style_item jQclr blue_theme style_active" title="blue">blue</a>
                <a href="javascript:void(0)" class="style_item jQclr dark_theme" title="dark">dark</a>
                <a href="javascript:void(0)" class="style_item jQclr green_theme" title="green">green</a>
                <a href="javascript:void(0)" class="style_item jQclr brown_theme" title="brown">brown</a>
                <a href="javascript:void(0)" class="style_item jQclr eastern_blue_theme" title="eastern_blue">eastern blue</a>
                <a href="javascript:void(0)" class="style_item jQclr tamarillo_theme" title="tamarillo">tamarillo</a>
            </div>
        </div>

        <div class="sepH_c">
            <p>Backgrounds:</p>
            <div class="clearfix">
                <span class="style_item jQptrn style_active ptrn_def" title=""></span>
                <span class="ssw_ptrn_a style_item jQptrn" title="ptrn_a"></span>
                <span class="ssw_ptrn_b style_item jQptrn" title="ptrn_b"></span>
                <span class="ssw_ptrn_c style_item jQptrn" title="ptrn_c"></span>
                <span class="ssw_ptrn_d style_item jQptrn" title="ptrn_d"></span>
                <span class="ssw_ptrn_e style_item jQptrn" title="ptrn_e"></span>
            </div>
        </div>

        <div class="sepH_c">
            <p>Sidebar position:</p> <?=SIDEBAR_POSITION?>
            <div class="clearfix">
                <label class="radio-inline"><input name="ssw_sidebar" id="ssw_sidebar_left" value="" <?=SIDEBAR_POSITION == '' ? 'checked' : '';?> type="radio"> Left</label>
                <label class="radio-inline"><input name="ssw_sidebar" id="ssw_sidebar_right" value="sidebar_right" <?=SIDEBAR_POSITION == 'sidebar_right' ? 'checked' : '';?> type="radio"> Right</label>
            </div>
        </div>

        <div class="hide">
            <ul id="ssw_styles">
                <li class="small ssw_mbColor sepH_a" style="display:none">body {<span class="ssw_mColor sepH_a" style="display:none"> color: #<span></span>;</span> <span class="ssw_bColor" style="display:none">background-color: #<span></span> </span>}</li>
                <li class="small ssw_lColor sepH_a" style="display:none">a { color: #<span></span> }</li>
            </ul>
        </div>
        <div class="gh_button-group">
            <a href="#" id="resetDefault" class="btn btn-default btn-sm">Reset</a>
        </div>
        <?php endif;?>
    </div>

        <input type="hidden" name="url" id="url" value="{url}" />
        <input type="hidden" name="css" id="css" value="{css}" />
        <input type="hidden" name="js" id="js" value="{js}" />
        <input type="hidden" name="classe" id="classe" value="<?=$this->uri->segment(1)?>" />
        <input type="hidden" name="metodo" id="metodo" value="<?=$this->uri->segment(2)?>" />



        <div id="maincontainer" class="clearfix">
            <header>
                {topo}
            </header>
            <div id="contentwrapper">
                <div class="main_content">
                    {menu}
                    {conteudo}
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="footer" class="footer text-center <?=(FOOTER_POSITION);?>">
            <div class="container">
                <span id="rodape" class="text-info text-center span12">Central de Notas - Suporte: <a href="mailto:contato@centraldenotas.com.br">contato@centraldenotas.com.br</a> - Fone: <a href="tel:01432219313">(14) 3221-9313</a></span>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="gedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body" id="myModalContent">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="mBtn-close" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" id="mBtn-save" class="btn btn-primary">Salvar</button>
                    </div>
                </div>
            </div>
        </div>

        {js_arquivos}
    </body>
</html>
