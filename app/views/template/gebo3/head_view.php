        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>GED - Central de Notas</title>

        <!-- Bootstrap framework -->
        <link rel="stylesheet" href="{skin}/bootstrap/css/bootstrap.min.css" />
        <!-- jQuery UI theme -->
        <!--        <link rel="stylesheet" href="{skin}/lib/jquery-ui/css/Aristo/Aristo.css" />-->
        <link rel="stylesheet" href="{skin}/lib/custom-theme/jquery-ui-1.10.0.custom.css" />

        <!-- breadcrumbs -->
        <link rel="stylesheet" href="{skin}/lib/jBreadcrumbs/css/BreadCrumb.css" />
        <link rel="stylesheet" href="{skin}/lib/sticky/sticky.css" />

        <!-- tooltips-->
        <link rel="stylesheet" href="{skin}/lib/qtip2/jquery.qtip.min.css" />
        <!-- colorbox -->
        <link rel="stylesheet" href="{skin}/lib/colorbox/colorbox.css" />
        <!-- datatables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="{skin}/lib/datatables/extras/TableTools/media/css/TableTools.css">
        <!-- font-awesome -->
        <link rel="stylesheet" href="{skin}/img/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{skin}/lib/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" />
        <link rel="stylesheet" href="{skin}/lib/chosen/chosen.css" />
        <!-- multi-select -->
        <link rel="stylesheet" href="{skin}/lib/multi-select/css/multi-select.css">
        <!-- fancytree -->
        <link rel="stylesheet" href="{skin}/lib/fancytreejs/ui.fancytree.css">
        <link rel="stylesheet" href="{skin}/lib/fancytreejs/ged-fancytree.css">
        <!-- theme color-->
        <link rel="stylesheet" href="{skin}/css/{theme}.css" id="link_theme" />

        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="{skin}/lib/sweetalert/dist/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="{skin}/lib/sweetalert/themes/google/google.css">
        <!--[if lte IE 8]>
        <link rel="stylesheet" href="{skin}/css/ie.css" />
        <![endif]-->
        <link rel="stylesheet" href="{skin}/lib/icheck/skins/square/blue.css">
        <!-- main styles -->
        <link rel="stylesheet" href="{skin}/css/style.css" />
        <link rel="stylesheet" media="print" href="{skin}/css/print.css" />
        <?php if($this->router->fetch_class() == 'arquivos'): ?>
            <link rel="stylesheet" href="{skin}/lib/formstone/dist/css/upload.css" />
            <link rel="stylesheet" href="{skin}/lib/colorbox/colorbox.css" />

        <?php endif; ?>
<?php if($this->router->fetch_class() == 'segmento'): ?>
<?php if($this->router->fetch_method() == 'diretorios'): ?>
<!--        segmentos/diretorio-->
        <link rel="stylesheet" href="{skin}/css/treeview.css">
<?php endif; ?>
<?php endif; ?>

<?php if($this->router->fetch_class() == 'configuracao'): ?>
<?php if(in_array($this->router->fetch_method(), array('contrato', 'executavel'))): ?>
<!--        <link rel="stylesheet" href="{skin}/lib/redactor-js/redactor/redactor.css"/>-->
<?php endif; ?>
<?php endif; ?>

<?php if($this->router->fetch_class() == 'configuracao'): ?>
<?php if($this->router->fetch_method() == 'adicional'): ?>
        <link rel="stylesheet" href="{skin}/lib/simple-slider/css/simple-slider.css" />
        <link rel="stylesheet" href="{skin}/lib/simple-slider/css/simple-slider-volume.css" />
<?php endif; ?>
<?php endif; ?>
<?php if($this->router->fetch_class() == 'grupo'): ?>
<?php if($this->router->fetch_method() == 'permissoes'): ?>
        <link rel="stylesheet" href="{skin}/lib/fancytreejs/ui.fancytree.css" />
        <link rel="stylesheet" href="{skin}/lib/fancytreejs/ged-fancytree.css" />

<?php endif; ?>
<?php endif; ?>


<?php if($this->router->fetch_class() == 'arquivos'): ?>
<?php if($this->router->fetch_method() == 'ftp'): ?>
        <link rel="stylesheet" href="{skin}/lib/elfinder/css/elfinder.min.css" />
        <link rel="stylesheet" href="{skin}/lib/elfinder/css/theme.css" />
<?php endif; ?>
<?php endif; ?>

        <!--[if lt IE 9]>
        <script src="{skin}/js/ie/html5.js"></script>
        <script src="{skin}/js/ie/respond.min.js"></script>
        <script src="{skin}/lib/flot/excanvas.min.js"></script>


        <![endif]-->
        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{skin}/favicon.ico">
        <link rel="apple-touch-icon" sizes="57x57" href="{skin}/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="{skin}/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="{skin}/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="{skin}/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="{skin}/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="{skin}/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="{skin}/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="{skin}/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{skin}/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="{skin}/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{skin}/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="{skin}/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{skin}/favicon-16x16.png">
        <link rel="manifest" href="{skin}/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <script>
            //* hide all elements & show preloader
            document.documentElement.className += 'js';
        </script>