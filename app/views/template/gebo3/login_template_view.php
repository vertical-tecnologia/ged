<!doctype html>
<html lang="pt-br" class="login_page ">
    <head>
        {head}
    </head>
    <body class="login">
        <input type="hidden" name="url" id="url" value="{url}" />
        <input type="hidden" name="js" id="js" value="{js}" />
        <input type="hidden" name="classe" id="classe" value="<?=$this->uri->segment(1)?>" />
        <input type="hidden" name="metodo" id="metodo" value="<?=$this->uri->segment(2)?>" />

        <nav class="navbar navbar-default nav-brand">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
                <img alt="Brand" src="{skin}/img/logo.png">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                   <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Saiba Mais</a></li>
                    <li><a href="#">Entre em Contato</a></li>
                  </ul>
                  </div>
          </div>
        </nav>

                    {conteudo}
        {js_arquivos}

    </body>
</html>