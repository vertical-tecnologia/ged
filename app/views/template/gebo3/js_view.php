<script src="{skin}/js/jquery.min.js"></script>
<script src="{skin}/js/jquery-migrate.min.js"></script>
<script src="{skin}/js/jquery-ui.1.11.js"></script>
<!-- main bootstrap js -->
<script src="{skin}/bootstrap/js/bootstrap.min.js"></script>
<!-- bootstrap plugins -->
<script src="{skin}/js/bootstrap.plugins.min.js"></script>
<script src="{skin}/lib/sweetalert/dist/sweetalert.min.js"></script>
<script src="{skin}/lib/chosen/chosen.jquery.min.js"></script>
<!-- js cookie plugin -->
<script src="{skin}/js/jquery_cookie_min.js"></script>
<!-- custom scrollbar -->
<script src="{skin}/lib/slimScroll/jquery.slimscroll.js"></script>
<!-- hidden elements width/height -->
<script src="{skin}/js/jquery.actual.min.js"></script>
<!-- touch events for jquery ui-->
<script src="{skin}/js/forms/jquery.ui.touch-punch.min.js"></script>
<!-- easing plugin -->
<script src="{skin}/js/jquery.easing.1.3.min.js"></script>
<!-- smart resize event -->
<script src="{skin}/js/jquery.debouncedresize.min.js"></script>
<script src="{skin}/lib/jeditable/jeditable.min.js"></script>
<!-- typeahead -->
<script src="{skin}/lib/typeahead/typeahead.min.js"></script>
<script src="{skin}/lib/sticky/sticky.min.js"></script>

<?php if($this->router->fetch_class() != 'arquivos'): ?>
<!-- jBreadcrumbs -->
<script src="{skin}/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>

<!-- moment.js date library -->
<script src="{skin}/lib/moment/moment.min.js"></script>

<script src="{skin}/lib/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<!-- small charts -->
<script src="{skin}/js/jquery.peity.min.js"></script>
<script src="{skin}/js/jquery.mask.min.js"></script>
<!-- fancytree -->
<script src="{skin}/js/jquery.hotkeys.js"></script>
<script src="{skin}/js/contextmenu.min.js"></script>
<script src="{skin}/lib/fancytreejs/jquery.fancytree.min.js"></script>
<script src="{skin}/lib/fancytreejs/src/jquery.fancytree.edit.js"></script>
<script src="{skin}/lib/fancytreejs/hotkeys/js/jquery.fancytree.hotkeys.js"></script>
<!-- calendar -->
<script src="{skin}/lib/fullcalendar/fullcalendar.min.js"></script>
<!-- multiselect -->
<script src="{skin}/lib/multi-select/js/jquery.multi-select.js"></script>
<!-- sortable/filterable list -->
<script src="{skin}/lib/list_js/list.min.js"></script>
<script src="{skin}/lib/list_js/plugins/paging/list.paging.min.js"></script>
<!-- dashboard functions -->
<script src="{skin}/lib/icheck/icheck.min.js"></script>
<script src="{skin}/lib/jquery.form.min.js"></script>

<script src="{skin}/lib/ckeditor/ckeditor.js"></script>
<script src="{skin}/js/jquery.validate.min.js"></script>

<!-- datatable -->
<!-- <script src="{skin}/lib/datatables/jquery.dataTables.min.js"></script> -->
<script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="{skin}/lib/datatables/jquery.dataTables.bootstrap.min.js"></script>
<script src="{js}/TableTools.min.js"></script>
<script src="{js}/ZeroClipboard.js"></script>

<!-- <script src="{skin}/js/application.js"></script> -->

<?php endif; ?>
<?php if($this->router->fetch_class() == 'admin'): ?>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/data.js"></script>
    <script src="http://code.highcharts.com/highcharts-3d.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
    <script>

        if($('#tipo-chart') !== null)
            $('#tipo-chart').highcharts({
            data: {
                table: 'tipotable'
            },
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Tipos de arquivos'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Totais'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.point.y + ' ' + this.point.name.toLowerCase();
                }
            }
        });


        
        if($('#empresas-chart') !== null)
            $('#empresas-chart').highcharts({
            data: {
                table: 'empresasTable'
            },
            chart: {
                type: 'pie'
            },
            title: {
                text: 'TOP Empresas'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Totais'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.point.y + ' ' + this.point.name;
                }
            }
        });


        if($('#users-chart') != null)
            $('#users-chart').highcharts({
            data: {
                table: 'usuariosTable'
            },
            chart: {
                type: 'pie'
            },
            title: {
                text: 'TOP Usuários'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Totais'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.point.y + ' ' + this.point.name;
                }
            }
        });


        if($('#tipoChartBars') != null)
            $('#tipoChartBars').highcharts({
                data: {
                    table: 'tipotablebars'
                },
                chart: {
                    type: 'column',
                    margin: 75,
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: 'Tipos de arquivos'
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Totais'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            this.point.y + ' ' + this.point.name.toLowerCase();
                    }
                }
            });
    </script>

<?php endif; ?>
<?php if($this->router->fetch_class() == 'arquivos'): ?>
    <script src="{skin}/lib/colorbox/jquery.colorbox.min.js"></script>
    <script src="{js}/jquery.imagesloaded.min.js"></script>
    <script src="{js}/jquery.wookmark.js"></script>
    <script type="text/javascript" src="{skin}/lib/formstone/dist/js/core.js"></script>
    <script type="text/javascript" src="{skin}/lib/formstone/dist/js/upload.js"></script>
    <script src="{skin}/js/uploader.js"></script>
    <?php if($this->router->fetch_method() != 'upload'): ?>
    <script src="{skin}/js/fm.js"></script>
    <?php endif; ?>
<?php endif; ?>

<?php if($this->router->fetch_class() == 'segmento'): ?>
    <?php if($this->router->fetch_method() == 'diretorios'): ?>
    <?php endif; ?>
<?php endif; ?>

<?php if($this->router->fetch_class() == 'grupo'): ?>
    <?php if($this->router->fetch_method() == 'permissoes'): ?>
        <script src="{skin}/lib/fancytreejs/jquery.fancytree.min.js"></script>
        <script src="{skin}/js/tree.permissions.min.js"></script>
    <?php endif; ?>
<?php endif; ?>

<?php if(in_array($this->router->fetch_class(), array('configuracao', 'admin'))): ?>
    <?php if(in_array($this->router->fetch_method(), array('contrato', 'executavel', 'index'))): ?>
        <script src="{skin}/lib/ckeditor/ckeditor.js"></script>
        <script src="{skin}/lib/ckeditor/ckeditor.js"></script>
        <script>
            $(document).ready(function(){
                var $contrato = document.getElementById('contrato');
                var $instrucoes = document.getElementById('instrucoes');
                var $contrato_text = document.getElementById('contrato_text');



                var options = {
                    language : 'pt_BR'
                };

                if($contrato != undefined)
                    CKEDITOR.replace( 'contrato', options );

                if($instrucoes != undefined)
                    CKEDITOR.replace( 'instrucoes', options );


                if($contrato_text != undefined){
                    var options = {
                        language : 'pt_BR',
                        height : 300,
                        readOnly : ($contrato_text.getAttribute('readonly') == "readonly" ? true : false),
                        extraPlugins : 'print',
                        removeButtons : 'Underline,Subscript,Superscript,Undo,Cut,Copy,Redo,Paste,PasteText,PasteFromWord,Scayt,Link,Image,Maximize,Source,Table,Unlink,Anchor,HorizontalRule,SpecialChar,Bold,Italic,Strike,RemoveFormat,NumberedList,Outdent,Indent,BulletedList,Format,Styles,About,Blockquote'
                    };
                    CKEDITOR.replace( 'contrato_text', options);
                }
            });

        </script>
    <?php endif; ?>
<?php endif; ?>


<?php if($this->router->fetch_class() == 'arquivos'): ?>
    <?php if($this->router->fetch_method() == 'ftp'): ?>
        <script src="{skin}/lib/elfinder/js/elfinder.min.js"></script>
        <script src="{skin}/lib/elfinder/js/i18n/elfinder.pt_BR.js"></script>

        <script>
            $().ready(function() {
                var f = $('#elfinder').elfinder({
                    url : '{url}arquivos/elfinder',
                    lang : 'pt_BR',
                }).elfinder('instance');
            });
        </script>
    <?php endif; ?>
<?php endif; ?>

<?php if($this->router->fetch_class() == 'configuracao'): ?>
    <?php if($this->router->fetch_method() == 'adicional'): ?>
        <script src="{skin}/lib/simple-slider/js/simple-slider.js"></script>
        <script >

            /**
             * @function: getBytesWithUnit()
             * @purpose: Converts bytes to the most simplified unit.
             * @param: (number) bytes, the amount of bytes
             * @returns: (string)
             */
            var getBytesWithUnit = function( bytes ) {

                if (isNaN(bytes)) {
                    return;
                }

                var units = [ ' bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB' ];
                var amountOf2s = Math.floor( Math.log( +bytes )/Math.log(2) );

                if (amountOf2s < 1) {
                    amountOf2s = 0;
                }

                var i = Math.floor( amountOf2s / 10 );
                bytes = +bytes / Math.pow( 2, 10*i );

                // Rounds to 3 decimals places.
                if (bytes.toString().length > bytes.toFixed(3).toString().length) {
                    bytes = bytes.toFixed(3);
                }

                return bytes + units[i];

            };


            $("[data-slider]")
                .each(function () {
                    var input = $(this);
                    $("<span>")
                        .addClass("output")
                        .insertAfter($(this));
                })
                .bind("slider:ready slider:changed", function (event, data) {

                    var valor_servico, quant_id, quant_size;
                    $.each(servicos, function(index, value) {

                        if (servicos[index].quant_valor == data.value) {
                            console.log(servicos[index].quant_preco);
                            $('#id_servico').val(servicos[index].quant_id);
                            valor_servico = servicos[index].quant_preco;
                            quant_id = servicos[index].quant_id;
                        }

                    });

                    $('#quant_id').val(quant_id);
                    $('#quant_preco').val(valor_servico);
                    $('#quant_size').val(data.value);
                    $('#val_id_servico').html(getBytesWithUnit(data.value));
                    $('#val_valor_id_servico').html(' Valor: R$ ' + valor_servico);

                }
            );

        </script>
    <?php endif; ?>
<?php endif; ?>

<script type="text/javascript" charset="utf-8">

    jQuery(document).ready(function($){

        <?php if($this->session->flashdata('warning')): ?>
        swal({title: "Atenção!", text: "<?=$this->session->flashdata('warning');?>", type: "warning", confirmButtonText: "Ok" });
        <?php elseif($this->session->flashdata('error')): ?>
        swal({title: "Erro!", text: "<?=$this->session->flashdata('error');?>", type: "error", confirmButtonText: "Ok" });
        <?php elseif($this->session->flashdata('success')): ?>
        swal({title: "Sucesso!", text: "<?=$this->session->flashdata('success');?>", type: "success", confirmButtonText: "Ok" });
        <?php endif; ?>



        $( ".datepicker" ).datepicker({
            dateFormat : "dd/mm/yy",
            minDate: 0,
            showAnim : "slideDown"
        });

        var $multiSelect = $('#grupo-usuarios');

        if($multiSelect.length) {
            $multiSelect.multiSelect();

            $('#select-all').click(function(){
                $multiSelect.multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function(){
                $multiSelect.multiSelect('deselect_all');
                return false;
            });

        }

    });
</script>
<script type="text/javascript" src="{skin}/js/app.js"></script>
<script src="{skin}/js/application.js"></script>

<!-- <a class="ssw_trigger" href="javascript:void(0)"><i class="glyphicon glyphicon-cog"></i></a> -->
