<!DOCTYPE html>
<html lang="en">
<head>
    <!--
    Boxer Template
    http://www.templatemo.com/preview/templatemo_446_boxer
    -->
    <meta charset="utf-8">
    <title>GED - Central de Notas</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- animate css -->
    <link rel="stylesheet" href="{skin}/css/animate.min.css">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{skin}/css/bootstrap.min.css">
    <!-- font-awesome -->
    <link rel="stylesheet" href="{skin}/css/font-awesome.min.css">
    <!-- google font -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{skin}/lib/sweetalert/dist/sweetalert.css">
    <!-- custom css -->
    <link rel="stylesheet" href="{skin}/css/templatemo-style.css">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-63659633-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<!-- start preloader -->
<div class="preloader">
    <div class="sk-spinner sk-spinner-rotating-plane"></div>
</div>
<!-- end preloader -->
<!-- start navigation -->
<nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><img src="{skin}/images/logo.png" class="logo" alt="GED"/></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right text-uppercase">
                <?php if(in_array($this->router->fetch_method(), array('plano', 'login', 'forgot', 'reset')) ): ?>
                <li><a href="{url}" class="external">Home</a></li>
                <li><a href="{url}login" class="external">Login</a></li>
                <?php else: ?>
                <li><a href="#home">Home</a></li>
                <li><a href="#feature1">Recursos</a></li>
                <li><a href="#pricing">Planos</a></li>
                <li><a href="#register">Cadastre-se</a></li>
                <li><a href="#contact">Contato</a></li>

                <li class="dropdown">
                    <a href="{url}" class="dropdown-toggle" data-toggle="dropdown">Login <b class="caret"></b></a>
                    <ul class="dropdown-menu" style="padding: 15px;min-width: 250px;">
                        <li>
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form" role="form" method="post" action="{url}auth/login/" accept-charset="UTF-8" id="login-nav">
                                        <div class="form-group">
                                            <label class="sr-only" for="login_usuario">Usuário</label>
                                            <input type="text" class="form-control" id="login_usuario" placeholder="Usuário" required name="login_usuario">
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="senha_usuario">Senha</label>
                                            <input type="password" class="form-control" id="senha_usuario" name="senha_usuario" placeholder="Senha" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success btn-block">Entrar</button>
                                        </div>
                                        <div class="form-group">
                                            <a href="{url}forgot" type="submit" class="btn btn-link btn-xs external"><i class="fa fa-lock"></i> Esqueci minha senha</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>

<!--                        <li class="divider"></li>-->
<!--                        <li>-->
<!--                            <input class="btn btn-primary btn-block" type="button" id="sign-in-google" value="Entrar com Google">-->
<!--                            <input class="btn btn-primary btn-block" type="button" id="sign-in-twitter" value="Entrar com Twitter">-->
<!--                        </li>-->
                    </ul>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
<!-- end navigation -->
{conteudo}
<!-- start footer -->
<footer>
    <div class="container">
        <div class="row">
            <p>Copyright © <?=date('Y')?> Central de Notas. Todo o conteúdo deste site é de uso exclusivo da Central de Notas.</p>
        </div>
    </div>
</footer>
<!-- end footer -->
<script src="{skin}/js/jquery.js"></script>
<script src="{skin}/js/bootstrap.min.js"></script>
<script src="{skin}/js/wow.min.js"></script>
<script src="{skin}/js/jquery.singlePageNav.min.js"></script>
<script src="{skin}/lib/sweetalert/dist/sweetalert.min.js"></script>
<script src="{skin}/js/jquery.validate.min.js"></script>
<!-- js cookie plugin -->
<script src="{skin}/js/jquery_cookie_min.js"></script>
<script src="{skin}/js/custom.js"></script>

<script>

    document.addEventListener('DOMContentLoaded', function(){
        <?php if($this->session->flashdata('warning')): ?>
        swal({title: "Oooops!", text: "<?=$this->session->flashdata('warning');?>", type: "warning", confirmButtonText: "Ok" });
        <?php elseif($this->session->flashdata('error')): ?>
        swal({title: "Erro!", text: "<?=$this->session->flashdata('error');?>", type: "error", confirmButtonText: "Ok" });
        <?php elseif($this->session->flashdata('success')): ?>
        swal({title: "Sucesso!", text: "<?=$this->session->flashdata('success');?>", type: "success", confirmButtonText: "Ok" });
        <?php endif; ?>


        gedValidate.init();
    });
</script>

</body>
</html>