{titulo_tela}
<div class="btn-group sepH_b pull-right">
    <button data-toggle="dropdown" class="btn dropdown-toggle">Ação <span class="caret"></span></button>
    <ul class="dropdown-menu"><?php
        if (in_array($this->uri->segment(2), array('editar','inserir','alterar', 'meus_dados')) || !$this->uri->segment(2)) {?>
            <li><a href="javascript: void(0)" onclick="$('#frm-{tela}').submit()"><i class="icon-ok"></i> Salvar</a></li><?php
        }?>
        <li><a href="{url}{tela}" class="ext_disabled"><i class="icon-plus-sign"></i> Novo</a></li><?php 
        if ($this->uri->segment(2) != "listar") {?>
            <li><a href="{url}{tela}/listar" class="ext_disabled"><i class="icon-arrow-left"></i> Cancelar</a></li><?php
        }
        if ($this->uri->segment(2) == "listar") {?>
            <li><a data-toggle="modal" data-backdrop="static" href="#myTasks" class="ttip_b"><i class="icon-filter"></i> Filtrar</a></li>
            <li><a href="javascript: void(0)" onclick="deletar()" class="delete_rows_simple" data-tableid="smpl_tbl"><i class="icon-trash"></i> Excluir</a></li><?php
        }?>
    </ul>
</div>
