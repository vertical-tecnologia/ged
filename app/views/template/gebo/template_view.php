<!doctype html>
<html lang="pt-br" class="login_page ">
    <head>
        {head}
    </head>
    <body>
        <input type="hidden" name="url" id="url" value="{url}" />
        <input type="hidden" name="js" id="js" value="{js}" />
        <input type="hidden" name="classe" id="classe" value="<?=$this->uri->segment(1)?>" />
        <input type="hidden" name="metodo" id="metodo" value="<?=$this->uri->segment(2)?>" />
        <div id="loading_layer" style="display:none"><img src="{img}/ajax_loader.gif" alt="" /></div>
        <div id="loading_data" style="display:none"><div class="label-loading"><img src="{img}/ajax_loader.gif" alt="" /><br />carregando dados...</div></div>
        <div id="maincontainer" class="clearfix">
            <header>
                {topo}
            </header>
            <div id="contentwrapper">
                <div class="main_content">
                    {conteudo}
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="container">
                <span id="rodape" class="text-info text-center span12">Central de Notas - Suporte: <a href="mailto:contato@centraldenotas.com.br">contato@centraldenotas.com.br</a> - Fone: <a href="tel:01432219313">(14) 3221-9313</a></span>
            </div>
        </div>
        {js_arquivos}
    </body>
</html>
