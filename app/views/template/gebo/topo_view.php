<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand ext_disabled" href="{url}"><img src="{img}/logo.png" border="0" /></a>
            <div class="menu_center">
            {menu}
            <ul class="nav user_menu pull-right">
<!--                 <li>
                    <a href="{url}/download/windows" class="fa fa-download" title="Baixe o GED para windows"></a>
                </li> -->
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle user" data-toggle="dropdown">
                        <!--<b class="icon-tasks" id="qtd_utilizada" title=""></b>--><?php
                        echo '<span class="hidden-phone">'. $this->session->userdata('nome_cliente').'</span>';
                        echo '<span class="divider-vertical hidden-phone"></span>';
                        echo '<span class="label" style="background-color: white; cursor: pointer"><img src="{img}/gCons/agent.png" alt="" style="width:20px !important" /></span>&nbsp;';
                        echo '<span class="hidden-phone">'.$this->session->userdata('nome_usuario').'</span>';?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user_brand visible-phone"><?=$this->session->userdata('nome_cliente').'</br>'.$this->session->userdata('nome_usuario')?></li>
                        <li><a href="{url}usuario/meus_dados" class="ext_disabled">Editar Dados</a></li>
                        <li class="divider"></li>
                        <li><a href="{url}admin/logoff" class="ext_disabled">Sair</a></li>
                    </ul>
                </li>
            </ul>
            </div>
        </div>
    </div>
</div>
