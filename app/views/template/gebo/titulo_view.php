<nav>
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="{url}" class="ext_disabled"><i class="icon-home"></i></a></li><?php
            if ($this->uri->segment(2) != "listar") {?>
                <li><a href="{url}{tela}/listar" class="ext_disabled">{tela_ant}</a></li><?php
            }?>
            <li>{nome_tela}</li>
        </ul>
    </div>
</nav>
