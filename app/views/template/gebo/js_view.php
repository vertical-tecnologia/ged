        <?php if(!in_array($this->uri->segment(2), array('admin', ''))){ ?>
           <script src="{js}/jquery.min.js"></script>
           <script src="{js}/jquery-ui.1.10.2.js"></script>
           <script src="{js}/jquery-migrate.min.js"></script>
           <script src="{js}/jquery.contextMenu-1.6.5.js"></script>
        <?php } else { ?>
            <script src="{js}/jquery.1.7.2.min.js"></script>
            <script src="{js}/jquery-ui.min.1.8.21.js"></script>
        <?php } ?>

            <!-- simple-slider -->
            <script src="{js}/simple-slider.js"></script>
            <!-- smart resize event -->
            <script src="{js}/jquery.debouncedresize.min.js"></script>
            <!-- hidden elements width/height -->
            <script src="{js}/jquery.actual.min.js"></script>
            <!-- js cookie plugin -->
            <script src="{js}/jquery_cookie.min.js"></script>
            <!-- main bootstrap js -->
            <script src="{skin}/bootstrap/js/bootstrap.min.js"></script>
             <!-- bootstrap plugins -->
            <script src="{js}/bootstrap.plugins.min.js"></script>
            <!-- tooltips -->
            <script src="{skin}/lib/qtip2/jquery.qtip.min.js"></script>
            <!-- sticky messages -->
            <script src="{skin}/lib/sticky/sticky.min.js"></script>
            <!-- stepy wizard -->
            <script src="{skin}/lib/stepy/js/jquery.stepy.min.js"></script>
            <!-- fix for ios orientation change -->
            <script src="{js}/ios-orientationchange-fix.js"></script>
            <!-- jbreadcrumbs -->
            <script src="{skin}/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.js"></script>
            <!-- scrollbar -->
            <script src="{skin}/lib/antiscroll/antiscroll.js"></script>
            <script src="{skin}/lib/antiscroll/jquery-mousewheel.js"></script>
            <!-- mobile nav -->
            <script src="{js}/selectNav.js"></script>
            <!-- multiselect -->
            <script src="{skin}/lib/multi-select/js/jquery.multi-select.js"></script>
            <script src="{skin}/lib/multi-select/js/jquery.quicksearch.js"></script>
            <!-- smoke_js -->
            <script src="{skin}/lib/smoke/smoke.js"></script>
            <!-- toggle-buttons -->
            <script src="{skin}/lib/toggle_buttons/jquery.toggle.buttons.js"></script>
            <!-- common functions -->
            <script src="{js}/gebo_common.js"></script>

            <!-- responsive table -->
            <script src="{js}/jquery.mediaTable.min.js"></script>

            <!-- masked inputs -->
            <script src="{js}/forms/jquery.inputmask.min.js"></script>
             <!-- maskmoney -->
            <script src="{js}/maskmoney2.0.js"></script>
            <!-- timepicker -->
            <script src="{skin}/lib/datepicker/bootstrap-timepicker.min.js"></script>

            <!-- datetimepicker -->
            <script src="{js}/bootstrap-datetimepicker.min.js"></script>

            <!-- colorbox -->
            <script src="{skin}/lib/colorbox/jquery.colorbox.min.js"></script>

            <!-- datatable -->
            <script src="{skin}/lib/datatables/jquery.dataTables.min.js"></script>
            <script src="{js}/TableTools.min.js"></script>
            <script src="{js}/ZeroClipboard.js"></script>

            <!-- validation -->
            <script src="{skin}/lib/validation/jquery.validate.js"></script>
            <!-- tables functions -->
            <script src="{js}/gebo_tables.js"></script>
            <!-- fancytree -->
            <script src="{js}/jquery.hotkeys.js"></script>
            <script src="{skin}/lib/fancytree/fancytree.js"></script>
            <script src="{skin}/lib/fancytree/fancytree.edit.js"></script>
            <script src="{skin}/lib/fancytree/fancytree.contextmenu.js"></script>
            <script src="{skin}/lib/fancytree/fancytree.hotkeys.js"></script>
            <!-- floating list header functions -->
            <script src="{skin}/lib/floating_header/jquery.list.min.js"></script>
            <script src="{js}/gebo_floating_header.js"></script>

            <!--zorbit functions -->
            <script src="{js}/functions.js"></script>
            <script src="{js}/sistema.js"></script>

            <?php if(in_array($this->uri->segment(2), array('admin', ''))){ ?>

            <!-- elfinder core -->
            <script src="{js}/elFinder.js"></script>
            <script src="{js}/elFinder.version.js"></script>
            <script src="{js}/jquery.elfinder.js"></script>
            <script src="{js}/elFinder.resources.js"></script>
            <script src="{js}/elFinder.options.js"></script>
            <script src="{js}/elFinder.history.js"></script>
            <script src="{js}/elFinder.command.js"></script>

            <!-- elfinder ui -->
            <script src="{js}/ui/overlay.js"></script>
            <script src="{js}/ui/workzone.js"></script>
            <script src="{js}/ui/navbar.js"></script>
            <script src="{js}/ui/dialog.js"></script>
            <script src="{js}/ui/tree.js"></script>
            <script src="{js}/ui/cwd.js"></script>
            <script src="{js}/ui/toolbar.js"></script>
            <script src="{js}/ui/button.js"></script>
            <script src="{js}/ui/uploadButton.js"></script>
            <script src="{js}/ui/viewbutton.js"></script>
            <script src="{js}/ui/searchbutton.js"></script>
            <script src="{js}/ui/sortbutton.js"></script>
            <script src="{js}/ui/panel.js"></script>
            <script src="{js}/ui/contextmenu.js"></script>
            <script src="{js}/ui/path.js"></script>
            <script src="{js}/ui/stat.js"></script>
            <script src="{js}/ui/places.js"></script>

            <!-- elfinder commands -->
            <script src="{js}/commands/back.js"></script>
            <script src="{js}/commands/forward.js"></script>
            <script src="{js}/commands/reload.js"></script>
            <script src="{js}/commands/up.js"></script>
            <script src="{js}/commands/home.js"></script>
            <script src="{js}/commands/copy.js"></script>
            <script src="{js}/commands/cut.js"></script>
            <script src="{js}/commands/paste.js"></script>
            <script src="{js}/commands/open.js"></script>
            <script src="{js}/commands/rm.js"></script>
            <script src="{js}/commands/info.js"></script>
            <script src="{js}/commands/duplicate.js"></script>
            <script src="{js}/commands/rename.js"></script>
            <script src="{js}/commands/help.js"></script>
            <script src="{js}/commands/getfile.js"></script>
            <script src="{js}/commands/mkdir.js"></script>
            <script src="{js}/commands/mkfile.js"></script>
            <script src="{js}/commands/upload.js"></script>
            <script src="{js}/commands/download.js"></script>
            <script src="{js}/commands/edit.js"></script>
            <script src="{js}/commands/quicklook.js"></script>
            <script src="{js}/commands/quicklook.plugins.js"></script>
            <script src="{js}/commands/extract.js"></script>
            <script src="{js}/commands/archive.js"></script>
            <script src="{js}/commands/search.js"></script>
            <script src="{js}/commands/view.js"></script>
            <script src="{js}/commands/resize.js"></script>
            <script src="{js}/commands/sort.js"></script>
            <script src="{js}/commands/netmount.js"></script>

            <!-- elFinder translation (OPTIONAL) -->
            <script src="{js}/i18n/elfinder.pt_BR.js"></script>
            <?php } ?>
            <script type="text/javascript" charset="utf-8">

                $(document).ready(function() {

                    setTimeout( function() {
                        $("html").removeClass("js");
                        var htmlDoc = document.getElementsByTagName("html");
                        //console.log(htmlDoc[0].className.replace(/\bjs\b/,''));
                        htmlDoc[0].className = htmlDoc[0].className.replace(/\bjs\b/,'');
                    }, 1000);<?php

                if ($this->uri->segment(1) == 'cliente_servico' && $this->uri->segment(2) == 'contrato') {

                    echo 'window.print()';

                }

                if (!in_array($this->uri->segment(2), array('cadastro', 'cadastra'))) {?>

                    volume_armazenado();<?php

                    //LOGIN
                    if (in_array($this->uri->segment(1), array('admin','')) && $this->session->userdata('id_usuario') == '') {?>

                        $('div.navbar-fixed-top').hide();

                        //* boxes animation
                        form_wrapper = $('div.login_box');

                        function boxHeight() {

                            form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) }, 400);

                        };

                        // form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
                        $('.linkform a.modal_link,.link_reg a').on('click', function(e) {

                            var target        = $(this).attr('href'),
                                target_height = $(target).actual('height');

                            $(form_wrapper).css({

                                'height' : form_wrapper.height()

                            });

                            $(form_wrapper.find('div.form_container:visible')).fadeOut(400,function() {

                                form_wrapper.stop().animate({
                                    height   : target_height,
                                    // marginTop: ( - (target_height/2) - 24)
                                },500,function() {

                                    $(target).fadeIn(400);
                                    $('div.links_btm .linkform').toggle();
                                    $(form_wrapper).css({
                                        'height' : ''
                                    });

                                });

                            });

                            e.preventDefault();

                        });

                        //* validation
                        $('#login_form').validate({
                            onkeyup: false,
                            errorClass: 'error',
                            validClass: 'valid',
                            rules: {
                                login_usuario: { required: true, minlength: 3 },
                                senha_usuario: { required: true, minlength: 3 }
                            },
                            highlight: function(element) {
                                $(element).closest('div').addClass("f_error");
                                setTimeout(function() {
                                    boxHeight()
                                }, 200)
                            },
                            unhighlight: function(element) {
                                $(element).closest('div').removeClass("f_error");
                                setTimeout(function() {
                                    boxHeight()
                                }, 200)
                            },
                            errorPlacement: function(error, element) {
                                $(element).closest('div').append(error);
                            }
                        });

                        $('button#forgot_password_button').on("click", function() {

                            var post_data = {
                                user_email : $('#forgot_password_user_email').val()
                            };

                            $.ajax({

                                url: $('#url').val()+'admin/esqueci_senha',
                                type: 'POST',
                                data: post_data,
                                success: function(msg) {

                                    $('div#pass_form div.alert-success strong').html(msg);

                                    if ($('div#pass_form div.alert-success').css('display') == 'none') {

                                        $('div#pass_form div.alert-success').slideToggle(400);

                                    }

                                }

                            });

                        });<?php

                    }

                    if (($this->uri->segment(1) == 'usuario' && in_array($this->uri->segment(2), array('editar', 'meus_dados'))) ||
                        $this->uri->segment(1) == 'usuario_tipo') {?>

                        if ($('#id_telas')) $('#id_telas').multiSelect();<?php

                    }

                    if ($this->session->userdata('msg_erro') != '' && in_array($this->uri->segment(1), array('', 'admin'))) {?>

                        $.sticky('<?=$this->session->userdata('msg_erro')?>', {autoclose : 5000, position: "top-center", type: "st-info" });<?php

                        $this->session->set_userdata('msg_erro', '');//DEPOIS QUE MOSTRAR 1X NÃO MOSTRAR MAIS

                    }?>

                    mimes = {"formats" : [
                                "application/msword",
                                "application/msword",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
                                "application/vnd.ms-word.document.macroEnabled.12",
                                "application/vnd.ms-word.template.macroEnabled.12",
                                "application/vnd.ms-excel",
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
                                "application/vnd.ms-excel.sheet.macroEnabled.12",
                                "application/vnd.ms-excel.template.macroEnabled.12",
                                "application/vnd.ms-excel.addin.macroEnabled.12",
                                "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
                                "application/vnd.ms-powerpoint",
                                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                                "application/vnd.openxmlformats-officedocument.presentationml.template",
                                "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
                                "application/vnd.ms-powerpoint.addin.macroEnabled.12",
                                "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
                                "application/vnd.ms-powerpoint.template.macroEnabled.12",
                                "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
                                "application/pdf",
                                "text/plain"
                    ]};

                    var contains = function (a, obj) {

                        for (var i = 0; i < a.length; i++) {

                            if (a[i] === obj) {
                                return true;
                            }

                        }

                        return false;

                    }

                    <?php if (in_array($this->uri->segment(2), array('admin', ''))) {?>
		
                    if ($('#elfinder')) {

                        var $elfinder = $('#elfinder').elfinder({
                            url  : $('#url').val()+'admin/elfinder',
                            lang : 'pt_BR',
                            debug : false,
                            height : 450,
                            resizable : false,
                            requestType : 'post',
                            uiOptions : {
                                // toolbar configuration
                                toolbar : [
                                    ['back', 'forward'],
                                    ['reload'],
                                    ['home'],
                                    ['mkdir', 'upload'],
                                    ['open', 'download', 'getfile'],
                                    ['info'],
                                    ['quicklook'],
                                    ['copy', 'cut', 'paste','rm'],
                                    ['duplicate', 'rename', 'edit'],
                                    ['extract', 'archive'],
                                    ['search'],
                                    ['view']
                                ],

                                // directories tree options
                                tree : {
                                    // expand current root on init
                                    openRootOnLoad : false,
                                    // auto load current dir parents
                                    syncTree : true
                                },

                                // navbar options
                                navbar : {
                                    minWidth : 150,
                                    maxWidth : 500
                                },

                                // current working directory options
                                cwd : {
                                    // display parent directory in listing as ".."
                                    oldSchool : false
                                }
                            },
                            getFileCallback : function(data) {

                                if (contains( mimes.formats, data.mime)) {
                                var overlay = document.getElementById('loading_data');
                                overlay.style.display="block";
                                var obj = '    <div id="e-document"><span id="doc-close">fechar</span>'
                                             +'<object type="text/html" data="http://docs.google.com/viewer?url='+data.url+'&embedded=true" width="100%" height="100%">'
                                             +'</object>';
                                             +'</div>';

                                overlay.innerHTML=obj;
                                var btnclose = document.getElementById('doc-close');

                                btnclose.addEventListener('click', function(){
                                    overlay.style.display="none";
                                    overlay.innerHTML='';
                                });

                                } else {

                                    window.open(data.url);

                                }

                            }
                        });

                        window.onresize = function(){

                            var _height = this.innerHeight;
                            var _h = _height - (_height * 0.3);

                            if ($elfinder.height() != _h) {
                                $elfinder.height(_h);
                            }

                        }

                    }
                    <?php } ?>

                    rmdir = function () {

                        var tree = $(".tree").fancytree("getTree");
                            node = tree.getActiveNode();
                            id_diretorio_segmento = node.key;

                        $.ajax({
                            url  : $('#url').val()+'diretorio_segmento/excluir/',
                            data : {
                                id_tabela : id_diretorio_segmento.replace('id_','')
                            },
                            type : 'POST',
                            dataType : 'JSON',
                            success : function(retorno) {

                                //console.log('diretório excluido');
                                $.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });
                                node.remove();
                                //console.log(id_diretorio_segmento);
                                $('#'+id_diretorio_segmento).remove();

                            }
                        });

                    }

                    newdir = function () {

                        var tree = $(".tree").fancytree("getTree"),
                            dir_name = "Novo diretorio",
                            node = tree.getActiveNode(),
                            newData = {title: dir_name, folder : true},
                            newSibling = node.addChildren(newData);
                        var id_segmento = $('#id_segmento').val();
                        var id_diretorio_segmento_pai = node.key;
                        $.ajax({
                                url  : $('#url').val()+'diretorio_segmento/inserir/',
                                data : {
                                    id_diretorio_segmento_pai : id_diretorio_segmento_pai.replace('id_', ''),
                                    id_segmento : id_segmento,
                                    nome_diretorio_segmento : dir_name,
                                    _tree : 1
                                },
                                type : 'POST',
                                dataType : 'JSON',
                                success : function(retorno) {

                                    $.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

                                    $.ajax({
                                        url  : $('#url').val()+'segmento/getdiretoriosegmento/'+retorno.id_diretorio_segmento,
                                        type : 'GET',
                                        success    : function (dados_retorno){
                                            var obj = $('#'+id_diretorio_segmento_pai + ' ul');
                                            //console.log(obj.length);
                                            if (obj.length == 0) {
                                                alert('node não existe');
                                                var ulist = document.createElement('ul');
                                                var ilist = document.getElementById(id_diretorio_segmento_pai);
                                                    ilist.appendChild(ulist);

                                                $('#'+id_diretorio_segmento_pai+' ul').append(dados_retorno);

                                            } else {
                                                $('#'+id_diretorio_segmento_pai + ' ul').append(dados_retorno);
                                            }
                                            //console.log($('#'+id_diretorio_segmento_pai+ ' ul'));
                                            var tree = $(".tree").fancytree("getTree");
                                            tree.reload();

                                        }
                                    });
                                }
                            }
                        );
                    };

                    newrootdir = function () {
                        var rootNode = $(".tree").fancytree("getRootNode"),
                            dir_name = "Novo diretorio",
                            newData = {title: dir_name, folder : true},
                            childNode = rootNode.addChildren(newData);
                            //console.log(childNode);
                        var id_segmento = $('#id_segmento').val();
                        var id_diretorio_segmento_pai = null;

                        $.ajax({
                                url  : $('#url').val()+'diretorio_segmento/inserir/',
                                data : {
                                    id_diretorio_segmento_pai : id_diretorio_segmento_pai,
                                    id_segmento : id_segmento,
                                    nome_diretorio_segmento : dir_name,
                                    _tree : 1
                                },
                                type : 'POST',
                                dataType : 'JSON',
                                success : function(retorno) {

                                    $.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

                                    $.ajax({
                                        url  : $('#url').val()+'segmento/getdiretoriosegmento/'+retorno.id_diretorio_segmento,
                                        type : 'GET',
                                        success    : function (dados_retorno){
                                            $('.tree ul.ui-fancytree-source.ui-helper-hidden').append(dados_retorno);
                                            var tree = $(".tree").fancytree("getTree");
                                            tree.reload();

                                        }
                                    });

                                }
                            }
                        );
                    };

                    $.ui.fancytree.debugLevel = 1; // silence debug output

                    $(".tree").contextmenu({
                            delegate: "span.fancytree-title",
                            //menu: "#options",
                            menu: [
                                    {title: "Novo Diretório", cmd: "newdir", uiIcon: "ui-icon-folder-collapsed"},
                                    {title: "Excluir", cmd: "rmdir", uiIcon: "ui-icon-trash" }
                                    ],
                            beforeOpen: function(event, ui) {
                                //console.log(ui);
                                var node = $.ui.fancytree.getNode(ui.target);
                                //node.setFocus();
                                node.setActive();
                            },
                            select: function(event, ui) {
                                var fnc = ui.cmd;
                                var fn = window[fnc];
                                if (typeof fn === "function") fn();
                            },
                        }
                    );

                    $(".tree").fancytree({
                        checkbox : false,
                        extensions: ["edit"],
                        edit: {
                            beforeEdit: function(event, data) {
                            },
                            save: function(event, data) {

                                var id_segmento = $('#id_segmento').val();
                                var dir_name = data.value;
                                var id_diretorio_segmento = data.node.key.replace('id_', '');

                                $.ajax({
                                    url  : $('#url').val()+'diretorio_segmento/alterar/',
                                    data : {
                                        id_diretorio_segmento : id_diretorio_segmento,
                                        id_segmento : id_segmento,
                                        nome_diretorio_segmento : dir_name,
                                        _tree    : 1
                                    },
                                    type : 'POST',
                                    dataType : 'JSON',
                                    success : function(retorno) {

                                        var id_diretorio = id_diretorio_segmento;
                                        $.sticky(retorno.msg, {autoclose : 5000, position: "top-center", type: "st-success" });

                                        $.ajax({
                                            url  : $('#url').val()+'segmento/getdiretorionome/'+id_diretorio,
                                            type : 'GET',
                                            success    : function (dados_retorno){
                                                $('#id_'+id_diretorio).text(dados_retorno);
                                                var tree = $(".tree").fancytree("getTree");
                                                tree.reload();

                                            }
                                        });

                                    }

                                });

                            }
                        }
                    });

                    if ($("#permissao_diretorio")) {

                        _canLog = false;
                        _id     = '';

                        $("#permissao_diretorio").fancytree({
                            checkbox: false,
                            selectMode: 1,
                            beforeActivate: function(event, data) {

                                var myRadio = $('input[name=usuario_diretorio]');
                                var checkedValue = myRadio.filter(':checked').attr('id');

                                if (checkedValue == undefined){

                                    alert('Selecione um usuário para carregar as permissões do do diretório');
                                    return false;

                                } else {

                                    _id = checkedValue;

                                }

                            },
                            activate: function(event, data) {

                                // A node was activated: display it's title:
                                var node = data.node;
                                post_data = {
                                    id_usuario : _id,
                                    id_diretorio : node.key
                                }

                                $('#id_diretorio').val(node.key.replace('id_', ''));

                                loadPermissions(post_data);

                            },
                        });

                    }

                    var loadPermissions = function(post_data) {

                        $.ajax({
                            url: $('#url').val()+'cliente/get_dir_permission',
                            type: 'POST',
                            data: post_data,
                            dataType: "JSON",
                            success: function(msg) {

                                var objSize = msg.length;

                                if (objSize != 1) {

                                    alert('Erro ao tentar buscar permissões. Por favor contate o administrador do sistema');
                                    return false;

                                } else {

                                    $('#id_usuario_diretorio').val(msg[0].id_usuario_diretorio);

                                    $("#ler_usuario_diretorio").val(msg[0].bloquear_usuario_diretorio);
                                    $("#tb-ler_usuario_diretorio").toggleButtons('setState', (msg[0].ler_usuario_diretorio == 1 ? true : false));

                                    $("#escrever_usuario_diretorio").val(msg[0].bloquear_usuario_diretorio);
                                    $("#tb-escrever_usuario_diretorio").toggleButtons('setState', (msg[0].escrever_usuario_diretorio == 1 ? true : false));

                                    $("#esconder_usuario_diretorio").val(msg[0].bloquear_usuario_diretorio);
                                    $("#tb-esconder_usuario_diretorio").toggleButtons('setState', (msg[0].esconder_usuario_diretorio == 1 ? true : false));

                                    $("#bloquear_usuario_diretorio").val(msg[0].bloquear_usuario_diretorio);
                                    $("#tb-bloquear_usuario_diretorio").toggleButtons('setState', (msg[0].bloquear_usuario_diretorio == 1 ? true : false));

                                }

                            }

                        });

                    }

                    //A CADA 10 MIN BUSCA O VALOR ATUALIZADO = 600 segundos
                    setInterval('volume_armazenado()', 1000 * 600);

                <?php } else { ?>

                    if ($('#simple_wizard')) {

                        $('#simple_wizard').stepy({
                            titleClick : true,
                            nextLabel  : 'Próximo <i class="icon-chevron-right icon-white"></i>',
                            backLabel  : '<i class="icon-chevron-left"></i> Voltar'
                        });

                    }

                    var fill_contract = function() {

                        $('input, select').each(function( index ) {

                            var el_id = $(this).attr('id');

                            if ($(this).attr('type') == 'text' || $(this).attr('type') == 'number' && $(this).attr('type') != 'password') {

                                $('#c_'+$(this).attr('id')).html($(this).val());

                            } else if($(this).prop('tagName') == 'select'){

                                $('#c_'+el_id).html($('#'+el_id+' option:selected').text());

                            } else if($(this).prop('type') == 'checkbox'){

                                if ($(this).is(':checked')) {

                                    $('#c_'+el_id).html(' x ');

                                }

                            } else if($(this).attr('type') == 'hidden') {

                                if (document.getElementById('val_'+el_id) != undefined) {

                                    $('#c_val_'+el_id).html($('#val_'+el_id).text());
                                    $('#c_valor_'+el_id).html($('#val_valor_'+el_id).text());

                                }

                            }

                            $('#contrato_cliente_servico').val($('.contrato').html());

                        });

                  };

                  $('select, input[type="checkbox"]').change(function(){
                        fill_contract();
                  });

                  $('input').blur(function(){
                        fill_contract();
                  });

                    fill_contract();
                <?php }?>
                });
            </script>