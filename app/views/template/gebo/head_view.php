        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>GED - Central de Notas</title>

        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="{skin}/bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="{skin}/bootstrap/css/bootstrap-responsive.min.css" />
        <!-- nice form elements -->
            <link rel="stylesheet" href="{skin}/lib/jquery-ui/css/Aristo/Aristo.css" />
            <link rel="stylesheet" href="{skin}/lib/uniform/Aristo/uniform.aristo.css" />
        <!-- tooltips-->
            <link rel="stylesheet" href="{skin}/lib/qtip2/jquery.qtip.min.css" />
        <!-- notifications -->
            <link rel="stylesheet" href="{skin}/lib/sticky/sticky.css" />
        <!-- stepy wizard -->
            <link rel="stylesheet" href="{skin}/lib/stepy/css/jquery.stepy.css" />
        <!-- colorbox -->
            <link rel="stylesheet" href="{skin}/lib/colorbox/colorbox.css" />
        <!-- breadcrumbs-->
            <link rel="stylesheet" href="{skin}/lib/jBreadcrumbs/css/BreadCrumb.css" />
        <!-- multiselect -->
            <link rel="stylesheet" href="{skin}/lib/multi-select/css/multi-select.css" />
        <!-- gebo color theme-->
            <link rel="stylesheet" href="{css}/blue.css" id="link_theme" />
        <!-- datepicker -->
            <link rel="stylesheet" href="{skin}/lib/datepicker/datepicker.css" />
        <!-- smoke_js -->
            <link rel="stylesheet" href="{skin}/lib/smoke/themes/gebo.css" />
        <!-- toggle-butons -->
            <link rel="stylesheet" href="{skin}/lib/toggle_buttons/bootstrap-toggle-buttons.css" />
        <!-- tabletools -->
            <link rel="stylesheet" href="{skin}/css/TableTools.css" />

        <link rel="stylesheet" href="{skin}/lib/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">

        <!-- fancytree -->
            <link rel="stylesheet" href="{skin}/lib/fancytree/skin-lion/ui.fancytree.css">
        <!-- simple-slider  -->
              <link href="{css}/simple-slider.css" rel="stylesheet" type="text/css" />
              <link href="{css}/simple-slider-volume.css" rel="stylesheet" type="text/css" />
        <!-- main styles -->
            <link rel="stylesheet" href="{css}/style.css" />
            <link rel="stylesheet" href="{css}/comp_paginacao.css" />

        <!-- Favicon -->
            <link rel="shortcut icon" href="{skin}/favicon.ico" />

        <!--[if lte IE 8]>
            <link rel="stylesheet" href="{css}/ie.css" />
            <script src="{js}/ie/html5.js"></script>
            <script src="{js}/ie/respond.min.js"></script>
        <![endif]-->

        <script>
            //* hide all elements & show preloader
            document.documentElement.className += 'js';
        </script>
