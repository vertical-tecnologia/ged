<form name="frm_grid" id="frm_grid" action="{url}{tela}/excluir" method="post" class="grid" enctype="multipart/form-data" onsubmit="return true">
    {botoes}
    <div style="clear:both"></div>
    <div id="central">
        {filtro}
        <table border="0" cellspacing="0" cellpadding="5" width="100%" id="grid" class="table table-striped table-bordered mediaTable">
            <thead>
                <tr>
                    <th class="no-sort table_checkbox persist"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="frm_grid" /></th>
                    {vet_col}
                        <th class="col-sortable"><span class="optional">{desc_coluna}</span></th>
                    {/vet_col}<?php
                    if (!empty($col_especial)) {?>
                        {col_especial}
                            <th><span class="optional">{desc_coluna}</span></th>
                        {/col_especial}<?php
                    }?>
                </tr>
            </thead>
            <tbody><?php
            if (!empty($vet_dados)) {?>
                {vet_dados}
                    <tr>
                        <td><input type="checkbox" id="id_tabela_{i}" name="id_tabela[]" value="{id_dado}" class="row_sel" /></td>
                        {dados_col}
                            <td><span><a href="{url_padrao}" title="{title_dado}" class="ext_disabled">{nome_dado}</a></span></td>
                        {/dados_col}<?php
                        if (!empty($botoes_especiais)) {?>
                            {botoes_especiais}
                                <td class="no-sort">
                                    <span class="nome_dado">
                                        <a href="{link}" class="ext_disabled">{nome_link}</a>
                                    </span>
                                </td>
                            {/botoes_especiais}<?php
                        }?>
                    </tr>
                {/vet_dados}<?php
            }?>
            </tbody><?php
            if (!empty($rodape_col)) {?>
            <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    {rodape_col}
                        <td colspan="{col_colspan}"><span class="text-info {col_align}">{nome_dado}</span></td>
                    {/rodape_col}
                </tr>
            </tfoot><?php
            }?>
        </table>
        <input type="hidden" name="total_reg" id="total_reg" value="<?=count($vet_dados)?>" />
    </div>

    <br />
    <br />

    <div id="total_mostrar">
        <select name="total_limite" id="total_limite" onchange="buscar('<?=$this->uri->segment(1)?>')" class="span1">
            <option value="10"    <?=$this->input->post('total_limite') == 10      ? 'selected="selected"' : ''?>>10</option>
            <option value="20"    <?=$this->input->post('total_limite') == 20      ? 'selected="selected"' : ''?>>20</option>
            <option value="50"    <?=$this->input->post('total_limite') == 50      ? 'selected="selected"' : ''?>>50</option>
            <option value="100"   <?=$this->input->post('total_limite') == 100     ? 'selected="selected"' : ''?>>100</option>
            <option value="TODOS" <?=$this->input->post('total_limite') == 'TODOS' ? 'selected="selected"' : ''?>>TODOS</option>
        </select>
    </div>

    <div id="paginacao">
        {paginacao}&nbsp;
    </div>

    <div id="total_display"><?php
        if ($tot_fim > 0) {?>
            <strong>{tot_ini} - {tot_fim} de {total} registros!</strong><?php
        }?>
    </div>

</form>
