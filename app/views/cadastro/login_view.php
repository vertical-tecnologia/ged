<!-- start pricing -->
<section id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow bounceIn">
                <h2 class="text-uppercase">Login</h2>
            </div>

            <div class="col-md-6 col-md-offset-3 wow fadeIn" data-wow-delay="0.6s">
                <form role="form" action="{url}auth/login" method="post" id="reg-form">
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" id="login_usuario" placeholder="Usuário" required name="login_usuario">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <input type="password" class="form-control input-lg" id="senha_usuario" name="senha_usuario" placeholder="Senha" required>
                            </div>
                        </div>
                    </div>
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-block btn-lg" href="{url}forgot">Esqueci minha senha</a>
                        </div>
                        <div class="col-xs-12 col-md-6 pull-right"><input type="submit" value="Entrar" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- end pricing -->