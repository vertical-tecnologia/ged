<!-- start pricing -->
<section id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow bounceIn">
                <h2 class="text-uppercase">Esqueceu sua senha?</h2>
            </div>

            <div class="col-md-6 col-md-offset-3 wow fadeIn" data-wow-delay="0.6s">
                <form role="form" action="{url}forgot" method="post" id="reg-form">
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="login_usuario">
                                    Informe seu email para rederinir sua senha.
                                </label>
                                <input type="text" class="form-control input-lg" id="email" placeholder="email@example.com" required name="email">
                            </div>
                        </div>
                    </div>

                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-block btn-lg" href="{url}login">Login</a>
                        </div>
                        <div class="col-xs-12 col-md-6 pull-right"><input type="submit" value="Redefinir Senha" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- end pricing -->