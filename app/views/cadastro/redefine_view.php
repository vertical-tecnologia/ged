<!-- start pricing -->
<section id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow bounceIn">
                <h2 class="text-uppercase">Redefinir minha senha</h2>
            </div>

            <div class="col-md-6 col-md-offset-3 wow fadeIn" data-wow-delay="0.6s">
                <form role="form" action="{url}reset/{key}" method="post" id="reg-form">
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="login_usuario">
                                    Informe sua nova senha
                                </label>
                                <input type="password" class="form-control input-lg" id="password" placeholder="Senha" required name="password">
                            </div>

                            <div class="form-group">
                                <label for="login_usuario">
                                    Confirme sua nova senha
                                </label>
                                <input type="password" class="form-control input-lg" id="confirmation" placeholder="Confirmaçao da Senha" required name="confirmation">
                            </div>
                        </div>
                    </div>

                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-block btn-lg" href="{url}login">Login</a>
                        </div>
                        <div class="col-xs-12 col-md-6 pull-right"><input type="submit" value="Redefinir Senha" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- end pricing -->