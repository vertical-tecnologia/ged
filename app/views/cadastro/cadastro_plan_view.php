<!-- start pricing -->
<section id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow bounceIn">
                <h2 class="text-uppercase">Cadastre-se</h2>
            </div>
            {planos}
            <div class="col-md-4 wow fadeIn" data-wow-delay="0.6s">
                <div class="pricing text-uppercase">
                    <div class="pricing-title">
                        <h4>{plan_name}</h4>
                        <p>{plan_price}</p>
                        <small class="text-lowercase">mensal</small>
                    </div>
                    <ul>
                        {servicos}
                        <li>{service_name}</li>
                        {/servicos}

                    </ul>
                </div>
            </div>
            {/planos}
            <div class="col-md-8 wow fadeIn" data-wow-delay="0.6s">
                <form role="form" action="{url}cadastro/save" method="post" id="reg-form">
                    <hr class="colorgraph">
                    <div class="row">
                        <input type="hidden" name="plan_id" id="plan_id" value="{plan_id}">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="user_fullname" id="user_fullname" class="form-control input-lg" placeholder="Nome completo" tabindex="1">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="user_login" id="user_login" class="form-control input-lg" placeholder="Login" tabindex="2">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="user_email" id="user_email" class="form-control input-lg" placeholder="Email" tabindex="4">
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="user_password" id="user_password" class="form-control input-lg" placeholder="Senha" tabindex="5">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="user_password_confirmation" id="user_password_confirmation" class="form-control input-lg" placeholder="Confirme a Senha" tabindex="6">
                            </div>
                        </div>
                    </div>
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-12 col-md-6"><input type="submit" value="Cadastrar" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- end pricing -->