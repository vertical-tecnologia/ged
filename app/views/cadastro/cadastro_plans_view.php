<!-- start home -->
<section id="home">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 wow fadeIn" data-wow-delay="0.3s">
                    <h1 class="text-upper">{home_title}</h1>
                    <p class="tm-white">{home_subtitle}</p>
                    <img src="{skin}/images/ged.png" class="img-responsive" alt="home img">
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>
</section>
<!-- end home -->
<!-- start feature1 -->
<section id="feature1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                <img src="{skin}/images/filemanager.png" class="img-responsive" alt="feature img">
            </div>
            <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                <h2 class="text-uppercase">Recursos</h2>
                <p>Veja alguns recusos do GED.</p>
                <p><i class="fa fa-hdd-o"></i>Armazenamento digital.</p>
                <p><i class="fa fa-database"></i>Backup Automático.</p>
                <p><i class="fa fa-lock"></i>Controle de acesso de arquivos</p>
                <p><i class="fa fa-share"></i>Compartilhamento de arquivos e pastas</p>
            </div>
        </div>
    </div>
</section>
<!-- end feature1 -->

<!-- start pricing -->
<section id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow bounceIn">
                <h2 class="text-uppercase">Nossos Planos</h2>
            </div>
            {planos}
            <div class="col-md-3 wow fadeIn" data-wow-delay="0.6s">
                <div class="pricing text-uppercase">
                    <div class="pricing-title">
                        <h4>{plan_name}</h4>
                        <p>{plan_price}</p>
                        <small class="text-lowercase">mensal</small>
                    </div>
                    <ul>
                        {servicos}
                        <li>{service_name}</li>
                        {/servicos}

                    </ul>
                    <a href="{url}cadastro/plano/{plan_id}" class="btn btn-primary text-uppercase">Escolher</a>
                </div>
            </div>
            {/planos}

            <div class="clearfix"></div>
            <div class="well">
                * Cobrado apenas no ato da contratação
            </div>
        </div>
    </div>
</section>
<!-- end pricing -->

<!-- start download -->
<section id="register">
    <div class="container">
        <div class="row">
            <div class="col-md-6 wow fadeInLeft soft-scroll" data-wow-delay="0.6s">
                <h2 class="text-uppercase">Cadastre-se e use.</h2>
                <p>Use o GED para gerenciar os arquivos de sua empresa. Tenha controle sobre os usuários e arquivos. Compartilhe e gerencie informações de forma fácil e eficiente.</p>
                <a class="btn btn-primary text-uppercase" href="#pricing"><i class="fa fa-sign-in"></i> Escolha seu plano</a>

            </div>
            <div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
                <img src="{skin}/images/software-img.png" class="img-responsive" alt="feature img">
            </div>
        </div>
    </div>
</section>
<!-- end download -->

<!-- start contact -->
<section id="contact">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                    <h2 class="text-uppercase">Entre em Contato</h2>
                    <p>Quer falar conosco e tirar suas dúvidas? Entre em contato pelo endereço abaixo ou pelo forumlário ao lado. </p>
                    <address>
                        <p><i class="fa fa-map-marker"></i>AV.: Hygino Muzzy Filho, 529 Sala 105 , Marília -SP , Brasil</p>
                        <p><i class="fa fa-phone"></i> (14) 3221-9313 / (14) 3306-9180</p>
                        <p><i class="fa fa-envelope-o"></i> contato@centraldenotas.com.br</p>
                    </address>
                </div>
                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="contact-form">
                        <form action="#" method="post">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Nome">
                            </div>
                            <div class="col-md-6">
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Assunto">
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control" placeholder="Mensagem" rows="4"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="form-control text-uppercase" value="Enviar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end contact -->
