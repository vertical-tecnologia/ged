<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/usuarios"> <i class="fa fa-user"></i> Meus dados</a>
        </li>
    </ul>
</div>
{user}
<div class="row">
    <div class="col-lg-4">
        <div class="hpanel hblue">
            <div class="panel-body">
                    <form action="{url}perfil/image" method="post" id="user_picture_form" enctype="multipart/form-data">
                        <input type="file" id="user_picture" name="user_picture" class="hide" accept="image/x-png, image/gif, image/jpeg" onchange="imageControl.preview(this)"/>
                        <img alt="Foto de perfil" class="img-avatar" id="uploadPreview" src="{user_picture}" onclick="imageControl.select('user_picture')">
                    </form>
                <h3><a>{user_fullname}</a></h3>
                {main_address}
                <div class="text-muted font-bold m-b-xs">{endereco_cidade}, {endereco_estado}</div>
                {/main_address}
                <input type="hidden" id="parent_field" value="user_id"/>
                <input type="hidden" id="user_id" name="user_id" value="<?=$this->session->userdata('USER_ID')?>"/>
            </div>
            <?php if($type_id != 1 && $type_id != 6): ?>
            <div class="panel-body margin-top-1em">
                <legend>Empresas vinculadas</legend>
                {empresa}
                    <h5>{empresa_razao}</h5>
                {/empresa}
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="col-lg-8">

        <div class="hpanel hblue">
            <div class="panel-body">
                <h3>Dados Pessoais <small class="pull-right">({verified})</small></h3>
                <hr/>
                <table>
                    <tbody>
                        <tr>
                            <td class="table-desc">Email:</td>
                            <td>{user_email}</td>
                        </tr>
                        <tr>
                            <td class="table-desc">Data de cadastro:</td>
                            <td>{user_register_date}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="hpanel margin-top-1em">
            <div class="panel-body">
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Alterar minha senha</button>
            </div>
        </div>
        <div class="hpanel margin-top-1em">
            <div class="panel-body">
                <div class="w-box" id="w_sort04">
                    <div class="w-box-header">
                        Endereços

                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalUsuarioEndereco">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="w-box-content">
                        <table class="table table-condensed" id="table_endereco">
                            <tbody>
                            {enderecos}
                            <tr>
                                <td>{endereco_alias}</td>
                                <td>{endereco_cidade}</td>
                                <td class="text-right">
                                    <a href="{url}endereco/excluir/{endereco_id}/usuario">excluir</a>
                                </td>
                            </tr>
                            {/enderecos}
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="w-box" id="w_sort04">
                    <div class="w-box-header">
                        Contatos

                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalUsuarioContato">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="w-box-content">
                        <table class="table table-condensed" id="table_contato">
                            <tbody>
                            {contatos}
                            <tr>
                                <td>{tipo_desc}</td>
                                <td>{contato_valor}</td>
                                <td  class="text-right">
                                    <a href="{url}contato/excluir/{contato_id}/perfil" >excluir</a>
                                </td>
                            </tr>
                            {/contatos}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO'): ?>
        <div class="hpanel margin-top-1em">
            <div class="panel-body">
                <a class="btn btn-xs btn-primary pull-right" href="{url}conta/encerrar">Encerrar minha conta</a>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
{/user}

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{url}perfil/update_pass" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Minha Senha</h4>
            </div>
            <div class="modal-body">

                    <div class="form-group">
                        <label for="actual_password">Senha Atual</label>
                        <input type="password" class="form-control" id="actual_password" name="actual_password" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="new_password">Nova Senha</label>
                        <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="cnew_password">Confirme a Nova Senha</label>
                        <input type="password" class="form-control" id="cnew_password" name="cnew_password" placeholder="Email">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar Senha</button>
            </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modalUsuarioContato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="{url}contato/save/usuario" id="modal-form" enctype="application/x-www-form-urlencoded">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" id="myModalContent">
                    <div class="modal-content-form">
                        <div class="col-sm-12 col-md-12">

                            <input type="hidden" name="usuario_id" id="usuario_id" value="{usuario_id}"/>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <label for="tipo_id">Tipo de Contato </label>
                                    <select name="tipo_id" id="tipo_id" class="form-control chosen-select" data-placeholder="Selecione o tipo de contato">
                                        <option></option>
                                        {type}
                                        <option value="{tipo_id}">{tipo_desc}</option>
                                        {/type}
                                    </select>

                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <label for="contato_valor">Contato</label>
                                    <input type="text" name="contato_valor" id="contato_valor" value="" class="form-control" maxlength="45" placeholder="" autofocus/>
                                    <?=form_error('contato_valor');?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="mBtn-close" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="mBtn-save" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>



<div class="modal fade" id="modalUsuarioEndereco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="{url}endereco/save/usuario" id="modal-form" enctype="application/x-www-form-urlencoded">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" id="myModalContent">
                    <div class="modal-content-form">
                        <div class="col-sm-12 col-md-12">
                            <form method="POST" id="modal-form" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="user_id" id="user_id" value="{usuario_id}"/>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_alias">Apelido </label>
                                        <input type="text" name="endereco_alias" id="endereco_alias" value="" class="form-control" maxlength="45" placeholder="Ex: casa, escritório" autofocus/>
                                        <?=form_error('endereco_alias');?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_cep">CEP</label>
                                        <input type="text" name="endereco_cep" id="endereco_cep" value="" class="form-control cep" maxlength="9" placeholder="CEP: 00000-000" onblur="gedcep.search(this)"/>
                                        <?=form_error('endereco_cep');?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-9">
                                        <label for="endereco_logradouro">Logradouro </label>
                                        <input type="text" name="endereco_logradouro" id="endereco_logradouro" value="" maxlength="155" class="form-control" placeholder="Ex: Av, Rua, Alameda"/>
                                        <?=form_error('endereco_logradouro');?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <label for="endereco_num">Número</label>
                                        <input type="text" name="endereco_num" id="endereco_num" value="" maxlength="10" class="form-control" placeholder="Número"/>
                                        <?=form_error('endereco_num');?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_bairro">Bairro </label>
                                        <input type="text" name="endereco_bairro" id="endereco_bairro" value="" maxlength="155" class="form-control" placeholder="Bairro"/>
                                        <?=form_error('endereco_bairro');?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label for="endereco_cidade">Cidade</label>
                                        <input type="text" name="endereco_cidade" id="endereco_cidade" value="" maxlength="155" class="form-control" placeholder="Cidade"/>
                                        <?=form_error('endereco_cidade');?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div>
                                            <label for="endereco_estado">Estado </label>
                                            <input type="text" name="endereco_estado" id="endereco_estado" value="" maxlength="2" class="form-control" placeholder="Estado"/>
                                            <?=form_error('endereco_estado');?>
                                        </div>
<!--                                        <div>-->
<!--                                            <input type="checkbox" name="endereco_principal" id="endereco_principal" value="1" />-->
<!--                                            <label for="endereco_principal" class="checkbox"> Endereço principal </label>-->
<!--                                        </div>-->
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <label for="endereco_complemento">Complemento</label>
                                        <textarea type="text" name="endereco_complemento" id="endereco_complemento" maxlength="155" class="form-control" placeholder="Ex: próximo ao supermercado."></textarea>
                                        <?=form_error('endereco_complemento');?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="mBtn-close" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="mBtn-save" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>
