<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}mensagens" class="ext_disabled"> <i class="fa fa-mail"></i> Mensagens</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Mensagens</h3>
    </div>
</div>


<!--<div class="row">-->
<!--    <a href="#" class="btn btn-info  send-message-btn" role="button"><i class="fa fa-search"></i> Search</a>-->
<!--    <a href="#" class="btn btn-info  send-message-btn" role="button"><i class="fa fa-gears"></i> Settings</a>-->
<!--</div>-->
<div class="row">
    <div class="conversation-wrap col-md-3">
        {usuarios}

        <div class="media conversation{selected}">

            <a class="pull-left" href="{url}mensagens/user/{user_login}">
                <img src="{user_picture}" alt="" class="media-object img-circle"  style="width: 43px; height: 43px;"/>
            </a>
            <div class="media-body">
                <a href="{url}mensagens/user/{user_login}">
                <h5 class="media-heading">{user_fullname}</h5>
                <small class="text-muted">{user_login}</small>
                </a>
            </div>

        </div>
        {/usuarios}

    </div>



    <div class="message-wrap col-md-8">
        <div class="msg-wrap">
            {toUser}
            <h2>Enviar mensagem para <strong>{user_fullname}</strong></h2>
            {/toUser}
            <span class="badge bage-info">{error}</span>
            {messages}

            <div class="media item-msg">
                <div class="media-left media-middle">
                    <a href="#">
                        <img class="media-object img-circle" style="width: 32px; height: 32px;" src="{user_picture}">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{user_fullname} <small class="pull-right time"><i class="fa fa-clock-o"></i> {sent}</small></h4>
                    {content}
                </div>
            </div>

            {/messages}
        </div>
        <form action="{url}mensagens/send/{actual}" method="post">
        <div class="send-wrap ">
            <input type="hidden" id="from_user" name="from_user" value="{from_user}"/>
            <input type="hidden" id="to_user" name="to_user" value="{to_user}"/>
            <textarea class="form-control send-message" name="content" rows="3" placeholder="Escreva uma resposta..." {disabled}></textarea>
        </div>
            <div class="clearfix"></div>
        <div class="btn-panel">
            <button class="btn btn-info pull-right" role="button" {disabled}>
                <i class="fa fa-share"></i> Enviar Mensagem
            </button>
        </div>
        </form>
    </div>
</div>