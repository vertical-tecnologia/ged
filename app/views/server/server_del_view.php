<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/servidor"> <i class="fa fa-server"></i> Servidores</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Excluir Servidor</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
            <h3>Você tem certeza que deseja excluir esse servidor?</h3>
            {servidor}
            <div class="formSep">
                <div class="col-m-12 col-md-6">
                    <div id="storage1" class="{storage_1_hidden} sbox">
                        <fieldset>
                            <legend>Conexão Local</legend>
                            <div class="col-sm-12 col-md-12">
                                <label for="server_path">Caminho do diretório no servidor</label>
                                <input type="text" class="form-control" id="server_path" name="server_path" value="{server_path}" placeholder="Exemplo: /home/usuario/public_html" disabled/>
                            </div>
                        </fieldset>

                    </div>
                    <div id="storage2" class="{storage_2_hidden} sbox">
                        <fieldset>
                            <legend>Amazom S3</legend>
                            <div class="col-sm-12 col-md-12">
                                <label for="server_key">AMAZOM KEY</label>
                                <input type="text" class="form-control" id="server_key" name="server_key" value="{server_key}"placeholder="Tokem de acesso ao servidor S3" disabled/>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <label for="server_secret">AMAZOM SECRET</label>
                                <input type="text" class="form-control" id="server_secret" name="server_secret" value="{server_secret}" placeholder="Chave de segurança" disabled/>
                            </div>
                        </fieldset>
                    </div>
                    <div id="storage3" class="{storage_3_hidden} sbox">
                        <fieldset>
                            <legend>FTP</legend>
                            <div class="col-sm-8 col-md-8">
                                <label for="server_address">Endereço do servidor:</label>
                                <input type="text" class="form-control" id="server_address" name="server_address" value="{server_address}" placeholder="Exemplo: ftp.ged.com.br" disabled/>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <label for="server_port">Porta:</label>
                                <input type="text" class="form-control" id="server_port" name="server_port" value="{server_port}" placeholder="Exemplo: 21" disabled/>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label for="server_username">Usuário</label>
                                <input type="text" class="form-control" id="server_username" name="server_username" value="{server_username}" autocomplete="off" placeholder="Exemplo: usuarioftp" disabled/>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label for="server_password">Senha</label>
                                <input type="password" class="form-control" id="server_password" name="server_password" value="{server_password}" autocomplete="off" placeholder="Senha *******" disabled/>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="col-m-12 col-md-6">
                    <h4>Se voce excluir esse sevidor:</h4>
                    <ul>
                        <li>Os clientes vinculados ao servidor perderão o acesso aos seus arquivos.</li>
                        <li>Os ambientes podem parar de funcionar.</li>
                        <li>Os planos vinculados ao servidor não funcionarão corretamente.</li>

                    </ul>

                    <ul>
                        <li>Nenhum arquivo será excluído.</li>
                        <li>Nenhum plano será excluído.</li>
                        <li>Nenhum cliente/usuário será excluído</li>
                    </ul>

                    <p>Se ainda assim você tem certeza que é seguro excluir esse servidor clique em confirmar exclusão.</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12 col-md-12">
                <a href="{url}servidor/confirma_exclusao/{server_id}" class="btn btn-danger pull-right">Confirmar Exclusão</a>
            </div>
        {/servidor}

    </div>
</div>