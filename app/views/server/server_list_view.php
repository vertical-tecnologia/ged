<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}servidor"> <i class="fa fa-server"></i> Servidores</a>
        </li>
    </ul>
</div>

<div class="row sepH_c">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Servidores</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        {button_add}
    </div>
</div>

<?php if (isset($mensagem)) :?>
<div class="row">
    <?php $class = ($mensagem['sucesso'] == true) ? 'success' : 'danger';?>
    <div class="col-md-12 alert alert-<?php echo $class;?>">
        <?php echo $mensagem['mensagem'];?>
    </div>
</div>
<?php endif;?>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
                <th>#</th>
                <th>Tipo</th>
                <th class="text-right"><i class="fa fa-cog"></i></th>
            </thead>

            <tbody>
            {servers}
            <tr>
                <td>{server_id}</td>
                <td>{server_type_desc}</td>
                <td class="text-right">
                    {link_upd}
                    {link_del}
                </td>
            </tr>
            {/servers}
            </tbody>
        </table>
    </div>
</div>
