<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/servidor"> <i class="fa fa-server"></i> Servidores</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Editar Servidor</h3>
    </div>
</div>

<?php if (isset($mensagem)) :?>
<div class="row">
    <?php $class = ($mensagem['sucesso'] == true) ? 'success' : 'danger';?>
    <div class="col-md-12 alert alert-<?php echo $class;?>">
        <?php echo $mensagem['mensagem'];?>
    </div>
</div>
<?php endif;?>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}servidor/save" enctype="multipart/form-data" method="post"  autocomplete="off">
            {servidor}
            <div class="formSep">

                <input type="hidden" name="server_id" id="server_id" value="{server_id}"/>
                <div class="col-m-12 col-md-6">
                    <label for="server_type">Tipo de Servidor</label>
                    <select name="server_type" id="server_type" class="form-control chosen-select" data-placeholder="Tipo do servidor" data-callback="ui|server_box" {disabled}>
                        <option></option>
                        <option value="1"{storage_1_selected}>{storage_1}</option>
                        <option value="2"{storage_2_selected}>{storage_2}</option>
                        <option value="3"{storage_3_selected}>{storage_3}</option>
                    </select>
                </div>
                <div class="col-m-12 col-md-6">
                    <div id="storage1" class="{storage_1_hidden} sbox">
                        <fieldset>
                            <legend>Conexão Local</legend>
                            <div class="col-sm-12 col-md-12">
                                <label for="server_path">Caminho do diretório no servidor</label>
                                <input type="text" class="form-control" id="server_path" name="server_path" value="{server_path}" placeholder="Exemplo: /home/usuario/public_html"/>
                            </div>
                        </fieldset>

                    </div>
                    <div id="storage2" class="{storage_2_hidden} sbox">
                        <fieldset>
                            <legend>Amazom S3</legend>
                        <div class="col-sm-12 col-md-12">
                            <label for="server_key">AMAZOM KEY</label>
                            <input type="text" class="form-control" id="server_key" name="server_key" value="{server_key}"placeholder="Tokem de acesso ao servidor S3"/>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <label for="server_secret">AMAZOM SECRET</label>
                            <input type="text" class="form-control" id="server_secret" name="server_secret" value="{server_secret}" placeholder="Chave de segurança"/>
                        </div>
                        </fieldset>
                    </div>
                    <div id="storage3" class="{storage_3_hidden} sbox">
                        <fieldset>
                            <legend>FTP</legend>

                            <div class="col-sm-8 col-md-8">
                                <label for="description">Descrição do servidor:</label>
                                <input type="text" class="form-control" id="description" name="description" value="{description}" placeholder="Exemplo: FTP GED"/>
                            </div>
                            <div class="col-sm-8 col-md-8">
                                <label for="server_address">Endereço do servidor:</label>
                                <input type="text" class="form-control" id="server_address" name="server_address" value="{server_address}" placeholder="Exemplo: ftp.ged.com.br"/>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <label for="server_port">Porta:</label>
                                <input type="text" class="form-control" id="server_port" name="server_port" value="{server_port}" placeholder="Exemplo: 21"/>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label for="server_username">Usuário</label>
                                <input type="text" class="form-control" id="server_username" name="server_username" value="{server_username}" autocomplete="off" placeholder="Exemplo: usuarioftp"/>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label for="server_password">Senha</label>
                                <input type="password" class="form-control" id="server_password" name="server_password" value="{server_password}" autocomplete="off" placeholder="Senha"/>
                            </div>

                            <div class="col-sm-8 col-md-8">
                                <label for="directory_monitor">Diretório a monitorar:</label>
                                <input type="text" class="form-control" id="directory_monitor" name="directory_monitor" value="{directory_monitor}" placeholder="Exemplo: Nome do diretório"/>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

            <div class="col-sm-12 col-md-12">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">salvar</button>
                    <a href="{url}servidor" class="btn btn-danger">cancelar</a>
                </div>
            </div>
            {/servidor}

        </form>
    </div>
</div>
