<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}faturamento" class="ext_disabled"> <i class="fa fa-usd"></i> Faturamento</a>
        </li>
        <li>
            <a href="{url}faturamento/cadastro" class="ext_disabled"> <i class="fa fa-plus"></i> Cadastro</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Novo Usuário</h3>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <form action="{url}faturamento/save" method="POST" enctype="application/x-www-form-urlencoded">
            {faturamento}
            <div class="formSep">

                <div class="col-m-12 col-md-6">
                    <input type="hidden" name="fatura_id" id="fatura_id" value="{fatura_id}"/>
                    <input type="hidden" name="fatura_emissao" id="fatura_emissao" value="{fatura_emissao}"/>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label for="client_id">Cliente</label>
                            <select name="client_id" id="client_id" class="form-control chosen-select" data-placeholder="Escolha um Cliente" data-callback="gedInvoices|loadClient" data-href="{url}faturamento/cliente">
                                <option value=""></option>
                                {clientes}
                                <option value="{cli_id}" data-value="{data_vencimento}" data-target="fatura_vencimento">{empresa_fantasia}</option>
                                {/clientes}
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="user_fullname">Data do Vencimento</label>
                            <div class="input-group">
                                <input name="fatura_vencimento"  id="fatura_vencimento" value="{fatura_vencimento}" type="text" class="form-control datepicker" readonly><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="user_type_id">Observações</label>
                            <textarea name="fatura_obs" id="fatura_obs" rows="5" class="form-control">{fatura_obs}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="row">
                        <legend>Serviços Cobrados</legend>
                        <div class="col-xs-12 col-sm-12">
                            <ul class="list-group">
                                {servicos}
                                <li class="list-group-item">
                                    <input type="checkbox" value="{service_id}" id="service_id_{service_id}" name="service_id[]"/>
                                    <label for="service_id_{service_id}" class="checkbox"> {service_name} - {service_price}</label>
                                </li>
                                {/servicos}
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a href="{url}faturamento" class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
            {/faturamento}
        </form>
    </div>
</div>
