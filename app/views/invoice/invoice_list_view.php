<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}/faturamento"> <i class="fa fa-usd"></i> Faturamento</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class="heading">Faturamento</h3>
    </div>
    <div class="col-sm-12 col-md-12">
        <a href="{url}faturamento/cadastro" class="btn btn-primary pull-right{hidden}" ><i class="fa fa-plus"></i> Nova Fatura</a>
    </div>
</div>

<div class="row table-content">
    <div class="col-sm-12 col-md-12">
        <table class="table table-condensed datatable">
            <thead>
            <th>#</th>
            <th>Cliente</th>
            <th>Emissão</th>
            <th>Vencimento</th>
            <th>Valor</th>
            <th>Status</th>
            </thead>

            <tbody>
            {faturas}
            <tr>
                <td><a href="{url}faturamento/detalhes/{fatura_id}">{fatura_id}</a></td>
                <td><a href="{url}faturamento/detalhes/{fatura_id}">{client_name}</a></td>
                <td><a href="{url}faturamento/detalhes/{fatura_id}">{fatura_emissao}</a></td>
                <td><a href="{url}faturamento/detalhes/{fatura_id}">{fatura_vencimento}</a></td>
                <td><a href="{url}faturamento/detalhes/{fatura_id}">R$ {fatura_valor}</a></td>
                <td><a href="{url}faturamento/detalhes/{fatura_id}">{status_name}</a></td>
            </tr>
            {/faturas}
            </tbody>
        </table>
    </div>
</div>
