<address>
    <strong>{user_fullname}</strong><br>
    {endereco_logradouro} {endereco_num}, {endereco_complemento} {endereco_bairro}<br>
    {endereco_cidade}, {endereco_estado} {endereco_cep}<br>
    Telefone: (555) 539-1037<br/>
    Email: john.doe@example.com
</address>