{fatura}
<div id="jCrumbs" class="breadCrumb module">
    <ul>
        <li>
            <a href="{url}" class="ext_disabled"><i class="glyphicon glyphicon-home"></i></a>
        </li>
        <li>
            <a href="{url}faturamento" class="ext_disabled"> <i class="fa fa-barcode"></i> Faturas</a>
        </li>
        <li>
            <a href="{url}faturamento/detalhes/{fatura_id}" class="ext_disabled"> <i class="fa fa-th-list"></i> Detalhes</a>
        </li>
    </ul>
</div>

<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Central de Notas LTDA
                <small class="pull-right">{fatura_emissao}</small>
            </h2>
        </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-5 invoice-col">
            De
            {company}
            <address>
                <strong>{company_razao}</strong><br>
                {company_endereco}, {company_complemento}<br>
                {company_cidade}, {company_estado} {company_cep}<br>
                Telefone: {company_telefone}<br/>
                Email: {company_email}
            </address>
            {/company}
        </div><!-- /.col -->
        <div class="col-sm-4 invoice-col">
            Para
            {client_view}
        </div><!-- /.col -->
        <div class="col-sm-3 invoice-col">
            <b>Fatura #{fatura_id}</b><br/>
            <b>Vencimento:</b> {fatura_vencimento}<br/>
        </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Produto</th>
                    <th>Descrição</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>
                {fatura_itens}
                <tr>
                    <td>{service_name}</td>
                    <td>{service_desc}</td>
                    <td>R${service_price}</td>
                </tr>
                {/fatura_itens}

                {fatura_itens_adicional}
                <tr>
                    <td>{quant_desc}</td>
                    <td>Espaço adicional</td>
                    <td>R${price}</td>
                </tr>
                {/fatura_itens_adicional}
                </tbody>
            </table>
        </div><!-- /.col -->
    </div><!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
            <p class="lead">Métodos de Pagamento:</p>
            <img src="{img}/payment/barcode.png" alt="Boleto" title="Boleto"/>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                {fatura_obs}
            </p>
        </div><!-- /.col -->
        <div class="col-xs-6">
            <p class="lead">Valor devido em {fatura_emissao}</p>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>R$ {fatura_valor}</td>
                    </tr>
                    <tr>
                        <th>Impostos</th>
                        <td>R$ 0.00</td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td>R$ {fatura_valor}</td>
                    </tr>
                </table>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <?php if($vencimento < $now): ?>
            <a href="{url}faturamento/boleto/{fatura_id}/via" target="_blank" class="btn btn-warning pull-right" style="margin-right: 5px;"><i class="fa fa-barcode"></i> 2&ordf; via Boleto</a>
            <?php else: ?>
            <a href="{url}faturamento/boleto/{fatura_id}" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-barcode"></i> Visualizar Boleto</a>
            <?php endif; ?>
            <?php if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') : ?>
            <a href="{url}faturamento/enviar/{fatura_id}" class="btn btn-warning pull-right" style="margin-right: 5px;"><i class="fa fa-envelope"></i> Enviar Fatura</a>
            <?php endif; ?>
        </div>
    </div>
</section><!-- /.content -->
<div class="clearfix"></div>
{/fatura}