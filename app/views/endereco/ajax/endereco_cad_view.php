{endereco}
<div class="modal-content-form">
    <div class="col-sm-12 col-md-12">
        <form method="POST" id="modal-form" enctype="application/x-www-form-urlencoded">
            <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>
            <input type="hidden" name="user_id" id="user_id" value="{user_id}"/>
            <input type="hidden" name="endereco_id" id="endereco_id" value="{endereco_id}"/>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <label for="endereco_alias">Apelido </label>
                    <input type="text" name="endereco_alias" id="endereco_alias" value="{endereco_alias}" class="form-control" maxlength="45" placeholder="Ex: casa, escritório" autofocus/>
                    <?=form_error('endereco_alias');?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <label for="endereco_cep">CEP</label>
                    <input type="text" name="endereco_cep" id="endereco_cep" value="{endereco_cep}" class="form-control cep" maxlength="9" placeholder="CEP: 00000-000" onblur="gedcep.search(this)"/>
                    <?=form_error('endereco_cep');?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <label for="endereco_logradouro">Logradouro </label>
                    <input type="text" name="endereco_logradouro" id="endereco_logradouro" value="{endereco_logradouro}" maxlength="155" class="form-control" placeholder="Ex: Av, Rua, Alameda"/>
                    <?=form_error('endereco_logradouro');?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <label for="endereco_num">Número</label>
                    <input type="text" name="endereco_num" id="endereco_num" value="{endereco_num}" maxlength="10" class="form-control" placeholder="Número"/>
                    <?=form_error('endereco_num');?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <label for="endereco_bairro">Bairro </label>
                    <input type="text" name="endereco_bairro" id="endereco_bairro" value="{endereco_bairro}" maxlength="155" class="form-control" placeholder="Bairro"/>
                    <?=form_error('endereco_bairro');?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <label for="endereco_cidade">Cidade</label>
                    <input type="text" name="endereco_cidade" id="endereco_cidade" value="{endereco_cidade}" maxlength="155" class="form-control" placeholder="Cidade"/>
                    <?=form_error('endereco_cidade');?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div>
                    <label for="endereco_estado">Estado </label>
                    <input type="text" name="endereco_estado" id="endereco_estado" value="{endereco_estado}" maxlength="2" class="form-control" placeholder="Estado"/>
                    <?=form_error('endereco_estado');?>
                    </div>
                    <div>
                        <input type="checkbox" name="endereco_principal" id="endereco_principal" value="1" />
                        <label for="endereco_principal" class="checkbox"> Endereço principal </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <label for="endereco_complemento">Complemento</label>
                    <textarea type="text" name="endereco_complemento" id="endereco_complemento" maxlength="155" class="form-control" placeholder="Ex: próximo ao supermercado.">{endereco_complemento}</textarea>
                    <?=form_error('endereco_complemento');?>
                </div>
            </div>
        </form>
    </div>
</div>
{/endereco}