<div class="col-m-12 col-md-6">
    <form action="{url}usuario/save" method="POST" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="empresa_id" id="empresa_id" value="{empresa_id}"/>
        <input type="hidden" name="id_endereco" id="id_endereco" value="{id_endereco}"/>

        <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-6">
            <label for="cod_ibge_endereco"><?=$this->lang->line('desc_col_cad_cep_endereco')?></label>
            <input type="text" name="cod_ibge_endereco" id="cod_ibge_endereco" value="{cod_ibge_endereco}" maxlength="8" onkeypress="return soNumero(event)" class="form-control" />
        </div>
        <div class="col-sm-12 col-xs-12 col-md-6">
            <label for="bairro_endereco"><?=$this->lang->line('desc_col_cad_bairro_endereco')?></label>
            <input type="text" name="bairro_endereco" id="bairro_endereco" value="{bairro_endereco}" maxlength="64" onkeypress="" class="form-control" />
        </div>
        </div>
        <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-6">
            <label for="cidade_endereco"><?=$this->lang->line('desc_col_cad_cidade_endereco')?></label>
            <input type="text" name="cidade_endereco" id="cidade_endereco" value="{cidade_endereco}" maxlength="64" onkeypress="" class="form-control" />
        </div>
        <div class="col-sm-12 col-xs-12 col-md-6">
            <label for="complemento_endereco"><?=$this->lang->line('desc_col_cad_complemento_endereco')?></label>
            <input type="text" name="complemento_endereco" id="complemento_endereco" value="{complemento_endereco}" maxlength="32" onkeypress="" class="form-control" />
        </div>
        </div>
        <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-12">
            <label for="logradouro_endereco"><?=$this->lang->line('desc_col_cad_logradouro_endereco')?></label>
            <input type="text" name="logradouro_endereco" id="logradouro_endereco" value="{logradouro_endereco}" maxlength="64" onkeypress="" class="form-control" />
        </div>
        </div>
        <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-6">
            <label for="numero_endereco"><?=$this->lang->line('desc_col_cad_numero_endereco')?></label>
            <input type="text" name="numero_endereco" id="numero_endereco" value="{numero_endereco}" maxlength="8" onkeypress="return soNumero(event)" class="form-control" />
        </div>
        <div class="col-sm-12 col-xs-12 col-md-6">
            <label for="uf_endereco"><?=$this->lang->line('desc_col_cad_uf_endereco')?></label>
            <input type="text" name="uf_endereco" id="uf_endereco" value="{uf_endereco}" maxlength="2" onkeypress="" class="form-control" />
        </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <a href="{url}usuario" class="btn btn-danger">Cancelar</a>
                <button class="btn btn-success">Salvar</button>
            </div>
        </div>
    </form>

</div>
<div class="col-md-12 col-md-6">
    <table class="table table-condensed ">
        <thead>
        <th>#</th>
        <th>Endereco</th>
        <th>Cidade</th>
        <th> </th>
        </thead>

        <tbody>
        {endereco}
        <tr>
            <td>{endereco_id}</td>
            <td>{endereco_rua}</td>
            <td>{endereco_cidade}</td>
            <td class="text-right">
                <button class="btn btn-xs btn-success" href="{url}plano/editar/{plan_id}"><i class="fa fa-edit"></i></button>
                <button class="btn btn-xs btn-danger" href="{url}plano/excluir/{plan_id}"><i class="fa fa-remove"></i></button>
            </td>
        </tr>
        {/endereco}
        </tbody>
    </table>
</div>