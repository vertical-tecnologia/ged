<?php

$lang['ftp_no_connection']       = "Não foi possível localizar um ID de conexão válido. Favor confirmar se está conectado antes de executar qualquer rotina de arquivos.";
$lang['ftp_unable_to_connect']   = "Não foi possível conectar ao servidor de FTP utilizando o servidor especificado.";
$lang['ftp_unable_to_login']     = "Não foi possível fazer o login no servidor de FTP. Favor verificar nome de usuário e senha.";
$lang['ftp_unable_to_makdir']    = "Não foi possível criar o diretório especificado.";
$lang['ftp_unable_to_changedir'] = "Não foi possível mudar de diretório.";
$lang['ftp_unable_to_chmod']     = "Não foi possível configurar as permissões do arquivo. Favor verificar o caminho. Obs.: Esta característica está disponível somente para PHP versão 5 ou superior.";
$lang['ftp_unable_to_upload']    = "Não foi possível carregar o arquivo especificado. Favor verificar o caminho.";
$lang['ftp_no_source_file']      = "Não foi possível encontrar o arquivo de origem. Favor verificar o caminho.";
$lang['ftp_unable_to_rename']    = "Não foi possível renomear o arquivo.";
$lang['ftp_unable_to_delete']    = "Não foi possível remover o arquivo.";
$lang['ftp_unable_to_move']      = "Não foi possível mover o arquivo. Favor confirmar diretório de destino.";




// $lang['ftp_no_connection']			= "Unable to locate a valid connection ID. Please make sure you are connected before peforming any file routines.";
// $lang['ftp_unable_to_connect']		= "Unable to connect to your FTP server using the supplied hostname.";
// $lang['ftp_unable_to_login']		= "Unable to login to your FTP server. Please check your username and password.";
// $lang['ftp_unable_to_makdir']		= "Unable to create the directory you have specified.";
// $lang['ftp_unable_to_changedir']	= "Unable to change directories.";
// $lang['ftp_unable_to_chmod']		= "Unable to set file permissions. Please check your path. Note: This feature is only available in PHP 5 or higher.";
// $lang['ftp_unable_to_upload']		= "Unable to upload the specified file. Please check your path.";
// $lang['ftp_unable_to_download']		= "Unable to download the specified file. Please check your path.";
// $lang['ftp_no_source_file']			= "Unable to locate the source file. Please check your path.";
// $lang['ftp_unable_to_rename']		= "Unable to rename the file.";
// $lang['ftp_unable_to_delete']		= "Unable to delete the file.";
// $lang['ftp_unable_to_move']			= "Unable to move the file. Please make sure the destination directory exists.";


/* End of file ftp_lang.php */
/* Location: ./system/language/pt-br/ftp_lang.php */