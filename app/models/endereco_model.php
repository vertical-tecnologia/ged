<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 12/06/15
 * Time: 11:49
 */

class endereco_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function fetchAll(EnderecoEntity $entity)
    {
        $this->db->from('endereco');

        if($entity->empresa_id != null){
            $this->db->join('empresa_endereco', 'empresa_endereco.endereco_id = endereco.endereco_id');
            $this->db->where('empresa_endereco.empresa_id', $entity->empresa_id);
        }

        if($entity->user_id != null){
            $this->db->join('usuario_endereco', 'usuario_endereco.endereco_id = endereco.endereco_id');
            $this->db->where('usuario_endereco.user_id', $entity->user_id);
        }

        return $this->db->get()->result();
    }

    public function fetch(EnderecoEntity $entity)
    {
        $this->db->from('endereco');
        $this->db->join('empresa_endereco', 'empresa_endereco.endereco_id = endereco.endereco_id', 'LEFT');
        $this->db->join('usuario_endereco', 'usuario_endereco.endereco_id = endereco.endereco_id', 'LEFT');

        if($entity->endereco_id != '')
        $this->db->where('endereco.endereco_id', $entity->endereco_id);

        return $this->db->get()->row();

    }

    public function fetchMainUserAddress(UsuarioEnderecoEntity $entity)
    {
        $this->db->from('endereco');
        $this->db->join('usuario_endereco', 'usuario_endereco.endereco_id = endereco.endereco_id');

        if($entity->endereco_id != '')
            $this->db->where('endereco_id', $entity->endereco_id);

        if($entity->user_id != '')
            $this->db->where('user_id', $entity->user_id);

        $this->db->where('endereco.endereco_principal', 1);

        return $this->db->get()->row();
    }


    public function save(EnderecoEntity $entity)
    {
        $this->db->trans_begin();

        $id = (int) $entity->endereco_id;

        if($id == 0){

            $this->setFields($entity);

            $this->db->insert('endereco');
            $entity->endereco_id = $this->db->insert_id();

            if($entity->endereco_id != 0){
                syslog::generate_log('NEW_ADDRESS_SUCCESS');
                return $entity;
            } else {
                $this->db->trans_rollback();
                syslog::generate_log('NEW_ADDRESS_ERROR');
                return false;
            }

        } else {

            $this->setFields($entity);
            $this->db->update('endereco');

            if($this->db->where('endereco_id', $entity->endereco_id)){
                syslog::generate_log('UPDATE_ADDRESS_ERROR');
                return $entity;
            } else {
                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_ADDRESS_ERROR');
                return false;
            }
        }

    }

    public function save_empresa(EnderecoEmpresaEntity $entity)
    {
        $this->db->set('endereco_id', $entity->endereco_id);
        $this->db->set('empresa_id', $entity->empresa_id);
        $this->db->set('principal', $entity->principal);

        $this->db->insert('empresa_endereco');
        $entity->empresa_endereco_id = $this->db->insert_id();
        if($entity->empresa_endereco_id != 0){

            $this->db->trans_commit();
            return $entity;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function save_user(UsuarioEnderecoEntity $entity)
    {
        $this->db->set('endereco_id', $entity->endereco_id);
        $this->db->set('user_id', $entity->user_id);

        $this->db->insert('usuario_endereco');
        $entity->user_endereco_id = $this->db->insert_id();
        if($entity->user_id != 0){

            if($this->input->post('endereco_principal') == 1){
                $this->get_user_address($entity->user_id);
                $this->reset_main($entity->user_id);
            }

            $this->db->trans_commit();
            return $entity;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function excluir(EnderecoEntity $entity)
    {
        $id = (int) $entity->endereco_id;
        if($id > 0){
            $this->db->where('endereco_id', $id);
            if($this->db->delete('endereco')){
                syslog::generate_log('DELETE_ADDRESS_SUCCESS');
                return true;
            } else {
                syslog::generate_log('DELETE_ADDRESS_ERROR');
                return false;
            };
        } else
            return false;
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'endereco_id' || $value == null || $value == '' || $key == 'empresa_endereco_id' || $key == 'user_endereco_id') continue;
            $this->db->set($key, $value);
        }
    }

    private function reset_main($enderecos)
    {
        $this->db->set('endereco.endereco_principal', 'NULL');
        $this->db->where_in('endereco_id' . $enderecos);
        $this->db->update('endereco');

    }

    public function get_user_address($user_id)
    {
        $this->db->select('endereco_id');
        $this->db->from('usuario_endereco');
        $this->db->where('user_id', $user_id);

        $this->db->get()->result_array();
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('endereco');
    }
}