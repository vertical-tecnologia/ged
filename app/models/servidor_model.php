<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 12/07/15
 * Time: 18:52
 */

class Servidor_model extends CI_Model{

	public $vet_dados;

	public function __construct()
	{
		parent::__construct();
		$this->vet_dados = base_dir($this->session->userdata('skin'));
	}

	public function index()
	{
		$server = $this->servidor_model->fetchAll(new ServerEntity(), '', array('enable' => SERVER_ATIVO));
		$this->vet_dados['button_add'] = anchor('servidor/cadastro', '<i class="fa fa-server"></i> Novo Servidor', array('title'=>'Cadastrar Servidor', 'class'=>"btn btn-primary pull-right"));

		for($i = 0;$i < count($server); $i++){
			$server[$i]->server_type_desc = constant('STORAGE_'.$server[$i]->server_type);
			$server[$i]->link_del = $server[$i]->server_is_default == 1 ? '' : anchor('servidor/excluir/' . $server[$i]->server_id, 'Excluir', array('title' => 'Excluir Servidor'));
			$server[$i]->link_upd = anchor('servidor/editar/' . $server[$i]->server_id, 'Editar', array('title' => 'Editar Servidor'));
		}

		$this->vet_dados['servers'] = $server;
		return $this->parser->parse('server/server_list_view', $this->vet_dados, TRUE);
	}

	private function verifyServerHost($entity)
	{
		$where = array('server_address' => $entity->server_address, 'enable' => SERVER_ATIVO);

		if ($entity->server_id)
			$where['server_id !='] = $entity->server_id;

		$result = $this->fetch(new ServerEntity(), $where);

		if (!$result)
			return true;

		$method = empty($entity->server_id) ? 'cadastro' : 'editar';
		$id = isset($entity->id) ? $entity->server_id : '';
		$this->session->set_flashdata('verify', array('sucesso' => false, 'mensagem' => 'O host informado já se encontra em uso.'));
		$this->session->set_flashdata('post_server', $entity);
		redirect("servidor/{$method}/{$id}");
	}

	public function save(ServerEntity $entity)
	{
		if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO')
			$entity->is_admin = true;

		if ($this->session->userdata('USER_ROLE') == 'CLIADMIN' && $this->session->userdata('IS_OWNER') == true) {
			$result = $this->findCompany(array('usuario.user_id' => $this->session->userdata('USER_ID')));
			$entity->empresa_id = $result['empresa_id'];
		}

		if ($entity->server_type = SERVER_FTP)
			$this->verifyServerHost($entity);

		if (!$this->verifyServer($entity)) {
			$method = empty($entity->server_id) ? 'cadastro' : 'editar';
			$mensagem = array('sucesso' => false, 'mensagem' => 'Não foi possível fazer o login no servidor. Favor verifique os dados informados.');
			$id = isset($entity->id) ? $entity->server_id : '';
			$this->session->set_flashdata('verify', $mensagem);
			$this->session->set_flashdata('post_server', $entity);
			redirect("servidor/{$method}/{$id}");
		}

		$arrProperty = get_object_vars($entity);
		foreach ($arrProperty as $property => $value) {
			if (is_null($value))
				continue;

			if (in_array($property, array('server_key' , 'server_secret', 'server_password')))
				$value = base64_encode($value);

			$this->db->set($property, $value);
		}

		$result = (!$entity->server_id) ? $this->insert($entity) : $this->update($entity);

		if ($entity->server_type == 3 && $result)
			syslog::generate_log("NEW_FTP_SERVER_SUCCESS");

		if ($entity->server_type == 3 && !$result)
			syslog::generate_log("NEW_FTP_SERVER_ERROR");

		if ($entity->server_type == 2 && $result)
			syslog::generate_log("NEW_S3_SERVER_SUCCESS");

		if ($entity->server_type == 2 && !$result)
			syslog::generate_log("NEW_S3_SERVER_ERROR");

		if ($entity->server_type == 1 && $result)
			syslog::generate_log("NEW_LOCAL_SERVER_SUCCESS");

		if ($entity->server_type == 1 && !$result)
			syslog::generate_log("NEW_LOCAL_SERVER_ERROR");

		return $result;
	}

	public function cadastro()
	{
		$verify = $this->verifyTotalServer();
		if ($verify['sucesso'] == false) {
			$this->session->set_flashdata('retorno', $verify);
			redirect('servidor');
		}

		if ($verify = $this->session->flashdata('verify'))
			$this->load->vars(array('mensagem' => $verify));

		$vet = array();
		$vet['server_id']         = set_value('');
		$vet['server_path']       = set_value('server_path');
		$vet['server_key']        = set_value('server_key');
		$vet['server_secret']     = set_value('server_secret');
		$vet['server_address']    = set_value('server_address');
		$vet['server_port']       = set_value('server_port');
		$vet['server_username']   = set_value('server_username');
		$vet['server_password']   = set_value('server_password');
		$vet['description']       = set_value('description');
		$vet['directory_monitor'] = set_value('directory_monitor');

		$arrStorage = array();

		if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO')
			$this->vet_dados['storage_1'] = STORAGE_1;
		else
			$this->vet_dados['storage_1'] = '';

		$this->vet_dados['storage_2'] = STORAGE_2;
		$this->vet_dados['storage_3'] = STORAGE_3;

		$this->vet_dados['storage_1_hidden'] = ' hide ';
		$this->vet_dados['storage_2_hidden'] = ' hide ' ;
		$this->vet_dados['storage_3_hidden'] = ' hide ' ;

		if ($this->session->flashdata('post_server')) {
			$vet = $this->session->flashdata('post_server');
			$this->vet_dados["storage_{$vet->server_type}_selected"] = 'selected';
			$this->vet_dados["storage_{$vet->server_type}_hidden"] = '' ;
		}

		$this->vet_dados['servidor'] = array($vet);

		return $this->parser->parse('server/server_cad_view', $this->vet_dados, TRUE);
	}

	public function editar(ServerEntity $entity)
	{
		if ($verify = $this->session->flashdata('verify'))
			$this->load->vars(array('mensagem' => $verify));

		$vet = $this->servidor_model->fetch($entity);
		$vet->server_password = base64_decode($vet->server_password);

		if ($this->session->flashdata('post_server'))
			$vet = $this->session->flashdata('post_server');

		$this->vet_dados['servidor'] = array($vet);

		$this->vet_dados['storage_1'] = STORAGE_1;
		$this->vet_dados['storage_2'] = STORAGE_2;
		$this->vet_dados['storage_3'] = STORAGE_3;

		$this->vet_dados['disabled'] = " readonly ";

		$this->vet_dados['storage_1_selected'] = $vet->server_type == 1  ? ' selected ' : '';
		$this->vet_dados['storage_2_selected'] = $vet->server_type == 2  ? ' selected ' : '';
		$this->vet_dados['storage_3_selected'] = $vet->server_type == 3  ? ' selected ' : '';

		$this->vet_dados['storage_1_hidden'] = $vet->server_type == 1  ? '' : ' hide ' ;
		$this->vet_dados['storage_2_hidden'] = $vet->server_type == 2  ? '' : ' hide ' ;
		$this->vet_dados['storage_3_hidden'] = $vet->server_type == 3  ? '' : ' hide ' ;

		return $this->parser->parse('server/server_cad_view', $this->vet_dados, TRUE);
	}

	public function excluir(ServerEntity $entity)
	{
		$vet = $this->servidor_model->fetch($entity);;

		$this->vet_dados['storage_1_hidden'] = $vet->server_type == 1  ? '' : ' hide ' ;
		$this->vet_dados['storage_2_hidden'] = $vet->server_type == 2  ? '' : ' hide ' ;
		$this->vet_dados['storage_3_hidden'] = $vet->server_type == 3  ? '' : ' hide ' ;

		$vet->server_type_desc = constant('STORAGE_'.$vet->server_type);

		$this->vet_dados['servidor'] = array($vet);

		return $this->parser->parse('server/server_del_view', $this->vet_dados, TRUE);
	}

	public function confirma_exclusao(ServerEntity $entity)
	{
		$vet = $this->servidor_model->fetch($entity);

		if(count($vet) == 1){
			$this->db->where('server_id', $vet->server_id);
			// if($this->db->delete('servidor')){
			 if($this->db->update('servidor', array('enable' => SERVER_INATIVO))){
				$this->session->set_flashdata('success', 'Servidor excluído com sucesso.');
			} else {
				$this->session->set_flashdata('error', 'Erro ao exclur servidor.');
				redirect('servidor/excluir/'.$vet->Server_id);
			}
		} else {
			$this->session->set_flashdata('error', 'Servidor não encontrado.');
		}

		redirect('servidor');
	}

	public function fetch(ServerEntity $entity, $where = array())
	{
		if ($this->session->userdata('USER_ROLE') == 'CLIADMIN' && $this->session->userdata('IS_OWNER') == true
			&& $entity->server_id != SERVER_LOCAL
			) {
			$result = $this->findCompany(array('usuario.user_id' => $this->session->userdata('USER_ID')));
			$where['empresa_id'] = $result['empresa_id'];
		}

		if ($entity->server_id != '')
			$where['servidor.server_id'] = $entity->server_id;


		$this->db->from('servidor');
		$this->db->where($where);
		return $this->db->get()->row();
	}

	public function fetchAll(ServerEntity $entity, $fields = '', $where = array(), $whereIn = array())
	{

		if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO'){
			$where['is_admin'] = 1;
			$where['client_id'] = null;
			// $this->db->where('is_admin', 1);
			// $this->db->where('client_id IS NULL');
		}

		if (in_array($this->session->userdata('USER_ROLE'), ['CLIADMIN', 'USUARIO']))
			$where['empresa_id'] = $this->session->userdata('EID');
			// $this->db->where('empresa_id',  $this->session->userdata('EID'));

		$this->db->from('servidor');
		if (!empty($fields))
			$this->db->select($fields);

		if ($whereIn)
			$this->db->where_in($whereIn['column'], $whereIn['values']);

		$this->db->where($where);
		return $this->db->get()->result();
	}

	public function insert()
	{
		$this->db->insert('servidor');
		$entity->server_id = (int) $this->db->insert_id();

		if ($entity->server_id > 0) {
			$this->session->set_flashdata('success', 'Servidor cadastrado com sucesso');
			$this->db->trans_commit();
			return true;
		}

		$this->session->set_flashdata('error', 'Erro ao cadastrar servidor');
		$this->db->trans_rollback();
		return false;
	}

	public function update(ServerEntity $entity)
	{
		$this->db->where('server_id', $entity->server_id);
		if ($this->db->update('servidor')) {
			$this->session->set_flashdata('success', 'Servidor alterado com sucesso');
			$this->db->trans_commit();
			return true;
		}

		$this->session->set_flashdata('error', 'Erro ao alterado servidor');
		$this->db->trans_rollback();
		return false;
	}

	public function findCompany($arrWhere)
	{
		$this->db->select('empresa.*');
		$this->db->from('empresa');
		$this->db->join('usuario', 'usuario.user_id = empresa.empresa_user_id');
		$this->db->where($arrWhere);
		$result = $this->db->get()->result_array();

		return ($result) ? $result[0] : array();
	}

	private function verifyTotalServer()
	{
		if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO')
			return array('sucesso' => true);

		if ($this->session->userdata('USER_ROLE') != 'CLIADMIN' || $this->session->userdata('IS_OWNER') == false)
			return array('sucesso' => false, 'mensagem' => 'Usuário não é administrador da empresa na qual está vinculado.');

		$company = $this->findCompany(array('usuario.user_id' => $this->session->userdata('USER_ID'), 'owner' => true));

		if (!$company)
			return array('sucesso' => false, 'mensagem' => 'Usuário não é administrador da empresa na qual está vinculado.');

		$servers = $this->fetchAll(new ServerEntity, 'count(servidor.server_id) as total', array('enable' => SERVER_ATIVO));
		$totalServers = ($servers) ? $servers[0]->total : 0;

		if ($totalServers >= $company['total_server'])
			return array('sucesso' => false, 'mensagem' => 'A empresa atingiu o total de servidores permitidos.');

		return array('sucesso' => true);
	}

	public function verifyServer(ServerEntity $entity)
	{
		switch ($entity->server_type) {
			case SERVER_FTP:
				return $this->testFTP($entity);
				break;
		}

		if (!$this->testFTP($entity))
			return false;
	}

	public function testFTP(ServerEntity $entity)
	{
		$this->load->library('ftp');
		$ftp = array(
			'server_address'  => $entity->server_address,
			'server_username' => $entity->server_username,
			'server_password' => $entity->server_password,
			'server_port'     => $entity->server_port);
		return $this->ftp->testConnection($ftp);
	}

	public function getServerInfo($server)
	{
		$this->db->select('servidor.directory_monitor, usuario.user_key, cliente.client_volume');
		$this->db->from('servidor');
		$this->db->join('empresa_usuario', 'empresa_usuario.id_empresa = servidor.empresa_id');
		$this->db->join('cliente', 'cliente.client_empresa_id = empresa_usuario.id_empresa');
		$this->db->join('usuario', 'usuario.user_id = empresa_usuario.id_usuario');
		$this->db->where('servidor.server_id', $server);
		$this->db->where('usuario.owner', true);
		return $this->db->get()->result_array();
	}
}