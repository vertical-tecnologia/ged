<?php

/**
 * Class cadastro_model
 * @package		App\Models
 */

class cadastro_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir('public');
        $this->load->model('plano_model');
        $this->load->model('servico_model');
    }

    public function login()
    {
        return $this->parser->parse('cadastro/login_view', $this->vet_dados, TRUE);
    }

    public function forgot($show_form = false)
    {
        if($show_form == false){
            $entity = new UsuarioEntity();
            $entity->user_email = $this->input->post('email');
            $user = $this->get_usuario_by_mail($entity);

            if(count($user) != 1){
                $this->session->set_flashdata('error', 'Email não cadastrado.');
                redirect('forgot');
                return;
            }

            $entity->redefinition_key = random_string('alnum', 255);
            $entity->user_email = $user->user_email;
            $entity->user_fullname = $user->user_fullname;
            $this->set_redefinition_key($entity);

            $this->enviar_email($entity);
            redirect('login');
        }
        return $this->parser->parse('cadastro/forgot_view', $this->vet_dados, TRUE);
    }


    public function reset($show_form = false)
    {
        $this->vet_dados['key'] = $this->uri->segment(2);
        
        if($show_form == false && $this->input->post('password') != ''){
            $entity = new UsuarioEntity();
            $entity->redefinition_key = $this->uri->segment(2);

            $user = $this->redefinition_key($entity->redefinition_key);

            $entity->user_password = $this->encrypt->encode($this->input->post('password'));

            if(count($user) != 1){
                $this->session->set_flashdata('error', 'Link invalido.');
                redirect('forgot');
                return;
            }
            $this->update_user_password($entity);

            $this->session->set_flashdata('success', 'Senha alterada com sucesso.');
            redirect('login');
            return;
        }
        return $this->parser->parse('cadastro/redefine_view', $this->vet_dados, TRUE);
    }

    public function plans()
    {
        $planos = $this->plano_model->fetchAll(array('plan_status_id' => 1));

        for($i=0;$i<count($planos);$i++){
            $planos[$i]->servicos = $this->servico_model->fetchAllServiceUser(array('plan_id' => $planos[$i]->plan_id));
        }

        $this->vet_dados['planos'] = $planos;

        $home_data = $this->get_home_description();

        $this->vet_dados['home_title'] = $home_data->home_title;
        $this->vet_dados['home_subtitle'] = $home_data->home_subtitle;

        return $this->parser->parse('cadastro/cadastro_plans_view', $this->vet_dados, TRUE);
    }

    public function plano()
    {
        $planos = $this->plano_model->fetchAll(array('plan_id' => $this->uri->segment(3)));

        for($i=0;$i<count($planos);$i++){
            $planos[$i]->servicos = $this->servico_model->fetchAllServiceUser(array('plan_id' => $planos[$i]->plan_id));
        }

        $this->vet_dados['planos'] = $planos;
        $this->vet_dados['plan_id'] = $this->uri->segment(3);

        return $this->parser->parse('cadastro/cadastro_plan_view', $this->vet_dados, TRUE);
    }

    public function confirm(UsuarioEntity $entity){
        $usuario = $this->get_usuario_by_hash($entity);

        if(isset($usuario->user_hash) && $usuario->user_hash != ''){

            $this->db->set('user_trusted_mail', true);
            $this->db->set('user_hash', null);
            $this->db->set('user_status_id', 7);
            $this->db->where('user_id', $usuario->user_id);
            $this->db->update('usuario');

            if($this->db->affected_rows() > 0){
                $this->session->set_flashdata('success', 'Email confirmado com sucesso.');
            } else {
                $this->session->set_flashdata('error', 'Erro ao tentar confirmar email  .');
            }
        } else {
            $this->session->set_flashdata('error', 'Link de confirmação inválido.');
        }

        redirect('cadastro');
    }

    public function save(UsuarioEntity $usuario)
    {

        $this->db->trans_begin();

        $this->setFields($usuario);

        $this->db->insert('usuario');

        $id = $this->db->insert_id();


        if ($this->db->trans_status() && $id > 0) {
            $usuario->user_id = $id;
            $this->db->trans_commit();
            return $usuario;

        } else {

            $this->db->trans_rollback();
            throw new Exception($this->lang->line('zorbit_erro_inserir'));

        }

    }

    public function get_usuario_by_hash(UsuarioEntity $entity)
    {
        $this->db->where('user_hash', $entity->user_hash);
        $this->db->from('usuario');
        return $this->db->get()->row();
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'user_id' || $key == 'user_group_id') continue;
            $this->db->set($key, $value);
        }
    }

    public function get_usuario_by_mail(UsuarioEntity $entity)
    {
        $this->db->select('user_id,user_fullname,user_login,user_email,user_trusted_mail,user_register_date,user_hash,user_status_id,user_plan_id,user_key,owner');
        $this->db->where('user_email', $entity->user_email);
        $this->db->from('usuario');
        return $this->db->get()->row();
    }

    public function set_redefinition_key(UsuarioEntity $entity)
    {
        $this->db->set('redefinition_key' , $entity->redefinition_key);

        if($entity->user_id != '')
        $this->db->where('user_id', $entity->user_id);

        if($entity->user_hash != '')
            $this->db->where('user_hash', $entity->user_hash);

        if($entity->user_key != '')
            $this->db->where('user_key', $entity->user_key);

        if($entity->user_email != '')
            $this->db->where('user_email', $entity->user_email);

        return $this->db->update('usuario');
    }


    protected function enviar_email($usuario){
        $this->load->library('Mailer');

        $this->vet_dados['usuario'] = array($usuario);

        $usuario->redefinition_key = urlencode(urlencode($usuario->redefinition_key));

        Mailer::$message = $this->parser->parse('template/public/reset_password_view', $this->vet_dados, TRUE);
        Mailer::$subject = "[GED] - Redefinição de Senha!";
        Mailer::$to = $usuario->user_email;
        Mailer::$toName = $usuario->user_fullname;
        $m = Mailer::send_mail();

        if($m == true){
            syslog::generate_log("NEW_USER_EMAIL_SUCCESS");
            return true;
        }
        else
        {
            syslog::generate_log("NEW_USER_EMAIL_ERROR");
            return false;
        }

    }

    public function redefinition_key($key)
    {
        $this->db->select('user_id, redefinition_key');
        $this->db->where('redefinition_key', $key);
        $this->db->from('usuario');
        return $this->db->get()->row();
    }

    public function update_user_password(UsuarioEntity $entity)
    {
        $this->db->set('user_password', $entity->user_password);
        $this->db->set('redefinition_key', null);

        $this->db->where('redefinition_key', $entity->redefinition_key);

        return $this->db->update('usuario');

    }

    public function email_exists(UsuarioEntity $entity)
    {
        $this->db->from('usuario');
        $this->db->where('user_email', $entity->user_email);

        $row = $this->db->get()->row();

        if(count($row) > 0)
            return true;
        else
            return false;
    }

    public function login_exists(UsuarioEntity $entity)
    {
        $this->db->from('usuario');
        $this->db->where('user_login', $entity->user_login);

        $row = $this->db->get()->row();

        if(count($row) > 0)
            return true;
        else
            return false;
    }


    public function get_home_description()
    {
        $this->db->select('home_title, home_subtitle');
        $this->db->from('compania');
        return $this->db->get()->row();
    }
}

