<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 02:37
 */

class usuario_status_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));

    }


    public function index()
    {

        $status = $this->fetchAll();

        for($i = 0;$i < count($status);$i++){
            $status[$i]->link_del = $status[$i]->status_editable == 0 ? '' : anchor('usuario_status/excluir/' . $status[$i]->status_id, 'Excluir', array('title' => 'Excluir Status'));
            $status[$i]->link_upd = $status[$i]->status_editable == 0 ? '' : anchor('usuario_status/editar/' . $status[$i]->status_id, 'Editar', array('title' => 'Editar Status'));
        }

        $this->vet_dados['usuario_status'] = $status;



        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Usuários';
        $this->vet_dados['tela']      = 'usuario_status';

        return $this->parser->parse('usuario_status/usuario_status_list_view', $this->vet_dados, TRUE);

    }

    public function cadastro()
    {

        $vet['status_id']        = set_value('status_id');
        $vet['status_name']        = set_value('status_name');
        $this->vet_dados['usuario_status'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Usuários';
        $this->vet_dados['tela']      = 'usuario_status';

        return $this->parser->parse('usuario_status/usuario_status_cad_view', $this->vet_dados, TRUE);

    }

    public function edit()
    {
        $user = $this->fetch($this->status_id);

        $vet['status_id']        = ($user->status_id);
        $vet['status_name']      = ($user->status_name);

        $this->vet_dados['usuario_status'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Usuários';
        $this->vet_dados['tela']      = 'usuario_status';

        return $this->parser->parse('usuario_status/usuario_status_cad_view', $this->vet_dados, TRUE);

    }

    public function fetch($id)
    {
        $this->db->from('usuario_status');
        $this->db->where('status_id', (int) $id);

        return $this->db->get()->row();
    }

    public function fetchAll()
    {
        $this->db->from('usuario_status');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->where('status_admin', 0);
        }

        return $this->db->get()->result();
    }

    public function save()
    {
        if($this->valida() === FALSE)
            return $this->cadastro();

        $data = [
            'status_name' => $this->status_name
        ];

        $id = (int) $this->status_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('usuario_status');

            $id = $this->db->insert_id();

            if ($this->db->trans_status() && $id > 0) {
                $this->db->trans_commit();
                syslog::generate_log("NEW_USER_STATUS_SUCCESS");
                return TRUE;

            } else {
                $this->db->trans_rollback();
                syslog::generate_log("NEW_USER_STATUS_ERROR");
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }

        } else {
            $this->setFields($data);
            $this->db->where('status_id', $id);
            $this->db->update('usuario_status');

            $count = $this->db->affected_rows();

            if ($this->db->trans_status() && $count > 0) {
                syslog::generate_log("UPDATE_USER_STATUS_SUCCESS");
                $this->db->trans_commit();
                return TRUE;

            } else {
                $this->db->trans_rollback();
                syslog::generate_log("UPDATE_USER_STATUS_ERROR");
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }
        }
    }

    public function delete($id)
    {
        $this->db->where('status_id', $id);
        if($this->db->delete('usuario_status')){
            syslog::generate_log("DELETE_USER_STATUS_SUCCESS");
            return true;
        } else {
            syslog::generate_log("DELETE_USER_STATUS_ERROR");
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('usuario_status');
    }
}