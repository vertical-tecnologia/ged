<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 01/06/15
 * Time: 22:29
 */

class empresa_model extends CI_Model{    

	public function __construct()
	{
		parent::__construct();
		$this->vet_dados = base_dir('public');
		$this->load->model('segmento_model');
		$this->load->model('usuario_model');
		$this->load->model('endereco_model');
		$this->load->model('contato_tipo_model');
		$this->load->model('contato_model');
//        $this->usuario_model->fetchAll(new UsuarioEntity());
	}

	public function index()
	{
		$entity = new EmpresaEntity();
		if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO' && $this->session->userdata('USER_ROLE') != 'SUPORTE'){
			$entity->empresa_user_id = $this->session->userdata('USER_ID');
			$this->vet_dados['button_add'] = '';
		} else {
			$this->vet_dados['button_add'] = anchor('empresa/cadastro', '<i class="fa fa-plus"></i> Nova Empresa', array('title'=>'Cadastrar Empresa', 'class'=>"btn btn-primary pull-right"));
		}

		$empresa = $this->fetchAll($entity);
		$Perfil = 1; // perfil empresa

		for ($i = 0;$i < count($empresa);$i++){
			$empresa[$i]->link_del = $empresa[$i]->empresa_principal == 1 ? '' : anchor('empresa/excluir/' . $empresa[$i]->empresa_id, 'Excluir', array('title' => 'Excluir Empresa'));
			$empresa[$i]->link_upd = anchor('empresa/editar/' . $empresa[$i]->empresa_id, 'Editar', array('title' => 'Editar Empresa'));
			
			if($empresa[$i]->empresa_principal == 1 &&
				$this->session->userdata('USER_ROLE') != 'SUPERUSUARIO' && $this->session->userdata('USER_ROLE') != 'SUPORTE' &&
				$empresa[$i]->tipo == 'P.Física')
			{
				$Perfil = 2; // perfil uso pessoal
			}
		}

		$this->vet_dados['empresa'] = $empresa;

		if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO'){
			$this->vet_dados['disable_new'] = count($this->vet_dados['empresa']) > 0 ? ' disabled' : '';
		}

		if($Perfil == 2){
			return $this->parser->parse('empresa/empresa_list_personal_view', $this->vet_dados, TRUE);
		}
		else{
			return $this->parser->parse('empresa/empresa_list_view', $this->vet_dados, TRUE);
		}
	}

	public function cadastro()
	{
		$total = $this->geTotalEmpresas();

		if ( $total->total_empresas > 0 && $this->session->userdata('USER_ROLE') != 'SUPERUSUARIO' ){
			redirect('empresa');
			return;
		}

		return $this->parser->parse('empresa/profile', $this->vet_dados, TRUE);
	}
	
	public function chama_cadastro($tipo)
	{
		$vet = array();
		$vet['empresa_razao'] = set_value('empresa_razao');
		$vet['empresa_fantasia'] = set_value('empresa_fantasia');
		$vet['empresa_cnpj'] = set_value('empresa_cnpj');
		$vet['empresa_ie'] = set_value('empresa_ie');
		$vet['empresa_segmento_id'] = set_value('empresa_segmento_id');
		$vet['empresa_user_id'] = set_value('empresa_user_id');
		$vet['disabled'] = "";
		$vet['empresa_id'] = set_value('empresa_id');
		$vet['empresa_rg_responsavel'] = set_value('empresa_rg_responsavel');

		$this->vet_dados['empresa'] = array($vet);
		$this->vet_dados['head_msg'] = "";
		$this->vet_dados['page_title'] = "Nova Empresa";

		$segEntity = new SegmentoEntity();
		$segEntity->segmento_status_id = 2;
		$this->vet_dados['segmento'] = $this->segmento_model->fetchAll($segEntity);
		$usuario = new UsuarioEntity();
		if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
			$usuario->user_type_id = 2;
		}

		if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
			$usuario->user_id = $this->session->userdata('USER_ID');
		}

		$this->vet_dados['usuario'] = array($this->usuario_model->fetch($usuario));
		
		if($tipo == STR_PROFILE_PERSONAL){
			return $this->parser->parse('empresa/empresa_cad_personal_view', $this->vet_dados, TRUE);
		}else{
			return $this->parser->parse('empresa/empresa_cad_view', $this->vet_dados, TRUE);
		}
	}

	public function visualizar()
	{
		$entity = new EmpresaEntity();
		$entity->empresa_id = $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);

		$this->vet_dados['page_title'] = "Detalhes da Empresa";
		$endereco = new EnderecoEntity();
		$contato = new ContatoEntity();
		$endereco->empresa_id = $entity->empresa_id;
		$contato->contato_empresa_id = $entity->empresa_id;

		$this->vet_dados['enderecos'] = $this->endereco_model->fetchAll($endereco);
		$this->vet_dados['contatos'] = $this->contato_model->fetchAll($contato);

		$empresa = $this->fetch($entity);

		$this->vet_dados['empresa'] = array($empresa);

		$this->vet_dados['empresa_id'] = $empresa->empresa_id;
		$this->vet_dados['user_id'] = null;
		$this->vet_dados['type'] = $this->contato_tipo_model->fetchAll();


		return $this->parser->parse('empresa/empresa_view', $this->vet_dados, TRUE);
	}

	public function editar()
	{
		$entity = new EmpresaEntity();
		$entity->empresa_id = $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);

		$empresa = $this->fetch($entity);

		$this->vet_dados['readonly'] = 'readonly';
		$this->vet_dados['disabled'] = 'disabled';
		// if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
			// $empresa->disabled = "readonly";
			// $this->vet_dados['head_msg'] = "Ediçao desabilitada para essa empresa.";
		// }
		// else{
		// 	$empresa->disabled = "";
		// 	$this->vet_dados['head_msg'] = "";
		// }
		// 

		if ($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
			$this->vet_dados['readonly'] = '';
			$this->vet_dados['disabled'] = '';
		}

		$this->vet_dados['head_msg'] = '';
		$this->vet_dados['empresa'] = array($empresa);
		$this->vet_dados['page_title'] = $empresa->tipo == "PERSONAL" ? "Editar Cadastro" : "Editar Empresa";

		$usuario = new UsuarioEntity();
		$usuario->user_id = $entity->empresa_user_id;

		$this->vet_dados['usuario'] = $this->fetchMyUsers($empresa);
		$entSegmento = new SegmentoEntity();
		$entSegmento->segmento_status_id = 2;
		$this->vet_dados['segmento'] = $this->segmento_model->fetchAll($entSegmento);

		$this->setSelected($this->vet_dados['segmento'], $empresa->empresa_segmento_id, 'segmento_id');
		$this->setSelected($this->vet_dados['usuario'], $empresa->empresa_user_id, 'user_id');

		$this->vet_dados['total_server'] = isset($empresa) ? $empresa->total_server : 0;

		if ($empresa->tipo == "PERSONAL") {
			return $this->parser->parse('empresa/empresa_cad_personal_view', $this->vet_dados, TRUE);
		}
		else{
			return $this->parser->parse('empresa/empresa_cad_view', $this->vet_dados, TRUE);
		}
	}

	public function endereco()
	{
		$entity = new EnderecoEmpresaEntity();
		$entity->empresa_id = (int) $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);
		$vet = null;

		$vet['endereco_alias'] = set_value('endereco_alias');
		$vet['endereco_cep'] = set_value('endereco_cep');
		$vet['endereco_logradouro'] = set_value('endereco_logradouro');
		$vet['endereco_num'] = set_value('endereco_num');
		$vet['endereco_bairro'] = set_value('endereco_bairro');
		$vet['endereco_cidade'] = set_value('endereco_cidade');
		$vet['endereco_estado'] = set_value('endereco_estado');
		$vet['endereco_complemento'] = set_value('endereco_complemento');
		$vet['endereco_id'] = set_value('');
		$vet['empresa_id'] = $entity->empresa_id;

		$this->vet_dados['endereco'] = array($vet);
		return $this->parser->parse('endereco/ajax/endereco_cad_view', $this->vet_dados, TRUE);
	}

	public function contato()
	{

		$entity = new EnderecoEmpresaEntity();
		$entity->empresa_id = (int) $this->input->post('empresa_id') != '' ? $this->input->post('empresa_id') : $this->uri->segment(3);
		$vet = null;

		$this->vet_dados['type'] = $this->contato_tipo_model->fetchAll();

		$vet['contato_id']    = set_value('contato_id');
		$vet['contato_valor'] = set_value('contato_valor');
		$vet['contato_tipo']  = set_value('contato_tipo');
		$vet['empresa_id']    = $entity->empresa_id;

		$this->vet_dados['contato'] = array($vet);
		return $this->parser->parse('contato/ajax/contato_cad_view', $this->vet_dados, TRUE);
	}

	public function save(EmpresaEntity $entity)
	{
		$this->db->trans_begin();

		$id = (int) $entity->empresa_id;

		if ($this->valida($id) === FALSE){
			if(((int) $entity->empresa_id > 0) === TRUE){
				return $this->editar();
			} else{
				return $this->cadastro();
			}
		}

		if($id == 0){
			$this->setFields($entity);
			$this->db->insert('empresa');
			$entity->empresa_id = (int) $this->db->insert_id();

			if($entity->empresa_id != 0){
				$this->session->set_flashdata('success', 'Empresa cadastrada com sucesso.');
				$this->db->trans_commit();
				syslog::generate_log('NEW_COMPANY_SUCCESS');
				return $entity;
			} else {
				$this->session->set_flashdata('error', 'Erro ao cadastrar empresa.');
				$this->db->trans_rollback();
				syslog::generate_log('NEW_COMPANY_ERROR');
				return FALSE;
			}

		} else {
			$this->setFields($entity);
			$this->db->where('empresa_id', $id);

			if($this->db->update('empresa')){
				$this->session->set_flashdata('success', 'Empresa alterada com sucesso.');
				$this->db->trans_commit();
				syslog::generate_log('UPDATE_COMPANY_SUCCESS');
				return $entity;
			} else {
				$this->session->set_flashdata('error', 'Erro ao alterar empresa.');
				$this->db->trans_rollback();
				syslog::generate_log('UPDATE_COMPANY_ERROR');
				return FALSE;
			}
		}
	}

	public function excluir(EmpresaEntity $entity)
	{
		$id = (int) $entity->empresa_id;

		$empresa = $this->fetch($entity);

		if(count($empresa) > 0 && $empresa->empresa_principal == 1){redirect('empresa');die;}


		if($id == 0){
			return false;
		} else {
			$this->db->where('empresa_id', $entity->empresa_id);
			$this->db->delete('empresa');

			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('success', 'Empresa exclída com sucesso.');
				syslog::generate_log('DELETE_COMPANY_SUCCESS');
				return true;
			}
			else{
				$this->session->set_flashdata('error', 'Erro ao excluir empresa.');
				syslog::generate_log('DELETE_COMPANY_SUCCESS');
				return false;
			}
		}
	}

	public function fetch(EmpresaEntity $entity)
	{
		//$this->db->select("empresa.*, segmento.segmento_name as seg_name, usuario.user_fullname");
		
		$this->db->select("empresa.*, segmento.segmento_name as seg_name, usuario.user_fullname, if(empresa.empresa_cnpj is null, 'PERSONAL', 'COMPANY') as tipo", FALSE);
		$this->db->from('empresa');
		$this->db->join('segmento', 'segmento.segmento_id = empresa.empresa_segmento_id');
		$this->db->join('usuario', 'usuario.user_id = empresa.empresa_user_id', 'left');

		if($entity->empresa_user_id != ''){
			$this->db->where('empresa_user_id', $entity->empresa_user_id);
		}

		if($entity->empresa_id != ''){
			$this->db->where('empresa_id', $entity->empresa_id);
		}

		return $this->db->get()->row();

	}

	public function fetchAll(EmpresaEntity $entity)
	{   
		//if(empresa.empresa_cnpj is null, '$tipopf', '$tipopj') as tipo
		
		/*$querysql = "SELECT empresa.*, segmento.segmento_name as seg_name, usuario.user_fullname " +
					"FROM empresa " +
					"JOIN segmento ON segmento.segmento_id = empresa.empresa_segmento_id " +
					"LEFT JOIN usuario ON usuario.user_id = empresa.empresa_user_id";*/
		
		$this->db->select("empresa.*, segmento.segmento_name as seg_name, usuario.user_fullname, if(empresa.empresa_cnpj is null, 'P.Física', 'P.Jurídica') as tipo", FALSE);
		$this->db->from('empresa');
		$this->db->join('segmento', 'segmento.segmento_id = empresa.empresa_segmento_id');
		$this->db->join('usuario', 'usuario.user_id = empresa.empresa_user_id', 'left');

		if($entity->empresa_user_id != ''){
			$this->db->where('empresa_user_id', $entity->empresa_user_id);
			//$querysql .= " WHERE empresa_user_id = ".$entity->empresa_user_id;
		}

		if($entity->empresa_id != ''){
			$this->db->where('empresa_id', $entity->empresa_id);
			//$querysql .= " WHERE empresa_id = ".$entity->empresa_id;
		}

		return $this->db->get()->result();
		//return $this->db->executeQuery( $querysql );
	}

	protected function setFields($fields)
	{
		foreach($fields as $key => $value){
			if($key == 'empresa_id' || $value == null || $value == '') continue;
			if($key == 'empresa_ie') $value = strtoupper($value);
			$this->db->set($key, $value);
		}
	}

	protected function geTotalEmpresas()
	{
		$this->db->select('count(empresa_id) as total_empresas');
		$this->db->from('empresa');

		$this->db->where('empresa_user_id', $this->session->userdata('USER_ID'));

		return $this->db->get()->row();
	}

	private function valida($argEmpresaId = null, $argCnpjCpf= null)
	{
		$this->form_validation->set_message('required', 'Campo obrigatório');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if (isset($_POST['empresa_cpf_responsavel']) && $_POST['empresa_cpf_responsavel'])
			return $this->form_validation->run('empresa_cpf');

		if ($argEmpresaId )
			return $this->form_validation->run('empresa_edit');

		return $this->form_validation->run('empresa');
	}

	protected function setSelected($object, $selected, $field)
	{
		for($i=0;$i<count($object);$i++){
			if($object[$i]->$field == $selected){
				$object[$i]->selected = " selected ";
			}
			 else
				 $object[$i]->selected = " ";
		}

	}

	protected function fetchMyUsers($entity)
	{
		$this->db->from('usuario');
		$this->db->where('usuario.user_id', $entity->empresa_user_id);

		return $this->db->get()->result();
	}

	public function fetchMyServer($where)
	{
		$this->db->from('servidor');
		$this->db->where($where);
		return $this->db->get()->result();
	}


	public function servidor($argEmpresaId)
	{
		$empresa = $this->findCompany(array('empresa.empresa_id' => $argEmpresaId));
		$this->vet_dados['empresa_id']     = $argEmpresaId;
		$this->vet_dados['total_servidor'] = $empresa['total_server'];

		if (!$empresa || !isset($empresa['empresa_user_id']))
			redirect('empresa');

		$qntServidor = (int) $this->input->post('total_servidor');
		if (is_int($qntServidor) && $qntServidor > 0) {
			$arrDados = array('total_server' => $qntServidor);
			if ($this->saveTotalServer($empresa['empresa_user_id'], $arrDados)) {
				$this->session->set_flashdata('success', 'Quantidade de servidores cadastrada com sucesso.');
				redirect('empresa');
			}
		}
		return $this->parser->parse('empresa/servidor_view', $this->vet_dados, TRUE);
	}

	public function saveTotalServer($argUserId, $arrDados)
	{
		$this->db->where('user_id', $argUserId);
		return $this->db->update('usuario', $arrDados);
	}
}
