<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 20/06/15
 * Time: 22:39
 */

class template_menu_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
    }

    public function getMenuData()
    {

        $data = [
            'count_files' => $this->getNewFiles(),
            'size_files' => $this->getTotalSize(),
            'quota' => $this->get_quota(),
            'total_space' => $this->get_total_space(),
            'shared' => $this->session->userdata('QDS') == 1 ? '' : ' compartilhada',
            'total_storage' => $this->get_total_storage()
        ];


        return $data;

    }

    public function getNewFiles()
    {
        $this->db->select('count(id) as total_files');
        $this->db->from('file_object');

        if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO'){

            if(in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN', 'CLIPESSOA', 'INDEFINIDO'))){
                $this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
                $this->db->where('type', 1);
            }

        }

        return $this->db->get()->row();
    }

    public function getTotalSize()
    {
        $this->db->select('SUM(size) as total_size_files');
        $this->db->from('file_object');


        if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO'){

            if(in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN', 'CLIPESSOA', 'INDEFINIDO'))){
                $this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
            }

        }

        return $this->db->get()->row();
    }

    public function get_quota()
    {
        if($this->session->userdata('QDS') == 0)
            return $this->get_quota_size();

        $this->db->select('quota_size');
        $this->db->from('usuario_quota');
        $this->db->where('user_id', $this->session->userdata('USER_ID'));

        return $this->db->get()->row();

    }

    public function get_aditional()
    {
        $this->db->select('SUM(quantidade_armazenamento.quant_valor) as adittional_space');
        $this->db->from('espaco_adicional');
        $this->db->join('quantidade_armazenamento', 'quantidade_armazenamento.quant_id = espaco_adicional.quant_id');
        $this->db->where('client_id', $this->session->userdata('CLID'));

        return $this->db->get()->row();
    }

    protected function get_quota_size()
    {
        if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') return null;
        $plano = $this->get_plan();
        @$size =  $this->get_services($plano->plan_id);
        $additional = $this->get_aditional();
        $size->quota_size = $size->quota_size + $additional->adittional_space;

        return $size;

    }

    protected function get_services($plan)
    {
        $this->db->select('SUM(servico.service_storage) as quota_size');
        $this->db->from('servico');
        $this->db->join('plano_servico', 'plano_servico.service_id = servico.service_id');
        $this->db->where('servico.service_storage <> -1');
        $this->db->where('plano_servico.plan_id', $plan);

        return $this->db->get()->row();
    }

    protected function get_plan()
    {
        $this->db->select('plano.plan_id');
        $this->db->from('cliente');
        $this->db->join('plano', 'plano.plan_id = cliente.client_plan_id');
        $this->db->where('client_id', $this->session->userdata('CLID'));
        return $this->db->get()->row();
    }

    protected function get_total_storage()
    {
        $this->db->select('storage_size');
        $this->db->from('compania');
        return $this->db->get()->row();
    }

    protected function get_total_space()
    {
        $additional = $this->get_aditional();
        $total = $this->get_quota_size();
        return @ $total->quota_size;
    }
}
