<?php

require APPPATH . 'exceptions/InvalidXMlFileException' . EXT;

/**
 * Class arquivos_model
 *
 * @created 11/07/15
 * @author  Vagner Leitte <vagnerleitte@outlook.com>
 * @version 1.0
 * @package		App\Models
 */
class arquivos_model extends CI_Model
{

	public $vet_dados;
	public $filesystem;
	protected static $homeFolder = 'Home';
	protected $storage;
	protected $str_key;
	protected $quota;
	protected $usage;

	public function __construct()
	{
		parent::__construct();

		$this->vet_dados = base_dir($this->session->userdata('skin'));
		$this->load->model('servidor_model');

		// $this->loadFileSystem($this->session->userdata('SRID'), $this->session->userdata('STR_DRIVER'));

		$this->vet_dados['current_directory'] = $this->uri->segment(3) != '' ? $this->uri->segment(3) : self::$homeFolder;
		$this->vet_dados['current_directory_url'] = $this->uri->segment(3) != '' ? 'folder/' . $this->uri->segment(3) :  'arquivos/folder/' . self::$homeFolder;

		$this->storage = $this->session->userdata('STR_DRIVER');
		$this->str_key = $this->session->userdata('STR_KEY');

		$this->quota = $this->get_quota();

		$this->usage = $this->get_usage()->disk_usage;

		$this->adittional = $this->get_aditional();

		$directory  = $this->getChildrenDir(null, $this->input->get('server'));

		$arrServers =  $this->get_servers();
		$this->load->vars(array('directories' => $directory, 'servers' => $arrServers));

		$this->vet_dados['free'] = $this->quota - $this->usage;
		$this->vet_dados['max'] = $this->vet_dados['free'] > 1000000000 ? 1000000000 : $this->vet_dados['free'] ;
	}

	public function ftp()
	{
		$server = $this->get_ftp_server();

		if($server->ftp_host == '' || $server->ftp_user == '' || $server->ftp_password == '')
			redirect('configuracao/ftp');

		return $this->parser->parse('arquivos/preview_ftp_view', $this->vet_dados, TRUE);
	}

	public function elfinder()
	{
		$dir = dirname(__FILE__).DIRECTORY_SEPARATOR.'../libraries/elfinder/php';

		include_once $dir.'/elFinderConnector.class.php';
		//Logvars::escreve(__CLASS__.':'.'Antes-Antes:1->>');
		include_once $dir.'/elFinder.class.php';
		//Logvars::escreve(__CLASS__.':'.'Antes-Antes:2->>');
		include_once $dir.'/elFinderVolumeDriver.class.php';
		//Logvars::escreve(__CLASS__.':'.'Antes-Antes:3->>');
		include_once $dir.'/elFinderVolumeMySQL.class.php';
		//Logvars::escreve(__CLASS__.':'.'Antes-Antes:4->>');
		include_once $dir.'/elFinderVolumeLocalFileSystem.class.php';
		//Logvars::escreve(__CLASS__.':'.'Antes-Antes:5->>');
		include_once $dir.'/elFinderVolumeFTP.class.php';

		$server = $this->get_ftp_server();

		$options['roots'] = array(
			array(
				'driver' => 'FTP',
				'host' => $server->ftp_host,
				'user' => $server->ftp_user,
				'pass' => $server->ftp_password,
				'path' => '/',
				'attributes' => array(
					array(
						'pattern' => '/^TEST$/', //You can also set permissions for file types by adding, for example, .jpg inside pattern.
						'read' => true,
						'write' => true,
						'locked' => true
					)
				)
			)
		);


		$this->conn = new elFinderConnector(new elFinder($options));
		$this->conn->run();
	}

	public function get_ftp_server()
	{
		$this->db->select('cliente.ftp_host, cliente.ftp_user, cliente.ftp_password');
		$this->db->from('cliente');
		$this->db->where('client_id', $this->session->userdata('CLID'));

		return $this->db->get()->row();
	}

	public function index()
	{
		$search   = isset($_GET['q']) ? $_GET['q'] : null;
		$path     = $this->vet_dados['current_directory'];
		$trash    = $this->uri->segment(2) == 'lixeira';
		$serverId = $this->input->get('server');

		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($serverId);

		$return = $this->libfactoryserver->listFiles($path, $search, $trash);
		$arquivos = $return['arquivos'];
		$breads   = $return['breads'];

		$this->vet_dados['empty'] = $this->parser->parse('arquivos/empty_directory_view', $this->vet_dados, TRUE);

		if ($return == false)
			$this->vet_dados['empty'] = $this->parser->parse('arquivos/dir_not_exits_view', $this->vet_dados, TRUE);

		if ($this->uri->segment(2) == 'lixeira' && count($arquivos) == 0)
			$this->vet_dados['empty'] = $this->parser->parse('arquivos/empty_trash_view', $this->vet_dados, TRUE);

		if ($this->uri->segment(2) == 'compartilhamento' && count($arquivos) == 0)
			$this->vet_dados['empty'] = $this->parser->parse('arquivos/empty_directory2_view', $this->vet_dados, TRUE);

		if ($this->uri->segment(2) == 'folder') {
			$arrDirectories = array();
			$directory = $this->getParentDir($this->uri->segment(3));
			$this->recursiveChildrenDir($directory, $arrDirectories);

			foreach ($arrDirectories as $index => $Directories) {
				foreach (array_values($Directories) as $index2 => $directory) {
					if ($tete = array_key_exists($directory['id'], $arrDirectories)){
						$arrDirectories[$index][$index2]['directories_children'] = $arrDirectories[$directory['id']];
						unset($arrDirectories[$directory['id']]);
					}
				}
			}
			foreach ($arrDirectories as $index => $dir) {
				$html = '';
				$this->buildSubDirMenuExplorer($dir, $html, $serverId);
				$arrDirectories[$index] = $html;
			}
			$this->load->vars('subHtmlDirs', $arrDirectories);
		}

		if ($arquivos)
			$this->vet_dados['empty'] = NULL;

		if ($this->vet_dados['current_directory'] != '') {
			$log             = new ObjectLog();
			$log->log_action = 'list_files';
			$log->user_key   = $this->session->userdata('USER_KEY');
			$log->hash       = $this->vet_dados['current_directory'];
			$log->datetime   = date('Y-m-d h:i:s');
			$this->generate_log($log);
		}

		$this->vet_dados['fm_class'] = $this->session->userdata('filemanager_view');
		$this->vet_dados['fm_ico_class'] = $this->session->userdata('filemanager_ico');
		$this->vet_dados['breads']   = $breads;
		$this->vet_dados['arquivos'] = $arquivos;

		// $directory = $this->getChildrenDir();
		
		// if ($this->session->userdata('SRID') == SERVER_LOCAL)
		// 	$serverId = SERVER_LOCAL;

		$this->load->vars(array('server_id' => $serverId));

		if ($this->uri->segment(2) == 'lixeira')
			return $this->parser->parse('arquivos/trash_file_manager', $this->vet_dados, TRUE);

		$permissions = array();
		foreach ($arquivos as $key => $value) {
			$permissions[] = $this->verifyPermissionId($value);
	
			foreach ($permissions as $key => $value) {
				if (empty($value) || $value == '' || $value == null)
					unset($permissions[$key]);
			}
		}

		$this->vet_dados['permissions'] = $permissions;
		return $this->parser->parse('arquivos/file_manager', $this->vet_dados, TRUE);
	}

	public function galeria()
	{
		$server = $this->input->get('server');

		if (!$server && $this->session->userdata('SRID') == SERVER_LOCAL)
			$server = SERVER_LOCAL;

		$entity = new ObjectEntity();
		$entity->server_id = $server;
		$arquivos = $this->getImageList($entity);

//        Debug::Test($arquivos);die;

		$this->vet_dados['arquivos'] = $arquivos;
		$server = !empty($server) ? $server : '0';
		$this->load->vars('server_id', $server);
		return $this->parser->parse('arquivos/gallery_view', $this->vet_dados, TRUE);
	}

	public function preview()
	{
		$download = new DownloadEntity();
		$download->key = $this->uri->segment(3);
		$link = $this->has_link($download);

		if (is_null($link->downloaded) && $link->single == 1) {
			$download->link_id = $link->link_id;
			$download->downloaded = date('y-m-d h:i:s', time());
			$download->user_agent = $this->input->user_agent();
			$download->ip_address = getRealIP();
			$obj = new ObjectEntity();
			$obj->hash = $link->hash;
			$object = $this->getObject($obj);

			$object->download_key = $download->key;
			$this->vet_dados['object'] = array($object);

			switch($object->mime_file_type){
				case 'audio/mpeg':
					return $this->parser->parse('preview/audio', $this->vet_dados, TRUE);
					break;
				case 'image/vnd.dxf':
				case 'image/bmp':
				case 'image/prs.btif':
				case 'image/vnd.dvb.subtit':
				case 'image/x-cmu-raster':
				case 'image/cgm':
				case 'image/x-cmx':
				case 'image/vnd.dece.graph':
				case 'image/vnd.djvu':
				case 'image/vnd.dwg':
				case 'image/vnd.fujixerox.':
				case 'image/vnd.xiff':
				case 'image/vnd.fst':
				case 'image/vnd.fastbidshe':
				case 'image/vnd.fpx':
				case 'image/vnd.net-fpx':
				case 'image/x-freehand':
				case 'image/g3fax':
				case 'image/gif':
				case 'image/x-icon':
				case 'image/ief':
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/vnd.ms-modi':
				case 'image/ktx':
				case 'image/x-pcx':
				case 'image/vnd.adobe.phot':
				case 'image/x-pict':
				case 'image/x-portable-any':
				case 'image/x-portable-bit':
				case 'image/x-portable-gra':
				case 'image/png':
				case 'image/x-portable-pix':
				case 'image/svg+xml':
				case 'image/x-rgb':
				case 'image/tiff':
				case 'image/vnd.wap.wbmp':
				case 'image/webp':
				case 'image/x-xbitmap':
				case 'image/x-xpixmap':
				case 'image/x-xwindowdump':
//                    $this->use_link($download);
					return $this->parser->parse('preview/image', $this->vet_dados, TRUE);
					break;
				case 'application/pdf':
					return $this->parser->parse('preview/pdf', $this->vet_dados, TRUE);
					break;
			}

		} else {
			echo 'null';
		}


		return $this->parser->parse('arquivos/preview_file_view', $this->vet_dados, TRUE);

	}

	public function edit()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);

		$this->vet_dados['custom'] = $this->get_labels($entity);

		$this->vet_dados['no_custom_data'] = count($this->vet_dados['custom']) > 0 ? '' : '<li>Nenhuma tag customizada. </li>';

		$this->vet_dados['arquivo'] = array($object);

		$this->load->vars('server_id', $object->server_id);

		return $this->parser->parse('arquivos/file_edit_view', $this->vet_dados, TRUE);
	}

	public function view()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$breads = null;
		$server = $this->input->get('server');

		$object = $this->getObject($entity, true);

		if (isset($object->parent) && !is_null($object->parent)) {
			$parent = new ObjectEntity();
			$parent->parent = $object->parent;
			$breads = $this->get_parents($parent);
		}

		$this->vet_dados['current_directory_url'] = isset($object->parent) && !is_null($object->parent) ? 'folder/'.$object->parent_id : 'folder/Home';
//        Dd::Test($this->vet_dados);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}

		$object->size = By2Mb($object->size);

		if($object->in_trash){
			$this->vet_dados['current_directory_url'] = 'lixeira';
			$this->vet_dados['breads'] = '';
		} else
			$this->vet_dados['breads'] = $breads;

		$this->vet_dados['arquivo'] = array($object);
		$this->vet_dados['media_preview'] = null;
		$Filelog = $this->file_log($entity);

		for($i=0;$i<count($Filelog);$i++){
			$Filelog[$i]->datetime = data_formatada($Filelog[$i]->datetime, 1);
			$Filelog[$i]->log_action_desc = $this->lang->line('log_' .$Filelog[$i]->log_action);
		}
//        Dd::Test($log);

		$log = new ObjectLog();
		$log->log_action = 'view_file';
		$log->user_key = $this->session->userdata('USER_KEY');
		$log->hash = $object->hash;
		$log->datetime = date('Y-m-d h:i:s');

		$this->generate_log($log);

		$this->vet_dados['file_log_list'] = $Filelog;

		// $this->vet_dados['custom'] = $this->get_labels($entity);

		$arrTags = array();
		$this->vet_dados['custom'] = $this->verifyParent($object->id, $arrTags);
		$this->vet_dados['no_custom_data'] = count($this->vet_dados['custom']) > 0 ? '' : '<li>Nenhuma tag customizada. </li>';

		$this->load->vars('server_id', $object->server_id);
		$directoryPermissions = $this->verifyPermissionId($object);

		$this->vet_dados['directoryPermissions'] = array($directoryPermissions);
		// $this->vet_dados['owner'] = $this->session->userdata("IS_OWNER");
		$this->vet_dados['owner'] = $this->session->userdata("IS_OWNER");
		$this->vet_dados['ownDirectory'] = false;

		if($this->vet_dados['owner'] == '0'){
			$this->vet_dados['ownDirectory'] = $this->verifyOwnDirectory($this->session->userdata("USER_KEY"), $object->id);
		}

		return $this->parser->parse('arquivos/file_detail_view', $this->vet_dados, TRUE);
	}

	public function new_directory()
	{
		if ($server = $this->input->get('server')){
			$this->load->vars('server_id', $server);
			$this->vet_dados['current_directory'] = $this->uri->segment(3);
		}

		return $this->parser->parse('arquivos/new_directory_view', $this->vet_dados, TRUE);
	}

	public function export()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$object = $this->getObject($entity, true);

		$this->vet_dados['hash'] = $object->hash;
		$this->vet_dados['original_name'] = $object->original_name;

		return $this->parser->parse('arquivos/export_xml', $this->vet_dados, TRUE);
	}

	private function verifyParent($fileID, &$arrTags)
	{
		$this->db->select('*');
		$this->db->from('file_object');
		$this->db->where('id', $fileID);

		$result = $this->db->get()->result();
		if (isset($result) && !is_null($result[0]->parent))
			$this->verifyParent($result[0]->parent, $arrTags);

		$entity = new ObjectEntity();
		$entity->hash = $result[0]->hash;

		$arrTags[$result[0]->id] = $this->get_labels($entity);

		unset($entity);

		return $arrTags;
	}


	public function download_xml($hash, $wfile)
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$object = $this->getObject($entity, true);
		$labels = array();

		$this->verifyParent($object->id, $labels);

		$file_path = FCPATH . "skin/public/xml/". $wfile . ".xml";
		
		$contents = file($file_path);
		if($wfile == 'example1'){
			$contents[30] = "			<value>".$object->full_path."</value>";
			$contents[33] = "			<value>". $wfile. ".pdf</value>";
			$contents[36] = "			<value>". $wfile. ".xml</value>";
			$linha = 6;
			foreach ($labels as $chave => $valor) {
				foreach ($valor as $key => $value) {
					if($contents[$linha]){
						$contents[$linha] = '		<item id = "' . $valor[$key]->label_description . '">' . $valor[$key]->label_content . '</item>';
					}
					$linha+=1;
				}
			}
		}
		if($wfile == 'example2'){
			$contents[40] = "				<value>". $wfile. ".pdf</value>";
			$contents[37] = "				<value>".$object->full_path."</value>";
			$linha = 5;
			foreach ($labels as $chave => $valor) {
				foreach ($valor as $key => $value) {
					if($contents[$linha]){
						$contents[$linha] = '		<number id= "' . $valor[$key]->label_description . '" desc="">'."\n" .
							'			<value>' . $valor[$key]->label_content . '</value>'."\n".
						'		</number>';
						;
					}
					$linha+=1;
				}
			}
		}

		$fileName = $wfile .".xml";
		$fileArray = array_values(array_filter($contents, "trim"));
		$contents = implode("\n", $fileArray);
		
		header("Content-Disposition: attachment; filename=\"" . basename($fileName) . "\"");
		header("Content-Type: application/force-download");
		header("Connection: close");

		echo $contents;
	}

	public function trash_folder()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$object = (object) $this->getObject($entity);
		$server = $this->input->get('server');

		if ($this->session->userdata('SRID') == SERVER_LOCAL)
			$server = $this->session->userdata('SRID');

		$totalFiles = $this->get_childrens($object, true);

		$this->vet_dados['totalFiles'] = $totalFiles;

		if($totalFiles > 0){
			if($totalFiles == 1)
				$msg = $totalFiles.' arquivo nessa pasta também será excluído.';
			else
				$msg = $totalFiles.' arquivos nessa pasta também serão excluídos.';
			$this->vet_dados['msg'] = $msg;
		} else {
			$msg = 'Nenhum arquivo para remover.';
			$this->vet_dados['msg'] = $msg;
		}

		$this->vet_dados['object'] = array($object);
		$this->load->vars('server_id', $server);

		return $this->parser->parse('arquivos/send_trash_folder', $this->vet_dados, TRUE);
	}

	public function send_trash_folder()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$object = $this->getObject($entity);

		if (!$object->owner == $this->session->userdata('USER_KEY')) {
			// return GedFileSystem::$DELETE_PERMISSION_DENIED;
			return 'DELETE_PERMISSION_DENIED';
		}

		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($object->server_id);
		return $this->libfactoryserver->sendFolderTrash($object);
	}

	public function get_childrens($entity, $count = false)
	{
		if ($count)
			$this->db->select('count(*) as total');
		$this->db->from('file_object');
		$this->db->where('parent', $entity->id);
		$return = $this->db->get()->result();

		if (isset($return[0]->total))
			return $return[0]->total;

		return $return;
	}

	public function saveDir()
	{
		$folder = $this->input->post('curdir');
		$server = $this->input->get('server');

		$new_directory = array(
			'original_name' => $this->input->post('directory'),
			'type'          => 2,
			'mime'          => $this->getObjectMime('directory', null)->id_file_type,
			'is_image'      => false,
			'extension'     => null,
			'size'          => 0,
			'owner'         => $this->session->userdata('USER_KEY'),
			'storage_key'   => $this->session->userdata('STR_DRIVER'),
			'mime'          => $this->getObjectMime('directory', null)->id_file_type,
			'server_id'     => !empty($server) ? $server : null,
			'ico_file_type'
		);

		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setServer($server);
		$directory = $this->libfactoryserver->createFileObject($new_directory, $folder);

		if ($this->dir_exists($directory->original_name, $directory->parent)) {
			$this->session->set_flashdata('error', "Já existe um diretório chamado \{$directory->original_name}\ em \{$directory->full_path}\\");
			return false;
		}

		$this->libfactoryserver->setTypeServer($server);
		$response = $this->libfactoryserver->newDiretory($folder, $directory);

		// $storedObject = array('status' => true, 'msg' => GedFileSystem::$DIRECTORY_CREATED);
		$storedObject = array('status' => true, 'msg' => 'Teste');
		$stored = $this->storeObject($directory);

		$log             = new ObjectLog();
		$log->log_action = 'new_dir';
		$log->user_key   = $this->session->userdata('USER_KEY');
		$log->hash       = $stored->hash;
		$log->datetime   = date('Y-m-d h:i:s');

		$this->generate_log($log);

		if ($stored != false) {
			$this->session->set_flashdata('success', "Diretório \{$directory->original_name}\ criado com sucesso em \{$directory->full_path}\\");
			return $storedObject;
		}

		$this->session->set_flashdata('error', 'Não foi possível criar o diretório.');
		return false;

		// $file = new ObjectEntity();
		// $file->original_name = $this->input->post('directory');
		// $file->in_trash      = false;
		// $file->size          = 0;
		// $file->type          = 2;
		// $file->hash          = random_string('alnum', 30);
		// $file->extension     = null;
		// $file->owner         = $this->session->userdata('USER_KEY');
		// $file->storage_key   = $this->session->userdata('STR_DRIVER');
		// $file->created       = date('Y-m-d h:i:s');
		// $mimeId              = $this->getObjectMime('directory', null)->id_file_type;
		// $file->mime          = (int)$mimeId > 0 ? $mimeId : 1;

		// $file->is_image = false;

		// $parent          = $this->get_parent($this->input->post('curdir'));
		// $file->full_path = isset($parent->full_path) && $parent->full_path != '' ? $parent->full_path : self::$homeFolder;
		// $file->parent    = isset($parent->id) ? $parent->id : null;

		// $dir_exists = $this->dir_exists($file->original_name, $parent->id);
		// if ($dir_exists) {
		// 	$msg = 'Já existe um diretório chamado \"' . $file->original_name . '\" em \"' . $file->full_path . '\"';
		// 	$this->session->set_flashdata('error', $msg);
		// 	return false;
		// }

		// $file->full_path .= DIRECTORY_SEPARATOR . $file->original_name;

		// $storedObject = [
		// 	'status' => true,
		// 	'msg'    => GedFileSystem::$DIRECTORY_CREATED,
		// ];

		// $stored = $this->storeObject($file);

		// $log = new ObjectLog();
		// $log->log_action = 'new_dir';
		// $log->user_key = $this->session->userdata('USER_KEY');
		// $log->hash = $stored->hash;
		// $log->datetime = date('Y-m-d h:i:s');

		// $this->generate_log($log);

		// if ($stored != false) {
		// 	$msg = 'Diretório \"' . $file->original_name . '\" criado com sucesso em \"' . $file->full_path . '\"';
		// 	$this->session->set_flashdata('success', $msg);
		// 	return $storedObject;
		// } else {
		// 	$msg = 'Não foi possível criar o diretório.';
		// 	$this->session->set_flashdata('error', $msg);
		// 	return false;
		// }
	}

	public function upload()
	{
		// $servers  = $this->servidor_model->fetchAll(new ServerEntity());
		// if ($servers)
		// 	$this->load->vars('servers', $servers);

		$this->vet_dados['current_directory'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 'Home'; //diretório raiz ftp
		$serverId = $this->input->get('server');

		if (!empty($serverId)) {
			$entity = new ServerEntity();
			$entity->server_id = $serverId;
			$server = $this->servidor_model->fetch($entity);

			if (!$server) {
				$this->session->set_flashdata('error', 'Servidor inválido.');
				redirect('/arquivos/');
			}

			$this->load->vars('server_id', $serverId);
			// $this->vet_dados['current_directory'] = isset($this->uri->segment(3)) ? $this->uri->segment(3) : 'Home'; //diretório raiz ftp
		}

		return $this->parser->parse('arquivos/upload_form', $this->vet_dados, TRUE);
	}

	public function download(ObjectEntity $entity = null)
	{
		if ($entity == null){
			$entity = new ObjectEntity();
			$entity->hash = $this->uri->segment(3);
		}

		$object = $this->getObject($entity, true);

		$download = new DownloadEntity();
		$download->hash = $object->hash;
		$download->single = true;
		$download->downloads =  '0';
		$download->downloaded = null;

		$link = $this->has_link($download);


		if($link === false || strtotime($link->expires) < time() || !is_null($link->downloaded) || $link->downloads > 0){

			$download->expires = date('y-m-d H:i:s', time() + (60*60));
			$download->key = random_string('alnum', 100);
			$download->user_key = $this->session->userdata('USER_KEY');
			$download->downloaded = null;
			$download->downloads = 0;
			$link = $this->download_link($download);

			if($link != false){
				$link->message = 'Seu link de download está pronto.';
			} else {
				$link->message = 'Erro ao criar link de download.';
			}


		} else {
			$link->message = 'Seu link de download está pronto.';
		}

		$link->original_name = $object->original_name;
		$link->ico_file_type = $object->ico_file_type;

		$object->size = By2Mb($object->size);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}


		$this->vet_dados['object'] = array($link);


		return $this->parser->parse('arquivos/file_download_details', $this->vet_dados, TRUE);
	}

	public function open_link()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);

		$download = new DownloadEntity();
		$download->hash = $object->hash;
		$download->single = true;
		$download->downloads =  '0';
		$download->downloaded = null;

		$link = $this->has_link($download);
		if ($link === false || strtotime($link->expires) < time() || !is_null($link->downloaded) || $link->downloads > 0){

			$download->expires = date('y-m-d H:i:s', time() + (60*60));
			$download->key = random_string('alnum', 100);
			$download->user_key = $this->session->userdata('USER_KEY');
			$download->downloaded = null;
			$download->downloads = 0;
			$link = $this->download_link($download);

			if ($link != false) {
				$link->message = 'Seu link de download está pronto.';
			} else {
				$link->message = 'Erro ao criar link de download.';
			}

		} else {
			$link->message = 'Seu link de download está pronto.';
		}

		$log = new ObjectLog();
		$log->log_action = 'open_or_download';
		$log->user_key = $this->session->userdata('USER_KEY');
		$log->hash = $download->hash;
		$log->datetime = date('Y-m-d h:i:s');

		$this->generate_log($log);

		$link->original_name = $object->original_name;
		$link->ico_file_type = $object->ico_file_type;

		$object->size = By2Mb($object->size);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}

		return $link;

	}

	public function open(ObjectEntity $entity = null)
	{
		if ($entity == null) {
			$entity = new ObjectEntity();
			$entity->hash = $this->uri->segment(3);
		}

		$object = $this->getObject($entity);
		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($object->server_id, $this->storage);
		return $this->libfactoryserver->viewFile($object, false);
	}

	public function open_and_download(ObjectEntity $entity = null, $extenal = array())
	{
		if ($entity == null) {
			$entity = new ObjectEntity();
			$entity->hash = $this->uri->segment(3);
		}

		$object = $this->getObject($entity);
		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($object->server_id, $this->storage);
		$this->libfactoryserver->download($object);

// 		if ($this->filesystem->object_exists($object->hash)) {
// 			$download = $this->filesystem->download($object->hash, $extenal);
// 			echo $download;
// 		}
	}

	public function get()
	{
		$download = new DownloadEntity();
		$download->key = $this->uri->segment(3);
		$link = $this->has_link($download);

		if (is_null($link->downloaded) && $link->single == 1) {
			$download->link_id = $link->link_id;
			$download->downloaded = date('y-m-d h:i:s', time());
			$download->user_agent = $this->input->user_agent();
			$download->ip_address = getRealIP();
			$obj = new ObjectEntity();
			$obj->hash = $link->hash;
			$object = $this->getObject($obj);

			// if ($this->filesystem->object_exists($object->hash)){
			// 	$this->use_link($download);
			// }
			// $open = $this->uri->segment(4) == 'true' ? true : false;
			// $download = $this->filesystem->download($object->hash, array('fileName' => $object->original_name), $open);

			$this->load->library('LibFactoryServer');
			$this->libfactoryserver->setTypeServer($object->server_id, $this->storage);
			$this->libfactoryserver->download($object);

			$open = $this->uri->segment(4) == 'true' ? true : false;

			if ($open == true)
				echo $download;

			return true;

		} else {
			$this->vet_dados['msg'] = 'Link de download inválido';
			return $this->parser->parse('arquivos/file_download_failed', $this->vet_dados, TRUE);
		}

	}


	public function detail()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);
		$object->size = By2Mb($object->size);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}

		$this->vet_dados['object'] = array($object);
		return $this->parser->parse('arquivos/file_panel_details', $this->vet_dados, TRUE);
	}

	public function share()
	{
		$this->load->model('usuario_model');
		$this->load->model('grupo_model');
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}


		$share = new ShareEntity();
		$share->object = $object->hash;
		$shares = $this->get_share($share);


		$all_users = $this->all_users();

		for($i=0;$i<count($all_users);$i++){
			for($j=0;$j<count($shares);$j++){

				if($all_users[$i]->user_key == $shares[$j]->user){
					$all_users[$i]->selected = ' selected ';
					break;
				} else {
					$all_users[$i]->selected = '';
				}
			}
		}

		$all_group = $this->all_groups();

		for($i=0;$i<count($all_group);$i++){
			for($j=0;$j<count($shares);$j++){

				if($all_group[$i]->group_id == $shares[$j]->group){
					$all_group[$i]->selected = ' selected ';
					break;
				} else{
					$all_group[$i]->selected = '';
				}
			}
		}



		$this->vet_dados['object'] = array($object);
		$this->vet_dados['usuario'] = $all_users;
		$this->vet_dados['group'] = $all_group;

		$this->vet_dados['shares'] = $shares;

		return $this->parser->parse('arquivos/file_share_details', $this->vet_dados, TRUE);

	}

	public function shared(ShareEntity $entity)
	{

		$error = 0;
		$this->db->trans_begin();

		$object = new ShareEntity();
		$object->object = $entity->object;

		$this->delete_shared($object);

		if($entity->email != ''){
			$temp = new ShareEntity();
			$temp->email = $entity->email;
			$temp->object = $entity->object;
			$temp->shared_at = $entity->shared_at;
			if($this->save_shared($temp) ){
				$this->share_email($temp);
			}
		}

		if(is_array($entity->group))
		for($i=0;$i<count($entity->group);$i++){
			$temp = new ShareEntity();
			$temp->group = $entity->group[$i];
			$temp->object = $entity->object;
			if($this->share_exists($temp) === false){
				$temp->shared_at = $entity->shared_at;
				if($this->save_shared($temp) == false)
					$error++;
			}
		}

		if(is_array($entity->user))
		for($i=0;$i<count($entity->user);$i++){
			$temp = new ShareEntity();
			$temp->user = $entity->user[$i];
			$temp->object = $entity->object;
			if($this->share_exists($temp) === false){
				$temp->shared_at = $entity->shared_at;
				if($this->save_shared($temp) == false)
					$error++;
			}
		}

		if($error == 0){

			$log = new ObjectLog();
			$log->log_action = 'share';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $object->object;
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);

			$this->db->trans_commit();
			return true;
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}

	public function delete_shared(ShareEntity $entity)
	{
		$this->db->where('object', $entity->object);
		if($this->db->delete('share')){

			$log = new ObjectLog();
			$log->log_action = 'delete_share';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $entity->object;
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);

			return true;
		}
		else
			return false;


	}

	public function save_shared(ShareEntity $entity){
		$this->db->set('shared_at', $entity->shared_at);
		$this->db->set('object', $entity->object);

		if($entity->group != '')
			$this->db->set('group', $entity->group);

		if($entity->user != '')
			$this->db->set('user', $entity->user);

		if($entity->email != '')
			$this->db->set('email', $entity->email);

		$this->db->insert('share');

		if($this->db->insert_id() > 0){
			return true;
		}
		else
			return false;
	}

	public function share_exists(ShareEntity $entity)
	{
		$this->db->from('share');

		$this->db->where('object', $entity->object);

		if($entity->user != '')
			$this->db->where('user', $entity->user);

		if($entity->group != '')
			$this->db->where('group', $entity->group);

		if($entity->email != '')
			$this->db->where('email', $entity->email);

		$row = $this->db->get()->row();

		if(count($row) > 0)
			return $row;
		else
			return false;
	}

	public function trash()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}

		$this->vet_dados['object'] = array($object);

		return $this->parser->parse('arquivos/file_delete_details', $this->vet_dados, TRUE);
	}

	public function recover()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);
		$object->size = By2Mb($object->size);

		if ($object->is_image == 1) {
			$object->ico_file_type = base_url() . 'thumb/show/250/250/' . $object->hash;
		}

		$this->vet_dados['object'] = array($object);

		return $this->parser->parse('arquivos/recover_file_details', $this->vet_dados, TRUE);
	}

	public function recovery()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);
		$object = $this->getObject($entity, true);

		$this->load->library('LibFactoryServer');
		return $this->libfactoryserver->recoveryFiles($object);

		// if (count($object) > 0) {
		// 	$entity->in_trash = false;
		// 	if($this->sendObjectToTrash($entity)){

		// 		$log = new ObjectLog();
		// 		$log->log_action = 'recovery_file';
		// 		$log->user_key = $this->session->userdata('USER_KEY');
		// 		$log->hash = $entity->hash;
		// 		$log->datetime = date('Y-m-d h:i:s');

		// 		$this->generate_log($log);

		// 		return GedFileSystem::$FILE_RECOVERED;
		// 	}
		// 	else
		// 		return GedFileSystem::$UNABLE_RECOVER_FILE;

		// } else {
		// 	return GedFileSystem::$ERROR_NOT_EXISTS;
		// }

	}

	public function clean_trash()
	{
		$server = $this->input->get('server');
		if (!$server && $this->session->userdata('SRID') == SERVER_LOCAL)
			$server = $this->session->userdata('SRID');

		$objects = $this->get_files_in_trash($server);

		$totalFiles = count($objects);
		$this->vet_dados['totalFiles'] = $totalFiles;

		if ($totalFiles > 0) {
			if($totalFiles == 1)
				$msg = $totalFiles.' arquivo será permanentemente excluído.';
			else
				$msg = $totalFiles.' arquivos serão permanentemente excluídos.';

			$this->vet_dados['msg'] = $msg;
			$this->vet_dados['trash_img'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAPAUlEQVR4Xu2dXYidVxWG197nnMk0rYnJ/ERaEIVoqhdSlVLJXS9EQW+0EFHoRQvGRiuotZTYzMnkTJUgtQUNtka0guBfLkQQRQR7o7ZUGqp4oTH+XkgzZ2JMU3Vmzjl7y4RepKU562S+75v9fWs/vZ21917vu/b7zJ6fTJ3wHw7gQLYOuGyVIxwHcEAAAJcABzJ2AABkPHyk4wAA4A7gQMYOAICMh490HAAA3AEcyNgBAJDx8JGOAwCAO4ADGTsAADIePtJxAABwB3AgYwcAQMbDRzoOAADuAA5k7AAAyHj4SMcBAMAdwIGMHQAAGQ8f6TgAALgDOJCxAwAg4+EjHQcAAHcABzJ2AABkPHyk4wAA4A7gQMYOAICMh490HAAA3AEcyNgBAJDx8JGOAwCgIXdg1wNxZ2vb6P0uyO3i4y0uhDdGL68R8Z16SwgDEXlBxP9VnHsuRvmFrLZ+vPJFd6nefefRHQCo+ZxnFtdu9tEdDqN4wHs/XfN2J2ovhPBfL63vSmt4vH9s+uxEiyiqxAEAUImtxTfd89l4fdg+/LxIuFfEt4rvWMcdwiA6/+gNrn30b4tutY4dWu8JANRwwrNHVvc53/qhiLylhu2V3lIM8rtWe/SBc4vTfyl9czYc6wAAqNkFmV1Yf6dz8WcifqZmrVXaTpB4rhXlPctLU7+t9CA2f5kDAKBGF2LjM7+I/NL51myN2tqyVjYg0PZhPy+BLbOc/zfg1lk9/qQbF+P24Wj4m+jkrXXpKUUfoyDP7b7YftfZr7i1FOfndiYvgJpMfL47eCSKfLom7aRtw8nx/rHO4bRN5HE6AKjBnC9/00/c78X7ttpOkNPRx5PiwpOv/de2v9f9M+XeT8Zt/9659gbv/O3RuYMi8nZVo4SB9/FmvhTQnSpaAQCKOljC+pkjw296H+8at1WQ8D8f3b39pc4TIi6WcGyCLaKbWxjcFVw84cVfN64BJ+5ry732PQmazOpIAJB43LsX4w4X1p8fF4iN8Leif/fyUudXidst5fj5xcH+0TD83Hu//WobhhBebK9Ove7cw+4/pRzKJq/qAABIfDFmu8MPO4nfGd9GvLvfm3oicaulHj+3sH63OPeN8Zu6A/1e+1SpB7PZyxwAAIkvxPyR4cno40fHtPFsv9e+tbnP/qspi26uO3x23PcEonOPrxxrH0o8ItPHA4DE453rDp4RkVuvGpMYP7ayNHUycZuVHD+/sH5PdO6xMZs/1e919ldyOJtedgAAJL4Is0dW++N+8Se60b6VY9NnErdZyfEv/crzH67+fYC4fP6hqT2VHM6mAKAOd2Cuu7Y+7p/07rzQnq77j/o26+PGjwgv7hpe9R8BBQlr53vbTPwLyM16VPU6XgBVO6zsP9cdjP2RXr/XMT2j3PUnvn58CZB6ALkHIHf9qe+f6c8uqc2d5PzcA5C7/knuSJU1AKBKdyfYO/cA5K5/gitSaQkAqNReffPcA5C7fv2GVFsBAKr1V9099wDkrl+9IBUXAICKDda2zz0AuevX7kfVHwcAVTvMjwHHOgAA0l5AAJDWf8k9ALnrT3z9+D2A1APIPQC56099/3gBJJ5A7gHIXX/i68cLIPUAcg9A7vpT3z9eAIknkHsActef+PrxAkg9gNwDkLv+1PePF0DiCeQegNz1J75+vABSDyD3AOSuP/X9a/wLQLtAqQ3m/LwdqPvfcwAAed9P1FfsAACo2GBeABUbzPaFHAAAhezTFwMA3SMq0jkAACr2HgBUbDDbF3IAABSyT18MAHSPqEjnAACo2HsAULHBbF/IAQBQyD59MQDQPaIinQMAIJ33E50MQCayKduiuge46GAa/3sARQ0AAEUdtL0eANier/oXeYzLR57iAAAwfkV4ARgfcEF5AKCggXVfDgDqPqG0/QGAtP5XfjoAqNziRh8AABo9Pr15AKB7lHMFADA+fQBgfMAF5QGAggbWfTkAqPuE0vYHANL6X/npAKByixt9AABo9Pj05gGA7lHOFQDA+PQBgPEBF5QHAAoaWPflAKDuE0rbHwBI63/lpwOAyi1u9AEAoNHj05sHALpHOVcAAOPTBwDGB1xQHgAoaGDdl6cGgHbBqu4v9/O1+6n5o62v+8f5ewDdQUw5JO2CAYB6zyfl3SnjbAAAAMbeAesA0kKkAVpbX/ePAwAAAADGpBQA1B1hBfur+jOc1p52waruL/fzi85HW1/3j/MC4AXAC4AXQN05VV1/VX+G1TrP/TNwav1F56Otr/vHeQHwAuAFwAug7pyqrj9eAB0AAACqC1jddwYAAGDcHdW+RKn7/db640sAvgTgBcALQOOE3Y/zAuAFwAvAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVAQAAAADUmNgtAAAAAADYzbeqDAAAAACgxsRuAQAAAADAbr5VZQAAAAAANSZ2CwAAAAAAdvOtKgMAAAAAqDGxWwAAAAAAsJtvVRkAAAAAQI2J3QIAAAAAgN18q8oAAAAAAGpM7BYAAAAAAOzmW1UGAAAAAFBjYrcAAAAAAGA336oyAAAAAIAaE7sFAAAAAAC7+VaVzR9ZW4veT6mFFGTnQJCwdr63bdqycGdZ3CTa5rprKyJ+ZpJaavJyIIbRyspD03OWVWcPgJkHh3/yrbjX8pDRtkkHopzpL3X2bXJ1I5ZlD4DZhbWfOuff24hp0eTWOhDDT/pL2963tYdu7WnZA2Du6OBRifKprbWd05rggBN5dLnX+UwTet1sjwBgYXhAXPz+Zg1knWUH3IF+r33KtELL4ibRtudonA9xeG6SWmpyciDEjp+a/+eiW7GsOvsXwMZwZ7uDp53IbZYHjbZrduCpfq+z/5pXNWwBABCRue76J0TciYbNjnYrdMBJ/Phyb+qxCo+oxdYAQER2LMbdnbD+Dy/++lpMhSaSOhBCeHE4nHr9xePuQtJGtuBwAPCSyfw0YAtuW0OOiE4eXjnWub8h7RZqEwC8ZN/s0XhjjOtneAUUuk8GFodLbjj1puUvuCy+MQwArriyc93BAyJy3MAtRsImHYhR7ltZ6jyyyeWNWwYArhzZwdjZPT98puXllsZNkoaLOxDkdL/dvk0W3bD4Zs3YAQC8Yk6zR1ffHEfuWe/9Dc0YIV2W4kAILziJ71h+aPrPpezXkE0AwKsMan5x+ME4HJ0S731D5kibhRwIIy+tO8712j8qtE0DFwOAqwxtvrt+KIr7agNnSsvX6ECUeHClN/X1a1xmohwAjBnjZQiEeIKXgIm7/ioiwiiKO5Rr+DcMAQDK3Z7vDu8YhdG3+J6ANQiES15ad+b47L9ykgBggns9e2R1n0jrB87L2yYop6TuDgQ5La3Rh/rHps/WvdWq+wMAkzp8MHbm9gzvCzEseO+3T7qMujo5EC7F6BdXWu0v5/SjvnETAADXeD9nHow3+dbw/iDhoBd/3TUupzyBAxu/2+9a/nHn2l/qL7rnE7RQ2yMBwCZHc9PhOLPeGXxExN0pIrduchuWVeZAiCL+aSfx22vrne/l8A97NmMlANiMa69YM/+5uCe0Rrc7H2+TGPaJ+L0xjHZ5cTv4k+MlGDxmi40/3e1CvOR864JIOCvO/1Gi+3XHt560/sc8ynAWAJThInvgQEMdAAANHRxt40AZDgCAMlxkDxxoqAMAoKGDo20cKMMBAFCGi+yBAw11AAA0dHC0jQNlOAAAynCRPXCgoQ4AgIYOjrZxoAwHAEAZLrIHDjTUAQDQ0MHRNg6U4QAAKMNF9sCBhjoAABo6ONrGgTIcAABluMgeONBQBwBAQwdH2zhQhgMAoAwX2QMHGuoAAGjo4GgbB8pw4P8rNjOmmqNafQAAAABJRU5ErkJggg==';
			$this->vet_dados['object'] = array($objects);
		} else {
			$msg = 'Nenhum arquivo para remover.';
			$this->vet_dados['trash_img'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAQ3UlEQVR4Xu2daYhkZxWGz/fV0p3FxMl09QQFcYmOikhUYiT+iiAq+kfFiII/EnBMXMAVjU731HRHDaIGNGiMSwTBLYgILgRB/6gjSoKKoo5xRWSmq2McJ5rpWr5PehxQg7XM1L15763z5G/u/e45z3nft05VdfcE4z8IQMAtgeC2cxqHAASMAEAEEHBMgABwPHxahwABgAYg4JgAAeB4+LQOAQIADUDAMQECwPHwaR0CBAAagIBjAgSA4+HTOgQIADQAAccECADHw6d1CBAAaAACjgkQAI6HT+sQIADQAAQcEyAAHA+f1iFAAKABCDgmQAA4Hj6tQ4AAQAMQcEyAAHA8fFqHAAGABiDgmAAB4Hj4tA4BAgANQMAxAQLA8fBpHQIEQE00sOed+eLG0uglIdnVFvPlIaXH5WiPMIutareQBmb2d7P4ewvhJznbd+xU4+vbHwgnq123j+oIgIrPeW9358kxhxvTKF8TY1yueLkzlZdS+me0xhesMby5d3j53plu4qJSCBAApWCd/9B9b88XpPOH7zVLbzSLjflPrOIJaZBDvOXC0Dz0h244VcUKF70mAqCCE145eGp/iI2vmtlTKlhe4SXlZD9rNEcvPd5d/l3hh3PgRAIEQMUEsrLWf1YI+S6zuLdipZVaTrJ8vJHtBVub7Z+W+iAO/x8CBECFBLH7ym9m3wuxsVKhsh62UnZDoBnTVWwCDxty/m3Ahw/15Cc9qpvPH46GP87BnlqVmhR1jJL95JITzefc+9Gwo3i+t2eyAVRk4qvrgw9ns7dUpBxtGcFu7h1u3agtwsfTCYAKzPn0h34Wfm4xNqeWk+yeHPPtFtJ3H/nXpT9W/ZXysjflpb9dvPPYGOLVOYQDZvaMqT1aGsSYn8xbgemk5r2CAJiXYAH37z04/EyM+dpJRyVLD8Yc3tjbbN1hFnIBjxUckUNnbXBtCvnWaPG8SQUEC5/Y2mheLyjS1SMJAPG4L+nmi0LqH5tkiF3zN3J8/tZm6/vicgt5/Gp3cNVomL4dYzx/3IEppQeap9qXHv9g+EchD+WQ/0uAABALY2V9+Kpg+fOTy8jX9Tbad4hLLfTxnbX+dRbCpycfGq7pbTTvLPTBHPY/BAgAsSBWDw5vzzG/dkIZd/c2mlfUd+0f11kOnfXh3ZM+E8gh3LZ9uHmDeEQL/XgCQDzezvrgR2Z2xVib5Py67c327eIyS3n86lr/+hzCxyccfqS30bqqlIdz6GkCBIBYCCsHT/Um/eBPDqP924eXj4rLLOXxZ37k+VfjPwfIW/fd1N5XysM5lACoggY66zv9Sb/Se/H9zeWqf9V3rhx3vyI8sWc49peAkqWd+zaWFuI3IM+VUdn3sQGUTXjK+Z31wcSv9HobrYWekff+xfLjLYB6AN4N4L1/tf4W+tVFDXeW53s3gPf+Z9FImdcQAGXSneFs7wbw3v8MEin1EgKgVLzTD/duAO/9T1dIuVcQAOXynXq6dwN473+qQEq+gAAoGfC0470bwHv/0/RR9v8nAMomzNeAEwkQAFoBEgBa/ubdAN77F8uPnwNQD8C7Abz3r9YfG4B4At4N4L1/sfzYANQD8G4A7/2r9ccGIJ6AdwN4718sPzYA9QC8G8B7/2r9sQGIJ+DdAN77F8uPDUA9AO8G8N6/Wn+13wCmCUgNmOf7JlD1v+dAAPjWJ92XTIAAKBkwG0DJgDl+LgIEwFz4pt9MAExnxBU6AgRAyewJgJIBc/xcBAiAufBNv5kAmM6IK3QECICS2RMAJQPm+LkIEABz4Zt+MwEwnRFX6AgQADr2Mz15WoBUfYAzNclFYwl4n3/tfw5gXm17F8C8/Op+v/f5EwDO/2Weuht43voJgHkJ1vx+7wKo+fjmLt/7/NkA2ADmNlGdDyAA6jy9Amr3LoACENb6CO/zZwNgA6i1gectngCYl2DN7/cugJqPb+7yvc+fDYANYG4T1fkAAqDO0yugdu8CKABhrY/wPn82ADaAWht43uIJgHkJ1vx+7wKo+fjmLt/7/NkA2ADmNlGdDyAA6jy9Amr3LoACENb6CO/zZwNgA6i1gectngCYl2DN7/cugJqPb+7yvc+fDUC8AagFyPMHeVKKLPrfgyAACACpAbwH0NwrzJwHEAAEAAEwwURsAHMmTNVv9/4KRP+8Bai6R0utDwNoDeCdf6ninuFw3gLwFoC3ALwFmCEqFvQS769A9K/dgNS2YgNgA2ADYANQ55Du+bwCal8BvfPXKf/fT2YDYANgA2ADUOeQ7vneX4HoX7sB6ZTPBnCaAAbQGsA7fwJATMC7AOlfG4Bi+fMZAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBLwLkP61ASiWPxsABtAawDt/AkBMwLsA6V8bgGL5swFgAK0BvPMnAMQEvAuQ/rUBKJY/GwAG0BrAO38CQEzAuwDpXxuAYvmzAWAArQG88ycAxAS8C5D+tQEolj8bAAbQGsA7fwJATMC7AOlfG4Bi+bMBYACtAbzzJwDEBFYP7uzkGNvjyujF5pJ1Q19cJo8vgcBlb8pLJ/YMT407OlnauW9jabmER1fmyFCZSkSFdNZ3ts3i3nGPb8Vm5y/dsC0qj8eWSGDfobya8vD4uEfkNNrevmm5U2IJ8qPdB8De9wx/Exv5srGvAjk/+77N9o/lk6KAwgnsO9R/TsrhyNiDsx3tbbb2F/7gCh3oPgBW1na+FUJ84dhXgZxfu73Z/lSFZkYpBRFYXetfn0P4+PgASN/sbS69uKDHVfIY9wHQOTS4xbK9edx0Qspf2Lqp/epKTo+i5iLQWR982cxeMXb2ZrdsbbTeOtdDKn4zAbA2vMZC/tL4OaWTrdi+9C/d8M+Kz5LyzoJAp5svTMP+8Rjj+eNvC9f0Npp3nsWxtbvUfQBM+yDo3xPN1/U22nfUbroUPJbAynr/dcHCbROCP7die3XRPwB2HwC7AlhZH/wwmF058cOg482n2e1hgKfqT2D367+/7Rn9Mlh+3IRujvQ2WlfVv9vJHRAAZtZZ77/BLNw6CVXO9rbtzdaHF10QHvrrrA3ebcHeO6nXYPn1Wxvt8R8QLggoAsDMLurmS1qp/6do8YJxc02WHmxlu+LY5tIvFmT2LttY6fafGYf5yKQf/kopPTActh9z4uZw/6JDIgDOTHjatwFnLvtNY9B87rH3h96iC2MR++t086VpMDgSG+GxE7e9YB/cPtx6xyIyeGhPBMAZIiuH8qNy7h+dtAWcufTuxqD5IkKgXvbYNX8eDu8K0Z4+ufJ0MgzbT9x6Xxj7E4L16pzPAGaeV2d98E4zu3nqDdmONiy9jLcDU0lV4oJOt3/5aBC/0mjkx08ryNtnPWwA/62IA7l1yerwR41ol08Tyu5nAiHHg9uN5kesG4bTruf/Cwh0c7uThm9Jlg5Hi0tTK0h2T6/ZvNLTPAmAh6hi5dCpJ+VRuDvGeOFUwexekO2oWX7/0gOtL/35lvDgTPdwUakE9r09XzA6b/CqHOK7ouUnzPSwlP4eLD9z66bl3850/YJcRAD8n0Gudocvy8PRnRZjnHnOpwUUvpGCfSdH+1mr3/r9sSU7wa8Sz0zwnC7c/U7/gQvtotwcPH4U7Okh2PPSKL9k5gA//dQ0itZ4+fGN5tfOqYga30QAjBne6nr/hmzhYzWeLaXPSCBbPrC90f7kjJcv1GUEwIRxng6BlG89q01goeSx6M2kUbZwg1fz706XAJii8dX14ctHafTZs1spF904i9BfOhmt8RqPa/9/T48AmEHLKwdP7TdrfHn6d8gzHMYlegLJ7rHG6JW9w8v36ovRVkAAzMr/QG519g3flnJam/wrpLMeyHUPP4F0MufY5avb/5AnAM5ShXvfkx8dG8N3JEsHosXzzvJ2LhcQ2P3Z/tCIt4XQ/FCvG44JSqjsIwmAcxzNo2/Me/utwavNwmvM7IpzPIbbSiOQsln8YbD8uZ1+64sefrHnXFASAOdC7SH3rL4770uN0dUh5istp/1m8bKcRnuihYsm/dZZAY92f8Tun+4OKZ8MsXG/WbrXQvy15fCDVmx8d9H/mEcRwycAiqDIGRCoKQECoKaDo2wIFEGAACiCImdAoKYECICaDo6yIVAEAQKgCIqcAYGaEiAAajo4yoZAEQQIgCIocgYEakqAAKjp4CgbAkUQIACKoMgZEKgpAQKgpoOjbAgUQYAAKIIiZ0CgpgQIgJoOjrIhUAQBAqAIipwBgZoSIABqOjjKhkARBAiAIihyBgRqSoAAqOngKBsCRRD4F1ytO9PeXIQ7AAAAAElFTkSuQmCC';
			$this->vet_dados['msg'] = $msg;
			$this->vet_dados['object'] = array($objects);
		}

		foreach ($objects as $object) {
			$log             = new ObjectLog();
			$log->log_action = 'clean_trash';
			$log->user_key   = $this->session->userdata('USER_KEY');
			$log->hash       = $object->hash;
			$log->datetime   = date('Y-m-d h:i:s');
			$this->generate_log($log);
			unset($log);
		}

		$this->load->vars('server_id', $server);
		return $this->parser->parse('arquivos/empty_trash_details', $this->vet_dados, TRUE);
	}

	public function empty_trash()
	{
		$obj = null;
		$server  = $this->input->get('server');

		if ($server == SERVER_LOCAL && $this->session->userdata('SRID') == SERVER_LOCAL)
			$server = $this->session->userdata('SRID');

		$objects = $this->get_files_in_trash($server);

		$return  = '';
		for($i=0;$i<count($objects);$i++){
			if($objects[$i]->type == 2)
				continue;
			$return = $this->removeFileFromFilesystem($objects[$i]);
			// if(GedFileSystem::$UNABLE_REMOVE_FILE == $return){
			if('UNABLE_REMOVE_FILE' == $return){
				return $return;
			}
		}
		return $return;
	}

	public function delete_object(ObjectEntity $entity)
	{
		$this->db->where('hash', $entity->hash);
		return $this->db->delete('file_object');
	}

	public function get_files_in_trash($server = null)
	{
		$this->db->from('file_object');
		$this->db->where('in_trash', 1);
		$server = $server ? $server : null;
		$this->db->where('server_id', $server);

		$this->db->where('storage_key', $this->storage);
		$this->db->where('owner', $this->session->userdata('USER_KEY'));

		return $this->db->get()->result();

	}

	public function sendtotrash()
	{
		$entity = new ObjectEntity();
		$entity->hash = $this->uri->segment(3);

		$object = $this->getObject($entity, true);

		$this->load->library('LibFactoryServer');
		return $this->libfactoryserver->sendToTrash($object);

		// if(count($object) > 0){
		// 	$entity->in_trash = true;
		// 	if($this->sendObjectToTrash($entity)){

		// 		$log = new ObjectLog();
		// 		$log->log_action = 'delete_file';
		// 		$log->user_key = $this->session->userdata('USER_KEY');
		// 		$log->hash = $entity->hash;
		// 		$log->datetime = date('Y-m-d h:i:s');

		// 		$this->generate_log($log);

		// 		return GedFileSystem::$FILE_REMOVED;
		// 	}
		// 	else
		// 		return GedFileSystem::$UNABLE_REMOVE_FILE;

		// } else {
		// 	return GedFileSystem::$ERROR_NOT_EXISTS;
		// }

	}

	public function send()
	{
		$uploaded = $_FILES['file'];
		$folder = $this->input->post('folder') != ''|| $this->input->post('folder') != 'Home' ? $this->input->post('folder') : '';
		$server = $this->input->get('server');

		if ($uploaded['error'] != 0)
			return array('error' => 'Erro ao realizar o upload do arquivo.');

		$drive = $this->session->userdata('STR_DRIVER');
		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setServer($server);

		$file_send = array(
			'original_name' => $uploaded['name'],
			'size'          => $uploaded['size'],
			'in_trash'      => false,
			'type'          => 1,
			'owner'         => $this->session->userdata('USER_KEY'),
			'storage_key'   => $this->session->userdata('STR_DRIVER'),
			'tmp'           => $uploaded['tmp_name']
		);

		$content_file = file_get_contents($file_send['tmp']);
		$file = $this->libfactoryserver->createFileObject($file_send, $folder);

		if ($this->file_exists($file->original_name, $file->parent, $server))
			return array('error' => "Já existe um arquivo chamado {$file->original_name} em {$file->full_path}.");

		if (!$server)
			$server = $this->session->userdata('SRID');

		$this->libfactoryserver->setTypeServer($server, $drive);
		$respose = $this->libfactoryserver->uploadFile($folder, $file, $uploaded);

		if ($respose['status'] == false)
			return $respose;

		$stored = $this->storeObject($file);

		if ($uploaded['type'] == 'text/xml') 
			$this->buildScheduleFileToBeProcessed($file, $content_file, $server);

		$log = new ObjectLog();
		$log->log_action = $respose['action'];
		$log->user_key   = $this->session->userdata('USER_KEY');
		$log->hash       = $stored->hash;
		$log->datetime   = $stored->created;

		$this->generate_log($log);

		if ($stored != false)
			return $respose;

		return false;
	}

	public function storeObject(ObjectEntity $entity)
	{
		$this->db->set('hash', $entity->hash);
		$this->db->set('original_name', $entity->original_name);
		$this->db->set('full_path', $entity->full_path);
		$this->db->set('type', $entity->type);
		$this->db->set('mime', $entity->mime);
		$this->db->set('size', $entity->size);
		$this->db->set('extension', $entity->extension);
		$this->db->set('owner', $entity->owner);
		$this->db->set('storage_key', $entity->storage_key);
		$this->db->set('created', $entity->created);
		$this->db->set('parent', $entity->parent);
		$this->db->set('in_trash', $entity->in_trash);
		$this->db->set('is_image', $entity->is_image);
		$this->db->set('server_id', $entity->server_id);

		$this->db->insert('file_object');

		$entity->id = (int)$this->db->insert_id();

		if ($entity->id > 0) {
			return $entity;
		} else {
			return false;
		}
	}

	public function getObjectMime($mime, $extension= null)
	{
		$this->db->select('id_file_type');
		$this->db->from('file_type');
		$this->db->where('mime_file_type', $mime);
		if($extension != null)
		$this->db->or_where('ext_file_type like "%'.$extension.'%"');

		return $this->db->get()->row();
	}

	// public function getObjectExtension($basePath)
	// {
	// 	return pathinfo($basePath, PATHINFO_EXTENSION);
	// }

	public function loadFileSystem($serverId, $strDriver)
	{
		$serverEntity = new ServerEntity();
		$serverEntity->server_id = $serverId;
		$server = $this->get_server($serverEntity);

		switch (@$server->server_type) {

			case 1:
				$storage = $strDriver;
				$str_key = $this->session->userdata('STR_KEY');
				$home = $server->server_path;
				$this->load->library('GedLocalFileSystem');
				$this->filesystem = new GedLocalFileSystem($home, $storage, $str_key);
				class_alias('GedLocalFileSystem', 'GedFileSystem');
				break;
			case 2:

				$home = $strDriver;
				$this->load->library('GedS3FileSystem');
				$this->filesystem = new GedS3FileSystem($home, $server->server_key, $server->server_secret);
				class_alias('GedS3FileSystem', 'GedFileSystem');

				break;
			case 3:
				break;

		};
	}

	public function getObjectList(ObjectEntity $entity, $search = false)
	{
		$conditionsTags    = array();
		$conditionsDefault = array();
		$conditions[]      = "WHERE storage_key = '{$this->storage}'";

		// $this->db->where('file_object.parent', $entity->parent);
		if ($entity->parent != NULL && $entity->original_name == null)
			$conditions[] = "AND file_object.parent = {$entity->parent}";
		else if ($entity->parent == NULL && $entity->in_trash == null && $entity->original_name == null)
			$conditions[] = 'AND file_object.parent IS NULL';

		if (!empty($entity->in_trash))
			$conditions[] = 'AND file_object.in_trash = 1';
		else
			$conditions[] = 'AND file_object.in_trash = 0';

		if ($this->session->userdata('USER_ROLE') != 'CLIADMIN')
			$conditions[] = "AND file_object.owner = '{$this->session->userdata('USER_KEY')}'";

		if ($entity->is_image == true)
			$conditions[] = 'AND file_object.is_image = true';

		if ($entity->original_name != null) {
			$conditionsDefault[] = 'AND file_object.original_name LIKE "%'.$entity->original_name.'%"';
			$conditionsTags[]    = "AND info_labels.label_content LIKE '%{$entity->original_name}%'";
			$conditionsTags[]    = "OR info_labels.label_description LIKE '%{$entity->original_name}%'";
		}

		if (!is_null($entity->server_id) && !$search)
			$conditions[] = "AND file_object.server_id = {$entity->server_id}";
		if (is_null($entity->server_id) && !$search)
			$conditions[] = "AND file_object.server_id IS NULL";

		$conditionsTags      = array_merge($conditions, $conditionsTags);
		$conditionsDefault   = array_merge($conditions, $conditionsDefault);
		$conditionPermission = $conditionsDefault;
		//AND file_object.owner = '{$this->session->userdata('USER_KEY')}

		$conditionsDefault   = implode(' ', $conditionsDefault);
		$conditionsTags      = implode(' ', $conditionsTags);
		$conditionPermission = implode(' ', $conditionPermission);

		$conditionPermission = str_replace('file_object.owner', 'usuario.user_key', $conditionPermission);

		$query = "
				SELECT *
				FROM
				(
					(
						SELECT DISTINCT file_object.id as file_object_id, file_object.*, file_type.*
						FROM file_object
						JOIN file_type ON file_type.id_file_type = file_object.mime
						{$conditionsDefault}
					)
					UNION
					(
						SELECT DISTINCT file_object.id as file_object_id, file_object.*, file_type.*
						FROM file_object
						JOIN file_type ON file_type.id_file_type = file_object.mime
						JOIN info_labels ON info_labels.object = file_object.hash
						{$conditionsTags}
					)
					UNION
					(
						SELECT DISTINCT file_object.id as file_object_id, file_object.*, file_type.*
						FROM file_object
						JOIN file_type ON file_type.id_file_type = file_object.mime
						JOIN group_permission ON group_permission.object_id = file_object.id
						JOIN group_users ON group_users.group_id = group_permission.group_id
						JOIN usuario ON usuario.user_id = group_users.user_id
						{$conditionPermission}
						AND group_permission.listable = 1
					)
				) AS temp_table
				ORDER BY temp_table.type DESC, temp_table.original_name ASC;";

		return $this->db->query($query)->result();
	}

	public function getImageList(ObjectEntity $entity = null)
	{
		$this->db->from('file_object');
		$this->db->join('file_type', 'file_type.id_file_type = file_object.mime');
		$this->db->where('storage_key', $this->storage);
		$this->db->where('in_trash', 0);
		$server = ($entity->server_id) ? $entity->server_id : null;
		$this->db->where('server_id', $server);

		if($this->session->userdata('USER_ROLE') != 'CLIADMIN')
			$this->db->where('owner', $this->session->userdata('USER_KEY'));

		$this->db->where('is_image', true);
		$this->db->order_by("type", 'desc');
		$this->db->order_by("original_name", 'asc');

		return $this->db->get()->result();
	}

	public function getObject(ObjectEntity $entity, $details = false)
	{
		if ($details == true) {
			$this->db->select('source.*, file_type.*, usuario.user_id as uid, usuario.user_fullname as uname, parent.hash as parent_id');
			$this->db->join('file_object as parent', 'source.parent = parent.id', 'left');
		}
		$this->db->from('file_object as source');
		$this->db->join('file_type', 'file_type.id_file_type = source.mime');

		$this->db->where('source.hash', $entity->hash);

		if ($details == true) {
			$this->db->join('usuario', 'usuario.user_key = source.owner');
		}

		return $this->db->get()->row();
	}

	public function get_parent($hash)
	{
		$this->db->select('full_path, id');
		$this->db->from('file_object');
		$this->db->where('hash', $hash);

		$row = $this->db->get()->row();

		if (count($row) > 0)
			return $row;
		else
			return false;
	}

	public function get_server(ServerEntity $entity, $default = false)
	{
		$this->db->from('servidor');
		$serverId =$this->session->userdata('SRID');
		if ($entity->server_id) {
			$serverId = $entity->server_id;
			$this->db->where('empresa_id', $this->session->userdata('EID'));
		}

		$this->db->where('server_id', $serverId);

		if (!$entity->server_id && $default)
			$this->db->or_where('server_is_default', 1);

		return $this->db->get()->row();
	}

	public function get_servers()
	{
		$this->db->from('servidor');
		$this->db->where('empresa_id', $this->session->userdata('EID'));
		$this->db->where('server_type', SERVER_FTP);
		$this->db->where('enable', SERVER_ATIVO);
		$this->db->order_by('server_id ASC');
		return $this->db->get()->result_array();
	}

	public function dir_exists($dir_name, $parent=null)
	{
		$this->db->select('id');
		$this->db->from('file_object');

		if($parent != null)
			$this->db->where('original_name', $dir_name);
		else
			$this->db->where('hash', $dir_name);

		$this->db->where('type', 2);

		if($parent != null)
		$this->db->where('parent', $parent);

		$row = $this->db->get()->row();

		if (count($row) > 0)
			return $row;
		else
			return false;
	}

	public function file_exists($dir_name, $parent, $server_id = null)
	{
		$this->db->select('id');
		$this->db->from('file_object');
		$this->db->where('original_name', $dir_name);
		$this->db->where('type', 1);
		$this->db->where('parent', $parent);
		$this->db->where('owner', $this->session->userdata('USER_KEY'));
		$this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));

		if (!empty($server_id) | !is_null($server_id))
			$this->db->where('server_id', $server_id);

		$row = $this->db->get()->row();

		if (count($row) > 0){
			return $row;
		}
		else
			return false;
	}

	public function file_log(ObjectEntity $entity)
	{
		$this->db->select('file_log.*, usuario.user_fullname as username');
		$this->db->from('file_log');
		$this->db->join('usuario', 'usuario.user_key = file_log.user_key');
		$this->db->join('file_object', 'file_object.hash = file_log.hash');
		$this->db->where('file_log.hash', $entity->hash);

		return $this->db->get()->result();
	}

	public function generate_log(ObjectLog $entity)
	{
		$this->db->set('log_action', $entity->log_action);
		$this->db->set('hash', $entity->hash);
		$this->db->set('user_key', $entity->user_key);
		$this->db->set('datetime', $entity->datetime);
		$this->db->set('text', $entity->text);

		$this->db->insert('file_log');

		if ($this->db->insert_id() > 0)
			return true;
		else
			return false;

	}

	public function get_parents(ObjectEntity $entity)
	{
		$html = "";
		$query = $this->db->get_where('file_object', array('id' => $entity->parent, 'type' => 2));

		foreach ($query->result() as $row) {
			$entity->parent = $row->parent;
			$url = base_url() . 'arquivos/folder/' . $row->hash;
			if (!is_null($row->server_id))
				$url .= "/?server={$row->server_id}";

			$html .= $this->get_parents($entity);
			$html .= '<div class="bcit">';
			$html .= '<a href="' . $url . '">';
			$html .= '<i class="fa fa-folder"></i> ' . $row->original_name;
			$html .= "</a>";
			$html .= "</div>";
		}

		$html .= "";
		return $html;

	}

	public function sendObjectToTrash($entity)
	{
		$this->db->set('in_trash',$entity->in_trash);
		$this->db->where('hash', $entity->hash);

		if($this->db->update('file_object'))
			return true;
		else
			return false;
	}

	public function sendAllObjectToTrash($arrObjectId)
	{
		$this->db->set('in_trash', true);
		$this->db->where_in('id', $arrObjectId);

		if ($this->db->update('file_object'))
			return true;
		return false;
	}

	public function download_link(DownloadEntity $entity)
	{
		$this->db->set('hash', $entity->hash);
		$this->db->set('user_key', $entity->user_key);
		$this->db->set('generated', $entity->generated);
		$this->db->set('expires', $entity->expires);
		$this->db->set('downloads', $entity->downloads);
		$this->db->set('key', $entity->key);
		$this->db->set('downloaded', $entity->downloaded);
		$this->db->set('ip_address', $entity->ip_address);
		$this->db->set('single', $entity->single);

		$this->db->insert('download_link');

		$entity->link_id = (int) $this->db->insert_id();

		if ($entity->link_id > 0){
			$log = new ObjectLog();
			$log->log_action = 'generate_download_link';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $entity->hash;
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);

			return $entity;
		}

		return false;
	}

	public function has_link(DownloadEntity $entity)
	{
		$this->db->from('download_link');

		if($entity->hash != '')
			$this->db->where('hash', $entity->hash);

		if($entity->expires != '')
			$this->db->where('expires', $entity->expires);

		if($entity->key != '')
			$this->db->where('key', $entity->key);

		if($entity->downloaded != '')
			$this->db->where('downloaded', $entity->downloaded);

		if($entity->downloads != '')
			$this->db->where('downloads', $entity->downloads);

		if($entity->single != '')
			$this->db->where('single', $entity->single);

		$row = $this->db->get()->row();

		if (count($row) > 0)
			return $row;
		return false;
	}

	public function get_link(DownloadEntity $entity)
	{
		$this->db->from('download_link');
		$this->db->where('link_id', $entity->link_id);
		return $this->db->get()->row();

	}

	public function use_link(DownloadEntity $entity)
	{
		$link = $this->get_link($entity);
		$this->db->set('downloads',  1);
		$this->db->set('downloaded', $entity->downloaded);
		$this->db->set('ip_address', $entity->ip_address);
		$this->db->set('user_agent', $entity->user_agent);

		$this->db->where('link_id', $entity->link_id);

		if($this->db->update('download_link')){

			$log = new ObjectLog();
			$log->log_action = 'download';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $link->hash;
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);
			return true;
		}
		else
			return false;
	}

	public function all_users()
	{
		$this->db->from('usuario');
		$this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id');

		$this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));

		return $this->db->get()->result();

	}

	public function all_groups()
	{
		$this->db->from('group');
		$this->db->where('group.empresa_id', $this->session->userdata('EID'));

		return $this->db->get()->result();
	}

	public function get_share(ShareEntity $entity)
	{
		$this->db->from('share');
		$this->db->join('usuario', 'usuario.user_key = share.user', 'LEFT');
		$this->db->join('group', 'group.group_id = share.group', 'LEFT');
		$this->db->where('object', $entity->object);

		return $this->db->get()->result();
	}

	public function update_description(ObjectEntity $entity)
	{
		$this->db->set('description', $entity->description);
		$this->db->where('hash', $entity->hash);

		if($this->db->update("file_object")){
			$log = new ObjectLog();
			$log->log_action = 'updated_description';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $entity->object;
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);
			return $entity;
		}
		else
			return false;

	}

	protected function get_quota()
	{
		if($this->session->userdata('QDS') == 0)
			return $this->get_quota_size();

		$this->db->select('quota_size');
		$this->db->from('usuario_quota');
		$this->db->where('user_id', $this->session->userdata('USER_ID'));

		$quota = $this->db->get()->row();
		return $quota->quota_size;

	}

	public function get_quota_size()
	{
		if ($this->session->userdata('USER_ROLE') != '' && $plano = $this->get_plan()) {
			$quota = $this->get_services($plano->plan_id);
			$adittional = $this->get_aditional();
			return ($quota->total_storage + $adittional->adittional_space);
		}

	}

	public function get_services($plan)
	{
		$this->db->select('SUM(servico.service_storage) as total_storage');
		$this->db->from('servico');
		$this->db->join('plano_servico', 'plano_servico.service_id = servico.service_id');
		$this->db->where('servico.service_storage <> -1');
		$this->db->where('plano_servico.plan_id', $plan);

		return $this->db->get()->row();
	}

	public function get_plan()
	{
		$this->db->select('plano.plan_id');
		$this->db->from('cliente');
		$this->db->join('plano', 'plano.plan_id = cliente.client_plan_id');
		$this->db->where('client_id', $this->session->userdata('CLID'));
		return $this->db->get()->row();
	}

	public function get_usage()
	{
		$this->db->select('SUM(size) as disk_usage');
		$this->db->from('file_object');

		if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
			$this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
		} else {
			$this->db->where('owner', $this->session->userdata('USER_KEY'));
		}

		return $this->db->get()->row();
	}

	public function get_aditional()
	{
		$this->db->select('SUM(quantidade_armazenamento.quant_valor) as adittional_space');
		$this->db->from('espaco_adicional');
		$this->db->join('quantidade_armazenamento', 'quantidade_armazenamento.quant_id = espaco_adicional.quant_id');
		$this->db->where('client_id', $this->session->userdata('CLID'));

		return $this->db->get()->row();
	}

	public function set_view()
	{

		$value = $this->session->userdata('filemanager_view') == " list " ? 0 : 1;
		$this->db->set('filemanager_view', $value);
		$this->db->where('user_id', $this->session->userdata('USER_ID'));
		$this->db->update('configuration');

	}

	public function get_compartilhamentos($server = null)
	{
		$my_groups = $this->get_my_groups();
		$groups_belong = $this->get_groups_belong();
		$groups = [];

		foreach($groups_belong as $group)
			$groups[] = $group->group_id;

		foreach($my_groups as $group)
			$groups[] = $group->group_id;

		$this->db->from('share');
		$this->db->join('file_object', 'file_object.hash = share.object');
		$this->db->join('file_type', 'file_type.id_file_type = file_object.mime');
		$this->db->where('user', $this->session->userdata('USER_KEY'));
		$server = ($server) ? $server : null;
		$this->db->where('file_object.server_id', $server);
		if (count($groups) > 0)
		$this->db->or_where_in('group', $groups);
		$this->db->where_not_in('file_object.owner', $this->session->userdata('USER_KEY'));
		$this->db->group_by('file_object.hash');

		return $this->db->get()->result();
	}

	public function get_my_groups()
	{
		$this->db->select('group_id');
		$this->db->from('group');
		$this->db->where('group_owner', $this->session->userdata('USER_ID'));

		return $this->db->get()->result();
	}

	public function get_groups_belong()
	{
		$this->db->select('group_id');
		$this->db->from('group_users');
		$this->db->where('user_id', $this->session->userdata('USER_ID'));

		return $this->db->get()->result();
	}

	public function compartilhamentos()
	{
		$server = $this->input->get('server');

		if (!$server && $this->session->userdata('SRID') == SERVER_LOCAL)
			$server = SERVER_LOCAL;

		$arquivos = $this->get_compartilhamentos($server);

			if (count($arquivos) == 0) {
					$this->vet_dados['empty'] = $this->parser->parse('arquivos/empty_directory2_view', $this->vet_dados, TRUE);
			} eLse {
				$this->vet_dados['empty'] = NULL;
			}

			for ($i = 0; $i < count($arquivos); $i++) {

				$arquivos[$i]->size = By2Mb($arquivos[$i]->size);
				$arquivos[$i]->created = data_formatada($arquivos[$i]->created, 3);

				if ($arquivos[$i]->type == 1) {
					$arquivos[$i]->file_url = base_url() . 'arquivos/view/' . $arquivos[$i]->hash;

					if ($arquivos[$i]->is_image == 1) {
						$arquivos[$i]->ico_file_type = base_url() . 'thumb/show/150/150/' . $arquivos[$i]->hash;
					}

				} else {
					$arquivos[$i]->file_url = base_url() . 'arquivos/folder/' . $arquivos[$i]->hash;
				}
			}

			$share = $this->get_shared($server);

			if (count($share) == 0) {
				$this->vet_dados['empty_shared'] = $this->parser->parse('arquivos/empty_directory2_view', $this->vet_dados, TRUE);
			} eLse {
				$this->vet_dados['empty_shared'] = NULL;
			}

			for ($i = 0; $i < count($share); $i++) {

				$share[$i]->size = By2Mb($share[$i]->size);
				$share[$i]->created = data_formatada($share[$i]->created, 3);

				if ($share[$i]->type == 1) {
					$share[$i]->file_url = base_url() . 'arquivos/view/' . $share[$i]->hash;

					if ($share[$i]->is_image == 1) {
						$share[$i]->ico_file_type = base_url() . 'thumb/show/150/150/' . $share[$i]->hash;
					}

				} else {
					$share[$i]->file_url = base_url() . 'arquivos/folder/' . $share[$i]->hash;
				}
			}

		$this->vet_dados['fm_class'] = $this->session->userdata('filemanager_view');
		$this->vet_dados['fm_ico_class'] = $this->session->userdata('filemanager_ico');

		$this->vet_dados['breads'] = null;
		$this->vet_dados['arquivos'] = $arquivos;
		$this->vet_dados['my_shared'] = $share;

		$this->load->vars('server_id', $server);
		return $this->parser->parse('arquivos/share_file_manager', $this->vet_dados, TRUE);
	}

	public function get_shared($server = null)
	{
		$this->db->from('share');
		$this->db->join('file_object', 'share.object = file_object.hash');
		$this->db->join('file_type', 'file_type.id_file_type = file_object.mime');
		$this->db->where('file_object.owner', $this->session->userdata('USER_KEY'));
		$server = ($server) ? $server : null;
		$this->db->where('file_object.server_id', $server);
		$this->db->group_by('file_object.hash');

		return $this->db->get()->result();
	}

	public function get_labels(ObjectEntity $entity)
	{
		$this->db->from('info_labels');
		$this->db->where('object', $entity->hash);
		return $this->db->get()->result();
	}

	public function add_tag($dataTag = null)
	{
		if($dataTag["idTag"]){
			$this->db->set('label_description', $dataTag["label_description"]);
			$this->db->set('label_content', $dataTag["label_content"]);
			$this->db->set('object', $dataTag["hash"]);
			$this->db->where('label_id', $dataTag["idTag"]);
			$this->db->update('info_labels');
			
			return true;
		}

		$this->db->set('label_description', $this->input->post('label_description'));
		$this->db->set('label_content', $this->input->post('label_content'));
		$this->db->set('object', $this->input->post('hash'));

		$this->db->insert('info_labels');

		$id = (int) $this->db->insert_id();

		if($id > 0){
			$log = new ObjectLog();
			$log->log_action = 'add_tag';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $this->input->post('hash');
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);
			return true;
		}
		else
			return false;
	}

	public function remove_tag($id)
	{
		$tag = $this->get_tag($id);
		if(count($tag) == 0)
			return false;
		else{

			$log = new ObjectLog();
			$log->log_action = 'remove_tag';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $tag->object;
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);

			$this->db->where('label_id', $tag->label_id);
			$this->db->delete('info_labels');
			return $tag->object;
		}
	}

	public function get_tag($id)
	{
		$this->db->from('info_labels');
		$this->db->where('label_id', $id);

		return $this->db->get()->row();
	}


	public function share_email(ShareEntity $object)
	{
		$this->load->library('Mailer');
		$object->hash = urlencode(urlencode($this->encrypt->encode($object->object.'>'.$object->email)));
		$this->vet_dados['object'] = array($object);

		Mailer::$message = $this->parser->parse('template/public/shared_file_email_view', $this->vet_dados, TRUE);
		Mailer::$subject = "[GED] Compartilhamento de Arquivo.";
		Mailer::$to = $object->email;
		$m = Mailer::send_mail();

		if($m == true){
			syslog::generate_log("SHARE_EMAIL_SUCCESS");
			return true;
		}
		else
		{
			syslog::generate_log("SHARE_EMAIL_ERROR");
			return false;
		}
	}

	protected function buildScheduleFileToBeProcessed($entity, $fileContent, $server)
	{
		$arrFile = array(
			'hash'        => $entity->hash,
			'user_key'    => $this->session->userdata('USER_KEY'),
			'storage_key' => $this->session->userdata('STR_DRIVER'),
			'content'     => $fileContent,
			'server_id'   => $server,
		);

		$this->scheduleFileToBeProcessed($arrFile);
	}

	// protected function scheduleFileToBeProcessed($entity, $fileContent)
	public function scheduleFileToBeProcessed($file)
	{
		// $this->db->set('hash', $entity->hash);
		// $this->db->set('created_at', (new DateTime())->format('Y-m-d h:i:s'));
		// $this->db->set('user_key',$this->session->userdata('USER_KEY'));
		// $this->db->set('storage_key',$this->session->userdata('STR_DRIVER'));
		// $this->db->set('processed', false);
		// $this->db->set('content', $fileContent);
		// $this->db->set('server_id', $this->session->userdata('SRID'));

		foreach ($file as $key => $value)
			$this->db->set($key, $value);

		$this->db->set('created_at', (new DateTime())->format('Y-m-d h:i:s'));
		$this->db->insert('xml_file');
		return $this->db->insert_id();
	}

	protected function unscheduleFileProcessing($hash)
	{
		$this->db->where('hash', $hash);
		return $this->db->delete('xml_file');
	}

	public function processScheduleFiles()
	{
		$this->db->select('*');
		$this->db->from('xml_file');
		$this->db->where('processed', 0);
		$filesToBeProcessed = $this->db->get()->result();

		foreach ($filesToBeProcessed as $fileToBeProcessed) {

			try {
				if(null !== $fileToBeProcessed->content){
					$this->loadFileSystem(
						$fileToBeProcessed->server_id,
						$fileToBeProcessed->storage_key);
					$import = $this->labels_import_xml($fileToBeProcessed, $fileToBeProcessed->user_key);
					if(false === $import){
						throw new InvalidXMlFileException('O XML não está no formato esperado');
					}

					$object = new ObjectEntity();
					$object->hash = $fileToBeProcessed->hash;
					$file = $this->fetch($object)[0];

					$this->removeFileFromFilesystem($file);
					$this->markScheduledFileAsProcessed($fileToBeProcessed);
				}
			}catch(InvalidXMlFileException $e){
				$this->unscheduleFileProcessing($fileToBeProcessed->hash);
			}catch(\Exception $e){}

		}
	}

	private function labels_import_xml($fileToBeProcessed, $argUserKey)
	{
		$doc = new DOMDocument();
		$doc->loadXML($fileToBeProcessed->content);

		$employees = $doc->getElementsByTagName("param");

		$directory = null;
		foreach ($employees as $employee) {
			if ($employee->getAttribute('name') == 'image_fname') {
				$imageName = trim($employee->nodeValue);

				$fileFormat = $doc->getElementsByTagName("fileFormat");

				// if ($fileFormat->length > 0){
				// 	$fileFormat = $fileFormat->item(0)->nodeValue;
				// 	$imageName .= '.' . $fileFormat;
				// }

				$data = $this->get_file_info(
					$imageName,
					$fileToBeProcessed->user_key,
					$fileToBeProcessed->storage_key,
					$fileToBeProcessed->server_id);
			}

			if ($employee->getAttribute('name') == 'ouptut_dir') {
				$object = new ObjectEntity();
				$object->full_path = trim($employee->nodeValue);
				$object->server_id = $fileToBeProcessed->server_id;
				$directory = ($result = $this->fetch($object)) ? $result[0] : false;

			}

		}

		if (!isset($data->hash))
			return false;

		//Realiza a troca de diretórios de acordo com o parametro output_dir
		$this->move_uploaded_file($data, $directory);

		$labelsAndValues = $this->extractTagsFromXml($doc, $argUserKey);
		foreach ($labelsAndValues as $key => $value) {

			$label = $this->get_labels_id(array('object'=>$data->hash, 'label_description'=>trim($key)));
			if($label){
				$this->update_labels(array("label_content"=>trim($value)),array('object'=>$data->hash, 'label_description'=>trim($key)));
				continue;
			}

			$this->add_labels(array(
					"label_description"=>trim($key),
					"label_content"=>trim($value),
					"object"=>$data->hash)
			);
		}
	}

	private function extractTagsFromXml(\DOMDocument $doc, $argUserKey){
		$labelsAndValues = [];
		$fields = $doc->getElementsByTagName('fields');
		if($fields->length == 0){
			return $labelsAndValues;
		}

		$fields = $fields->item(0);

		foreach($fields->childNodes as $childNodes){
			if ($childNodes->nodeName == 'item') {
				$value = $childNodes->nodeValue;
				$label = trim($childNodes->getAttribute('id'));
				$arrResult = $this->getXMLTag($argUserKey, XML_SCANNER1);
				$arrTags = isset($arrResult[0]) ? explode(';', $arrResult[0]['tag']) : array();

				if ($arrTags && array_search($label, $arrTags) !== false)
					$labelsAndValues[$label] = trim($value);

				if (empty($arrResult))
					$labelsAndValues[$label] = trim($value);

			}

			if ($childNodes->nodeName == 'number') {
				$value = '';
				$valueNodes = $childNodes->childNodes;
				if ($valueNodes->length > 0) {
					foreach($valueNodes as $valueNode){
						$tmpValue = trim($valueNode->nodeValue);
						if($tmpValue != ''){
							$value = $tmpValue;
						}
					}
				}
				$label = trim($childNodes->getAttribute('id'));
				$arrResult = $this->getXMLTag($argUserKey, XML_SCANNER2);
				$arrTags = isset($arrResult[0]) ? explode(';', $arrResult[0]['tag']) : array();

				if ($arrTags && array_search($label, $arrTags) !== false)
					$labelsAndValues[$label] = trim($value);

				if (empty($arrResult))
					$labelsAndValues[$label] = trim($value);
			}
		}

		return $labelsAndValues;
	}

	protected function move_uploaded_file($file, $directory)
	{
		if (!$directory)
			return false;

		if ($file->full_path == $directory->full_path."/{$file->original_name}")
			return false;

		$file_xml = new ObjectEntity();
		$file_xml->full_path = preg_replace('/\..*/', '.xml', $file->full_path);
		$xml = $this->fetch($file_xml)[0];

		$file_xml->hash = $xml->hash;
		$file_xml->original_name = $xml->original_name;
		$file_xml->full_path = $xml->full_path;

		$objects = array($file, $file_xml);
		foreach ($objects as $object) {

			$this->db->set('full_path', $directory->full_path."/{$object->original_name}");
			$this->db->set('parent', $directory->id);
			$this->db->where('hash', $object->hash);
			$this->db->update('file_object');
		}

		$server = new ServerEntity();
		$server->server_id = $directory->server_id;
		$server = $this->servidor_model->fetch($server);

		if (SERVER_FTP != $server->server_type)
			return true;


		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($directory->server_id);

		foreach ($objects as $object)
			$this->libfactoryserver->moveFile($object->full_path, $directory->full_path."/{$object->original_name}");

		return true;
	}

	public function get_labels_id($argWhere)
	{
		$this->db->from('info_labels');
		$this->db->where($argWhere);
		return $this->db->get()->result();
	}

	public function get_file_info($dir_name, $userKey, $storageKey, $server)
	{
		$this->db->select('hash, full_path, original_name');
		$this->db->from('file_object');
		$this->db->where('original_name', $dir_name);
		$this->db->where('type', 1);
		$this->db->where('owner', $userKey);
		$this->db->where('storage_key', $storageKey);
		$this->db->where('server_id', $server);
		$row = $this->db->get()->row();

		if (count($row) > 0)
			return $row;

		return false;
	}

	public function update_labels($argData, $argWhere)
	{
		$this->db->where($argWhere);

		if($this->db->update('info_labels', $argData)){
			$log = new ObjectLog();
			$log->log_action = 'updated_description';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $argWhere['object'];
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);
			return true;
		}

		return false;
	}

	public function add_labels($argLabes)
	{
		$this->db->insert('info_labels',$argLabes);

		$id = (int) $this->db->insert_id();

		if($id > 0){
			$log = new ObjectLog();
			$log->log_action = 'add_tag';
			$log->user_key = $this->session->userdata('USER_KEY');
			$log->hash = $argLabes['object'];
			$log->datetime = date('Y-m-d h:i:s');

			$this->generate_log($log);
			return true;
		}
		else
			return false;
	}

	protected function markScheduledFileAsProcessed($fileToBeProcessed){
		$this->db->set('processed', true);
		$this->db->where('id', $fileToBeProcessed->id);
		$this->db->update('xml_file');
	}

	protected function removeFileFromFilesystem($object)
	{
		$this->load->library('LibFactoryServer');
		$this->libfactoryserver->setTypeServer($object->server_id, $this->storage);
		return $this->libfactoryserver->cleanTrash($object);
	}

	private function getXMLTag($argUserKey, $argXmlType)
	{
		$this->db->select('usuario_xml_tag.tag');
		$this->db->from('usuario_xml_tag');
		$this->db->where('xml_type', $argXmlType);
		$this->db->where('usuario_xml_tag.user_key', $argUserKey);
		return $this->db->get()->result_array();
	}

	public function getChildrenDir($file_id = null, $server_id = null)
	{
		if (!$this->session->userdata('STR_DRIVER'))
			return false;

		if (!$server_id && $this->session->userdata('SRID') == SERVER_LOCAL)
			$server_id = SERVER_LOCAL;

		$server = ($server_id) ? $server_id : null;
		// $this->db->where('server_id', $server);
		$conditions['server'] = "server_id IS NULL";
		if ($server)
			$conditions['server'] = "server_id = {$server}";

		$conditions['file_id'] = "parent IS NULL";
		if ($file_id)
			$conditions['file_id'] = "parent = {$file_id}";

		$conditions[] = 'type = '.TYPE_DIR;
		$conditions[] = "in_trash = false";
		$conditions[] = "storage_key = '{$this->session->userdata('STR_DRIVER')}'";
		$ownFiles[] = "file_object.owner = '{$this->session->userdata('USER_KEY')}'";
		$files[] = "usuario.user_key = '{$this->session->userdata('USER_KEY')}'";
		

		$ownFiles = implode('AND', $ownFiles);
		$param = implode(' AND ', $conditions);
		$files = implode(' AND ', $files);

		$param_group = '';
		if ($this->session->userdata('USER_ID'))
			'AND group_users.user_id = '.$this->session->userdata('USER_ID');

		$query = "
				SELECT *
				FROM
				(
					(
						SELECT DISTINCT file_object.id as file_object_id, file_object.*, file_type.*
						FROM file_object
						JOIN file_type ON file_type.id_file_type = file_object.mime
						WHERE {$param}
						and {$ownFiles}
					)
					UNION
					(
						SELECT DISTINCT file_object.id as file_object_id, file_object.*, file_type.*
						FROM file_object
						JOIN file_type ON file_type.id_file_type = file_object.mime
						JOIN group_permission ON group_permission.object_id = file_object.id
						JOIN group_users ON group_users.group_id = group_permission.group_id
						JOIN usuario ON usuario.user_id = group_users.user_id
						WHERE {$param}
						AND {$files}
						AND group_permission.listable = 1
					) 
				)AS tmp_file_object";


		return $this->db->query($query)->result_array();
	}

	public function recursiveChildrenDir(&$directory, &$arrDirectories = array())
	{
		$this->db->select('file_object.parent, file_object.id, file_object.hash, file_object.original_name');
		$this->db->from('file_object');
		$where = array(
			'file_object.parent'      => $directory['id'],
			'file_object.type'        => TYPE_DIR,
			'file_object.in_trash'    => false,
			'file_object.storage_key' => $this->session->userdata('STR_DRIVER'));

		$this->db->where($where);
		$directories = $this->db->get()->result_array();


		if (!$directories)
			return false;

		$arrDirectories[$directory['id']] = $directories;

		if (is_null($directory['parent']))
			return false;

		$directory = $this->getParentDir($directory['hash']);
		$this->recursiveChildrenDir($directory, $arrDirectories);
	}

	public function getParentDir($hash)
	{
		$this->db->select('directory.parent, directory.id, directory.hash, directory.original_name');
		$this->db->from('file_object');
		$this->db->join('file_object as directory', 'directory.id = file_object.parent');
		$where = array(
			'file_object.hash'        => $hash,
			'file_object.type'        => TYPE_DIR,
			'file_object.in_trash'    => false,
			'file_object.storage_key' => $this->session->userdata('STR_DRIVER'));
		$this->db->where($where);
		$directory = $this->db->get()->result_array();

		if (!$directory)
			return false;

		return $directory[0];
	}

	public function buildSubDirMenuExplorer($arrSubDirs, &$html = '', &$serverId)
	{
		$html .= '<ul data-parent="'.$arrSubDirs[0]['parent'].'" style="list-style-type: none; margin-left:-25px;">';
		foreach ($arrSubDirs as $subDirs) {

			$class_icon = isset($subDirs['directories_children']) ? 'fa fa-caret-down' : 'fa fa-caret-right';
			$param = $serverId ? "/?server={$serverId}" : null;
			$html .= '<li>
					<a data-id="'.$subDirs['id'].'" data-server="'.$serverId.'" class="menu-explorer" href="#"><i class="'.$class_icon.'" aria-hidden="true"></i></i></a>
					<a class="ext_disabled" href="{url}arquivos/folder/'.$subDirs['hash'].''.$param.'"><i class="fa fa-folder" aria-hidden="true"></i> '.$subDirs['original_name'].'<b class=""></b></a>
				</li>';

			if (isset($subDirs['directories_children']))
				$this->buildSubDirMenuExplorer($subDirs['directories_children'], $html, $serverId);
		}
		$html .= '</ul>';
	}

	public function fileExists($path, $owner, $drive, $server = null, $type = TYPE_FILE)
	{
		$this->db->select('id');
		$this->db->from('file_object');

		$where = array(
			'full_path'   => $path,
			'type'        => $type,
			'owner'       => $owner,
			'storage_key' => $drive,
			'server_id' => $server);

		$this->db->where($where);
		$row = $this->db->get()->row();
		if (count($row) > 0)
			return $row;
		return false;
	}

	public function verifyPermissionId($arquivos)
	{
		$this->db->select('*');
		$this->db->from('group_permission');
		$this->db->where('object_id', $arquivos->id);
		$result = $this->db->get()->result();
		if(!empty($result))
			return $result[0];

		return $result;
	}

	public function findFolderByHash($hash)
	{
		$this->db->select('*');
		$this->db->from('file_object');
		$this->db->where('hash', $hash);
		$result = $this->db->get()->result()[0];
		// if(!empty($result))
		// 	return $result[0];
		return $result;
	}

	public function verifyOwnDirectory($key, $id)
	{
		$this->db->select('*');
		$this->db->from('file_object');
		$this->db->where('id', $id);
		$this->db->where('owner', $key);
		$result = $this->db->get()->result();
		if(!empty($result))
			return true;

		return false;
	}

	public function fetch(ObjectEntity $entity)
	{
		$this->db->select('*');
		$this->db->from('file_object');

		foreach (get_object_vars($entity) as $prop => $value) {
			if ($entity->{$prop})
				$this->db->where($prop, $entity->{$prop});
		}

		// echo $this->db->last_query(); exit;
		return  ($result = $this->db->get()->result()) ? $result : false;
	}

	public function recentFiles()
	{
		$server = $this->input->get('server');

		if (!$server && $this->session->userdata('SRID') == SERVER_LOCAL)
			$server = SERVER_LOCAL;

		$files = $this->fetchRecentFile($server);

		$this->vet_dados['empty'] = '';
		if (!$files)
			$this->vet_dados['empty'] = $this->parser->parse('arquivos/empty_recent_directory_view', $this->vet_dados, TRUE);

		$this->vet_dados['arquivos'] = $files ? $files : array();
		$this->vet_dados['fm_class'] = $this->session->userdata('filemanager_view');
		$this->vet_dados['fm_ico_class'] = $this->session->userdata('filemanager_ico');
		$this->vet_dados['server_id'] = $server;


		return $this->parser->parse('arquivos/file_recent', $this->vet_dados, TRUE);
	}

	private function fetchRecentFile($server_id)
	{
		$this->db->select('*');
		$this->db->from('file_object');
		$this->db->join('file_type', 'file_type.id_file_type = file_object.mime');
		$this->db->order_by('file_object.created DESC');
		$where = array(
			'server_id' => $server_id,
			'in_trash' => false,
			'created >= DATE_SUB(NOW(), INTERVAL 20 MINUTE)' => null);

		if (!$this->session->userdata('IS_OWNER'))
			$where['owner'] = $this->session->userdata('USER_KEY');

		$this->db->where($where);
		return ($result = $this->db->get()->result()) ? $result : false;
	}
}
