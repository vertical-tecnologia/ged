<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 20/07/15
 * Time: 20:38
 */

class configuracao_model extends CI_Model{

	public $vet_dados;
	private $log;

	public function __construct()
	{
		$this->_log = new ObjectLog();
		parent::__construct();
		$this->vet_dados = base_dir($this->session->userdata('skin'));
	}

	public function home()
	{
		$this->vet_dados['page_title'] = 'Pagina Inicial';
		$this->vet_dados['home_data'] = array($this->get_home_data());
		return $this->parser->parse('config/home_site_view', $this->vet_dados, TRUE);
	}

	public function contrato()
	{
		$this->vet_dados['page_title'] = 'Contrato';
		$this->vet_dados['contrato'] = $this->get_contrato()->modelo_contrato;

		return $this->parser->parse('config/contrato_view', $this->vet_dados, TRUE);
	}

	public function meu_contrato()
	{
		$this->vet_dados['page_title'] = 'Contrato de Prestação de Serviços.';
		$this->vet_dados['contrato'] = $this->get_clicontrato()->texto;

		return $this->parser->parse('config/user_contrato_view', $this->vet_dados, TRUE);
	}

	public function executavel()
	{
		$this->vet_dados['page_title'] = 'Executavel';

		$exec = $this->get_executable();

		$this->vet_dados['instrucoes'] = $exec->exe_instructions;

		$this->vet_dados['path'] = $exec->exe_path;
		$this->vet_dados['linux_path'] = $exec->linux_path;
		$this->vet_dados['macos_path'] = $exec->macos_path;

		return $this->parser->parse('config/executavel_view', $this->vet_dados, TRUE);
	}

	public function save_executavel()
	{

		$win_path = $this->do_upload('path');
		$linux_path = $this->do_upload('linux_path');
		$macos_path = $this->do_upload('macos_path');

		if(!$win_path['error']){
			$this->db->set('exe_path', $win_path['upload_data']['full_path']);
		}

		if(!$linux_path['error']){
			$this->db->set('linux_path', $linux_path['upload_data']['full_path']);
		}

		if(!$macos_path['error']){
			$this->db->set('macos_path', $macos_path['upload_data']['full_path']);
		}

		$this->db->set('exe_instructions', $this->input->post('instrucoes'));
		if($this->db->update('compania'))
			$this->session->set_flashdata('success', 'Configuraçoes atualizadas com sucesso');
		else
			$this->session->set_flashdata('error', 'Erro ao atualizar configuraçoes.');
	}

	public function save_contrato()
	{
		syslog::generate_log('UPDATE_CONTRATO');
		$this->db->set('modelo_contrato', $this->input->post('contrato'));
		return $this->db->update('compania');
	}

	public function save_home()
	{
		syslog::generate_log('UPDATE_HOME');
		$this->db->set('home_title', $this->input->post('home_title'));
		$this->db->set('home_subtitle', $this->input->post('home_subtitle'));
		return $this->db->update('compania');
	}

	public function ftp()
	{
		$this->vet_dados['page_title'] = 'Configuraçao de FTP';
		$this->vet_dados['server'] = array($this->get_ftp_server());

		return $this->parser->parse('config/ftp_view', $this->vet_dados, TRUE);
	}
	public function banco()
	{
		$this->vet_dados['page_title'] = 'Configuração de dados Bancários';
		$this->vet_dados['bank'] = array($this->get_bank());

		return $this->parser->parse('config/banco_view', $this->vet_dados, TRUE);
	}

	public function save_banco()
	{
		syslog::generate_log('UPDATE_BANK_DATA');
		$this->db->set('bank_alias', $this->input->post('bank_alias'));
		$this->db->set('bank_nome', $this->input->post('bank_nome'));
		$this->db->set('bank_codigo', $this->input->post('bank_codigo'));
		$this->db->set('bank_conta', $this->input->post('bank_conta'));
		$this->db->set('bank_cdigito', $this->input->post('bank_cdigito'));
		$this->db->set('bank_acodigo', $this->input->post('bank_acodigo'));
		$this->db->set('bank_adigito', $this->input->post('bank_adigito'));
		$this->db->set('bank_carteira', $this->input->post('bank_carteira'));
		$this->db->set('bank_nosso_ini', $this->input->post('bank_nosso_ini'));
		$this->db->set('bank_nosso_fim', $this->input->post('bank_nosso_fim'));
		$this->db->set('bank_nosso_atual', $this->input->post('bank_nosso_atual'));
		$this->db->set('bank_contrato', $this->input->post('bank_contrato'));
		$this->db->set('bank_responsavel', $this->input->post('bank_responsavel'));

		$banco = $this->get_bank();

		if($banco->bank_id > 0){
			$this->db->where('bank_id', $this->input->post('bank_id'));
			return $this->db->update('banco');
		} else {
			return $this->db->insert('banco');
		}
	}

	public function adicional()
	{
		$quantidades = $this->get_quantidades();

		$q = null;

		for($i=0;$i<count($quantidades);$i++){
			$q .= $quantidades[$i]->quant_valor.",";
		}

		$q = substr($q, 0, -1);

		$this->vet_dados['val'] = $q;
		$this->vet_dados['quantidades'] = json_encode($quantidades);

		return $this->parser->parse('config/adicional_view', $this->vet_dados, TRUE);
	}

	public function add_armazenamento()
	{
		syslog::generate_log('ADD_STORAGE_VALUE');

		if($this->valida() === false)
			return $this->armazenamento();

		$this->db->set('quant_valor', $this->input->post('quant_valor'));
		$this->db->set('quant_preco', $this->input->post('quant_preco'));
		$this->db->set('quant_desc', $this->input->post('quant_desc'));

		$this->db->insert('quantidade_armazenamento');

		if($this->db->insert_id() > 0)
			return true;
		else
			return false;
	}

	public function excluir_armazenamento()
	{
		syslog::generate_log('DELETE_STORAGE_VALUE');

		$id = (int) $this->uri->segment(3);

		if($id != 0){

			$this->db->where('quant_id', $id);
			$this->db->delete('quantidade_armazenamento');
			$this->session->set_flashdata('success','Excluído com sucesso.');
		} else {
			$this->session->set_flashdata('error','Não foi possível excluir.');
		}
		redirect('configuracao/armazenamento/');
	}

	public function armazenamento()
	{
		$vet = array();

		$vet['quant_valor'] = set_value('quant_valor');
		$vet['quant_preco'] = set_value('quant_preco');
		$vet['quant_desc'] = set_value('quant_desc');

		$this->vet_dados['qtde'] = array($vet);

		$quantidades = $this->get_quantidades();
		$this->vet_dados['quantidades'] = $quantidades;

		return $this->parser->parse('config/quantidades_view', $this->vet_dados, TRUE);
	}

	public function quota()
	{
		$this->vet_dados['quota'] = array();

		$this->vet_dados['checked'] = $this->session->userdata('QDS') == 1 ? ' checked ' : '';

		$this->vet_dados['_total_disco'] = $this->get_quota_size();

		$_total_adicional = $this->get_aditional();

		$this->vet_dados['_total_disco'] += $_total_adicional->adicional;

		$this->vet_dados['total_disco'] = By2Mb($this->vet_dados['_total_disco']);

		$users = $this->get_users();


		$this->vet_dados['free_space'] = By2Mb($this->free_space());

		$total_users = count($users);

		$this->vet_dados['_cota_individual'] = ceil($this->vet_dados['_total_disco'] / $total_users);
		$this->vet_dados['cota_individual'] = By2Mb($this->vet_dados['_cota_individual']);

		for($i=0;$i<count($users);$i++){
			$quota = $this->get_quota($users[$i]->user_id);
			if(empty($quota)){
				$entity = new QuotaEntity();
				$entity->quota_size = $this->vet_dados['_cota_individual'];
				$entity->user_id = $users[$i]->user_id;
				$this->create_quota($entity);

				$quota = $this->get_quota($users[$i]->user_id);
				$quota->quota_size = By2Mb((int) $quota->quota_size);
				$this->vet_dados['quota'][] = $quota;
				continue;
			}else{
				$quota->quota_size = By2Mb((int) $quota->quota_size);
				$this->vet_dados['quota'][] = $quota;
			}

		}

		$this->vet_dados['quota_status'] = (boolean) $this->quota_status()->quota_enabled;


		return $this->parser->parse('config/quota_view', $this->vet_dados, TRUE);
	}

	public function quota_edit()
	{
		$entity = new UsuarioEntity();
		$entity->user_id = $this->uri->segment(3);

		$this->load->model('usuario_model');
		$this->vet_dados['user'] = array($this->usuario_model->fetch($entity));

		return $this->parser->parse('config/quota_edit', $this->vet_dados, TRUE);
	}

	protected function free_space()
	{
		$total_disco = $this->get_quota_size();

		$_total_adicional = $this->get_aditional();

		$total_disco += $_total_adicional->adicional;

		$a = $this->sum_quota_user();

		$total_disco -= $a->quota_sum;

		return $total_disco;
	}

	public function save_quota_user()
	{
		$total_disco = $this->get_quota_size();

		$_total_adicional = $this->get_aditional();

		$total_disco += $_total_adicional->adicional;

		$a = $this->sum_quota_user();

		$total_disco -= $a->quota_sum;

		$entity = new QuotaEntity();
		$entity->user_id = $this->input->post('user_id');
		$entity->quota_size = toBytes(strtoupper($this->input->post('quota_user')));

		if($entity->quota_size > $total_disco)
		{
			$this->session->set_flashdata('error', $this->input->post('quota_user'). ' e muito grande. Apenas ' . By2Mb($total_disco) . ' restantes.');
			return false;
		}

		$this->delete_quota_if_exists($entity);

		$this->create_quota($entity);

	}

	public function sum_quota_user()
	{
		$this->db->select('SUM(usuario_quota.quota_size) as quota_sum');
		$this->db->from('usuario_quota');
		$this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario_quota.user_id');
		$this->db->where('empresa_usuario.id_empresa',  $this->session->userdata('EID'));

		return $this->db->get()->row();

	}

	public function delete_quota_if_exists(QuotaEntity $entity)
	{
		$this->db->where('user_id', $entity->user_id);
		return $this->db->delete('usuario_quota');

	}

	protected function getTotalStorage()
	{
		$this->db->select('storage_size');
		$this->db->from('compania');
		return $this->db->get()->row();
	}

	public function espaco()
	{
		$this->vet_dados['espaco'] = array($this->getTotalStorage());
		return $this->parser->parse('config/espaco_view', $this->vet_dados, TRUE);
	}

	public function save_storage()
	{
		$storage = $this->input->post('espaco');
		$this->db->set('storage_size', $storage);
		return $this->db->update('compania');

	}

	public function save_quota()
	{
		syslog::generate_log('UPDATE_QUOTA');

		$this->db->trans_begin();
		$users = $this->get_users();
		$actual = new stdClass();
		$actual->user_id = $this->session->userdata('USER_ID');
		$users[] = $actual;


		$quota = $this->input->post('quota_value');
		$enabled = $this->input->post('quota_enabled') == 'on' ? true : false;

		$this->db->set('quota_enabled', $enabled);
		$this->db->where('client_id', $this->session->userdata('CLID'));

		if($this->db->update('cliente')){

			$this->session->set_userdata('QDS', $enabled);
			$this->db->trans_commit();
			return true;
		} else {
			$this->db->trans_rollback();
			return false;
		}

	}

	public function quota_status()
	{
		$this->db->select('quota_enabled');
		$this->db->from('cliente');
		$this->db->where('client_id', $this->session->userdata('CLID'));

		return $this->db->get()->row();
	}

	protected function get_users()
	{
		$this->db->select('usuario.*');
		$this->db->from('usuario');
		$this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id');
		$this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));

		$this->db->group_by('usuario.user_id');

		return $this->db->get()->result();
	}

	protected function get_quota($user_id){

		$this->db->select('usuario_quota.*, usuario.user_fullname, usuario.user_login');
		$this->db->from('usuario_quota');
		$this->db->join('usuario', 'usuario_quota.user_id = usuario.user_id');
		$this->db->where('usuario_quota.user_id', $user_id);

		return $this->db->get()->row();
	}

	protected function create_quota(QuotaEntity $entity)
	{
		$this->db->set('quota_size', $entity->quota_size);
		$this->db->set('user_id', $entity->user_id);

		$this->db->insert('usuario_quota');

		if($this->db->insert_id() > 0)
			return true;
		else
			return false;

	}

	protected function set_quota(QuotaEntity $entity)
	{
		syslog::generate_log('SET_USER_QUOTA');
		$this->db->set('quota_size', $entity->quota_size);

		if($entity->user_id != '')
			$this->db->where('user_id', $entity->user_id);

		if($entity->quota_id != '')
			$this->db->where('quota_id', $entity->quota_id);

		return $this->db->update('usuario_quota');

	}

	protected function get_quota_size()
	{
		$plano = $this->get_plan();
		$quota = $this->get_services($plano->plan_id);

		return $quota->total_storage;

	}

	protected function get_services($plan)
	{
		$this->db->select('SUM(servico.service_storage) as total_storage');
		$this->db->from('servico');
		$this->db->join('plano_servico', 'plano_servico.service_id = servico.service_id');
		$this->db->where('servico.service_storage <> -1');
		$this->db->where('plano_servico.plan_id', $plan);

		return $this->db->get()->row();
	}

	protected function get_plan()
	{
		$this->db->select('plano.plan_id');
		$this->db->from('cliente');
		$this->db->join('plano', 'plano.plan_id = cliente.client_plan_id');
		$this->db->where('client_id', $this->session->userdata('CLID'));
		return $this->db->get()->row();
	}

	protected function get_usage()
	{
		$this->db->select('SUM(size) as disk_usage');
		$this->db->from('file_object');

		if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
			$this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
		} else {
			$this->db->where('owner', $this->session->userdata('USER_KEY'));
		}

		return $this->db->get()->row();
	}


	public function get_quantidades()
	{
		$this->db->from('quantidade_armazenamento');
		$this->db->where('quant_valor > 1');

		$this->db->order_by('quant_valor ASC');
		return $this->db->get()->result();
	}

	public function save_adicional()
	{

		syslog::generate_log('ADD_ADITIONAL_SPACE');
		$this->db->set('quant_id', $this->input->post('quant_id'));
		$this->db->set('client_id', $this->session->userdata('CLID'));
		$this->db->set('price', $this->input->post('quant_preco'));

		$this->db->insert('espaco_adicional');

		$id = (int) $this->db->insert_id();

		if($id > 0)
			$this->session->set_flashdata('success', 'Espaço adicional contratado com sucesso.');
		else
			$this->session->set_flashdata('error', 'Erro ao contratar espaço adicional.');

	}

	public function get_contrato()
	{
		$this->db->select('modelo_contrato');
		$this->db->from('compania');

		return $this->db->get()->row();
	}

	public function get_ftp_server()
	{
		$this->db->select('cliente.ftp_host, cliente.ftp_user, cliente.ftp_password');
		$this->db->from('cliente');
		$this->db->where('client_id', $this->session->userdata('CLID'));

		return $this->db->get()->row();
	}

	public function update_ftp_server()
	{
		syslog::generate_log('UPDATE_FTP_SERVER');
		$this->db->set('cliente.ftp_host',$this->input->post('ftp_host'));
		$this->db->set('cliente.ftp_user',$this->input->post('ftp_user'));
		$this->db->set('cliente.ftp_password', $this->input->post('ftp_password'));
		$this->db->where('client_id', $this->session->userdata('CLID'));
		$this->db->update('cliente');
	}

	public function get_home_data()
	{
		$this->db->select('home_title, home_subtitle');
		$this->db->from('compania');
		return $this->db->get()->row();
	}

	private function valida()
	{
		$this->form_validation->set_message('required', 'Campo obrigatório');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		return $this->form_validation->run('quantidades');
	}

	private function get_bank()
	{
		$this->db->from('banco');
		return $this->db->get()->row();
	}

	public function get_executable()
	{
		$this->db->select('compania.exe_path , compania.exe_instructions, compania.linux_path, compania.macos_path');
		$this->db->from('compania');

		return $this->db->get()->row();
	}

	function do_upload($field_name)
	{
		syslog::generate_log('UPLOAD_EXE_FILE');

		$config['upload_path'] = './dist/';
		$config['allowed_types'] = 'tar|exe|tar.gz|zip|rar';
		$config['max_size']	= '102400';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());

			return $error;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	protected function get_aditional()
	{
		$this->db->select('SUM(quant_valor) as adicional');
		$this->db->from('espaco_adicional');
		$this->db->join('quantidade_armazenamento', 'quantidade_armazenamento.quant_id = espaco_adicional.quant_id');
		$this->db->where('espaco_adicional.client_id', $this->session->userdata('CLID'));

		return $this->db->get()->row();
	}

	public function get_clicontrato()
	{
		$this->db->select('texto');
		$this->db->from('contrato');
		$this->db->where('contrato_cliente', $this->session->userdata('CLID'));
		return $this->db->get()->row();
	}

	public function xml()
	{
		$dados = array('content'=> '', 'scanner1' => '', 'scanner2' => '');

		if ($this->input->post()) {
			$scanners = $this->input->post('scanner');
			$arrScanner = array();

			if ($scanners) {
				foreach ($scanners as $id => $scanner) {
					if (empty($scanner))
						continue;
					$scanner = str_replace([' ', '.'], '', implode(';', $scanner));
					$arrScanner[] = array('xml_type' => $id,'tags' => explode(';', $scanner));
				}
			}

			$arrTags = array();
			foreach ($arrScanner as $scanner) {
				$arrTags[] = array(
					'xml_type'   => $scanner['xml_type'],
					'tag'        => implode(';', array_unique($scanner['tags'])),
					'user_key' => $this->session->userdata('USER_KEY'));
			}

			$dados['class']    = 'success';
			$dados['content']  = 'Tags cadastradas com sucesso.';
			$this->saveTagXML($arrTags);
		}

		$arrResult = $this->getTagXML();
		foreach ($arrResult as $result){
			foreach (explode(';', $result['tag']) as $value)
				$dados["scanner{$result['xml_type']}"][] = array('tag' => $value);
		}

		return $this->parser->parse('config/xml_view', $dados, TRUE);
	}

	private function saveTagXML($arrTags)
	{
		$this->db->where('usuario_xml_tag.user_key', $this->session->userdata('USER_KEY'));
		$this->db->delete('usuario_xml_tag');

		if ($arrTags)
			$this->db->insert_batch('usuario_xml_tag', $arrTags);
	}

	private function getTagXML()
	{
		$this->db->select('usuario_xml_tag.*');
		$this->db->from('usuario_xml_tag');
		$this->db->where('usuario_xml_tag.user_key', $this->session->userdata('USER_KEY'));
		return $this->db->get()->result_array();
	}

}