<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 01:33
 */

class plano_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('plano_status_model');
        $this->load->model('servico_model');
        $this->load->model('servidor_model');

    }

    public function index()
    {
        $this->vet_dados['planos'] = $this->fetchAll();

        $this->vet_dados['nome_tela'] = 'Gerenciar Planos';
        $this->vet_dados['tela']      = 'plano';

        return $this->parser->parse('plano/plano_list_view', $this->vet_dados, TRUE);
    }

    public function edit(PlanoEntity $plano)
    {
        $plan = $this->fetch(array('plan_id' => $plano->plan_id));

        $plano_status = $this->plano_status_model->fetchAll();

        for($i=0;$i<count($plano_status);$i++){
            if($plano_status[$i]->status_id == $plan->plan_status_id)
                $plano_status[$i]->selected = ' selected ';
            else
                $plano_status[$i]->selected = '';
        }

        $this->vet_dados['plano_status'] = $plano_status;




        $entity = new PlanoEntity();
        $entity->plan_id = $plan->plan_id;
        $servicos_plano = $this->getServicesPlan($entity);

        $servico = $this->servico_model->fetchAll();



        for($i=0;$i<count($servico);$i++){

            for($j=0;$j<count($servicos_plano);$j++){

                if($servicos_plano[$j]->service_id == $servico[$i]->service_id){
                    $servico[$i]->selected = " selected "; break;
                }
                else
                    $servico[$i]->selected = "";
            }
        }

        $servidor = $this->servidor_model->fetchAll(new ServerEntity());

        for($i=0;$i<count($servidor);$i++){
            $servidor[$i]->server_type_desc = constant('STORAGE_'.$servidor[$i]->server_type);
            if($servidor[$i]->server_id == $plan->server_id)
                $servidor[$i]->selected = ' selected ';
            else
                $servidor[$i]->selected = '';
        }

        $this->vet_dados['servico'] = $servico;
        $this->vet_dados['servidor'] = $servidor;

        $vet['plan_id'] = $plan->plan_id;
        $vet['plan_name'] = $plan->plan_name;
        $vet['plan_desc'] = $plan->plan_desc;
        $vet['plan_price'] = $plan->plan_price;
        $vet['plano_max_users'] = $plan->plano_max_users;

        $vet['plan_startdate'] = data_formatada($plan->plan_startdate, 3);
        $vet['plan_enddate'] = data_formatada($plan->plan_enddate, 3);
        $vet['plan_status_id'] = $plan->plan_status_id;


        if(count($plan) == 0)
            redirect('plano');

        $this->vet_dados['plano'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Usuários';
        $this->vet_dados['tela']      = 'plano';

        return $this->parser->parse('plano/plano_cad_view', $this->vet_dados, TRUE);
    }


    public function cadastro()
    {
        $this->vet_dados['plano_status'] = $this->plano_status_model->fetchAll();
        $this->vet_dados['servico'] = $this->servico_model->fetchAll();

        $servidor = $this->servidor_model->fetchAll(new ServerEntity());
        for($i=0;$i<count($servidor);$i++){
            $servidor[$i]->server_type_desc = constant('STORAGE_'.$servidor[$i]->server_type);
            $servidor[$i]->selected = '';
        }
        $this->vet_dados['servidor'] = $servidor;

        $vet['plan_id'] = set_value('plan_id');
        $vet['plan_name'] = set_value('plan_name');
        $vet['plan_desc'] = set_value('plan_desc');
        $vet['plan_price'] = set_value('plan_price');
        $vet['plan_promotional_price'] = set_value('plan_promotional_price');
        $vet['plan_startdate'] = set_value('plan_startdate');
        $vet['plan_enddate'] = set_value('plan_enddate');
        $vet['plan_status_id'] = set_value('plan_status_id');
        $vet['plano_max_users'] = set_value('plano_max_users');

        $this->vet_dados['plano'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Planos';
        $this->vet_dados['tela']      = 'plano';

        return $this->parser->parse('plano/plano_cad_view', $this->vet_dados, TRUE);
    }

    public function fetch($params)
    {


        $this->db->select('plano.*, plano_status.status_name');
        $this->db->from('plano');
        $this->db->join('plano_status', 'plano_status.status_id = plano.plan_status_id');


        if($params['plan_id'] == 0){
            throw new Exception('Wrong Parameter');
        }


        $this->db->where('plano.plan_id', $params['plan_id']);

        return $this->db->get()->row();

    }

    public function fetchAll($params = null)
    {

        $this->db->select('plano.*, plano_status.status_name');
        $this->db->from('plano');
        $this->db->join('plano_status', 'plano_status.status_id = plano.plan_status_id');

        if(isset($params['plan_status_id'])){
            $this->db->where('plano.plan_status_id', $params['plan_status_id']);
        }

        if(isset($params['plan_id'])){
            $this->db->where('plano.plan_id', $params['plan_id']);
        }

        $this->db->order_by('plano.plan_order');

        return $this->db->get()->result();

    }

    public function save(PlanoEntity $plano)
    {

        if($this->valida()=== FALSE){
            return $this->cadastro();
        }

        $this->db->trans_begin();

        $id = (int) $plano->plan_id;

        if($id == 0){

            $this->setFields($plano);
            $this->db->insert('plano');

            $id = $this->db->insert_id();


            if($id > 0 && count($this->input->post('service_id')) > 0){
                $this->saveSevicesPlan($plano->service_id, $id);
            }

            if ($this->db->trans_status() && $id > 0) {

                $this->db->trans_commit();
                syslog::generate_log('NEW_PLAN_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('NEW_PLAN_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }

        } else {
            $this->setFields($plano);
            $this->db->where('plan_id', $id);
            $this->db->update('plano');


            $this->deleteAllSevicesPlan($id);

            $this->saveSevicesPlan($plano->service_id, $id);

            if ($this->db->trans_status()) {

                $this->db->trans_commit();
                syslog::generate_log('UPDATE_PLAN_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_PLAN_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_update'));

            }
        }

    }

    public function saveSevicesPlan($services, $plan_id)
    {
        foreach($services as $key => $value){
            $this->db->set('plan_id', $plan_id);
            $this->db->set('service_id', $value);
            $this->db->insert('plano_servico');
        }

    }

    public function deleteAllSevicesPlan($plan_id)
    {
        $this->db->where('plan_id', $plan_id);
        return $this->db->delete('plano_servico');

    }

    public function delete(PlanoEntity $plano)
    {
        $this->db->where('plan_id', $plano->plan_id);
        if($this->db->delete('plano')){
            syslog::generate_log('DELETE_PLAN_SUCCESS');
            return true;
        }  else {
            syslog::generate_log('DELETE_PLAN_ERROR');
            return false;
        }
    }

    public function getServicesPlan(PlanoEntity $entity)
    {
        $this->db->select('service_id');
        $this->db->from('plano_servico');
        $this->db->where('plan_id', $entity->plan_id);

        return $this->db->get()->result();
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if(gettype($value) =='array' || $key == 'plan_id')
                continue;

            if($value != null)
                $this->db->set($key, $value);
        }
    }

    protected function saveServices($plan, $services)
    {
        foreach($services as $service){
            $this->db->set('service_id', $service);
            $this->db->set('plan_id', $plan);

            $this->db->insert('plano_servico');

            if($this->db->insert_id() == 0){
                $this->db->trasn_rollback();
                throw new Exception('Erro ao cadastrar servicos do planos');
            }
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('plano');
    }

}