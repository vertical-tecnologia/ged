<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 29/06/15
 * Time: 12:31
 */

class configuration_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_config()
    {
        if($this->session->userdata('USER_ROLE')){
            $empresa = $this->get_empresa();

            if(count($empresa) == 0){
                return false;
            }

            $contatoEntity = new ContatoEmpresaEntity();
            $contatoEntity->empresa_id = $empresa->empresa_id;
            $telefone   = $this->configuration_model->get_telefone_empresa($contatoEntity);
            $email      = $this->configuration_model->get_email_empresa($contatoEntity);

            if(count($email) ==0 || count($telefone) == 0){
                return false;
            }

            if(count($empresa) > 0){
                $entity = new EmpresaEntity();
                $entity->empresa_id = $empresa->empresa_id;
                $endereco = $this->get_endereco_empresa($entity);
                if(count($endereco) == 0)
                    return false;
            }

            $cliente = $this->get_cliente($empresa->empresa_id);

            if(count($cliente) == 0){
                return false;
            }

            if(isset($cliente->cliente_id)){

                $contratoEntity = new ContratoEntity();
                $contratoEntity->contrato_cliente = $cliente->client_id;
                $contrato = $this->getContrato($contratoEntity);
            }
//            Debug::Test($contrato);die;

        }
    }

    public function get_empresa()
    {
        $this->db->from('empresa');
        $this->db->where('empresa.empresa_user_id', $this->session->userdata('USER_ID'));

        return $this->db->get()->row();
    }

    public function get_cliente($empresa_id)
    {
        $this->db->from('cliente');
        $this->db->where('client_empresa_id', $empresa_id);

        return $this->db->get()->row();
    }

    public function create_client(ClienteEntity $entity)
    {
        $entity->client_key = random_string('alnum', 20);
        $entity->client_volume = random_string('alnum', 20);
        $entity->client_local = true;


        $this->db->set('client_empresa_id', $entity->client_empresa_id);
        $this->db->set('client_volume', $entity->client_volume);
        $this->db->set('client_key', $entity->client_key);
        $this->db->set('client_local', $entity->client_local);
        $this->db->set('client_plan_id', $entity->client_plan_id);
        $this->db->set('server_id', $entity->server_id);

        $this->db->insert('cliente');

        $entity->client_id = (int) $this->db->insert_id();

        if($entity->client_id > 0){
            return $entity;
        } else {
            return false;
        }
    }

    public function get_cliente_empresa(ClienteEntity $entity)
    {
        $this->db->select('cliente.client_id, cliente.client_volume, cliente.client_key, cliente.client_local');
        $this->db->from('cliente');
        $this->db->where('cliente.client_empresa_id', $entity->client_empresa_id);

        return $this->db->get()->row();
    }

    public function get_endereco_empresa(EmpresaEntity $entity)
    {
        $this->db->from('endereco');
        $this->db->join('empresa_endereco', 'empresa_endereco.endereco_id = endereco.endereco_id');
        $this->db->where('endereco.endereco_principal', true);
        $this->db->where('empresa_endereco.empresa_id', $entity->empresa_id);

        return $this->db->get()->row();

    }

    public function get_telefone_empresa(ContatoEmpresaEntity $entity)
    {
        $this->db->from('contato');
        $this->db->join('empresa_contato', 'empresa_contato.contato_id = contato.contato_id');
        $this->db->where('contato.tipo_id', 1);
        $this->db->where('contato.contato_principal', 1);
        $this->db->where('empresa_contato.empresa_id', $entity->empresa_id);

        return $this->db->get()->result();
    }

    public function get_email_empresa(ContatoEmpresaEntity $entity)
    {
        $this->db->from('contato');
        $this->db->join('empresa_contato', 'empresa_contato.contato_id = contato.contato_id');
        $this->db->where('contato.tipo_id', 2);
        $this->db->where('contato.contato_principal', 1);
        $this->db->where('empresa_contato.empresa_id', $entity->empresa_id);

        return $this->db->get()->result();
    }

    public function getContrato(ContratoEntity $entity)
    {
        $this->db->from('contrato');

        if($entity->contrato_id != ''){
            $this->db->where('contrato_id', $entity->contrato_id);
            return $this->db->get()->row();
        }

        if($entity->contrato_cliente != ''){
            $this->db->where('contrato_cliente', $entity->contrato_cliente);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();

    }

    public function get_user_plan(UsuarioEntity $usuarioEntity)
    {
        $this->db->select('usuario.user_plan_id, plano.server_id');
        $this->db->from('usuario');
        $this->db->join('plano', 'plano.plan_id = usuario.user_plan_id');

        $this->db->where('user_id', $usuarioEntity->user_id);
        return $this->db->get()->row();
    }
    
    public function set_client_plan($client_id, $plan_id)
    {
        $this->db->set('client_plan_id', $plan_id);
        $this->db->where('cliente_id', $client_id);
        return $this->db->update('cliente');
    }

    public function set_contrato_valor($contrato_id, $contrato_valor)
    {
        $this->db->set('contrato_id', $contrato_id);
        $this->db->where('contrato_valor', $contrato_valor);
        return $this->db->update('contrato');
    }
}