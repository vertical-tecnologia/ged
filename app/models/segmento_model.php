<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 02:37
 */

class segmento_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('segmento_status_model');
        $this->load->model('segmento_diretorio_model');
    }


    public function index()
    {
        $this->vet_dados['segmento'] = $this->fetchAll(new SegmentoEntity());

        $this->vet_dados['nome_tela'] = 'Gerenciar Segmentos';
        $this->vet_dados['tela']      = 'segmento';

        return $this->parser->parse('segmento/segmento_list_view', $this->vet_dados, TRUE);

    }

    public function cadastro()
    {

        $this->vet_dados['segmento_status'] = $this->segmento_status_model->fetchAll();
        $vet['segmento_id']          = set_value('segmento_id');
        $vet['segmento_name']        = set_value('segmento_name');
        $vet['segmento_status_id']   = set_value('segmento_status_id');

        $this->vet_dados['segmento'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Segmentos';
        $this->vet_dados['tela']      = 'segmento';

        return $this->parser->parse('segmento/segmento_cad_view', $this->vet_dados, TRUE);

    }

    public function edit(SegmentoEntity $entity)
    {
        $segmento = $this->fetch($entity);

        $status = $this->segmento_status_model->fetchAll();


        foreach($status as $s){
            $s->selected = $segmento->segmento_status_id == $s->status_id ? ' selected ' : '';
        }

        $this->vet_dados['segmento_status'] = $status;

        $vet['segmento_id']          = $segmento->segmento_id;
        $vet['segmento_name']        = $segmento->segmento_name;
        $vet['segmento_status_id']   = $segmento->segmento_status_id;

        $this->vet_dados['segmento'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Segmentos';
        $this->vet_dados['tela']      = 'segmento_status';

        return $this->parser->parse('segmento/segmento_edit_view', $this->vet_dados, TRUE);

    }

    public function directory(SegmentoEntity $entity)
    {
        $segmento = $this->fetch($entity);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Segmentos';
        $this->vet_dados['tela']      = 'segmento_status';

        $this->vet_dados['segmento_id'] = $segmento->segmento_id;


        $dEntity = new DiretorioEntity();
        $dEntity->dir_segmento_id = $entity->segmento_id;

        $this->vet_dados['segmento'] = array($segmento);


        $html = $this->segmento_diretorio_model->get_child_dir($dEntity);

        $this->vet_dados['diretorio'] = $html;
//
//
//        if($this->input->is_ajax_request()){
//            $html = "<li id = \"$dir->dir_id\" class=\"folder\">".$dir->dir_name."</li>";
//        }

        return $this->parser->parse('segmento_diretorio/segmento_diretorio_cad_view', $this->vet_dados, TRUE);

    }


    public function novo_diretorio(SegmentoEntity $entity)
    {
        $this->vet_dados['dir_parent_id'] = $this->uri->segment(4);
        $this->vet_dados['segmento'] = array($this->fetch($entity));
        return $this->parser->parse('segmento_diretorio/segmento_novo_diretorio', $this->vet_dados, TRUE);
    }

    public function save_diretorio()
    {
        $this->db->set('dir_name', $this->input->post('dir_name'));
        $this->db->set('dir_parent_id', $this->input->post('dir_parent_id') != '' ? $this->input->post('dir_parent_id') : null);
        $this->db->set('dir_segmento_id', $this->input->post('segmento_id'));

        $this->db->insert('segmento_diretorio');

        if($this->db->insert_id() > 0){
            syslog::generate_log('NEW_SEGMENT_DIRECTORY_SUCCESS');

        } else {
            syslog::generate_log('NEW_SEGMENT_DIRECTORY_ERROR');
        }

        redirect('segmento/diretorios/'.$this->input->post('segmento_id'));

    }

    public function excluir_diretorio()
    {
        $entity = new DiretorioEntity();
        $entity->dir_id = $this->uri->segment(3);

        $diretorio = $this->segmento_diretorio_model->fetch($entity);

        if((int)$diretorio->dir_id > 0)
        {
            $this->segmento_diretorio_model->excluir($entity);
            redirect('segmento/diretorios/' . $diretorio->dir_segmento_id);
        }
    }

    public function fetch(SegmentoEntity $entity)
    {
        $this->db->from('segmento');
        if($entity->segmento_id)
            $this->db->where('segmento_id', (int) $entity->segmento_id);

        if($entity->segmento_status_id)
            $this->db->where('segmento_status_id', $entity->segmento_status_id);

        return $this->db->get()->row();
    }

    public function fetchAll(SegmentoEntity $entity)
    {
        $this->db->select('segmento.*, segmento_status.status_name');
        $this->db->from('segmento');
        $this->db->join('segmento_status', 'segmento.segmento_status_id = segmento_status.status_id');

        if($entity->segmento_status_id)
            $this->db->where('segmento_status_id', $entity->segmento_status_id);

        return $this->db->get()->result();
    }

    public function save(SegmentoEntity $segmento)
    {
        if($this->valida() === FALSE){
            if($this->input->post('action') == 'editar')
                return $this->edit($segmento);
            else
                return $this->cadastro();
        }


        $data = [
            'segmento_name' => $segmento->segmento_name,
            'segmento_status_id' => $segmento->segmento_status_id
        ];

        $id = (int) $segmento->segmento_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('segmento');

            $id = $this->db->insert_id();

            if ($this->db->trans_status() && $id > 0) {

                $this->db->trans_commit();
                syslog::generate_log('NEW_SEGMENT_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('NEW_SEGMENT_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }

        } else {
            $this->setFields($data);
            $this->db->where('segmento_id', $id);
            $this->db->update('segmento');

            $count = $this->db->affected_rows();

            if ($this->db->trans_status() && $count > 0) {

                $this->db->trans_commit();
                syslog::generate_log('UPDATE_SEGMENT_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_SEGMENT_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }
        }
    }

    public function delete(SegmentoEntity $segmento)
    {
        $this->db->where('segmento_id', $segmento->segmento_id);
        $this->db->delete('segmento');

        if($this->db->affected_rows() == 1){
            syslog::generate_log('DELETE_SEGMENT_SUCCESS');
            return true;
        }
        else{
            syslog::generate_log('DELETE_SEGMENT_ERROR');
            return false;
        }

    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($value != "" && $value != null)
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('segmento');
    }

}