<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 02:37
 */

class segmento_diretorio_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));

    }


    public function fetchAll(DiretorioEntity $entity)
    {
        $this->db->from('segmento_diretorio');

        if($entity->dir_id != '')
            $this->db->set('dir_id', $entity->dir_id);

        if($entity->dir_segmento_id != '')
            $this->db->set('dir_segmento_id', $entity->dir_segmento_id);

        return $this->db->get()->result();
    }


    public function fetch(DiretorioEntity $entity)
    {
        $this->db->from('segmento_diretorio');

        if($entity->dir_id != '')
            $this->db->set('dir_id', $entity->dir_id);

        return $this->db->get()->row();
    }

    public function get_root_dir(DiretorioEntity $diretorio)
    {
        $this->db->from('segmento_diretorio');
        $this->db->where('dir_parent_id IS NULL');

        if($diretorio->dir_segmento_id != ''){
            $this->db->where('dir_segmento_id', $diretorio->dir_segmento_id);
        }

        return $this->db->get()->row();
    }

    public function get_child_dir(DiretorioEntity $diretorio) {

        $query = $this->db->get_where('segmento_diretorio', array('dir_segmento_id' => $diretorio->dir_segmento_id, 'dir_parent_id' => $diretorio->dir_parent_id));

        $html = "<ul>";

        foreach ($query->result() as $row)
        {

            $html .= "<li>";
            $html .= '<input type="checkbox", id="dir-'.$row->dir_id.'"/>';
            $html .= '<label for="dir-'.$row->dir_id.'">'.$row->dir_name;
            $html .= "<span class='pull-right'><button onclick='goToHref(this)' data-href='".base_url()."segmento/novo_diretorio/".$row->dir_segmento_id."/".$row->dir_id."' class='btn btn-success btn-xs'><i class='fa fa-plus'></i></button>";
            $html .= "<button onclick='goToHref(this)' data-href='".base_url()."segmento/excluir_diretorio/".$row->dir_id."' class='btn btn-danger btn-xs'><i class='fa fa-times'></i></button>";
            $html .= '</span></label>';
            $diretorio->dir_parent_id = $row->dir_id;
            $html .= $this->get_child_dir($diretorio);
            $html .= "</li>";
        }

        $html .="</ul>";

        $html .="";
        return $html;
    }

    public function save(DiretorioEntity $diretorio)
    {
        $data = [
            'dir_name' => $diretorio->dir_name,
            'dir_segmento_id' => $diretorio->dir_segmento_id,
            'dir_parent_id' => $diretorio->dir_parent_id,
        ];

        $id = (int) $diretorio->dir_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('segmento_diretorio');

            $id = $this->db->insert_id();

            if ($this->db->trans_status() && $id > 0) {

                $this->db->trans_commit();
                syslog::generate_log('NEW_SEGMENT_DIRECTORY_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('NEW_SEGMENT_DIRECTORY_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }

        } else {
            $this->setFields($data);
            $this->db->where('dir_id', $id);
            $this->db->update('segmento_diretorio');

            $count = $this->db->affected_rows();

            if ($this->db->trans_status() && $count > 0) {

                $this->db->trans_commit();
                syslog::generate_log('UPDATE_SEGMENT_DIRECTORY_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_SEGMENT_DIRECTORY_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }
        }
    }

    public function delete($id)
    {
        $this->db->where('dir_id', $id);
        if($this->db->delete('segmento_diretorio')){
            syslog::generate_log('DELETE_SEGMENT_DIRECTORY_SUCCESS');
            return true;
        } else {
            syslog::generate_log('DELETE_SEGMENT_DIRECTORY_ERROR');
            return false;
        }
    }

    public function excluir(DiretorioEntity $entity)
    {
        $this->db->where('dir_id', $entity->dir_id);
        if($this->db->delete('segmento_diretorio')){
            syslog::generate_log('DELETE_SEGMENT_DIRECTORY_SUCCESS');
            return true;
        } else {
            syslog::generate_log('DELETE_SEGMENT_DIRECTORY_ERROR');
            return false;
        }
    }


    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($value != "" && $value != null)
                $this->db->set($key, $value);
        }
    }



}