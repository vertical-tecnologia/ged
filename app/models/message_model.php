<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 21/07/15
 * Time: 09:55
 */

// require_once 'ged_model.php';

class message_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('arquivos_model');
        $arrServers = $this->arquivos_model->get_servers();
        $this->load->vars(array('servers' => $arrServers));
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function user()
    {
        $users = $this->fetchMyUsers();
        $entity = new MessageEntity();
        $toUser = $this->get_user_id(urldecode($this->uri->segment(3)));
        $entity->to_user = isset($toUser->user_id) ? $toUser->user_id : 0;

        $entity->from_user = $this->session->userdata('USER_ID');
        if($entity->to_user == 0){
            $messages = array();
            $this->vet_dados['disabled'] = ' disabled ';
        } else {
            $messages = $this->fetchAll($entity);
            $readed = new MessageEntity();
            $readed->from_user = $entity->to_user;
            $readed->to_user = $entity->from_user;
            $this->set_readed_message($readed);
            $this->vet_dados['disabled'] = '';

            for($i=0;$i<count($messages);$i++){
                $messages[$i]->user_picture = $messages[$i]->user_picture != '' ? base_url() .'public/static/'.$messages[$i]->user_picture : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAGJ0lEQVR4Xu2bZah1RRSGn8/uTuzCDhRBRRFbRDAxEQO7EwsTsRvEBgtbfxhYoIItdv+wCwwUu4Pnsvdl7nz7nLP3mTn3nBvr33fPxLvevWbNivmmMMFlygTXn0kCJi1ggjMweQRG0QBmB7YGNgZWB5YG5ij2/wH4CHgNeBx4APh5NLCNhgUsB5wA7ArMXFOpX4FbgfOA92vO6WpYLwlQ2TOBI4HpukIHfwEXA6cDv3e5RttpvSJgWeBeYNVMoF8Htgc+zLTe8DK9IGAN4BFggQqwHwO3AU8AbwPfFmPmA1Yu/INHZYmKuV8BmwNv5CQhNwF++WcqlP8EOB64C/i3gwLTADsX53+xaKwkrJfTEnIS4Jl/ocLsdWb7A780/HKzAdcCu0TzvCnWAf5ouF7l8JwEnA8cF+1yEXBsItBLCkcaLnMucGLiukPTcxHgVfdO5O398rtnACnG24GdgrW8HVYEPkhdPxcB1wP7BGA88zq1pmbfSh+DKAleNBjg8fBoJUkOAgSncwqDHM/tHUnIpp6sNd0S/FlyF0wlOQcBXluaeyledcvU8PZN+Zm28P6LBxO9Le5sulA4PgcBmuK+waLnACelgGozN3a0VwMHpuyVg4AXgbUDEAYrj6WAajN3S+Ch4PfngXVT9spBgNHcvAGIRYAvU0C1masT/Cz4/evCD3S9XQ4C/gSmDxDMCPi3Xohrh0mR+/i3rmWsE/BPQqY5RFoOAr4BTGZKGc0j4PGbv+vPn4mAfjrBlyIH3JiLHBZwDbBfsHO2OL1Cmwui3MK9D2isdTAhBwFGfeb4pRgGW+/rlPY2xW0gZN0wTJEHIhAybTUUniXQaLeIlKbKVo3fA7gp+MFQeKHU4mkOCxBTHA16V6+UCi5Q1uqxyZAOtpSrgINSmc1FgJUgAYbxgCmseUKqiNF4f8dgIYshKwDmHUmSiwBB6Pwse4VyKXA08F+XKMXnGodH888CTulyzRHTchIwE2BsbtMjFL+eydJPDQFr9tYZwi/vEi8XdcEs0WZOAgSn93+2Ij7/vGiOeCyM3tqJ3l4nalYZnnnneLOYeL3SkMyWw3MT4EarAY+2SFI+LQoltr/eAowiFaO5VYBNiopwXA0OFbBldllBUHLFqRcElJZwD2CPoFeiVR0C3JeyQa8IEJM+4TTgmOh2aIJXb38/sBkwZ4uJXsFHAL81Wbgc2y0BmuhRRT3uQsDubiuxPObt4LmetSZIzdygxwqQkaXzDLddx+AnFh3jtoBW0Ui6IcCz6hlfuNjJHuAONXY1Ytwqao/PBej0vi/CXJ2b/uHBFsVOiTi1ID+MOdz+C2BT4L0aWIaHNCXAZqd9vbACJOvtnFYTPHXHrlVUiA2GQtGpbgi8W3ehJgTYsPSej03QHr79/9EWy/E3A9tEGxuG2z+sdRzqEmCio/Jxuzs10kslzUbqFRWVYX3C+nXeFNQl4AZgzwitPTvD3EGQKytIuC6qU1TirEOADu7uaLZdHxOdbmP83KRpCTrj+Dj477ZxQicCjMf1qqXHF/ibRXvadzyDJPoEy3OhY/RmWL5d+6wTAXGGZwKyZvG6Y5CUL7GITV8VXpFnAye3AtuOABuPlqDCpucZxYOlQVS+xBS3z8wXlgye44zA3o4As7HwetOcLHz05LVWRkYNlnw34AcspaUVtCLAOF6F5wkWORjQ244F8Wmet1QpBkgGa1M9q2lFgNVWc/dSbEC4wKB//RKvVuAHDBMoX5j4SKvWEfBK2S4YacITv/8ZdEu4HDgsAKny4TOboZ+qLGAG4Lsoc9O7vjroGkf4fEn2XPA3M8y5gb/DcVUEGEI+FQzqR7KTg2t1s18R9g7NEUJSKi3AAoYmX8qNwF45EPVhDf2Y/qwUaxjmL8NSZQEWIuzClHJokXD0AX/ylr5RtJ9YijnN3p0IsKobPjvZCHgyGUp/FtgCeDjY+mlgg04EWLkNCxyWuo0Ix6KYB4QVIjtJS3UiQG9Zt3Y31kj5MS6uVvkAGxeml+NRvAJH1BKrCPDlVdKzkwFmzmtxREmvigCLCL68qPoPDwOsW0doKm9p3T5D22uw40rjaUCngsh40rVSl0kCxv0n7qDg/4zyCFAOcj9oAAAAAElFTkSuQmCC';
                $messages[$i]->sent = type_format($messages[$i]->sent, 3);
            }

        }

        for($i=0;$i<count($users);$i++){
            $users[$i]->user_picture = $users[$i]->user_picture != '' ? base_url() .'public/static/'.$users[$i]->user_picture : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAGJ0lEQVR4Xu2bZah1RRSGn8/uTuzCDhRBRRFbRDAxEQO7EwsTsRvEBgtbfxhYoIItdv+wCwwUu4Pnsvdl7nz7nLP3mTn3nBvr33fPxLvevWbNivmmMMFlygTXn0kCJi1ggjMweQRG0QBmB7YGNgZWB5YG5ij2/wH4CHgNeBx4APh5NLCNhgUsB5wA7ArMXFOpX4FbgfOA92vO6WpYLwlQ2TOBI4HpukIHfwEXA6cDv3e5RttpvSJgWeBeYNVMoF8Htgc+zLTe8DK9IGAN4BFggQqwHwO3AU8AbwPfFmPmA1Yu/INHZYmKuV8BmwNv5CQhNwF++WcqlP8EOB64C/i3gwLTADsX53+xaKwkrJfTEnIS4Jl/ocLsdWb7A780/HKzAdcCu0TzvCnWAf5ouF7l8JwEnA8cF+1yEXBsItBLCkcaLnMucGLiukPTcxHgVfdO5O398rtnACnG24GdgrW8HVYEPkhdPxcB1wP7BGA88zq1pmbfSh+DKAleNBjg8fBoJUkOAgSncwqDHM/tHUnIpp6sNd0S/FlyF0wlOQcBXluaeyledcvU8PZN+Zm28P6LBxO9Le5sulA4PgcBmuK+waLnACelgGozN3a0VwMHpuyVg4AXgbUDEAYrj6WAajN3S+Ch4PfngXVT9spBgNHcvAGIRYAvU0C1masT/Cz4/evCD3S9XQ4C/gSmDxDMCPi3Xohrh0mR+/i3rmWsE/BPQqY5RFoOAr4BTGZKGc0j4PGbv+vPn4mAfjrBlyIH3JiLHBZwDbBfsHO2OL1Cmwui3MK9D2isdTAhBwFGfeb4pRgGW+/rlPY2xW0gZN0wTJEHIhAybTUUniXQaLeIlKbKVo3fA7gp+MFQeKHU4mkOCxBTHA16V6+UCi5Q1uqxyZAOtpSrgINSmc1FgJUgAYbxgCmseUKqiNF4f8dgIYshKwDmHUmSiwBB6Pwse4VyKXA08F+XKMXnGodH888CTulyzRHTchIwE2BsbtMjFL+eydJPDQFr9tYZwi/vEi8XdcEs0WZOAgSn93+2Ij7/vGiOeCyM3tqJ3l4nalYZnnnneLOYeL3SkMyWw3MT4EarAY+2SFI+LQoltr/eAowiFaO5VYBNiopwXA0OFbBldllBUHLFqRcElJZwD2CPoFeiVR0C3JeyQa8IEJM+4TTgmOh2aIJXb38/sBkwZ4uJXsFHAL81Wbgc2y0BmuhRRT3uQsDubiuxPObt4LmetSZIzdygxwqQkaXzDLddx+AnFh3jtoBW0Ui6IcCz6hlfuNjJHuAONXY1Ytwqao/PBej0vi/CXJ2b/uHBFsVOiTi1ID+MOdz+C2BT4L0aWIaHNCXAZqd9vbACJOvtnFYTPHXHrlVUiA2GQtGpbgi8W3ehJgTYsPSej03QHr79/9EWy/E3A9tEGxuG2z+sdRzqEmCio/Jxuzs10kslzUbqFRWVYX3C+nXeFNQl4AZgzwitPTvD3EGQKytIuC6qU1TirEOADu7uaLZdHxOdbmP83KRpCTrj+Dj477ZxQicCjMf1qqXHF/ibRXvadzyDJPoEy3OhY/RmWL5d+6wTAXGGZwKyZvG6Y5CUL7GITV8VXpFnAye3AtuOABuPlqDCpucZxYOlQVS+xBS3z8wXlgye44zA3o4As7HwetOcLHz05LVWRkYNlnw34AcspaUVtCLAOF6F5wkWORjQ244F8Wmet1QpBkgGa1M9q2lFgNVWc/dSbEC4wKB//RKvVuAHDBMoX5j4SKvWEfBK2S4YacITv/8ZdEu4HDgsAKny4TOboZ+qLGAG4Lsoc9O7vjroGkf4fEn2XPA3M8y5gb/DcVUEGEI+FQzqR7KTg2t1s18R9g7NEUJSKi3AAoYmX8qNwF45EPVhDf2Y/qwUaxjmL8NSZQEWIuzClHJokXD0AX/ylr5RtJ9YijnN3p0IsKobPjvZCHgyGUp/FtgCeDjY+mlgg04EWLkNCxyWuo0Ix6KYB4QVIjtJS3UiQG9Zt3Y31kj5MS6uVvkAGxeml+NRvAJH1BKrCPDlVdKzkwFmzmtxREmvigCLCL68qPoPDwOsW0doKm9p3T5D22uw40rjaUCngsh40rVSl0kCxv0n7qDg/4zyCFAOcj9oAAAAAElFTkSuQmCC';
            $users[$i]->selected = $users[$i]->user_login == $this->uri->segment(3) ? ' active ' : '';
        }

        if(count($messages) == 0)
            $this->vet_dados['error'] = 'Nenhuma mensagem encontrada.';
        else
            $this->vet_dados['error'] = '';


        $this->vet_dados['actual'] = $this->uri->segment(3);
        $this->vet_dados['toUser'] = empty($toUser) ? array() : array($toUser);
        $this->vet_dados['messages'] = $messages;
        $this->vet_dados['usuarios'] = $users;
        $this->vet_dados['from_user'] = $entity->from_user;
        $this->vet_dados['to_user'] = $entity->to_user;


        return $this->parser->parse('messages/messages_home_view', $this->vet_dados, TRUE);

    }

    public function send(MessageEntity $entity)
    {
        $this->db->trans_begin();

        $this->db->set('from_user', $entity->from_user);
        $this->db->set('to_user', $entity->to_user);
        $this->db->set('content', $entity->content);
        $this->db->set('viewed', $entity->viewed);
        $this->db->set('sent', $entity->sent);

        $this->db->insert('mensagens');

        $entity->id = (int) $this->db->insert_id();

        if($entity->id > 0){
            $this->db->trans_commit();
            syslog::generate_log("CHAT_MESSAGE_SENT");
            return $entity;
        } else {
            $this->db->trans_rollback();
            syslog::generate_log("CHAT_MESSAGE_ERROR");
            return false;
        }

    }

    public function fetchMyUsers()
    {
        $this->db->from('usuario');

        if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO' && $this->session->userdata('USER_ROLE') != 'SUPORTE'){
            $this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id', 'LEFT');
            $this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));
            $this->db->or_where('usuario.user_type_id', 6);
        }

        if($this->session->userdata('USER_ROLE') == 'SUPORTE'){
            // $this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id', 'LEFT');
            $this->db->where_in('usuario.user_type_id', array(1, 2, 3, 4, 6));
        }

        $this->db->where('usuario.user_id <>' . $this->session->userdata('USER_ID'));


        $return =  $this->db->get()->result();
        return $return;
    }

    public function fetchAll(MessageEntity $entity)
    {
        $users = [
            $entity->from_user,
            $entity->to_user
        ];

        $this->db->select('mensagens.*, usuario.user_fullname, usuario.user_picture');
        $this->db->from('mensagens');
        $this->db->where_in('from_user', $users);
        $this->db->where_in('to_user', $users);

        $this->db->join('usuario', 'usuario.user_id = mensagens.from_user');

        $this->db->order_by('id', 'ASC');

        $this->db->limit(50);

        return $this->db->get()->result();
    }

    public function get_user_id($login)
    {
        if($login == '') return null;
        $this->db->select('user_id, user_fullname, user_login, user_picture');
        $this->db->from('usuario');
        $this->db->where('user_login', $login);

        return $this->db->get()->row();
    }

    public function get_new_messages()
    {
        $this->db->select('count(id) as total_nao_lidas');
        $this->db->from('mensagens');
        $this->db->where('to_user', $this->session->userdata('USER_ID'));
        $this->db->where('viewed', 0);

        return $this->db->get()->row();

    }

    public function set_readed_message(MessageEntity $entity)
    {

        $this->db->set('viewed', 1);
        $this->db->where('to_user', $entity->to_user);
        $this->db->where('from_user', $entity->from_user);

        if($this->db->update('mensagens')){
            syslog::generate_log("NEW_MESSAGE_READED");
            return true;
        }
        else{
            syslog::generate_log("UNABLE_READ_NEW_MESSAGE");
            return false;
        }



    }
}
