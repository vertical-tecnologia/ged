<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 16:48
 */

class servico_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('servico_status_model');
    }

    public function index()
    {
        $servicos = $this->fetchAll();

        for($i=0;$i<count($servicos);$i++){
            $servicos[$i]->link_del = $servicos[$i]->service_editable == 0 ? '' : anchor('servico/excluir/'. $servicos[$i]->service_id, 'Excluir', array('title' => 'Excluir Serviço'));
            $servicos[$i]->link_upd = anchor('servico/editar/'. $servicos[$i]->service_id, 'Editar', array('title' => 'Excluir Serviço'));
        }

        $this->vet_dados['servico'] = $servicos;

        $this->vet_dados['nome_tela'] = 'Gerenciar Serviços';
        $this->vet_dados['tela']      = 'servico';

        return $this->parser->parse('servico/servico_list_view', $this->vet_dados, TRUE);
    }

    public function cadastro()
    {

        $vet['service_id']          = set_value('service_id');
        $vet['service_name']        = set_value('service_name');
        $vet['service_desc']        = set_value('service_desc');
        $vet['service_price']       = set_value('service_price');
        $vet['service_recursive']   = set_value('service_recursive');
        $vet['service_storage']     = set_value('service_storage');
        $vet['status_id']           = set_value('status_id');

        $this->vet_dados['servico'] = array($vet);

        $this->vet_dados['service_status'] = $this->servico_status_model->fetchAll();

        $this->vet_dados['quantidades'] = $this->quantidades();

        $this->vet_dados['nome_tela'] = 'Gerenciar Serviços';
        $this->vet_dados['tela']      = 'servico';

        return $this->parser->parse('servico/servico_cad_view', $this->vet_dados, TRUE);
    }

    public function editar(ServicoEntity $servico)
    {
        $id = (int) $servico->service_id;

        $service = $this->fetch($id);
        $this->vet_dados['field_disabled'] = $service->service_editable == 1 ? '' : ' readonly ';

        if(!$service){
            return false;
        }
        $service->service_recursive_checked = $service->service_recursive == 1 ? 'checked' : '';

        $status = $this->servico_status_model->fetchAll();

        for($i=0;$i<count($status);$i++){
            $status[$i]->selected = $status[$i]->service_status_id == $service->service_status_id ? ' selected ' : '';
        }

        $quantidades = $this->quantidades();

        for($i=0;$i<count($quantidades);$i++){
            $quantidades[$i]->selected = $quantidades[$i]->quant_valor == $service->service_storage ? ' selected ' : '';
        }

        $this->vet_dados['servico'] = array($service);
        $this->vet_dados['service_status'] = $status;
        $this->vet_dados['quantidades'] = $quantidades;
        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Serviços';
        $this->vet_dados['tela']      = 'servico_status';

        return $this->parser->parse('servico/servico_cad_view', $this->vet_dados, TRUE);
    }

    public function fetchAll($params = null)
    {
        $this->db->select('servico.*, servico_status.service_status_name');
        $this->db->from('servico');
        $this->db->join('servico_status', 'servico_status.service_status_id = servico.service_status_id');

        if(isset($params['service_status_id'])){
            $this->db->where('servico.service_status_id', $params['service_status_id']);
        }

        return $this->db->get()->result();
    }

    public function fetchAllServiceUser($params = null)
    {
        $this->db->select('servico.*');
        $this->db->from('servico');
        $this->db->join('plano_servico', 'plano_servico.service_id = servico.service_id');


        if(isset($params['plan_id'])){
            $this->db->where('plano_servico.plan_id', $params['plan_id']);
        }

        return $this->db->get()->result();
    }

    public function fetch($id)
    {
        $id = (int) $id;
        $this->db->from('servico');
        $this->db->where('service_id', $id);
        return $this->db->get()->row();

    }

    public function save(ServicoEntity $servico)
    {
        if($this->valida() === FALSE)
            return $this->cadastro();

        $data = [
                'service_name'      => $servico->service_name,
                'service_desc'      => $servico->service_desc,
                'service_price'     => $servico->service_price,
                'service_recursive' => $servico->service_recursive,
                'service_storage'   => $servico->service_storage,
                'service_status_id' => $servico->service_status_id
        ];

        $id = (int) $servico->service_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('servico');
            $servico->service_id = (int) $this->db->insert_id();
            if($servico->service_id == 0){
                syslog::generate_log('NEW_SERVICE_SUCCESS');
                return false;
            } else {
                syslog::generate_log('NEW_SERVICE_ERROR');
                return true;
            }
        } else {
            $this->setFields($data);
            $this->db->where('service_id', $id);
            $this->db->update('servico');
            $count = $this->db->affected_rows();
            if($count == 0){
                syslog::generate_log('UPDATE_SERVICE_SUCCESS');
                return false;
            } else {
                syslog::generate_log('UPDATE_SERVICE_ERROR');
                return true;
            }
        }

    }

    public function delete(ServicoEntity $servico)
    {
        $id = (int) $servico->service_id;

        $service = $this->fetch($id);

        if($service->service_editable == 0){
            syslog::generate_log('TRY_UPDATE_LOCKED_SERVICE');
            redirect('servico');
        }

        $this->db->where('service_id', $servico->service_id);
        if($this->db->delete('servico')){
            syslog::generate_log('DELETE_SERVICE_SUCCESS');
            return true;
        } else {
            syslog::generate_log('DELETE_SERVICE_ERROR');
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            $this->db->set($key, $value);
        }
    }

    protected function quantidades()
    {
        $this->db->from('quantidade_armazenamento');
        return $this->db->get()->result();
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('servico');
    }

}