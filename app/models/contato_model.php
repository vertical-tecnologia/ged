<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 15/06/15
 * Time: 14:36
 */

class contato_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function fetchAll(ContatoEntity $entity)
    {
        $this->db->from('contato');
        $this->db->join('contato_tipo', 'contato.tipo_id = contato_tipo.tipo_id');

        if($entity->contato_empresa_id != ''){
            $this->db->join('empresa_contato', 'contato.contato_id = empresa_contato.contato_id');
            $this->db->where('empresa_contato.empresa_id', $entity->contato_empresa_id);
        }

        if($entity->contato_user_id != ''){
            $this->db->join('usuario_contato', 'contato.contato_id = usuario_contato.contato_id');
            $this->db->where('usuario_contato.usuario_id', $entity->contato_user_id);
        }

        if($entity->tipo_id != '')
            $this->db->where('tipo_id', $entity->tipo_id);

        if($entity->contato_id != '')
            $this->db->where('contato_id', $entity->contato_id);

        return $this->db->get()->result();
    }

    public function fetch(ContatoEntity $entity)
    {
        $this->db->from('contato');
        $this->db->join('contato_tipo', 'contato.tipo_id = contato_tipo.tipo_id');
        $this->db->join('empresa_contato', 'contato.contato_id = empresa_contato.contato_id', 'LEFT');
        $this->db->join('usuario_contato', 'usuario_contato.contato_id = contato.contato_id', 'LEFT');

        if($entity->tipo_id != '')
            $this->db->where('contato.tipo_id', $entity->tipo_id);

        if($entity->contato_id != '')
            $this->db->where('contato.contato_id', $entity->contato_id);

        return $this->db->get()->row();
    }

    public function save(ContatoEntity $entity)
    {

        $this->db->trans_begin();
        $id = (int) $entity->contato_id;
        $this->setFields($entity);

        if($id == 0){
            $this->db->insert('contato');
            $entity->contato_id = $this->db->insert_id();

            if($entity->contato_id > 0){
                syslog::generate_log('NEW_CONTACT_SUCCESS');
                return $entity;
            }
            else{
                syslog::generate_log('NEW_CONTACT_ERROR');
                $this->db->trans_rollback();
                return false;
            }
        } else {
            $this->db->where('contato_id', $entity->contato_id);
            if($this->db->update('contato')){
                syslog::generate_log('UPDATE_CONTACT_SUCCESS');
                return true;
            } else {
                syslog::generate_log('UPDATE_CONTACT_ERROR');
                $this->db->trans_rollback();
                return false;
            }

        }
    }

    public function save_usuario(ContatoUsuarioEntity $entity)
    {
        $this->db->set('contato_id', $entity->contato_id);
        $this->db->set('usuario_id', $entity->usuario_id);

        $this->db->insert('usuario_contato');

        if($this->db->insert_id() > 0){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function save_empresa(ContatoEmpresaEntity $entity)
    {
        $this->db->set('contato_id', $entity->contato_id);
        $this->db->set('empresa_id', $entity->empresa_id);
        $this->db->set('principal', $entity->principal);

        $this->db->insert('empresa_contato');

        if($this->db->insert_id() > 0){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function excluir(ContatoEntity $entity)
    {
        $id = (int) $entity->contato_id;
        if($id > 0){
            $this->db->where('contato_id', $id);
            if($this->db->delete('contato')){
                return true;
            } else {
                return false;
            };
        } else
            return false;
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'contato_id' || $value == null || $value == '' || $key == 'empresa_endereco_id') continue;
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('endereco');
    }
}