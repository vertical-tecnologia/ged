<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 02:37
 */
class grupo_model extends CI_Model
{

	public $group_id;
	public $group_name;
	public $group_desc;
	public $group_admin;

	public $vet_dados;

	public function __construct()
	{
		parent::__construct();
		$this->vet_dados = base_dir($this->session->userdata('skin'));
	}

	public function index()
	{
		$grupo = $this->fetchAll();

		for ($i = 0; $i < count($grupo); $i++) {
			$grupo[$i]->link_del = $grupo[$i]->group_default == 1 ? '' : anchor('grupo/excluir/' . $grupo[$i]->group_id, 'Excluir', array('title' => 'Excluir Grupo', 'class' => 'btn btn-danger btn-xs'));
			$grupo[$i]->link_upd = $grupo[$i]->group_default == 1 ? '' : anchor('grupo/editar/' . $grupo[$i]->group_id, 'Editar', array('title' => 'Editar Grupo', 'class' => 'btn btn-success btn-xs'));
			$grupo[$i]->link_usr = anchor('grupo/usuarios/' . $grupo[$i]->group_id, 'Usuários', array('title' => 'Usuários do Grupo', 'class' => 'btn btn-default btn-xs'));
			$grupo[$i]->link_per = anchor('grupo/permissoes/' . $grupo[$i]->group_id, 'Permissões', array('title' => 'Permissões do Grupo', 'class' => 'btn btn-default btn-xs'));
		}

		$this->vet_dados['grupo'] = $grupo;

		$this->vet_dados['nome_tela'] = 'Gerenciar Status de Usuários';
		$this->vet_dados['tela'] = 'usuario_status';

		return $this->parser->parse('grupo/grupo_list_view', $this->vet_dados, TRUE);
	}

	public function cadastro()
	{

		$vet['group_id'] = set_value('group_id');
		$vet['group_name'] = set_value('group_name');

		$this->vet_dados['grupo'] = array($vet);

		$this->vet_dados['nome_tela'] = 'Gerenciar Status de Usuários';
		$this->vet_dados['tela'] = 'grupo';

		return $this->parser->parse('grupo/grupo_cad_view', $this->vet_dados, TRUE);

	}


	public function permissao()
	{
		$entity = new GroupEntity();
		$entity->group_id = $this->uri->segment(3);
		$group = $this->fetch($entity);

		if ($this->session->userdata('USER_ID') != $group->group_owner) {
			$this->session->set_flashdata('error', 'Voce nao tem permissao para alterar esse grupo.');
			redirect('grupo');
		}

		$this->vet_dados['grupo'] = array($group);

		$this->vet_dados['nome_tela'] = 'Gerenciar Status de Usuários';
		$this->vet_dados['tela'] = 'grupo';

		$this->load->model('servidor_model');
		$servers = $this->servidor_model->fetchAll(new ServerEntity(), '', array('enable' => SERVER_ATIVO));

		$default = new ServerEntity();
		$servers = array_merge([$default], $servers);
		$arrHtmlDirs = array();
		foreach ($servers as $server) {
			$arrHtmlDirs[$server->server_id]['html'] = $this->get_child_dir(null, $server->server_id, false);
			$description = !empty($server->description) ? $server->description : $server->server_address;
			$arrHtmlDirs[$server->server_id]['description'] = $description;
		}

		$this->load->vars('arrHtmlDirs', $arrHtmlDirs);

		// $this->vet_dados['objects'] = $this->get_child_dir();
		return $this->parser->parse('grupo/grupo_permissao_view', $this->vet_dados, TRUE);
	}

	public function edit(GroupEntity $entity)
	{

		$group = $this->fetch($entity);

		if ($this->session->userdata('USER_ID') != $group->group_owner) {
			$this->session->set_flashdata('error', 'Voce nao tem permissao para alterar esse grupo.');
			redirect('grupo');
		}

		$vet['group_id'] = $group->group_id;
		$vet['group_name'] = $group->group_name;

		$this->vet_dados['grupo'] = array($vet);

		$this->vet_dados['nome_tela'] = 'Gerenciar Grupos Usuários';
		$this->vet_dados['tela'] = 'grupo';

		return $this->parser->parse('grupo/grupo_cad_view', $this->vet_dados, TRUE);
	}

	public function usuarios(GroupEntity $entity)
	{
		$group = $this->grupo_model->fetch($entity);

		if ($this->session->userdata('USER_ID') != $group->group_owner) {
			$this->session->set_flashdata('error', 'Voce nao tem permissao para alterar esse grupo.');
			redirect('grupo');
		}

		$this->vet_dados['grupo'] = array($group);
		$usuarios = $this->get_usuarios();
		$this->vet_dados['usuario'] = $usuarios;

		$grupoEntity = new GroupEntity();
		$grupoEntity->group_id = $group->group_id;
		$usuarios_grupo = $this->get_usuarios_grupo($grupoEntity);


		for ($i = 0; $i < count($usuarios); $i++) {

			for ($j = 0; $j < count($usuarios_grupo); $j++) {
				if ($usuarios_grupo[$j]->user_id == $usuarios[$i]->user_id)
					$usuarios[$i]->selected = ' selected ';
			}
		}

		for ($i = 0; $i < count($usuarios); $i++) {

			if (!isset($usuarios[$i]->selected))
				$usuarios[$i]->selected = '';
		}

		return $this->parser->parse('grupo/grupo_usuarios_cad_view', $this->vet_dados, TRUE);

	}

	public function fetch(GroupEntity $entity)
	{
		$this->db->from('group');
		$this->db->where('group_id', (int)$entity->group_id);

		return $this->db->get()->row();
	}

	public function fetchAll()
	{
		$this->db->select('group.*, usuario.user_fullname as group_owner_name');
		$this->db->from('group');
		$this->db->join('usuario', 'usuario.user_id = group.group_owner');

		if (in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN', 'USUARIO'))) {
			$this->db->where('group.empresa_id', $this->session->userdata('EID'));
		}

		$this->db->where('group.group_owner', $this->session->userdata('USER_ID'));

		return $this->db->get()->result();
	}

	public function save(GroupEntity $entity)
	{
		if ($this->valida() === FALSE)
			return $this->cadastro();

		$data = [
			'group_name' => $entity->group_name,
			'empresa_id' => $entity->empresa_id,
			'group_owner' => $entity->group_owner,
		];

		$id = (int)$entity->group_id;

		if ($id == 0) {

			$this->setFields($data);

			$this->db->insert('group');

			$id = $this->db->insert_id();

			if ($this->db->trans_status() && $id > 0) {
				syslog::generate_log('NEW_GROUP_SUCCESS');
				$this->db->trans_commit();
				return TRUE;

			} else {
				$this->db->trans_rollback();
				syslog::generate_log('NEW_GROUP_ERROR');
				throw new Exception($this->lang->line('zorbit_erro_inserir'));

			}

		} else {

			$this->setFields($data);
			$this->db->where('group_id', $id);
			$this->db->update('group');


			if ($this->db->trans_status()) {

				$this->db->trans_commit();
				syslog::generate_log('UPDATE_GROUP_SUCCESS');
				return TRUE;

			} else {
				$this->db->trans_rollback();
				syslog::generate_log('UPDATE_GROUP_ERROR');
				throw new Exception($this->lang->line('zorbit_erro_inserir'));

			}
		}
	}

	public function insert($data)
	{
		$this->db->insert_batch('group_users', $data);

		if ($this->db->affected_rows() != 0) {
			$this->db->trans_commit();
			return true;
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}

	public function delete_before(GroupUserEntity $entity)
	{
		$this->db->trans_begin();

		$this->db->where('group_id', $entity->group_id);
		if ($this->db->delete('group_users'))
			return true;
		else
			return false;

	}

	public function save_usuarios(GroupUserEntity $entity)
	{
		if (true == $this->delete_before($entity)) {
			$data = array();
			if (is_array($entity->user_id)) {
				for ($i = 0; $i < count($entity->user_id); $i++) {
					$data[] = array('user_id' => $entity->user_id[$i], 'group_id' => $entity->group_id);
				}
				if ($this->insert($data)){
					syslog::generate_log('GROUP_MEMBERS_UPDATE_SUCCESS');
					return true;
				}
				else{
					syslog::generate_log('GROUP_MEMBERS_UPDATE_ERROR');
					return false;
				}
			}

			$this->db->trans_commit();
			return true;


		} else {
			syslog::generate_log('GROUP_MEMBERS_UPDATE_ERROR');
			return false;
		}

	}

	public function get_usuarios_grupo(GroupEntity $entity)
	{
		$this->db->select('user_id');
		$this->db->from('group_users');
		$this->db->where('group_id', $entity->group_id);

		return $this->db->get()->result();
	}

	public function get_usuarios()
	{
		$this->db->from('usuario');
		$this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id');
		$this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));

		return $this->db->get()->result();
	}

	public function delete($id)
	{
		$entity = new GroupEntity();
		$entity->group_id = $id;
		$group = $this->grupo_model->fetch($entity);

		if ($this->session->userdata('USER_ID') != $group->group_owner) {
			$this->session->set_flashdata('error', 'Voce nao tem permissao para alterar esse grupo.');
			redirect('grupo');
			die;
		}


		$this->db->where('group_id', $id);
		if($this->db->delete('group')){
			syslog::generate_log('DELETE_GROUP_SUCCESS');
			return true;
		} else {
			syslog::generate_log('DELETE_GROUP_ERROR');
			return false;
		}
	}

	protected function setFields($fields)
	{
		foreach ($fields as $key => $value) {
			if ($value != null && $value != '')
				$this->db->set($key, $value);
		}
	}

	private function valida()
	{
		$this->form_validation->set_message('required', 'Campo obrigatório');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		return $this->form_validation->run('grupo');
	}

	private function getObjects()
	{
		$this->db->from('file_object');
		$this->db->where('type', 2);

		return $this->db->get()->result();
	}

	public function get_child_dir($diretorio = null, $server = null, $loop = true)
	{

		$html = "";

		$query = "SELECT * FROM `file_object` WHERE type = 2 ";

		$query .= " AND `owner` = '" . $this->session->userdata('USER_KEY') . "' ";
		if (is_null($server) && $this->session->userdata('SRID') == SERVER_LOCAL)
			$query .= ' AND server_id = 1';
		else
			$query .= !is_null($server) ? " AND server_id = {$server}" : " AND server_id IS NULL";

		if (isset($diretorio->parent) && $diretorio->parent != '')
			$query .= " AND `parent` = '" . $diretorio->parent . "' ";
		else 
			$query .= " AND `parent` IS NULL";

		if (isset($diretorio->storage_key) && $diretorio->storage_key)
			$query .= " AND `storage_key` = '" . $diretorio->storage_key . "' ORDER BY `type` ASC";

		$exec = $this->db->query($query);

		if ($loop)
			$html = "<ul>";

		foreach ($exec->result() as $row) {

			$html .= "<li id=\"$row->id\" class=\"folder\" data-href=\"" . base_url() . "grupo/folder_permissions\" data-callback=\"load_folder_permissions\" >";
			$html .= $row->original_name;
			@$diretorio->parent = $row->id;
			$diretorio->storage_key = $row->storage_key;
			$html .= $this->get_child_dir($diretorio, $server);
			$html .= "</li>";
		}

		if ($loop)
			$html .= "</ul>";

		$html .= "";
		return $html;
	}

	public function get_folder_permissions(GroupPermissionEntity $entity, $create = false)
	{
		$this->db->from('group_permission');
		$this->db->where('object_id', $entity->object_id);
		$this->db->where('group_id', $entity->group_id);

		$row = $this->db->get()->row();
// echo $this->db->last_query();
// var_dump($row); exit;
		// if (count($row) == 0)
		// 	return $this->create_permission($entity);
		// else
			return $row;
	}

	public function create_permission(GroupPermissionEntity $entity)
	{

		$this->db->set('group_id', $entity->group_id);
		$this->db->set('object_id', $entity->object_id);
		$this->db->insert('group_permission');

		$entity->permission_id = (int)$this->db->insert_id();

		if ($entity->permission_id > 0){
			syslog::generate_log("GROUP_FOLDER_PERMISSIONS_CREATE_SUCCESS");
			return $entity;
		}
		else{
			syslog::generate_log("GROUP_FOLDER_PERMISSIONS_CREATE_ERROR");
			return false;
		}
	}

	public function update_permission(GroupPermissionEntity $entity)
	{
		$this->db->set('permission_id', $entity->permission_id);
		$this->db->set('group_id', $entity->group_id);
		$this->db->set('object_id', $entity->object_id);
		$this->db->set('readable', $entity->readable);
		$this->db->set('listable', $entity->listable);
		$this->db->set('writable', $entity->writable);
		$this->db->set('deletable', $entity->deletable);
		$hasNoPermission = $entity->readable == false && $entity->listable == false && $entity->writable == false && $entity->deletable == false;

		if($entity){
			if ($hasNoPermission == false) {
				if($entity->permission_id != ""){

					$this->db->where('permission_id', $entity->permission_id);
					$this->db->update('group_permission');
					$entityAll = $this->verifyParent($entity);

					if(!is_null($entityAll)){
						foreach ($entityAll as $key => $value) {
							$this->update_permission($value);
						}
					}
				}
				if($entity->permission_id == ""){	
					$this->db->insert('group_permission');
					$entityAll = $this->verifyParent($entity);
					if(!is_null($entityAll)){
						foreach ($entityAll as $key => $value) {
							$this->update_permission($value);
						}
					}
				}
			}
			if($hasNoPermission){
				$entityAll = $this->verifyParent($entity);
				foreach ($entityAll as $key => $value) {
					$this->update_permission($entityAll[$key]);
				}
				$this->db->where('permission_id', $entity->permission_id);
				$this->db->delete('group_permission');
			}

			syslog::generate_log("GROUP_FOLDER_PERMISSIONS_UPDATE_SUCCESS");
			return $entity;
		}
		else{
			syslog::generate_log("GROUP_FOLDER_PERMISSIONS_UPDATE_ERROR");
			return false;
		}

	}

	private function verifyParent($entityMain)
	{
		$this->db->select('*');
		$this->db->from('file_object');
		$this->db->where('parent', $entityMain->object_id);
		$result = $this->db->get()->result();

		// $permissionId = $this->verifyPermissionId($result);
		$entity = array();
		if(!is_null($result)){
			foreach ($result as $key => $value) {
		        $entity[$key] = new GroupPermissionEntity();
				$permission = $this->verifyPermissionId($value);
				$entity[$key]->permission_id = ($permission) ? $permission[0]->permission_id : '';
				$entity[$key]->group_id = $entityMain->group_id;
				$entity[$key]->object_id = $result[$key]->id;
				$entity[$key]->deletable = $entityMain->deletable;
				$entity[$key]->writable = $entityMain->writable;
				$entity[$key]->listable = $entityMain->listable;
				$entity[$key]->readable = $entityMain->readable;

			}
			return $entity;
		}

		return null;
	}

	private function verifyPermissionId($entity)
	{
		// var_dump($entity);exit;
		$this->db->select('*');
		$this->db->from('group_permission');
		$this->db->where('object_id', $entity->id);
		$result = $this->db->get()->result();

		if(!$result)
			return false;

		return $result;
	}
}