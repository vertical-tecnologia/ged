<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 22/07/15
 * Time: 15:49
 */

class relatorios_model extends CI_Model{


    public function __construct()
    {
        parent::__construct();
    }

    public function get_usuarios_mes()
    {

        $this->db->select('MONTH(user_register_date) as mes ,count(MONTH(user_register_date)) as total');
        $this->db->from('usuario');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id');
            $this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));
            $this->db->where('user_type_id <>', 1);
        }

        $this->db->group_by('MONTH(user_register_date)');

        $this->db->limit('8');

        return $this->db->get()->result();
    }

    public function total_arquivos_mes()
    {
        $this->db->select('MONTH(created) as mes ,count(MONTH(created)) as total');
        $this->db->from('file_object');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
        }

        if($this->session->userdata('USER_ROLE') == 'USUARIO'){
            $this->db->where('owner', $this->session->userdata('USER_KEY'));
        }
        $this->db->limit('8');

        return $this->db->get()->result();

    }

    public function arquivos_usuarios()
    {
        $this->db->select('COUNT(id) as total, owner');
        $this->db->from('file_object');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
        }

        if($this->session->userdata('USER_ROLE') == 'USUARIO'){
            $this->db->where('owner', $this->session->userdata('USER_KEY'));
        }

        $this->db->group_by('owner');

        $this->db->limit('8');

        return $this->db->get()->result();

    }

    public function arquivos_tipo()
    {
        $this->db->select('COUNT(mime) as total, owner, file_type.desc_file_type');
        $this->db->from('file_object');
        $this->db->join('file_type', 'file_type.id_file_type = file_object.mime');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->where('storage_key', $this->session->userdata('STR_DRIVER'));
        }

        if($this->session->userdata('USER_ROLE') == 'USUARIO'){
            $this->db->where('owner', $this->session->userdata('USER_KEY'));
        }
        $this->db->where('type', 1);

        $this->db->group_by('mime');

        $this->db->limit('8');

        return $this->db->get()->result();

    }

    public function arquivos_empresas()
    {
        $this->db->select('COUNT(storage_key) as total, owner, empresa.empresa_fantasia');
        $this->db->from('file_object');
        $this->db->join('cliente', 'cliente.client_volume = file_object.storage_key');
        $this->db->join('empresa', 'cliente.client_empresa_id = empresa.empresa_id');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->where('file_object.storage_key', $this->session->userdata('STR_DRIVER'));
        }

        if($this->session->userdata('USER_ROLE') == 'USUARIO'){
            $this->db->where('file_object.owner', $this->session->userdata('USER_KEY'));
        }
        $this->db->where('type', 1);

        $this->db->group_by('storage_key');

        $this->db->limit('8');

        return $this->db->get()->result();

    }

    public function top_usuarios()
    {
        $this->db->select('COUNT(file_object.owner) as total, usuario.user_fullname');
        $this->db->from('file_object');
        $this->db->join('usuario', 'usuario.user_key = file_object.owner');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->where('file_object.storage_key', $this->session->userdata('STR_DRIVER'));
        }

        if($this->session->userdata('USER_ROLE') == 'USUARIO'){
            $this->db->where('file_object.owner', $this->session->userdata('USER_KEY'));
        }
        $this->db->where('type', 1);

        $this->db->group_by('file_object.owner');

        $this->db->limit('8');

        return $this->db->get()->result();

    }
}