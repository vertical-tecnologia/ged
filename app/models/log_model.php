<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 20/08/15
 * Time: 23:54
 */
class log_model extends CI_Model
{
    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('segmento_status_model');
        $this->load->model('segmento_diretorio_model');
    }

    public function index()
    {
        $log = $this->fetchAll();

        if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
            if ($this->uri->segment(2) == 'clientes') {
                $this->vet_dados['filter_link'] = anchor('log/', 'Ver logs administrativos', array('class' => 'btn btn-warning'));
            } else {
                $this->vet_dados['filter_link'] = anchor('log/clientes', 'Ver log de clientes', array('class' => 'btn btn-warning '));
            }
        } else {
            $this->vet_dados['filter_link'] = null;
        }


        for($i=0;$i<count($log);$i++){
            $log[$i]->datetime = data_formatada($log[$i]->datetime, 2);
            $log[$i]->operation = $this->lang->line($log[$i]->operation) != '' ? $this->lang->line($log[$i]->operation) : $log[$i]->operation;
        }

        $this->vet_dados['nome_tela'] = 'Gerenciar Ações';
        $this->vet_dados['tela']      = 'acao';
        $this->vet_dados['log'] = $log;

        return $this->parser->parse('log/log_list_view', $this->vet_dados, TRUE);
    }

    public function fetchAll()
    {
        $this->db->from('log_history');

        if ($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO')
            $this->db->where('client_id', $this->session->userdata('CLID'));

        if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
            if ($this->uri->segment(2) == 'clientes') {
                $this->db->where('client_id !=', 0);
            } else
                $this->db->where('client_id', 0);
        }

        return $this->db->get()->result();
    }
}
