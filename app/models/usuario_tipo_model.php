<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 16:48
 */

class usuario_tipo_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $tipos = $this->fetchAll();

        for($i=0;$i<count($tipos);$i++){
            $tipos[$i]->link_del = $tipos[$i]->type_editable == 0 ? '' : anchor('usuario_tipo/excluir/'. $tipos[$i]->type_id, 'Excluir', array('title' => 'Excluir Tipo'));
            $tipos[$i]->link_upd = $tipos[$i]->type_editable == 0 ? '' : anchor('usuario_tipo/editar/'. $tipos[$i]->type_id, 'Editar', array('title' => 'Excluir Tipo'));
        }

        $this->vet_dados['usuario_tipo'] = $tipos;



        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos Usuários';
        $this->vet_dados['tela']      = 'usuario_tipo';

        return $this->parser->parse('usuario_tipo/usuario_tipo_list_view', $this->vet_dados, TRUE);
    }

    public function cadastro()
    {

        $vet['type_id']        = set_value('type_id');
        $vet['type_name']        = set_value('type_name');
        $vet['type_desc']        = set_value('type_desc');

        $this->vet_dados['usuario_tipo'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos Usuários';
        $this->vet_dados['tela']      = 'usuario_tipo';

        return $this->parser->parse('usuario_tipo/usuario_tipo_cad_view', $this->vet_dados, TRUE);
    }

    public function editar(UsuarioTipo $usuarioTipo)
    {
        $user = $this->fetch($usuarioTipo);

        if(!$user || $user->type_editable == 0){
            $this->session->set_flashdata('error', 'Você não tem permissão para alterar esse registro ou ele não existe.');
            redirect('usuario_tipo');
        }
        $vet = array();

        $vet['type_id']        = $user->type_id;
        $vet['type_name']      = $user->type_name;
        $vet['type_desc']      = $user->type_desc;

        $this->vet_dados['usuario_tipo'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos Usuários';
        $this->vet_dados['tela']      = 'usuario_tipo';

        return $this->parser->parse('usuario_tipo/usuario_tipo_cad_view', $this->vet_dados, TRUE);
    }


    public function fetchAll()
    {
        $this->db->from('usuario_tipo');

        if($this->session->userdata('USER_ROLE') != 'MASTER')
            $this->db->where('type_has_permissions', TRUE);

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN')
            $this->db->where_in('type_id', array(4, 2));

        return $this->db->get()->result();
    }

    public function fetch(UsuarioTipo $entity)
    {
        $id = (int) $entity->type_id;

        $this->db->from('usuario_tipo');
        $this->db->where('type_id', $id);
        return $this->db->get()->row();

    }

    public function save(UsuarioTipo $usuarioTipo)
    {
        if($this->valida() === FALSE)
            return $this->cadastro();
        $data = [
                'type_name'     => $usuarioTipo->type_name ,
                'type_desc'     => $usuarioTipo->type_desc ,
                'type_editable' => $usuarioTipo->type_editable == '' ? 0 : $usuarioTipo->type_editable,
        ];

        $id = (int) $usuarioTipo->type_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('usuario_tipo');
            $usuarioTipo->type_id = (int) $this->db->insert_id();

            if($usuarioTipo->type_id == 0){
                syslog::generate_log("NEW_USERTYPE_SUCCESS");
                return FALSE;
            } else {
                syslog::generate_log("NEW_USERTYPE_ERROR");
                return TRUE;
            }

        } else {
            $this->setFields($data);
            $this->db->where('type_id', $id);
            $this->db->update('usuario_tipo');
            $count = $this->db->affected_rows();

            if($count == 0){
                syslog::generate_log("UPDATE_USERTYPE_SUCCESS");
                return FALSE;
            } else {
                syslog::generate_log("UPDATE_USERTYPE_ERROR");
                return TRUE;
            }
        }

    }

    public function delete(UsuarioTipo $usuarioTipo)
    {

        $user = $this->fetch($usuarioTipo);

        if(!$user || $user->type_editable == 0){
            $this->session->set_flashdata('error', 'Você não tem permissão para alterar esse registro ou ele não existe.');
            redirect('usuario_tipo');
        }

        $this->db->where('type_id', $usuarioTipo->type_id);
        if($this->db->delete('usuario_tipo')){
            syslog::generate_log("DELETE_USERTYPE_SUCCESS");
            return true;
        } else {
            syslog::generate_log("DELETE_USERTYPE_ERROR");
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('usuario_tipo');
    }
}