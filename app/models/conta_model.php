<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 22/07/15
 * Time: 10:45
 */

class conta_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('usuario_model');
    }

    public function encerrar()
    {
        $entity = new UsuarioEntity();
        $entity->user_id = $this->session->userdata('USER_ID');
        $user = $this->usuario_model->get_userdata($entity);

        $user->user_register_date = data_formatada($user->user_register_date, 4);

        $this->vet_dados['usuario'] = array($user);

        return $this->parser->parse('conta/encerrar_view', $this->vet_dados, TRUE);
    }

    public function finish()
    {
        $entity = new UsuarioEntity();
        $entity->user_id = $this->session->userdata('USER_ID');
        $user = $this->usuario_model->get_userdata($entity);

        if($this->encrypt->decode($user->user_password) == $this->input->post('password')){
            $this->session->set_flashdata('success', 'Conta encerrada.');
            $users = $this->get_users();

            if($this->update_users($users, 1)){

                if($this->update_users(array($entity), 8)){
                    $this->session->set_flashdata('success', 'Conta encerrada com sucesso.');
                    $this->session->sess_destroy();
                    redirect('cadastro/');
                    syslog::generate_log('ACCOUNT_FINISHED');
                    return true;
                } else {
                    $this->session->set_flashdata('error', 'Erro ao tentar encerrar conta.');
                    redirect('conta/');
                    syslog::generate_log('ACCOUNT_FINISH_ERROR');
                    return false;
                }
            } else {
                $this->session->set_flashdata('error', 'Erro ao tentar encerrar conta.');
                syslog::generate_log('ACCOUNT_FINISH_ERROR_USERS');
                redirect('conta/');
                return false;
            }
            return true;
        } else {
            $this->session->set_flashdata('error', 'Senha inválida.');
            syslog::generate_log('ACCOUNT_FINISH_ERROR_INVALID_PASSWORD');
            redirect('conta/encerrar');
            return false;
        }

    }

    public function update_users($data, $value)
    {
        for($i=0;$i<count($data);$i++){
            $this->db->set('user_status_id', $value);
            $this->db->where('user_id', $data[$i]->user_id);
            if(!$this->db->update('usuario'))
                return false;
        }
        return true;
    }

    public function get_users()
    {
        $this->db->select('usuario.user_id');
        $this->db->from('usuario');
        $this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id');
        $this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));

        return $this->db->get()->result();
    }

}