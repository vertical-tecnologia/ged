<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 05/09/15
 * Time: 21:46
 */
class download_model extends CI_Model
{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));

    }

    public function index()
    {
        $exec = $this->get_executable();

        $this->vet_dados['instrucoes'] = $exec->exe_instructions;

        $this->vet_dados['path'] = $exec->exe_path;
        $this->vet_dados['linux_path'] = $exec->linux_path;
        $this->vet_dados['macos_path'] = $exec->macos_path;

        return $this->parser->parse('download/download_view', $this->vet_dados, TRUE);
    }

    public function windows()
    {
        syslog::generate_log('WINDOWS_CLIENT_DOWNLOAD');
        $this->download_file('exe_path');
    }

    public function macos()
    {
        syslog::generate_log('OSX_CLIENT_DOWNLOAD');
        $this->download_file('macos_path');
    }

    public function linux()
    {
        syslog::generate_log('LINUX_CLIENT_DOWNLOAD');
        $this->download_file('linux_path');
    }

    public function download_file($param)
    {
        $file = $this->get_executable();
        if(file_exists($file->$param)) {

            $fileName = basename($file->$param);
            $fileSize = filesize($file->$param);

            header("Content-Type: application/stream");
            header("Content-Type: application/force-download");
            header("Content-Type: application/download");
            header("Content-Type: application/octet-stream");

            header("Content-Length: ".$fileSize);
            header("Content-Disposition: attachment; filename=".$fileName);

            readfile ($file->$param);
            exit();
        }
        return;
    }

    public function get_executable()
    {
        $this->db->select('compania.exe_path , compania.exe_instructions, compania.linux_path, compania.macos_path');
        $this->db->from('compania');

        return $this->db->get()->row();
    }
}