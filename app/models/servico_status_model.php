<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 16:48
 */

class servico_status_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $servico_status = $this->fetchAll();

        for($i = 0;$i < count($servico_status);$i++){
            $servico_status[$i]->link_del = $servico_status[$i]->service_status_editable == 0 ? '' : anchor('servico_status/excluir/' . $servico_status[$i]->service_status_id, 'Excluir', array('title' => 'Excluir Status do Serviço'));
            $servico_status[$i]->link_upd = anchor('servico_status/editar/' . $servico_status[$i]->service_status_id, 'Editar', array('title' => 'Editar Status do Serviço'));
        }

        $this->vet_dados['servico_status'] = $servico_status;

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos de Serviço';
        $this->vet_dados['tela']      = 'servico_status';

        return $this->parser->parse('servico_status/servico_status_list_view', $this->vet_dados, TRUE);
    }

    public function cadastro()
    {

        $vet['service_status_id']          = set_value('service_status_id');
        $vet['service_status_name']        = set_value('service_status_name');

        $this->vet_dados['servico_status'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos de Serviços';
        $this->vet_dados['tela']      = 'servico_status';

        return $this->parser->parse('servico_status/servico_status_cad_view', $this->vet_dados, TRUE);
    }

    public function editar(ServicoStatus $servicoStatus)
    {
        $id = (int) $servicoStatus->service_status_id;

        $status = $this->fetch($id);

        if($status->service_status_editable == 0)
        {
            redirect('servico_status');
            return;
        }

        if(!$status){
            return false;
        }
        $vet = array();

        $vet['service_status_id']        = $status->service_status_id;
        $vet['service_status_name']      = $status->service_status_name;


        $this->vet_dados['servico_status'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Serviços';
        $this->vet_dados['tela']      = 'servico_status';

        return $this->parser->parse('servico_status/servico_status_cad_view', $this->vet_dados, TRUE);
    }


    public function fetchAll()
    {
        $this->db->from('servico_status');
        return $this->db->get()->result();
    }

    public function fetch($id)
    {
        $id = (int) $id;
        $this->db->from('servico_status');
        $this->db->where('service_status_id', $id);
        return $this->db->get()->row();

    }

    public function save(ServicoStatus $servicoStatus)
    {
        if($this->valida() === FALSE){
            return $this->cadastro();
        }

        $data = [
                'service_status_name'     => $servicoStatus->service_status_name,
        ];

        $id = (int) $servicoStatus->service_status_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('servico_status');
            $servicoStatus->service_status_id = (int) $this->db->insert_id();
            if($servicoStatus->service_status_id == 0){
                syslog::generate_log('NEW_SERVICE_STATUS_SUCCESS');
                return false;
            } else {
                syslog::generate_log('NEW_SERVICE_STATUS_ERROR');
                return true;
            }
        } else {
            $this->setFields($data);
            $this->db->where('service_status_id', $id);
            if($this->db->update('servico_status')){
                syslog::generate_log('UPDATE_SERVICE_STATUS_ERROR');
                return false;
            } else {
                syslog::generate_log('UPDATE_SERVICE_STATUS_SUCCESS');
                return true;
            }
        }
    }

    public function delete(ServicoStatus $servicoStatus)
    {
        $id = (int) $servicoStatus->service_status_id;

        $status = $this->fetch($id);

        if($status->service_status_editable == 0)
        {
            redirect('servico_status');
            syslog::generate_log('TRY_DELETE_LOCKED_SERVICE_STATUS');
            return;
        }

        $this->db->where('service_status_id', $servicoStatus->service_status_id);
        if($this->db->delete('servico_status')){
            syslog::generate_log('DELETE_SERVICE_STATUS_SUCCESS');
            return true;
        } else {
            syslog::generate_log('DELETE_SERVICE_STATUS_ERROR');
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('servico_status');
    }

}