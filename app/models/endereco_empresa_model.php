<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 12/06/15
 * Time: 11:49
 */

class endereco_empresa_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function fetchAll(EnderecoEntity $entity)
    {
        $this->db->from('endereco');
        return $this->db->get()->result();
    }

    public function fetch(EnderecoEntity $entity)
    {
        $this->db->from('endereco');

        if($entity->endereco_id != '')
        $this->db->where('endereco_id', $entity->endereco_id);

        return $this->db->get()->row();

    }


    public function save(EnderecoEmpresaEntity $entity)
    {

        $this->db->trans_begin();

        $id = (int) $entity->endereco_id;


        $data = [
            'endereco_logradouro' => $this->endereco_logradouro,
            'endereco_bairro' => $this->endereco_bairro,
            'endereco_num' => $this->endereco_num,
            'endereco_cep' => $this->endereco_cep,
            'endereco_estado' => $this->endereco_estado,
            'endereco_cidade' => $this->endereco_cidade,
            'endereco_complemento' => $this->endereco_complemento,
            'endereco_alias' => $this->endereco_alias,
        ];

        if($id == 0){

            $this->setFields($data);
            $this->db->insert('endereco');
            $entity->endereco_id = $this->db->insert_id();
            if($entity->endereco_id != 0){
                if(true === $this->save_empresa($entity)){
                    $this->db->trans_commit();
                    syslog::generate_log('NEW_COMPANY_ADDRESS_SUCCESS');
                    return $entity;
                }
            } else {
                $this->db->trans_rollback();
                syslog::generate_log('NEW_COMPANY_ADDRESS_ERROR');
                return false;
            }

        } else {

            $this->setFields($entity);
            $this->db->update('endereco');

            if($this->db->where('endereco_id', $entity->endereco_id)){
                syslog::generate_log('UPDATE_COMPANY_ADDRESS_SUCCESS');
                return $entity;
            } else {
                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_COMPANY_ADDRESS_ERROR');
                return false;
            }
        }

    }

    public function save_empresa(EnderecoEmpresaEntity $entity)
    {
        $data = [
            'empresa_id' => $entity->empresa_id,
            'endereco_id' => $entity->endereco_id,
        ];

        $this->setFields($data);
        $this->db->insert('empresa_endereco');

        if($this->db->insert_id() != 0){
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'endereco_id' || $value == null || $value == '' || $key == 'empresa_endereco_id') continue;
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('endereco');
    }
}