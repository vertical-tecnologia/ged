<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 06/06/15
 * Time: 19:43
 */

class permissao_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('usuario_tipo_model');
        $this->load->model('module_model');
        $this->load->model('action_model');
    }

    public function index()
    {
        $this->vet_dados['user_type'] = $this->usuario_tipo_model->fetchAll();
        return $this->parser->parse('permissao/permissao_list_view', $this->vet_dados, TRUE);
    }

    public function editar()
    {
        $entity = new UsuarioTipo();
        $entity->type_id = $this->uri->segment(3);
        $this->vet_dados['user_type'] = array($this->usuario_tipo_model->fetch($entity));
        $mEntity = new ModuleEntity();
        $mEntity->module_system = false;
        $this->vet_dados['module']  = $this->module_model->fetchAll($mEntity);
        $this->vet_dados['action']  = $this->action_model->fetchAll(new ActionEntity());

        return $this->parser->parse('permissao/permissao_edit_view', $this->vet_dados, TRUE);

    }


    public function get_permissions($object)
    {
        $this->db->from('permissao');
        $this->db->where('permissao_type_id', $object->type_id);
        $this->db->where('permissao_module_id', $object->module_id);

        return $this->db->get()->result();
    }

    public function save_permission(stdClass $object)
    {

//        Debug::Test($object);die;
        $inserted = 0;
        $this->delete_permissions($object);

        foreach($object->permissions as $key => $value){

            $this->db->set('permissao_module_id', $object->module_id);
            $this->db->set('permissao_type_id', $object->type_id);
            $this->db->set('permissao_action_id', str_replace('action_id_', '', $key));
            $this->db->set('permissao_allow', $value == 'true' ? true : false);
            $this->db->insert('permissao');

            if($this->db->insert_id() > 0)
                $inserted++;

        }

        if($inserted >  0){
            syslog::generate_log('UPDATE_PERMISSION_SUCCESS');
            return true;
        }
        else{
            syslog::generate_log('UPDATE_PERMISSION_ERROR');
            return false;
        }

    }

    protected function delete_permissions(stdClass $object){

        $this->db->where('permissao_type_id', $object->type_id);
        $this->db->where('permissao_module_id', $object->module_id);
        if($this->db->delete('permissao')){
            syslog::generate_log('REVOKE_PERMISSION_SUCCESS');
            return true;
        } else {
            syslog::generate_log('UPDATE_PERMISSION_ERROR');
            return false;
        }



    }
    
    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if(gettype($value) =='array' || $key == 'permissao_id')
                continue;

            if($value != null)
                $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('permissao');
    }
}
