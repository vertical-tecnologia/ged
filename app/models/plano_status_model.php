<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 02:37
 */

class plano_status_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));

    }


    public function index()
    {
        $status = $this->fetchAll();
        for($i = 0;$i < count($status);$i++){
            $status[$i]->link_del = $status[$i]->status_editable == 0 ? '' : anchor('plano_status/excluir/' . $status[$i]->status_id, 'Excluir', array('title' => 'Excluir Status do Plano'));
            $status[$i]->link_upd = anchor('plano_status/editar/' . $status[$i]->status_id, 'Editar', array('title' => 'Editar Status do Plano'));
        }

        $this->vet_dados['plano_status'] = $status;

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Planos';
        $this->vet_dados['tela']      = 'plano_status';

        return $this->parser->parse('plano_status/plano_status_list_view', $this->vet_dados, TRUE);

    }

    public function cadastro()
    {

        $vet['status_id']        = set_value('status_id');
        $vet['status_name']        = set_value('status_name');
        $this->vet_dados['plano_status'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Planos';
        $this->vet_dados['tela']      = 'plano_status';

        return $this->parser->parse('plano_status/plano_status_cad_view', $this->vet_dados, TRUE);

    }

    public function edit()
    {
        $user = $this->fetch($this->status_id);

        $vet['status_id']        = ($user->status_id);
        $vet['status_name']      = ($user->status_name);

        $this->vet_dados['plano_status'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Status de Planos';
        $this->vet_dados['tela']      = 'plano_status';

        return $this->parser->parse('plano_status/plano_status_cad_view', $this->vet_dados, TRUE);

    }

    public function fetch($id)
    {
        $this->db->from('plano_status');
        $this->db->where('status_id', (int) $id);

        return $this->db->get()->row();
    }

    public function fetchAll()
    {
        $this->db->from('plano_status');

        return $this->db->get()->result();
    }

    public function save()
    {

        if($this->valida() === FALSE)
            return $this->cadastro();

        $data = [
            'status_name' => $this->status_name
        ];

        $id = (int) $this->status_id;

        if($id == 0){
            $data['status_editable'] = 1;
            $this->setFields($data);
            $this->db->insert('plano_status');

            $id = $this->db->insert_id();

            if ($this->db->trans_status() && $id > 0) {

                $this->db->trans_commit();
                syslog::generate_log('NEW_PLAN_STATUS_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('NEW_PLAN_STATUS_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }

        } else {
            $this->setFields($data);
            $this->db->where('status_id', $id);
            $this->db->update('plano_status');

            $count = $this->db->affected_rows();

            if ($this->db->trans_status() && $count > 0) {

                $this->db->trans_commit();
                syslog::generate_log('UPDATE_PLAN_STATUS_SUCCESS');
                return TRUE;

            } else {

                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_PLAN_STATUS_ERROR');
                throw new Exception($this->lang->line('zorbit_erro_inserir'));

            }
        }
    }

    public function delete($id)
    {
        $this->db->where('status_id', $id);
        if($this->db->delete('plano_status')){
            syslog::generate_log('DELETE_PLAN_STATUS_SUCCESS');
            return true;
        } else {
            syslog::generate_log('DELETE_PLAN_STATUS_ERROR');
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('plano_status');
    }

}