<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 25/06/15
 * Time: 23:10
 */
class cliente_model extends CI_Model
{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index(ClienteEntity $entity)
    {
        $this->vet_dados['button_add'] = '';
        $cliente = $this->fetchAllClienteEmpresa(new ClienteEntity());

        for ($i = 0; $i < count($cliente); $i++) {
            $cliente[$i]->empresa_register_date = data_formatada($cliente[$i]->empresa_register_date, 5);
            $cliente[$i]->server_type_desc = constant('STORAGE_' . $cliente[$i]->server_type);
        }

        $this->vet_dados['clientes'] = $cliente;

        return $this->parser->parse('client/client_list_view', $this->vet_dados, TRUE);
    }

    public function visualizar(ClienteEntity $entity)
    {
        $this->vet_dados['button_add'] = '';
        $cliente = $this->fetchAllClienteEmpresa($entity);


        $cliente->empresa_register_date = data_formatada($cliente->empresa_register_date, 5);
        $cliente->server_type_desc = constant('STORAGE_' . $cliente->server_type);
        $cliente->empresa_cnpj = formatter($cliente->empresa_cnpj,'##.###.###/####-##');

//        Debug::Test($cliente);die;

        $this->vet_dados['cliente'] = array($cliente);

        return $this->parser->parse('client/client_view', $this->vet_dados, TRUE);

    }

    public function fetchAllClienteEmpresa(ClienteEntity $entity)
    {

        $this->db->select('cliente.*, empresa.empresa_register_date, empresa.empresa_fantasia, empresa.empresa_razao, empresa.empresa_cnpj, servidor.server_type, plano.plan_name, plano.plan_price, plano.plano_max_users');
        $this->db->from('cliente');
        $this->db->join('empresa', 'empresa.empresa_id = cliente.client_empresa_id');
        $this->db->join('plano', 'plano.plan_id = cliente.client_plan_id');
        $this->db->join('servidor', 'servidor.server_id = cliente.server_id');

        if ($entity->client_id != '') {
            $this->db->where('cliente.client_id', $entity->client_id);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();

    }

    public function fetchAllClientePessoal(ClienteEntity $entity)
    {

        $this->db->from('cliente');
        $this->db->join('usuario', 'usuario.user_id = cliente.client_user_id');

        return $this->db->get()->result();

    }

    public function fetchClienteEmpresa(ClienteEntity $entity)
    {

        $this->db->from('cliente');
        $this->db->join('empresa', 'empresa.empresa_id = cliente.client_empresa_id');
        $this->db->join('empresa_endereco', 'empresa_endereco.empresa_id = cliente.client_empresa_id');
        $this->db->join('endereco', 'empresa_endereco.endereco_id = endereco.endereco_id');

        if ($entity->client_empresa_id != '')
            $this->db->where('empresa.empresa_id', $entity->client_empresa_id);

//        $this->db->where('endereco.endereco_principal',1);

        return $this->db->get()->row();

    }

    public function fetchClientePessoal(ClienteEntity $entity)
    {

        $this->db->from('cliente');
        $this->db->join('usuario', 'usuario.user_id = cliente.client_user_id');
        $this->db->join('usuario_endereco', 'usuario_endereco.user_id = cliente.client_user_id');
        $this->db->join('endereco', 'usuario_endereco.endereco_id = endereco.endereco_id');

        if ($entity->client_id != '')
            $this->db->where('client_id', $entity->client_id);

        $this->db->where('endereco.endereco_principal', 1);

        return $this->db->get()->row();

    }

}