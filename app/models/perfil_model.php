<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 24/06/15
 * Time: 11:03
 */

class perfil_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('usuario_status_model');
        $this->load->model('usuario_tipo_model');
        $this->load->model('segmento_model');
        $this->load->model('grupo_model');
        $this->load->model('endereco_model');
        $this->load->model('contato_tipo_model');
        $this->load->model('contato_model');
    }

    public function profile_data()
    {
        $entity = new UsuarioEntity();
        $entity->user_id = $this->session->userdata('USER_ID');

        $endereco = new EnderecoEntity();
        $contato = new ContatoEntity();
        $endereco->user_id = $this->session->userdata('USER_ID');
        $contato->contato_user_id = $this->session->userdata('USER_ID');
        $uEndereco = new UsuarioEnderecoEntity();
        $uEndereco->user_id = $this->session->userdata('USER_ID');
        $this->vet_dados['main_address'] = $this->endereco_model->fetchMainUserAddress($uEndereco)                                          ;

        $this->vet_dados['enderecos'] = $this->endereco_model->fetchAll($endereco);
        $this->vet_dados['contatos'] = $this->contato_model->fetchAll($contato);
        $uEmpresa = new EmpresaUsuarioEntity();
        $uEmpresa->id_usuario = $entity->user_id;

        $this->vet_dados['empresa'] = $this->fetchEmpresa($uEmpresa);
        $this->vet_dados['type'] = $this->contato_tipo_model->fetchAll();
        $this->vet_dados['usuario_id'] = $entity->user_id;

        $user = $this->get_userdata();

        $user->verified = $user->user_trusted_mail == true ? 'conta verificada' : 'verificar email';
        $user->user_picture = $user->user_picture != '' ? base_url().'public/static/'.$user->user_picture : USER_NO_PICTURE;
        $user->user_register_date = type_format($user->user_register_date, 'data_br');

        $this->vet_dados['type_id'] = $user->user_type_id;


        $this->vet_dados['user'] = array($user);
        return $this->parser->parse('perfil/perfil_view', $this->vet_dados, TRUE);
    }

    public function get_userdata()
    {
        $this->db->select('usuario.*');
        $this->db->from('usuario');

        $this->db->where('usuario.user_id', $this->session->userdata('USER_ID'));


        return $this->db->get()->row();
    }

    public function fetchEmpresa(EmpresaUsuarioEntity $entity)
    {
        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->db->from('empresa');
            $this->db->where('empresa.empresa_user_id', $entity->id_usuario);
        } else {

            $this->db->from('empresa');
            $this->db->join('empresa_usuario', 'empresa_usuario.id_empresa = empresa.empresa_id');
            $this->db->where('empresa_usuario.id_usuario', $entity->id_usuario);
        }

        return $this->db->get()->result();
    }

    public function compare_password()
    {
        $this->db->select('user_password');
        $this->db->from('usuario');
        $this->db->where('user_id', $this->session->userdata('USER_ID'));

        return $this->db->get()->row();
    }

    public function update_pass(UsuarioEntity $entity)
    {
        $user_id = (int) $this->session->userdata('USER_ID');

        $this->db->set('user_password', $entity->user_password);
        $this->db->where('user_id', $user_id);

        if($this->db->update('usuario')){
            syslog::generate_log("UPDATE_PASSWORD_SUCCESS");
            return true;
        }
        else{
            syslog::generate_log("UPDATE_PASSWORD_ERROR");
            return false;
        }


    }


    public function update_picture(UsuarioEntity $entity)
    {
        $user_id = (int) $this->session->userdata('USER_ID');

        $this->db->set('user_picture', $entity->user_picture);
        $this->db->where('user_id', $user_id);

        if($this->db->update('usuario')){
            syslog::generate_log('UPDATE_PROFILE_PIC_SUCCESS');
            return true;
        }
        else{
            syslog::generate_log('UPDATE_PROFILE_PIC_ERROR');
            return false;
        }
    }

}
