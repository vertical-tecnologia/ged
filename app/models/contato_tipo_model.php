<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 16:48
 */

class contato_tipo_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $this->vet_dados['contato_tipo'] = $this->fetchAll();

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos de Contato';
        $this->vet_dados['tela']      = 'contato_tipo';

        return $this->parser->parse('contato_tipo/contato_tipo_list_view', $this->vet_dados, TRUE);
    }

    public function cadastro()
    {

        $vet['tipo_id']        = set_value('tipo_id');
        $vet['tipo_desc']        = set_value('tipo_desc');

        $this->vet_dados['contato_tipo'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos Contato';
        $this->vet_dados['tela']      = 'contato_tipo';

        return $this->parser->parse('contato_tipo/contato_tipo_cad_view', $this->vet_dados, TRUE);
    }

    public function editar(ContatoTipoEntity $entity)
    {
        $id = (int) $entity->tipo_id;

        $tipo = $this->fetch($entity);

        if(!$tipo){
            return false;
        }
        $vet = array();

        $vet['tipo_id']        = $tipo->tipo_id;
        $vet['tipo_desc']      = $tipo->tipo_desc;

        $this->vet_dados['contato_tipo'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Tipos Usuários';
        $this->vet_dados['tela']      = 'contato_tipo';

        return $this->parser->parse('contato_tipo/contato_tipo_cad_view', $this->vet_dados, TRUE);
    }


    public function fetchAll()
    {
        $this->db->from('contato_tipo');
        return $this->db->get()->result();
    }

    public function fetch(ContatoTipoEntity $entity)
    {
        $id = (int) $entity->tipo_id;


        $this->db->from('contato_tipo');
        $this->db->where('tipo_id', $id);
        return $this->db->get()->row();

    }

    public function save(ContatoTipoEntity $entity)
    {
        if($this->valida() === FALSE)
            return $this->cadastro();
        $data = [
                'tipo_desc'     => $entity->tipo_desc,
        ];

        $id = (int) $entity->tipo_id;

        if($id == 0){
            $this->setFields($data);
            $this->db->insert('contato_tipo');
            $entity->tipo_id = (int) $this->db->insert_id();

            if($entity->tipo_id == 0){
                syslog::generate_log('NEW_CONTACT_TYPE_SUCCESS');
                return FALSE;
            } else {
                syslog::generate_log('NEW_CONTACT_TYPE_ERROR');
                return TRUE;
            }

        } else {
            $this->setFields($data);
            $this->db->where('tipo_id', $id);
            $this->db->update('contato_tipo');
            $count = $this->db->affected_rows();

            if($count == 0){
                syslog::generate_log('UPDATE_CONTACT_TYPE_SUCCESS');
                return FALSE;
            } else {
                syslog::generate_log('UPDATE_CONTACT_TYPE_ERROR');
                return TRUE;
            }
        }

    }

    public function delete(ContatoTipoEntity $entity)
    {
        $this->db->where('tipo_id', $entity->tipo_id);
        if($this->db->delete('contato_tipo')){
            syslog::generate_log('DELETE_CONTACT_TYPE_SUCCESSS');
            return true;
        } else {
            syslog::generate_log('DELETE_CONTACT_TYPE_ERROR');
            return false;
        }

    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($value == "") continue;
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('contato_tipo');
    }
}