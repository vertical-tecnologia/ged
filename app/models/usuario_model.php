<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 27/05/15
 * Time: 01:33
 */

class usuario_model extends CI_Model{

    public $max_users;
    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('usuario_status_model');
        $this->load->model('usuario_tipo_model');
        $this->load->model('segmento_model');
        $this->load->model('grupo_model');
        $this->load->model('endereco_model');
        $this->load->model('contato_tipo_model');
        $this->load->model('contato_model');

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $this->max_users = $this->maxUsers();
        }
    }

    public function index()
    {

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN' && $this->session->userdata('EKEY') == ''){
            $this->session->set_flashdata('error', 'Por favor, cadastre uma empresa antes de cadastrar seus usuários.');
            redirect('empresa');
            return;
        }

        if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
            if ($this->uri->segment(2) == 'clientes') {
                $this->vet_dados['filter_link'] = anchor('usuario/admin', 'Ver usuários administrativos', array('class' => 'btn btn-warning'));
            } else {
                $this->vet_dados['filter_link'] = anchor('usuario/clientes', 'Ver usuários de clientes', array('class' => 'btn btn-warning '));
            }
        } else {
            $this->vet_dados['filter_link'] = null;
        }


        $user = $this->fetchAll(new UsuarioEntity());

        $total = count($user);
        if(($this->session->userdata('USER_ROLE') == 'CLIADMIN') && $total >= $this->max_users->plano_max_users)
            $this->vet_dados['button_add'] = anchor('', '<i class="fa fa-plus"></i> Novo Usuário</a>', array('class'=>'btn btn-primary pull-right disabled'));
        else
            $this->vet_dados['button_add'] = anchor('usuario/cadastro', '<i class="fa fa-plus"></i> Novo Usuário</a>', array('class'=>'btn btn-primary pull-right'));


        for($i=0;$i<count($user);$i++){
            $user[$i]->user_register_date = type_format($user[$i]->user_register_date, 'data_br');
            $user[$i]->user_update_day = type_format($user[$i]->user_update_day, 'data_br');
            $user[$i]->link_del = anchor('usuario/excluir/'.$user[$i]->user_id, 'Excluir');
            $user[$i]->link_edt = anchor('usuario/editar/'.$user[$i]->user_id, 'Alterar');

            if($user[$i]->user_trusted_mail == null || $user[$i]->user_trusted_mail == false){
                $user[$i]->link_mail = anchor('usuario/confirmar/'.$user[$i]->user_id, 'Confirmar Email');
            } else {
                $user[$i]->link_mail = null;
            }

        }

        $this->vet_dados['usuarios'] = $user;

        $this->vet_dados['nome_tela'] = 'Gerenciar Usuários';
        $this->vet_dados['tela']      = 'usuario';

        return $this->parser->parse('usuario/usuario_list_view', $this->vet_dados, TRUE);
    }

    public function endereco()
    {

        $entity = new EnderecoEntity();
        $entity->user_id = (int) $this->input->post('user_id') != '' ? $this->input->post('user_id') : $this->uri->segment(3);
        $vet = null;

        $vet['endereco_alias'] = set_value('endereco_alias');
        $vet['endereco_cep'] = set_value('endereco_cep');
        $vet['endereco_logradouro'] = set_value('endereco_logradouro');
        $vet['endereco_num'] = set_value('endereco_num');
        $vet['endereco_bairro'] = set_value('endereco_bairro');
        $vet['endereco_cidade'] = set_value('endereco_cidade');
        $vet['endereco_estado'] = set_value('endereco_estado');
        $vet['endereco_complemento'] = set_value('endereco_complemento');
        $vet['endereco_id'] = set_value('');
        $vet['user_id'] = $entity->user_id;
        $vet['empresa_id']    = $entity->empresa_id;

        $this->vet_dados['endereco'] = array($vet);
        return $this->parser->parse('endereco/ajax/endereco_cad_view', $this->vet_dados, TRUE);
    }

    public function contato()
    {

        $entity = new EnderecoEntity();
        $entity->user_id = (int) $this->input->post('user_id') != '' ? $this->input->post('user_id') : $this->uri->segment(3);
        $vet = null;

        $this->vet_dados['type'] = $this->contato_tipo_model->fetchAll();

        $vet['contato_id']    = set_value('contato_id');
        $vet['contato_valor'] = set_value('contato_valor');
        $vet['contato_tipo']  = set_value('contato_tipo');
        $vet['user_id']    = $entity->user_id;
        $vet['empresa_id']    = $entity->empresa_id;

        $this->vet_dados['contato'] = array($vet);
        return $this->parser->parse('contato/ajax/contato_cad_view', $this->vet_dados, TRUE);
    }

    public function visualizar(UsuarioEntity $entity)
    {

        $this->load->model('empresa_model');
        $user = $this->get_userdata($entity);

        $endereco = new EnderecoEntity();
        $contato = new ContatoEntity();
        $endereco->user_id = $user->user_id;
        $contato->contato_user_id = $user->user_id;
        $uEndereco = new UsuarioEnderecoEntity();
        $uEndereco->user_id = $user->user_id;
        $this->vet_dados['main_address'] = $this->endereco_model->fetchMainUserAddress($uEndereco);

        $this->vet_dados['enderecos'] = $this->endereco_model->fetchAll($endereco);
        $this->vet_dados['contatos'] = $this->contato_model->fetchAll($contato);
        $uEmpresa = new EmpresaUsuarioEntity();
        $uEmpresa->id_usuario = $entity->user_id;

        $this->vet_dados['empresa'] = array($this->fetchEmpresa($uEmpresa));


        $this->vet_dados['type_id'] = $user->user_type_id;

        $user->verified = $user->user_trusted_mail == true ? 'conta verificada' : 'verificar email';
        $user->user_picture = $user->user_picture != '' ? base_url().'public/static/'.$user->user_picture : USER_NO_PICTURE;
        $user->user_register_date = type_format($user->user_register_date, 'data_br');

        $this->vet_dados['user'] = array($user);

        return $this->parser->parse('usuario/usuario_view', $this->vet_dados, TRUE);
    }

    public function empresa(EmpresaEntity $entity)
    {
        $this->vet_dados['segmento'] = $this->segmento_model->fetchAll();

        $empresa = $this->empresa_model->fetch($entity);

        if(count($empresa) > 0)
            $this->vet_dados['empresa']  = array($empresa);
        else{
            $vet = array();
            $vet['empresa_user_id'] =   $entity->empresa_user_id;
            $vet['empresa_id'] =   set_value('empresa_id');
            $vet['empresa_fantasia'] = set_value('empresa_fantasia');
            $vet['empresa_razao'] = set_value('empresa_razao');
            $vet['empresa_cnpj'] = set_value('empresa_cnpj');
            $vet['empresa_ie'] = set_value('empresa_ie');
            $vet['empresa_segmento_id'] = set_value('empresa_segmento_id');

            $this->vet_dados['empresa']  = array($vet);
        }

        $this->vet_dados['empresa_endereco'] = $this->get_empresa_endereco($entity);


        return $this->parser->parse('usuario/usuario_empresa_cad_view', $this->vet_dados, TRUE);

    }

    public function excluir()
    {
        $entity = new UsuarioEntity();
        $entity->user_id = $this->uri->segment(3);
        $user = $this->get_userdata($entity);

        $user->user_register_date = data_formatada($user->user_register_date, 4);

        $this->vet_dados['usuario'] = array($user);

        return $this->parser->parse('usuario/usuario_confirm_delete_view', $this->vet_dados, TRUE);

    }

    public function get_empresa(UsuarioEntity $entity)
    {
        return $this->parser->parse('usuario/usuario_empresa_view', $this->vet_dados, TRUE);
    }

    public function get_empresa_endereco(EmpresaEntity $entity)
    {
        return $this->parser->parse('endereco/endereco_list_view', $this->vet_dados, TRUE);
    }

    public function edit(UsuarioEntity $entity)
    {
        $this->vet_dados['group'] = $this->grupo_model->fetchAll();
        $usuario_status = $this->usuario_status_model->fetchAll();
        $this->vet_dados['group'] = $this->grupo_model->fetchAll();
        $usuario_tipo = $this->usuario_tipo_model->fetchAll();
        $this->vet_dados['readonly'] = " readonly ";

        $user = $this->fetch($entity);

        if(count($user) == 0)
            redirect('usuario');

        $vet['user_id'] = $user->user_id;
        $vet['user_fullname'] = $user->user_fullname;
        $vet['user_login'] = $user->user_login;
        $vet['user_email'] = $user->user_email;
        $vet['user_status_id'] = $user->user_status_id;

        $this->vet_dados['usuario'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Usuários';
        $this->vet_dados['tela']      = 'usuario';


        $empresa = new EmpresaEntity();
        if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO'){
            $empresa->empresa_id = $this->session->userdata('EID');
        }

        $empresas = $this->fetchEmpresas($empresa);

        for ($i = 0; $i < count($usuario_status); $i++) {
            $usuario_status[$i]->selected = $usuario_status[$i]->status_id == $user->user_status_id ? ' selected ' : '';
        }

        for ($i = 0; $i < count($usuario_tipo); $i++) {
            $usuario_tipo[$i]->selected = $usuario_tipo[$i]->type_id == $user->user_type_id ? ' selected ' : '';
        }

        for($i = 0; $i < count($empresas); $i++) {
            if($user->user_type_id == 2)
                $empresas[$i]->selected = $empresas[$i]->empresa_user_id == $user->user_id  ? ' selected ' : '';
            else{

                $empresas[$i]->selected = ' selected ';
            }
        }

        $this->vet_dados['empresas'] = $empresas;
        $this->vet_dados['usuario_tipo'] = $usuario_tipo;
        $this->vet_dados['usuario_status'] = $usuario_status;

        return $this->parser->parse('usuario/usuario_cad_view', $this->vet_dados, TRUE);
    }


    public function cadastro()
    {
        $user = $this->fetchAll(new UsuarioEntity());
        $total = count($user);

        if(($this->session->userdata('USER_ROLE') == 'CLIADMIN') && $total >= $this->max_users->plano_max_users ){
            $this->session->set_flashdata('warning', 'Seu plano só permite '.$this->max_users->plano_max_users.' usuários.');
            redirect('usuario');
            return;
        }


        $vet['user_id'] = set_value('user_id');
        $vet['user_fullname'] = set_value('user_fullname');
        $vet['user_login'] = set_value('user_login');
        $vet['user_email'] = set_value('user_email');
        $vet['user_status_id'] = set_value('user_status_id');

        $this->vet_dados['readonly'] = " ";

        $status = $this->usuario_status_model->fetchAll();
        $this->vet_dados['usuario_status'] = $status;

        for($i=0;$i<count($status);$i++){
            if($status[$i]->status_id == 6){
                $status[$i]->selected = " selected ";
            } else {
                $status[$i]->selected = "";
            }
        }

        $tipo = $this->usuario_tipo_model->fetchAll();

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            for($i=0;$i<count($tipo);$i++){
                if($tipo[$i]->type_name == 'USUARIO'){
                    $tipo[$i]->selected = " selected ";
                } else {
                    $tipo[$i]->selected = "";
                }
            }
        }

        $this->vet_dados['usuario_tipo'] = $tipo;

        $this->vet_dados['usuario'] = array($vet);

        $this->vet_dados['nome_tela'] = 'Gerenciar Usuários';
        $this->vet_dados['tela']      = 'usuario';

        if($this->session->userdata('USER_ROLE') == 'CLIADMIN'){
            $empresa = new EmpresaUsuarioEntity();
            $empresa->id_empresa = $this->session->userdata('EID');
            $retorno = $this->fetchEmpresa($empresa);

            $this->vet_dados['empresa_id'] = $retorno->empresa_id;

            $this->vet_dados['empresa_razao'] = $retorno->empresa_razao;

        }

        $empresa = new EmpresaEntity();
        if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO'){
            $empresa->empresa_id = $this->session->userdata('EID');
        }

        $empresas = $this->fetchEmpresas($empresa);

        if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO') {
            $empresas[0]->selected = " selected ";
        } else {
            $empresas[0]->selected = "";
        }

        $this->vet_dados['empresas'] = $empresas;

        return $this->parser->parse('usuario/usuario_cad_view', $this->vet_dados, TRUE);
    }

    public function safe_delete()
    {
        $this->db->trans_begin();

        if(!in_array($this->session->userdata('USER_ROLE'), array('SUPERUSUARIO', 'CLIADMIN')))
        {
            syslog::generate_log("UNAUTHORIZED_DELETE_USER");
            $this->session->set_flashdata('error', 'Você não tem permissões de executar essa ação.');
            redirect('usuario');
            return;
        }

        if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO'){

        } else
            $admin = $this->session->userdata('USER_KEY');

        $entity = new UsuarioEntity();
        $entity->user_id = $this->uri->segment(3);

        $user = $this->get_userdata($entity);

        if(count($user) == 1){
            $files = new ObjectEntity();
            $files->owner = $admin;
            $files->id = $user->user_key;

            if(!$this->update_user_files($files))
                return false;

            if($this->delete($entity)){
                $this->session->set_flashdata('success', 'Usuário excluído com sucesso.');
                $this->db->trans_commit();

            } else {
                $this->session->set_flashdata('error', 'Erro ao excluir usuário.');
                $this->db->trans_rollback();
            }
        }
        return;
    }

    public function fetch(UsuarioEntity $entity)
    {
        if($entity->user_id != null)
            $this->db->where('user_id', $entity->user_id);

        if($entity->user_type_id != null)
            $this->db->where('user_type_id', $entity->user_type_id);


        $this->db->from('usuario');

        return $this->db->get()->row();
    }

    public function fetchAll(UsuarioEntity $entity, $count = true)
    {
        if($count == true)
            $this->db->select('usuario.*, usuario_status.status_name as status_name, usuario_tipo.type_name');
        else
            $this->db->select('count(usuario.*)');


        $this->db->from('usuario');
        $this->db->join('usuario_status', 'usuario_status.status_id = usuario.user_status_id');
        $this->db->join('usuario_tipo', 'usuario_tipo.type_id = usuario.user_type_id');

        if(in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN')) ) {
            $this->db->join('empresa_usuario', 'empresa_usuario.id_usuario = usuario.user_id');
            $this->db->where('empresa_usuario.id_empresa', $this->session->userdata('EID'));
        }

        if(in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN', 'CLIPESSOA')) ) {
            $this->db->where_in('user_type_id', array(4, 2));
        }

        if(in_array($this->session->userdata('USER_ROLE'), array('SUPERUSUARIO')) ) {
            if($this->uri->segment(2) == 'clientes')
                $this->db->where_in('user_type_id', array(2, 3, 4));
            else
                $this->db->where_in('user_type_id', array(1, 6));
        }

        if($entity->user_type_id != ''){
            $this->db->where('user_type_id', $entity->user_type_id);
        }

        if($entity->user_group_id != ''){
            $this->db->where('user_group_id', $entity->user_group_id);
        }

        if($entity->user_status_id != ''){
            $this->db->where('user_status_id', $entity->user_status_id);
        }

        if($entity->user_trusted_mail != ''){
            $this->db->where('user_trusted_mail', $entity->user_trusted_mail);
        }

        if($entity->user_id != ''){
            $this->db->where('usuario.user_id', $entity->user_id);
        }

        $this->db->group_by('usuario.user_id');

        return $this->db->get()->result();

    }

    public function save(UsuarioEntity $usuario)
    {
        $this->db->trans_begin();

        $id = (int) $usuario->user_id;



        if($id == 0){

            $tmpPass = $usuario->user_password;

            $usuario->user_password = $this->encrypt->encode($tmpPass);

            $usuario->user_hash = $this->encrypt->encode(random_string('alnum', 10));
            $usuario->user_key  = random_string('alnum', 100);
            $usuario->user_trusted_mail  = false;
            $usuario->user_register_date  = date('Y-m-d h:i:s');

            $this->setFields($usuario);
            $this->db->insert('usuario');

            $id = $this->db->insert_id();

            if ($this->db->trans_status() && $id > 0) {
                syslog::generate_log("NEW_USER_SUCCESS");
                $usuario->user_id = $id;
                $usuario->user_password = $tmpPass;
                $this->enviar_email($usuario);
                $this->session->set_flashdata('success', 'Usuário cadastrado com sucesso');
                return $usuario;

            } else {
                syslog::generate_log("NEW_USER_ERROR");
                $this->session->set_flashdata('error', 'Erro ao cadastrar usuário.');
                $this->db->trans_rollback();
                return false;

            }

        } else {
            $this->setFields($usuario);
            $this->db->set('user_update_day', date('Y-m-d H:i:s'));
            $this->db->where('user_id', $id);
            $this->db->update('usuario');

            $count = $this->db->affected_rows();

            if ($this->db->trans_status() && $count > 0) {
                syslog::generate_log("UPDATE_USER_SUCCESS");
                $this->session->set_flashdata('success', 'Usuário alterado com sucesso');
                return $usuario;

            } else {
                syslog::generate_log("UPDATE_USER_ERROR");
                $this->session->set_flashdata('error', 'Erro ao alterar usuário.');
                $this->db->trans_rollback();
                return false;

            }
        }

    }

    public function get_grupos_usuario($params = null)
    {
        $this->db->select('group_usuario.*, group.group_name');
        $this->db->join('group', 'group_usuario.group_id = group.group_id');
        $this->db->from('group_usuario');

        if(isset($params['user_id'])){
            $this->db->where('user_id', $params['user_id']);
        }

        return $this->db->get()->result();
    }

    public function delete(UsuarioEntity $entity)
    {
        if(!isset($entity->user_id))
            return false;

        if($entity->user_id == $this->session->userdata('USER_ID'))
        {
            syslog::generate_log("UNABLE_SELF_DELETE_USER");
            return false;
        }

        $this->db->where('user_id', $entity->user_id);
        if($this->db->delete('usuario')){
            syslog::generate_log("DELETE_USER_SUCCESS");
            return true;
        } else {
            syslog::generate_log("DELETE_USER_ERROR");
            return false;
        }

    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'user_id' || $value == null || $value == '') continue;
            $this->db->set($key, $value);
        }
    }

    protected function saveGroups($user, $groups)
    {
        foreach($groups as $group){
            $this->db->set('group_id', $group);
            $this->db->set('user_id', $user);

            $this->db->insert('group_usuario');

            if($this->db->insert_id() == 0){
                $this->db->trasn_rollback();
                syslog::generate_log("USER_ADD_GROUP_ERROR");
                throw new Exception('Erro ao cadastrar grupos do usuário');
            } else {
                syslog::generate_log("USER_ADD_GROUP_SUCCESS");
            }
        }
    }

    public function saveUsuarioEmpresa(EmpresaUsuarioEntity $entity){

        $this->db->set('id_empresa' , $entity->id_empresa);
        $this->db->set('id_usuario' , $entity->id_usuario);

        $this->db->insert('empresa_usuario');

        if($this->db->insert_id() > 0){
            $this->db->trans_commit();
            syslog::generate_log("USER_LINKED_COMPANY_SUCCESS");
            return true;
        } else {
            $this->db->trans_rollback();
            syslog::generate_log("USER_LINKED_COMPANY_ERROR");
            return false;
        }
    }

    public function fetchEmpresas(EmpresaEntity $entity)
    {
        $this->db->from('empresa');

        if($entity->empresa_id != '')
            $this->db->where('empresa_id', $entity->empresa_id);

        return $this->db->get()->result();
    }

    public function fetchEmpresa(EmpresaUsuarioEntity $entity)
    {
        $this->db->from('empresa');
        $this->db->join('empresa_usuario', 'empresa_usuario.id_empresa = empresa.empresa_id', 'LEFT');
        if($entity->id_usuario != '')
            $this->db->where('empresa_usuario.id_usuario', $entity->id_usuario);

        if($entity->id_empresa != '')
        $this->db->where('empresa.empresa_id', $entity->id_empresa);

        return $this->db->get()->row();

    }

    public function maxUsers()
    {
        $this->db->select('plano_max_users');
        $this->db->from('plano');
        $this->db->join('cliente', 'cliente.client_plan_id = plano.plan_id');
        $this->db->where('cliente.client_id', $this->session->userdata('CLID'));

        return $this->db->get()->row();
    }

    public function getTotalUsers()
    {
        $this->db->select('plano_max_users');
        $this->db->from('plano');
        $this->db->join('cliente', 'cliente.client_plan_id = plano.plan_id');
        $this->db->where('cliente.client_id', $this->session->userdata('CLID'));

        return $this->db->get()->row();
    }


    protected function enviar_email($usuario){

        $this->load->library('Mailer');
        $usuario->user_hash = urlencode(urlencode($usuario->user_hash));
        $this->vet_dados['usuario'] = array($usuario);

        Mailer::$message = $this->parser->parse('template/public/usuario_cad_email_view', $this->vet_dados, TRUE);
        Mailer::$subject = "[GED] Cadastro de usuário.";
        Mailer::$to = $usuario->user_email;
        Mailer::$toName = $usuario->user_fullname;
        $m = Mailer::send_mail();

        if($m == true){
            syslog::generate_log("NEW_USER_EMAIL_SUCCESS");
            return true;
        }
        else
        {
            syslog::generate_log("NEW_USER_EMAIL_ERROR");
            return false;
        }
    }

    public function get_userdata(UsuarioEntity $entity = null)
    {
        $this->db->select('usuario.*');
        $this->db->from('usuario');

        if(isset($entity->user_id))
            $this->db->where('usuario.user_id', $entity->user_id);
        else
            $this->db->where('usuario.user_id', $this->session->userdata('USER_ID'));


        return $this->db->get()->row();
    }

    public function update_user_files(ObjectEntity $entity)
    {

        $this->db->set('owner', $entity->owner);
        $this->db->where('owner', $entity->id);

        if($this->db->update('file_object')){
            syslog::generate_log("USER_TRANSFER_FILES_SUCCESS");
            return true;
        } else {
            syslog::generate_log("USER_TRANSFER_FILES_ERROR");
            return false;
        }
    }


    public function confirmar()
    {
        $entity = new UsuarioEntity();
        $entity->user_id = $this->uri->segment(3);

        if($entity->user_id ==  '' || $entity->user_id == null){
            $this->session->set_flashdata('error', 'Usuário não encontrado.');
            redirect('usuario');
        }

        $user = $this->fetch($entity);

        if(count($user) == 1){
            $this->db->set('user_trusted_mail', true);
            $this->db->where('user_id', $user->user_id);

            if($this->db->update('usuario')){
                $this->session->set_flashdata('success', 'Email confirmado com sucesso.');
            } else {
                $this->session->set_flashdata('error', 'Erro ao confirmar email.');
            }
        } else
            $this->session->set_flashdata('error', 'Usuário não encontrado.');

        redirect('usuario');
    }


    public function check_email($email)
    {
        $this->db->from('usuario');
        $this->db->where('user_email', $email);
        $row = $this->db->get()->row();

        if (count($row) == 0) {
            return true;
        } else {
            $this->form_validation->set_message('check_email', 'Esse email ja esta cadastrado.');
            return false;
        }
    }

    public function check_login($login)
    {
        $this->db->from('usuario');
        $this->db->where('user_login', $login);
        $row = $this->db->get()->row();

        if (count($row) == 0) {
            return true;
        } else {
            $this->form_validation->set_message('check_login', 'Esse login ja esta cadastrado.');
            return false;
        }
    }

}
