<?php

/**
 * Class template_skin_model
 * @package		App\Models
 */
class template_skin_model extends CI_Model {

    public $pos       = null;
    public $vet_dados = null;

    public function __construct() {

        parent::__construct();

        $this->pos  = 0;
        $this->session->unset_userdata('skin');

        if ($this->session->userdata('skin') == '') {
            $this->getTemplateSkin();
        }

        if($this->authenticator->is_logged_in() === true) {
            $sidebar_position = $this->get_sidebar_position();
            @$sidebar = $sidebar_position->sidebar_position != '' ? $sidebar_position->sidebar_position : '';
            @$footer  = $sidebar_position->sidebar_position != '' ? 'footer-left' : '';
            define('SIDEBAR_POSITION', $sidebar);
            define('BACKGROUND_PATTERN', @$this->get_background()->background);
            define('FOOTER_POSITION', $footer);
        }
    }

    public function getTopo() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        if($this->authenticator->is_logged_in() == true) {
            $this->load->model('message_model');
            $this->vet_dados['total_nao_lidas'] = $this->message_model->get_new_messages()->total_nao_lidas == 0 ? '' : $this->message_model->get_new_messages()->total_nao_lidas;
        }
        $this->vet_dados['data_hora'] = data_hora_formatada();
        return $this->parser->parse('template/'.$this->session->userdata('template').'/topo_view', $this->vet_dados, TRUE);

    }

    public function getTituloTela() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        return $this->parser->parse('template/'.$this->session->userdata('template').'/titulo_view', $this->vet_dados, TRUE);

    }

    public function getBotoes() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        return $this->parser->parse('template/'.$this->session->userdata('template').'/botoes/botoes_view', $this->vet_dados, TRUE);

    }

    public function getFiltro($tela) {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        $this->vet_dados['tela']        = $tela;
        $this->vet_dados['filtro_tela'] = $this->parser->parse($tela.'/'.$tela.'_filtro_view', $this->vet_dados, TRUE);

        return $this->parser->parse('template/'.$this->session->userdata('template').'/filtro_view', $this->vet_dados, TRUE);

    }

    public function getHead() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        $theme = $this->get_theme();


        $this->vet_dados['theme'] = isset($theme->theme) ? $theme->theme : 'blue';

        return $this->parser->parse('template/'.$this->session->userdata('template').'/head_view', $this->vet_dados, TRUE);

    }

    public function getLoginHead() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        return $this->parser->parse('template/'.$this->session->userdata('template').'/login_head_view', $this->vet_dados, TRUE);

    }

    public function getRodape() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        return $this->parser->parse('template/'.$this->session->userdata('template').'/rodape_view', $this->vet_dados, TRUE);

    }

    public function getJS() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        return $this->parser->parse('template/'.$this->session->userdata('template').'/js_view', $this->vet_dados, TRUE);

    }


    public function getLoginJS() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        return $this->parser->parse('template/'.$this->session->userdata('template').'/login_js_view', $this->vet_dados, TRUE);

    }

    public function getValidacao() {

        if ($this->session->userdata('vet_dados') != '') {
            $this->vet_dados = $this->session->userdata('vet_dados');
        } else {
            $this->vet_dados = base_dir($this->session->userdata('skin'));
        }

        $this->vet_dados['msg_erro'] = validation_errors();
        $aux_val = $this->parser->parse('template/'.$this->session->userdata('template').'/validacao_view', $this->vet_dados, TRUE);

        return strlen(validation_errors()) > 0 ? $aux_val : '';

    }

    public function getSide($tipo=2) {

        return $this->parser->parse('menu_gebo/side_steps', $this->vet_dados, TRUE);

    }

    public function getMenu($tipo=2) {

        $this->load->model('template_menu_model');
        if($this->session->userdata('USER_ID')) {
            $total_files = $this->template_menu_model->getMenuData();
            $this->vet_dados['total_files'] = $total_files['count_files']->total_files;
            $this->vet_dados['size_files'] = By2Mb($total_files['size_files']->total_size_files);
            if($this->session->userdata('USER_ROLE') == 'SUPERUSUARIO') {
                $this->vet_dados['total_storage'] = By2Mb($total_files['total_storage']->storage_size);
                $this->vet_dados['total_percent_free'] = round(($total_files['size_files']->total_size_files * 100)/$total_files['total_storage']->storage_size, 2);
            }
            if($this->session->userdata('USER_ROLE') != 'SUPERUSUARIO') {

                $this->vet_dados['quota_size'] = By2Mb($total_files['quota']->quota_size);
                $this->vet_dados['shared'] = $total_files['shared'];
                $free_space = ($total_files['quota']->quota_size - $total_files['size_files']->total_size_files) < 0 ? 0 : $total_files['quota']->quota_size - $total_files['size_files']->total_size_files;
                $this->vet_dados['free_space'] = byte_format($free_space);
                $this->vet_dados['free_total_space'] = byte_format($total_files['total_space']);

                if($total_files['quota']->quota_size != 0)
                    $this->vet_dados['percent_free'] = round(($total_files['size_files']->total_size_files * 100) / $total_files['quota']->quota_size, 2);
            }
        }

        return $this->parser->parse('menu_gebo/menu_estatico_view', $this->vet_dados, TRUE);

    }

    public function getTemplateSkin() {

        $this->session->set_userdata('template', 'gebo3');
        $this->session->set_userdata('skin',     'gebo3');
        $this->session->set_userdata('pskin',     'public');
    }

    public function get_theme()
    {
        $this->db->select('theme');
        $this->db->from('configuration');
        $this->db->where('user_id', $this->session->userdata('USER_ID'));

        return $this->db->get()->row();
    }

    public function get_sidebar_position()
    {
        $this->db->select('sidebar_position');
        $this->db->from('configuration');
        $this->db->where('user_id', $this->session->userdata('USER_ID'));

        return $this->db->get()->row();
    }

    public function get_background()
    {
        $this->db->select('background');
        $this->db->from('configuration');
        $this->db->where('user_id', $this->session->userdata('USER_ID'));

        return $this->db->get()->row();
    }
}

/* End of file template_skin_model.php */
/* Location: ./app/models/template_skin_model.php */
