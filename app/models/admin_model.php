<?php

/**
 * Class admin_model
 * @package		App\Models
 */
class admin_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();

        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model(['message_model', 'relatorios_model', 'arquivos_model']);
        $arrServers = $this->arquivos_model->get_servers();
        $this->load->vars(array('servers' => $arrServers));
        $this->accesscontrol->verify($this->router->fetch_class(), $this->router->fetch_method());
    }

    public function index()
    {
        if($this->session->userdata('USER_ROLE') != 'INDEFINIDO'){

            $users = $this->message_model->fetchMyUsers();

            $novos = $this->relatorios_model->get_usuarios_mes();

            $this->vet_dados['total_novos'] = 0;

            for($i=0;$i<count($novos);$i++){
                if($i+1 != count($novos))
                $novos[$i]->total = $novos[$i]->total.",";
                $this->vet_dados['total_novos'] += $novos[$i]->total;

            }

            $novosArquivos = $this->relatorios_model->total_arquivos_mes();
            $this->vet_dados['total_arquivos_novos'] = 0;

            for($i=0;$i<count($novosArquivos);$i++){
                if($i+1 != count($novosArquivos))
                    $novosArquivos[$i]->total = $novosArquivos[$i]->total.",";
                $this->vet_dados['total_arquivos_novos'] += $novosArquivos[$i]->total;

            }

            $arquivosUsuarios = $this->relatorios_model->arquivos_usuarios();

            $this->vet_dados['total_arquivos_usuario'] = 0;

            for($i=0;$i<count($arquivosUsuarios);$i++){
                if($i+1 != count($arquivosUsuarios))
                    $arquivosUsuarios[$i]->total = $arquivosUsuarios[$i]->total.",";
                $this->vet_dados['total_arquivos_usuario'] += $arquivosUsuarios[$i]->total;

            }

            $tipos = $this->relatorios_model->arquivos_tipo();


            $empresas = $this->relatorios_model->arquivos_empresas();
            $usuarios = $this->relatorios_model->top_usuarios();

            $this->vet_dados['tipos'] = $tipos;
            $this->vet_dados['tipos_bar'] = $tipos;
            $this->vet_dados['empresas'] = $empresas;
            $this->vet_dados['usuarios'] = $usuarios;
            $this->vet_dados['novos'] = $novos;
            $this->vet_dados['novos_arquivos'] = $novosArquivos;
            $this->vet_dados['arquivos_usuario'] = $arquivosUsuarios;

//            Dd::Test($this->vet_dados['tipos']);


            for($i=0;$i<count($users);$i++){
                $users[$i]->user_picture = $users[$i]->user_picture != '' ? base_url() .'public/static/'.$users[$i]->user_picture : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAGJ0lEQVR4Xu2bZah1RRSGn8/uTuzCDhRBRRFbRDAxEQO7EwsTsRvEBgtbfxhYoIItdv+wCwwUu4Pnsvdl7nz7nLP3mTn3nBvr33fPxLvevWbNivmmMMFlygTXn0kCJi1ggjMweQRG0QBmB7YGNgZWB5YG5ij2/wH4CHgNeBx4APh5NLCNhgUsB5wA7ArMXFOpX4FbgfOA92vO6WpYLwlQ2TOBI4HpukIHfwEXA6cDv3e5RttpvSJgWeBeYNVMoF8Htgc+zLTe8DK9IGAN4BFggQqwHwO3AU8AbwPfFmPmA1Yu/INHZYmKuV8BmwNv5CQhNwF++WcqlP8EOB64C/i3gwLTADsX53+xaKwkrJfTEnIS4Jl/ocLsdWb7A780/HKzAdcCu0TzvCnWAf5ouF7l8JwEnA8cF+1yEXBsItBLCkcaLnMucGLiukPTcxHgVfdO5O398rtnACnG24GdgrW8HVYEPkhdPxcB1wP7BGA88zq1pmbfSh+DKAleNBjg8fBoJUkOAgSncwqDHM/tHUnIpp6sNd0S/FlyF0wlOQcBXluaeyledcvU8PZN+Zm28P6LBxO9Le5sulA4PgcBmuK+waLnACelgGozN3a0VwMHpuyVg4AXgbUDEAYrj6WAajN3S+Ch4PfngXVT9spBgNHcvAGIRYAvU0C1masT/Cz4/evCD3S9XQ4C/gSmDxDMCPi3Xohrh0mR+/i3rmWsE/BPQqY5RFoOAr4BTGZKGc0j4PGbv+vPn4mAfjrBlyIH3JiLHBZwDbBfsHO2OL1Cmwui3MK9D2isdTAhBwFGfeb4pRgGW+/rlPY2xW0gZN0wTJEHIhAybTUUniXQaLeIlKbKVo3fA7gp+MFQeKHU4mkOCxBTHA16V6+UCi5Q1uqxyZAOtpSrgINSmc1FgJUgAYbxgCmseUKqiNF4f8dgIYshKwDmHUmSiwBB6Pwse4VyKXA08F+XKMXnGodH888CTulyzRHTchIwE2BsbtMjFL+eydJPDQFr9tYZwi/vEi8XdcEs0WZOAgSn93+2Ij7/vGiOeCyM3tqJ3l4nalYZnnnneLOYeL3SkMyWw3MT4EarAY+2SFI+LQoltr/eAowiFaO5VYBNiopwXA0OFbBldllBUHLFqRcElJZwD2CPoFeiVR0C3JeyQa8IEJM+4TTgmOh2aIJXb38/sBkwZ4uJXsFHAL81Wbgc2y0BmuhRRT3uQsDubiuxPObt4LmetSZIzdygxwqQkaXzDLddx+AnFh3jtoBW0Ui6IcCz6hlfuNjJHuAONXY1Ytwqao/PBej0vi/CXJ2b/uHBFsVOiTi1ID+MOdz+C2BT4L0aWIaHNCXAZqd9vbACJOvtnFYTPHXHrlVUiA2GQtGpbgi8W3ehJgTYsPSej03QHr79/9EWy/E3A9tEGxuG2z+sdRzqEmCio/Jxuzs10kslzUbqFRWVYX3C+nXeFNQl4AZgzwitPTvD3EGQKytIuC6qU1TirEOADu7uaLZdHxOdbmP83KRpCTrj+Dj477ZxQicCjMf1qqXHF/ibRXvadzyDJPoEy3OhY/RmWL5d+6wTAXGGZwKyZvG6Y5CUL7GITV8VXpFnAye3AtuOABuPlqDCpucZxYOlQVS+xBS3z8wXlgye44zA3o4As7HwetOcLHz05LVWRkYNlnw34AcspaUVtCLAOF6F5wkWORjQ244F8Wmet1QpBkgGa1M9q2lFgNVWc/dSbEC4wKB//RKvVuAHDBMoX5j4SKvWEfBK2S4YacITv/8ZdEu4HDgsAKny4TOboZ+qLGAG4Lsoc9O7vjroGkf4fEn2XPA3M8y5gb/DcVUEGEI+FQzqR7KTg2t1s18R9g7NEUJSKi3AAoYmX8qNwF45EPVhDf2Y/qwUaxjmL8NSZQEWIuzClHJokXD0AX/ylr5RtJ9YijnN3p0IsKobPjvZCHgyGUp/FtgCeDjY+mlgg04EWLkNCxyWuo0Ix6KYB4QVIjtJS3UiQG9Zt3Y31kj5MS6uVvkAGxeml+NRvAJH1BKrCPDlVdKzkwFmzmtxREmvigCLCL68qPoPDwOsW0doKm9p3T5D22uw40rjaUCngsh40rVSl0kCxv0n7qDg/4zyCFAOcj9oAAAAAElFTkSuQmCC';
                $users[$i]->selected = $users[$i]->user_login == $this->uri->segment(3) ? ' active ' : '';
            }

            $this->vet_dados['usuario'] = $users;

            return $this->parser->parse('admin/admin_home_view', $this->vet_dados, TRUE);
        }

    }


    public function home()
    {
        switch($this->session->userdata('USER_ROLE')){
            case 'SUPERUSUARIO':
                return $this->parser->parse('admin/home/home_super_view', $this->vet_dados, TRUE);
            case 'CLIADMIN':
                return $this->parser->parse('admin/home/home_client_view', $this->vet_dados, TRUE);
            case 'CLIPESSOA':
                return $this->parser->parse('admin/home/home_pessoa_view', $this->vet_dados, TRUE);
        }

    }


    public function set_client_scope()
    {

        if($this->input->post('scope') != ''){
            $user_type_id = $this->input->post('scope') == 'pessoal' ?  3 : 2;
            $this->db->set('user_type_id', $user_type_id);
        }

        $this->db->where('user_id', $this->session->userdata('USER_ID'));
        $this->db->update('usuario');

        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }

    }

    public function setFields($object)
    {
        foreach($object as $key => $value){
            if($value != null && $key != 'client_id'){
                $this->db->set($key, $value);
            }
        }
    }
}