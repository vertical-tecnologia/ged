<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 06/06/15
 * Time: 18:19
 */

class action_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('module_model');
    }

    public function index()
    {
        $entity = new ActionEntity();
        $this->vet_dados['action'] = $this->fetchAll($entity);

        $this->vet_dados['nome_tela'] = 'Gerenciar Ações';
        $this->vet_dados['tela']      = 'acao';

        return $this->parser->parse('action/action_list_view', $this->vet_dados, TRUE);
    }

    public function cadastro()
    {
        $vet['action_id'] = set_value('action_id');
        $vet['action_name'] = set_value('action_name');
        $this->vet_dados['modules'] = $this->module_model->fetchAll(new ModuleEntity());

        $this->vet_dados['action'] = array($vet);

        return $this->parser->parse('action/action_cad_view', $this->vet_dados, TRUE);
    }

    public function fetch(ModuleEntity $entity)
    {
        $this->db->from('acao');
        $this->db->where('action_id',$entity->module_id);
        return $this->db->get()->row();
    }

    public function fetchAll(ActionEntity $entity)
    {
        $this->db->select('acao.*, modulo.module_name');
        $this->db->from('acao');
        $this->db->join('modulo', 'modulo.module_id = acao.action_module_id');

        if($entity->action_id != '')
            $this->db->where('action_id',$entity->action_id);

        if($entity->action_module_id != '')
            $this->db->where('action_module_id',$entity->action_module_id);

        $this->db->order_by('modulo.module_name');

        return $this->db->get()->result();
    }

    public function save(ActionEntity $entity)
    {



        if($this->valida() === FALSE){
            return $this->cadastro();
        }

        $this->db->trans_begin();

        $this->setFields($entity);

        $id = (int) $entity->action_id;

        if($id == 0){
            $this->db->insert('acao');
            $entity->action_id = $this->db->insert_id();
            if($entity->action_id > 0){
                $this->session->set_flashdata('success', 'Ação cadastrada com sucesso.');
                syslog::generate_log('NEW_ACTION_SUCCESS');
                $this->db->trans_commit();
                return true;
            } else {
                $this->session->set_flashdata('error', 'Erro ao cadastrar ação.');
                syslog::generate_log('NEW_ACTION_ERROR');
                $this->db->trans_rollback();
                return false;
            }
        } else {
            $this->db->where('action_id', $entity->action_id);
            $this->db->update('acao');
            echo $this->db->last_query();
            if($this->db->affected_rows() > 0){
                $this->session->set_flashdata('success', 'Ação alterada com sucesso.');
                $this->db->trans_commit();
                syslog::generate_log('UPDATE_ACTION_SUCCESS');
                return true;
            } else {
                $this->session->set_flashdata('error', 'Erro ao alterar ação.');
                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_ACTION_ERROR');
                return false;
            }
        }

    }

    public function fetchAcaoPermissao(stdClass $entity)
    {
        $this->db->select('acao.*, modulo.module_name, permissao.permissao_allow');
        $this->db->from('acao');
        $this->db->join('modulo', 'modulo.module_id = acao.action_module_id');
        $this->db->join('permissao', 'modulo.module_id = permissao.permissao_module_id AND acao.action_id = permissao.permissao_action_id ');

        if($entity->permissao_type_id != '')
            $this->db->where('permissao.permissao_type_id',$entity->permissao_type_id);

        if($entity->permissao_module_id != '')
            $this->db->where('permissao_module_id',$entity->permissao_module_id);

        return $this->db->get()->result();
    }

    public function delete(ActionEntity $entity)
    {
        $id = (int) $entity->action_id;
        if($id == 0){
            syslog::generate_log('DELETE_ACTION_SUCCESS');
            return false;
        }
        else {
            $this->db->where('action_id', $entity->action_id);
            syslog::generate_log('DELETE_ACTION_ERROR');
            return $this->db->delete('acao');
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'action_id')
                continue;
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('actions');
    }

}