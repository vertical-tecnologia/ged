<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 25/06/15
 * Time: 16:39
 */

use OpenBoleto\Agente;
use OpenBoleto\Banco\Itau;

class faturamento_model extends CI_Model{

    public $vet_dados;
    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
        $this->load->model('cliente_model');

    }

    public function index()
    {
        if(in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN', 'CLIPESSOA'))){
            $this->vet_dados['hidden'] = " hide";
        }

        $entity = new FaturaEntity();
        $fat = $this->fetchAll($entity);

        for($i=0;$i<count($fat);$i++){

            $fat[$i]->fatura_emissao = data_formatada($fat[$i]->fatura_emissao, 3);
            $fat[$i]->fatura_vencimento = data_formatada($fat[$i]->fatura_vencimento, 3);
            $fatEntity = new FaturaEntity();
            $fatEntity->fatura_id = $fat[$i]->fatura_id;
            $fatura_itens = $this->fetchFaturaItens($fatEntity);

            $fatura_itens_adicional = $this->fetchFaturaAdicionalItens($fatEntity);

            for($k=0;$k<count($fatura_itens_adicional); $k++){
                $fat[$i]->fatura_valor += $fatura_itens_adicional[$k]->price;
            }

            for($j=0;$j<count($fatura_itens); $j++){
                $fat[$i]->fatura_valor += $fatura_itens[$j]->service_price;
            }
            $fat[$i]->fatura_valor = type_format($fat[$i]->fatura_valor , 'brl');
        }

        $this->vet_dados['faturas'] = $fat;
        $this->vet_dados['nome_tela'] = 'Gerenciar Faturamento';
        $this->vet_dados['tela']      = 'faturamento';

        return $this->parser->parse('invoice/invoice_list_view', $this->vet_dados, TRUE);

    }

    public function detalhes(FaturaEntity $entity)
    {
        $this->vet_dados['nome_tela'] = 'Gerenciar Faturamento';
        $this->vet_dados['tela']      = 'faturamento';

        $data = $this->fetch($entity);

        if($data->client_empresa_id != ''){
            $empresa = new ClienteEntity();
            $empresa->client_empresa_id = $data->client_empresa_id;
            $d['cliente'] = $this->cliente_model->fetchClienteEmpresa($empresa);

            $client_view = $this->parser->parse('invoice/empresa_invoice_view', $d['cliente'], TRUE);
        } else {
            $pessoa = new ClienteEntity();
            $pessoa->client_user_id = $data->client_user_id;
            $d['cliente'] = $this->cliente_model->fetchClientePessoal($pessoa);
            $client_view = $this->parser->parse('invoice/pessoa_invoice_view', $d['cliente'], TRUE);
        }

        $data->fatura_emissao = data_formatada($data->fatura_emissao, 3);
        $data->fatura_vencimento = data_formatada($data->fatura_vencimento, 3);

        $fatura_itens = $this->fetchFaturaItens($entity);

        $fatura_itens_adicional = $this->fetchFaturaAdicionalItens($entity);


        for($i=0;$i<count($fatura_itens); $i++){
            $data->fatura_valor += $fatura_itens[$i]->service_price;
        }

        for($i=0;$i<count($fatura_itens_adicional); $i++){
            $data->fatura_valor += $fatura_itens_adicional[$i]->price;
        }

        $data->fatura_valor = type_format($data->fatura_valor , 'brl');

        $this->vet_dados['fatura'] = array($data);
        $this->vet_dados['fatura_itens'] = $fatura_itens;
        $this->vet_dados['fatura_itens_adicional'] = $fatura_itens_adicional;
        $company = $this->company();
        $this->vet_dados['company'] = array($company);
        $this->vet_dados['client_view'] = $client_view;

        $this->vet_dados['vencimento'] = strtotime(type_format($data->fatura_vencimento, 'data'));
        $this->vet_dados['now'] = strtotime(date('Y-m-d'));

        return $this->parser->parse('invoice/invoice_view', $this->vet_dados, TRUE);
    }

    public function boleto(FaturaEntity $entity)
    {
        $via = $this->uri->segment(4) != 'via' ? false : true;

        $banco = $this->faturamento_model->fetchBanco();
        $fatura = $this->fetch($entity);
        $faturaEntity = new FaturaEntity();
        $faturaEntity->fatura_id = $fatura->fatura_id;

        $fatura_itens = $this->fetchFaturaItens($faturaEntity);
        $fatura_itens_adicional = $this->fetchFaturaAdicionalItens($entity);

        $valor = 0;

        $empresa = new ClienteEntity();
        $empresa->client_empresa_id = $fatura->client_empresa_id;
        $cliente = $this->cliente_model->fetchClienteEmpresa($empresa);
        $cedente = new Agente($cliente->empresa_razao, $cliente->empresa_cnpj, $cliente->endereco_logradouro.','.$cliente->endereco_num.', '.$cliente->endereco_bairro, $cliente->endereco_cep, $cliente->endereco_cidade, $cliente->endereco_estado);

        for($i=0;$i<count($fatura_itens); $i++){
            $valor += $fatura_itens[$i]->service_price;
        }

        for($i=0;$i<count($fatura_itens_adicional); $i++){
            $valor += $fatura_itens_adicional[$i]->price;
        }

        $stop_date = new DateTime();
        $stop_date->modify('+1 day');

        if($via == true && strtotime($fatura->fatura_vencimento) < strtotime(date('Y-m-d h:i:s')))
            $fatura->fatura_vencimento = $stop_date->format('Y-m-d');

        $company = $this->company();
        $sacado = new Agente($company->company_razao, $company->company_cnpj, $company->company_endereco.' '.$company->company_complemento, $company->company_cep, $company->company_estado, $company->company_cidade);

        $boleto = new Itau(array(
            // Parâmetros obrigatórios
            'dataVencimento' => new DateTime($fatura->fatura_vencimento),
            'valor' => $valor,
            'sequencial' => $fatura->fatura_id, // Para gerar o nosso número
            'sacado' => $sacado,
            'cedente' => $cedente,
            'agencia' => $banco->bank_acodigo, // Até 4 dígitos
            'carteira' => $banco->bank_carteira,
            'conta' => $banco->bank_conta, // Até 8 dígitos
            'convenio' => $banco->bank_contrato, // 4, 6 ou 7 dígitos
            'instrucoes' => array( // Até 8
                'Após o dia '.type_format($fatura->fatura_vencimento, 'data_br').' cobrar 2% de mora e 1% de juros ao dia.',
                'Não receber após o vencimento.',
            ),

        ));

        echo $boleto->getOutput();
    }

    public function enviar_fatura(FaturaEntity $entity)
    {
        $this->load->model('usuario_model');

        $fatura = $this->fetch($entity);

        $empresa = new ClienteEntity();
        $empresa->client_empresa_id = $fatura->client_empresa_id;
        $cliente = $this->cliente_model->fetchClienteEmpresa($empresa);
        $user = new UsuarioEntity();
        $user->user_id = $cliente->empresa_user_id;
        $usuario = $this->usuario_model->fetch($user);

        $dados = new stdClass();

        $vet = new stdClass();
        $vet->user_fullname = $usuario->user_fullname;
        $vet->fatura_id = $fatura->fatura_hash;

        $dados->user_fullname = $usuario->user_fullname;
        $dados->user_email = $usuario->user_email;
        $this->vet_dados['fatura'] = array($vet);

        return $this->enviar_email($dados);

    }

    protected function enviar_email($dados){


        $this->load->library('Mailer');

        Mailer::$message = $this->parser->parse('template/public/usuario_fatura_email_view', $this->vet_dados, TRUE);
        Mailer::$subject = "[GED] - Nova Fatura!";
        Mailer::$to = $dados->user_email;
        Mailer::$toName = $dados->user_fullname;
        $m = Mailer::send_mail();

        if($m == true){
            syslog::generate_log("NEW_USER_EMAIL_SUCCESS");
            return true;
        }
        else
        {
            syslog::generate_log("NEW_USER_EMAIL_ERROR");
            return false;
        }
    }

    public function cadastro()
    {
        $vet = array();
        $vet['fatura_id']           = set_value('fatura_id');
        $vet['user_id']             = $this->session->userdata('USER_ID');
        $vet['fatura_emissao']      = date('Y-m-d h:i:s');
        $vet['fatura_vencimento']   = set_value('fatura_vencimento');
        $vet['client_id']           = set_value('client_id');
        $vet['fatura_obs']          = set_value('fatura_obs');

        $this->vet_dados['faturamento'] = array($vet);

        $clientes = $this->cliente_model->fetchAllClienteEmpresa(new ClienteEntity());

        for($i=0;$i<count($clientes);$i++){
            $dia = date('d', strtotime($clientes[$i]->empresa_register_date)) + 10;
            $mes = date('m') + 1;
            $clientes[$i]->data_vencimento = $dia.'/'.$mes.'/'.date('Y');
            $clientes[$i]->cli_id = $clientes[$i]->client_id;

        }

        $this->vet_dados['servicos'] = $this->fetchServicos();
        $this->vet_dados['clientes'] = $clientes;

        return $this->parser->parse('invoice/invoice_cad_view', $this->vet_dados, TRUE);
    }

    public function fetch(FaturaEntity $entity)
    {
        $this->db->select('faturas.*, fatura_status.status_name, (SELECT SUM(item_valor) FROM fatura_itens WHERE fatura_itens.fatura_id = faturas.fatura_id) as fatura_valor, cliente.client_user_id, cliente.client_empresa_id');
        $this->db->from('faturas');
        $this->db->join('fatura_status', 'faturas.status_id = fatura_status.status_id');
        $this->db->join('cliente', 'cliente.client_id = faturas.client_id');

        if($entity->fatura_id != '')
            $this->db->where('fatura_id', $entity->fatura_id);

        if($entity->fatura_hash != '')
            $this->db->where('fatura_hash', $entity->fatura_hash);

        return $this->db->get()->row();

    }

    public function fetchAll(FaturaEntity $entity)
    {
        $this->db->select('faturas.*, fatura_status.status_name, (SELECT SUM(item_valor) FROM fatura_itens WHERE fatura_itens.fatura_id = faturas.fatura_id) as fatura_valor, COALESCE( usuario.user_fullname, empresa.empresa_fantasia) as client_name ', false);
        $this->db->from('faturas');
        $this->db->join('fatura_status', 'faturas.status_id = fatura_status.status_id');
        $this->db->join('cliente', 'cliente.client_id = faturas.client_id');

        if(in_array($this->session->userdata('USER_ROLE'), array('CLIADMIN', 'CLIPESSOA'))){

            $this->db->where('faturas.client_id' , $this->session->userdata('CLID'));

        }

        $this->db->join('usuario', 'cliente.client_user_id = usuario.user_id', 'LEFT');
        $this->db->join('empresa', 'cliente.client_empresa_id = empresa.empresa_id', 'LEFT');

        return $this->db->get()->result();
    }

    public function save(FaturaEntity $entity)
    {
        $this->db->trans_begin();

        $this->setFields($entity);

        $id = (int) $entity->fatura_id;
        if($id == 0){
            $this->db->insert('faturas');
            $entity->fatura_id = $this->db->insert_id();

            if($entity->fatura_id > 0 ){
                syslog::generate_log('NEW_INVOICE_SUCCESS');
                return $entity;
            } else {
                $this->db->trans_rollback();
                syslog::generate_log('NEW_INVOICE_ERROR');
                return false;
            }
        } else {
            $this->db->where('fatura_id', $entity->fatura_id);
            if($this->db->update('faturas')){
                syslog::generate_log('UPDATE_INVOICE_SUCCESS');
                return $entity;
            }
            else{
                $this->db->trans_rollback();
                syslog::generate_log('UPDATE_INVOICE_ERROR');
                return false;
            }
        }

    }

    public function saveItens(FaturaEntity $entity){

        $id = (int) $entity->fatura_id;
        $data = array();

        $adicional = $this->fetchServicosAdicionais($entity->client_id);

        for($i=0; $i<count($adicional);$i++){
            $data[] = array('fatura_id' => $entity->fatura_id, 'service_id' => $adicional[$i]->id, 'adicional' => true);
        }


        if($id == 0){
            $this->db->trans_rollback();
            return false;
        } else {
            foreach($entity->fatura_itens as $key => $val) {
                $data[] = array('fatura_id' => $entity->fatura_id, 'service_id' => $val, 'adicional' => false);
            }

            if($data != null)
            $batch = $this->db->insert_batch('fatura_itens', $data);
            else $batch =null;

            if($batch != null){
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
    }

    protected function company()
    {
        $this->db->from('compania');
        return $this->db->get()->row();
    }

    protected function fetchFaturaItens(FaturaEntity $entity)
    {
        $this->db->from('fatura_itens');
        $this->db->join('servico', 'fatura_itens.service_id = servico.service_id');

        $this->db->where('fatura_id', $entity->fatura_id);
        $this->db->where('adicional', false);

        return $this->db->get()->result();
    }

    protected function fetchFaturaAdicionalItens(FaturaEntity $entity)
    {
        $this->db->from('fatura_itens');
        $this->db->join('espaco_adicional', 'espaco_adicional.id = fatura_itens.service_id');
        $this->db->join('quantidade_armazenamento', 'quantidade_armazenamento.quant_id = espaco_adicional.quant_id');

        $this->db->where('fatura_id', $entity->fatura_id);
        $this->db->where('adicional', true);

        return $this->db->get()->result();
    }

    public function get_cliente(ClienteEntity $entity)
    {
        $this->db->from('cliente');
        $this->db->where('client_id', $entity->client_id);

        return $this->db->get()->row();
    }

    public function get_cliente_servicos(PlanoEntity $entity)
    {
        $this->db->from('servico');
        $this->db->join('plano_servico', 'plano_servico.service_id = servico.service_id');
        $this->db->where('plano_servico.plan_id', $entity->plan_id);

        return $this->db->get()->result();

    }

    public function fetchBanco()
    {
        $this->db->from('banco');
        return $this->db->get()->row();
    }

    public function update_status(FaturaEntity $entity)
    {
        $this->db->set('status_id', $entity->status_id);
        $this->db->where('fatura_id', $entity->fatura_id);

        return $this->db->update('faturas');
    }

    protected function fetchServicos()
    {
        $this->db->from('servico');
        return $this->db->get()->result();
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'fatura_id' || $key == 'fatura_itens') continue;
            $this->db->set($key, $value);
        }
    }


    public function fetchServicosAdicionais($clientId)
    {
        $this->db->select('espaco_adicional.price, espaco_adicional.id');
        $this->db->from('espaco_adicional');
        $this->db->join('quantidade_armazenamento', 'quantidade_armazenamento.quant_id = espaco_adicional.quant_id');

        $this->db->where('client_id', $clientId);

        return $this->db->get()->result();
    }

}