<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 06/06/15
 * Time: 16:37
 */

class module_model extends CI_Model{

    public $vet_dados;

    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir($this->session->userdata('skin'));
    }

    public function index()
    {
        $entity = new ModuleEntity();
        $this->vet_dados['module'] = $this->fetchAll($entity);

        $this->vet_dados['nome_tela'] = 'Gerenciar Módulos';
        $this->vet_dados['tela']      = 'module';

        return $this->parser->parse('module/module_list_view', $this->vet_dados, TRUE);
    }

    public function cadastro()
    {
        $vet['module_id'] = set_value('module_id');
        $vet['module_name'] = set_value('module_name');

        $this->vet_dados['module'] = array($vet);

        return $this->parser->parse('module/module_cad_view', $this->vet_dados, TRUE);
    }

    public function fetch(ModuleEntity $entity)
    {
        $this->db->from('modulo');
        $this->db->where('module_id',$entity->module_id);
        return $this->db->get()->row();
    }

    public function fetchAll(ModuleEntity $entity)
    {
        $this->db->from('modulo');

        if($entity->module_id != '')
            $this->db->where('module_id',$entity->module_id);

        if($entity->module_system === false)
            $this->db->where('module_system', $entity->module_system);

        return $this->db->get()->result();

    }

    public function save(ModuleEntity $entity)
    {
        if($this->valida() === FALSE)
            return $this->cadastro();

        $this->db->trans_begin();

        $this->setFields($entity);

        $id = (int) $entity->module_id;

        if($id == 0){
            $this->db->insert('modulo');
            $entity->module_id = $this->db->insert_id();
            if($entity->module_id > 0){
                $this->db->trans_commit();
                $this->session->set_flashdata('success', 'Módulo cadastrado com sucesso.');
                syslog::generate_log("NEW_MODULE_SUCCESS");
                return true;
            } else {
                $this->session->set_flashdata('error', 'Erro ao cadastrar módulo.');
                $this->db->trans_rollback();
                syslog::generate_log("NEW_MODULE_ERROR");
                return false;
            }
        } else {
            $this->db->where('module_id', $entity->module_id);
            $this->db->update('modulo');
            echo $this->db->last_query();
            if($this->db->affected_rows() > 0){
                $this->session->set_flashdata('success', 'Módulo alterado com sucesso.');
                $this->db->trans_commit();
                return true;
            } else {
                $this->session->set_flashdata('error', 'Erro ao alterar módulo.');
                $this->db->trans_rollback();
                return false;
            }
        }

    }

    public function delete(ModuleEntity $entity)
    {
        $id = (int) $entity->module_id;

        if($id >0){
            $this->db->where('module_id', $id);
            $this->session->set_flashdata('success', 'Módulo excluído com sucesso.');
            syslog::generate_log("DELETE_MODULE_SUCCESS");
            return $this->db->delete('modulo');

        } else {
            $this->session->set_flashdata('error', 'Erro ao excluir módulo.');
            syslog::generate_log("DELETE_MODULE_ERROR");
            return false;
        }
    }

    protected function setFields($fields)
    {
        foreach($fields as $key => $value){
            if($key == 'module_id')
                continue;
            $this->db->set($key, $value);
        }
    }

    private function valida()
    {
        $this->form_validation->set_message('required', 'Campo obrigatório');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        return $this->form_validation->run('modulo');
    }
}