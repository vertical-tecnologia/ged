<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Models
 * Date: 30/05/15
 * Time: 01:07
 */

class auth_model extends CI_Model{

    public $vet_dados;
    public function __construct()
    {
        parent::__construct();
        $this->vet_dados = base_dir('public');
        $this->load->model('plano_model');

    }
    public function index()
    {
        $planos = $this->plano_model->fetchAll(array('plan_status_id' => 1));

        for($i=0;$i<count($planos);$i++){
            $planos[$i]->servicos = $this->servico_model->fetchAllServiceUser(array('plan_id' => $planos[$i]->plan_id));
        }

        $this->vet_dados['planos'] = $planos;

        return $this->parser->parse('cadastro/cadastro_plans_view', $this->vet_dados, TRUE);
    }

}