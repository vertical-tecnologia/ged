<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 12/07/15
 * Time: 18:47
 */

class ServerEntity {
    public $server_id;
    public $server_type;
    public $server_path;
    public $server_key;
    public $server_secret;
    public $server_address;
    public $server_port;
    public $server_username;
    public $server_password;
    public $server_is_default;
    public $client_id;
    public $is_admin;
    public $empresa_id;
    public $description;
    public $directory_monitor;
}