<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 21/07/15
 * Time: 10:31
 */

class MessageEntity {
    public $id;
    public $from_user;
    public $to_user;
    public $content;
    public $sent;
    public $viewed;
    public $time_viewed;
}