<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 30/05/15
 * Time: 21:24
 */

class DiretorioEntity {

    public $dir_id;
    public $dir_name;
    public $dir_parent_id;
    public $dir_segmento_id;

    public function getArrayCopy()
    {
        return array(
            'dir_id' => $this->dir_id,
            'dir_name' => $this->dir_name,
            'dir_parent_id' => $this->dir_parent_id,
            'dir_segmento_id' => $this->dir_segmento_id,
        );
    }

    public function exchangeArray($data)
    {
        $this->dir_id = $data['dir_id'];
        $this->dir_name = $data['dir_name'];
        $this->dir_parent_id = $data['dir_parent_id'];
        $this->dir_segmento_id = $data['dir_segmento_id'];
    }
}