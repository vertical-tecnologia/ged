<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 11/07/15
 * Time: 13:04
 * @author     Vagner Leitte <vagnerleitte@outlook.com>
 * @version    1.0
 */

class GroupUserEntity {

    /**
     * The unique id of group user identification on system
     * @access public
     * @var int $group_user_id
     */
    public $group_user_id;
    /**
     * The group id key
     * @access public
     * @var int $group_id
     */
    public $group_id;

    /**
     * The user id key
     * @access public
     * @var int $user_id
     */
    public $user_id;

    /**
     * The date and time when user was added to group
     * @access public
     * @var datetime $group_user_added
     */
    public $group_user_added;

}