<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 27/05/15
 * Time: 21:56
 */

class ServicoEntity
{

    public $service_id;
    public $service_name;
    public $service_desc;
    public $service_price;
    public $service_recursive;
    public $service_storage;
    public $service_status_id;
    public $service_editable;


    public function getArrayCopy()
    {
        return array(
             'service_id' =>   $this->service_id,
             'service_name' =>   $this->service_name,
             'service_desc' =>   $this->service_desc,
             'service_price' =>   $this->service_price,
             'service_recursive' =>   $this->service_recursive,
             'service_storage' =>   $this->service_storage,
             'service_status_id' =>   $this->service_status_id,
            'service_editable' => $this->service_editable
        );
    }

    public function exchangeArray($data)
    {
        $this->service_id = $data['service_id'];
        $this->service_name = $data['service_name'];
        $this->service_desc = $data['service_desc'];
        $this->service_price = $data['service_price'];
        $this->service_recursive = $data['service_recursive'];
        $this->service_storage = $data['service_storage'];
        $this->service_status_id = $data['service_status_id'];
        $this->service_editable = $data['service_editable'];
    }
}