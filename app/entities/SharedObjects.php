<?php
/**
 * Created by PhpStorm.
 * @created 11/07/15 13:35
 * @author Vagner Leitte <vagnerleitte@outlook.com>
 * @version 1.0
 * @package		App\Entities
 *
 */

class SharedObjects {


    /**
     * The primary key on the database
     * @var int $share_id;
     */
    public $share_id;
    /**
     * The public hash identification of the shared object on the system.
     * @var string $share_hash;
     */
    public $share_hash;
    /**
     * The id of shared object
     * @var int $share_object
     */
    public $share_object;
    /**
     * The expiration date of this shared object. If is null the shared object is permanently
     * @var datetime $share_expiration
     */
    public $share_expiration;
    /**
     * The permissions of the shared element.
     * This permissions can be:
     * 1 - read
     * 2 - read and write
     * 3 - downladed only
     *
     * @var int $share_permission
     */
    public $share_permission;
    /**
     * The user id that can access this shared object
     * @var int $share_user
     */
    public $share_user;
    /**
     * The email address that receive the shared object out of the system.
     * @var string $share_email
     */
    public $share_email;

}