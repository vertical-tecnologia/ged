<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 28/06/15
 * Time: 19:54
 */

class EmpresaUsuarioEntity {

    public $id_empresa_usuario;
    public $id_empresa;
    public $id_usuario;

    public function getArrayCopy()
    {
        return array(
            'id_empresa_usuario' => $this->id_empresa_usuario,
            'id_empresa' => $this->id_empresa,
            'id_usuario' => $this->id_usuario
        );
    }

    public function exchangeArray($data)
    {
        $this->id_empresa_usuario = $data['id_empresa_usuario'];
        $this->id_empresa = $data['id_empresa'];
        $this->id_usuario = $data['id_usuario'];
    }

}