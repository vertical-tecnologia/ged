<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 17/07/15
 * Time: 19:18
 */

class DownloadEntity {

    public $link_id;
    public $hash;
    public $user_key;
    public $generated;
    public $expires;
    public $downloads;
    public $key;
    public $downloaded;
    public $ip_address;
    public $single;
    public $user_agent;

}