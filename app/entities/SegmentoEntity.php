<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 30/05/15
 * Time: 11:35
 */

class SegmentoEntity {

    public $segmento_id;
    public $segmento_name;
    public $segmento_status_id;

    public function getArrayCopy()
    {
        return array(
            'segmento_id' => $this->segmento_id,
            'segmento_name' => $this->segmento_name,
            'segmento_status_id' => $this->segmento_status_id,
        );
    }

    public function exchangeArray($data)
    {
        $this->segmento_id = $data['segmento_id'];
        $this->segmento_name = $data['segmento_name'];
        $this->segmento_status_id = $data['segmento_status_id'];
    }

}