<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 04/06/15
 * Time: 16:44
 */

class ClienteEntity {

    public $client_id;
    public $client_user_id;
    public $client_empresa_id;
    public $client_volume;
    public $client_key;
    public $client_local;
    public $client_plan_id;
    public $client_data_vencimento;
    public $server_id;

    public function getArrayCopy()
    {
        return array(
            'client_id' =>$this->client_id,
            'client_user_id' =>$this->client_user_id,
            'client_empresa_id' =>$this->client_empresa_id,
            'client_volume' => $this->client_volume,
            'client_key' => $this->client_key,
            'client_local' => $this->client_local,
            'client_plan_id' => $this->client_plan_id,
            'client_data_vencimento' => $this->client_data_vencimento,
            'server_id' => $this->server_id

        );
    }

    public function exchangeArray($data)
    {
        $this->client_id = $data['client_id'];
        $this->client_user_id = $data['client_user_id'];
        $this->client_empresa_id = $data['client_empresa_id'];
        $this->client_volume = $data['client_volume'];
        $this->client_key = $data['client_key'];
        $this->client_local = $data['client_local'];
        $this->client_plan_id = $data['client_plan_id'];
        $this->client_data_vencimento = $data['client_data_vencimento'];
        $this->server_id = $data['server_id'];
    }

}