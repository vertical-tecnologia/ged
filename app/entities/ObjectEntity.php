<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 11/07/15
 * Time: 12:58
 */

class ObjectEntity {

    /**
     * The object id on the database
     * @var int $id
     */
    public $id;
    /**
     * The hash name based for the stored file on system
     * @var string $hash
     */
    public $hash;
    /**
     * The original name for the uploaded object
     * @var string $original_name
     */
    public $original_name;
    /**
     * The full path of the object on the system
     * This value consists on the Storage Driver id for the company and the virtual path of the object on the system
     * @var string $full_path
     */
    public $full_path;
    /**
     * The type of the object stored on the system
     * They can be file (1) or directory (2)
     * @var int $type
     */
    public $type;
    /**
     * The mime tipe of the object stored. This property has been found on the database from
     * the value returned from the uploaded object
     * @var int $mime
     */
    public $mime;
    /**
     * The object size in bytes
     * @var int $size
     */
    public $size;
    /**
     * The object extension
     * @var string $extension
     */
    public $extension;
    /**
     * The owner hash. This property store the hash id for the user that upload or create the object on system.
     * @var string $owner
     */
    public $owner;
    /**
     * The storage key hash of the company
     * @var string $storage_key
     */
    public $storage_key;
    /**
     * The date and time that object has been created or uploaded on the system.
     * @var datetime $created
     */
    public $created;
    /**
     * The id of the parent element. If the object has a parent element, the parent element only can be a folder type
     * object. Each element must have only one parent element, but a parent element can have any children's. This
     * child's can be type of file or directory object.
     *
*@var int $parent
     */
    public $parent;
    /**
     * This boolean value set if the object is available on the system or is located on the system trash.
     * If this object is in trash only systems administrators can view this files.
     * The only options for this files is restore to original place or remove permanently from the system.
     * @var boolean $in_trash;
     */
    public $in_trash;
    /**
     * This boolean set if an object is image or not
     * @var boolean $is_image;
     */
    public $is_image;

    /**
     * The description of the file that can be edited by user
     * @var text $description;
     */
    public $description;

    public $server_id;

}