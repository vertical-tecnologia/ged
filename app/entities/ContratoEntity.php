<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 04/07/15
 * Time: 15:55
 */

class ContratoEntity {
    public $contrato_id;
    public $contrato_valor;
    public $contrato_adesao;
    public $contrato_ativo;
    public $contrato_cliente;
    public $accepted;
    public $text;

    public function getArrayCopy()
    {
        return array(
            'contrato_id' => $this->contrato_id,
            'contrato_valor' => $this->contrato_valor,
            'contrato_adesao' => $this->contrato_adesao,
            'contrato_ativo' => $this->contrato_ativo,
            'contrato_cliente' => $this->contrato_cliente,
            'accepted' => $this->accepted,
            'text' => $this->text
        );
    }

    public function exchangeArray($data)
    {
        $this->contrato_id = $data['contrato_id'];
        $this->contrato_valor = $data['contrato_valor'];
        $this->contrato_adesao = $data['contrato_adesao'];
        $this->contrato_ativo = $data['contrato_ativo'];
        $this->contrato_cliente = $data['contrato_cliente'];
        $this->accepted = $data['accepted'];
        $this->text = $data['text'];
    }
}