<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 15/06/15
 * Time: 14:26
 */

class ContatoEntity {
    public $contato_id;
    public $contato_valor;
    public $tipo_id;
    public $contato_empresa_id;
    public $contato_user_id;

    public function getArrayCopy()
    {
        return array(
            'contato_id' => $this->contato_id,
            'contato_valor' => $this->contato_valor,
            'tipo_id' => $this->tipo_id,
            'contato_empresa_id' => $this->contato_empresa_id,
            'contato_user_id' => $this->contato_user_id
        );
    }

    public function exchangeArray($data)
    {
        $this->contato_id = $data['contato_id'];
        $this->contato_valor = $data['contato_valor'];
        $this->tipo_id = $data['tipo_id'];
        $this->contato_empresa_id = $data['contato_empresa_id'];
        $this->contato_user_id = $data['contato_user_id'];
    }
}