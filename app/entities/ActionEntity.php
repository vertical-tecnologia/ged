<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 06/06/15
 * Time: 18:16
 */

class ActionEntity {

    public $action_id;
    public $action_name;
    public $action_module_id;

    public function getArrayCopy()
    {
        return array(
            'action_id' => $this->action_id,
            'action_name' => $this->action_name,
            'action_module_id' => $this->action_module_id
        );
    }

    public function exchangeArray($data)
    {
        $this->action_id = $data['action_id'];
        $this->action_name = $data['action_name'];
        $this->action_module_id = $data['action_module_id'];
    }
}