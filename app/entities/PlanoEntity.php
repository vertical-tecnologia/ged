<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 28/05/15
 * Time: 00:23
 */
class PlanoEntity
{

    public $plan_id;
    public $plan_name;
    public $plan_desc;
    public $plan_price;
    public $plan_status_id;
    public $service_id;
    public $server_id;
    public $plano_max_users;

    public function getArrayCopy()
    {
        return array(
            'plan_id' => $this->plan_id,
            'plan_name' => $this->plan_name,
            'plan_desc' => $this->plan_desc,
            'plan_price' => $this->plan_price,
            'plan_status_id' => $this->plan_status_id,
            'service_id' => $this->service_id,
            'server_id' => $this->server_id,
            'plano_max_users' => $this->plano_max_users
        );
    }

    public function exchangeArray($data)
    {
        $this->plan_id = $data['plan_id'];
        $this->plan_name = $data['plan_name'];
        $this->plan_desc = $data['plan_desc'];
        $this->plan_price = $data['plan_price'];
        $this->plan_status_id = $data['plan_status_id'];
        $this->service_id = $data['service_id'];
        $this->server_id = $data['server_id'];
        $this->plano_max_users = $data['plano_max_users'];

    }
}