<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 16/07/15
 * Time: 12:38
 */

class ObjectLog {
    public $log_id;
    public $log_action;
    public $user_key;
    public $hash;
    public $datetime;
    public $text;
    public $alert;
}