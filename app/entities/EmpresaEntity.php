<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 01/06/15
 * Time: 22:26
 */

class EmpresaEntity {

    public $empresa_id;
    public $empresa_razao;
    public $empresa_fantasia;
    public $empresa_cnpj;
    public $empresa_ie;
    public $empresa_segmento_id;
    public $empresa_user_id;
    public $empresa_key;
    public $empresa_principal;
    public $empresa_cpf_responsavel;
    public $empresa_rg_responsavel;
    public $total_server;

    public function getArrayCopy()
    {
        return array(
            'empresa_id'              => $this->empresa_id,
            'empresa_razao'           => $this->empresa_razao,
            'empresa_fantasia'        => $this->empresa_fantasia,
            'empresa_cnpj'            => $this->empresa_cnpj,
            'empresa_ie'              => $this->empresa_ie,
            'empresa_segmento_id'     => $this->empresa_segmento_id,
            'empresa_user_id'         => $this->empresa_user_id,
            'empresa_key'             => $this->empresa_key,
            'empresa_principal'       => $this->empresa_principal,
            'empresa_cpf_responsavel' => $this->empresa_cpf_responsavel,
            'empresa_rg_responsavel'  => $this->empresa_rg_responsavel,
            'total_server'            => $this->total_server,
        );
    }

    public function exchangeArray($data)
    {
        $this->empresa_id              = $data['empresa_id'];
        $this->empresa_razao           = $data['empresa_razao'];
        $this->empresa_fantasia        = $data['empresa_fantasia'];
        $this->empresa_cnpj            = $data['empresa_cnpj'];
        $this->empresa_ie              = $data['empresa_ie'];
        $this->empresa_segmento_id     = $data['empresa_segmento_id'];
        $this->empresa_user_id         = $data['empresa_user_id'];
        $this->empresa_key             = $data['empresa_key'];
        $this->empresa_principal       = $data['empresa_principal'];
        $this->empresa_cpf_responsavel = $data['empresa_cpf_responsavel'];
        $this->empresa_rg_responsavel  = $data['empresa_rg_responsavel'];
        $this->total_server            = $data['total_server'];
    }
}