<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * Date: 19/07/15
 * @package		App\Entities
 * Time: 15:12
 */

class ShareEntity {

    public $share_id;
    public $object;
    public $user;
    public $group;
    public $expires;
    public $shared_at;
    public $email;

}