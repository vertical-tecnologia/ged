<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 25/06/15
 * Time: 17:24
 */

class FaturaEntity {

    public $fatura_id;
    public $fatura_emissao;
    public $fatura_vencimento;
    public $client_id;
    public $status_id;
    public $fatura_obs;
    public $fatura_itens;
    public $fatura_hash;

    public function getArrayCopy()
    {
        return array(
            'fatura_id' => $this->fatura_id,
            'fatura_emissao' => $this->fatura_emissao,
            'fatura_vencimento' => $this->fatura_vencimento,
            'client_id' => $this->client_id,
            'status_id' => $this->status_id,
            'fatura_obs' => $this->fatura_obs,
            'fatura_itens' => $this->$fatura_itens,
            'fatura_hash' => $this->fatura_hash
        );
    }

    public function exchangeArray($data)
    {
        $this->fatura_id = $data['fatura_id'];
        $this->fatura_emissao = $data['fatura_emissao'];
        $this->fatura_vencimento = $data['fatura_vencimento'];
        $this->client_id = $data['client_id'];
        $this->status_id = $data['status_id'];
        $this->fatura_obs = $data['fatura_obs'];
        $this->fatura_itens = $data['fatura_itens'];
        $this->fatura_hash = $data['fatura_hash'];
    }
}