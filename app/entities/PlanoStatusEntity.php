<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 27/05/15
 * Time: 17:01
 */

class PlanoStatusEntity{

    public $status_id;
    public $status_name;
    public $status_editable;

    public function getArrayCopy()
    {
        return array(
            'status_id' => $this->status_id,
            'status_name' => $this->status_name,
            'status_editable' => $this->status_editable
        );
    }

    public function exchangeArray($data)
    {
        $this->status_id = $data['status_id'];
        $this->status_name = $data['status_name'];
        $this->status_editable = $data['status_editable'];
    }
}