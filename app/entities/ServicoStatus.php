<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 27/05/15
 * Time: 17:01
 */

class ServicoStatus {

    public $service_status_id;
    public $service_status_name;

    public function getArrayCopy()
    {
        return array(
            'service_status_id' => $this->service_status_id,
            'service_status_name' => $this->service_status_name
        );
    }

    public function exchangeArray($data)
    {
        $this->service_status_id = $data['service_status_id'];
        $this->service_status_name = $data['service_status_name'];
    }
}