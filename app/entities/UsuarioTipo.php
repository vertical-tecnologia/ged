<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 27/05/15
 * Time: 17:01
 */

class UsuarioTipo {

    public $type_id;
    public $type_name;
    public $type_desc;
    public $type_editable;

    public function getArrayCopy()
    {
        return array(
            'type_id' => $this->type_id,
            'type_name' => $this->type_name,
            'type_desc' => $this->type_desc,
            'type_editable' => $this->type_editable
        );
    }

    public function exchangeArray($data)
    {
        $this->type_id = $data['type_id'];
        $this->type_name = $data['type_name'];
        $this->type_desc = $data['type_desc'];
        $this->type_editable = $data['type_editable'];
    }
}