<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 13/08/15
 * Time: 19:22
 */
class GroupPermissionEntity
{
    public $permission_id;
    public $group_id;
    public $object_id;
    public $readable;
    public $listable;
    public $writable;
    public $deletable;


    public function getArrayCopy()
    {
        return array(
            'permission_id' => $this->permission_id,
            'group_id' => $this->group_id,
            'object_id' => $this->object_id,
            'readable' => $this->readable,
            'listable' => $this->listable,
            'writable' => $this->writable,
            'deletable' => $this->deletable,
        );
    }

    public function exchangeArray($data)
    {
        $this->permission_id = $data['permission_id'];
        $this->group_id = $data['group_id'];
        $this->object_id = $data['object_id'];
        $this->readable = $data['readable'];
        $this->listable = $data['listable'];
        $this->writable = $data['writable'];
        $this->deletable = $data['deletable'];
    }
}