<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 11/07/15
 * Time: 13:02
 */

class GroupEntity {

    /**
     * The group id key
     * @access public
     * @var int $group_id
     */
    public $group_id;
    /**
     * The group name on system
     * @access public
     * @var string $group_name
     */
    public $group_name;
    /**
     * The company id on system
     * @access public
     * @var int $empresa_id
     */
    public $empresa_id;
    /**
     * The user key id that create this group
     * @access public
     * @var int $group_owner
     */
    public $group_owner;

    public $read;

    public $del;

    public $write;
}