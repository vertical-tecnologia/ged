<?php

/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 28/08/15
 * Time: 19:26
 */
class ContatoUsuarioEntity
{
    public $usuario_contato_id;
    public $usuario_id;
    public $contato_id;

    public function getArrayCopy()
    {
        return array(
            'usuario_contato_id' => $this->usuario_contato_id,
            'usuario_id' => $this->usuario_id,
            'contato_id' => $this->contato_id,
        );
    }

    public function exchangeArray($data)
    {
        $this->usuario_contato_id = $data['usuario_contato_id'];
        $this->usuario_id = $data['usuario_id'];
        $this->contato_id = $data['contato_id'];
    }
}