<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 04/06/15
 * Time: 16:39
 */

class EnderecoEntity {

    public $endereco_id;
    public $endereco_logradouro;
    public $endereco_num;
    public $endereco_bairro;
    public $endereco_cep;
    public $endereco_estado;
    public $endereco_cidade;
    public $endereco_complemento;
    public $endereco_alias;
    public $empresa_id;
    public $endereco_principal;
    public $user_id;

    public function getArrayCopy()
    {
        return array(
            'endereco_id' => $this->endereco_id,
            'endereco_logradouro' => $this->endereco_logradouro,
            'endereco_bairro' => $this->endereco_bairro,
            'endereco_num' => $this->endereco_num,
            'endereco_cep' => $this->endereco_cep,
            'endereco_estado' => $this->endereco_estado,
            'endereco_cidade' => $this->endereco_cidade,
            'endereco_complemento' => $this->endereco_complemento,
            'endereco_alias' => $this->endereco_alias,
            'endereco_principal' => $this->endereco_principal,
            'user_id' => $this->user_id
        );
    }

    public function exchangeArray($data)
    {
        $this->endereco_id = $data['endereco_id'];
        $this->endereco_logradouro = $data['endereco_logradouro'];
        $this->endereco_num = $data['endereco_num'];
        $this->endereco_bairro = $data['endereco_bairro'];
        $this->endereco_cep = $data['endereco_cep'];
        $this->endereco_estado = $data['endereco_estado'];
        $this->endereco_cidade = $data['endereco_cidade'];
        $this->endereco_complemento = $data['endereco_complemento'];
        $this->endereco_alias = $data['endereco_alias'];
        $this->endereco_principal = $data['endereco_principal'];
        $this->user_id = $data['user_id'];
    }
}