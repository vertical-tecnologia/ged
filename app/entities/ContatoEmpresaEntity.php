<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 15/06/15
 * Time: 14:50
 */

class ContatoEmpresaEntity {

    public $empresa_contato_id;
    public $empresa_id;
    public $contato_id;
    public $principal;

    public function getArrayCopy()
    {
        return array(
            'empresa_contato_id' => $this->empresa_contato_id,
            'empresa_id' => $this->empresa_id,
            'contato_id' => $this->contato_id,
            'principal' => $this->principal
        );
    }

    public function exchangeArray($data)
    {
        $this->empresa_contato_id = $data['empresa_contato_id'];
        $this->empresa_id = $data['empresa_id'];
        $this->contato_id = $data['contato_id'];
        $this->principal = $data['principal'];
    }
}