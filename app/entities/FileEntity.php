<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 20/06/15
 * Time: 14:14
 */

class FileEntity {

    public $file_id;
    public $file_name;
    public $file_path;
    public $file_type_id;
    public $file_original_name;
    public $file_size;
    public $file_extension;
    public $file_uploaded;
    public $file_user_id;
    public $file_isimage;
    public $client_volume;
    public $parent_of;
    public $owner;

}