<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 25/06/15
 * Time: 10:08
 */

class UsuarioEnderecoEntity {
    public $user_endereco_id;
    public $user_id;
    public $endereco_id;
    public $endereco_logradouro;
    public $endereco_num;
    public $endereco_bairro;
    public $endereco_cep;
    public $endereco_estado;
    public $endereco_cidade;
    public $endereco_complemento;
    public $endereco_alias;

    public function getArrayCopy()
    {
        return array(
            'user_endereco_id' => $this->user_endereco_id,
            'user_id' => $this>user_id,
            'endereco_id' => $this->endereco_id,
            'endereco_logradouro' => $this->endereco_logradouro,
            'endereco_bairro' => $this->endereco_bairro,
            'endereco_num' => $this->endereco_num,
            'endereco_cep' => $this->endereco_cep,
            'endereco_estado' => $this->endereco_estado,
            'endereco_cidade' => $this->endereco_cidade,
            'endereco_complemento' => $this->endereco_complemento,
            'endereco_alias' => $this->endereco_alias
        );
    }

    public function exchangeArray($data)
    {
        $this->user_endereco_id = $data['user_endereco_id'];
        $this->user_id = $data['user_id'];
        $this->endereco_id = $data['endereco_id'];
        $this->endereco_logradouro = $data['endereco_logradouro'];
        $this->endereco_num = $data['endereco_num'];
        $this->endereco_bairro = $data['endereco_bairro'];
        $this->endereco_cep = $data['endereco_cep'];
        $this->endereco_estado = $data['endereco_estado'];
        $this->endereco_cidade = $data['endereco_cidade'];
        $this->endereco_complemento = $data['endereco_complemento'];
        $this->endereco_alias = $data['endereco_alias'];
    }
}