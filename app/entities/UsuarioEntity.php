<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 27/05/15
 * Time: 18:56
 */

class UsuarioEntity {

    public $user_id;
    public $user_fullname;
    public $user_login;
    public $user_email;
    public $user_password;
    public $user_trusted_mail;
    public $user_register_date;
    public $user_hash;
    public $user_status_id;
    public $user_type_id;
    public $user_group_id;
    public $user_plan_id;
    public $user_key;
    public $user_picture;
    public $owner;
    public $redefinition_key;


    public function getArrayCopy()
    {
        return array(
            'user_id' => $this->user_id,
            'user_fullname' => $this->user_fullname,
            'user_login' => $this->user_login,
            'user_email' => $this->user_email,
            'user_password' => $this->user_password,
            'user_trusted_mail' => $this->user_trusted_mail,
            'user_register_date' => $this->user_register_date,
            'user_hash' => $this->user_hash,
            'user_status_id' => $this->user_status_id,
            'user_type_id' => $this->user_type_id,
            'user_group_id' => $this->user_group_id,
            'user_plan_id' => $this->user_plan_id,
            'user_key' => $this->user_key,
            'user_picture' => $this->user_picture,
            'owner' => $this->owner,
            'redefinition_key' => $this->redefinition_key,
        );
    }

    public function exchangeArray($data)
    {
        $this->user_id = $data['user_id'];
        $this->user_fullname = $data['user_fullname'];
        $this->user_login = $data['user_login'];
        $this->user_email = $data['user_email'];
        $this->user_password = $data['user_password'];
        $this->user_trusted_mail = $data['user_trusted_mail'];
        $this->user_register_date = $data['user_register_date'];
        $this->user_hash = $data['user_hash'];
        $this->user_status_id = $data['user_status_id'];
        $this->user_type_id = $data['user_type_id'];
        $this->user_group_id = $data['user_group_id'];
        $this->user_plan_id = $data['user_plan_id'];
        $this->user_key = $data['user_key'];
        $this->user_picture = $data['user_picture'];
        $this->owner = $data['owner'];
        $this->redefinition_key = $data['redefinition_key'];
    }
}