<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 15/06/15
 * Time: 11:50
 */

class ContatoTipoEntity {

    public $tipo_id;
    public $tipo_desc;

    public function getArrayCopy()
    {
        return array(
            'tipo_id' => $this->tipo_id,
            'tipo_desc' => $this->tipo_desc,
        );
    }

    public function exchangeArray($data)
    {
        $this->tipo_id = $data['tipo_id'];
        $this->tipo_desc = $data['tipo_desc'];
    }
}