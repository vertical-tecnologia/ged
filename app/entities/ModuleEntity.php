<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * @package		App\Entities
 * Date: 06/06/15
 * Time: 16:34
 */

class ModuleEntity {

    public $module_id;
    public $module_name;
    public $module_system;

    public function getArrayCopy()
    {
        return array(
            'module_id' => $this->module_id,
            'module_name' => $this->module_name,
        );
    }

    public function exchangeArray($data)
    {
        $this->module_id  =$data['module_id'];
        $this->module_name  =$data['module_name'];
    }
}