<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group  = 'ged';
$active_record = TRUE;

$db['ged']['hostname'] = 'localhost';
$db['ged']['username'] = 'root';
$db['ged']['password'] = 'root';
$db['ged']['database'] = 'ged';
$db['ged']['dbdriver'] = 'mysqli';
$db['ged']['dbprefix'] = '';
$db['ged']['pconnect'] = '';
$db['ged']['db_debug'] = true;
$db['ged']['cache_on'] = false;
$db['ged']['cachedir'] = FCPATH.APPPATH.'cache/';
$db['ged']['char_set'] = 'utf8';
$db['ged']['dbcollat'] = 'utf8_general_ci';
$db['ged']['swap_pre'] = '';
$db['ged']['autoinit'] = true;
$db['ged']['stricton'] = false;

$db['cep']['hostname'] = 'localhost';
$db['cep']['username'] = 'root';
$db['cep']['password'] = 'root';
$db['cep']['database'] = 'cep';
$db['cep']['dbdriver'] = 'mysqli';
$db['cep']['dbprefix'] = '';
$db['cep']['pconnect'] = '';
$db['cep']['db_debug'] = true;
$db['cep']['cache_on'] = false;
$db['cep']['cachedir'] = FCPATH.APPPATH.'cache/';
$db['cep']['char_set'] = 'utf8';
$db['cep']['dbcollat'] = 'utf8_general_ci';
$db['cep']['swap_pre'] = '';
$db['cep']['autoinit'] = true;
$db['cep']['stricton'] = false;

$db['production']['hostname'] = 'mysql.hostinger.com.br';
$db['production']['username'] = 'u534220876_cnged';
$db['production']['password'] = 'YK^&$0l|1v>Cd$oCEt';
$db['production']['database'] = 'u534220876_cnged';
$db['production']['dbdriver'] = 'mysqli';
$db['production']['dbprefix'] = '';
$db['production']['pconnect'] = '';
$db['production']['db_debug'] = true;
$db['production']['cache_on'] = false;
$db['production']['cachedir'] = FCPATH.APPPATH.'cache/';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = true;
$db['production']['stricton'] = false;




/* End of file database.php */
/* Location: ./application/config/database.php */
