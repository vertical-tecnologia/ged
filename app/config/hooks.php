<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/



//$hook['post_controller_constructor'] = array('function' => 'log_profiler',
//                                             'filename' => 'log_profiler.php',
//                                             'filepath' => 'hooks'
//);

$hook['post_controller_constructor'][] = array(
    'class'    => 'system_configuration',
    'function' => 'verify_configuration',
    'filename' => 'system_configuration.php',
    'filepath' => 'hooks',
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'user_customization',
    'function' => 'user_customization',
    'filename' => 'user_customization.php',
    'filepath' => 'hooks',
);

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */
