<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	'actions' => array(
		array('field' => 'action_module_id', 'label' => 'Módulo', 'rules' => 'required'),
		array('field' => 'action_name', 'label' => 'Ação', 'rules' => 'required')
	),

	'plano' => array(
		array('field' => 'plan_name', 'label' => 'Módulo', 'rules' => 'required'),
		array('field' => 'plan_desc', 'label' => 'Ação', 'rules' => 'required'),
		array('field' => 'plan_status_id', 'label' => 'Ação', 'rules' => 'required'),
		array('field' => 'service_id', 'label' => 'Ação', 'rules' => 'required')
	),

	'plano_status' => array(
		array('field' => 'status_name', 'label' => 'Nome do status', 'rules' => 'required')
	),
	'servico_status' => array(
		array('field' => 'service_status_name', 'label' => 'Nome do status', 'rules' => 'required')
	),
	'servico' => array(
		array('field' => 'service_name', 'label' => 'Nome', 'rules' => 'required'),
		array('field' => 'service_desc', 'label' => 'Descrição', 'rules' => 'required'),
		array('field' => 'status_id', 'label' => 'Status', 'rules' => 'required'),
	),
	'usuario_tipo' => array(
		array('field' => 'type_name', 'label' => 'Tipo', 'rules' => 'required'),
		array('field' => 'type_desc', 'label' => 'Descrição', 'rules' => 'required')
	),
	'usuario_status' => array(
		array('field' => 'status_name', 'label' => 'Status', 'rules' => 'required'),
	),
	'grupo' => array(
		array('field' => 'group_name', 'label' => 'Grupo', 'rules' => 'required')
	),
	'segmento_status'=> array(
		array('field' => 'status_name', 'label' => 'Status', 'rules' => 'required'),
	),
	'segmento'=> array(
		array('field' => 'segmento_name', 'label' => 'Segmento', 'rules' => 'required'),
		array('field' => 'segmento_status_id', 'label' => 'Status', 'rules' => 'required'),
	),
	'modulo'=> array(
		array('field' => 'module_name', 'label' => 'Módulo', 'rules' => 'required'),
	),
	'empresa' => array(
		array('field' => 'empresa_razao', 'label' => 'Razão Social'),
		array('field' => 'empresa_fantasia', 'label' => 'Nome Fantasia'),
		array('field' => 'empresa_cnpj', 'label' => 'CNPJ', 'rules' => 'required|unique_cnpj_cpf'),
		array('field' => 'empresa_segmento_id', 'label' => 'Segmento', 'rules' => 'required'),
	),
	'empresa_edit' => array(
		array('field' => 'empresa_razao', 'label' => 'Razão Social'),
		array('field' => 'empresa_fantasia', 'label' => 'Nome Fantasia'),
		array('field' => 'empresa_cnpj', 'label' => 'CNPJ', 'rules' => 'required'),
	),
	'empresa_cpf' => array(
		array('field' => 'empresa_cpf_responsavel', 'label' => 'CPF', 'rules' => "required|unique_cnpj_cpf"),
		array('field' => 'empresa_rg_responsavel', 'label' => 'RG', 'rules' => "required|unique_rg"),
		array('field' => 'empresa_segmento_id', 'label' => 'Segmento', 'rules' => 'required'),
	),
	'endereco' => array(
		array('field' => 'empresa_cep', 'label' => 'CEP', 'rules' => 'required'),
		array('field' => 'empresa_logradouro', 'label' => 'Logradouro', 'rules' => 'required'),
		),
	'endereco_initial' => array(
		array('field' => 'endereco_alias' , 'label' => 'Apelido', 'rules' => 'required'),
		array('field' => 'endereco_cep' , 'label' => 'CEP', 'rules' => 'required'),
		array('field' => 'endereco_logradouro' , 'label' => 'Logradouro', 'rules' => 'required'),
		array('field' => 'endereco_num' , 'label' => 'Número', 'rules' => 'required'),
		array('field' => 'endereco_bairro' , 'label' => 'Bairro', 'rules' => 'required'),
		array('field' => 'endereco_cidade' , 'label' => 'Cidade', 'rules' => 'required'),
		array('field' => 'endereco_estado' , 'label' => 'Estado', 'rules' => 'required'),
	),
	'contato_initial' => array(
		array('field' => 'email_empresa' , 'label' => 'Email', 'rules' => 'required'),
		array('field' => 'telefone_empresa' , 'label' => 'Telefone', 'rules' => 'required'),
	),
	'contato_tipo'=> array(
		array('field' => 'tipo_desc', 'label' => 'Nome', 'rules' => 'required'),
	),
	'quantidades'=> array(
		array('field' => 'quant_desc', 'label' => 'Descriçao', 'rules' => 'required'),
		array('field' => 'quant_preco', 'label' => 'Preço', 'rules' => 'required'),
		array('field' => 'quant_valor', 'label' => 'Quantidade (em bytes)', 'rules' => 'required'),
	),

	'usuario' => array(
		array('field' => 'user_fullname', 'label' => 'Nome Completo', 'rules' => 'required'),
		array('field' => 'user_login', 'label' => 'Login', 'rules' => 'required|callback_check_login'),
		array('field' => 'user_email', 'label' => 'Email', 'rules' => 'required|valid_email|callback_check_email'),
		array('field' => 'user_type_id', 'label' => 'Tipo de Usuário', 'rules' => 'required'),
		array('field' => 'user_password', 'label' => 'Senha', 'rules' => 'required|min_length[6]'),
		array('field' => 'cuser_password', 'label' => 'Confirmaçao da Senha', 'rules' => 'required|matches[user_password]|min_length[6]'),
	),

	'usuario_update' => array(
		array('field' => 'user_fullname', 'label' => 'Nome Completo', 'rules' => 'required'),
		array('field' => 'user_login', 'label' => 'Login', 'rules' => 'required'),
		array('field' => 'user_email', 'label' => 'Email', 'rules' => 'required|valid_email'),
		array('field' => 'user_type_id', 'label' => 'Tipo de Usuário', 'rules' => 'required'),
	),
);
