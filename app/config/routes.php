<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']     = "cadastro";
$route['404_override']           = '';

$route['boleto/(:any)']          = 'boleto/index';
$route['arquivos/lixeira']       = 'arquivos/folder/lixeira';
$route['arquivos/lixeira/clear'] = 'arquivos/clean_trash';
$route['arquivos/lixeira/empty'] = 'arquivos/empty_trash';
$route['login']                  = 'cadastro/login';
$route['forgot']                 = 'cadastro/forgot';
$route['reset/(:any)']           = 'cadastro/reset';

//$route['profile/(:any)']         = 'profile/index';
$route['profile/(:any)']         = 'profile/index';

$route['save_empresa']     = 'admin';
$route['save_contatos']    = 'admin';
$route['save_contrato']    = 'admin';

$route['usuario/admin']    = 'usuario/index/admin';
$route['usuario/clientes'] = 'usuario/index/clientes';

$route['log/clientes']     = 'log/index/clientes';

$route['adicional']        = 'configuracao/adicional';
$route['shared/(:any)']    = 'shared/index';
$route['download/(:any)']  = 'shared/download';

$route['download/windows'] = 'download/windows';
$route['download/macos']   = 'download/macos';
$route['download/linux']   = 'download/linux';
$route['meu_contrato']     = 'configuracao/meu_contrato';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
